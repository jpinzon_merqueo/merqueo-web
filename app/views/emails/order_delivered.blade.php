@extends('emails/email_layout')
@section('title') Merqueo | Califica tu pedido! @stop
@section('content')
  <!-- Content -->
  <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="scaleForMobile">
    <tr>
      <td width="100%">
        <!-- Header -->
        <table style="padding: 40px 0 0;" width="700"  bgcolor="#F9FAFC" border="0" cellpadding="0" cellspacing="0" align="center" class="scaleForMobile">
          <tr>
            <td style="font-size: 20px; line-height: 45px; color: #333333; text-align: center; font-weight: normal; font-family: Helvetica, Arial, sans-serif;" width="100%">
              <h1 style="margin: 0!important; font-weight: bold;">¿Salió todo bien <br><span style="font-weight: bold">con tu pedido?</span></h1>
            </td>
          </tr>
        </table>
        <table style="padding: 30px 0 10px;" width="700" bgcolor="#F9FAFC" border="0" cellpadding="0" cellspacing="0" align="center" class="scaleForMobile">
          <tr>
            <td style="font-size: 22px; color: #FFFFFF; text-align: center; font-weight: normal; font-family: Helvetica, Arial, sans-serif; line-height: 2px;" width="100%">
              <a href="{{ Config::get('app.url_web') }}/orden/{{ $order->user_score_token }}/calificacion#/choose-options" style="outline:none;text-decoration: none;border-radius: 4px;display:inline-block;color: #ffffff;font-style: normal;font-weight: bold;font-size: 22px;height: 60px;line-height: 60px;text-align: center;width: 60px;background: #F81616;border: 2px solid #F81616;margin-right: 15px;">No</a>
              <a href="{{ Config::get('app.url_web') }}/orden/{{ $order->user_score_token }}/calificacion#/?done=yes" style="outline:none;text-decoration: none;border-radius: 4px;display:inline-block;color: #ffffff;font-style: normal;font-weight: bold;font-size: 22px;height: 60px;line-height: 60px;text-align: center;width: 60px;background: #1BC47C;border: 2px solid #1BC47C;margin-left: 15px;">Sí</a>
            </td>
          </tr>
        </table><!-- End Header -->
        <table style="padding: 10px 0 30px;" width="700" bgcolor="#F9FAFC" border="0" cellpadding="0" cellspacing="0" align="center" class="scaleForMobile">
          <tr>
            <td style="font-size: 16px; color: #333333; text-align: center; font-weight: normal; font-family: Helvetica, Arial, sans-serif;" width="100%">
              <p style="font-size: 16pt; font-weight: bold;">Número: {{ $order->reference }}</p>
              <p style="color: #333333;"><img style="vertical-align: sub; position: relative; top: 2px; margin-right: 5px;" src="{{ asset_url() }}img/email_design/20190423/location.png" alt=""> Entregado en {{ $order_group->user_address.' '.$order_group->user_address_further }}</p>
              <p style="color: #333333;"><img style="vertical-align: sub; position: relative; top: 2px; margin-right: 5px;" src="{{ asset_url() }}img/email_design/20190423/calendar.png" alt="">Entregado el {{ format_date('normal_long_with_time', $order->management_date) }}</p>
              <p style="color: #333333;"><img style="vertical-align: middle; position: relative; top: 2px; margin-right: 5px;" src="{{ asset_url() }}img/email_design/20190423/cash.png" alt="">Método de pago {{ $order->payment_method }}</p>
            </td>
          </tr>
        </table><!-- End Header -->

      </td>
    </tr>
  </table><!-- End Main -->


  <!-- 3 Columns Middle Images -->
  <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="scaleForMobile">
    <tr>
      <td width="100%">
        <!-- Space -->
        <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="mobileCenter">
          <tr>
            <td width="100%" height="30">
            </td>
          </tr>
        </table>

        <table  width="700px" border="0" cellpadding="0" cellspacing="0" align="center" class="scaleForMobile">
          <tbody>
          <tr><td>
              <h1 style="font-size: 20px; color: #3f3f3f; font-family: Helvetica, Arial, sans-serif">Resumen del pedido</h1>
              <!-- 1st 3 Images -->
              <table width="100%" border="0" cellspacing="0" cellpadding="0" style="color: #545454; font-family: Helvetica, Arial, sans-serif; font-size: 14px;">
                <tr style="text-align: center; font-weight: bold">
                  <td class="productRes" style="border-bottom: 1px solid #ddd; padding: 5px; color: #073349; width: 5%;">Estado</td>
                  <td class="productRes" style="border-bottom: 1px solid #ddd; padding: 5px; color: #073349; width: 20%;">Imagen</td>
                  <td class="productRes" style="border-bottom: 1px solid #ddd; padding: 5px; color: #073349; width: 40%;">Producto</td>
                  <td class="productRes" style="border-bottom: 1px solid #ddd; padding: 5px; color: #073349; width: 10%;">Cantidad</td>
                  <td class="productRes" style="border-bottom: 1px solid #ddd; padding: 5px; color: #073349; width: 12%; text-align: right">Precio</td>
                  <td class="productRes" style="border-bottom: 1px solid #ddd; padding: 5px; color: #073349; width: 12%; text-align: right">Total</td>
                </tr>
                @if (!empty($products_not_available['products']) )
                  @foreach($products_not_available['products'] as $product)
                    <tr height="70">
                      <td style="text-align: center; @if(!isset($product['gift'])) border-bottom: 1px solid #ddd; @endif padding: 5px;">
                        <img style="max-width: 20px" src="{{ asset_url() }}img/email_design/20170727/{{ $product['status'] }}" width="100%" height="auto" alt="">
                      </td>
                      <td style="text-align: center; @if(!isset($product['gift'])) border-bottom: 1px solid #ddd; @endif padding: 5px;">
                        <img style="max-width: 100px" src="{{ $product['img'] }}" width="auto" height="100px" alt="">
                      </td>
                      <td style="@if(!isset($product['gift'])) border-bottom: 1px solid #ddd; @endif padding: 5px">{{ $product['name'] }} {{ $product['quantity'] }} {{ $product['unit'] }}</td>
                      <td style="text-align: center; @if(!isset($product['gift'])) border-bottom: 1px solid #ddd; @endif padding: 5px;">{{ $product['qty'] }}</td>
                      <td style="text-align: right; @if(!isset($product['gift'])) border-bottom: 1px solid #ddd; @endif padding: 5px;">${{ number_format($product['price'], 0, ',', '.') }}</td>
                      <td style="text-align: right; @if(!isset($product['gift'])) border-bottom: 1px solid #ddd; @endif padding: 5px;">${{ number_format($product['total'], 0, ',', '.') }}</td>
                    </tr>
                    @if(isset($product['gift']))
                      <tr height="70">
                        <td colspan="6">
                          <div style="background-color: #e9f5fa; margin: 10px; border-radius: 5px">
                            <div style="text-align: center; padding: 12px 5px; width: 15%; float: left">
                              <img style="max-width: 25px" src="{{ asset_url() }}img/email_design/20170727/gift.png" width="100%" height="auto" alt="">
                            </div>
                            <div style="padding: 5px; width: 85%; color: #545454; line-height: 20px">
                              <strong>{{ $product['gift']['message'] }}</strong>
                            </div>
                          </div>
                        </td>
                      </tr>
                      <tr height="70">
                        <td style="text-align: center; border-bottom: 1px solid #ddd; padding: 5px;">
                          <img style="max-width: 20px" src="{{ asset_url() }}img/email_design/20170727/arrow.png" width="100%" height="auto" alt="">
                        </td>
                        <td style="text-align: center; border-bottom: 1px solid #ddd; padding: 5px;"><img src="{{ $product['gift']['img'] }}" alt="" width="auto" height="100px" alt=""></td>
                        <td style="border-bottom: 1px solid #ddd; padding: 5px">{{ $product['name'] }} {{ $product['gift']['quantity'] }} {{ $product['gift']['unit'] }}</td>
                        <td style="text-align: center; border-bottom: 1px solid #ddd; padding: 5px;">{{ $product['gift']['qty'] }}</td>
                        <td style="text-align: right; border-bottom: 1px solid #ddd; padding: 5px;">${{ number_format($product['gift']['price'], 0, ',', '.') }}</td>
                        <td style="text-align: right; border-bottom: 1px solid #ddd; padding: 5px; color: #fc1569; font-weight: bold">GRATIS</td>
                      </tr>
                    @endif
                  @endforeach
                @endif
                @if ( !empty($products_available['products']) )
                  @foreach($products_available['products'] as $product)
                    <tr height="70">
                      <td style="text-align: center;  @if(!isset($product['sample'])) border-bottom: 1px solid #ddd; @endif padding: 5px;">
                        <img style="max-width: 20px" src="{{ asset_url() }}img/email_design/20170727/{{ $product['status'] }}" width="100%" height="auto" alt="">
                      </td>
                      <td style="text-align: center; border-bottom: 1px solid #ddd; padding: 5px;">
                        <img style="max-width: 100px" src="{{ $product['img'] }}" width="auto" height="100px" alt="">
                      </td>
                      <td style="border-bottom: 1px solid #ddd; padding: 5px; width: 40%;">{{ $product['name'] }} {{ $product['quantity'] }} {{ $product['unit'] }}
                        @if($product['qty_original'] > $product['qty'])
                          <br><p style="color:#ff0000;">
                            <img src="{{ asset_url() }}img/email_design/20180418/no-disponible.png">
                            {{ $product['qty_original'] - $product['qty'] }}
                            @if(($product['qty_original'] - $product['qty']) == 1) unidad no disponible @else unidades no disponibles @endif
                          </p>
                        @endif
                      </td>
                      <td style="text-align: center; border-bottom: 1px solid #ddd; padding: 5px; width: 10%;">{{ $product['qty'] }}</td>
                      <td style="text-align: right; border-bottom: 1px solid #ddd; padding: 5px; width: 15%;">${{ number_format($product['price'], 0, ',', '.') }}</td>
                      <td style="text-align: right; border-bottom: 1px solid #ddd; padding: 5px; width: 15%;">${{ number_format($product['total'], 0, ',', '.') }}</td>
                    </tr>
                    @if(isset($product['sample']) && $product['status'] == 'available.png')
                      <tr height="70">
                        <td colspan="6">
                          <div style="background-color: #e9f5fa; margin: 10px; border-radius: 5px; background-image: url('{{ asset_url() }}img/email_design/20170727/gift.png'); background-repeat: no-repeat; background-position-x: left; background-position-y: center; padding-left: 16px; padding-top: 5px; padding-bottom: 10px; background-size: 21px; background-origin: content-box;">
                            <div style=" width: 85%; color: #545454; line-height: 20px; padding-left: 133px;">
                              <strong>{{ $product['sample']['message'] }}</strong>
                            </div>
                          </div>
                        </td>
                      </tr>
                      <tr height="70">
                        <td style="text-align: center; border-bottom: 1px solid #ddd; padding: 5px;">
                          <img style="max-width: 20px" src="{{ asset_url() }}img/email_design/20170727/arrow.png" width="100%" height="auto" alt="">
                        </td>
                        <td style="text-align: center; border-bottom: 1px solid #ddd; padding: 5px;"><img src="{{ $product['sample']['img'] }}" alt="" style="max-width: 40px" width="100%" height="auto" alt=""></td>
                        <td style="border-bottom: 1px solid #ddd; padding: 5px">{{ $product['sample']['name'] }} {{ $product['sample']['quantity'] }} {{ $product['sample']['unit'] }}</td>
                        <td style="text-align: center; border-bottom: 1px solid #ddd; padding: 5px;">{{ $product['sample']['qty'] }}</td>
                        <td style="text-align: right; border-bottom: 1px solid #ddd; padding: 5px;">${{ number_format($product['sample']['price'], 0, ',', '.') }}</td>
                        <td style="text-align: right; border-bottom: 1px solid #ddd; padding: 5px; color: #fc1569; font-weight: bold">GRATIS</td>
                      </tr>
                    @endif
                  @endforeach
                @endif
                <tr height="30">
                  <td colspan="5" style="text-align: left; padding: 5px; font-weight: bold">Subtotal</td>
                  <td style="text-align: right; padding: 5px">${{ number_format($products_available['subtotal'] + $products_not_available['subtotal'], 0, ',', '.') }}</td>
                </tr>
                @if ($products_not_available['subtotal'])
                  <tr height="30">
                    <td colspan="5" style="text-align: left; padding: 5px; font-weight: bold; color: #ff0000;">Productos no disponibles</td>
                    <td style="text-align: right; padding: 5px; font-weight: bold; color: #ff0000;">-${{ number_format($products_not_available['subtotal'], 0, ',', '.') }}</td>
                  </tr>
                @endif
                <tr height="30">
                  <td colspan="5" style="text-align: left; padding: 5px; font-weight: bold">Costo domicilio</td>
                  <td style="text-align: right; padding: 5px">${{ number_format($order->delivery_amount, 0, ',', '.') }}</td>
                </tr>
                <tr height="30">
                  <td colspan="5" style="text-align: left; padding: 5px; font-weight: bold">Descuento</td>
                  <td style="text-align: right; padding: 5px">@if ( !empty( $order->discount_amount ) )-@endif${{ number_format($order->discount_amount, 0, ',', '.') }}</td>
                </tr>
                <tr height="50">
                  <td colspan="5" style="border-top: 1px solid #ddd; text-align: left; font-size: 18px; padding: 5px; color: #3f3f3f;"><p><b>Total</b></p></td>
                  <td style="border-top: 1px solid #ddd; text-align: right; font-size: 18px; font-weight: bold; padding: 15px 5px; color: #073349; color: #3f3f3f;">${{ number_format($order->total_amount + $order->delivery_amount - $order->discount_amount, 0, ',', '.') }}</td>
                </tr>
              </table><!-- End Message 2 -->
            </td></tr>
          </tbody>
        </table>
      </td>
    </tr>
  </table><!-- End 3 Columns -->

  <!-- Products CTA -->
  <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="scaleForMobile">
    <tr>
      <td width="100%">

        <!-- Space -->
        <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="MobileCenter">
          <tr>
            <td width="100%" height="25">
            </td>
          </tr>
        </table>

        <!-- Icons -->
        <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="scaleForMobile">
          <tr>
            <td width="100%">
              <!-- Icons -->
              <table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="mobileCenter">
                <tr>
                  <td width="100%" style="text-align: center;">
                    <p style="text-align: center; color: #3f3f3f; font-size: 18px; font-weight: 100; font-family: Helvetica, Arial, sans-serif; margin: 0;">Sí necesitas ayuda, comun&iacute;cate con nosotros al correo electr&oacute;nico<br><b>{{ Config::get('app.admin_email') }}<b></p>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table><!-- Products CTA -->
      </td>
    </tr>
  </table><!-- Products CTA -->
  <!-- End Content -->
@stop