@extends('emails/email_layout')
@section('title')Merqueo | Pedido Recibido! @stop
@section('content')

    <!-- HEADER -->
    <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="scaleForMobile">
        <tr>
            <td width="100%">
                <!-- Header -->
                <table style="padding: 30px 0;" width="700"  bgcolor="#23bcbb" border="0" cellpadding="0" cellspacing="0" align="center" class="scaleForMobile">
                    <tr>
                        <td style="font-size: 16px; color: #FFFFFF; text-align: center; font-weight: normal; font-family: Helvetica, Arial, sans-serif;" width="100%">
                            <center><img src="{{ asset_url() }}img/email_design/20180418/icono-pedido-recibido.png" alt=""></center>
                            <h1 style="font-weight: normal;">HEMOS RECIBIDO <br><span style="font-weight: bold">EL PEDIDO #{{$order}}</span></h1>

                        </td>
                    </tr>
                </table><!-- End Header -->

            </td>
        </tr>
    </table><!-- End Main -->

    <!-- 3 Columns Middle Images -->
    <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="scaleForMobile">
        <tr>
            <td width="100%">
                <!-- Space -->
                <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="mobileCenter">
                    <tr>
                        <td width="100%" height="30">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table><!-- End 3 Columns -->

    <!-- Products CTA -->
    <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="scaleForMobile">
        <tr>
            <td width="100%">

                <!-- Space -->
                <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="MobileCenter">
                    <tr>
                        <td width="100%" height="25">
                        </td>
                    </tr>
                </table>

                <!-- Icons -->
                <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="scaleForMobile">
                    <tr>
                        <td width="100%">
                            <!-- Icons -->
                            <table style="padding: 2% 0;" width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="mobileCenter">
                                <tr>
                                    <td width="100%" style="text-align: center;">
                                        <p style="text-align: center; color: #3f3f3f; font-size: 18px; font-weight: 100; font-family: Helvetica, Arial, sans-serif; margin: 0;">Sí necesitas ayuda, comun&iacute;cate con nosotros al correo electr&oacute;nico<br><b>{{ Config::get('app.admin_email') }}<b></p><br>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table><!-- Products CTA -->

            </td>
        </tr>
    </table><!-- Products CTA -->
@stop
