<!doctype html>
<html lang="es">
<head>
    <base href="/">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title', 'El supermercado del ahorro. Entrega a domicilio - Merqueo.com')</title>
    <meta name="description"
          content="@yield('description', 'Haz mercado online de forma rápida y fácil, con los mejores precios. Paga con tarjeta de crédito, datáfono o efectivo contra entrega.')">
    <meta name="keywords"
          content="@yield('keywords', 'supermercado online, supermercado, mercado a domicilio, supermercado a domicilio, hipermercado, domicilios, merqueo.com, mercado express')">

    <link rel="shortcut icon" href="{{ web_url() }}/favicon.ico">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:400,700">
    <link href="https://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="{{asset_url()}}css/styles/merqueo.css?v={{Cache::get('js_version_number')}}">
    <link rel="stylesheet" href="{{asset_url()}}/css/bootstrap.min.css">

    <link rel="canonical" href="{{ Request::url() }}">
    <script>
        var fb_app_id = '{{ Config::get("app.facebook_app_id") }}';
        var web_url = '{{ web_url() }}';
        var google_api_key = '{{ Config::get("app.google_api_key") }}';
    </script>

    <script type="text/javascript" src="{{asset_url()}}js/jquery.min.js"></script>
    <script type="text/javascript" src="{{asset_url()}}js/bootstrap.min.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.10.1/firebase.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBvibhMsWZ-mUXjLzzqktEbxTTVOs65FxI"></script>
    <style>
        html, body, #map {
            width: 100%;
            height: 100%;
        }

        .info-order {
            position: fixed;
            width: 100%;
            left: 0;
            bottom: 0;
            height: 1px;
            transition: all .4s linear;
        }

        .info-order.showed {
            height: auto;
        }

        .info-order.showed .info-order-order {
            -webkit-transform: translateY(0%);
            transform: translateY(0%);
        }

        .info-order.showed .info-order-btn {
            background-color: white;
            color: #f70f73;
        }

        .info-order-order {
            width: 100%;
            background-color: white;
            list-style: none;
            margin: 0;
            padding: 20px;
            text-align: center;
            -webkit-transform: translateY(100%);
            transform: translateY(100%);
            transition: all .1s linear;
        }

        .info-order-order .img {
            width: 40px;
            height: 40px;
            box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.1);
            border-radius: 50%;
            overflow: hidden;
            display: inline-block;
            margin-right: 5px;
        }

        .info-order-order .img img {
            width: 100%;
            height: 100%;
            object-fit: cover;
        }

        .info-order-order .data {
            display: inline-block;
        }

        .info-order-order li {
            border-top: solid 0.5px #979797;
            padding: 10px 0;
        }

        .info-order-order li:first-child {
            border: none;
        }

        .info-order-order li p {
            text-align: center;
            margin: 0;
            font-size: 15px;
        }

        .info-order .info-order-btn {
            width: 80px;
            border-radius: 6px;
            background-color: #35e6b1;
            box-shadow: 0 -1px 4px 0 rgba(0, 0, 0, 0.5);
            cursor: pointer;
            color: white;
            font-size: 18px;
            position: absolute;
            top: -36px;
            left: 50%;
            margin-left: -40px;
            transition: all .3s linear;
        }

        .info-order .info-order-btn img {
            width: 100%;
            height: 31px;
            transform: rotate(180deg);
        }

        .info-order .info-order-btn .open {
            display: block;
        }

        .info-order .info-order-btn .close {
            display: none;
            opacity: 1;
        }

        .info-order .info-order-btn.showed .close {
            display: block;
        }

        .info-order .info-order-btn.showed .open {
            display: none;
        }

        #InfoWindowMap {
            background-color: white;
            padding: 8px;
            margin: 0;
            border: solid 1px #00000045;
            border-radius: 5px;
            font-size: 14px;
        }
    </style>
</head>
<body>
<div id="map"></div>
<div class="info-order showed">
    <button class="btn info-order-btn showed">
        <img class="open"
             src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB2ZXJzaW9uPSIxLjEiIHZpZXdCb3g9IjAgMCAxMjkgMTI5IiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCAxMjkgMTI5IiB3aWR0aD0iNTEycHgiIGhlaWdodD0iNTEycHgiPgogIDxnPgogICAgPHBhdGggZD0ibTEyMS4zLDM0LjZjLTEuNi0xLjYtNC4yLTEuNi01LjgsMGwtNTEsNTEuMS01MS4xLTUxLjFjLTEuNi0xLjYtNC4yLTEuNi01LjgsMC0xLjYsMS42LTEuNiw0LjIgMCw1LjhsNTMuOSw1My45YzAuOCwwLjggMS44LDEuMiAyLjksMS4yIDEsMCAyLjEtMC40IDIuOS0xLjJsNTMuOS01My45YzEuNy0xLjYgMS43LTQuMiAwLjEtNS44eiIgZmlsbD0iI0ZGRkZGRiIvPgogIDwvZz4KPC9zdmc+Cg=="/>
        <img class="close"
             src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCFET0NUWVBFIHN2ZyBQVUJMSUMgIi0vL1czQy8vRFREIFNWRyAxLjEvL0VOIiAiaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkIj4KPHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB3aWR0aD0iMzJweCIgdmVyc2lvbj0iMS4xIiBoZWlnaHQ9IjMycHgiIHZpZXdCb3g9IjAgMCA2NCA2NCIgZW5hYmxlLWJhY2tncm91bmQ9Im5ldyAwIDAgNjQgNjQiPgogIDxnPgogICAgPHBhdGggZmlsbD0iI2Y1MTE3NCIgZD0iTTMuMzUyLDQ4LjI5NmwyOC41Ni0yOC4zMjhsMjguNTgsMjguMzQ3YzAuMzk3LDAuMzk0LDAuOTE3LDAuNTksMS40MzYsMC41OWMwLjUyLDAsMS4wNC0wLjE5NiwxLjQzNi0wLjU5ICAgYzAuNzkzLTAuNzg3LDAuNzkzLTIuMDYyLDAtMi44NDlsLTI5Ljk4LTI5LjczNWMtMC4yLTAuMi0wLjQ5NC0wLjM3NS0wLjc1Ny0wLjQ3NWMtMC43NS0wLjI4Mi0xLjU5Ny0wLjEwNy0yLjE2NiwwLjQ1NiAgIEwwLjQ3OSw0NS40NDdjLTAuNzkzLDAuNzg3LTAuNzkzLDIuMDYyLDAsMi44NDlDMS4yNzMsNDkuMDgyLDIuNTU4LDQ5LjA4MiwzLjM1Miw0OC4yOTZ6Ii8+CiAgPC9nPgo8L3N2Zz4K"/>
    </button>
    <ul class="info-order-order">
        <li>
            <div class="img">
                <img id="driver-img" src="/assets/img/avatar.png">
            </div>
            <div class="data">
                <p class="tx-gray-text">Tu pedido será entregado por</p>
                <p><b id="drive-name">{{ $driver->first_name }} {{ $driver->last_name }}</b></p>
            </div>
        </li>
        <li>
            <p id="number-orders" style="font-weight: bold"></p>
            <p class="tx-gray-text">Pronto llegaremos a tu destino</p>
        </li>
        <li>
            <p><b>{{$rangeOrder}}</b></p>
            <p class="tx-gray-text">RANGO DE ENTREGA DE TU PEDIDO</p>
        </li>
    </ul>
</div>
<script>
    var config = {
        apiKey: "{{ Config::get('app.firebase.general.key') }}",
        authDomain: "{{ Config::get('app.firebase.general.domain') }}",
        databaseURL: "{{ $url }}"
    };
    firebase.initializeApp(config);

    var db = firebase.database().ref('orders/{{ $order->id }}');
    var driver = firebase.database().ref('vehicles/{{ $vehicle_id }}');
    var markers = [];
    var ordersAll = [];
    var showInfo = true;
    var vehicle = null;

    var style = [
        {
            elementType: 'geometry',
            stylers: [{color: '#f5f5f5'}]
        },
        {
            elementType: 'labels.icon',
            stylers: [{visibility: 'off'}]
        },
        {
            elementType: 'labels.text.fill',
            stylers: [{color: '#616161'}]
        },
        {
            elementType: 'labels.text.stroke',
            stylers: [{color: '#f5f5f5'}]
        },
        {
            featureType: 'administrative.land_parcel',
            elementType: 'labels.text.fill',
            stylers: [{color: '#bdbdbd'}]
        },
        {
            featureType: 'poi',
            elementType: 'geometry',
            stylers: [{color: '#eeeeee'}]
        },
        {
            featureType: 'poi',
            elementType: 'labels.text.fill',
            stylers: [{color: '#757575'}]
        },
        {
            featureType: 'poi.park',
            elementType: 'geometry',
            stylers: [{color: '#e5e5e5'}]
        },
        {
            featureType: 'poi.park',
            elementType: 'labels.text.fill',
            stylers: [{color: '#9e9e9e'}]
        },
        {
            featureType: 'road',
            elementType: 'geometry',
            stylers: [{color: '#ffffff'}]
        },
        {
            featureType: 'road.arterial',
            elementType: 'labels.text.fill',
            stylers: [{color: '#757575'}]
        },
        {
            featureType: 'road.highway',
            elementType: 'geometry',
            stylers: [{color: '#dadada'}]
        },
        {
            featureType: 'road.highway',
            elementType: 'labels.text.fill',
            stylers: [{color: '#616161'}]
        },
        {
            featureType: 'road.local',
            elementType: 'labels.text.fill',
            stylers: [{color: '#9e9e9e'}]
        },
        {
            featureType: 'transit.line',
            elementType: 'geometry',
            stylers: [{color: '#e5e5e5'}]
        },
        {
            featureType: 'transit.station',
            elementType: 'geometry',
            stylers: [{color: '#eeeeee'}]
        },
        {
            featureType: 'water',
            elementType: 'geometry',
            stylers: [{color: '#c9c9c9'}]
        },
        {
            featureType: 'water',
            elementType: 'labels.text.fill',
            stylers: [{color: '#9e9e9e'}]
        }
    ];

    var map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 4.624335, lng: -74.063644},
        zoom: 0,
        styles: style,
        disableDefaultUI: true
    });
    var infoWindow = new google.maps.InfoWindow({map: map});
    var bounds = new google.maps.LatLngBounds();

    console.log('{{ $order->status }}');

    db.on('value', function (data) {
        const orders = data.val().statuses;
        addOrders(orders);
        centerMap();
    });

    driver.on('value', function (data) {
        var vpos = data.val();
        var pos = {
            lat: vpos.lat,
            lng: vpos.lng,
        }, marker;

        if (vehicle) {
            $.each(markers, function (key, val) {
                if (val.id === 1) {
                    val.setPosition(new google.maps.LatLng(pos.lat, pos.lng));
                    centerMap();
                }
            });
            vehicle.setPosition(new google.maps.LatLng(pos.lat, pos.lng));
        } else {
            marker = new google.maps.Marker({
                position: pos,
                map: map,
                icon: '/assets/img/map/cerdito.png',
                id: 1
            });
            vehicle = marker;
            markers.push(marker);
            centerMap();
        }
    });

    function myOrder() {
        var pos = {
            lat: {{ $coordinates['lat'] }},
            lng: {{ $coordinates['lng'] }}
        };
        var marker = new google.maps.Marker({
            position: pos,
            map: map,
            icon: '/assets/img/map/pin.png',
            id: 2
        });
        infoWindow.setPosition({
            lat: pos.lat,
            lng: pos.lng
        });
        infoWindow.setContent('<div id="InfoWindowMap"><b>{{ $direction }}</b></div>');

        markers.push(marker);

        var $siblings = $('.gm-style-iw').siblings();
        $siblings.remove();
    }

    function cleanOrders() {
        var newMarkers = [];
        $.each(markers, function (key, val) {
            if (val.id <= 2) {
                newMarkers.push(val);
            }else{
                markers[key].setMap(null);
            }
        });
        markers = [];
        markers = newMarkers;
    }

    function addOrders(orders) {
        ordersAll = [];
        cleanOrders();
        if (orders[2] && orders[2].is_checked === 1 && orders[2].is_on_the_way) {
            if(orders[2].is_on_the_way.orders_before){
                $.each(orders[2].is_on_the_way.orders_before, function (key, val) {
                    if (val.status === 'Dispatched' || val.status === 'Despachado') {
                        var pos = {
                            lat: parseFloat(val.user_address_latitude),
                            lng: parseFloat(val.user_address_longitude)
                        };
                        var marker = new google.maps.Marker({
                            position: pos,
                            map: map,
                            icon: '/assets/img/map/turno.png',
                            id: key + 3
                        });
                        markers.push(marker);
                        ordersAll.push(marker);
                    }
                });
            }
        }
        if (ordersAll.length > 0) {
            $('#number-orders').html("Hay " + ordersAll.length + " entregas antes que la tuya");
        } else {
            $('#number-orders').html("Tu pedido va en camino");
        }
        centerMap();
    }

    function centerMap() {
        $.each(markers, function (key, val) {
            bounds.extend(val.getPosition());
            map.fitBounds(bounds);
        });
    }

    function toogleInfo() {
        var $order = $('.info-order');
        var $btn = $('.info-order-btn');
        if (!showInfo) {
            $order.addClass('showed');
            $btn.addClass('showed');
            showInfo = true;
        } else {
            $order.removeClass('showed');
            $btn.removeClass('showed');
            showInfo = false;
        }
    }

    $('.info-order-btn').click(function () {
        toogleInfo();
    });

    $(window).load(function () {
        myOrder();
        //$('#number-orders').html(ordersAll.length);
        $('#number-orders').attr('data-orders',ordersAll.length);
    });

</script>
</body>
</html>