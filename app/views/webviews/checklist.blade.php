<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
    <meta name="description"
          content="El supermercado del ahorro. Entrega a domicilio en Bogotá, puedes pagar con tarjeta o en efectivo contra entrega. Todo sin moverte de casa.">
    <meta name="keywords"
          content="supermercado online, supermercado, mercado a domicilio, supermercado a domicilio, hipermercado, domicilios, merqueo.com, mercado express">
    <meta name="author" content="Merqueo">
    <title>Merqueo - El supermercado del ahorro. Entrega a domicilio.</title>
    <link rel="shortcut icon" href="https://merqueo.com/favicon.ico">
    <link href="{{ asset_url() }}/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{asset_url()}}css/styles/merqueo.css">
    <script type="text/javascript" src="{{ asset_url() }}/js/jquery.min.js"></script>
    <style>
        .color-active{
            border-color: #d0006f;
            border-width: 5px;
        }

        body {
            background-color: #f5f5f5;
        }

        .item{
            flex: 1 1 50%;
            padding: 0;
            margin: 0;
            position: relative;
        }

        .display-flex {
            display: flex;
            flex-wrap: wrap;
            justify-content: space-between;
        }
        .thumbnail {
            height: 100%;
        }

        /* extra positioning */
        .thumbnail {
            display: flex;
            flex-direction: column;
        }

        .thumbnail .caption {
            display: flex;
            flex-direction: column;
            justify-content: space-between;
            height: 100%;
        }

        .tag {
            float: left;
            position: absolute;
            left: 10%;
            top: 0px;
            z-index: 1000;
            background-color: #00aaec;
            padding: 20px;
            color: #FFFFFF;
            font-size: 30px;
            font-weight: bold;
        }

        @media only screen and (max-width: 980px) and (min-width: 5px)  {
            .panel-body {
                font-size: 32px !important;
            }

            .stats  {
                font-size: 32px !important;
            }
            .dates{
                font-size: 32px !important;
            }


            strong{
                font-size: 20px !important;
            }

            .panel-heading{
                font-size: 32px!important;

            }

            .list-group-item-heading{
                font-size: 28px !important;;
            }
        }

        .panel{
            box-shadow: 0 2px 4px rgba(0,0,0,0.15) !important;
        }

        .panel-merqueo > .panel-heading {
            color: #ffffff;
            background-color: #d0006f;
        }

        .panel-merqueo > .panel-heading + .panel-collapse .panel-body {
            border-top-color: #d0006f;
        }

        .panel-merqueo > .panel-footer + .panel-collapse .panel-body {
            border-bottom-color: #d0006f;
        }

        .dates{
            border:1px solid #ebeff2;
            border-radius:5px;
            padding:20px 0px;
            font-size:16px;
            color:#5aadef;
            font-weight:600;
            overflow:auto;
        }

        .dates div{
            float:left;
            width:50%;
            text-align:center;
            position:relative;
        }

        .dates strong,
        .stats strong{
            display:block;
            color:#adb8c2;
            font-size:11px;
            font-weight:700;
        }
        .dates span{
            width:1px;
            height:40px;
            position:absolute;
            right:0;
            top:0;
            background:#ebeff2;
        }
        .stats{
            border-top:1px solid #ebeff2;
            background:#f7f8fa;
            padding:15px 0;
            font-size:16px;
            color:#59687f;
            font-weight:600;
            margin-top: 36px;
            border-radius: 0 0 5px 5px;
        }
    </style>
    @include('common/analytics')
</head>

<body>
<div align="center" style="margin: 20px">
    <div class="row">
        <div class="col-xs-12 col-lg-12">
            <div class="panel panel-merqueo">
                <div class="panel-heading">Informacion del pedido</div>
                <div class="panel-body" >
                    <div class="dates">
                        <div class="start">
                            <strong>FRANJA DE ENTREGA</strong>
                            {{ $order->delivery_window ? $order->deliveryWindow->delivery_window : 'Sin asignar'}}
                            <span></span>
                        </div>
                        <div class="ends">
                            <strong>TURNO DE ENTREGA</strong>
                            {{ $planing_order ? $planing_order : 'Sin asignar' }}
                        </div>
                    </div>

                    <div class="stats">
                        <div class="row">
                            <div class="col-xs-{{$column_width}} col-lg-{{$column_width}}">
                                <strong>METODO DE PAGO</strong>
                                <span>{{ $order->payment_method }}</span>
                            </div>
                            <div class="col-xs-{{$column_width}} col-lg-{{$column_width}}">
                                <strong>COSTO DOMICILIO</strong>
                                @if($decimal_amount == 2)
                                    <span>$&nbsp;{{ number_format($order->delivery_amount, $decimal_amount) }}</span>
                                @else 
                                    <span>$&nbsp;{{ number_format($order->delivery_amount, $decimal_amount, ',', '.') }}</span>
                                @endif
                            </div>
                            @if($have_discount)
                            <div class="col-xs-{{$column_width}} col-lg-{{$column_width}}">
                                <strong>DESCUENTO</strong>
                                @if($decimal_amount == 2)
                                    <span>$&nbsp;{{ number_format($order->discount_amount, $decimal_amount) }}</span>
                                @else 
                                    <span>$&nbsp;{{ number_format($order->discount_amount, $decimal_amount, ',', '.') }}</span>
                                @endif
                            </div>
                            @endif
                            <div class="col-xs-{{$column_width}} col-lg-{{$column_width}}">
                                <strong>TOTAL A PAGAR</strong>
                                @if($decimal_amount == 2)
                                    <span>$&nbsp;{{ number_format($order->total_amount + $order->delivery_amount - $order->discount_amount, $decimal_amount) }} </span>
                                @else 
                                    <span>$&nbsp;{{ number_format($order->total_amount + $order->delivery_amount - $order->discount_amount, $decimal_amount, ',', '.') }} </span>
                                @endif
                            </div>
                            <div class="col-xs-{{$column_width}} col-lg-{{$column_width}}">
                                <strong style="margin-bottom: 5px">ESTADO DE PAGO</strong>
                                @if($order->payment_date == '')
                                    <span class="label label-danger ">PENDIENTE</span>
                                @else
                                    <span class="label label-success">PAGADO</span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

        @foreach($groups as $key=>$group)
        <div class="panel panel-merqueo" >
            <div class="panel-heading">
                @if($key == 'Not Available')
                    Productos no enviados
                @else
                    Productos enviados
                @endif
            </div>
            <div id="products" class="display-flex list-group">
                @foreach($group as $product)
                <div class="item"  >
                    <div class="thumbnail" id="row_{{$product->id}}" onclick="changeColor({{$product->id}})" data-isactive="false">
                        <img class="group list-group-image" src="{{ $product->product_image_url }}" alt="" />
                        <div class="caption">
                            <h4 class="group inner list-group-item-heading">
                                {{ $product->product_name.' '.$product->product_quantity.' '.$product->product_unit }}
                            </h4>
                            <div class="tag">
                                x{{ $product->quantity }}
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        @endforeach


</div>

<script type="text/javascript">
    function changeColor(id){
        var isActive = $("#row_"+id).attr("data-isactive");
        if(isActive === "false"){
            $("#row_"+id).addClass("color-active");
            $("#row_"+id).attr("data-isactive", "true");
        }else{
            $("#row_"+id).removeClass("color-active");
            $("#row_"+id).attr("data-isactive", "false");
        }
    }
    
</script>

</body>

</html>