<!doctype html>
<html lang="es">

<head>
    <base href="/">
    <meta charset="utf-8">
    <title>@yield('title', 'Mercado a domicilio en Bogotá - Merqueo.com')</title>
    <meta name="description" content="@yield('description', 'Haz mercado online de forma rápida y fácil, con los mejores precios, pide a los principales supermercados online de Colombia y uno de nuestros compradores lo seleccionara cuidadosamente por ti. Entregamos a domicilio en menos de 2 horas. Paga con tarjeta de crédito, datáfono o efectivo contra entrega.')">
    <meta name="keywords" content="@yield('keywords', 'supermercado online, supermercado, mercado a domicilio, surtifruver a domicilio, gastronomy market a domicilio, supermercado colsubsidio a domicilio, hipermercado, domicilios agrocampo, merqueo.com, merqueo express, Mercado a domicilio bogotá')">
    <meta name="author" content="Merqueo">
    <meta name="viewport" content="width=device-width">
    <meta property="fb:app_id" content="{{Config::get(" app.facebook_app_id ")}}"/>
    <meta property="og:locale" content="es_ES" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="@yield('title', 'Mercado a domicilio en Bogotá - Merqueo.com')" />
    <meta property="og:description" content="@yield('description', 'Haz mercado online de forma rápida y fácil, con los mejores precios, pide a los principales supermercados online de Colombia y uno de nuestros compradores lo seleccionara cuidadosamente por ti. Entregamos a domicilio en menos de 2 horas. Paga con tarjeta de crédito, datáfono o efectivo contra entrega.')" />
    <meta property="og:url" content="{{web_url()}}" />
    <meta property="og:site_name" content="Merqueo.com" />
    <meta property="og:image" content="{{asset_url()}}img/fb_logo.jpg" />
    <link rel="stylesheet" href="{{asset_url()}}/css/mobile.css">
    @include('common/meta_icons')
    <script>
        var fb_app_id = '{{Config::get("app.facebook_app_id")}}';
        var web_url = '{{web_url()}}';
        var cart_id = '{{Session::get("cart_id")}}';
        var store_id = '';
        var google_api_key = '{{Config::get("app.google_api_key")}}';
    </script>

    <script>
    	var url = "{{$footer['links']['android_url']}}";
    	@if ($controlmobile == '1') {
    		url = "{{$footer['links']['ios_url']}}";
    	}
    	@endif

      var isMobileSplash = true;
    </script>

    @include('common/analytics')

    <script type="text/javascript" src="{{asset_url()}}js/jquery.min.js"></script>
    <script type="text/javascript" src="{{asset_url()}}js/material.js"></script>
    <script type="text/javascript" src="{{asset_url()}}js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="{{asset_url()}}js/bootstrap.min.js"></script>
    <script type="text/javascript" src="{{asset_url()}}js/login.js?v={{ Cache::get('js_version_number') }}"></script>
    <script type="text/javascript" src="{{asset_url()}}/lib/fancybox/jquery.fancybox.pack.js"></script>
</head>

<body>
  <header class="main-header" style="text-align: center">
      <img src="{{asset_url()}}img/logonew.png" alt="Merqueo logo" style="width: 200px">
  </header>

  <div class="main-content">
    @yield('content')
  </div>

  <script>
      $(document).ready(function() {
          $('.fancybox').fancybox({
              fitToView: false,
              width: '70%',
              height: '70%',
              autoSize: true,
              closeClick: false,
              openEffect: 'none',
              closeEffect: 'none'
          });
      });
  </script>
</body>
</html>
