<?php
namespace services;

use contracts\ApiDashboardInterface;
use GuzzleHttp\Client as HttpClient;
use Illuminate\Support\Facades\Config;

/**
 * Class ApiDashboardService.
 * @author Gustavo Luna <gluna@merqueo.com>
 */
class ApiDashboardService implements ApiDashboardInterface
{   
    const DASHBOARD_UPDATE_STOCK_URL = "/api/store-product-warehouse/1.0/current-stock-refresh";
    
    /**
     * @var mixed
     */
    private $httpClient;

    /**
     * @var string
     */
    private $url;

    /**
     * Crea nueva instancia de ApiDashboardService.
     *
     * @param HttpClient $httpClient
     */
    public function __construct(HttpClient $httpClient)
    {
        $this->httpClient  = $httpClient;
        $this->url         = Config::get('services.dashboard.url');
    }

    /**
     * 
     * @param  int $warehouseId
     * @param  array $storeProductIds
     * @return bool  
     */
    public function currentStockRefresh($warehouseId, $storeProductsIds)
    {
        try {

            $this->httpClient->post($this->url . self::DASHBOARD_UPDATE_STOCK_URL, [
                'form_params' => [
                    'warehouse_id' => $warehouseId,
                    'ids' => $storeProductsIds
                ]
            ]);

        } catch (\Throwable $th) {
            throw new \Exception("Falló servicio currentStockRefresh Dashboard. ".$th->getMessage());
        }

        return true;
    }
}
