<?php

namespace App\Services;

use Config;

/**
 * OSRM service consume
 */
class OSRM extends ApiService
{
    function __construct()
    {
        parent::__construct(
            Config::get('app.url_osrm'),
            ['content-type' => 'application/json']
        );
    }

    /**
     * Function to consume the service OSRM Api
     */
    public function getDistanceMatrix(
        $origins, $destinations, 
        $options = ['sources' => 0, 'annotations' => 'distance,duration']
    ) {
        $url = '/table/v1/driving/' . $origins . ';' . $destinations . $this->prepareOptions($options);
        return $this->sendCall('GET', $url, []);
    }

    /**
     * Prepare options to send them to request if it's required
     *
     * @param Array $options
     * @return void
     */
    public function prepareOptions($options)
    {
        $stringOptions = '?';
        foreach ($options as $key => $value) {
            $stringOptions .= $key . '=' . $value .'&';
        }
        return trim($stringOptions,'&');
    }
}
