<?php

namespace App\Services;

use Illuminate\Http\Response;
use Zend\Http\Client;

/**
 * Esta clase consume los servicios expuestos por algun api
 *
 * @author Fabian Caicedo <fcaicedo@merqueo.com>
 */
class ApiService
{
    /**
     * @var string
     */
    private $url;

    /**
     * @var Array
     */
    private $headers;

    public function __construct($base_url, $headers)
    {
        $this->url = $base_url;
        $this->headers = $headers;
    }

    /**
     * Post Call tfor any json api
     *
     * @param String $route
     * @param array $body
     * @param array $headers
     * @return void
     */
    public function sendCall($method, $route, $body, $headers = [])
    {
        try {
            $this->setHeaders($headers);
            $client = new Client($this->url . $route);
            return json_decode(
                $client->setHeaders($this->headers)
                    ->setMethod($method)
                    ->setRawBody(json_encode($body))
                    ->send()
                    ->getBody(),
                true
            );
        } catch (\Throwable $th) {
            return json_decode(new Response(['error' => $th->getMessage()]), true);
        }
    }

    /**
     * Preparem the headers for request
     *
     * @param Array $headers
     * @return void
     */
    public function setHeaders($headers)
    {
        $this->headers = !empty($headers) ? array_merge($this->headers, $headers) : $this->headers;
    }
}
