<?php
namespace services;

use contracts\SmsService;
use GuzzleHttp\Client as HttpClient;
use Illuminate\Support\Facades\Config;

/**
 * Class InfoBipSmsService.
 *
 * Esta clase envía mensajes de texto conectándose a la API de InfoBip. Para
 * más información acerca de la API de envío de mensajes de texto, consultar
 * la documentación en:
 * https://dev.infobip.com/send-sms/single-sms-message
 *
 * @author Johan Alvarez <jalvarez@merqueo.com>
 */
class InfoBipSmsService implements SmsService
{
    /**
     * @var mixed
     */
    private $httpClient;

    /**
     * @var string
     */
    private $url;

    /**
     * @var string
     */
    private $accessToken;

    /**
     * @var string
     */
    private $from;

    /**
     * Crea nueva instancia de InfoBipSmsService.
     *
     * @param HttpClient $httpClient
     */
    public function __construct(HttpClient $httpClient)
    {
        $this->httpClient  = $httpClient;
        $this->from        = Config::get('app.name');
        $this->url         = Config::get('services.infobip.url');
        $this->accessToken = Config::get('services.infobip.key');
    }

    /**
     * @param  $to
     * @param  $text
     * @return bool
     */
    public function send($to, $text)
    {
        try {
            $response = $this->httpClient->post($this->url, [
                'json'    => [
                    'from' => $this->from,
                    'to'   => $to,
                    'text' => $text,
                ],
                'headers' => [
                    'Authorization' => "App {$this->accessToken}",
                    'Content-Type'  => 'application/json',
                    'Accept'        => 'application/json',
                ],
            ]);

            $statusCode          = (int) $response->getStatusCode();
            $responseBody        = json_decode($response->getBody()->getContents(), true);
            $reasonPhrase        = $response->getReasonPhrase();
            $infoBipMessageError = array_get($responseBody, 'requestError.serviceException.text');

            if ($statusCode >= 400 && $statusCode <= 499) {
                throw new \Exception("Error consumiendo servicio InfoBip. $reasonPhrase. $infoBipMessageError");
            }

            if ($statusCode >= 500) {
                throw new \Exception("Servicio InfoBip ha fallado. $reasonPhrase. $infoBipMessageError");
            }

        } catch (\Throwable $th) {
            throw new \Exception("Falló envío de mensaje de texto. ".$th->getMessage());
        }

        return true;
    }
}
