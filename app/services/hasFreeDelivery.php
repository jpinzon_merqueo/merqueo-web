<?php
namespace App\Services;

use User;

/**
 * Class hasFreeDelivery
 * @package App\Services
 */
class hasFreeDelivery
{
    /**
     * @var User
     */
    private $user;

    /**
     * @var float
     */
    private $orderAmount;

    /**
     * @var float
     */
    private $minimumDiscountAmount;

    /**
     * @param $deliveryWindow
     * @param $orderAmount
     * @param $minimumDiscountAmount
     * @return int
     */
    public function calculateDeliveryWindow($deliveryWindow, $minimumDiscountAmount, $orderAmount)
    {
        if ($this->hasFreeDeliveryMandatory()) {
            return 0;
        }
        $this->minimumDiscountAmount = $minimumDiscountAmount;
        $this->orderAmount = $orderAmount;
        return $this->getDeliveryWindowAmount($deliveryWindow);
    }

    /**
     * @param $deliveryWindow
     * @return float
     */
    private function getDeliveryWindowAmount($deliveryWindow)
    {
        $percentageValue = $deliveryWindow->percentage_discount_delivery_to_minimum_amount;
        $hasFreeDeliveryUser = $this->hasFreeDeliveryUser();

        if ($hasFreeDeliveryUser && !$deliveryWindow->disabled_free_delivery_minimum_amount && empty($percentageValue)) {
            $deliveryWindow->delivery_amount = 0;
            return $deliveryWindow->delivery_amount;
        }

        if ($hasFreeDeliveryUser && !empty($percentageValue) && $deliveryWindow->delivery_amount > 0) {
            $calculateDelivery = (float)number_format(
                $deliveryWindow->delivery_amount * ($percentageValue / 100),
                2,
                '.',
                ''
            );

            $deliveryWindow->delivery_amount = $deliveryWindow->delivery_amount - $calculateDelivery;
        }

        return $deliveryWindow->delivery_amount;
    }

    /**
     * @return bool
     */
    private function hasFreeDeliveryUser()
    {
        return $this->minimumDiscountAmount <= $this->orderAmount;
    }
    
    /**
     * @return bool
     */
    private function hasFreeDeliveryMandatory()
    {
        if ($this->user) {
            return $this->userHasFreeDelivery() || $this->hasFreeDeliveryByTotalOrders();
        }

        return false;
    }

    /**
     * @return bool
     */
    private function userHasFreeDelivery()
    {
        return $this->user->hasFreeDeliveryByExpirationDate() || $this->user->hasFreeDeliveryOnNextOrder();
    }

    /**
     * @return bool
     */
    private function hasFreeDeliveryByTotalOrders()
    {
        return $this->user->deliveredOrders() === 0 && $this->user->activeOrders() === 0;
    }

    /**
     * @param $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }
}
