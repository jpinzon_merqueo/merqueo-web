<?php
/**
 * Created by PhpStorm.
 * User: marlon
 * Date: 7/03/19
 * Time: 02:04 PM
 */

namespace usecases\cities;


use repositories\contracts\CityRepositoryInterface;
use usecases\contracts\cities\GetUsecaseInterface;

/**
 * Class GetUsecase
 * @package app\usecases\cities
 */
class GetUsecase implements GetUsecaseInterface
{
    /**
     * @var CityRepositoryInterface
     */
    private $cityRepository;

    /**
     * GetUsecase constructor.
     * @param CityRepositoryInterface $cityRepository
     */
    public function __construct(CityRepositoryInterface $cityRepository)
    {
        $this->cityRepository = $cityRepository;
    }

    /**
     * @return array
     */
    public function allActives()
    {
        return $this->cityRepository
            ->findAllMainActives()
            ->toArray();
    }

    /**
     * @return array
     */
    public function mainAndNeighborhoodsActives()
    {
        $cities = $this->cityRepository
            ->findAllActives();

        $data = [
            'main' => [],
            'neighborhoods' => []
        ];

        foreach ($cities as $city) {
            if (1 === $city->id || 2 === $city->id) {
                $data['main'][] = $city;
                continue;
            }

            $data['neighborhoods'][] = $city;
        }

        return $data;
    }
}