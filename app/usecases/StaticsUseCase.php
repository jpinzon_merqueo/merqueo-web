<?php

namespace usecases;

use repositories\CmsDocumentRepository;
use usecases\contracts\StaticsUseCaseInterface;

class StaticsUseCase implements StaticsUseCaseInterface
{
    protected $cmsDocumentRepository;

    public function __construct(CmsDocumentRepository $cmsDocumentRepository)
    {
        $this->cmsDocumentRepository = $cmsDocumentRepository;
    }

    public function getOne($slug)
    {
        return $this->cmsDocumentRepository->findOne($slug);
    }
}