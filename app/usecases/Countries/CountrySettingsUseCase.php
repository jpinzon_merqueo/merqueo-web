<?php
namespace usecases\Countries;

use Illuminate\Support\Facades\Config;
use usecases\contracts\Countries\CountrySettingsUseCaseInterface;
use repositories\contracts\CountryRepositoryInterface;

class CountrySettingsUseCase implements CountrySettingsUseCaseInterface
{
    /**
     * @var CountryRepositoryInterface
     */
    private $countryRepository;

    public function __construct(CountryRepositoryInterface $countryRepository)
    {
        $this->countryRepository = $countryRepository;
    }

    /**
     * Get the data urls associated with a country
     * @param int $storeId
     * @return array
     */
    public function getByStoreId($storeId)
    {
        $country = $this->countryRepository->findByStoreId($storeId);
        $referredCredit = $country->referred_credit;
        $referredByCredit = $country->referred_by_credit;
        $minimumOrder = $country->minimum_order_total_use_credit;

        $dataCountry = Config::get('merqueo.country.' . $country->country_code);
        $dataCountry['referred_credit'] = CountryCurrencyFormat($dataCountry['format_value'], $referredCredit);
        $dataCountry['referred_by_credit'] = CountryCurrencyFormat($dataCountry['format_value'], $referredByCredit);
        $dataCountry['minimum_order_total_use_credit'] = CountryCurrencyFormat($dataCountry['format_value'], $minimumOrder);
        $dataCountry['referred_credit_expiration_days'] = $country->referred_credit_expiration_days;
        $dataCountry['country_code'] = $country->country_code;
        $dataCountry['admin_email'] = $country->admin_email;
        $dataCountry['web_view'] = $country->domain;

        return $dataCountry;
    }

    /**
     * Get data country by phone code
     * @param string $phoneCode
     * @return array
     */
    public function getCountryByPhoneCode($phoneCode)
    {
        $country = $this->countryRepository->findCountryByPhoneCode($phoneCode);
        $arrUrl = Config::get('merqueo.country.' . $country->country_code);
        $arrUrl['country_code'] = $country->country_code;
        $arrUrl['contact_email'] = $country->admin_email;
        $arrUrl['web_view'] = $country->domain;
        $arrUrl['admin_phone'] = $country->admin_phone;
        
        return $arrUrl;
    }
}
