<?php
namespace usecases\Products\Interfaces;

/**
 * Interface GetSuggestedProductByProductUseCaseInterface
 * @package usecases\Products\Interfaces
 * @author Jeremy Reyes B. <jreyes@merqueo.com>
 */
interface GetIdsSuggestedProductByProductUseCaseInterface
{
    /**
     * @param $storeProductIds
     * @return int
     */
    public function handle($storeProductIds);
}