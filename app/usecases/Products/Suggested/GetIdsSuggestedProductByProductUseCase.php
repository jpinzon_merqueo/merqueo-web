<?php
namespace usecases\Products\Suggested;

use repositories\Interfaces\EloquentShelvesSuggestedProductRepositoryInterface;
use usecases\Products\Interfaces\GetIdsSuggestedProductByProductUseCaseInterface;

/**
 * Class GetSuggestedProductByProductUseCase
 * @package usecases\Products\Suggested
 * @author Jeremy Reyes B. <jreyes@merqueo.com>
 */
class GetIdsSuggestedProductByProductUseCase implements GetIdsSuggestedProductByProductUseCaseInterface
{
    /**
     * @var EloquentShelvesSuggestedProductRepositoryInterface
     */
    private $eloquentShelvesSuggestedProductRepository;

    /**
     * GetIdsSuggestedProductByProductUseCase constructor.
     * @param EloquentShelvesSuggestedProductRepositoryInterface $eloquentShelvesSuggestedProductRepository
     */
    public function __construct(
        EloquentShelvesSuggestedProductRepositoryInterface $eloquentShelvesSuggestedProductRepository
    ) {
        $this->eloquentShelvesSuggestedProductRepository = $eloquentShelvesSuggestedProductRepository;
    }

    /**
     * @param $storeProductIds
     * @return int|void
     */
    public function handle($storeProductIds)
    {
        if (!$storeProductIds) {
            return null;
        }

        if ($shelvesSuggestedProduct = $this->findActiveShelveByStoreIds($storeProductIds)) {
            return $shelvesSuggestedProduct->id;
        }

        return null;
    }

    /**
     * @param $storeProductIds
     * @return mixed
     */
    private function findActiveShelveByStoreIds($storeProductIds)
    {
        return $this->eloquentShelvesSuggestedProductRepository->findActiveByStoreIds($storeProductIds);
    }
}