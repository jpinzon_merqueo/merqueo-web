<?php
namespace usecases\Products\ValidatePrices;

use usecases\Products\ValidatePrices\Interfaces\ValidateRealPublicPriceUseCaseInterface;

/**
 * Class ValidateRealPublicPriceUseCase
 * @package usecases\Products\ValidatePrices
 * @author Jeremy Reyes B. <jreyes@merqueo.com>
 */
class ValidateRealPublicPriceUseCase implements ValidateRealPublicPriceUseCaseInterface
{
    /**
     * @param int $orders
     * @param float $price
     * @param float $publicPrice
     * @param float $specialPrice
     * @param float $firstOrderSpecialPrice
     * @return float
     */
    public function handler($orders, $price, $publicPrice, $specialPrice, $firstOrderSpecialPrice)
    {
        if ($this->showRealPublicPrice($price, $publicPrice, $specialPrice, $firstOrderSpecialPrice, $orders)) {
            return $publicPrice;
        }

        return 0.0;
    }

    /**
     * Condiciones para mostrar el precio de otros supermercados:
     *
     * No Adquisición, Tiene promoción primera compra
     * No Adquisición, No promoción primera compra y No promoción normal
     * Adquisición, No promoción primera compra y No promoción normal
     *
     * @param $price
     * @param $publicPrice
     * @param $specialPrice
     * @param $firstOrderSpecialPrice
     * @param $orders
     * @return bool
     */
    private function showRealPublicPrice($price, $publicPrice, $specialPrice, $firstOrderSpecialPrice, $orders)
    {
        if ($publicPrice <= $price) {
            return false;
        }

        if (count($orders)) {
            if ($firstOrderSpecialPrice) {
                return true;
            } elseif (empty($specialPrice)) {
                return true;
            }
        } else {
            return !$firstOrderSpecialPrice && empty($specialPrice);
        }

        return false;
    }
}