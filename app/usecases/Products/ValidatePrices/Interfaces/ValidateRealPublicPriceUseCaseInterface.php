<?php
namespace usecases\Products\ValidatePrices\Interfaces;

/**
 * Interface ValidateRealPublicPriceUseCaseInterface
 * @package usecases\Products\ValidatePrices\Interfaces
 * @author Jeremy Reyes B. <jreyes@merqueo.com>
 */
interface ValidateRealPublicPriceUseCaseInterface
{
    /**
     * @param $orders
     * @param $price
     * @param $publicPrice
     * @param $specialPrice
     * @param $firstOrderSpecialPrice
     * @return mixed
     */
    public function handler($orders, $price, $publicPrice, $specialPrice, $firstOrderSpecialPrice);
}