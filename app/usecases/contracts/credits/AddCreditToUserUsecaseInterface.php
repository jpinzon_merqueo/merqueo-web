<?php


namespace usecases\contracts\credits;


use app\objects\CreditDataObject;
use Credit;
use User;

interface AddCreditToUserUsecaseInterface
{

    public function handle(CreditDataObject $creditData);

}