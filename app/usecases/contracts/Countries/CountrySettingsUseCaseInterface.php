<?php
namespace usecases\contracts\Countries;

interface CountrySettingsUseCaseInterface
{
     /**
     * Get Country's settings by store_id
     * @param int $store_id
     * @return array
    */
    public function getByStoreId($storeId);

    /**
     * Get data country by phone code
     * @param string $phoneCode
     * @return array
     */
    public function getCountryByPhoneCode($phoneCode);
}
