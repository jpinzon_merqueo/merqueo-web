<?php

namespace usecases\contracts;

interface StaticsUseCaseInterface
{
    public function getOne($slug);
}