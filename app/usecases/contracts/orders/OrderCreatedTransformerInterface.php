<?php


namespace usecases\contracts\orders;


use Order;

interface OrderCreatedTransformerInterface
{

    /**
     * Method in charge to trigger SNS notifications 
     * after an order has been created
     * @param Order $order
     * @return bool
     */
    public function prepareData(Order $order);

}