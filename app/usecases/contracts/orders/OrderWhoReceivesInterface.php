<?php


namespace usecases\contracts\orders;

/**
 * Interface OrderWhoReceivesInterface
 * @package usecases\contracts\orders
 */
interface OrderWhoReceivesInterface
{
    /**
     * @param $orderId
     * @param $userId
     * @return array
     */
    public function getWhoReceives($orderId, $userId);

    /**
     * @param $orderId
     * @param $nameUserReceives
     * @param $phoneUserReceives
     * @return bool
     */
    public function updateWhoReceives($orderId, $nameUserReceives, $phoneUserReceives);

}