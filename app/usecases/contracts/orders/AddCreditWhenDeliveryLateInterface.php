<?php


namespace usecases\contracts\orders;


use Carbon\Carbon;
use UserCredit, Order;

interface AddCreditWhenDeliveryLateInterface
{

    /**
     * @param Order $order
     * @param Carbon $expire_date
     * @param string $type_notification
     * @return mixed
     */
    public function handle(Order $order, Carbon $expire_date, $type_notification = "user_credits");

    public function addCredit(Carbon $expire_date);

    public function sendNotifications(UserCredit $credit, array $data);

}