<?php


namespace usecases\contracts\orders;


use Order;

interface OrderCreatedNotificationInterface
{

    /**
     * Method in charge to trigger SNS notifications 
     * after an order has been created
     * @param Order $order
     * @return bool
     */
    public function handle(Order $order);

    public function sendSNSNotification($contract);

}