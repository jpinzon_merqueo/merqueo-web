<?php


namespace usecases\contracts\cities;


/**
 * Interface GetUsecaseInterface
 * @package app\usecases\contracts\cities
 */
interface GetUsecaseInterface
{

    /**
     * @return array
     */
    public function allActives();

    /**
     * La ciudad con identificador 1 y la ciudad con identificador 2, siempre seran las ciudades pricipales;
     * El modelo de datos actual no permite categorizar las ciudades. Este metodo se debe replantear. Se realiza
     * acorde al requerimiento.
     *
     * @return array
     */
    public function mainAndNeighborhoodsActives();

}