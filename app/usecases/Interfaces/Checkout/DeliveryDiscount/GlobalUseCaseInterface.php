<?php

namespace usecases\Interfaces\Checkout\DeliveryDiscount;

use repositories\contracts\DiscountRepositoryInterface;

/**
 * Interface GlobalUseCaseInterface
 * @package usecases\Interfaces\Checkout\DeliveryDiscount
 */
interface GlobalUseCaseInterface
{
    /**
     * GlobalDiscountDeliveryUsecaseInterface constructor.
     * @param DiscountRepositoryInterface $repository
     */
    public function __construct(DiscountRepositoryInterface $repository);

    /**
     * @return mixed
     */
    public function handle($cityId, $storeId, $cartSubtotal);
}