<?php

namespace usecases\Interfaces\Checkout\DeliveryDiscount;

use repositories\Interfaces\OrderRepositoryInterface;

/**
 * Interface DeliveryDiscountInterface
 * @package usecases\Interfaces
 * @author Jeremy Reyes B. <jreyes@merqueo.com>
 */
interface FirstOrderUseCaseInterface
{
    /**
     * FirstOrderUseCaseInterface constructor.
     * @param OrderRepositoryInterface $orderRepository
     */
    public function __construct(OrderRepositoryInterface $orderRepository);

    /**
     * @return mixed
     */
    public function handle($userId, $isDeviceWeb, $isNewDevice);
}