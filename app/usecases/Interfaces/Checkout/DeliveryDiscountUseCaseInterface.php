<?php

namespace usecases\Interfaces\Checkout;

/**
 * Interface DeliveryDiscountUseCaseInterface
 * @package usecases\Interfaces\Checkout
 * @author Jeremy Reyes B. <jreyes@merqueo.com>
 */
interface DeliveryDiscountUseCaseInterface
{
    /**
     * @param $userId
     * @param $isDeviceWeb
     * @param $isNewDevice
     * @param $cityId
     * @param $storeId
     * @param $cartSubtotal
     * @return mixed
     */
    public function handle($userId, $isDeviceWeb, $isNewDevice, $cityId, $storeId, $cartSubtotal);
}