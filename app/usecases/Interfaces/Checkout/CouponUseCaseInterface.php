<?php

namespace usecases\Interfaces\Checkout;

use Coupon;
use usecases\Interfaces\Checkout\Coupon\DiscountByValuesUseCaseInterface;

/**
 * Interface CouponUseCaseInterface
 * @package usecases\Interfaces\Checkout
 */
interface CouponUseCaseInterface
{
    /**
     * CouponUseCaseInterface constructor.
     * @param DiscountByValuesUseCaseInterface $discountByValuesUseCase
     */
    public function __construct(DiscountByValuesUseCaseInterface $discountByValuesUseCase);

    /**
     * @param Coupon $coupon
     * @return mixed
     */
    public function setCoupon(Coupon $coupon);

    /**
     * @param $subtotal
     * @return mixed
     */
    public function setCartSubtotal($subtotal);

    /**
     * @return mixed
     */
    public function handle();
}