<?php

namespace usecases\Interfaces\Checkout\Coupon;

use Coupon;

/**
 * Interface DiscountByValuesUseCaseInterface
 * @package usecases\Interfaces\Checkout\Coupon
 */
interface DiscountByValuesUseCaseInterface
{
    /**
     * @param Coupon $coupon
     * @return mixed
     */
    public function setCoupon(Coupon $coupon);

    /**
     * @param $subtotal
     * @return mixed
     */
    public function setCartSubtotal($subtotal);

    /**
     * @return mixed
     */
    public function handle();
}