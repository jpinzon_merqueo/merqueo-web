<?php


namespace usecases\Interfaces\Checkout\DeliveryQuick;
use Order;
use Zone;

/**
 * Interface RouteOrderUseCaseInterface
 * @package usecases\Interfaces\Checkout\DeliveryQuick
 */
interface RouteOrderUseCaseInterface
{
    /**
     * @param Order $order
     * @param Zone $zone
     * @return mixed
     */
    public function handler(Order $order, Zone $zone);
}