<?php
namespace usecases\Interfaces\orders;

interface CalculateDeliveryWindowAmountInterface
{
    /**
     * @param $order
     * @return mixed
     */
    public function handle($order);
}