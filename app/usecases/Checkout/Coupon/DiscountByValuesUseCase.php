<?php

namespace usecases\Checkout\Coupon;

use Coupon;
use usecases\Interfaces\Checkout\Coupon\DiscountByValuesUseCaseInterface;

class DiscountByValuesUseCase implements DiscountByValuesUseCaseInterface
{
    CONST MESSAGE         = 'Para redimir el cupón de descuento %s el total del pedido debe ';
    CONST MIN_MESSAGE     = 'ser menor o igual a $%s';
    CONST MAX_MESSAGE     = 'ser mayor o igual a $%s';
    CONST MIN_MAX_MESSAGE = 'estar entre $%s y $%s';

    /**
     * @var Coupon
     */
    private $coupon;

    /**
     * @var float
     */
    private $cartSubtotal;


    /**
     * @param Coupon $coupon
     * @return mixed|void
     */
    public function setCoupon(Coupon $coupon)
    {
        $this->coupon = $coupon;
    }

    /**
     * @param $cartSubtotal
     * @return mixed|void
     */
    public function setCartSubtotal($cartSubtotal)
    {
        $this->cartSubtotal = (float) $cartSubtotal;
    }

    /**
     * @return mixed
     */
    public function handle()
    {
        $methods = [
            $this->checkByMinimumOrderAmount(),
            $this->checkByMaximumOrderAmount(),
            $this->checkByMinAndMaxOrderAmount()
        ];

        foreach ($methods as $method) {

            if ($message = $method) {
                return $message;
            }
        }
    }

    /**
     * @return string
     */
    private function checkByMinimumOrderAmount()
    {
        if ($this->coupon->minimum_order_amount &&
            !$this->coupon->maximum_order_amount &&
            $this->cartSubtotal < $this->coupon->minimum_order_amount
        ) {
            return sprintf(
                self::MESSAGE . self::MIN_MESSAGE,
                $this->coupon->code,
                $this->formatMoney($this->coupon->minimum_order_amount)
            );
        }
    }

    /**
     * @return string
     */
    private function checkByMaximumOrderAmount()
    {
        if ($this->coupon->maximum_order_amount &&
            !$this->coupon->minimum_order_amount &&
            $this->cartSubtotal > $this->coupon->maximum_order_amount
        ) {
            return sprintf(
                self::MESSAGE . self::MAX_MESSAGE,
                $this->coupon->code,
                $this->formatMoney($this->coupon->maximum_order_amount)
            );
        }
    }

    /**
     * @return string
     */
    private function checkByMinAndMaxOrderAmount()
    {
        if ($this->coupon->minimum_order_amount &&
            $this->coupon->maximum_order_amount &&
            $this->cartSubtotal < $this->coupon->minimum_order_amount
        ) {
            return sprintf(
                self::MESSAGE . self::MIN_MAX_MESSAGE,
                $this->coupon->code,
                $this->formatMoney($this->coupon->minimum_order_amount),
                $this->formatMoney($this->coupon->maximum_order_amount)
            );
        }
    }

    /**
     * @param $value
     * @return string
     */
    private function formatMoney($value)
    {
        return number_format($value, 0, ',', '.');
    }
}