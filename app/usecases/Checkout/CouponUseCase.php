<?php

namespace usecases\Checkout;

use Coupon;
use usecases\Interfaces\Checkout\Coupon\DiscountByValuesUseCaseInterface;
use usecases\Interfaces\Checkout\CouponUseCaseInterface;

/**
 * Class CouponUseCase
 * @package usecases\Checkout
 */
class CouponUseCase implements CouponUseCaseInterface
{
    /**
     * @var Coupon
     */
    private $coupon;

    /**
     * @var float
     */
    private $cartSubtotal;

    /**
     * @var DiscountByValuesUseCaseInterface
     */
    private $discountByValuesUseCase;

    /**
     * CouponUseCase constructor.
     * @param DiscountByValuesUseCaseInterface $discountByValuesUseCase
     */
    public function __construct(DiscountByValuesUseCaseInterface $discountByValuesUseCase)
    {
        $this->discountByValuesUseCase = $discountByValuesUseCase;
    }

    /**
     * @param Coupon $coupon
     * @return mixed|void
     */
    public function setCoupon(Coupon $coupon)
    {
        $this->coupon = $coupon;
    }

    /**
     * @param $cartSubtotal
     * @return mixed|void
     */
    public function setCartSubtotal($cartSubtotal)
    {
        $this->cartSubtotal = (float) $cartSubtotal;
    }

    /**
     * @return mixed|void
     */
    public function handle()
    {
        return $this->discountByTotals();
    }

    /**
     * @return mixed|void
     */
    private function discountByTotals()
    {
        $this->discountByValuesUseCase->setCoupon($this->coupon);
        $this->discountByValuesUseCase->setCartSubtotal($this->cartSubtotal);

        return $this->discountByValuesUseCase->handle();
    }
}