<?php

namespace usecases\Checkout\DeliveryDiscount;

use Auth;
use repositories\Interfaces\OrderRepositoryInterface;
use usecases\Interfaces\Checkout\DeliveryDiscount\FirstOrderUseCaseInterface;

/**
 * Class FirstOrderUseCase
 * @package usecases\Checkout\DeliveryDiscount
 * @author Jeremy Reyes B. <jreyes@merqueo.com>
 */
class FirstOrderUseCase implements FirstOrderUseCaseInterface
{
    /**
     * @var bool
     */
    private $isNewDevice;

    /**
     * @var int
     */
    private $quantityOrders;

    /**
     * @var int
     */
    private $userId;

    /**
     * @var object
     */
    private $auth;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var bool
     */
    private $isDeviceWeb;

    /**
     * FirstOrderUseCase constructor.
     * @param OrderRepositoryInterface $orderRepository
     */
    public function __construct(OrderRepositoryInterface $orderRepository)
    {
        $this->auth = Auth::check();
        $this->orderRepository = $orderRepository;
    }

    /**
     * @return bool
     */
    public function handle($userId, $isDeviceWeb, $isNewDevice)
    {
        $this->userId      = $userId;
        $this->isDeviceWeb = $isDeviceWeb;
        $this->isNewDevice = $isNewDevice;

        $this->getQuantityOrdersByUser();

        return $this->verifyWhenUserIsNotLogged() || $this->verifyWhenUserIsLogged();
    }

    /**
     * Get quantity orders by user
     */
    private function getQuantityOrdersByUser()
    {
        if ($orders = $this->orderRepository->getQuantityOrderByUser($this->userId)) {
            $this->quantityOrders = $orders->quantity;
        }
    }

    /**
     * @return bool
     */
    private function verifyWhenUserIsNotLogged()
    {
        return !$this->userId && !$this->auth;
    }

    /**
     * @return bool
     */
    private function verifyWhenUserIsLogged()
    {
        return $this->isDeviceWeb ? $this->checkOnWeb() : $this->checkOnMovil();
    }

    /**
     * @return bool
     */
    private function checkOnWeb()
    {
        return (is_null($this->quantityOrders) || !$this->quantityOrders) && ($this->auth || $this->userId);
    }

    /**
     * @return bool
     */
    protected function checkOnMovil()
    {
        return (is_null($this->quantityOrders) || !$this->quantityOrders)
            && $this->isNewDevice
            && ($this->auth || $this->userId);
    }
}