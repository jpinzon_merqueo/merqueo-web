<?php

namespace usecases\Checkout\DeliveryDiscount;

use Carbon\Carbon;
use repositories\contracts\DiscountRepositoryInterface;
use usecases\Interfaces\Checkout\DeliveryDiscount\GlobalUseCaseInterface;

/**
 * Class GlobalDiscountDeliveryUsecase
 * @package usecases\discount
 */
class GlobalUseCase implements GlobalUseCaseInterface
{
    /**
     * @var DiscountRepositoryInterface
     */
    protected $discountRepository;

    /**
     * @var Carbon
     */
    protected $currentDatetime;

    /**
     * @var int
     */
    private $cityId;

    /**
     * @var int
     */
    private $storeId;

    /**
     * @var int
     */
    private $cartSubtotal;


    /**
     * GlobalDiscountDeliveryUsecase constructor.
     * @param DiscountRepositoryInterface $repository
     */
    public function __construct(DiscountRepositoryInterface $discountRepository)
    {
        $this->discountRepository   = $discountRepository;
        $this->currentDatetime      = Carbon::now()->format('Y-m-d H:i:s');
    }

    /**
     * @return bool|mixed
     */
    public function handle($cityId, $storeId, $cartSubtotal)
    {
        $this->cityId       = $cityId;
        $this->storeId      = $storeId;
        $this->cartSubtotal = $cartSubtotal;

        return $this->searchByCity() || $this->searchByStore();
    }

    /**
     * @return bool
     */
    private function searchByCity()
    {
        $byCity = $this->discountRepository->findByDateCitySubtotal($this->getParameterObjects());

        return $byCity && $byCity->count();
    }

    /**
     * @return bool
     */
    private function searchByStore()
    {
        $byCityStore = $this->discountRepository->findByDateCityStoreSubtotal($this->getParameterObjects());

        return $byCityStore && $byCityStore->count();
    }

    /**
     * @return object
     */
    private function getParameterObjects()
    {
        return (object) [
            'currentDateTime'   => $this->currentDatetime,
            'cityId'            => $this->cityId,
            'storeId'           => $this->storeId,
            'cartSubtotal'      => $this->cartSubtotal
        ];
    }
}