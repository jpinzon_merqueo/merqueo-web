<?php


namespace usecases\Checkout\DeliveryQuick;


use DeliveryWindow;
use Order;
use repositories\Interfaces\OrderRepositoryInterface;
use repositories\Interfaces\RoutesRepositoryInterface;
use usecases\Interfaces\Checkout\DeliveryQuick\RouteOrderUseCaseInterface;
use Zone;


/**
 * Class RouteOrderUseCase
 * @package usecases\Checkout\DeliveryQuick
 */
class RouteOrderUseCase implements RouteOrderUseCaseInterface
{
    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;
    /**
     * @var RoutesRepositoryInterface
     */
    private $routesRepository;

    public function __construct(OrderRepositoryInterface $orderRepository, RoutesRepositoryInterface $routesRepository)
    {
        $this->orderRepository = $orderRepository;
        $this->routesRepository = $routesRepository;
    }

    /**
     * @param Order $order
     * @param Zone $zone
     * @return mixed
     */
    public function handler(Order $order, Zone $zone)
    {
        $route = $this->routesRepository->getCurrentDeliveryQuickRoute(
            $order->orderGroup->warehouse,
            $zone,
            DeliveryWindow::SHIFT_FAST_DELIVERY);
        $this->orderRepository->updateStatusOrderExpressToRouted($order->id, $route->id);
    }

}