<?php

namespace usecases\Checkout;

use usecases\Checkout\DeliveryDiscount\FirstOrderUseCase;
use usecases\Interfaces\Checkout\DeliveryDiscount\GlobalUseCaseInterface;
use usecases\Interfaces\Checkout\DeliveryDiscountUseCaseInterface;

/**
 * Class DeliveryDiscountUseCase
 *
 * Este caso de uso se encarga de verificar los descuentos por domicilios.
 *
 * 1. Domicilio gratis cuando es la primera compra del usuario y el dipositivo no esta registrado.
 * 2. Domicilio gratis global cuando pasa del valor minimo de descuento.
 *
 * Nota: No estan todas las verificaciones, al tocar alguna agreguenla aquí para tener un mejor orden.
 *
 * @package usecases\Checkout
 * @author Jeremy Reyes B. <jreyes@merqueo.com>
 */
class DeliveryDiscountUseCase implements DeliveryDiscountUseCaseInterface
{
    /**
     * @var FirstOrderUseCase
     */
    private $firstOrderUseCase;

    /**
     * @var GlobalUseCaseInterface
     */
    private $globalUseCase;

    /**
     * DeliveryDiscountUseCase constructor.
     * @param FirstOrderUseCase $firstOrderUseCase
     * @param GlobalUseCaseInterface $globalUseCase
     */
    public function __construct(FirstOrderUseCase $firstOrderUseCase, GlobalUseCaseInterface $globalUseCase)
    {
        $this->firstOrderUseCase = $firstOrderUseCase;
        $this->globalUseCase     = $globalUseCase;
    }

    /**
     * @param $userId
     * @param $isDeviceWeb
     * @param $isNewDevice
     * @param $cityId
     * @param $storeId
     * @param $cartSubtotal
     * @return bool|mixed
     */
    public function handle($userId, $isDeviceWeb, $isNewDevice, $cityId, $storeId, $cartSubtotal)
    {
        return $this->firstOrderUseCase->handle($userId, $isDeviceWeb, $isNewDevice) ||
            $this->globalUseCase->handle($cityId, $storeId, (double) $cartSubtotal);
    }
}