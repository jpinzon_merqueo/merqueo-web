<?php


namespace usecases\credits;


use app\objects\contracts\ObjectInterface;
use app\objects\CreditDataObject;
use repositories\contracts\UserCreditRepositoryInterface;
use usecases\contracts\credits\AddCreditToUserUsecaseInterface;

class AddCreditToUserUsecase implements AddCreditToUserUsecaseInterface
{



    /**
     * @var UserCreditRepositoryInterface
     */
    private $userCreditRepository;

    public function __construct(UserCreditRepositoryInterface $userCreditRepository)
    {
        $this->userCreditRepository = $userCreditRepository;
    }

    public function handle(CreditDataObject $creditData)
    {
        return $this->userCreditRepository->add($creditData);
    }
}