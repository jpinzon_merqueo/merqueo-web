<?php
namespace usecases\orders;

use Order;
use OrderGroup;
use OrderPayment;
use Store;
use User;
use UserCreditCard;
use Carbon\Carbon;
use usecases\contracts\Countries\CountrySettingsUseCaseInterface;
use usecases\contracts\orders\OrderCreatedTransformerInterface;

class OrderCreatedTransformer implements OrderCreatedTransformerInterface
{

    /**
     * Order information
     * @var Order
     */
    protected $order;

    /**
     * Order information group
     * @var OrderGroup
     */
    protected $orderGroup;

    /**
     * @var CountrySettingsUseCaseInterface
     */
    private $countrySettingsUseCase;

    /**
     * Main constructor
     * @param CountrySettingsUseCaseInterface $countrySettingsUseCase
     */
    public function __construct(CountrySettingsUseCaseInterface $countrySettingsUseCase)
    {
        $this->countrySettingsUseCase = $countrySettingsUseCase;
    }

    /**
     * Method in charge to set contract data
     * @param Order $order
     * @return array
     */
    public function prepareData(Order $order)
    {
        $this->order = $order;
        $this->orderGroup = OrderGroup::find($order->group_id);

        $delivery_date = Carbon::parse($this->order->delivery_date);

        $data = [
            "country" => $this->_prepareCountry(),
            "address" => $this->_prepareAddress(),
            "store" => $this->_prepareStore(),
            "user" => $this->_prepareUser(),
            "business_line" => $order->type,
            "payment" => $this->_preparePayment(),
            "total_amount" => "{$this->order->total_amount}",
            "delivery_amount" => "{$this->order->delivery_amount}",
            "discount_amount" => "{$this->order->discount_amount}",
            "delivery_date" => $delivery_date->toRfc3339String(),
            "delivery_time" => "{$this->order->delivery_time}",
            "reference" => "{$this->order->reference}",
            "id" => $this->order->id,
            "warehouse_id" => $this->orderGroup->warehouse_id,
            "zone_id" => $this->orderGroup->zone_id,
            "ip" => "{$this->orderGroup->ip}",
            "coupon_id" => $this->orderGroup->coupon ?: null,
            "products" => $this->_prepareProducts()
        ];
        return $data;
    }

    /**
     * Return Order Country
     * @return array
     */
    private function _prepareCountry()
    {
        if (empty($this->order->store_id)) {
            return [];
        }

        $dataCountry = $this->countrySettingsUseCase->getByStoreId($this->order->store_id);

        if (empty($dataCountry['country_code'])) {
            return [];
        }

        return ['code' => $dataCountry['country_code']];
    }

    /**
     * Return order address array
     * @return array
     */
    private function _prepareAddress()
    {
        if (empty($this->orderGroup)) {
            return [];
        }

        return[
            'id' => $this->orderGroup->getAttribute('address_id'),
            'name' => $this->orderGroup->getAttribute('user_address'),
            'further' => $this->orderGroup->getAttribute('user_address_further'),
            'latitude' => $this->orderGroup->getAttribute('user_address_latitude'),
            'longitude' => $this->orderGroup->getAttribute('user_address_longitude'),
        ];
    }

    /**
     * Returns store minimum order amount
     * @return array
     */
    private function _prepareStore()
    {
        if (empty($this->order->store_id)) {
            return [];
        }

        // Retrieve store information from model
        $store = Store::find($this->order->store_id);

        if (empty($store->minimum_order_amount)) {
            return [];
        }
        return ["minimum_order_amount" => $store->minimum_order_amount];
    }

    /**
     * Returns order user data
     * @return array
     */
    private function _prepareUser()
    {
        if (empty($this->order->user_id)) {
            return [];
        }

        // Retrieve user information from model
        $user = User::find($this->order->user_id);

        if (empty($user)) {
            return [];
        }

        return [
            "id" => $user->getAttribute('id'),
            "first_name" => $user->getAttribute('first_name'),
            "last_name" => $user->getAttribute('last_name'),
            "email" => $user->getAttribute('email'),
            "referred_by" => $user->getAttribute('referred_by') ? intval($user->getAttribute('referred_by')) : null,
            "device_reg_id" => '', //TODO Add the right field from model
            "phone" => $user->getAttribute('phone'),
        ];
    }

    /**
     * Returns order payment information
     * @return array
     */
    private function _preparePayment()
    {
        if (empty($this->order)) {
            return [];
        }
        $payment_method = $this->order->getAttribute('payment_method');
        $payment = [
            "method" => $payment_method,
            "credit_card_id" => null,
            "credit_card_type" => "",
            "credit_card_bin" => "",
            "credit_card_exp_month" => null,
            "credit_card_exp_year" => null,
            "credit_card_last_four" => null,
            "franchise" => "",
            "paid" => is_null($this->order->payment_date) ? false : true,
        ];

        if ($payment_method == OrderPayment::CREDIT_CARD) {
            $credit_card = UserCreditCard::find($this->order->getAttribute('credit_card_id'));
            $payment['credit_card_id'] = $credit_card->getAttribute('id');
            $payment['credit_card_type'] = $credit_card->getAttribute('card_type');
            $payment['credit_card_bin'] = $credit_card->getAttribute('bin');
            $payment['credit_card_exp_month'] = $credit_card->getAttribute('expiration_month');
            $payment['credit_card_exp_year'] = $credit_card->getAttribute('expiration_year');
            $payment['credit_card_last_four'] = $credit_card->getAttribute('last_four');
            $payment['franchise'] = $credit_card->getAttribute('type');
        }

        return $payment;
    }

    /**
     * Returns order products
     * @return array
     */
    private function _prepareProducts()
    {
        if ($this->order->orderProducts->count() == 0) {
            return [];
        }

        $products = [];

        foreach ($this->order->orderProducts as $product) {
            $products[] = [
                'id' => $product->id,
                'name' => $product->product_name,
                'unit' => $product->product_unit,
                'reference' => $product->reference,
                'quantity' => $product->quantity,
                'price' => $product->price,
                'product_image_url' => $product->product_image_url,
                'shelf_id' => $product->storeProduct->shelf_id,
                'department_id' => $product->storeProduct->department_id,
                'store_product_id' => $product->store_product_id,
            ];
        }

        return $products;
    }
}
