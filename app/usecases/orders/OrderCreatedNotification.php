<?php


namespace usecases\orders;

use Order;
use usecases\contracts\orders\OrderCreatedNotificationInterface;
use usecases\contracts\orders\OrderCreatedTransformerInterface;
use contracts\SendSNSNotificationInterface;

class OrderCreatedNotification implements OrderCreatedNotificationInterface
{
    /**
     * @var SendSNSNotificationInterface
     */
    private $sendNotification;
    
        /**
     * @var OrderCreateOrderTransformeInterface
     */
    private $orderTransformer;
    
    /**
     * @var topicArn
     */
    private $topicArn = "queue_order_confirmed";


    /**
     * SendSNSNotification constructor.
     * @param OrderCreateOrderTransformeInterface $orderTransformer
     * @param SendNotificationInterface $sendNotification
     */
    public function __construct(
        OrderCreatedTransformerInterface $orderTransformer, 
        SendSNSNotificationInterface $sendNotification)
    {
        $this->orderTransformer =$orderTransformer;
        $this->sendNotification = $sendNotification;
    }

    /**
     * Method in charge to trigger SNS notifications 
     * after an order has been created
     * @param Order $order
     * @return bool
     */
    public function handle(Order $order)
    {
        $contract = $this->orderTransformer->prepareData($order);
        return $this->sendSNSNotification($contract);
    }


    /**
     * Method in charge of send notification to SNS
     * @param array $contract
     */
    public function sendSNSNotification($contract)
    {
        return $this->sendNotification->sendMessage($contract,$this->topicArn);
    }

}