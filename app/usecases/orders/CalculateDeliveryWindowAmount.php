<?php
namespace usecases\orders;

use App\Services\hasFreeDelivery;
use repositories\contracts\DiscountRepositoryInterface;
use repositories\contracts\EloquentDeliveryWindowInterface;
use repositories\contracts\EloquentStoreRepositoryInterface;
use repositories\contracts\OrderGroupRepositoryInterface;
use repositories\Interfaces\OrderRepositoryInterface;
use repositories\OrderGroupRepository;
use usecases\Interfaces\orders\CalculateDeliveryWindowAmountInterface;
use Order;

/**
 * Class CalculateDeliveryWindowAmount
 * @package usecases\orders
 */
class CalculateDeliveryWindowAmount implements CalculateDeliveryWindowAmountInterface
{
    /**
     * @var DiscountRepositoryInterface
     */
    private $eloquentDiscountRepository;

    /**
     * @var EloquentStoreRepositoryInterface
     */
    private $eloquentStoreRepository;

    /**
     * EloquentDeliveryWindowInterface
     */
    private $eloquentDeliveryWindow;

    /**
     * @var \Store
     */
    private $store;

    /**
     * @var \Discount
     */
    private $discount;

    /**
     * CalculateDeliveryWindowAmount constructor.
     * @param DiscountRepositoryInterface $eloquentDiscountRepository
     * @param EloquentStoreRepositoryInterface $eloquentStoreRepository
     * @param EloquentDeliveryWindowInterface $eloquentDeliveryWindow
     */
    public function __construct(
        DiscountRepositoryInterface $eloquentDiscountRepository,
        EloquentStoreRepositoryInterface $eloquentStoreRepository,
        EloquentDeliveryWindowInterface $eloquentDeliveryWindow
    ) {
        $this->eloquentDeliveryWindow = $eloquentDeliveryWindow;
        $this->eloquentStoreRepository = $eloquentStoreRepository;
        $this->eloquentDiscountRepository = $eloquentDiscountRepository;
    }

    /**
     * @var hasFreeDelivery
     */
    private $hasFreeDelivery;

    /**
     * @var Order
     */
    private $order;

    /**
     * @param $order
     * @return mixed|void
     */
    public function handle($order)
    {
        $this->order = $order;
        $this->hasFreeDelivery = new hasFreeDelivery();
        $this->initialiseHasFreeDelivery();
        $this->findCityByStore();
        $this->findDiscountByCity();
        return $this->calculateDeliveryWindowAmountTotal();
    }

    private function calculateDeliveryWindowAmountTotal()
    {
        $minimumDiscountAmount = $this->store->minimum_order_amount_free_delivery;
        $orderAmount = $this->order->orderGroup->total_amount;
        $deliveryWindow = $this->eloquentDeliveryWindow->findDeliveryWindow($this->order->delivery_window_id);
        $deliveryAmount = $this->hasFreeDelivery->calculateDeliveryWindow(
            $deliveryWindow,
            $minimumDiscountAmount,
            $orderAmount
        );

        return [
            'subtotal' => $this->order->orderGroup->total_amount,
            'delivery_amount' => $deliveryAmount,
            'discounts' =>  $this->order->orderGroup->discount_amount,
            'total' => $this->order->orderGroup->total_amount + $deliveryAmount - $this->order->orderGroup->discount_amount,
        ];
    }


    private function initialiseHasFreeDelivery()
    {
        $this->hasFreeDelivery->setUser($this->order->user);
    }

    /**
     * @return mixed
     */
    public function findCityByStore()
    {
        $this->store = $this->eloquentStoreRepository->findStore($this->order->store_id);
    }

    /**
     *
     */
    public function findDiscountByCity()
    {
        $this->discount = $this->eloquentDiscountRepository->findDiscountByCity($this->store->city_id);
    }
}
