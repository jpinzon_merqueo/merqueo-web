<?php


namespace usecases\orders;

use app\objects\CreditDataObject;
use Carbon\Carbon;
use contracts\SendNotificationInterface;
use Order;
use repositories\contracts\ConfigurationRepositoryInterface;
use usecases\contracts\credits\AddCreditToUserUsecaseInterface;
use usecases\contracts\orders\AddCreditWhenDeliveryLateInterface;
use UserCredit;

class AddCreditWhenDeliveryLate implements AddCreditWhenDeliveryLateInterface
{

    /**
     * @var Order
     */
    private $order;
    /**
     * @var ConfigurationRepositoryInterface
     */
    private $configurationRepository;
    /**
     * @var SendNotificationInterface
     */
    private $sendNotification;
    /**
     * @var string
     */
    public $type_notification;
    /**
     * @var integer
     */
    private $amount = 0;
    /**
     * @var AddCreditToUserUsecaseInterface
     */
    private $addCreditToUserUsecase;
    /**
     * @var CreditDataObject
     */
    private $creditDataObject;


    /**
     * AddCreditWhenDeliveryLate constructor.
     * @param ConfigurationRepositoryInterface $configurationRepository
     * @param SendNotificationInterface $sendNotification
     * @param CreditDataObject $creditDataObject
     * @param AddCreditToUserUsecaseInterface $addCreditToUserUsecase
     */
    public function __construct(ConfigurationRepositoryInterface $configurationRepository, SendNotificationInterface $sendNotification,  CreditDataObject $creditDataObject, AddCreditToUserUsecaseInterface $addCreditToUserUsecase)
    {
        $this->configurationRepository = $configurationRepository;
        $this->sendNotification = $sendNotification;
        $this->addCreditToUserUsecase = $addCreditToUserUsecase;
        $this->creditDataObject = $creditDataObject;
    }

    /**
     * Verify order if apply to credits for late delivered
     * @param Order $order
     * @param Carbon $expire_date
     * @param string $type_notification
     * @return bool
     */
    public function handle(Order $order, Carbon $expire_date, $type_notification = "user_credits")
    {
        $this->order = $order;
        $this->type_notification = $type_notification;

        if($this->isEnabledDistributeCredit() and $this->isOutWindow()){

            $credit = $this->addCredit($expire_date);
            $data =  $this->prepareData($credit, $this->order);
            $this->sendNotifications($credit, $data);
            return true;
        }
        return false;
    }

    /**
     * Method por prepare data to send notification
     * @param UserCredit $credit
     * @param $order
     * @return array
     */
    private function prepareData(UserCredit $credit, $order)
    {
        $data = [
            'message' => $credit->description,
            'order_id' => $order->id,
            'user_id' => $order->user->id,
            'type' => $this->type_notification
        ];
        return $data;
    }

    /**
     * Method for add credit to user
     * @param Carbon $expire_date
     * @return UserCredit
     */
    public function addCredit(Carbon $expire_date)
    {

        $this->amount = $this->configurationRepository->findByKey('amount_credit_late');

        $creditObj = $this->creditDataObject->create([
            'amount' => $this->amount,
            'expiration_date' => $expire_date,
            'object' => ['order_id' => $this->order->id],
            'user_id' => $this->order->user->id,
            'credit_id' => 1
        ]);

        return $this->addCreditToUserUsecase->handle($creditObj);
    }

    /**
     * Method for send notification to user
     * @param UserCredit $credit
     * @param array $data
     */
    public function sendNotifications(UserCredit $credit, array $data)
    {
        $this->sendNotification->push($this->order->orderGroup, $data);
        $mail = [
            'template_name' => 'emails.credit_when_delivery_late',
            'subject' => 'Compensación llegada tarde',
            'to' => [['email' => $this->order->user->email, 'name' => $this->order->user->first_name . ' ' . $this->order->user->last_name]],
            'vars' => [
                'amount' => currency_format($this->amount),
                'name' => $this->order->user->first_name
            ]
        ];
        $this->sendNotification->mail($mail);
        $this->sendNotification->sms(
            $credit->description,
            $this->order->orderGroup->user_phone
        );
    }

    /**
     * Method for check if enabled distribute credit
     * @return string
     */
    private function isEnabledDistributeCredit()
    {
        return $this->configurationRepository->findByKey('distribute_credit_late') and $this->order->isDelivered();
    }

    /**
     * @return bool
     */
    private function isOutWindow()
    {
        return $this->order->managementDateIsGreaterThanDeliveryDate() &&  ($this->order->diffBetweenDeliveryAndManagementDatesInMinutes() >= $this->configurationRepository->findByKey('max_time_credit_late'));
    }


}