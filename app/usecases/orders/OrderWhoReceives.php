<?php


namespace usecases\orders;

use usecases\contracts\orders\OrderWhoReceivesInterface;
use usecases\Exception\OrderNotFoundException;
use repositories\Interfaces\OrderRepositoryInterface;
use repositories\contracts\OrderGroupRepositoryInterface;

/**
 * Class OrderWhoReceives
 * @package usecases\orders
 */
class OrderWhoReceives implements OrderWhoReceivesInterface
{
    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;
    /**
     * @var OrderGroupRepositoryInterface
     */
    private $orderGroupRepository;

    public function __construct(
        OrderRepositoryInterface $orderRepository,
        OrderGroupRepositoryInterface $orderGroupRepository
    )
    {
        $this->orderRepository = $orderRepository;
        $this->orderGroupRepository = $orderGroupRepository;
    }

    /**
     * @param $orderId
     * @param $userId
     * @return array|mixed
     * @throws OrderNotFoundException
     */
    public function getWhoReceives($orderId, $userId)
    {
        $order = $this->orderRepository->findByIdUserAndOrder($orderId, $userId);
        if (empty($order)) {
            throw new OrderNotFoundException('Datos Inválidos');
        }
        // obtenemos el grupo de la Orden
        $orderGroup = $order->orderGroup;
        if (is_null($orderGroup->reception_user_name) && is_null($orderGroup->reception_user_phone)) {
            throw new OrderNotFoundException('No se encontró Información');
        }

        $ownerUserName = sprintf('%s %s', $orderGroup->user_firstname, $orderGroup->user_lastname);
        $receptionUserName = $orderGroup->reception_user_name;
        $receptionPhoneName = $orderGroup->reception_user_phone;

        return [
            "order_id" => (string)$orderId,
            'is_user_owner' => ($ownerUserName == $receptionUserName) ? true : false,
            'name_user_receives' => $receptionUserName,
            'phone_user_receives' => $receptionPhoneName
        ];
    }

    /**
     * @param $orderId
     * @param $nameUserReceives
     * @param $phoneUserReceives
     * @return bool
     */
    public function updateWhoReceives($orderId, $nameUserReceives, $phoneUserReceives)
    {
        $order = $this->orderRepository->findOrderbyId($orderId);
        $orderGroup = $order->orderGroup;
        return $this->orderGroupRepository->updateWhoReceives($orderGroup, $nameUserReceives, $phoneUserReceives);
    }
}