<?php

class InventoryCountingDetail extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'inventory_counting_details';

    public function storeProduct() {
      return $this->belongsTo('StoreProduct');
    }

    public function firstCountAdmin() {
      return $this->belongsTo('Admin');
    }

    public function secondCountAdmin() {
      return $this->belongsTo('Admin');
    }

    public function inventoryCounting(){
      return $this->belongsTo('InventoryCounting');
    }

    public function inventoryCountingDetailsCount() {
      return $this->hasMany('InventoryCountingDetailCount');
    }

}
