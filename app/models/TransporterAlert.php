<?php

class TransporterAlert extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'transporter_alerts';
    public static $reasons = [
    		'Salto de secuencia',
    		'Ultima entrega > 21 minutos',
    		'GPS apagado o genera error',
    		'Datos apagados',
    		'Vehículo fuera de zona',
    		'Cierre de 2 o más pedidos en menos de 15 minutos',
    		'Vehículo quieto por más de 20 minutos'
    	];
    public static $statuses = ['Pendiente', 'Resuelto'];

    public static function getReasons()
    {
    	return self::$reasons;
    }

    public static function getStatus()
    {
    	return  self::$statuses;
    }

    public function getPusherFormattedInfo()
    {
    	$alert = TransporterAlert::leftJoin('admin', 'transporter_alerts.admin_id', '=', 'admin.id')
    								->join('vehicles', 'transporter_alerts.vehicle_id', '=', 'vehicles.id')
    								->join('vehicle_drivers', 'vehicles.id', '=', 'vehicle_drivers.vehicle_id')
    								->join('drivers', 'vehicles_drivers.driver_id', '=', 'drivers.id')
    								->join('transporters', 'vehicle.transporter_id', '=', 'transporters.id')
                                    ->join('cities', 'transporters.city_id', '=', 'cities.id')
    								->select(
    									'transporter_alerts.id AS id',
    									'vehicles.plate',
    									'drivers.cellphone',
    									DB::raw('CONCAT(drivers.first_name, drivers.last_name) AS driver'),
    									'transporter_alerts.reason',
    									'transporter_alerts.status',
                                        'transporter_alerts.created_at',
    									'transporter_alerts.order_id',
                                        'cities.city',
                                        'cities.id AS city_id'
    								)
    								->where('transporter_alerts.id', $this->id)
    								->first()
    								->toArray();
        if(!is_null($alert['order_id']))
            $alert['order_url'] = route('adminOrderStorage.details', ['id' => $alert['order_id']]);
		$alert['created_at'] = date("d M Y g:i a",strtotime( $alert['created_at']));

		return $alert;
    }
}