<?php

class StoreProductRelated extends \Illuminate\Database\Eloquent\Model
{
	protected $table = 'store_product_related';

	/**
     * Valida la información contenida en el archivo de carga
     * 
     * @param array $array con los datos contenidos en el archivo
     * @param array $errors array que contiene los errores del procesamiento de los datos del excel
     * @return array con los registros validados para cargar en la base de datos y listado de errores

     */
	public static function validateArray( $array, $errors, $products_parent_ids )
	{
		$first_element = reset( $array );
	
		uasort($array, 'StoreProductRelated::sortByPriority');
		
		if( count( $array ) > 3 )
		{
			$cant_elements = count( $array ) - 3;

			$error_rows = [];
			
			for( $i = 0; $i < $cant_elements; $i++ )
			{
				$last_element = end( $array );
				$error_rows[] = "<li><b>Producto relacionado:</b> ".$last_element['name_product_related'].', <b>Código:</b> '.$last_element['store_product_related_id'].', <b>Excel Linea:</b> '.$last_element['excel_row'].'</li>';
				array_pop( $array );
			}
			
			$errors[] = '- El producto <b>'.$first_element['name_product'].'</b> no puede tener mas de tres productos asociados.<br />Los siguientes productos no fueron asociados: <br /><ul>'.implode( '', $error_rows ).'</ul>';
		}
		$i = 1;
		
		foreach ($array as $key => $value) {
			if( $array[$key]['store_product_related_id'] != $array[$key]['store_product_id'] ){
				$array[$key]['priority'] = $i;
				$i++;
			}else{
				$errors[] = '- El producto <b>'.$array[$key]['name_product'].'</b>  no puede ser asociado como hijo de si mismo. Excel linea '.$array[$key]['excel_row'];
				unset($array[$key]);
			}
		}
		
		return [ $array, $errors ];
	}

	/**
     * Función necesaria para la función de ordenamiento uasort  de PHP,permite ordenar los elementos de una matriz asociativa por uno de los atributos
     *
     * @param array $a posiciòn actual dentro de la matriz
     * @param array $b posiciòn siguiente dentro de la matriz
     * @return el cambio de poscisión del array actual dentro de la matriz
     */
	private static function sortByPriority( $a, $b )
	{
		if ($a['priority'] == $b['priority'])
		{
	        return 0;
	    }

		return ($a['priority'] < $b['priority']) ? -1 : 1;
	}

	/**
     * Permite la obtención del listado de productos que tienen elementos relacionados, para la vista principal del modulo
     *
     * @param text $search valor para filtrar los productos (opcional)
     * @param int $city entero que contiene el id de la cidad para poder filtrar los productos
     * @return el listado de productos por ciudad 
     */
	public static function getStoreProductRelatedList( $search = '', $warehouse_id = '' )
	{
		$list = self::select( 
				'products.name',
				'store_product_related.store_product_id',
				DB::Raw("( SELECT GROUP_CONCAT( p.name separator ', ')
					FROM store_products sp
					INNER JOIN store_product_related spr ON sp.id = spr.store_product_related_id
					INNER JOIN products p ON p.id = sp.product_id 
					INNER JOIN store_product_warehouses spw ON spw.store_product_id = spr.store_product_related_id
					WHERE spr.store_product_id = store_product_related.store_product_id
						AND spw.warehouse_id = ".$warehouse_id."
					ORDER BY spr.priority ) AS products_related"),
				'warehouses.warehouse', 'store_product_related.status'
			)
			->join( 'store_products', 'store_products.id', '=', 'store_product_related.store_product_id' )
			->join( 'products', 'products.id', '=', 'store_products.product_id' )
			->join( 'store_product_warehouses', 'store_product_warehouses.store_product_id', '=', 'store_product_related.store_product_id')
			->join( 'warehouses', 'warehouses.id', '=', 'store_product_warehouses.warehouse_id' )
			->where('warehouses.id', $warehouse_id)
			->groupBy( 'store_product_related.store_product_id', 'warehouses.warehouse');

		if( !empty( $search ) )
		{

			$list->Where( function( $query ) use ($search) {
				$query = $query->Where('products.name', 'LIKE', '%'.$search.'%')
					->orWhere( 'products.reference', $search );
				if( is_numeric( $search ) )
				{
					$query->orWhere( 'store_product_related.store_product_id', $search );
				}
			});
			
		}

		/*if( !empty( $city ) )
		{

			$list->Where( 'stores.city_id', $city );
		}*/

		return $list->get();
	}

	/**
     * Función para validar los datos antes de agregar una nueva relación
     *
     * @param array $data contiene los datos que deben ser insertados en la tabla
     * @return mensajes de error dependiendo de la validación
     */
	public static function validate( $data )
	{
		$related_products =  StoreProductRelated::select( '*' )
								->where( 'store_product_id', $data['store_product_id'] )
								->count();
						
		if( $related_products >= 3  )
		{
			return 'No es posible relacionar mas de 3 productos';
		}

		$related_products =  StoreProductRelated::select( '*' )
								->where( 'store_product_id', $data['store_product_id'] )
								->where( 'store_product_related_id', $data['store_product_related_id'] )
								->count();
						
		if( $related_products > 0 )
		{
			return 'No es posible relacionar dos veces el mismo producto';
		}

		$related_products =  StoreProductRelated::select( '*' )
								->where( 'store_product_id', $data['store_product_id'] )
								->where( 'priority', $data['priority'] )
								->count();
		
		if( $related_products > 0 )
		{
			return 'Ya existe un producto con la prioridad '.$data['priority'];
		}

	}

	/**
     * Función para actualizar la prioridad de los productos asociados
     *
     * @param array $data contiene los datos que producto al cual se le va a actualizar la prioridad
     * @return mensajes de error dependiendo del resultado
     */
	public static function updatePriority( $data )
	{

		try{
	            
            DB::beginTransaction();
        	
			$related_to_update = self::find( $data['id'] );
			$current_priority = $related_to_update->priority;
        	
        	switch ( $data['action'] )
        	{
				case 'down':
					if( $current_priority >= 1 && $current_priority < 3 )
					{
						
						$priority = $current_priority + 1;

						$actually_product = self::select( '*' )
												->where( 'store_product_id', $data['store_product_id'] )
												->where( 'priority', $priority )->first();

						self::where( 'id', $actually_product->id )
							->update( [ 'priority'=> $current_priority ] );

						self::where( 'id', $related_to_update->id )
							->update( [ 'priority'=> $priority ] );
					}

					break;

				case 'up':
					if( $current_priority > 1 && $current_priority <= 3 )
					{
						
						$priority = $current_priority - 1;

						$actually_product = self::select( '*' )
												->where( 'store_product_id', $data['store_product_id'] )
												->where( 'priority', $priority )->first();

						self::where( 'id', $actually_product->id )
							->update( [ 'priority'=> $current_priority ] );

						self::where( 'id', $related_to_update->id )
							->update( [ 'priority'=> $priority ] );
					}

					break;
			}
        	
            
            DB::commit();
            return Response::json( [ 'result' => true, 'msg' => 'Se ha actualizado el producto sin Errores' ] );
            
        }catch(\Exception $exception){

            DB::rollback();
            return Response::json( [ 'result' => false, 'msg' => 'Ocurrio un error al guardar el producto ::'.$exception->getMessage() ] );

        }

	}

	/**
     * Función para obtener el productos principal segun el store_product_id
     *
     * @param int $id contiene el store_product_id del producto principal
     * @return objeto con el resultado de la consulta
     */
	public static function getProduct( $id )
	{
		return StoreProductRelated::select( 'products.name', 'products.quantity', 'products.unit', 'products.reference', 'products.image_medium_url','store_product_related.store_product_id', 'stores.name as store_name', 'store_products.store_id', 'store_product_related.warehouse_id' )
        			->join( 'store_products', 'store_products.id', '=', 'store_product_related.store_product_id' )
					->join( 'products', 'products.id', '=', 'store_products.product_id' )
					->join( 'stores', 'stores.id', '=', 'store_products.store_id' )
					->join( 'store_product_warehouses', function($query){
						$query->on('store_products.id', '=', 'store_product_warehouses.store_product_id')
							->on('store_product_warehouses.warehouse_id', '=', 'store_product_related.warehouse_id');
					})
					->join( 'cities', 'stores.city_id', '=', 'cities.id' )
					->where( 'store_product_related.store_product_id', $id )
					->groupBy( 'store_product_related.store_product_id', 'store_products.store_id')
					->first();
	}

	/**
     * Función para obtener los productos asociados segun el store_product_id del producto principal
     *
     * @param int $id contiene el store_product_id del producto principal
     * @return objeto con el resultado de la consulta
     */
	public static function getRelatedProducts( $id )
	{
		return StoreProductRelated::select( 'store_product_related.id', 'products.name', 'products.reference', 'products.image_small_url','store_product_related.store_product_related_id', 'store_product_related.priority')
        			->join( 'store_products', 'store_products.id', '=', 'store_product_related.store_product_related_id' )
					->join( 'products', 'products.id', '=', 'store_products.product_id' )
					->join( 'stores', 'stores.id', '=', 'store_products.store_id' )
					->join( 'cities', 'stores.city_id', '=', 'cities.id' )
					->where( 'store_product_related.store_product_id', $id )
					->orderBy( 'store_product_related.priority' )
					->get();
	}

	public static function getStatus(){
		$obj = self::select('status')->get()->first();
		return $obj->status;
	}

	/**
     * Función para obtener los store_product_id almacenados
     *
     * @return array con los id almacenados
     */
	public static function getAllIdRelatedProducts($warehouse_id){
		$products = self::select('store_product_related.store_product_id')
						->join('store_products', 'store_products.id', '=', 'store_product_related.store_product_id')
						->join('store_product_warehouses', 'store_products.id', '=', 'store_product_warehouses.store_product_id' )
						->where('store_product_warehouses.warehouse_id', $warehouse_id)
						->get();
		$ids = array();
		if($products){
			foreach ($products as $product) {
				$ids[] = $product->store_product_id;
			}
		}

		return $ids;
	}
}
