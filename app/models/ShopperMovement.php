<?php

class ShopperMovement extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'shopper_movements';

    /**
     * Guardar movimiento de shopper
     *
     * @param object $shopper_movement Objeto movimiento
     * @param object $order Objeto pedido
     * @return object $movement Objeto movimiento
     */
     public static function saveMovement($movement, $order)
     {
        $order_group = OrderGroup::find($order->group_id);
        $order_stores = OrderStore::where('order_id', $order->id)->get();

        $order_total_amount = $order->total_amount + $order->delivery_amount - $order->discount_amount;
        $total_real_amount = 0;
        $movement->shopper_balance = 0;
        $movement->card_balance = 0;
        /*foreach ($order_stores as $order_store)
            $total_real_amount += $order_store->total_real_amount;*/

        foreach ($order_stores as $order_store)
        {
            $total_real_amount = $order_store->total_real_amount;
            //cliente efectivo, shopper efectivo
            if ($order->payment_method == 'Efectivo' && $order_store->shopper_payment_method == 'Efectivo'){
                if ($order_group->source == 'Callcenter' && $order->store_id == 26)
                    $movement->shopper_balance += $order->delivery_amount * -1;
                else $movement->shopper_balance = $total_real_amount - $order_total_amount;
                $movement->card_balance += 0;
            }
            //cliente efectivo, shopper tarjeta
            if ($order->payment_method == 'Efectivo' && $order_store->shopper_payment_method == 'Tarjeta merqueo'){
                if ($order_group->source == 'Callcenter' && $order->store_id == 26)
                    $movement->shopper_balance += ($total_real_amount + $order->delivery_amount) * -1;
                else $movement->shopper_balance = $order_total_amount * -1;
                $movement->card_balance += $total_real_amount * -1;
            }
            //cliente datafono o tarjeta, shopper efectivo
            //cliente datafono o tarjeta, shopper tarjeta
            if ($order->payment_method == 'Datáfono' || $order->payment_method == 'Tarjeta de crédito'){
               if ($order_store->shopper_payment_method == 'Efectivo'){
                   $movement->shopper_balance += $total_real_amount;
                   $movement->card_balance += 0;
               }
               if ($order_store->shopper_payment_method == 'Tarjeta merqueo'){
                    $movement->card_balance += $total_real_amount * -1;
                    $movement->shopper_balance += 0;
               }
            }
            //cliente efectivo, shopper efectivo y tarjeta
            //cliente datafono o tarjeta, shopper efectivo y tarjeta
            if ($order_store->shopper_payment_method == 'Efectivo y tarjeta merqueo'){
                if ($order->payment_method == 'Efectivo'){
                    if ($order_group->source == 'Callcenter' && $order->store_id == 26)
                        $movement->shopper_balance += ($total_real_amount + $order->delivery_amount - $order_store->shopper_cash_paid) * -1;
                    else $movement->shopper_balance = ($order_total_amount - $order_store->shopper_cash_paid) * -1;
                }else{
                    if ($order->payment_method == 'Efectivo y datáfono')
                        $movement->shopper_balance += $movement->user_cash_paid > $order_store->shopper_cash_paid ? ($movement->user_cash_paid - $order_store->shopper_cash_paid) * -1 : $order_store->shopper_cash_paid - $movement->user_cash_paid;
                    else $movement->shopper_balance += $order_store->shopper_cash_paid;
                }
                $movement->card_balance = $order_store->shopper_card_paid * -1;
            }
            //cliente efectivo, shopper no pago
            if ($order->payment_method == 'Efectivo' && $order_store->shopper_payment_method == 'No aplica'){
                if ($order_group->source == 'Callcenter' && $order->store_id == 26)
                    $movement->shopper_balance += ($total_real_amount + $order->delivery_amount) * -1;
                else $movement->shopper_balance = $order_total_amount * -1;
                $movement->card_balance += 0;
            }else{
                //shopper no pago
                if ($order_store->shopper_payment_method == 'No aplica'){
                    $movement->card_balance += 0;
                    $movement->shopper_balance += 0;
                }
            }
            //cliente efectivo y datafono, shopper efectivo
            if ($order->payment_method == 'Efectivo y datáfono' && $order_store->shopper_payment_method == 'Efectivo'){
                if ($order_group->source == 'Callcenter' && $order->store_id == 26)
                    $movement->shopper_balance += $movement->user_cash_paid > $total_real_amount ? ($total_real_amount + $order->delivery_amount - $movement->user_cash_paid) * -1 : $total_real_amount - $movement->user_cash_paid;
                else $movement->shopper_balance += $total_real_amount - $movement->user_cash_paid;
                $movement->card_balance += 0;
            }
            //cliente efectivo y datafono, shopper tarjeta merqueo
            if ($order->payment_method == 'Efectivo y datáfono' && $order_store->shopper_payment_method == 'Tarjeta merqueo'){
                $movement->shopper_balance += $movement->user_cash_paid * -1;
                $movement->card_balance += $total_real_amount * -1;
            }
        }
        //debug($movement);
        $movement->save();

        return $movement;
     }

}