<?php

/**
 * @property bool has_warning
 */
class Shelf extends \Illuminate\Database\Eloquent\Model
{
    /**
     * @type array
     */
    const CIGARETTES = [387, 390, 404, 413];

    /**
     * @type array
     */
    const MILK = [428, 457, 459, 670, 3, 286];

    /**
     * @var string
     */
    protected $table = 'shelves';

    /**
     * @var array
     */
    protected $hidden = ['created_at','updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function department()
    {
        return $this->belongsTo('Department');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function store()
    {
        return $this->belongsTo('Store', 'store_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function suggestedProducts()
    {
        return $this->belongsToMany('Product', 'shelves_suggested_products', 'shelf_id', 'product_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function products()
    {
        return $this->belongsToMany('Product', 'store_products', 'shelf_id', 'product_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function storeProducts()
    {
        return $this->hasMany('StoreProduct');
    }

    /**
     * Obtiene productos del pasillo
     *
     * @param int $warehouse_id ID de la bodega
     * @return Product
     * @deprecated
     */
    public function getProducts($warehouse_id)
    {
        return new \Illuminate\Support\Collection();
    }

    /**
     * Modificacion al guardar el evento
     */
    public static function boot()
    {
        parent::boot();

        static::updated(function ($shelf) {

            if ($shelf->isDirty())
            {
                $dirty_data = $shelf->getDirty();
                if (Config::get('app.aws.elasticsearch.is_enable') && isset($dirty_data['slug']) || isset($dirty_data['status']))
                {
                    $aws_elasticsearch = new AWSElasticsearch();
                    $aws_elasticsearch->update_products(array('shelf_id' => $shelf->id));
                }
            }
            
        });
    }
}
