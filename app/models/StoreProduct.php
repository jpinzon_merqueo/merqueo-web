<?php

use Carbon\Carbon;
use exceptions\MerqueoException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;
use Trade\StoreProductTags;
use Trade\Tags;

/**
 * @property double $special_price
 * @property int $quantity_special_price
 * @property int $first_order_special_price
 * @property int $quantity_special_price_stock
 * @property string $special_price_expiration_date
 * @property bool $has_quantity_special_price_stock
 * @property int $merqueo_discount
 * @property int $provider_discount
 * @property int $seller_discount
 * @property double $price
 * @property int $id
 * @property Collection|StoreProductWarehouse[] $storeProductWarehouses
 * @property Product $product
 * @property int $allied_store_id
 * @property Provider $provider
 * @property Store $store
 * @property double $delivery_discount_amount
 * @property StoreProductGroup[] $storeProductGroupChildren
 * @property StoreProductGroup[]|Collection $storeProductGroup
 * @property float|int $cost
 * @property float|int base_cost
 * @property Shelf shelf
 * @property mixed is_best_price
 * @property mixed store_id
 * @property mixed shelf_id
 * @property mixed department_id
 * @property StoreProduct[]|Collection storeProducts
 * @property float|int base_price
 * @property mixed iva
 */
class StoreProduct extends Model
{
    protected $table = 'store_products';
    protected $hidden = array('created_at','updated_at');
    protected $casts = [
        'quantity_special_price_stock' => 'int',
        'has_quantity_special_price_stock' => 'bool',
    ];

    /**
     * @var bool
     */
    public static $applyFivePercentDiscount = false;

    public function product()
	{
		return $this->belongsTo('Product', 'product_id');
	}

    public function store()
	{
		return $this->belongsTo('Store', 'store_id');
	}

    public function department()
    {
        return $this->belongsTo('Department', 'department_id');
    }

    public function shelf()
    {
        return $this->belongsTo('Shelf', 'shelf_id');
    }

    public function storeProductGroup()
    {
        return $this->hasMany('StoreProductGroup', 'store_product_id');
    }

    public function tag()
    {
        $currentDatetime = Carbon::now()->toDateTimeString();

        return $this->belongsToMany(
            Tags::class,
            'store_product_tags',
            'store_product_id',
            'tag_id'
        )
            ->select(
                'tags.title',
                'tags.description',
                'tags.button_text',
                'tags.tag_text',
                'tags.url_styles',
                'tags.updated_at AS last_updated'
            )
            ->where('tags.status', Tags::ENABLED_STATUS)
            ->whereRaw("'{$currentDatetime}' BETWEEN tags.start_date AND tags.expiration_date")
            ->orderBy('tags.expiration_date');
    }

    /**
     * @return HasMany
     */
    public function storeProductTags()
    {
        return $this->hasMany(StoreProductTags::class);
    }

    /**
     * @return BelongsToMany
     */
    public function storeProducts()
    {
        return $this->belongsToMany(
            'StoreProduct',
            'store_product_group',
            'store_product_id',
            'store_product_group_id'
        )
            ->withPivot('merqueo_discount', 'provider_discount', 'seller_discount', 'discount_amount');
    }

    /**
     * @return BelongsTo
     */
    public function provider()
    {
        return $this->belongsTo('Provider', 'provider_id');
    }

    /**
     * @return BelongsTo
     */
    public function alliedStore()
    {
        return $this->belongsTo('AlliedStore', 'allied_store_id');
    }

    public function storeProductGroupChildren()
    {
        return $this->hasMany('StoreProductGroup', 'store_product_group_id');
    }

    /**
     * @return HasMany
     */
    public function storeProductWarehouses()
    {
        return $this->hasMany('StoreProductWarehouse', 'store_product_id');
    }

    public function warehouseStorages()
    {
        return $this->hasMany('WarehouseStorage', 'store_product_id');
    }

    public function orderProducts()
    {
        return $this->hasMany('OrderProduct', 'store_product_id');
    }

    public function providerOrderDetails()
    {
        return $this->hasMany('ProviderOrderDetail', 'store_product_id');
    }

    /**
     * @param $query
     * @param Warehouse $warehouse
     * @return mixed
     */
    public function scopeInWarehouse($query, Warehouse $warehouse)
    {
        return $query->whereHas('storeProductWarehouses', function ($query) use ($warehouse) {
            $query->available()
                ->where('warehouse_id', $warehouse->id);
        });
    }

    /**
	* Actualizar costo promedio del producto
	*/
	public function updateCostAverage()
	{
		$product = ProviderOrderDetail::join('provider_orders', 'provider_order_details.provider_order_id', '=', 'provider_orders.id')
									->join('provider_order_receptions', 'provider_order_receptions.provider_order_id', '=', 'provider_orders.id')
									->join('provider_order_reception_details', function($join){
										$join->on('provider_order_reception_details.reception_id', '=', 'provider_order_receptions.id');
										$join->on('provider_order_details.store_product_id', '=', 'provider_order_reception_details.store_product_id');
									})
									->whereIn('provider_order_reception_details.status', ['Recibido', 'Parcialmente recibido'])
									->where('provider_order_reception_details.store_product_id', $this->id)
									->groupBy('provider_order_reception_details.store_product_id')
									->select(DB::raw('ROUND(AVG(provider_order_reception_details.cost)) AS average_cost'))
									->first();
		if ($product){
			$this->cost_average = $product->average_cost;
			$this->save();
		}
	}

    /**
	 * Funcion para obtener los productos por proveedor
     *
	 * @param  String $search   [description]
	 * @param  Int $provider_id [description]
	 * @param  Array $store_ids   [description]
	 * @return [type]              [description]
	 */
	public static function searchProductsByProvider($search, $provider_order, $store_ids)
	{
		$product = Product::join('store_products', 'products.id', '=', 'store_products.product_id')
                            ->join('store_product_warehouses', function ($q) use($provider_order)
                            {
                                $q->on('store_products.id', '=', 'store_product_warehouses.store_product_id')
                                    ->where('store_product_warehouses.warehouse_id', '=', $provider_order->warehouse_id);
                            })
							->where('store_products.provider_id', $provider_order->provider_id)
							->whereIn('store_products.store_id', $store_ids)
							->where('store_product_warehouses.discontinued', 0)
							->where(function ($query) use ($search) {
                                $query->where('products.reference', 'LIKE', '%'.$search.'%');
                                $query->orWhere('store_products.provider_plu', 'LIKE', '%'.$search.'%');
                                $query->orWhere('products.name', 'LIKE', '%'.$search.'%');
                            })
                            ->select(
                            	'products.image_small_url AS image_url',
                            	'products.id AS product_id',
                            	'store_products.provider_plu AS plu',
                            	'products.name AS product_name',
                            	'products.type',
                            	'products.reference',
                            	'store_products.id',
                            	'store_products.id AS store_product_id',
                            	'store_products.handle_expiration_date',
                            	DB::raw('CONCAT(store_products.provider_pack_type, " X ",store_products.provider_pack_quantity) AS pack_description')
                        	)
							->get();

		return $product;
	}

    /**
	* Actualiza stock de un producto
	*/
	public function update_stock_product($quantity)
	{
        $quantity_stock_before = $this->current_stock;
        if(Input::get('type') == 'Aumento de unidades')
            $this->current_stock = $quantity_stock_after = $quantity_stock_before + Input::get('quantity');
        else $this->current_stock = $quantity_stock_after = $quantity_stock_before - Input::get('quantity');
        $this->save();
	}

    /**
     * Obtener stock almacenado
     *
     * @param int $warehouse_id ID de bodega
     * @return int $storage_stock Stock almacenado
     */
    public function getStorageStock($warehouse_id)
    {
        return $this->warehouseStorages()->where('warehouse_id', $warehouse_id)->sum('warehouse_storages.quantity');
    }

    /**
     * Obtener stock actual
     *
     * @param int $warehouse_id ID de bodega
     * @return int $current_stock Stock actual
     */
    public function getCurrentStock($warehouse_id)
    {
        $current_stock = $this->getStorageStock($warehouse_id);
        if ($store_product_warehouse = $this->storeProductWarehouses()->where('warehouse_id', $warehouse_id)->first()){
            $current_stock = $current_stock +  $store_product_warehouse->picking_stock;
        }

        return $current_stock;
    }

    /**
     * Obtener stock comprometido de producto en bodega
     *
     * @param int $warehouse_id ID de la bodega
     * @return int $committed_stock Stock comprometido
     */
    public function getCommittedStock($warehouse_id)
    {
        $simple_committed_stock = Order::whereIn('orders.status',['Validation', 'Initiated', 'Enrutado', 'In Progress'])
                                ->where('order_products.fulfilment_status', 'Pending')
                                ->where('order_products.type', 'Product')
                                ->where('store_product_warehouses.warehouse_id', $warehouse_id)
                                ->where('order_groups.warehouse_id', $warehouse_id)
                                ->where('order_products.store_product_id', $this->id)
                                ->join('order_groups','orders.group_id', '=', 'order_groups.id')
                                ->join('order_products','order_products.order_id', '=', 'orders.id')
                                ->join('store_product_warehouses', 'store_product_warehouses.store_product_id', '=', 'order_products.store_product_id')
                                ->sum('order_products.quantity');

        $group_committed_stock = Order::whereIn('orders.status',['Validation', 'Initiated', 'Enrutado', 'In Progress'])
                                    ->where('order_product_group.fulfilment_status', 'Pending')
                                    ->where('order_products.type', 'Agrupado')
                                    ->where('store_product_warehouses.warehouse_id', $warehouse_id)
                                    ->where('order_groups.warehouse_id', $warehouse_id)
                                    ->where('order_product_group.store_product_id', $this->id)
                                    ->join('order_groups','orders.group_id', '=', 'order_groups.id')
                                    ->join('order_products','order_products.order_id', '=', 'orders.id')
                                    ->join('order_product_group','order_product_group.order_product_id', '=', 'order_products.id')
                                    ->join('store_product_warehouses', 'store_product_warehouses.store_product_id', '=', 'order_product_group.store_product_id')
                                    ->sum('order_product_group.quantity');

       return $simple_committed_stock + $group_committed_stock;
    }

    /**
     * Obtiene la cantidad recomendada para pedir según parámetros de inventario
     *
     * @param $warehouse_id
     * @return mixed
     */
    public function recommendedQuantityToRequest($warehouse_id)
    {
        $current_stock_tmp = $current_stock = $this->getCurrentStock($warehouse_id);
        $committed_stock = $this->getCommittedStock($warehouse_id);
        $on_route_stock = $this->getOnRouteStockProviderOrders($warehouse_id);
        $quantity_to_request = 0;

        if (in_array($this->product->type, ['Agrupado', 'Proveedor'])) {
            $response['current_stock'] = 0;
            $response['quantity_to_request'] = 1;
            $response['on_route_stock'] = 0;
            $response['committed_stock'] = 0;
            $response['quantity_to_package_request'] = 1;
            return $response;
        }

        if ($current_stock <= 0) {
            $current_stock = 0;
        }

        $current_stock = $current_stock + $on_route_stock - $committed_stock;

        $store_product_warehouse = $this->storeProductWarehouses()->where('warehouse_id', $warehouse_id)->first();
        $ideal_stock = $store_product_warehouse->ideal_stock;
        $minimum_stock = $store_product_warehouse->minimum_stock;
        if ($current_stock < $ideal_stock) {
            $quantity_to_request = $ideal_stock - $current_stock;
        }
        $quantity_to_package_request = 1;
        if (in_array($this->provider_pack_type, ['Caja', 'Estiba'])) {
            $quantity_to_package_request = $this->provider_pack_quantity ? ceil($quantity_to_request / $this->provider_pack_quantity) : 1;
            $quantity_to_request = $quantity_to_package_request * $this->provider_pack_quantity;
        }

        $response['current_stock'] = $current_stock_tmp;
        $response['quantity_to_request'] = $quantity_to_request;
        $response['on_route_stock'] = $on_route_stock;
        $response['committed_stock'] = $committed_stock;
        $response['quantity_to_package_request'] = $quantity_to_package_request;
        return $response;
    }

    /**
     * Calcular las cantidades a pedir, si es surtido, se hace el cálculo.
     *
     * @param int $quantity_unit_to_request
     * @return mixed
     */
    public function calculateQuantityToOrder($quantity_unit_to_request = 0)
    {
        $quantity_to_request = $quantity_unit_to_request;
        $quantity_to_package_request = 1;
        if (in_array($this->provider_pack_type, ['Caja', 'Estiba'])) {
            $quantity_to_package_request = $this->provider_pack_quantity ? ceil($quantity_to_request / $this->provider_pack_quantity) : 1;
            $quantity_to_request = $quantity_to_package_request * $this->provider_pack_quantity;
        }
        $response['quantity_to_request'] = $quantity_to_request;
        $response['quantity_to_package_request'] = $quantity_to_package_request;
        return $response;
    }

    /**
     * Obtener stock solictado de producto en bodega
     *
     * @param int $warehouse_id ID de la bodega
     * @return int $on_route_stock Stock solicitado
     */
    public function getOnRouteStock($warehouse_id)
    {
        //obtener stock solicitado como producto simple
        $simple_on_route_stock = ProviderOrderDetail::join('provider_orders', 'provider_order_details.provider_order_id', '=', 'provider_orders.id')
            ->join('provider_order_receptions', 'provider_orders.id', '=', 'provider_order_receptions.provider_order_id')
            ->join('provider_order_reception_details', 'provider_order_receptions.id', '=', 'provider_order_reception_details.reception_id')
            ->where('provider_order_receptions.status', 'Recibido con factura')
            ->where('provider_order_details.store_product_id', $this->id)
            ->where('provider_order_reception_details.store_product_id', $this->id)
            ->where('provider_order_reception_details.status', 'No recibido')
            ->where('provider_orders.id', '<>', 17)
            ->where('provider_orders.status', 'Enviada')
            ->where('provider_orders.warehouse_id', $warehouse_id)
            ->whereRaw('DATE(provider_orders.delivery_date) = "'.date('Y-m-d').'"')
            ->groupBy('provider_orders.id')
            ->select('provider_order_reception_details.quantity_expected AS on_route_stock')
            ->pluck('on_route_stock');
        $on_route_stock = $simple_on_route_stock ? $simple_on_route_stock : 0;

        //obtener stock solicitado como producto agrupado
        $group_on_route_stock = ProviderOrderDetailProductGroup::join('provider_order_details', 'provider_order_detail_product_group.provider_order_detail_id', '=', 'provider_order_details.id')
            ->join('provider_orders', 'provider_order_details.provider_order_id', '=', 'provider_orders.id')
            ->join('provider_order_receptions', 'provider_orders.id', '=', 'provider_order_receptions.provider_order_id')
            ->join('provider_order_reception_details', 'provider_order_receptions.id', '=', 'provider_order_reception_details.reception_id')
            ->join('provider_order_reception_detail_product_group', 'provider_order_reception_details.id', '=', 'provider_order_reception_detail_product_group.reception_detail_id')
            ->where('provider_order_receptions.status', 'Recibido con factura')
            ->where('provider_order_reception_detail_product_group.store_product_id', $this->id)
            ->where('provider_order_reception_detail_product_group.status', 'No recibido')
            ->where('provider_orders.id', '<>', 17)
            ->where('provider_orders.status', 'Enviada')
            ->where('provider_orders.warehouse_id', $warehouse_id)
            ->whereRaw('DATE(provider_orders.delivery_date) = "'.date('Y-m-d').'"')
            ->select('provider_order_reception_detail_product_group.quantity_expected AS on_route_stock')
            ->pluck('on_route_stock');

        $on_route_stock += $group_on_route_stock ? $group_on_route_stock : 0;

        return $on_route_stock;
    }

    /**
     * Anula la promocion en caso de que no
     * existan mas unidades en stock.
     *
     * @param int $quantity
     * @return bool
     */
    public function validateStockDiscount($quantity)
    {
        if (empty($this->quantity_special_price_stock) || !$this->has_quantity_special_price_stock) {
            return false;
        }
        $this->quantity_special_price_stock -= $quantity;
        if ($this->special_price > 0 && $this->quantity_special_price_stock <= 0) {
            $this->special_price = null;
            $this->quantity_special_price = 0;
            $this->first_order_special_price = 0;
            $this->special_price_expiration_date = '';
            // Evitar cantidades negativas en un campo entero 'unsigned'
            $this->quantity_special_price_stock = 0;
        } else if ($this->quantity_special_price_stock < $this->quantity_special_price) {
            $this->quantity_special_price = $this->quantity_special_price_stock;
        }

        $this->save();

        return true;
    }


    /**
     * Anula la promocion en caso de que no
     * existan mas unidades en stock.
     *
     * @param int $quantity
     * @return bool
     */
    public function storeProductsValidateStockDiscount($orderId)
    {
        $orderProducts = OrderProduct::with(['storeProduct'])->where('order_id', $orderId)->get();
        foreach ($orderProducts as $orderProduct) {
            $storeProduct = $orderProduct->storeProduct;
            if (empty($storeProduct->quantity_special_price_stock) || !$storeProduct->has_quantity_special_price_stock) {
                continue;
            }

            if ($orderProduct->price == $orderProduct->original_price) {
                continue;
            }

            $storeProduct->quantity_special_price_stock -= ($orderProduct->quantity >= $storeProduct->quantity_special_price)
                ? $storeProduct->quantity_special_price
                : $orderProduct->quantity
            ;

            if ($storeProduct->special_price > 0 && $storeProduct->quantity_special_price_stock <= 0) {
                $storeProduct->special_price = null;
                $storeProduct->quantity_special_price = 0;
                $storeProduct->first_order_special_price = 0;
                $storeProduct->special_price_expiration_date = '';
                // Evitar cantidades negativas en un campo entero 'unsigned'
                $storeProduct->quantity_special_price_stock = 0;
            } else {
                if ($storeProduct->quantity_special_price_stock < $storeProduct->quantity_special_price) {
                    $storeProduct->quantity_special_price = $storeProduct->quantity_special_price_stock;
                }
            }

            $storeProduct->save();
        }

        return true;
    }

    /**
     * Actualiza el precio de los productos agrupados que cuentan
     * con el producto actual.
     *
     * @throws Exception
     */
    public function updateAncestorsPrice()
    {
        if ($this->product->type !== 'Simple') {
            throw new MerqueoException('Los productos simples pueden actualizar el precio de los combos.');
        }

        $difference = $this->price - $this->getOriginal('price');
        $this->load('storeProductGroupChildren.storeProduct');
        foreach ($this->storeProductGroupChildren as $child) {
            if ($child->discount_amount > $this->price) {
                throw new MerqueoException(
                    "El descuento del producto '{$child->storeProductGroup->product->name}' en " .
                    "el combo '{$child->storeProduct->product->name}' de la " .
                    "tienda '{$child->storeProduct->store->name}' no puede ser mayor al precio unitario"
                );
            }
            
            $child->price   = $this->price;
            $child->save();
            $child->storeProduct
                ->calculatePriceOnGroup($difference)
                ->save();
        }
    }

    /**
     * Calcula el precio de un producto agrupado.
     *
     * @param int $difference is the value to be added to the combo price, it can
     * be both positive and negative, depending on the adjustment made to the individual
     * product price
     *
     * @return StoreProduct
     * @throws Exception
     */
    public function calculatePriceOnGroup($difference = 0)
    {
        if ($this->product->type === 'Simple') {
            throw new MerqueoException('Solo en los productos "agrupados" o "proveedor" se calcula el precio.');
        }

        $total = 0;
        $total_cost = 0;
        $total_discount = 0;
        $this->load('storeProductGroup.storeProductGroup');

        foreach ($this->storeProductGroup as $store_product_group) {
            $total += $store_product_group->storeProductGroup->price * $store_product_group->quantity;
            $total_cost += $store_product_group->storeProductGroup->cost * $store_product_group->quantity;
            $total_discount += $store_product_group->discount_amount;
        }

        $this->price = $total;
        $this->cost = $total_cost;
        $this->base_cost = round($this->cost / (1 + $this->iva / 100));

        if (empty($this->special_price)) {
            foreach ($this->storeProductGroup as $store_product_group) {
                $store_product_group->discount_amount = null;
                $store_product_group->save();
            }

            return $this;
        }

        $this->special_price += $difference;

        return $this;
    }

    /**
     * Recalcula los valores para base_price y base_cost
     * 
     */
    public function recalculateBasePriceAndCost() {
        $this->base_price = $this->price / (1 + $this->iva / 100);
        $this->base_cost = $this->cost / (1 + $this->iva / 100);
    }

    /**
     * Reemplaza el precio del producto
     *
     * @param $query
     * @return mixed
     */
    public function scopeOnlyPublicPrice($query)
    {
        return $query->select(self::getRawPublicPriceQuery());
    }

    /**
     * Obtiene pum de producto
     *
     * @param $query
     * @return mixed
     */
    public function scopePum($query)
    {
        return $query->addSelect(
            DB::raw(
                "IF(products.type = 'Simple',".
                  "IF(products.net_quantity_pum <> 0 AND products.unit_pum IS NOT NULL AND store_products.provider_id <> 0 AND store_products.price <> 0,".
                    "CONCAT(products.unit_pum, ' a $', FORMAT( IF((SELECT pum_special_price) IS NOT NULL, (SELECT pum_special_price), store_products.price) / products.net_quantity_pum, 2, 'de_DE')),".
                    " '' ".
                  "),".
                  "IF(COUNT(DISTINCT child.id) = 1,".
		          "  GROUP_CONCAT( IF(child.price <> 0 AND child_products.net_quantity_pum <> 0 AND child_products.unit_pum IS NOT NULL AND child.provider_id <> 0, CONCAT(child_products.name, ': ', child_products.unit_pum,' a $', FORMAT( ( ( `store_products`.`price` / IF( (child_products.net_quantity_pum * store_product_group.quantity) > 0, (child_products.net_quantity_pum * store_product_group.quantity), child_products.net_quantity_pum) ) * IF( store_products.special_price IS NOT NULL, (store_products.special_price / store_products.price),1) ), 2, 'de_DE')), NULL) SEPARATOR '|'),".
		          "  IF(COUNT(DISTINCT child.id) >= 2 AND COUNT(DISTINCT child.id) <= 4, ".
			      "  GROUP_CONCAT(DISTINCT IF(child.price <> 0 AND child_products.net_quantity_pum <> 0 AND child_products.unit_pum IS NOT NULL AND child.provider_id <> 0, CONCAT(child_products.name, ': ', child_products.unit_pum,' a $', FORMAT( ( (child.price / child_products.net_quantity_pum) * IF( store_products.special_price IS NOT NULL, (store_products.special_price / store_products.price) ,1) ), 2, 'de_DE')), NULL) SEPARATOR '|'),".
			      "  '' ".
		          "         ) ".
	              "      ) ".
                  " ) AS pum, ".
                  "IF (products.type = 'Simple', products.volume, SUM(DISTINCT child_products.volume * store_product_group.quantity)) AS volume, ".
                  "IF (products.type = 'Simple', products.weight, SUM(DISTINCT child_products.weight * store_product_group.quantity)) AS weight "
            )
        )->leftJoin('store_product_group', 'store_product_group.store_product_id', '=', 'store_products.id')
            ->leftJoin('store_products AS child', 'store_product_group.store_product_group_id', '=', 'child.id')
            ->leftJoin('products AS child_products', 'child.product_id', '=', 'child_products.id')
            ->groupBy('store_products.id');
    }

    /**
     * Obtiene la forma en la cual se carga el campo precio
     * de un producto.
     *
     * @return mixed
     */
    public static function getRawPublicPriceQuery()
    {
        $condition = self::getRawConditionByDateRange();

        return DB::raw(
            "IF(store_products.special_price IS NOT NULL AND store_products.public_price > 0 AND {$condition}, " .
                'store_products.public_price, ' .
                'store_products.price) AS price'
        );
    }

    /**
     * Obtiene los campos para select de precios de producto en promocion
     *
     * @param int $totalOrders
     * @return mixed
     */
    public static function getRawSpecialPriceQuery($totalOrders)
    {
        $condition = self::getRawConditionByDateRange();
        $applyDiscount = app(SpecialPriceService::class)
            ->shouldApplyFiveSpecialDiscount();

        if ($applyDiscount) {
            $shelves = implode(',', Discount::skippedShelves());
            $promoCondition = "store_products.shelf_id NOT IN ($shelves) AND (store_products.special_price IS NULL OR ($condition) = FALSE)";

            return DB::raw("IF($promoCondition, CAST(ROUND(store_products.price * 0.95) AS SIGNED), store_products.special_price) AS special_price," .
            "IF($promoCondition, 5, CAST(ROUND((store_products.price - store_products.special_price) * 100 / store_products.price, 0) AS SIGNED)) AS discount_percentage," .
            "IF($promoCondition, 2, CAST(store_products.quantity_special_price AS SIGNED)) AS quantity_special_price," .
            "IF($promoCondition, CAST(ROUND(store_products.price * 0.95) AS SIGNED), store_products.special_price) AS pum_special_price");
        }

        return DB::raw(
            $totalOrders > 0
                ? "IF($condition AND store_products.first_order_special_price = 0 AND store_products.special_price IS NOT NULL, store_products.special_price, NULL) AS special_price,
                   IF($condition AND store_products.first_order_special_price = 0 AND store_products.special_price IS NOT NULL, 
                        CAST(ROUND((store_products.price - store_products.special_price) * 100 / store_products.price, 0) AS SIGNED), 
                        NULL) 
                   AS discount_percentage,
                   store_products.quantity_special_price,
                   IF($condition AND store_products.first_order_special_price = 0 AND store_products.special_price IS NOT NULL, store_products.special_price, NULL) AS pum_special_price"
                : "IF($condition AND store_products.special_price IS NOT NULL, store_products.special_price, NULL) AS special_price,
                   IF($condition AND store_products.special_price IS NOT NULL, 
                        CAST(ROUND((store_products.price - store_products.special_price) * 100 / store_products.price, 0) AS SIGNED), 
                        NULL) 
                   AS discount_percentage,
                   store_products.quantity_special_price,
                   IF($condition AND store_products.special_price IS NOT NULL, store_products.special_price, NULL) AS pum_special_price"
        );
    }

    /**
     * @return string
     */
    public static function getRawConditionByDateRange()
    {
        $now = Carbon::create()->format('Y-m-d H:i:s');

        return "'{$now}' BETWEEN " .
            "IF(store_products.special_price_starting_date IS NOT NULL, store_products.special_price_starting_date, '$now') " .
            "AND IF(store_products.special_price_expiration_date IS NOT NULL, store_products.special_price_expiration_date, '$now')";
    }

    /**
     * Aplica Delivery discount si la fecha actual es
     * mayor o igual a la fecha de inicio del descuentol.
     */
    public static function getRawDeliveryDiscountByDate()
    {
        $date = date("Y-m-d H:i:s");
        return DB::raw(
            "IF(
                store_products.delivery_discount_start_date <= '$date', 
                store_products.delivery_discount_amount, 
                0
            ) as delivery_discount_amount"
        );
    }

    /**
     * Obtener stock solictado de producto en bodega
     *
     * @param int $warehouse_id ID de la bodega
     * @return int $on_route_stock Stock solicitado
     */
    public function getOnRouteStockProviderOrders($warehouse_id)
    {
        //obtener stock solicitado como producto simple
        $simple_on_route_stock = ProviderOrderDetail::join('provider_orders', 'provider_order_details.provider_order_id', '=', 'provider_orders.id')
            ->where('provider_order_details.store_product_id', $this->id)
            ->where('provider_orders.provider_id', '<>', 17)
            ->where('provider_orders.status', 'Enviada')
            ->where('provider_orders.warehouse_id', $warehouse_id)
            ->groupBy('provider_order_details.store_product_id')
            ->select(DB::raw('SUM(provider_order_details.quantity_order) AS on_route_stock'))
            ->pluck('on_route_stock');
        $on_route_stock = $simple_on_route_stock ? $simple_on_route_stock : 0;

        //obtener stock solicitado como producto agrupado
        $group_on_route_stock = ProviderOrderDetailProductGroup::join('provider_order_details', 'provider_order_detail_product_group.provider_order_detail_id', '=', 'provider_order_details.id')
            ->join('provider_orders', 'provider_order_details.provider_order_id', '=', 'provider_orders.id')
            ->where('provider_order_detail_product_group.store_product_id', $this->id)
            ->where('provider_orders.provider_id', '<>', 17)
            ->where('provider_orders.status', 'Enviada')
            ->where('provider_orders.warehouse_id', $warehouse_id)
            ->groupBy('provider_order_detail_product_group.store_product_id')
            ->select(DB::raw('SUM(provider_order_detail_product_group.quantity_order) AS on_route_stock'))
            ->pluck('on_route_stock');

        $on_route_stock += $group_on_route_stock ? $group_on_route_stock : 0;

        return $on_route_stock;
    }

    /**
     * Muestra el precio especial teniendo en cuenta las siguientes condiciones:
     * - Fechas de vencimiento de la promoción validas.
     * - La promoción solo es valida para primera compra.
     *
     * @param $totalOrders
     * @return int|null
     */
    public function realSpecialPrice($totalOrders)
    {
        $validSpecialPrice = $this->isValidSpecialPrice($totalOrders);
        $canShowNewDiscount = $this->shouldApplyFivePercentDiscount();

        if (!$validSpecialPrice && !$canShowNewDiscount) {
            return null;
        }

        if ($validSpecialPrice) {
            return $this->special_price;
        }

        return $this->getSpecialPriceDiscount();
    }

    /**
     * @return bool
     */
    private function shouldApplyFivePercentDiscount()
    {
        return self::$applyFivePercentDiscount;
    }

    /**
     * @return float
     */
    private function getSpecialPriceDiscount()
    {
        return round($this->price * 0.95);
    }

    /**
     * @param $totalOrders
     * @return int
     */
    public function getQuantitySpecialPrice($totalOrders)
    {
        if ($this->isValidSpecialPrice($totalOrders)) {
            return $this->quantity_special_price;
        }

        if ($this->shouldApplyFivePercentDiscount()) {
            return 2;
        }

        return $this->quantity_special_price;
    }

    /**
     * Determina cual es el porcentaje de descuento teniendo en cuenta si
     * la promoción existente es valida.
     *
     * @param $hasOrders
     * @return float|int|null
     */
    public function getPercentageDiscount($hasOrders)
    {
        $specialPrice = $this->realSpecialPrice($hasOrders);

        if (empty($specialPrice)) {
            return null;
        }

        $public_price = $this->getPublicPrice($hasOrders);

        return round(($public_price - $specialPrice) * 100 / $public_price);
    }

    /**
     * Obtiene el precio publico del producto, esto solo aplica
     * si el producto tiene promoción y cuenta con el valor
     * "public_price".
     * ESTO YA NO VA, SE CAMBIÓ EL PROPÓSITO DEL CAMPO public_price
     *
     * @param $hasOrders
     * @return int|mixed
     */
    public function getPublicPrice($hasOrders)
    {
        return $this->price;
    }

    /**
     * @param $totalOrders
     * @return bool
     */
    private function isValidSpecialPrice($totalOrders)
    {
        return $this->isValidSpecialPriceRanges() && (!$this->first_order_special_price || $totalOrders == 0);
    }

    /**
     * @return int|null
     */
    private function isValidSpecialPriceRanges()
    {
        $now = Carbon::create();
        $startTime = null;
        $endTime = null;

        if (!empty($this->special_price_starting_date)) {
            $startTime = new Carbon($this->special_price_starting_date);
        }

        if (!empty($this->special_price_expiration_date)) {
            $endTime = new Carbon($this->special_price_expiration_date);
        }

        if (!empty($startTime) && !empty($endTime)) {
            return $now->gte($startTime) && $now->lt($endTime);
        }

        if (!empty($startTime)) {
            return $now->gte($startTime);
        }

        if (!empty($endTime)) {
            return $now->lt($endTime);
        }

        return false;
    }
}
