<?php

class ShrinkageOriginLog extends \Illuminate\Database\Eloquent\Model {
    protected $table = 'shrinkage_origin_log';
    protected $hidden = array('created_at','updated_at');
}