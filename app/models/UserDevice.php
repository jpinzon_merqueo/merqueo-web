<?php

use \Illuminate\Database\Eloquent\Model;

/**
 * Class UserDevice
 * @property string $os
 * @property string regid
 * @property int $totalOrders
 * @property User $user
 */
class UserDevice extends Model
{
    /**
     * @var string
     */
    protected $table = 'user_devices';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orderGroup()
    {
        return $this->hasMany('OrderGroup', 'user_device_id');
    }

    /**
     * @return int
     */
    public function getTotalOrdersAttributes()
    {
        return $this->orderGroup()->count();
    }
}
