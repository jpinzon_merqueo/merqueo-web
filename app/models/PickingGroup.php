<?php

use Carbon\Carbon;

class PickingGroup extends \Illuminate\Database\Eloquent\Model
{
    const EXPRESS_COLOR = '#F3FE00';
    protected $table = 'picking_group';

    /**
     * Obtiene siguiente pedido a alistar.
     *
     * @param object $picker Objeto picker
     * @param int $picking_group_id Id del grupo de alistamiento picker
     * @param boolean $get_enlisted_orders Obtener pedidos alistados
     * @param int $warehouse_id ID de la bodega
     * @return array
     */
    public static function getNextOrder($picker, $picking_group_id, $get_enlisted_orders = true, $warehouse_id)
    {
        //$date = '2017-08-17';
        $date = date('Y-m-d');
        $picking_orders = array();

        //verificar si tiene un pedido pendiente asignado
        $order = Order::select('orders.id', 'orders.status', 'orders.store_id', 'orders.delivery_time', 'orders.route_id', 'routes.route', 'orders.planning_sequence', 'color',
            'picking_group', 'picking_group_id', 'order_groups.user_comments AS user_comments', 'orders.delivery_date', 'orders.picking_baskets', 'orders.picking_bags', 'picker_dry_id', 'picker_cold_id',
            DB::raw("IF(order_groups.source IN ('Web', 'Device'), (SELECT COUNT(o.id) FROM orders o WHERE o.user_id = users.id AND o.status = 'Delivered' GROUP BY o.user_id), 0) AS qty_orders"))
            ->join('stores', 'orders.store_id', '=', 'stores.id')
            ->join('users', 'orders.user_id', '=', 'users.id')
            ->join('order_groups', 'order_groups.id', '=', 'orders.group_id')
            ->join('routes', 'route_id', '=', 'routes.id')
            ->join('picking_group', 'picking_group_id', '=', 'picking_group.id')
            ->leftJoin('order_picking_quality', 'order_picking_quality.order_id', '=', 'orders.id')
            ->where('orders.delivery_date', '>', date('Y-m-d 00:00:00', strtotime($date)))
            ->where('orders.delivery_date', '<', date('Y-m-d 23:59:59', strtotime($date . ' +1 days')))
            ->where('routes.warehouse_id', $warehouse_id)
            ->where('routes.picking_group_id', $picking_group_id)
            ->where('routes.city_id', $picker->city_id)
            ->whereNull('order_picking_quality.id')
            ->where(function ($query) {
                $query->where('orders.status', '=', 'Enrutado')
                    ->orWhere('orders.status', '=', 'In Progress');
            });
        if ($picker->profile == 'Alistador Seco') {
            $order->where('orders.picker_dry_id', $picker->id);
            $order->whereNotNull('orders.picking_dry_start_date');
            $order->whereNull('orders.picking_dry_end_date');
        } else {
            $order->where('orders.picker_cold_id', $picker->id);
            $order->whereNotNull('orders.picking_cold_start_date');
            $order->whereNull('orders.picking_cold_end_date');
        }

        if ($order = $order->first())
            $picking_orders[] = $order->toArray();
        else {
            //sino obtener siguiente pedido a alistar del grupo
            $orders = Order::select('orders.id', 'orders.status', 'orders.store_id', 'orders.delivery_time', 'orders.route_id', 'routes.route', 'orders.planning_sequence', 'color',
                'picking_group', 'picking_group_id', 'order_groups.user_comments AS user_comments', 'orders.delivery_date', 'orders.picking_baskets', 'orders.picking_bags', 'picker_dry_id', 'picker_cold_id',
                DB::raw("IF(order_groups.source IN ('Web', 'Device'), (SELECT COUNT(o.id) FROM orders o WHERE o.user_id = users.id AND o.status = 'Delivered' GROUP BY o.user_id), 0) AS qty_orders"))
                ->join('stores', 'orders.store_id', '=', 'stores.id')
                ->join('users', 'orders.user_id', '=', 'users.id')
                ->join('order_groups', 'order_groups.id', '=', 'orders.group_id')
                ->join('routes', 'route_id', '=', 'routes.id')
                ->join('zones', 'routes.zone_id', '=', 'zones.id')
                ->join('picking_group', 'picking_group_id', '=', 'picking_group.id')
                ->leftJoin('order_picking_quality', 'order_picking_quality.order_id', '=', 'orders.id')
                ->where('orders.delivery_date', '>', date('Y-m-d 00:00:00', strtotime($date)))
                ->where('orders.delivery_date', '<', date('Y-m-d 23:59:59', strtotime($date . ' +1 days')))
                ->where('routes.warehouse_id', $warehouse_id)
                ->where('routes.picking_group_id', $picking_group_id)
                ->whereNull('order_picking_quality.id')
                ->where('routes.city_id', $picker->city_id);
            if ($picker->profile == 'Alistador Seco')
                $orders->whereNull('orders.picker_dry_id');
            else $orders->whereNull('orders.picker_cold_id');

            $orders = $orders->where(function ($query) {
                $query->where('orders.status', '=', 'Enrutado')
                    ->orWhere('orders.status', '=', 'In Progress');
            })
                ->orderBy('routes.shift')
                ->orderBy('routes.picking_priority')
                ->orderBy('orders.planning_sequence', 'ASC')
                ->get();

            if ($orders) {
                //obtener primer pedido que tenga productos dependiendo del perfil
                $order_has_valid_product = false;
                foreach ($orders as $order) {
                    $products = OrderProduct::select('order_products.*', 'store_products.storage', 'store_product_warehouses.shrinkage_stock', 'store_product_warehouses.picking_stock', 'store_product_warehouses.picked_stock')
                        ->join('store_products', 'order_products.store_product_id', '=', 'store_products.id')
                        ->join('store_product_warehouses', 'store_product_warehouses.store_product_id', '=', 'store_products.id')
                        ->where('order_products.fulfilment_status', 'Pending')
                        ->where('order_products.type', "!=", 'Recogida')
                        ->where('store_product_warehouses.warehouse_id', $warehouse_id)
                        ->where('order_id', $order->id)->get();

                    foreach ($products as $product) {
                        if ($product->type == 'Agrupado') {
                            $product_group = OrderProductGroup::select('order_products.*', 'order_product_group.*', 'store_product_warehouses.shrinkage_stock', 'store_product_warehouses.picking_stock', 'store_product_warehouses.picked_stock')
                                ->join('order_products', 'order_products.id', '=', 'order_product_group.order_product_id')
                                ->join('store_products', 'order_product_group.store_product_id', '=', 'store_products.id')
                                ->join('store_product_warehouses', 'store_product_warehouses.store_product_id', '=', 'store_products.id')
                                ->where('order_product_id', $product->id)
                                ->where('store_product_warehouses.warehouse_id', $warehouse_id)
                                ->where('order_product_group.fulfilment_status', 'Pending');

                            if ($picker->profile == 'Alistador Seco')
                                $product_group->whereIn('store_products.storage', ['Seco', '']);
                            else $product_group->whereIn('store_products.storage', ['Refrigerado', 'Congelado']);
                            if ($product_group = $product_group->first()) {
                                $order_has_valid_product = $order->toArray();
                                break;
                            }
                        } else {
                            if ($picker->profile == 'Alistador Seco' && in_array($product->storage, ['Seco', ''])) {
                                $order_has_valid_product = $order->toArray();
                                break;
                            }
                            if ($picker->profile == 'Alistador Frío' && in_array($product->storage, ['Refrigerado', 'Congelado'])) {
                                $order_has_valid_product = $order->toArray();
                                break;
                            }
                        }
                    }
                    if ($order_has_valid_product) {
                        $picking_orders[] = $order_has_valid_product;
                        break;
                    }
                }
            }
        }

        if ($get_enlisted_orders) {
            //obtener pedidos alistados
            $orders = Order::select('orders.id', 'orders.status', 'orders.store_id', 'orders.delivery_time', 'orders.route_id', 'routes.route', 'orders.planning_sequence', 'color',
                'picking_group', 'picking_group_id', 'order_groups.user_comments AS user_comments', 'orders.delivery_date', 'orders.picking_baskets', 'orders.picking_bags', 'picker_dry_id', 'picker_cold_id',
                DB::raw("IF(order_groups.source IN ('Web', 'Device'), (SELECT COUNT(o.id) FROM orders o WHERE o.user_id = users.id AND o.status = 'Delivered' GROUP BY o.user_id), 0) AS qty_orders"))
                ->join('stores', 'orders.store_id', '=', 'stores.id')
                ->join('users', 'orders.user_id', '=', 'users.id')
                ->join('order_groups', 'order_groups.id', '=', 'orders.group_id')
                ->join('routes', 'route_id', '=', 'routes.id')
                ->join('zones', 'routes.zone_id', '=', 'zones.id')
                ->join('picking_group', 'picking_group_id', '=', 'picking_group.id')
                ->where('orders.delivery_date', '>', date('Y-m-d 00:00:00', strtotime($date)))
                ->where('orders.delivery_date', '<', date('Y-m-d 23:59:59', strtotime($date . ' +1 days')))
                ->where('routes.warehouse_id', $warehouse_id)
                ->where('routes.picking_group_id', $picking_group_id)
                ->where('routes.city_id', $picker->city_id);

            if ($picker->profile == 'Alistador Seco') {
                $orders->where('orders.picker_dry_id', $picker->id);
                $orders->whereNotNull('orders.picking_dry_end_date');
            } else {
                $orders->where('orders.picker_cold_id', $picker->id);
                $orders->whereNotNull('orders.picking_cold_end_date');
            }

            $orders = $orders->where(function ($query) {
                $query->where('orders.status', '=', 'In Progress')
                    ->orWhere('orders.status', '=', 'Alistado');
            })
                ->orderBy('routes.shift')
                ->orderBy('routes.picking_priority')
                ->orderBy('orders.planning_sequence', 'ASC')
                ->get();
            if (count($orders)) {
                foreach ($orders as $order) {
                    array_push($picking_orders, $order->toArray());
                }
            }
        }

        //debug($picking_orders);

        foreach ($picking_orders as $i => $order) {
            //OBTENER PRODUCTOS DEL PEDIDO
            $order_products = [];
            $products = OrderProduct::select('order_products.*')
                ->where('order_id', $order['id'])
                ->where('type', '<>', 'Recogida')
                ->get();
            $products_info = [];
            foreach ($products as $product) {
                //obtener ids de los productos del producto agrupado
                if ($product->type == 'Agrupado') {
                    $product_group = OrderProductGroup::select('order_products.*', 'order_product_group.*')
                        ->join('order_products', 'order_products.id', '=', 'order_product_group.order_product_id')
                        ->where('order_product_id', $product->id)
                        ->get();
                    if ($product_group) {
                        foreach ($product_group as $product_group_item) {
                            if (!array_key_exists($product_group_item->store_product_id, $products_info)) {
                                $products_info[$product_group_item->store_product_id]['product_group'][$product_group_item->id] = ['product' => $product_group_item->toArray(), 'quantity_group' => $product_group_item->quantity, 'order_product_group_id' => $product_group_item->id];
                                $products_info[$product_group_item->store_product_id]['product'] = $product_group_item->toArray();
                            } else {
                                $products_info[$product_group_item->store_product_id]['product_group'][$product_group_item->id]['product'] = $product_group_item->toArray();
                                $products_info[$product_group_item->store_product_id]['product_group'][$product_group_item->id]['quantity_group'] = isset($products_info[$product_group_item->store_product_id][$product_group_item->id]['quantity_group']) ? $products_info[$product_group_item->store_product_id][$product_group_item->id]['quantity_group'] + $product_group_item->quantity : $product_group_item->quantity;
                                $products_info[$product_group_item->store_product_id]['product_group'][$product_group_item->id]['order_product_group_id'] = $product_group_item->id;
                            }
                        }
                    }
                } elseif ($product->type == 'Muestra') {
                    if (!array_key_exists($product->store_product_id, $products_info)) {
                        $products_info[$product->store_product_id]['muestra'][$product->id] = ['product' => $product->toArray(), 'quantity' => $product->quantity, 'order_product_id' => $product->id];
                        //$products_info[$product->store_product_id] = ['product' => $product->toArray(), 'quantity' => $product->quantity, 'order_product_id' => $product->id];
                    } else {
                        $products_info[$product->store_product_id]['muestra'][$product->id] = ['product' => $product->toArray(), 'quantity' => $product->quantity, 'order_product_id' => $product->id];
                    }
                } else {

                    if (!array_key_exists($product->store_product_id, $products_info))
                        $products_info[$product->store_product_id] = ['product' => $product->toArray(), 'quantity' => $product->quantity, 'order_product_id' => $product->id];
                    else {
                        $products_info[$product->store_product_id]['quantity'] = isset($products_info[$product->store_product_id]['quantity']) ? $products_info[$product->store_product_id]['quantity'] + $product->quantity : $product->quantity;
                        $products_info[$product->store_product_id]['order_product_id'] = $product->id;
                    }
                }
            }
            //debug($products_info);
            $store_products_ids = array_keys($products_info);

            //obtener productos del pedido
            $products = Product::select('store_products.id AS store_product_id', 'reference', 'description', 'image_large_url',
                #DB::raw(" SUM(warehouse_storages.quantity) AS current_stock "),
                'store_product_warehouses.reception_stock', 'store_product_warehouses.return_stock', 'storage', 'store_product_warehouses.shrinkage_stock', 'store_product_warehouses.picking_stock', 'store_product_warehouses.picked_stock',
                DB::raw("CONCAT(storage_position, '-', storage_height_position) AS storage_position_product"))
                ->join('store_products', 'store_products.product_id', '=', 'products.id')
                ->join('store_product_warehouses', 'store_product_warehouses.store_product_id', '=', 'store_products.id')
                #->join('warehouse_storages', 'warehouse_storages.store_product_id', '=', 'store_products.id')
                ->where('store_product_warehouses.warehouse_id', $warehouse_id)
                ->whereIn('store_products.id', $store_products_ids);

            if ($picker->profile == 'Alistador Seco')
                $products->orderBy(DB::raw("FIELD(storage, 'Seco', 'Congelado', 'Refrigerado')"));
            else $products->orderBy(DB::raw("FIELD(storage, 'Refrigerado', 'Congelado', 'Seco')"));

            $products = $products->orderBy('storage_position')
                ->orderBy('storage_height_position')
                ->orderBy('department_id', 'desc')
                ->orderBy('name', 'asc')
                ->groupBy('store_products.id')
                ->get();
            $valid_products = 0;
            foreach ($products as $product) {
                $product->profile = $picker->profile;
                if (($picker->profile == 'Alistador Seco' && in_array($product->storage, ['Seco', '']))
                    || ($picker->profile == 'Alistador Frío' && in_array($product->storage, ['Refrigerado', 'Congelado'])))
                    $valid_products++;

                if (isset($products_info[$product->store_product_id]['product']))
                    $product_merge = array_merge($products_info[$product->store_product_id]['product'], $product->toArray());
                if (isset($products_info[$product->store_product_id]['muestra']))
                    foreach ($products_info[$product->store_product_id]['muestra'] as $order_product_id => $product_sample)
                        $products_info[$product->store_product_id]['muestra'][$order_product_id]['product'] = array_merge($product_sample['product'], $product->toArray());
                if (isset($products_info[$product->store_product_id]['product_group']))
                    foreach ($products_info[$product->store_product_id]['product_group'] as $order_product_group_id => $product_group)
                        $products_info[$product->store_product_id]['product_group'][$order_product_group_id]['product'] = array_merge($product_group['product'], $product->toArray());

                //setear cantidades
                if (array_key_exists($product->store_product_id, $products_info)) {
                    //producto simple
                    if (isset($products_info[$product->store_product_id]['quantity'])) {
                        $product_merge['id'] = $products_info[$product->store_product_id]['order_product_id'];
                        $product_merge['quantity'] = $products_info[$product->store_product_id]['quantity'];
                        $product_merge['type'] = 'Product';
                        $order_products[] = $product_merge;
                    }
                    //pedido agrupado
                    if (isset($products_info[$product->store_product_id]['product_group'])) {
                        foreach ($products_info[$product->store_product_id]['product_group'] as $order_product_group_id => $product_group) {
                            $product_merge = $product_group['product'];
                            $product_merge['id'] = $order_product_group_id;
                            $product_merge['quantity'] = $product_group['quantity_group'];
                            $product_merge['type'] = 'Agrupado';
                            $order_products[] = $product_merge;
                        }
                    }
                    //producto muestra
                    if (isset($products_info[$product->store_product_id]['muestra'])) {
                        foreach ($products_info[$product->store_product_id]['muestra'] as $order_product_id => $product_sample) {
                            $product_merge = $product_sample['product'];
                            $product_merge['id'] = $order_product_id;
                            $product_merge['quantity'] = $product_sample['quantity'];
                            $product_merge['type'] = 'Muestra';
                            $order_products[] = $product_merge;
                        }
                    }
                    unset($product_merge);
                }

                //$product_merge['product_image_url'] = asset_url().'img/placeholder.jpg';
                //$product_merge['image_large_url'] = asset_url().'img/placeholder.jpg';
            }

            if (!$valid_products) {
                //eliminar pedidos que no tengan ningun producto del tipo de almacenamiento del alistador logueado
                unset($picking_orders[$i]);
                continue;
            } else {
                //colocar en estado alistado pedidos completos dependiendo del perfil
                $order_enlisted = true;

                foreach ($order_products as $index => $product) {

                    if ($product['fulfilment_status'] == 'Pending') {
                        if (($picker->profile == 'Alistador Seco' && in_array($product['storage'], ['Seco', '']))
                            || ($picker->profile == 'Alistador Frío' && in_array($product['storage'], ['Refrigerado', 'Congelado']))) {
                            $order_enlisted = false;
                            break;
                        }
                    }
                }
                if ($order_enlisted)
                    $picking_orders[$i]['status'] = 'Alistado';
            }

            $picking_orders[$i]['products'] = $order_products;
            $picking_orders[$i]['profile'] = $picker->profile;
            $picking_orders[$i]['total_products'] = 0;

            if (empty($picking_orders[$i]['qty_orders']))
                $picking_orders[$i]['qty_orders'] = 0;
        }

        return $picking_orders;

    }

    /********************************************************** New App versión 1.1 *******************************************************************/
    /**
     * Obtiene siguiente pedido a alistar
     *
     * @param object $picker Objeto picker
     * @param int $picking_group_id Id del grupo de alistamiento picker
     * @param int $warehouse_id ID de la bodega
     */
    public static function getNextOrderTemp($picker, $picking_group_id, $get_enlisted_orders = true, $warehouse_id)
    {
        //$date = '2017-08-17';
        $date = date('Y-m-d');
        $picking_orders = array();

        //verificar si tiene un pedido pendiente asignado
        $order = Order::select('orders.id', 'orders.status', 'orders.store_id', 'orders.delivery_time', 'orders.route_id', 'routes.route', 'orders.planning_sequence', 'color',
            'picking_group', 'picking_group_id', 'order_groups.user_comments AS user_comments', 'orders.delivery_date', 'orders.picking_baskets', 'orders.picking_bags', 'picker_dry_id', 'picker_cold_id',
            DB::raw("IF(order_groups.source IN ('Web', 'Device'), (SELECT COUNT(o.id) FROM orders o WHERE o.user_id = users.id AND o.status = 'Delivered' GROUP BY o.user_id), 0) AS qty_orders"))
            ->join('stores', 'orders.store_id', '=', 'stores.id')
            ->join('users', 'orders.user_id', '=', 'users.id')
            ->join('order_groups', 'order_groups.id', '=', 'orders.group_id')
            ->join('routes', 'route_id', '=', 'routes.id')
            ->join('picking_group', 'picking_group_id', '=', 'picking_group.id')
            ->leftJoin('order_picking_quality', 'order_picking_quality.order_id', '=', 'orders.id')
            ->where('orders.delivery_date', '>', date('Y-m-d 00:00:00', strtotime($date)))
            ->where('orders.delivery_date', '<', date('Y-m-d 23:59:59', strtotime($date . ' +1 days')))
            ->where('routes.warehouse_id', $warehouse_id)
            ->where('routes.picking_group_id', $picking_group_id)
            ->where('routes.city_id', $picker->city_id)
            ->whereNull('order_picking_quality.id')
            ->where(function ($query) {
                $query->where('orders.status', '=', 'Enrutado')
                    ->orWhere('orders.status', '=', 'In Progress');
            });
        if ($picker->profile == 'Alistador Seco') {
            $order->where('orders.picker_dry_id', $picker->id);
            $order->whereNotNull('orders.picking_dry_start_date');
            $order->whereNull('orders.picking_dry_end_date');
        } else {
            $order->where('orders.picker_cold_id', $picker->id);
            $order->whereNotNull('orders.picking_cold_start_date');
            $order->whereNull('orders.picking_cold_end_date');
        }

        if ($order = $order->first()) {
            $picking_orders = $order->toArray();

            if ($picker->profile == 'Alistador Seco' && $picking_orders) {
                $picking_orders['product_cold'] = "";
                $products_cold = OrderProduct::join('store_products', 'order_products.store_product_id', '=', 'store_products.id')
                    ->where('order_products.order_id', $picking_orders['id'])
                    ->whereIn('store_products.storage', ['Refrigerado', 'Congelado'])
                    ->select(DB::raw("SUM(quantity) AS quantity"))
                    ->first();

                $products_cold_group = OrderProductGroup::join('store_products', 'order_product_group.store_product_id', '=', 'store_products.id')
                    ->where('order_product_group.order_id', $picking_orders['id'])
                    ->whereIn('store_products.storage', ['Refrigerado', 'Congelado'])
                    ->select(DB::raw("SUM(quantity) AS quantity"))
                    ->first();

                if ($products_cold || $products_cold_group) {
                    $picking_orders['product_cold'] = $products_cold->quantity + $products_cold_group->quantity;
                }
            }

        } else {
            //sino obtener siguiente pedido a alistar del grupo
            $orders = Order::select('orders.id', 'orders.status', 'orders.store_id', 'orders.delivery_time', 'orders.route_id', 'routes.route', 'orders.planning_sequence', 'color',
                'picking_group', 'picking_group_id', 'order_groups.user_comments AS user_comments', 'orders.delivery_date', 'orders.picking_baskets', 'orders.picking_bags', 'picker_dry_id', 'picker_cold_id',
                DB::raw("IF(order_groups.source IN ('Web', 'Device'), (SELECT COUNT(o.id) FROM orders o WHERE o.user_id = users.id AND o.status = 'Delivered' GROUP BY o.user_id), 0) AS qty_orders"))
                ->join('stores', 'orders.store_id', '=', 'stores.id')
                ->join('users', 'orders.user_id', '=', 'users.id')
                ->join('order_groups', 'order_groups.id', '=', 'orders.group_id')
                ->join('routes', 'route_id', '=', 'routes.id')
                ->join('zones', 'routes.zone_id', '=', 'zones.id')
                ->join('picking_group', 'picking_group_id', '=', 'picking_group.id')
                ->leftJoin('order_picking_quality', 'order_picking_quality.order_id', '=', 'orders.id')
                ->where('orders.delivery_date', '>', date('Y-m-d 00:00:00', strtotime($date)))
                ->where('orders.delivery_date', '<', date('Y-m-d 23:59:59', strtotime($date . ' +1 days')))
                ->where('routes.warehouse_id', $warehouse_id)
                ->where('routes.picking_group_id', $picking_group_id)
                ->whereNull('order_picking_quality.id')
                ->where('routes.city_id', $picker->city_id);
            if ($picker->profile == 'Alistador Seco')
                $orders->whereNull('orders.picker_dry_id');
            else $orders->whereNull('orders.picker_cold_id');

            $orders = $orders->where(function ($query) {
                $query->where('orders.status', '=', 'Enrutado')
                    ->orWhere('orders.status', '=', 'In Progress');
            })
                ->orderByRaw('FIELD(routes.shift, "AM", "PM", "MD")')
                ->orderBy('routes.picking_priority')
                ->orderBy('orders.planning_sequence')
                ->get();

            if ($orders) {
                foreach ($orders as $order) {
                    //obtener primer pedido que tenga productos dependiendo del perfil
                    $order_has_valid_product = false;
                    $products = OrderProduct::select('order_products.*', 'store_products.storage', 'store_product_warehouses.shrinkage_stock', 'store_product_warehouses.picking_stock', 'store_product_warehouses.picked_stock')
                        ->join('store_products', 'order_products.store_product_id', '=', 'store_products.id')
                        ->join('store_product_warehouses', 'store_product_warehouses.store_product_id', '=', 'store_products.id')
                        ->where('order_products.fulfilment_status', 'Pending')
                        ->where('store_product_warehouses.warehouse_id', $warehouse_id)
                        ->where('order_id', $order->id)->get();

                    $order_has_valid_product = false;
                    foreach ($products as $product) {
                        if ($product->type == 'Agrupado') {
                            $product_group = OrderProductGroup::select('order_products.*', 'order_product_group.*', 'store_product_warehouses.shrinkage_stock', 'store_product_warehouses.picking_stock', 'store_product_warehouses.picked_stock')
                                ->join('order_products', 'order_products.id', '=', 'order_product_group.order_product_id')
                                ->join('store_products', 'order_product_group.store_product_id', '=', 'store_products.id')
                                ->join('store_product_warehouses', 'store_product_warehouses.store_product_id', '=', 'store_products.id')
                                ->where('order_product_id', $product->id)
                                ->where('store_product_warehouses.warehouse_id', $warehouse_id)
                                ->where('order_product_group.fulfilment_status', 'Pending');

                            if ($picker->profile == 'Alistador Seco')
                                $product_group->whereIn('store_products.storage', ['Seco', '']);
                            else $product_group->whereIn('store_products.storage', ['Refrigerado', 'Congelado']);
                            if ($product_group = $product_group->first()) {
                                $order_has_valid_product = $order->toArray();
                                break;
                            }
                        } else {
                            if ($picker->profile == 'Alistador Seco' && in_array($product->storage, ['Seco', ''])) {
                                $order_has_valid_product = $order->toArray();
                                break;
                            }
                            if ($picker->profile == 'Alistador Frío' && in_array($product->storage, ['Refrigerado', 'Congelado'])) {
                                $order_has_valid_product = $order->toArray();
                                break;
                            }
                        }
                    }
                    if ($order_has_valid_product) {
                        $picking_orders = $order_has_valid_product;
                        break;
                    }
                }


                if ($picker->profile == 'Alistador Seco' && $picking_orders) {
                    $picking_orders['product_cold'] = "";

                    $products_cold = OrderProduct::join('store_products', 'order_products.store_product_id', '=', 'store_products.id')
                        ->where('order_products.order_id', $picking_orders['id'])
                        ->whereIn('store_products.storage', ['Refrigerado', 'Congelado'])
                        ->select(DB::raw("SUM(quantity) AS quantity"))
                        ->first();

                    $products_cold_group = OrderProductGroup::join('store_products', 'order_product_group.store_product_id', '=', 'store_products.id')
                        ->where('order_product_group.order_id', $picking_orders['id'])
                        ->whereIn('store_products.storage', ['Refrigerado', 'Congelado'])
                        ->select(DB::raw("SUM(quantity) AS quantity"))
                        ->first();

                    if ($products_cold || $products_cold_group) {
                        $picking_orders['product_cold'] = $products_cold->quantity + $products_cold_group->quantity;
                    }
                }
            }
        }

        return $picking_orders;
    }

    //OBTENER PRODUCTOS DEL PEDIDO
    public static function getProductsList($picker, $id, $warehouse_id)
    {
        $products_info = $order_products = $picking_products = [];
        $products = OrderProduct::select('order_products.*', 'store_products.storage')
            ->join('store_products', 'store_products.id', '=', 'order_products.store_product_id')
            ->where('type', '<>', 'Recogida')
            ->where('order_id', $id)
            ->get();

        foreach ($products as $key => $product) {
            //obtener ids de los productos del producto agrupado
            if ($product->type == 'Agrupado') {
                $product_group = OrderProductGroup::select('order_products.*', 'order_product_group.*', 'store_products.storage')
                    ->join('order_products', 'order_products.id', '=', 'order_product_group.order_product_id')
                    ->join('store_products', 'store_products.id', '=', 'order_product_group.store_product_id')
                    ->where('order_product_id', $product->id)
                    ->get();

                if ($product_group) {
                    foreach ($product_group as $product_group_item) {
                        if (!array_key_exists($product_group_item->store_product_id, $products_info)) {
                            $products_info[$product_group_item->store_product_id]['product_group'][$product_group_item->id] = ['product' => $product_group_item->toArray(), 'quantity_group' => $product_group_item->quantity, 'order_product_group_id' => $product_group_item->id];
                            $products_info[$product_group_item->store_product_id]['product'] = $product_group_item->toArray();
                        } else {
                            $products_info[$product_group_item->store_product_id]['product_group'][$product_group_item->id]['product'] = $product_group_item->toArray();
                            $products_info[$product_group_item->store_product_id]['product_group'][$product_group_item->id]['quantity_group'] = isset($products_info[$product_group_item->store_product_id][$product_group_item->id]['quantity_group']) ? $products_info[$product_group_item->store_product_id][$product_group_item->id]['quantity_group'] + $product_group_item->quantity : $product_group_item->quantity;
                            $products_info[$product_group_item->store_product_id]['product_group'][$product_group_item->id]['order_product_group_id'] = $product_group_item->id;
                        }
                    }
                }
            } elseif ($product->type == 'Muestra' || $product->type == 'Product') {
                if (!array_key_exists($product->store_product_id, $products_info))
                    $products_info[$product->store_product_id] = ['product' => $product->toArray(), 'quantity' => $product->quantity, 'order_product_id' => $product->id];
                else {
                    $products_info[$product->store_product_id]['quantity'] = isset($products_info[$product->store_product_id]['quantity']) ? $products_info[$product->store_product_id]['quantity'] + $product->quantity : $product->quantity;
                    $products_info[$product->store_product_id]['order_product_id'] = $product->id;
                }
            }
        }


        $store_products_ids = [];
        foreach ($products_info as $key => $item) {
            if ($picker->profile == 'Alistador Seco') {
                if (isset($item['product']['storage']) && ($item['product']['storage'] == 'Seco' || $item['product']['storage'] == '')) {
                    $store_products_ids[$item['product']['store_product_id']] = $item;
                }
            } elseif ($picker->profile == 'Alistador Frío' && isset($item['product']['storage']) && ($item['product']['storage'] == 'Refrigerado' || $item['product']['storage'] == 'Congelado')) {
                $store_products_ids[$item['product']['store_product_id']] = $item;
            }
        }


        $store_products_ids = array_keys($store_products_ids);
        //obtener productos del pedido
        $products = Product::select('store_products.id AS store_product_id', 'reference', 'description', 'image_medium_url',
            'store_product_warehouses.reception_stock', 'store_product_warehouses.return_stock', 'storage', 'store_product_warehouses.shrinkage_stock', 'store_product_warehouses.picking_stock', 'store_product_warehouses.picked_stock',
            DB::raw("CONCAT(storage_position, '-', storage_height_position) AS storage_position_product"))
            ->join('store_products', 'store_products.product_id', '=', 'products.id')
            ->join('store_product_warehouses', 'store_product_warehouses.store_product_id', '=', 'store_products.id')
            ->where('store_product_warehouses.warehouse_id', $warehouse_id)
            ->whereIn('store_products.id', $store_products_ids);

        if ($picker->profile == 'Alistador Seco')
            $products->orderBy(DB::raw("FIELD(storage, 'Seco')"));
        else $products->orderBy(DB::raw("FIELD(storage, 'Refrigerado', 'Congelado')"));

        $products = $products->orderBy('storage_position')
            ->orderBy('storage_height_position')
            ->orderBy('department_id', 'desc')
            ->orderBy('name', 'asc')
            ->groupBy('store_products.id')
            ->get();

        foreach ($products as $product) {
            $product->profile = $picker->profile;

            if (isset($products_info[$product->store_product_id]['product'])) {
                $product_merge = array_merge($products_info[$product->store_product_id]['product'], $product->toArray());
            }

            if (isset($products_info[$product->store_product_id]['product_group'])) {
                foreach ($products_info[$product->store_product_id]['product_group'] as $order_product_group_id => $product_group) {
                    $products_info[$product->store_product_id]['product_group'][$order_product_group_id]['product'] = array_merge($product_group['product'], $product->toArray());
                }
            }

            //setear cantidades
            if (array_key_exists($product->store_product_id, $products_info)) {
                //producto simple
                if (isset($products_info[$product->store_product_id]['quantity'])) {
                    $product_merge['id'] = $products_info[$product->store_product_id]['order_product_id'];
                    $product_merge['quantity'] = $products_info[$product->store_product_id]['quantity'];
                    $product_merge['type'] = 'Product';
                    $order_products[] = $product_merge;
                }
                //pedido agrupado
                if (isset($products_info[$product->store_product_id]['product_group'])) {
                    $quantity_request = $quantity_group = 0;
                    $order_product_group_ids = [];
                    foreach ($products_info[$product->store_product_id]['product_group'] as $order_product_group_id => $product_group) {
                        if ($product->store_product_id == $product_group['product']['store_product_id']) {
                            $order_product_group_ids[$order_product_group_id] = $product_group['quantity_group'];
                            $quantity_group = $quantity_group + $product_group['quantity_group'];
                            $quantity_request++;
                        }
                    }

                    foreach ($products_info[$product->store_product_id]['product_group'] as $order_product_group_id => $product_group) {
                        $product_merge = $product_group['product'];
                        $product_merge['id'] = $order_product_group_id;
                        $product_merge['order_product_group_ids'] = $order_product_group_ids;
                        $product_merge['quantity'] = $quantity_group;
                        $product_merge['type'] = 'Agrupado';

                        if ($quantity_request > 1) {
                            $order_products[] = $product_merge;
                            break;
                        } else {
                            $order_products[] = $product_merge;
                        }
                    }
                }
                unset($product_merge);
            }
        }

        $picking_products = $order_products;
        return $picking_products;
    }

    /********************************************************** New App versión 1.2*******************************************************************/
    /**
     * Asigna pedido a picker
     *
     * @param object $picker Objeto picker
     * @param int $picking_group_id Id del grupo de alistamiento picker
     * @param int $warehouse_id ID de la bodega
     *
     * "@return response
     */
    public static function assignOrder($picker, $pickingGroupId, $warehouse, $orderId, $route, $whitoutPlaninig = false)
    {
        try {
            DB::beginTransaction();
            $date = Carbon::now()->format('Y-m-d H:i:s');
            $pickingOrders = array();

            //verificar si tiene un pedido pendiente asignado
            $order = Order::select('orders.id', 'orders.status', 'orders.store_id', 'orders.delivery_time', 'orders.route_id', 'routes.route', 'orders.planning_sequence', 'color',
                'picking_group', 'picking_group_id', 'orders.delivery_date', 'orders.picking_baskets', 'orders.picking_bags', 'picker_dry_id', 'picker_cold_id',
                DB::raw('expected_containers->>"$.total" AS recommended_baskets'))
                ->join('order_groups', 'order_groups.id', '=', 'orders.group_id')
                ->leftjoin('routes', 'route_id', '=', 'routes.id')
                ->leftjoin('zones', 'routes.zone_id', '=', 'zones.id')
                ->leftjoin('picking_group', 'picking_group_id', '=', 'picking_group.id')
                ->leftJoin('order_picking_quality', 'order_picking_quality.order_id', '=', 'orders.id')
                ->where('orders.delivery_date', '>', date('Y-m-d 00:00:00', strtotime($date)))
                ->where('orders.delivery_date', '<', date('Y-m-d 23:59:59', strtotime($date . ' +1 days')))
                ->where('order_groups.warehouse_id', $warehouse->id)
                ->where('orders.status', '<>', 'Cancelled')
                ->whereNull('order_picking_quality.id');

            if ($picker->profile == 'Alistador Seco') {
                $order->where('orders.picker_dry_id', $picker->id);
                $order->whereNotNull('orders.picking_dry_start_date');
                $order->whereNull('orders.picking_dry_end_date');
            } else {
                $order->where('orders.picker_cold_id', $picker->id);
                $order->whereNotNull('orders.picking_cold_start_date');
                $order->whereNull('orders.picking_cold_end_date');
            }

            $order = $order->first();
            if ($order) {
                $pickingOrders = $order->toArray();
                $pickingOrders['product_dry'] = $pickingOrders['product_cold'] = $pickingOrders['product_quantity_cold'] = $pickingOrders['order_bags'] = "";
                if ($picker->profile == 'Alistador Seco' && $pickingOrders) {
                    $productsDry = OrderProduct::join('store_products', 'order_products.store_product_id', '=', 'store_products.id')
                        ->where('order_products.order_id', $pickingOrders['id'])
                        ->whereIn('order_products.type', ['Product', 'Muestra'])
                        ->whereIn('store_products.storage', ['Seco', ''])
                        ->select(DB::raw("SUM(quantity) AS quantity"))
                        ->first();

                    $productsDryGroup = OrderProductGroup::join('store_products', 'order_product_group.store_product_id', '=', 'store_products.id')
                        ->where('order_product_group.order_id', $pickingOrders['id'])
                        ->whereIn('store_products.storage', ['Seco', ''])
                        ->select(DB::raw("SUM(quantity) AS quantity"))
                        ->first();

                    $productsCold = OrderProduct::join('store_products', 'order_products.store_product_id', '=', 'store_products.id')
                        ->whereIn('order_products.type', ['Product', 'Muestra'])
                        ->where('order_products.order_id', $pickingOrders['id'])
                        ->whereIn('store_products.storage', ['Refrigerado', 'Congelado'])
                        ->select(DB::raw("SUM(quantity) AS quantity"))
                        ->first();

                    $productsColdGroup = OrderProductGroup::join('store_products', 'order_product_group.store_product_id', '=', 'store_products.id')
                        ->where('order_product_group.order_id', $pickingOrders['id'])
                        ->whereIn('store_products.storage', ['Refrigerado', 'Congelado'])
                        ->select(DB::raw("SUM(quantity) AS quantity"))
                        ->first();

                    if ($productsCold || $productsColdGroup) {
                        $pickingOrders['product_cold'] = $productsCold->quantity + $productsColdGroup->quantity;
                    }

                    if ($productsDry || $productsDryGroup) {
                        $pickingOrders['product_dry'] = $productsDry->quantity + $productsDryGroup->quantity;
                    }

                    $orderBags = \OrderPickingBasket::where('order_id', $pickingOrders['id'])
                        ->where('type', 'Frío')
                        ->select('code_basket')
                        ->get();

                    if (count($orderBags))
                        $pickingOrders['order_bags'] = json_encode($orderBags);

                } elseif ($picker->profile == 'Alistador Frío' && $pickingOrders) {

                    $productsCold = OrderProduct::join('store_products', 'order_products.store_product_id', '=', 'store_products.id')
                        ->where('order_products.order_id', $pickingOrders['id'])
                        ->whereIn('order_products.type', ['Product', 'Muestra'])
                        ->whereIn('store_products.storage', ['Refrigerado', 'Congelado'])
                        ->select(DB::raw("SUM(quantity) AS quantity"))
                        ->first();

                    $productsColdGroup = OrderProductGroup::join('store_products', 'order_product_group.store_product_id', '=', 'store_products.id')
                        ->where('order_product_group.order_id', $pickingOrders['id'])
                        ->whereIn('store_products.storage', ['Refrigerado', 'Congelado'])
                        ->select(DB::raw("SUM(quantity) AS quantity"))
                        ->first();

                    if ($productsCold || $productsColdGroup) {
                        $pickingOrders['product_quantity_cold'] = $productsCold->quantity + $productsColdGroup->quantity;
                    }
                }
            } else {
                //sino obtener siguiente pedido a alistar del grupo
                $order = Order::lockForUpdate()->select('orders.id', 'orders.status', 'orders.store_id', 'orders.delivery_time', 'orders.route_id', 'routes.route', 'orders.planning_sequence', 'color',
                    'picking_group', 'picking_group_id', 'orders.delivery_date', 'orders.picking_baskets', 'orders.picking_bags', 'picker_dry_id', 'picker_cold_id',
                    DB::raw('expected_containers->>"$.total" AS recommended_baskets'))
                    ->join('order_groups', 'order_groups.id', '=', 'orders.group_id')
                    ->join('order_products', 'order_products.order_id', '=', 'orders.id')
                    ->leftJoin('order_product_group', 'order_product_group.order_id', '=', 'orders.id')
                    ->join('store_products', function ($join) {
                        $join->on('store_products.id', '=', 'order_products.store_product_id');
                        $join->orWhere('store_products.id', '=', 'order_product_group.store_product_id');
                    })
                    ->join('delivery_windows', 'orders.delivery_window_id', '=', 'delivery_windows.id')
                    ->leftjoin('routes', 'route_id', '=', 'routes.id')
                    ->leftjoin('zones', 'routes.zone_id', '=', 'zones.id')
                    ->leftjoin('picking_group', 'picking_group_id', '=', 'picking_group.id')
                    ->leftJoin('order_picking_quality', 'order_picking_quality.order_id', '=', 'orders.id')
                    ->where('order_groups.warehouse_id', $warehouse->id)
                    ->whereNull('order_picking_quality.id');

                if (!$whitoutPlaninig) {
                    $order->where('orders.delivery_date', '>', date('Y-m-d 00:00:00', strtotime($date)))
                        ->where('orders.delivery_date', '<', date('Y-m-d 23:59:59', strtotime($date . ' +1 days')));
                }else{
                    $order->where('orders.delivery_date', '>', date('Y-m-d 00:00:00', strtotime($date)))
                        ->where('orders.delivery_date', '<', date('Y-m-d 23:59:59', strtotime($date)));
                }

                if ($picker->profile == 'Alistador Seco') {
                    $order->whereNull('orders.picker_dry_id');
                    $order->whereIn('store_products.storage', ['Seco', '']);
                }else {
                    $order->whereNull('orders.picker_cold_id');
                    $order->whereIn('store_products.storage', ['Refrigerado', 'Congelado']);
                }

                if (!$whitoutPlaninig) {
                    $order->where(function ($query) {
                        $query->where('orders.status', '=', 'Enrutado')
                            ->orWhere('orders.status', '=', 'In Progress');
                    });
                } else {
                    $order->where(function ($query) {
                        $query->where('orders.status', '=', 'Initiated')
                            ->orWhere('orders.status', '=', 'In Progress');
                    });
                    $order->where('delivery_windows.shifts', $warehouse->shift_planning);
                }

                if (!is_null($route)) {
                    $order->where('routes.city_id', $picker->city_id);
                }

                if (!empty($pickingGroupId) && !$whitoutPlaninig) {
                    $order->where('routes.picking_group_id', $pickingGroupId);
                }

                if (!$whitoutPlaninig) {
                    $order->orderByRaw('FIELD(routes.shift, "AM", "PM", "MD")')
                        ->orderBy('routes.picking_priority')
                        ->orderBy('orders.planning_sequence');
                } else {
                    $order->orderBy('orders.id', 'ASC');
                }
                $order = $order->groupBy('orders.id')->first();

                if ($order) {
                    $orderAssign = $order;
                    $pickingOrders = $order->toArray();
                    $pickingOrders['product_dry'] = $pickingOrders['product_cold'] = $pickingOrders['product_quantity_cold'] = $pickingOrders['order_bags'] = "";
                    if ($picker->profile == 'Alistador Seco' && $pickingOrders) {
                        $productsDry = OrderProduct::join('store_products', 'order_products.store_product_id', '=', 'store_products.id')
                            ->where('order_products.order_id', $pickingOrders['id'])
                            ->whereIn('order_products.type', ['Product', 'Muestra'])
                            ->whereIn('store_products.storage', ['Seco', ''])
                            ->select(DB::raw("SUM(quantity) AS quantity"))
                            ->first();

                        $productsDryGroup = OrderProductGroup::join('store_products', 'order_product_group.store_product_id', '=', 'store_products.id')
                            ->where('order_product_group.order_id', $pickingOrders['id'])
                            ->whereIn('store_products.storage', ['Seco', ''])
                            ->select(DB::raw("SUM(quantity) AS quantity"))
                            ->first();

                        $productsCold = OrderProduct::join('store_products', 'order_products.store_product_id', '=', 'store_products.id')
                            ->whereIn('order_products.type', ['Product', 'Muestra'])
                            ->where('order_products.order_id', $pickingOrders['id'])
                            ->whereIn('store_products.storage', ['Refrigerado', 'Congelado'])
                            ->select(DB::raw("SUM(quantity) AS quantity"))
                            ->first();

                        $productsColdGroup = OrderProductGroup::join('store_products', 'order_product_group.store_product_id', '=', 'store_products.id')
                            ->where('order_product_group.order_id', $pickingOrders['id'])
                            ->whereIn('store_products.storage', ['Refrigerado', 'Congelado'])
                            ->select(DB::raw("SUM(quantity) AS quantity"))
                            ->first();

                        if ($productsCold || $productsColdGroup) {
                            $pickingOrders['product_cold'] = $productsCold->quantity + $productsColdGroup->quantity;
                        }

                        if ($productsDry || $productsDryGroup) {
                            $pickingOrders['product_dry'] = $productsDry->quantity + $productsDryGroup->quantity;
                        }

                        if ($pickingOrders['product_dry'] > 0 && is_null($orderAssign->picker_dry_id)) {
                            $orderAssign->picker_dry_id = $picker->id;
                            $orderAssign->status = "In Progress";
                            $pickingOrders['picker_dry_id'] = $picker->id;
                            $orderAssign->picking_dry_start_date = $date;
                            $orderAssign->picking_date = $date;
                        }

                        $orderBags = \OrderPickingBasket::where('order_id', $pickingOrders['id'])
                            ->where('type', 'Frío')
                            ->select('code_basket')
                            ->get();

                        if (count($orderBags))
                            $pickingOrders['order_bags'] = json_encode($orderBags);

                    } elseif ($picker->profile == 'Alistador Frío' && $pickingOrders) {

                        $productsCold = OrderProduct::join('store_products', 'order_products.store_product_id', '=', 'store_products.id')
                            ->where('order_products.order_id', $pickingOrders['id'])
                            ->whereIn('order_products.type', ['Product', 'Muestra'])
                            ->whereIn('store_products.storage', ['Refrigerado', 'Congelado'])
                            ->select(DB::raw("SUM(quantity) AS quantity"))
                            ->first();

                        $productsColdGroup = OrderProductGroup::join('store_products', 'order_product_group.store_product_id', '=', 'store_products.id')
                            ->where('order_product_group.order_id', $pickingOrders['id'])
                            ->whereIn('store_products.storage', ['Refrigerado', 'Congelado'])
                            ->select(DB::raw("SUM(quantity) AS quantity"))
                            ->first();

                        if ($productsCold || $productsColdGroup) {
                            $pickingOrders['product_quantity_cold'] = $productsCold->quantity + $productsColdGroup->quantity;
                        }

                        if ($pickingOrders['product_quantity_cold'] > 0 && is_null($orderAssign->picker_cold_id)) {
                            $orderAssign->picker_cold_id = $picker->id;
                            $orderAssign->status = "In Progress";
                            $pickingOrders['picker_cold_id'] = $picker->id;
                            $orderAssign->picking_cold_start_date = $date;
                            $orderAssign->picking_date = $date;
                        }
                    }
                    $orderAssign->save();
                }
            }
            DB::commit();
            return $pickingOrders;
        } catch (\Exception $e) {
            ErrorLog::add($e, 500);
            DB::rollBack();
        }
    }

    /**
     * obtener producto de pedido
     *
     * @param object $picker Objeto picker
     * @param int $picking_group_id Id del grupo de alistamiento picker
     * @param int $warehouse_id ID de la bodega
     */
    public static function getProductsListV12($picker, $id, $warehouse_id)
    {
        $products_info = $order_products = $picking_products = [];
        $products = OrderProduct::select('order_products.*', 'store_products.storage')
            ->join('store_products', 'store_products.id', '=', 'order_products.store_product_id')
            ->where('type', '<>', 'Recogida')
            ->where('order_products.fulfilment_status', 'Pending')
            ->where('order_id', $id)
            ->get();

        foreach ($products as $key => $product) {
            //obtener ids de los productos del producto agrupado
            if ($product->type == 'Agrupado') {
                $product_group = OrderProductGroup::select('order_products.*', 'order_product_group.*', 'store_products.storage')
                    ->join('order_products', 'order_products.id', '=', 'order_product_group.order_product_id')
                    ->join('store_products', 'store_products.id', '=', 'order_product_group.store_product_id')
                    ->where('order_product_id', $product->id)
                    ->get();

                if ($product_group) {
                    foreach ($product_group as $product_group_item) {
                        if (!array_key_exists($product_group_item->store_product_id, $products_info)) {
                            $products_info[$product_group_item->store_product_id]['product_group'][$product_group_item->id] = ['product' => $product_group_item->toArray(), 'quantity_group' => $product_group_item->quantity, 'order_product_group_id' => $product_group_item->id];
                            $products_info[$product_group_item->store_product_id]['product'] = $product_group_item->toArray();
                        } else {
                            $products_info[$product_group_item->store_product_id]['product_group'][$product_group_item->id]['product'] = $product_group_item->toArray();
                            $products_info[$product_group_item->store_product_id]['product_group'][$product_group_item->id]['quantity_group'] = isset($products_info[$product_group_item->store_product_id][$product_group_item->id]['quantity_group']) ? $products_info[$product_group_item->store_product_id][$product_group_item->id]['quantity_group'] + $product_group_item->quantity : $product_group_item->quantity;
                            $products_info[$product_group_item->store_product_id]['product_group'][$product_group_item->id]['order_product_group_id'] = $product_group_item->id;
                        }
                    }
                }
            } elseif ($product->type == 'Muestra' || $product->type == 'Product') {
                if (!array_key_exists($product->store_product_id, $products_info))
                    $products_info[$product->store_product_id] = ['product' => $product->toArray(), 'quantity' => $product->quantity, 'order_product_id' => $product->id];
                else {
                    $products_info[$product->store_product_id]['quantity'] = isset($products_info[$product->store_product_id]['quantity']) ? $products_info[$product->store_product_id]['quantity'] + $product->quantity : $product->quantity;
                    $products_info[$product->store_product_id]['order_product_id'] = $product->id;
                }
            }
        }

        $store_products_ids = [];
        foreach ($products_info as $key => $item) {
            if ($picker->profile == 'Alistador Seco') {
                if (isset($item['product']['storage']) && ($item['product']['storage'] == 'Seco' || $item['product']['storage'] == '')) {
                    $store_products_ids[$item['product']['store_product_id']] = $item;
                }
            } elseif ($picker->profile == 'Alistador Frío' && isset($item['product']['storage']) && ($item['product']['storage'] == 'Refrigerado' || $item['product']['storage'] == 'Congelado')) {
                $store_products_ids[$item['product']['store_product_id']] = $item;
            }
        }


        $store_products_ids = array_keys($store_products_ids);
        //obtener productos del pedido
        $products = Product::select('store_products.id AS store_product_id', 'reference', 'description', 'image_medium_url',
            'store_product_warehouses.reception_stock', 'store_product_warehouses.return_stock', 'storage', 'store_product_warehouses.shrinkage_stock', 'store_product_warehouses.picking_stock', 'store_product_warehouses.picked_stock',
            DB::raw("CONCAT(storage_position, '-', storage_height_position) AS storage_position_product"))
            ->join('store_products', 'store_products.product_id', '=', 'products.id')
            ->join('store_product_warehouses', 'store_product_warehouses.store_product_id', '=', 'store_products.id')
            ->where('store_product_warehouses.warehouse_id', $warehouse_id)
            ->whereIn('store_products.id', $store_products_ids);

        if ($picker->profile == 'Alistador Seco')
            $products->orderBy(DB::raw("FIELD(storage, 'Seco')"));
        else $products->orderBy(DB::raw("FIELD(storage, 'Refrigerado', 'Congelado')"));

        $products = $products->orderBy('storage_position')
            ->orderBy('storage_height_position')
            ->orderBy('department_id', 'desc')
            ->orderBy('name', 'asc')
            ->groupBy('store_products.id')
            ->get();

        foreach ($products as $product) {
            $product->profile = $picker->profile;

            if (isset($products_info[$product->store_product_id]['product'])) {
                $product_merge = array_merge($products_info[$product->store_product_id]['product'], $product->toArray());
            }

            if (isset($products_info[$product->store_product_id]['product_group'])) {
                foreach ($products_info[$product->store_product_id]['product_group'] as $order_product_group_id => $product_group) {
                    $products_info[$product->store_product_id]['product_group'][$order_product_group_id]['product'] = array_merge($product_group['product'], $product->toArray());
                }
            }

            //setear cantidades
            if (array_key_exists($product->store_product_id, $products_info)) {
                //producto simple
                if (isset($products_info[$product->store_product_id]['quantity'])) {
                    $product_merge['id'] = $products_info[$product->store_product_id]['order_product_id'];
                    $product_merge['quantity'] = $products_info[$product->store_product_id]['quantity'];
                    $product_merge['type'] = 'Product';
                    $order_products[] = $product_merge;
                }
                //pedido agrupado
                if (isset($products_info[$product->store_product_id]['product_group'])) {
                    $quantity_request = $quantity_group = 0;
                    $order_product_group_ids = [];
                    foreach ($products_info[$product->store_product_id]['product_group'] as $order_product_group_id => $product_group) {
                        if ($product->store_product_id == $product_group['product']['store_product_id']) {
                            $order_product_group_ids[$order_product_group_id] = $product_group['quantity_group'];
                            $quantity_group = $quantity_group + $product_group['quantity_group'];
                            $quantity_request++;
                        }
                    }

                    foreach ($products_info[$product->store_product_id]['product_group'] as $order_product_group_id => $product_group) {
                        $product_merge = $product_group['product'];
                        $product_merge['id'] = $order_product_group_id;
                        $product_merge['order_product_group_ids'] = $order_product_group_ids;
                        $product_merge['quantity'] = $quantity_group;
                        $product_merge['type'] = 'Agrupado';

                        if ($quantity_request > 1) {
                            $order_products[] = $product_merge;
                            break;
                        } else {
                            $order_products[] = $product_merge;
                        }
                    }
                }
                unset($product_merge);
            }
        }

        $picking_products = $order_products;
        return $picking_products;
    }
}
