<?php

class SuggestedProduct
{
    /**
     * Obtener promociones con base en productos del carrito
     */
    public static function promosForProducts($storeProductIds = array())
    {
        $excluded = DB::table('shelves_suggested_products')
                     ->join('store_products', 'store_products.id', '=', 'shelves_suggested_products.store_product_id')
                     ->whereIn('store_products.id', $storeProductIds)
                     ->where('shelves_suggested_products.status', 1)
                     ->groupBy('promo')
                     ->orderBy('priority')
                     ->get();
        $excluded = array_map(function ($p) { return $p->promo; }, $excluded);

        return $excluded;
    }

    /**
     * Obtener producto sugerido con base a pasillos de los productos del carrito
     * @param array $shelfIds
     * @param array $excluded
     * @param int $limit
     * @param null $warehouse
     * @param null $user_orders_info
     * @return array|\Illuminate\Database\Query\Builder[]
     */
    public static function forShelves(
        $shelfIds = array(),
        $excluded = array(),
        $limit = 1,
        $warehouse = null,
        $user_orders_info = null,
        $shelveSuggestedProductId = null
    ) {
        $has_orders = $user_orders_info && $user_orders_info->qty ? $user_orders_info->qty : false;
        $last_order_date = $user_orders_info && $user_orders_info->last_order_date ? $user_orders_info->last_order_date : false;

        $shelfIds = array_unique($shelfIds);
        $query = StoreProduct::select(
            'store_products.public_price',
            'store_products.special_price',
                'store_products.id', 'store_products.store_id', 'store_products.department_id', 'store_products.shelf_id AS shelf_id', 'products.name', 'store_products.price',
                'store_products.first_order_special_price', 'products.image_large_url', 'products.image_medium_url', 'products.image_small_url', 'products.image_app_url', 'store_products.is_best_price',
                'products.quantity', 'products.unit', 'products.description', 'products.nutrition_facts', 'shelves.has_warning', StoreProduct::getRawDeliveryDiscountByDate(),
                'stores.name as store', 'departments.name as department', 'shelves.name as shelf','products.type', StoreProduct::getRawSpecialPriceQuery($has_orders, $last_order_date)
            )
            ->join('shelves_suggested_products', 'store_products.id', '=', 'shelves_suggested_products.store_product_id')
            ->join('products', 'products.id', '=', 'store_products.product_id')
            ->join('stores', 'stores.id', '=', 'store_products.store_id')
            ->join('departments', 'departments.id', '=', 'store_products.department_id')
            ->join('shelves', 'shelves.id', '=', 'store_products.shelf_id')
            ->pum()
            ->whereIn('shelves_suggested_products.shelf_id', $shelfIds)
            ->whereNotIn('promo', $excluded)
            ->where('shelves_suggested_products.status', 1)
            ->where('shelves_suggested_products.start_date', '<=', date("Y-m-d"))
            ->where('shelves_suggested_products.end_date', '>=', date("Y-m-d"))
            ->orderBy('priority')
            ->orderBy(DB::raw('RAND()'))
            ->limit($limit);

        if (!empty($shelveSuggestedProductId)) {
            $query->where('shelves_suggested_products.id', $shelveSuggestedProductId);
        } else {
            $query->where('shelves_suggested_products.store_product_ids', null);
        }

        if (!empty($warehouse->id)) {
            $query->join('store_product_warehouses', 'store_products.id', '=', 'store_product_warehouses.store_product_id')
                ->where('store_product_warehouses.warehouse_id', $warehouse->id)
                ->where('store_product_warehouses.status', 1)
                ->where('store_product_warehouses.is_visible', 1)
                ->whereRaw('IF(store_product_warehouses.manage_stock = 1, IF(store_product_warehouses.is_visible_stock = 0, IF( store_product_warehouses.current_stock > 0, TRUE, FALSE), TRUE), TRUE)');
        }

        return $query->get();
    }
}
