<?php


class ProductTransferDetailLog extends Illuminate\Database\Eloquent\Model
{
    protected $table = 'product_transfer_detail_logs';

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

    public function productTransferDetail()
    {
        return $this->belongsTo(ProductTransferDetail::class, 'transfer_detail_id');
    }
}