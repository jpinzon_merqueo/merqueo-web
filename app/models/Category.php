<?php

class Category extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'categories';

    public function getSubCategories()
    {
    	$subcategory = Subcategory::where('category_id', $this->id)->get();
    	return $subcategory;
    }

}