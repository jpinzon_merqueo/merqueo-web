<?php

class ShrinkageDetail extends \Illuminate\Database\Eloquent\Model
{

    protected $table = 'shrinkage_details';
    protected $hidden = array('created_at', 'updated_at');

    /**
     * Relación con el modelo Shrinkage
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function shrinkage()
    {
        return $this->belongsTo('Shrinkage');
    }

    /**
     * Relación con el modelo StoreProduct
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function storeProduct()
    {
        return $this->belongsTo('StoreProduct');
    }

}