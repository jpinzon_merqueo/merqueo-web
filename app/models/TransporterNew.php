<?php

/**
 * Class TransporterNew
 * 
 */
class TransporterNew extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'transporter_news';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function vehicle() {
        return $this->belongsTo('Vehicle');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function admin() {
        return $this->belongsTo('Admin');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order() {
        return $this->belongsTo('Order', 'order_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transporterNewDetail(){
        return $this->hasMany('TransporterNewDetail','transporter_new_id', 'id');
    }

    /**
     * @return mixed
     */
    public function city() {
        return Order::select('cities.*')
            ->join('order_groups', 'orders.group_id','=','order_groups.id')
            ->join('warehouses', 'order_groups.warehouse_id','=','warehouses.id')
            ->join('cities', 'cities.id','=','warehouses.city_id')
            ->where('orders.id', $this->order_id)
            ->first();
    }

    /**
     * @return \Illuminate\Support\Collection|null|static
     */
    public function orderProducts() {
        if($this->product_type == 'Agrupado'){
            return OrderProductGroup::find($this->order_product_id);
        }else{
            return OrderProduct::find($this->order_product_id);
        }
    }

    /**
     * @param $order_id
     * @param $order_product_id
     * @param $product_type
     * @return mixed
     */
    public static function getNewsByProduct($order_id, $order_product_id, $product_type){
        return self::where('transporter_news.type', 'Productos')
            ->join('transporter_new_details', 'transporter_new_details.transporter_new_id', '=', 'transporter_news.id')
            ->where('transporter_new_details.product_type', $product_type)
            ->where('transporter_news.status', 'Sin Asignar')
            ->where('transporter_news.order_id', $order_id)
            ->where('transporter_new_details.order_product_id', $order_product_id)
            ->first();
    }
}
