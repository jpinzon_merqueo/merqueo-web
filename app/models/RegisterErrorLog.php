<?php

/**
 * Class RegisterErrorLog
 */
class RegisterErrorLog extends ErrorLog
{
    /**
     * @var string
     */
    protected $table = 'register_error_log';

    /**
     * {@inheritdoc}
     */
    public static function add(Exception $exception, $code = 500)
    {
        $error_log = new RegisterErrorLog();
        $error_log->url = \URL::current();
        $error_log->request = self::buildMessage($exception);
        $error_log->message = $exception->getMessage();
        $error_log->ip = get_ip();

        $error_log->save();
    }
}
