<?php

class Transporter extends \Illuminate\Database\Eloquent\Model
{

    protected $table = 'transporters';

    public function city()
    {
        return $this->belongsTo('City', 'city_id');
    }

    public function driver()
    {
        return $this->hasMany('Driver');
    }
}
