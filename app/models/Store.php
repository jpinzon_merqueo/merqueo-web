<?php

use \Carbon\Carbon;
use Illuminate\Support\Collection;

/**
 * @property int $city_id
 * @property float $minimum_order_amount_free_delivery
 */
class Store extends \Illuminate\Database\Eloquent\Model
{
    const SAME_DAY_LIMIT_HOUR = 16;
    const SAME_DAY_HOUR_23 = 23;
    const SLUG_DARK_MEXICO = 'mexico-df';

    protected $table = 'stores';
    protected $hidden = array('created_at','updated_at');

    /**
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function storeProducts()
    {
        return $this->hasMany('StoreProduct');
    }

    /**
     * Relación "muchos a muchos" con productos
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function products()
    {
        return $this->belongsToMany(Product::class, 'store_products')
            ->withPivot(
                'id',
                'price',
                'special_price',
                'quantity_special_price',
                'department_id',
                'shelf_id',
                'current_stock'
            );
    }

    /**
     * Relación de la ciudad
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo(City::class);
    }

    /**
     * Obtiene departamentos de la tienda
     *
     * @param string $warehouse_id ID de la bodega
     * @return Store
     */
    public function getDepartments($warehouse_id)
    {
        return Department::select('departments.id', 'departments.name', 'departments.store_id',
            DB::raw('COUNT(store_products.id) AS products_qty'), 'image_list_app_url')
            ->join('store_products', 'store_products.department_id', '=', 'departments.id')
            ->join('products', 'products.id', '=', 'store_products.product_id')
            ->join('shelves', 'store_products.shelf_id', '=', 'shelves.id')
            ->join('store_product_warehouses', 'store_products.id', '=', 'store_product_warehouses.store_product_id')
            ->where('departments.store_id', $this->id)
            ->where('store_product_warehouses.status', 1)
            ->where('departments.status', 1)
            ->where('shelves.status', 1)
            ->where('products.type', '<>', 'Proveedor')
            ->where('store_product_warehouses.warehouse_id', $warehouse_id)
            ->whereRaw('IF(store_product_warehouses.manage_stock = 1, IF(store_product_warehouses.is_visible_stock = 0, IF( store_product_warehouses.current_stock > 0, TRUE, FALSE), TRUE), TRUE)')
            ->groupBy('departments.id')
            ->orderBy('departments.sort_order')
            ->orderBy('departments.name')
            ->get();
    }

    /**
     * Obtiene departamento con pasillos y productos de la tienda
     *
     * @param int $department_id ID del departamento
     * @param int $warehouse_id ID de la bodega
     * @return array
     * @deprecated
     */
    public function getDepartment($department_id, $warehouse_id)
    {
        return [];
    }

    /**
     * Obtiene tiendas que atienden una ubicación de latitud y longitud
     *
     * @param string|float|int $latitude Latitud
     * @param string|float|int $longitude Longitud
     * @return Collection Tiendas
     * @throws \exceptions\CoverageException
     */
    public static function getByLatlng($latitude = 1, $longitude = 1)
    {
        $store_query = Store::select(
            'stores.id', 'stores.city_id', 'stores.is_storage', 'stores.name', 'stores.slug',
            'stores.app_background_color', 'stores.description_app', 'stores.coverage',
            'stores.description_web', 'stores.logo_url', 'stores.app_logo_large_url as image_url',
            'cities.city AS city_name', 'cities.slug AS city_slug', 'minimum_order_amount', 'minimun_order_amount_express'
        )
            ->join('cities', 'cities.id', '=', 'stores.city_id')
            ->where('stores.status', 1)
            ->where('stores.is_hidden', 0)
            ->orderBy('stores.sort_order');

        if ($latitude == 1 && $longitude == 1) {
            return $store_query->where('city_id', Session::get('city_id'))->get();
        }

        foreach ($store_query->get() as $store) {
            if (empty($store->coverage)) {
                continue;
            }

            if (point_in_polygon($store->coverage, $latitude, $longitude)) {
                return $store;
            }
        }

        throw new \exceptions\CoverageException($latitude, $longitude);
    }

    /**
     * Obtiene el tiempo de entrega de una tienda
     *
     * @param int $store_id ID de la tienda
     * @return array Estado y tiempo de entrega de la tienda en texto
     */
    public static function getDeliverySlot($store_id)
    {
        $is_open = $next_slot = 0;
        $now = date('Y-m-d H:i:s');
        $date = date('Y-m-d');

        $slot = Slot::select('end_time')
            ->join('stores', 'stores.id', '=', 'store_id')
            ->where('store_id', $store_id)
            ->where('start_time', '<', $now)
            ->where('end_time', '>', $now)
            ->where('is_available', 1)
            ->orderBy('start_time')
            ->first();

        if ($slot) {
            $closes_in = strtotime($slot->end_time) - strtotime($now);
            if ($closes_in > 0) {
                $is_open = 1;
            }
        }

        //obtener texto de tiempo de entrega del dia de hoy
        $row = Slot::select(
            'delivery_time', 'start_time', 'end_time', 'delivery_time_minutes',
            DB::raw("DATEDIFF(day, DATE_FORMAT(('{$date}' + INTERVAL delivery_time_minutes MINUTE), '%Y-%m-%d')) AS delivery_days")
        )
            ->join('stores', 'stores.id', '=', 'store_id')
            ->where('store_id', $store_id)
            ->where('day', '=', DB::raw("DATE_FORMAT(('{$date}' + INTERVAL delivery_time_minutes MINUTE), '%Y-%m-%d')"))
            ->where('is_available', 1)
            ->orderBy('end_time')
            ->first();

        if ($row) {
            if (!$row->delivery_days && $now <= $row->start_time) {
                $next_slot = $row->delivery_time_minutes >= 360 ? 'Mañana' : 'Hoy';
            } else if (!$row->delivery_days && $now <= $row->end_time) {
                $next_slot = $row->delivery_time;
            } else if ($row->delivery_days == 1) {
                $next_slot = 'Mañana';
            } else if ($row->delivery_days == 2) {
                $next_slot = 'Pasado mañana';
            } else if ($row->delivery_days > 2) {
                $next_slot = "Dentro de {$row->delivery_days} días";
            }
        }

        //obtener texto de tiempo de proxima entrega
        if (!$next_slot) {
            $rows = Slot::select(DB::raw("DATEDIFF(day, '".$date."') AS delivery_days"))
                ->join('stores', 'stores.id', '=', 'store_id')
                ->where('store_id', $store_id)
                ->where('day', '>', $date)
                ->where('day', '<', date('Y-m-d', date(strtotime($date.'+5 day'))))
                ->where('is_available', 1)
                ->orderBy('end_time')
                ->get();

            if ($rows) {
                foreach($rows as $key => $row) {
                    if (!$row->delivery_days) {
                        $next_slot = 'Hoy';
                    } else if ($row->delivery_days == 1) {
                        $next_slot = 'Mañana';
                    } else if ($row->delivery_days == 2) {
                        $next_slot = 'Pasado mañana';
                    } else if ($row->delivery_days > 2) {
                        $next_slot = 'Dentro de ' . $row->delivery_days . ' días';
                    } if ($next_slot) {
                        break;
                    }
                }
            }

            if (!$next_slot){
                $is_open = 0;
                $next_slot = 'Mañana';
            }
        }

        return ['is_open' => $is_open, 'next_slot' => $next_slot];
    }

    /**
     * Obtiene las franjas de entrega en el checkout para un pedido
     *
     * @param Zone $zone ID de la zona en el pedido
     * @param boolean $hasFreeDelivery Si el usuario tiene domicilio gratis
     * @param int $additionalDays Número de días a mostrar
     * @param bool $sortResults
     * @param bool $allowSameDay
     * @param bool $hideMdShifts
     * @return array $delivery tiempos de entrega de la tiendas
     */
    public function getDeliverySlotCheckout(
        Zone $zone,
        $hasFreeDelivery = false,
        $additionalDays = 6,
        $sortResults = true,
        $allowSameDay = 1,
        $hideMdShifts = 0
    ) {
        $slots = $this->getDeliverySlots($zone, $hasFreeDelivery, $allowSameDay, $hideMdShifts);

        return $this->getOldFormat($additionalDays, $sortResults, $slots);
    }

    /**
     * Obtiene franjas de entrega
     *
     * @param Zone $zone
     * @param $setFreeDelivery
     * @param int $allowSameDay
     * @param int $hideMdShifts
     * @param $slot_warehouse_express
     * @return Collection
     */
    public function getDeliverySlots(Zone $zone, $setFreeDelivery, $allowSameDay = 1, $hideMdShifts = 0, $slot_warehouse_express = null)
    {
        $slots = $this->getDeliverySlotsByShift($zone, $allowSameDay, $hideMdShifts, $slot_warehouse_express);

        if ($setFreeDelivery) {
            $slots = $slots->map(function ($slot) {
                $slot->deliveryWindow->delivery_amount = 0;

                return $slot;
            });
        }

        return $slots;
    }

    /**
     * Obtiene franjas de entrega
     *
     * @param Zone $zone
     * @param $freeDelivery
     * @param $latitude
     * @param $longitude
     * @param $slot_warehouse_express
     * @return Collection
     */
    public function getClearDeliverySlots(
        Zone $zone,
        $freeDelivery,
        $latitude,
        $longitude,
        $slot_warehouse_express = null
    ) {
        $today = Carbon::now();
        if(in_array($zone->warehouse_id, Warehouse::WAREHOUSE_DARKSUPERMARKET_ID)){
            $collection = $this->getDeliverySlotsForDarkSupermarket($zone, 1, 0);
        }else{
            $collection = $this->getDeliverySlots($zone, $freeDelivery, 1,0, $slot_warehouse_express);
        }
        $zoneLocations = $zone->zoneLocations()
            ->where('status', 1)
            ->get();

        foreach ($zoneLocations as $zoneLocation) {
            if (!point_in_polygon($zoneLocation->delivery_zone, $latitude, $longitude)) {
                continue;
            }

            $today = $today->hour == self::SAME_DAY_HOUR_23 ? $today->addDay() : $today;

            foreach ($zoneLocation->zoneLocationSlots as $slot) {
                foreach ($collection as $index => $item) {
                    $sameDeliveryWindowId = $item->delivery_window_id == $slot->delivery_window_id;
                    $forgetSameDay = $sameDeliveryWindowId && $item->day == SlotWarehouse::SAME_DAY && $slot->day == $today->dayOfWeek;
                    $forgetCommonDay = $sameDeliveryWindowId && $slot->day == $item->day;

                    if ($forgetSameDay || $forgetCommonDay) {
                        $collection->forget($index);
                    }
                }
            }
        }

        $collection->values();

        return $this->getOldFormat(7, false, $collection);
    }

    /**
     * Obtiene franjas de entrega disponibles con base a limite de productos
     *
     * @param Zone $zone
     * @param bool $allowSameDay
     * @param bool $hideMdShifts
     * @param $slot_warehouse_express
     * @return mixed
     */
    private function getDeliverySlotsByShift(Zone $zone, $allowSameDay = 1, $hideMdShifts = 0, $slot_warehouse_express = null)
    {
        $current_date = \Carbon\Carbon::create();
        $current_hour = $current_date->format('H:i:s');
        $slot_query = function ($query) use ($zone, $hideMdShifts, $current_hour) {
            $query->where('status', 1)
                ->where('city_id', $this->city_id);

            if (!$zone->canShowSameDay() || $hideMdShifts) {
                $query->where('shifts', '<>', 'MD');
            }

            $query->whereRaw("(
                    CASE
                        WHEN shifts = 'ER'  
                        THEN '$current_hour' BETWEEN hour_start AND hour_end
                        ELSE TRUE	                    
                    END
                )");
        };

        $zone_ids = [$zone->warehouse->id];
        if($slot_warehouse_express){
            $zone_ids = array_merge($zone_ids,[$slot_warehouse_express->warehouse_id]);
        }


        $slots = SlotWarehouse::select('current_products', 'limit_products', 'slot_warehouses.*', $this->getRawNewLabelDeliverySlot())
            ->join('delivery_windows', 'delivery_window_id', '=', 'delivery_windows.id')
            ->leftJoin('slots_products', function($leftJoin) {
                $leftJoin->on('slot_warehouses.warehouse_id', '=', 'slots_products.warehouse_id');
                $leftJoin->on('slot_warehouses.day', '=', 'slots_products.day');
                $leftJoin->on('slot_warehouses.delivery_window_id', '=', 'slots_products.delivery_window_id');
            })
            ->whereIn('slot_warehouses.warehouse_id', $zone_ids)
            ->whereRaw('IF (current_products IS NULL, TRUE, IF (current_products < number_products, TRUE, FALSE))')
            ->where('number_products', '>', 0)
            ->whereHas('deliveryWindow', $slot_query)
            ->with(['deliveryWindow' => $slot_query])
            ->orderBy('slot_warehouses.day')
            ->orderBy('hour_start');

        if ($allowSameDay) {
            $current_date_hour = $current_date->hour == self::SAME_DAY_HOUR_23 ? $current_date->addDay() : $current_date;
            $slots->where('slot_warehouses.day', '<>', $current_date_hour->dayOfWeek);
        }

        if($slot_warehouse_express){
            $slots->whereRaw("(
                    CASE
                        WHEN slot_warehouses.warehouse_id IN ({$slot_warehouse_express->warehouse_id}) 
                        THEN slot_warehouses.delivery_window_id = {$slot_warehouse_express->delivery_window_id}
	                    ELSE slot_warehouses.warehouse_id IN ({$zone->warehouse->id})
                    END
                )");
        }

        $slots = $slots->get();

        return $slots->values();
    }

    /**
     * Valida que una tienda tenga cobertura en una direccion usando la latitud longitud
     * y retorna objeto deliveryWindow franja horaria de entrega
     *
     * @param bool|string $latitude Latitud de la direccion
     * @param bool|string $longitude Longitud de la direccion
     * @param Carbon $delivery_date
     * @param int $delivery_window_id
     * @return DeliveryWindow
     * @throws \exceptions\CoverageException
     * @throws \exceptions\MerqueoException
     */
    public function validateStoreDeliversToAddress($latitude, $longitude, Carbon $delivery_date, $delivery_window_id)
    {
        if ($latitude === 1 && $longitude === 1) {
            return Zone::where('status|', 1)
                ->whereHas('warehouse', function ($query) {
                    $query->where('status', 1);
                })
                ->first();
        }

        //validar franja horaria express
        $shift_express = \DeliveryWindow::where(['id' => $delivery_window_id,'shifts' => 'EX'])->count();

        $zone = Zone::getByLatLng($this->city_id, $latitude, $longitude, $shift_express);
        if (empty($zone)) {
            throw new \exceptions\CoverageException($latitude, $longitude);
        }

        $slot_warehouse = $zone->warehouse->slotWarehouse()
            ->where('delivery_window_id', $delivery_window_id)
            ->first();

        if (empty($slot_warehouse)) {
            throw new \exceptions\MerqueoException('Los horarios de entrega han cambiado, por favor selecciona otro.');
        }

        $zone->validateSlotConstrains($delivery_date, $slot_warehouse->deliveryWindow, $latitude, $longitude);

        return $slot_warehouse->deliveryWindow;
    }

    /**
     * Obtener tiendas por ciudad
     *
     * @param $city_id
     * @return mixed
     */
    public static function getActiveStores($city_id)
    {
        return Store::where('city_id', $city_id)->where('status', 1)->get();
    }

    /**
     * @param int $additional_days
     * @param bool $sort_results
     * @param array|Collection $slots
     * @return mixed
     */
    private function getOldFormat($additional_days, $sort_results, $slots)
    {
        $current_date = \Carbon\Carbon::create();
        if($current_date->hour == Store::SAME_DAY_HOUR_23){
            $current_date->addDay();
        }
        $delivery_days = [];
        $delivery_time = [];
        for ($i = 0; $i < $additional_days; $i++) {
            $hours = [];
            foreach ($slots as $index => $slot) {
                if ($slot->day == $current_date->dayOfWeek || $slot->day == SlotWarehouse::SAME_DAY) {
                    $hours[] = $slot;
                    $slots->forget($index);
                }
            }

            if (empty($hours)) {
                $current_date->addDay();
                continue;
            }

            $items = [];
            $date_format = $current_date->format('Y-m-d');
            $delivery_days[$date_format] = format_date('day_name', $date_format);

            foreach ($hours as $hour) {
                $delivery_window = $hour->deliveryWindow;
                $delivery_value = $delivery_window->id;
                $delivery_amount_text = $delivery_window->delivery_amount > 0
                    ? (' (Domicilio: ' . currency_format($delivery_window->delivery_amount) . ')') : '';

                $items[$delivery_value] = [
                    'value' => $delivery_value,
                    'text' => $delivery_window->delivery_window . $delivery_amount_text,
                    'delivery_amount' => $delivery_window->delivery_amount,
                    'is_new' => $hour->is_new,
                    'is_express' => $delivery_window->shifts === 'EX' ? 1 : 0
                ];
            }
            $delivery_time[$date_format] = $items;
            $current_date->addDay();
        }

        if ($sort_results) {
            $order = [
                // Este horario solo se tiene en cuenta en los slots del mismo día.
                '20:00:00 - 22:00:00',

                // Estos horarios se tienen en cuenta para el resto de los slots.
                '18:00:00 - 22:00:00',
            ];
            foreach ($delivery_time as $date => $date_detail) {
                $temporal_delivery_times = [];
                foreach ($order as $item) {
                    if (!empty($date_detail[$item])) {
                        $temporal_delivery_times[$item] = $date_detail[$item];
                        unset($date_detail[$item]);
                    }
                    foreach ($date_detail as $time => $detail) {
                        $temporal_delivery_times[$time] = $detail;
                    }
                    $delivery_time[$date] = $temporal_delivery_times;
                }
            }
        }

        $delivery[$this->id] = ['name' => $this->name, 'days' => $delivery_days, 'time' => $delivery_time];

        return $delivery;
    }

    /**
     * Retorna 1, sí la franja horaria es nueva
     *
     * @return \Illuminate\Database\Query\Expression
     */
    private function getRawNewLabelDeliverySlot()
    {
        return \DB::raw(
            "IF(DATEDIFF(NOW(),`delivery_windows`.`created_at`) <= 30, 1, 0) AS is_new"
        );
    }

    /**
     * Obtiene franjas de entrega disponibles para darkSupermarket
     *
     * @param Zone $zone
     * @param bool $allowSameDay
     * @param bool $hideMdShifts
     * @return mixed
     */
    private function getDeliverySlotsForDarkSupermarket(Zone $zone, $allowSameDay = 1, $hideMdShifts = 0)
    {
        $slot_query = function ($query) use ($zone, $hideMdShifts) {
            $query->where('status', 1)
                ->where('city_id', $this->city_id)
                ->where('shifts', 'MD');
        };

        $current_date = \Carbon\Carbon::create();
        $slots = $zone->warehouse->slotWarehouse()
            ->select( 'slot_warehouses.*', $this->getRawNewLabelDeliverySlot())
            ->join('delivery_windows', 'delivery_window_id', '=', 'delivery_windows.id')
            ->whereHas('deliveryWindow', $slot_query)
            ->with(['deliveryWindow' => $slot_query])
            ->orderBy('slot_warehouses.day')
            ->orderBy('hour_start');

        if ($allowSameDay) {
            $slots->where('slot_warehouses.day', '<>', $current_date->dayOfWeek);
        }

        $slots = $slots->get();
        return $slots->values();
    }

}
