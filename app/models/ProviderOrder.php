<?php

/**
 * Class ProviderOrder
 * @property Warehouse $warehouse
 */
class ProviderOrder extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'provider_orders';
    protected $hidden = array('created_at','updated_at');
    protected $products = null;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reception()
    {
        return $this->hasMany('ProviderOrderReception', 'provider_order_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function providerOrderRejectReason()
    {
        return $this->belongsTo(ProviderOrderRejectReason::class, 'provider_order_return_typification_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function admin()
    {
        return $this->belongsTo('Admin', 'admin_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function providerOrderDeliveryWindow()
    {
        return $this->belongsTo(ProviderOrderDeliveryWindow::class, 'provider_order_delivery_window_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function provider()
    {
        return $this->belongsTo('Provider', 'provider_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function providerOrderDetails()
    {
        return $this->hasMany('ProviderOrderDetail', 'provider_order_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function providerOrderReception()
    {
        return $this->hasMany('ProviderOrderReception', 'provider_order_id');
    }

    /**
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class);
    }

    /**
     * Valida y actualiza estado de una orden de compra con base a recibos de bodega
     */
    public static function updateStatus($id)
    {
        $provider_order_products = ProviderOrderDetail::where('provider_orders.id', $id)
        ->join('store_products', 'store_products.id', '=', 'provider_order_details.store_product_id')
        ->join('products', 'products.id', '=', 'store_products.product_id')
        ->join('provider_orders', 'provider_orders.id', '=', 'provider_order_details.provider_order_id')
        ->leftJoin(DB::raw('(provider_order_receptions, provider_order_reception_details)'), function ($join) {
            $join->on('provider_order_receptions.provider_order_id', '=', 'provider_orders.id');
            $join->on('provider_order_reception_details.reception_id', '=', 'provider_order_receptions.id');
            $join->on('provider_order_reception_details.store_product_id', '=', 'store_products.id');
        })
        ->select(
            'provider_order_details.*',
            'products.name',
            'products.reference',
            'products.image_small_url',
            'store_products.provider_pack_quantity_approach',
            'store_products.provider_pack_type',
            'products.unit',
            'store_products.provider_pack_quantity',
            'provider_order_reception_details.status',
            DB::raw('SUM(provider_order_reception_details.quantity_received) AS quantity_received')
        )
        ->groupBy('provider_order_details.store_product_id')
        ->get();
        $quantity_received = $quantity_order = 0;
        $received_order = true;
        foreach ($provider_order_products as $product) {
            if ($product->quantity_received >= $product->quantity_order) {
                $received_order &= true;
            } else {
                $received_order &= false;
            }
            $quantity_received = $quantity_received + $product->quantity_received;
            $quantity_order = $quantity_order + $product->quantity_order;
        }

        if ($received_order) {
            $provider_order = ProviderOrder::find($id);
            $provider_order->management_date = date('Y-m-d H:i:s');
            $provider_order->status = 'Cerrada';
            $provider_order->save();

            $log = new ProviderOrderLog();
            $log->type = 'Estado actualizado: Cerrada';
            $log->admin_id = Session::get('admin_id');
            $log->provider_order_id = $id ;
            $log->save();
        } else {
            if ($quantity_received) {
                $provider_order = ProviderOrder::find($id);
                $provider_order->status = 'Pendiente';
                $provider_order->save();

                $log = new ProviderOrderLog();
                $log->type = 'Estado actualizado: Pendiente';
                $log->admin_id = Session::get('admin_id');
                $log->provider_order_id = $id ;
                $log->save();
            }
        }
    }

    /**
     * Agrega productos a la orden de compra
     * @param Int  $store_product_id
     * @param Int  $cant
     * @param boolean $is_merqueo_provider
     */
    public function addProduct($store_product_id, $cant, $is_merqueo_provider = true)
    {
        $show_custom_quantity_unit_to_request = false;
        $provider_order_details = ProviderOrderDetail::where('provider_order_id', $this->id)->where('store_product_id', $store_product_id)->count();

        if (!$provider_order_details) {
            $store_product = StoreProduct::select('products.*', 'store_products.*')->join('products', 'store_products.product_id', '=', 'products.id')->where('store_products.id', $store_product_id)->first();


            if ( $store_product->provider_id != $this->provider_id && $is_merqueo_provider ) {
                return ['status' => false, 'message' => 'El producto no pertenece al proveedor de la orden de compra.'];
            }

            $store_obj = Store::find($store_product->store_id);
            if ( $store_obj->city_id != $this->warehouse->city_id ) {
                return ['status' => false, 'message' => 'El producto no pertenece a la ciudad de la orden de compra.'];
            }

            $provider_order_detail = new ProviderOrderDetail;
            $provider_order_detail->provider_order_id = $this->id;
            $provider_order_detail->store_product_id = $store_product->id;
            $provider_order_detail->type = $store_product->type;
            $provider_order_detail->plu = $store_product->provider_plu;
            $provider_order_detail->reference = $store_product->reference;
            $provider_order_detail->product_name = $store_product->name.' '.$store_product->quantity.' '.$store_product->unit;
            $provider_order_detail->ideal_stock = $store_product->ideal_stock;
            $provider_order_detail->current_stock = $store_product->getCurrentStock($this->warehouse->id);

            if (!$store_product->provider_pack_quantity_approach)
            {
                //si el proveedor no ofrece aproximacion de unidades
                if ($cant > $store_product->provider_pack_quantity)
                {
                    $quantity_pack_to_request = $store_product->provider_pack_quantity ? ceil($cant / $store_product->provider_pack_quantity) : 1;
                    $quantity_pack_to_request = $quantity_pack_to_request;
                    $quantity_unit_to_request = $show_custom_quantity_unit_to_request ? $store_product->quantity_unit_to_request : $quantity_pack_to_request * $store_product->provider_pack_quantity;
                }else{
                    $quantity_pack_to_request = 1;
                    $quantity_unit_to_request = $show_custom_quantity_unit_to_request ? $store_product->quantity_unit_to_request : $store_product->provider_pack_quantity;
                }
            }else{
                //el proveedor si ofrece aproximacion de unidades
                $quantity_pack_to_request = floor($cant / $store_product->provider_pack_quantity);
                $quantity_unit_to_request = $cant;
                $product_quantity_unit = $cant - ($store_product->provider_pack_quantity * floor($cant / $store_product->provider_pack_quantity));
            }

            $provider_order_detail->quantity_order = $quantity_unit_to_request;
            $provider_order_detail->quantity_pack = $quantity_pack_to_request ? $quantity_pack_to_request : $quantity_unit_to_request;
            $provider_order_detail->pack_description = $store_product->provider_pack_type != 'Unidad' ? '1 '.$store_product->provider_pack_type.' x '.$store_product->provider_pack_quantity.' Unidades' : '1 Unidad';
            $provider_order_detail->base_cost = round($store_product->base_cost, 0, PHP_ROUND_HALF_DOWN);
            $provider_order_detail->cost = $store_product->cost;
            $provider_order_detail->iva = $store_product->iva;
            $provider_order_detail->image_url = $store_product->image_medium_url;
            $provider_order_detail->save();

            //guardar producto agrupado
            if ($store_product->type == 'Proveedor')
            {
                $store_products_group = ProductGroup::select('products.*', 'product_group_id', 'product_group.quantity AS group_quantity')
                                        ->join('products', 'product_group.product_group_id', '=', 'products.id')
                                        ->where('product_group.product_id', $store_product->id)
                                        ->get();
                foreach($store_products_group as $store_product_group)
                {
                    $store_product_obj = StoreProduct::find($store_product_group->store_product_group_id);
                    $provider_order_detail_product_group = new ProviderOrderDetailProductGroup;
                    $provider_order_detail_product_group->provider_order_detail_id = $provider_order_detail->id;
                    $provider_order_detail_product_group->store_product_id = $store_product_group->store_product_group_id;
                    $provider_order_detail_product_group->plu = $store_product_group->provider_plu;
                    $provider_order_detail_product_group->product_name = $store_product_group->name.' '.$store_product_group->quantity.' '.$store_product_group->unit;
                    $provider_order_detail_product_group->product_quantity = $store_product_group->quantity;
                    $provider_order_detail_product_group->product_unit = $store_product_group->unit;
                    $provider_order_detail_product_group->quantity_order = $store_product_group->group_quantity;
                    $provider_order_detail_product_group->ideal_stock = $store_product_group->ideal_stock;
                    $provider_order_detail_product_group->current_stock = $store_product_obj->getCurrentStock($this->warehouse->id);
                    $provider_order_detail_product_group->base_cost = round($store_product_group->base_cost, 0, PHP_ROUND_HALF_DOWN);
                    $provider_order_detail_product_group->cost = $store_product_group->cost;
                    $provider_order_detail_product_group->iva = $store_product_group->iva;
                    $provider_order_detail_product_group->image_url = $store_product_group->image_medium_url;
                    $provider_order_detail_product_group->save();
                }
            }

            //log
            /*Event::fire('reception.addedProduct', [[
                'provider_order_detail' => $provider_order_detail,
                'reception_id' => $id,
                'admin_id' => Session::get('admin_id')
            ]]);*/

            return ['status' => true, 'message' => 'El producto se ha agregado exitosamente.'];
        }
        return ['status' => false, 'message' => 'El producto ya ha sido agregado anteriortmente.'];
    }

    /**
     * Crea recibos de bodega desde la orden de compra
     *
     * @param $invoice_number
     * @param $transporter
     * @param $plate
     * @param $driver_name
     * @param null $driver_document_number
     * @param null $observation
     * @return array
     * @throws Exception
     */
    public function createReception($invoice_number, $transporter, $plate, $driver_name, $driver_document_number = null, $observation = null)
    {
        if ( $this->status == 'Enviada' || $this->status == 'Pendiente' ) {
            //validar que no haya recibos de bodega de la orden de compra en proceso
            if (ProviderOrderReception::where('provider_order_id', $this->id)->where('status', 'En proceso')->first())
                    return ['status' => false, 'message' => 'Hay un recibo de bodega de la orden de compra en proceso.'];

            //validar número de factura
            if (ProviderOrderReception::where('provider_order_id', $this->id)->where('invoice_number', $invoice_number)->first())
                    return ['status' => false, 'message' => 'Ya existe un recibo de bodega con el mismo número de factura en la misma orden de compra.'];

            //obtener cantidad de recibos de bodega de la orden de compra
            $receptions = ProviderOrderReception::where('provider_order_id', $this->id)->where('status', '<>', 'En proceso')->count();
            if (!$receptions){
                //crear detalle con todos los productos
                $products = ProviderOrderDetail::where('provider_order_id', $this->id)->get();
            }else{
                //crear detalle solo con productos no recibidos o parcialmente recibidos
                $products = ProviderOrderReceptionDetail::select(
                                                        'provider_order_reception_details.store_product_id',
                                                        'provider_order_details.product_name',
                                                        'provider_order_details.base_cost',
                                                        'provider_order_details.cost',
                                                        'provider_order_details.iva',
                                                        'provider_order_details.iva_amount',
                                                        'provider_order_details.consumption_tax',
                                                        'provider_order_details.old_cost',
                                                        DB::raw('SUM(provider_order_reception_details.quantity_received) AS quantity_received'),
                                                        'provider_order_details.quantity_order',
                                                        'provider_order_details.id',
                                                        'provider_order_details.type'
                                                    )
                                                    ->join('provider_order_receptions', 'provider_order_receptions.id', '=', 'provider_order_reception_details.reception_id')
                                                    ->join('provider_order_details', function($join){
                                                         $join->on('provider_order_details.provider_order_id', '=', 'provider_order_receptions.provider_order_id');
                                                         $join->on('provider_order_details.store_product_id', '=', 'provider_order_reception_details.store_product_id');
                                                    })
                                                    ->where('provider_order_receptions.provider_order_id', $this->id)
                                                    ->whereIn('provider_order_receptions.status', ['Recibido', 'Almacenado', 'Revisado', 'Contabilizado'])
                                                    ->groupBy('provider_order_reception_details.store_product_id')
                                                    ->havingRaw('quantity_received < quantity_order')
                                                    ->get();
            }

            if ($products->count())
            {
                DB::beginTransaction();
                //crear encabezado
                $provider_order_reception = new ProviderOrderReception();
                $provider_order_reception->provider_order_id = $this->id;
                $provider_order_reception->admin_id = Session::get('admin_id');
                $provider_order_reception->date = date('Y-m-d H:i:s');
                $provider_order_reception->status = 'Iniciado';
                $provider_order_reception->transporter = $transporter;
                $provider_order_reception->plate = $plate;
                $provider_order_reception->driver_name = $driver_name;
                if ( !empty( $driver_document_number ) ) {
                    $provider_order_reception->driver_document_number = $driver_document_number;
                }
                $provider_order_reception->invoice_number = $invoice_number;
                if ( !empty($observation) ) {
                    $provider_order_reception->observation = $observation;
                }
                $provider_order_reception->save();

                //crear detalle
                foreach($products as $product)
                {
                    if ($product->quantity_received < $product->quantity_order)
                    {
                        $reception_detail = new ProviderOrderReceptionDetail();
                        $reception_detail->reception_id = $provider_order_reception->id;
                        $reception_detail->store_product_id = $product->store_product_id;
                        $reception_detail->type = $product->type;
                        $reception_detail->status = 'No recibido';
                        $reception_detail->quantity_received = isset($product->quantity_received) ? $product->quantity_received : 0;
                        $reception_detail->base_cost = $product->base_cost;
                        $reception_detail->cost = $product->cost;
                        $reception_detail->iva = $product->iva;
                        $reception_detail->iva_amount = $product->iva_amount;
                        $reception_detail->consumption_tax = $product->consumption_tax;
                        $reception_detail->old_cost = $product->old_cost;
                        $reception_detail->save();

                        //crear detalle de productos Proveedor
                        if ( $reception_detail->type == 'Proveedor' ) {
                            $order_detail_groups = ProviderOrderDetailProductGroup::where('provider_order_detail_id', $product->id)->get();
                            foreach ($order_detail_groups as $key => $order_detail_group) {
                                $old_reception_detail_group = ProviderOrderReceptionDetailProductGroup::where('reception_detail_id', $reception_detail->id)->where('store_product_id', $order_detail_group->store_product_id)->first();
                                $reception_detail_group = new ProviderOrderReceptionDetailProductGroup;
                                $reception_detail_group->reception_detail_id = $reception_detail->id;
                                $reception_detail_group->store_product_id = $order_detail_group->store_product_id;
                                $reception_detail_group->status = 'No recibido';
                                $reception_detail_group->quantity_received = isset($old_reception_detail_group->quantity_received) ? $old_reception_detail_group->quantity_received : 0;
                                $reception_detail_group->cost = $order_detail_group->cost;
                                $reception_detail_group->base_cost = $order_detail_group->base_cost;
                                $reception_detail_group->iva = $order_detail_group->iva;
                                $reception_detail_group->iva_amount = $order_detail_group->iva_amount;
                                $reception_detail_group->consumption_tax = $order_detail_group->consumption_tax;
                                $reception_detail_group->old_cost = $order_detail_group->old_cost;
                                $reception_detail_group->save();
                            }
                        }
                    }
                }
                DB::commit();
                //log
                Event::fire('reception.created', [[
                    'reception_id' => $provider_order_reception->id,
                    'provider_order_id' => $provider_order_reception->provider_order_id,
                    'admin_id' => Session::get('admin_id')
                ]]);
                $this->status = 'Pendiente';
                $this->save();
                return ['status' => true, 'message' => 'Recibo de bodega generado con éxito.', 'reception_obj' => $provider_order_reception];
            }
            return ['status' => false, 'message' => 'La orden de compra no tiene productos pendientes por recibir en la orden de compra.'];
        }else{
            return ['status' => false, 'message' => 'La orden de compra se encuentra en un estado no válido para recibir productos.'];
        }
    }

    /**
     * Crea un órden de compra
     *
     * @param Warehouse $warehouse
     * @param Provider $provider
     * @param array $products
     * @return ProviderOrder
     * @throws Exception
     */
    public static function createProviderOrder(
        \Warehouse $warehouse,
        \Provider $provider,
        array $products
    ) {
        if (empty($products)) {
            throw new Exception("No se encontraro productos para crear la órden de compra.");
        }

        $provider_order = new ProviderOrder;
        $provider_order->warehouse_id = $warehouse->id;
        $provider_order->city_id = $warehouse->city_id;
        $provider_order->provider_id = $provider->id;
        $provider_order->admin_id = Session::get('admin_id');
        $provider_order->provider_name = $provider->name;
        $provider_order->status = 'Iniciada';
        $provider_order->date = \Carbon\Carbon::now()->toDateTimeString();
        $provider_order->delivery_date = \Carbon\Carbon::now()->addHours($provider->delivery_time_hours)->toDateTimeString();
        $provider_order->first_delivery_date = \Carbon\Carbon::now()->addHours($provider->delivery_time_hours)->toDateTimeString();
        $provider_order->save();

        foreach ($products as $index => $product) {
            $provider_order->addProductToProviderOrder($product['store_product_id'], $product['quantity_unit_to_request']);
        }
        return $provider_order;
    }

    /**
     * Calcula las cantidades solicitadas en una orden de compra.
     *
     * @return mixed
     */
    public function getQuantityRequested()
    {
        $quantity_requested = $this->providerOrderDetails->sum('quantity_order');

        return $quantity_requested;
    }

    public function addProductToProviderOrder($store_product_id, $quantity_to_request)
    {
        $warehouse = $this->warehouse;
        $store_product = StoreProduct::whereHas('storeProductWarehouses', function ($query) use ($warehouse) {
            $query->where('warehouse_id', $warehouse->id);
        })
            ->with([
                'product',
                'storeProductWarehouses' => function ($query) use ($warehouse) {
                    $query->where('warehouse_id', $warehouse->id);
                },
                'storeProductGroup.storeProductGroup' => function ($query) use ($warehouse){
                    $query->with([
                        'product',
                        'storeProductWarehouses' => function ($query) use ($warehouse) {
                            $query->where('warehouse_id', $warehouse->id);
                        }
                    ]);
                }
            ])
            ->where('provider_id', $this->provider_id)
            ->where('id', $store_product_id)
            ->first();

        if (empty($store_product)) {
            throw new Exception("No se agregó el producto debido a: no pertenece al proveedor de la orden de compra o no se encontró en la base de datos.");
        }

        if ($store_product->product->type == 'Agrupado') {
            throw new Exception("El producto {$store_product->product->name} {$store_product->product->quantity} {$store_product->product->unit} es un produco agrupado y no se puede crear órdenes de compra con estos productos.");
        }

        $provider_order_details_found = $this->providerOrderDetails()->where('store_product_id', $store_product->id)->first();
        if (!empty($provider_order_details_found)) {
            throw new Exception("El producto {$store_product->product->name} {$store_product->product->quantity} {$store_product->product->unit} ya se encuentra en la órden de compra.");
        }

        $pack_info_to_request = $store_product->calculateQuantityToOrder($quantity_to_request);
        $provider_order_detail = new ProviderOrderDetail;
        $provider_order_detail->store_product_id = $store_product->id;
        $provider_order_detail->type = $store_product->product->type;
        $provider_order_detail->plu = $store_product->provider_plu;
        $provider_order_detail->reference = $store_product->product->reference;
        $provider_order_detail->product_name = "{$store_product->product->name} {$store_product->product->unit} {$store_product->product->quantity}";
        $provider_order_detail->quantity_order = $pack_info_to_request['quantity_to_request'];
        $provider_order_detail->ideal_stock = $store_product->storeProductWarehouses->first()->ideal_stock;
        $provider_order_detail->quantity_pack = $pack_info_to_request['quantity_to_package_request'];
        $provider_order_detail->pack_description = "1 {$store_product->provider_pack_type} x {$store_product->provider_pack_quantity}";
        $provider_order_detail->base_cost = round($store_product->base_cost, 0, PHP_ROUND_HALF_DOWN);
        $provider_order_detail->cost = $store_product->cost;
        $provider_order_detail->iva = $store_product->iva;
        $provider_order_detail->consumption_tax = $store_product->consumption_tax;
        $provider_order_detail->image_url = $store_product->product->image_medium_url;
        $provider_order_detail->current_stock = $store_product->getCurrentStock($warehouse->id);

        $provider_order_detail = $this->providerOrderDetails()->save($provider_order_detail);

        if ($store_product->product->type == 'Proveedor') {
            $store_product_groups = $store_product->storeProductGroup;
            $children_product_count = $store_product_groups->count();
            $group_details = [];
            foreach ($store_product_groups as $index => $store_product_group) {
                $provider_order_detail_product_group = new ProviderOrderDetailProductGroup;
                $provider_order_detail_product_group->store_product_id = $store_product_group->store_product_group_id;
                $provider_order_detail_product_group->plu = $store_product_group->storeProductGroup->provider_plu;
                $provider_order_detail_product_group->product_name = $store_product_group->storeProductGroup->product->name;
                $provider_order_detail_product_group->product_quantity = $store_product_group->storeProductGroup->product->quantity;
                $provider_order_detail_product_group->product_unit = $store_product_group->storeProductGroup->product->unit;
                $provider_order_detail_product_group->quantity_order = ceil($provider_order_detail->quantity_order / $children_product_count);
                $provider_order_detail_product_group->ideal_stock = $store_product_group->storeProductGroup->storeProductWarehouses->first()->ideal_stock;
                $provider_order_detail_product_group->cost = $store_product_group->storeProductGroup->cost;
                $provider_order_detail_product_group->base_cost = round($store_product_group->storeProductGroup->base_cost, 0, PHP_ROUND_HALF_DOWN);
                $provider_order_detail_product_group->iva = $store_product_group->storeProductGroup->iva;
                $provider_order_detail_product_group->consumption_tax = $store_product_group->storeProductGroup->consumption_tax;
                $provider_order_detail_product_group->image_url = $store_product_group->storeProductGroup->product->image_medium_url;
                $provider_order_detail_product_group->current_Stock = $store_product_group->storeProductGroup->getCurrentStock($warehouse->id);
                $group_details[] = $provider_order_detail_product_group;
            }
            $provider_order_detail->providerOrderDetailProductGroup()->saveMany($group_details);
        }

        return true;
    }
}