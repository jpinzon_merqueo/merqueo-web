<?php
class OrderPickingQualityDetail extends \Illuminate\Database\Eloquent\Model
{
	protected $table = 'order_picking_quality_details';

	/**
	 * Obtener productos de la orden validada
	 * @param int $id identificador de la orden
	 * @param string $validation que puede ser seco o frio según el storage del producto
     * @return array productos validados
	 */
	public function getProductInOrderValidated($id,$validation){
		$productsValidated  = OrderPickingQualityDetail::select(
			'order_picking_quality.order_id',
			'order_picking_quality_details.id AS product_validation_id',
			'order_picking_quality_details.status AS status_product_validation',
			'order_picking_quality_details.status_shrinkage',
			DB::raw('SUM(CASE WHEN order_picking_quality_details.status = "Validado" AND order_picking_quality_details.status_shrinkage IS NULL THEN 1 ELSE 0 END) AS quantity_validated'),
			DB::raw('SUM(CASE WHEN order_picking_quality_details.status = "Trocado" AND order_picking_quality_details.status_shrinkage IS NULL THEN 1 ELSE 0 END) AS quantity_changed'),
			DB::raw('SUM(CASE WHEN order_picking_quality_details.status = "Adicional" AND order_picking_quality_details.status_shrinkage IS NULL THEN 1 ELSE 0 END) AS quantity_additional'),
			DB::raw('SUM(CASE WHEN order_picking_quality_details.status = "Faltante" AND order_picking_quality_details.status_shrinkage IS NULL THEN 1 ELSE 0 END) AS quantity_pending'),
			DB::raw('SUM(CASE WHEN order_picking_quality_details.status = "Validado" AND order_picking_quality_details.status_shrinkage = "Vencido" THEN 1 ELSE 0 END) AS quantity_validated_expired'),
			DB::raw('SUM(CASE WHEN order_picking_quality_details.status = "Trocado" AND order_picking_quality_details.status_shrinkage = "Vencido" THEN 1 ELSE 0 END) AS quantity_changed_expired'),
			DB::raw('SUM(CASE WHEN order_picking_quality_details.status = "Adicional" AND order_picking_quality_details.status_shrinkage = "Vencido" THEN 1 ELSE 0 END) AS quantity_additional_expired'),
			DB::raw('SUM(CASE WHEN order_picking_quality_details.status = "Validado" AND order_picking_quality_details.status_shrinkage = "Mal estado" THEN 1 ELSE 0 END) AS quantity_validated_poor_condition'),
			DB::raw('SUM(CASE WHEN order_picking_quality_details.status = "Trocado" AND order_picking_quality_details.status_shrinkage = "Mal estado" THEN 1 ELSE 0 END) AS quantity_changed_poor_condition'),
			DB::raw('SUM(CASE WHEN order_picking_quality_details.status = "Adicional" AND order_picking_quality_details.status_shrinkage = "Mal estado" THEN 1 ELSE 0 END) AS quantity_additional_poor_condition'),
			'products.reference AS reference',
			'products.name AS product_name',
			'products.quantity AS product_quantity',
			'products.unit AS product_unit',
			'store_products.id AS store_product_id',
			'products.image_medium_url AS product_image_url',
			DB::raw("(SELECT sum(quantity) FROM order_products WHERE order_id = ".$id." AND order_products.store_product_id = order_picking_quality_details.store_product_id) AS quantity"),
			DB::raw("(SELECT sum(quantity) FROM order_product_group WHERE order_id = ".$id." AND order_product_group.store_product_id = order_picking_quality_details.store_product_id) AS quantity_group")
			)
		  ->join('store_products', 'store_product_id', '=', 'store_products.id')
		  ->join('products', 'products.id', '=', 'product_id')
		  ->join('order_picking_quality', 'order_picking_quality.id', '=', 'order_picking_quality_id')
		  ->join('orders', 'orders.id', '=', 'order_picking_quality.order_id')
		  ->where('order_picking_quality.order_id', $id);
		if($validation == 'seco')
			$productsValidated->where('store_products.storage', 'Seco');
		else
			$productsValidated->where('store_products.storage', '<>', 'Seco');

		$productsValidated = $productsValidated->groupBy('store_products.id', 'order_picking_quality_details.status')->get();
		return $productsValidated; 
	}

    /**
     * Buscar producto de un pedido por referencia, por id del producto o por nombre
     * @param int $id identificador del pedido
     * @param int $productId identificador del producto
     * @param string $search  referencia o nombre del producto
     * @param array productos
     */
	public function getProductsInValidation($id, $productId, $search){
		$products = OrderProduct::select('order_products.order_id',
                                            'order_products.reference',
                                            'order_products.store_product_id',
                                            'product_image_url',
                                            'order_products.product_name',
                                            'order_products.fulfilment_status',
                                            'order_products.product_quantity',
                                            'order_products.product_unit',
                                            DB::raw("sum(order_products.quantity) AS quantity"),
                                            'order_picking_quality.id AS order_picking_quality_id',
                                            'order_picking_quality_details.id AS product_validation_id',
                                            'order_products.price',
                                            'order_products.type'
                                            )
                                            ->join('orders', 'orders.id', '=', 'order_products.order_id')
                                            ->join('store_products', 'order_products.store_product_id', '=', 'store_products.id')
                                            ->leftJoin('order_picking_quality', 'orders.id', '=', 'order_picking_quality.order_id')
                                            ->leftJoin('order_picking_quality_details', function($join){
                                                $join->on('order_picking_quality_details.order_picking_quality_id', '=', 'order_picking_quality.id');
                                                $join->on('store_products.id', '=', 'order_picking_quality_details.store_product_id');
                                            })
                                            ->where('orders.id', $id)
                                            ->where('order_products.type', 'Product')
                                            ->whereIn('order_products.fulfilment_status', ['Fullfilled', 'Not Available']);

            if($productId){
                $products->where('order_products.store_product_id', $productId);
            }else{
                $products->where(function($query) use ($search){
                $query->where('order_products.reference', 'LIKE', '%'.$search.'%');
                $query->orWhere('order_products.product_name', 'LIKE', '%'.$search.'%');
                });
            }
            $products = $products->groupBy('order_products.store_product_id')->get();
        return $products;   
	}

    /** 
     * Buscar productos agrupados de un pedido
     * @param int $id identificador de la orden
     * @param int $productId identificador del producto
     * @param string $search referencia o nombre del producto
     * @return array productos agrupados
    */
	public function getProductGroupsInValidation($id, $productId, $search){
		$productGroups = OrderProductGroup::select('order_product_group.order_id',
                                                       'order_product_group.reference',
                                                       'order_product_group.store_product_id',
                                                       'order_product_group.fulfilment_status',
                                                       'product_image_url', 'order_product_group.product_name',
                                                       DB::raw("count('order_product_group.quantity') AS quantity"),
                                                       'order_product_group.product_unit',
                                                       'order_product_group.quantity',
                                                       'order_picking_quality.id AS order_picking_quality_id',
                                                       'order_picking_quality_details.id AS product_validation_id',
                                                       'order_product_group.price')
                                                      ->join('orders', 'orders.id', '=', 'order_product_group.order_id')
                                                      ->join('store_products', 'order_product_group.store_product_id', '=', 'store_products.id')
                                                      ->leftJoin('order_picking_quality', 'orders.id', '=', 'order_picking_quality.order_id')
                                                      ->leftJoin('order_picking_quality_details', function($join){
                                                            $join->on('order_picking_quality_details.order_picking_quality_id', '=', 'order_picking_quality.id');
                                                            $join->on('store_products.id', '=', 'order_picking_quality_details.store_product_id');
                                                      })
                                                      ->whereIn('order_product_group.fulfilment_status', ['Fullfilled', 'Not Available'])
                                                      ->where('orders.id', $id);
        if($productId){
            $productGroups->where('order_product_group.store_product_id', $productId);
        }else{
            $productGroups->where(function($query) use ($search){
                $query->where('order_product_group.reference', 'LIKE', '%'.$search.'%');
                $query->orWhere('order_product_group.product_name', 'LIKE', '%'.$search.'%');
            });
        }
        $productGroups = $productGroups->groupBy('order_product_group.store_product_id')->get();
        return $productGroups;
    }
    
    /**
     * Obtener la cantidad de producto para un pedido
     * @param object $order pedido al que se le está haciendo calidad
     * @param int $orderPickingQualityId identificador de calidad de picking de un pedido
     * @param string $search referencia o nombre del producto
     * @return array de productos adicionales en un pedido
     */
    public function getAditionalProductsForQuality($order,$orderPickingQualityId,$search){
        $additionalProducts = StoreProduct::join('products', 'products.id', '=', 'store_products.product_id')
                                                ->leftJoin('order_products', function($join) use ($order){
                                                    $join->on('store_products.id', '=', 'order_products.store_product_id');
                                                    $join->where('order_products.order_id', '=', $order->id);
                                                })
                                                ->leftJoin('order_picking_quality_details', function($join) use ($orderPickingQualityId){
                                                    $join->on('store_products.id', '=', 'order_picking_quality_details.store_product_id');
                                                    $join->where('order_picking_quality_id', '=', $orderPickingQualityId);
                                                })
                                                ->where('store_products.store_id', $order->store_id)
                                                ->select('products.quantity AS product_quantity', 'products.unit AS product_unit',
                                                'order_picking_quality_id',
                                                'order_picking_quality_details.id AS product_validation_id',
                                                'store_products.id AS store_product_id',
                                                'order_products.quantity',
                                                'order_products.fulfilment_status',
                                                'products.reference AS reference',
                                                'products.name AS product_name',
                                                'image_medium_url AS product_image_url');
            $additionalProducts->where(function($query) use ($search){
                $query->where('products.reference', 'LIKE', '%'.$search.'%');
                $query->orWhere('products.name', 'LIKE', '%'.$search.'%');
            });
        
        return $additionalProducts; 
    }

    /**
     * Obtener productos de una orden para actualizar el estado de validación de calidad
     * @param object $order pedido al que se le está haciendo calidad
     * @param string $vallidation tipo de producto seco o frio
     * @param array productos
     */
    public function getProductsToUpdateValidate($order, $validation){
        $products = OrderProduct::lockforUpdate()->select('order_products.id',
                                         'order_products.order_id',
                                         'order_products.store_product_id',
                                         'order_products.type',
                                         'order_products.product_name',
                                         'order_products.product_unit',
                                         'order_products.product_quantity',
                                         DB::raw("(
                                                SELECT SUM(op.quantity)
                                                from order_products op
                                                WHERE op.store_product_id = order_products.store_product_id AND op.order_id = $order->id
                                            )
                                                 AS quantity "),
                                         'order_picking_quality.id AS order_picking_quality_id',
                                         'order_picking_quality_details.id AS product_validation_id',
                                         'order_picking_quality_details.status AS status_product_validation',
                                         'order_picking_quality_details.status_shrinkage AS status_shrinkage',
                                         DB::raw("
                                            (
                                                SELECT COUNT('order_picking_quality_details.store_product_id')
                                                from order_picking_quality_details opd
                                                INNER JOIN order_picking_quality opq ON opq.id = order_picking_quality_id
                                                WHERE opd.store_product_id = order_products.store_product_id AND opq.order_id = $order->id
                                            )
                                                 AS quantity_product_validated")
                                         )
                                ->join('orders', 'order_products.order_id', '=', 'orders.id')
                                ->join('store_products', 'order_products.store_product_id', '=', 'store_products.id')
                                ->join('products', 'store_products.product_id', '=', 'products.id')
                                ->leftJoin('order_picking_quality',function ($join) use ($order){
                                    $join->on('order_picking_quality.order_id', '=', 'orders.id');
                                    $join->where('order_picking_quality.order_id', '=', $order->id);
                                })
                                ->leftJoin('order_picking_quality_details', function($join){
                                    $join->on('order_picking_quality_id', '=', 'order_picking_quality.id');
                                    $join->on('store_products.id', '=', 'order_picking_quality_details.store_product_id');
                                    $join->on('order_products.store_product_id', '=', 'order_picking_quality_details.store_product_id');
                                    $join->where('order_picking_quality_details.type', '=', 'Product');
                                })
                                ->orderBy('department_id', 'desc')
                                ->orderBy('order_products.product_name', 'asc')
                                ->where('order_products.order_id', $order->id)
                                ->where('order_products.type', 'Product')
                                ->where('order_products.fulfilment_status', 'Fullfilled');

        if($validation == 'seco')
            $products->where('store_products.storage', 'Seco');
        else
            $products->where('store_products.storage', '<>', 'Seco');

        $products = $products->groupBy('order_products.store_product_id')->get();
        return $products;
    }

    /**
     * Obtener productos agrupados de una orden para actualizar el estado de validación de calidad
     * @param object $order pedido al que se le está haciendo calidad
     * @param string $vallidation tipo de validación seco o frío
     * @param array productos agrupados
     */
    public function getProductGroupToUpdateValidate($order, $validation){
        $productGroups = OrderProductGroup::lockforUpdate()->select('order_product_group.id',
                                                   'order_product_group.order_id',
                                                   'order_product_group.store_product_id',
                                                   'order_products.type',
                                                   'order_product_group.product_name',
                                                   'order_product_group.product_unit',
                                                   'order_product_group.product_quantity',
                                                   DB::raw("(
                                                        SELECT SUM(opg.quantity)
                                                        from order_product_group opg
                                                        WHERE opg.store_product_id = order_product_group.store_product_id AND opg.order_id = ".$order->id."
                                                    )
                                                         AS quantity "),
                                                   'order_picking_quality.id AS order_picking_quality_id',
                                                   'order_picking_quality_details.id AS product_validation_id',
                                                   'order_picking_quality_details.status AS status_product_validation',
                                                   'order_picking_quality_details.status_shrinkage AS status_shrinkage',
                                                   DB::raw("
                                                            (
                                                                SELECT COUNT('order_picking_quality_details.store_product_id')
                                                                from order_picking_quality_details opd
                                                                INNER JOIN order_picking_quality opq ON opq.id = order_picking_quality_id
                                                                WHERE opd.store_product_id = order_product_group.store_product_id AND opq.order_id = $order->id
                                                            )
                                                                 AS quantity_product_validated ")
                                               )
                                            ->join('order_products', 'order_products.id', '=', 'order_product_group.order_product_id')
                                            ->join('orders', 'order_products.order_id', '=', 'orders.id')
                                            ->join('store_products', 'order_product_group.store_product_id', '=', 'store_products.id')
                                            ->join('products', 'store_products.product_id', '=', 'products.id')
                                            ->leftJoin('order_picking_quality',function ($join) use ($order){
                                                $join->on('order_picking_quality.order_id', '=', 'orders.id');
                                                $join->where('order_picking_quality.order_id', '=', $order->id);
                                            })
                                            ->leftJoin('order_picking_quality_details', function($join){
                                                $join->on('order_picking_quality_id', '=', 'order_picking_quality.id');
                                                $join->on('store_products.id', '=', 'order_picking_quality_details.store_product_id');
                                                $join->where('order_picking_quality_details.type', '=', 'Agrupado');
                                            })
                                            ->where('order_product_group.order_id', $order->id)
                                            ->where('order_product_group.fulfilment_status', 'Fullfilled');
        if($validation == 'seco')
            $productGroups->where('store_products.storage', 'Seco');
        else
            $productGroups->where('store_products.storage', '<>', 'Seco');

        $productGroups = $productGroups->groupBy('order_product_group.store_product_id')->get();
        return $productGroups;
    }
    
    /**
     * Obtener los productos validados de una orden
     * @param int $orderId identificador de la orden
     * @return array productos
     */
    public function getTotalProductsValidatedInOrder($orderId){
        return OrderPickingQualityDetail::select(
            'order_picking_quality.order_id',
            'order_picking_quality_details.id AS product_validation_id',
            'order_picking_quality_details.status AS status_product_validation',
            'order_picking_quality_details.status_shrinkage AS status_shrinkage',
            'products.reference AS reference',
            'products.name AS product_name',
            'products.quantity AS product_quantity',
            'products.unit AS product_unit',
            'store_products.id AS store_product_id',
            DB::raw('COUNT(store_product_id) AS quantity_product_validated')
            )
          ->join('store_products', 'store_product_id', '=', 'store_products.id')
          ->join('products', 'products.id', '=', 'product_id')
          ->join('order_picking_quality', 'order_picking_quality.id', '=', 'order_picking_quality_id')
          ->where('order_picking_quality.order_id', $orderId);
    }
}
?>