<?php

class Shopper extends \Illuminate\Database\Eloquent\Model
{

    protected $table = 'shoppers';

    public function zonerel()
    {
    	return $this->belongsToMany('Zone', 'shopper_zones', 'shopper_id', 'zone_id');
    }

    /**
     * Obtener saldos de shopper
     *
     * @param int $shopper_id ID shopper
     * @return array $balance Datos de saldos de shopper
     */
    public static function getBalance($shopper_id)
    {
        $balance['shopper'] = ShopperMovement::where('shopper_id', $shopper_id)->where('status', 1)->sum('shopper_balance');
        $balance['card'] = ShopperMovement::where('shopper_id', $shopper_id)->where('status', 1)->sum('card_balance');
        return $balance;
    }

    /**
     * Obtiene shopper con calificacion
     *
     * @param array $id ID del shopper
     * @return array $shopper Datos de shopper
     */
    public static function getShopper($id)
    {
        $shopper = Shopper::find($id);
        if ($shopper){
            $result = Order::selectRaw('(SUM(user_score) / COUNT(id)) AS average')->where('shopper_id', $shopper->id)
            ->where('status', 'Delivered')->where('user_score', '>', 0)->groupBy('shopper_id')->first();
            $shopper->score_average = $result ? round($result->average, 1) : 5;
            if ($shopper->score_average == 0) $shopper->score_average_image = 'score_0.png';
            else if ($shopper->score_average < 1) $shopper->score_average_image = 'score_0x.png';
            else if ($shopper->score_average == 1) $shopper->score_average_image = 'score_1.png';
            else if ($shopper->score_average > 1 && $shopper->score_average < 2) $shopper->score_average_image = 'score_1x.png';
            else if ($shopper->score_average == 2) $shopper->score_average_image = 'score_2.png';
            else if ($shopper->score_average > 2 && $shopper->score_average < 3) $shopper->score_average_image = 'score_2x.png';
            else if ($shopper->score_average == 3) $shopper->score_average_image = 'score_3.png';
            else if ($shopper->score_average > 3 && $shopper->score_average < 4) $shopper->score_average_image = 'score_3x.png';
            else if ($shopper->score_average == 4) $shopper->score_average_image = 'score_4.png';
            else if ($shopper->score_average > 4 && $shopper->score_average < 5) $shopper->score_average_image = 'score_4x.png';
            else $shopper->score_average_image = 'score_5.png';
            $shopper->score_average = number_format($shopper->score_average, 1, '.', ',');

            //shopper de servicio al cliente
            if ($shopper->email == 'shoppertemporal@merqueo.com'){
                $shopper->first_name = 'Alejandro';
                $shopper->last_name = 'de Merqueo';
                $shopper->score_average = '5.0';
                $shopper->score_average_image = 'score_5.png';
            }

            return $shopper;
        }

        return false;
    }

}