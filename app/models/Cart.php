<?php

use Illuminate\Database\Eloquent\Model;

/**
 * @property CartProduct[] $products
 * @property User $user
 * @property int user_id
 */
class Cart extends Model {

    protected $table = 'carts';

    public $total = null;
    public $specialPrice = null;

    /**
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function warehouse()
    {
        return $this->belongsTo('Warehouse', 'warehouse_id');
    }

    /**
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('User', 'user_id');
    }

    /**
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany('CartProduct', 'cart_id');
    }

    public function cartProducts()
    {
        return $this->hasMany('CartProduct')
            ->leftJoin('store_products', 'cart_products.store_product_id', '=', 'store_products.id')
            ->leftJoin('products', 'store_products.product_id', '=', 'products.id')
            ->leftJoin('users', 'cart_products.added_by', '=', 'users.id')
            ->select(
                'products.*', 'store_products.store_id', 'store_products.price', 'store_products.special_price',
                'cart_products.quantity AS cart_quantity', 'cart_products.product_name',
                'cart_products.comment AS cart_comment', 'cart_products.store_id AS cart_store_id',
                'cart_products.store_product_id AS product_id', 'users.first_name AS added_by'
            )
            ->orderBy('store_products.store_id');
    }

    /**
     * Devuelve el total del carrito
     *
     * @return double Total
     */
    public function getTotalAttribute()
    {
        if (!isset($this->total)) {
            $this->getTotals();
        }

        return $this->total;
    }

    /**
     * Devuelve el descuento de precios especiales del carrito
     *
     * @return double Descuento
     */
    public function getSpecialPriceAttribute()
    {
        if (!isset($this->specialPrice)) {
            $this->getTotals();
        }

        return $this->specialPrice;
    }

    /**
     * Devuelve una tienda asociada al carrito
     *
     * @return Store Tienda
     */
    public function getStoreAttribute()
    {
        $product = $this->cartProducts->last();

        if ($product) {
            return $product->store;
        } else {
            return new Store();
        }
    }

    /**
     * Calcular totales
     *
     * @return void
     */
    private function getTotals()
    {
        $this->specialPrice = 0;
        $this->total = 0;

        foreach ($this->cartProducts as $cart_product) {
            $product = $cart_product->product;

            if ($cart_product->special_price > -1) {
                $price = $cart_product->special_price;
                $this->specialPrice += ($cart_product->price - $price) * $cart_product->cart_quantity;
            }else{
                $price = $cart_product->price;
            }

            $this->total += $cart_product->cart_quantity * $price;
        }
    }

    /**
     * Limpia los productos del carrito que no se encuentren en la bodega actual.
     *
     * @param Warehouse $warehouse
     * @param StdClass $cart Objeto plano con los datos del carrito.
     * @return array ['cart', 'removed_products']
     */
    public static function sanitizeByWarehouse(Warehouse $warehouse, StdClass $cart)
    {
        $removed_products = [];
        $product_ids = array_keys(get_object_vars($cart));
        $store_products = StoreProductWarehouse::select(
                'store_product_warehouses.store_product_id',
                'store_product_warehouses.current_stock',
                'store_product_warehouses.is_visible_stock',
                'store_product_warehouses.manage_stock',
                'store_products.product_id'
            )
            ->join('store_products', 'store_products.id', '=', 'store_product_warehouses.store_product_id')
            ->available()
            ->whereIn('store_product_id', $product_ids)
            ->where('store_product_warehouses.warehouse_id', $warehouse->id)
            ->orderBy('store_product_warehouses.store_product_id', 'ASC')
            ->get();

        foreach (get_object_vars($cart) as $product_id => $product) {
            if (empty($product->qty) || empty($product->price) || !intval($product_id)) {
                continue;
            }

            $warehouse_product = binary_search($product_id, $store_products, 'store_product_id');

            if (empty($warehouse_product)) {
                $removed_products[] = self::normalizeRemovedStoreProduct($product, $product_id);
                continue;
            }

            $items_left = $warehouse_product->current_stock;

            if (!$warehouse_product->manage_stock || $items_left >= $product->qty || $warehouse_product->is_visible_stock) {
                continue;
            }

            $removed_products[] = self::normalizeRemovedStoreProduct($product, $product_id);
        }

        return $removed_products;
    }

    /**
     * Retonar el producto con la información del detalle del producto.
     *
     * @param StdClass $product Item del carrito
     * @param int $store_product_id Identificador del store_product
     * @return StdClass
     */
    private static function normalizeRemovedStoreProduct(StdClass $product, $store_product_id)
    {
        $product_details = StoreProduct::with('product')->find($store_product_id) ?: new StoreProduct();
        $product->id = $store_product_id;

        if (!intval($product->price)) {
            $product->price = $product_details->special_price ?: $product_details->price;
        }

        if (!empty($product_details->product)) {
            $product_details = $product_details->product;
            $product->image_app_url = $product_details->image_app_url;
            $product->image_medium_url = $product_details->image_medium_url;
            $product->image_large_url = $product_details->image_large_url;
            $product->name = $product_details->name;
            $product->full_name = $product_details->fullName;
            $product->unit = $product_details->unit;
            $product->quantity = $product_details->quantity;
        }

        return $product;
    }
}
