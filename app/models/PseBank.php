<?php

/**
 * Class PseBank.
 * 
 * @author Johan Alvarez <jalvarez@merqueo.com>
 */
class PseBank extends \Illuminate\Database\Eloquent\Model {

    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'description', 'pseCode'
    ];

}