<?php

use exceptions\MerqueoException;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $storage_position
 * @property int $storage_height_position
 * @property int $minimum_picking_stock
 * @property int $maximum_picking_stock
 * @property bool $manage_stock
 * @property bool $is_visible_stock
 * @property int $ideal_stock
 * @property int $minimum_stock
 * @property int $reception_stock
 * @property int $picking_stock
 * @property int $picked_stock
 * @property int $return_stock
 * @property int $shrinkage_stock
 * @property bool $status
 * @property int $store_product_id
 * @property int $warehouse_id
 * @property StoreProduct $storeProduct
 */
class StoreProductWarehouse extends Model
{
    protected $table = 'store_product_warehouses';

    const PICKING_STOCK = 'picking_stock';

    const CURRENT_STOCK = 'current_stock';

    /**
     * {@inheritdoc}
     */
    protected $fillable = [
        'store_product_id',
        'warehouse_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function storeProduct()
    {
        return $this->belongsTo('StoreProduct', 'store_product_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function warehouse()
    {
        return $this->belongsTo('Warehouse', 'warehouse_id');
    }

    /**
     * Al guardar el objeto
     */
    public static function boot()
    {
        parent::boot();

        static::created(function ($store_product_warehouse)
        {
            if (Config::get('app.aws.elasticsearch.is_enable')){
                $aws_elasticsearch = new AWSElasticsearch();
                $aws_elasticsearch->update_products(['store_product_warehouse_id' => $store_product_warehouse->id]);
            }
        });

        static::updated(function ($store_product_warehouse) {
            if ($store_product_warehouse->isDirty()) {
                $dirty_data = $store_product_warehouse->getDirty();
                $original_data = $store_product_warehouse->getOriginal();

                if (isset($dirty_data['reception_stock']) || isset($dirty_data['picking_stock'])
                    || isset($dirty_data['picked_stock']) || isset($dirty_data['return_stock'])) {
                    StoreProductWarehouse::validateStockStatus($store_product_warehouse);
                }

		        if (Config::get('app.aws.elasticsearch.is_enable') &&
                    (isset($dirty_data['is_visible_stock']) || isset($dirty_data['current_stock']) || isset($dirty_data['is_visible'])
                    || isset($dirty_data['status']) || isset($dirty_data['manage_stock']))) {
                    $aws_elasticsearch = new AWSElasticsearch();
                    $aws_elasticsearch->update_products(['store_product_warehouse_id' => $store_product_warehouse->id]);
                }
            }

            $request = get_current_request();
            $methods = ['products_committed_current_stock', 'save_store_product_warehouse_ajax', 'import', 'createMassiveCountingAjax', 'process_storage_ajax','transferProductsPicking'];
            if (isset($dirty_data) && in_array($request['action'], $methods))
            {
                $original = $dirty = [];
                foreach ($dirty_data as $field => $value) {
                    $dirty[$field] = $value;
                    $original[$field] = $original_data[$field];
                    if ($field == 'status')
                        $status_has_changed = true;
                }
                $changes = ['original' => $original, 'changes' => $dirty, 'url' => $request];
                $store_product_warehouse_log = new StoreProductWarehouseLog;
                if (Session::has('admin_id'))
                    $store_product_warehouse_log->admin_id = Session::get('admin_id');

                $store_product_warehouse_log->store_product_warehouse_id = $store_product_warehouse->id;
                $store_product_warehouse_log->fields = json_encode($changes);

                //si el estado cambio
                if (isset($status_has_changed)) {
                    $store_product = StoreProduct::find($store_product_warehouse->store_product_id);
                    $store_product_warehouse_log->reception_stock = $store_product_warehouse->reception_stock;
                    $store_product_warehouse_log->picking_stock = $store_product_warehouse->picking_stock;
                    $store_product_warehouse_log->picked_stock = $store_product_warehouse->picked_stock;
                    $store_product_warehouse_log->storage_stock = $store_product->getStorageStock($store_product_warehouse->warehouse_id);
                    $store_product_warehouse_log->return_stock = $store_product_warehouse->return_stock;
                    $store_product_warehouse_log->current_stock = $store_product->getCurrentStock($store_product_warehouse->warehouse_id);
                    $store_product_warehouse_log->committed_stock = $store_product->getCommittedStock($store_product_warehouse->warehouse_id);
                }

                $store_product_warehouse_log->save();
            } else {
                if (Config::get('app.log.store_product_warehouse_log')) {
                    $original = $dirty = [];
                    foreach ($dirty_data as $field => $value) {
                        $dirty[$field] = $value;
                        $original[$field] = $original_data[$field];
                    }
                    $changes = ['original' => $original, 'changes' => $dirty, 'url' => $request];
                    $store_product_warehouse_log = new StoreProductWarehouseLog;
                    if (Session::has('admin_id')) {
                        $store_product_warehouse_log->admin_id = Session::get('admin_id');
                    }

                    $store_product_warehouse_log->store_product_warehouse_id = $store_product_warehouse->id;
                    $store_product_warehouse_log->fields = json_encode($changes);
                    $store_product_warehouse_log->save();
                }
            }
        });
    }

    /**
     * Activa o inactiva producto y actualiza stock actual
     *
     * @param $store_product_warehouse StoreProductWarehouse
     */
    public static function validateStockStatus($store_product_warehouse)
    {
        $warehouse = Warehouse::find($store_product_warehouse->warehouse_id);

        $store_product = StoreProduct::with(['warehouseStorages' => function ($query) use ($store_product_warehouse) {
            $query->where('warehouse_id', $store_product_warehouse->warehouse_id);
        }])->find($store_product_warehouse->store_product_id);

        // Actualizar stock actual
        $store_product_warehouse = StoreProductWarehouse::find($store_product_warehouse->id);
        $current_stock = $store_product_warehouse->picking_stock+$store_product->warehouseStorages->sum('quantity');
        $store_product_warehouse->current_stock = $current_stock;
        $store_product_warehouse->save();

        // Obtener cantidad comprometida del producto
        $committed_stock = $store_product->getCommittedStock($warehouse->id);
        // Obtener cantidad en solicitada del producto

        // Desactivar producto si esta comprometido
        if ($store_product_warehouse->status
            && !$store_product_warehouse->is_visible_stock
            && $committed_stock >= $current_stock) {
            $store_product_warehouse->status = 0;
            $store_product_warehouse->save();
            AdminLog::saveLog('store_product_warehouses', $store_product_warehouse->id, 'update_cron_inactive');
            // Verificar si hay que desactivar banner y combo de producto activo
            $store_product_warehouse->disable();

            return $store_product_warehouse;
        }

        // Activar producto
        if (!$store_product_warehouse->status) {
            // Si lo actual es mayor a lo comprometido
            if ($current_stock > $committed_stock) {
                $store_product_warehouse->enable();
            } else {
                // Verificar si hay que desactivar banner y combo de producto inactivo
                $store_product_warehouse->disable();
            }
        }

        return $store_product_warehouse;
    }

    /**
     * Activa el store_product, si es un combo activa los productos relacionados
     * y activa los banners por bodega que pueden ser mostrados.
     */
    public function enable()
    {
        $this->status = 1;
        $this->save();
        AdminLog::saveLog('store_product_warehouses', $this->id, 'update_cron_active');

        $this->toggleRelatedStoreProducts();
        $this->enableRelatedBanners();
    }

    /**
     * Desactiva el store_product, si es un combo desactiva los productos relacionados
     * y desactiva los banners por bodega que pueden ser mostrados.
     */
    public function disable()
    {
        $this->toggleRelatedStoreProducts();
        $this->disableBannerRelated();
    }

    /**
     * Activa los banners relacionados al store_product_warehouse.
     */
    private function enableRelatedBanners()
    {
        $banners = Banner::storeProductWarehouse($this, true)
            ->validDateRange()
            ->with(['warehouses' => function ($query) {
                $query->where('warehouse_id', $this->warehouse_id);
            }])
            ->get();

        foreach ($banners as $banner) {
            if ($banner->warehouses->count()) {
                $banner->status = 1;

                if ($banner->isDirty('status')) {
                    $banner->save();
                }
            }
        }
    }

    /**
     * Activa los productos relacionados al StoreProductWarehouse.
     */
    public function toggleRelatedStoreProducts()
    {
        $store_product_groups = $this->storeProduct->storeProductGroupChildren()
            ->with(['storeProduct.storeProductWarehouses' => function ($query) {
                $query->where('warehouse_id', $this->warehouse_id);
            }])
            ->get();

        foreach ($store_product_groups as $store_product_group) {
            $combo = $store_product_group->storeProduct;
            if (!$combo->storeProductWarehouses->count()) {
                continue;
            }

            // Es necesario saber cuantos productos activos en la tienda tiene el combo.
            $products_available = $combo->storeProductGroup()
                ->whereHas('storeProductGroup', function ($query) {
                    $query->whereHas('storeProductWarehouses', function ($query) {
                        $query->available(false)
                            ->where('warehouse_id', $this->warehouse_id);
                    });
                })
                ->count();

            // Debido a que el filtro se realiza por el producto en bodega
            // siempre se obtendra una coleción con un item.
            $single_product_warehouse = $combo->storeProductWarehouses[0];
            $single_product_warehouse->status = $products_available === $combo->storeProductGroup()->count() ? 1 : 0;
            if ($single_product_warehouse->isDirty('status')) {
                $single_product_warehouse->save();
            }
        }
    }

    /**
     * Desactiva las banners relacionado a un StoreProductWarehouse.
     */
    public function disableBannerRelated()
    {
        $banners = Banner::storeProductWarehouse($this, false)
            ->get();

        foreach ($banners as $banner) {
            $storeProductsId = $banner->getRelatedStoreProductsId();
            $availableAt = StoreProductWarehouse::whereIn('store_product_id', $storeProductsId)
                ->available(true)
                ->count();

            if ($availableAt === 0) {
                $banner->status = 0;
                $banner->save();
                AdminLog::saveLog('banners', $banner->id, 'update_cron_inactive');
            }
        }
    }

    /**
     * @param $query
     * @param bool $needToBeVisible
     * @return mixed
     */
    public function scopeAvailable($query, $needToBeVisible = true)
    {
        $query->where('store_product_warehouses.status', 1)
            ->whereRaw('IF(store_product_warehouses.manage_stock = 1,
                IF(store_product_warehouses.is_visible_stock = 0,
                    IF( store_product_warehouses.current_stock > 0, TRUE, FALSE),
                 TRUE),
              TRUE)'
            );

        if ($needToBeVisible) {
            $query->where('store_product_warehouses.is_visible', 1);
        }

        return $query;
    }

    /**
     * Obtener objeto por bodega y producto
     *
     * @param int $warehouseId ID de la bodega
     * @param int $storeProductId ID del producto en tienda
     * @return StoreProductWarehouse
     */
    public static function getStoreProductWarehouse($warehouseId, $storeProductId)
    {
        return StoreProductWarehouse::where('store_product_warehouses.warehouse_id', $warehouseId)
            ->where('store_product_warehouses.store_product_id', $storeProductId)
            ->first();
    }

    /**
     * Obtiene el objeto de bodega bloqueando el registro para actualización.
     *
     * @param $warehouseId
     * @param $storeProductId
     * @return StoreProductWarehouse
     */
    public static function getStoreProductWarehouseWithLocking($warehouseId, $storeProductId)
    {
        return StoreProductWarehouse::where('store_product_warehouses.warehouse_id', $warehouseId)
            ->where('store_product_warehouses.store_product_id', $storeProductId)
            ->lockForUpdate()
            ->first();
    }

    /**
     * Obtiene los objetos de bodega bloqueando el registro para actualización.
     *
     * @param $warehouseId
     * @param array $storeProductIds
     * @return StoreProductWarehouse
     */
    public static function getStoreProductsWarehouseWithLocking($warehouseId, $storeProductIds)
    {
        return StoreProductWarehouse::where('store_product_warehouses.warehouse_id', $warehouseId)
            ->whereIn('store_product_warehouses.store_product_id', $storeProductIds)
            ->lockForUpdate()
            ->get();
    }

    /**
     * Obtener productos inactivos por bodega
     *
     * @param int $warehouse_id ID de la bodega
     * @return array Productos inactivos
     */
    public static function getNotVisibleProducts($warehouse_id, $type)
    {
        $date = new DateTime();
        $date->setTimezone(new DateTimeZone('America/Bogota'));
        $date->modify('-7 days');
        $qty_all_products =  self::select('store_products.id')
                        ->join('store_products', 'store_product_warehouses.store_product_id', '=', 'store_products.id')
                        ->join('products', 'products.id', '=', 'store_products.product_id')
                        ->where('products.type', 'Simple')
                        ->where('store_product_warehouses.discontinued', 0)
                        ->where('store_products.provider_id', '<>', 46)
                        ->whereNull('store_products.allied_store_id')
                        ->where('store_product_warehouses.warehouse_id', $warehouse_id)
                        ->whereNotNull('store_product_warehouses.first_reception_date')
                        ->count();

        $product_associated = Product::where('products.provider_type_product', 'Asociado')
            ->join('store_products', 'products.id', '=', 'store_products.product_id')
            ->join('store_product_group', 'store_products.id', '=', 'store_product_group.store_product_id')
            ->join('store_product_warehouses', 'store_products.id', '=', 'store_product_warehouses.store_product_id')
            ->where('store_product_warehouses.warehouse_id', $warehouse_id)
            ->select('products.id AS products_id', 'store_product_group.store_product_group_id AS store_product_group_id')
            ->get();

        $product_associated = $product_associated->toArray();

        $products_to_analiz = [];
        foreach ($product_associated as $product) {
            $products_to_analiz[$product['products_id']][] = $product['store_product_group_id'];
        }
        $avoid_products = [];
        foreach ($products_to_analiz as $product) {
            $test = false;
            foreach ($product as $prod) {
                $store_product = StoreProduct::find($prod);
                $current_stock = $store_product->getCurrentStock($warehouse_id);
                $commited_stock = $store_product->getCommittedStock($warehouse_id);
                if ($current_stock <= $commited_stock) {
                    $test |= true;
                }
            }
            if ($test) {
                foreach ($product as $prod) {
                    $avoid_products[] = $prod;
                }
            }
        }

        $products =  self::select(
            'store_product_warehouses.status',
            DB::raw("CONCAT_WS(' ', products.name, products.quantity, products.unit) AS product_name"),
            'products.reference',
            'products.type',
            'products.image_small_url AS image',
            'store_products.id AS store_product_id',
            'store_product_warehouses.id',
            DB::raw('SUM(order_products.quantity) AS quantity_sale')
        )
                        ->join('store_products', 'store_product_warehouses.store_product_id', '=', 'store_products.id')
                        ->join('products', 'products.id', '=', 'store_products.product_id')
                        ->leftjoin('order_products', function ($join) {
                            $join->on('order_products.store_product_id', '=', 'store_products.id')
                                 ->where('order_products.fulfilment_status', '=', 'Fullfilled');
                        })
                        ->leftjoin('orders', function ($join) use ($date) {
                            $join->on('order_products.order_id', '=', 'orders.id')
                                ->where('orders.status', '=', 'Delivered')
                                ->where(DB::raw('DATE(orders.delivery_date)'), '>=', $date->format('Y-m-d'));
                        })
                        ->leftjoin('order_groups', function ($join) {
                            $join->on('order_groups.id', '=', 'orders.group_id')
                                 ->on('order_groups.warehouse_id', '=', 'store_product_warehouses.warehouse_id');
                        })
                        ->where('store_products.provider_id', '<>', 46)
                        ->where('store_product_warehouses.discontinued', 0)
                        ->where('store_product_warehouses.warehouse_id', $warehouse_id)
                        ->whereNotNull('store_product_warehouses.first_reception_date')
                        ->where('products.type', $type)
                        ->whereNull('store_products.allied_store_id')
                        ->where(function($query){
                            $query->where('store_product_warehouses.status', 0)
                                ->orWhereRaw('store_product_warehouses.current_stock <= 0 AND store_product_warehouses.status = 1');
                        })
                        ->groupBy('store_product_warehouses.id')
                        ->orderBy('quantity_sale', 'DESC')
                        ->get();

        $qty_not_visibles = 0;
        if (count($products) > 0) {
            foreach ($products as $key => $product) {
                if ($product->type == 'Simple') {
                    $qty_not_visibles++;
                }

                $log = StoreProductWarehouseLog::select('store_product_warehouse_log.*', DB::raw("IFNULL(admin.fullname, 'Sistema') AS admin_fullname"))
                    ->leftJoin('admin', 'admin.id', '=', 'store_product_warehouse_log.admin_id')
                    ->where('store_product_warehouse_log.store_product_warehouse_id', $product->id)
                    ->where(function ($query) {
                        $query->where('fields', 'LIKE', '%changes":{"status":0%')
                                ->orWhere('fields', 'LIKE', '%"changes":{"status":"0"%');
                    })
                    ->orderBy('created_at', 'DESC')
                    ->get();
                //->toSql();

                $store_product = StoreProduct::find($product->store_product_id);
                $product->current_stock = $store_product->getCurrentStock($warehouse_id);
                $product->commited_stock = $store_product->getCommittedStock($warehouse_id);

                if (count($log) > 0) {
                    $log = $log->first();
                    $product->admin_fullname = $log->admin_fullname;
                    $deactivate_date = $log->created_at;
                    $current_date = \Carbon\Carbon::now();
                    $diff_days = $current_date->diffInDays($deactivate_date);
                    $diff_hours = $current_date->diffInHours($deactivate_date);
                    $diff_minutes = $current_date->diffInMinutes($deactivate_date);
                    $diff_seconds = $current_date->diffInSeconds($deactivate_date);
                    $diff = '';
                    if ($diff_days > 0) {
                        $diff = $current_date->diffInDays($deactivate_date).' Días';
                    } else if ($diff_hours > 0) {
                        $diff = $current_date->diffInHours($deactivate_date).' Horas';
                    } else if ($diff_minutes) {
                        $diff = $current_date->diffInMinutes($deactivate_date).' Minutos';
                    } else if ($diff_seconds) {
                        $diff = $current_date->diffInSeconds($deactivate_date).' Segundos';
                    }
                    $product->quantity_days = $diff;
                    $product->deactivate_date = $deactivate_date->format('d M Y h:i a');
                    $products[$key] = $product;
                } else {
                    $product->quantity_days = '';
                    $product->deactivate_date = '';
                    $products[$key] = $product;
                    $product->admin_fullname = '';
                }
            }
        }
        return ['not_visibles' => $products, 'qty_not_visibles' => $qty_not_visibles, 'qty_all_products' => $qty_all_products];
    }

    /**
     * Saca cantidades del inventario de picking
     * @param $storeProductWarehouse
     * @param $module
     * @param null $quantity
     * @return mixed
     * @throws MerqueoException
     */
    public static function pullFrom($storeProductWarehouse, $module, $quantity = null)
    {
        if (!is_a($storeProductWarehouse, StoreProductWarehouse::class)) {
            throw new MerqueoException('El debe ingresar un objeto tipo StoreProductWarehouse');
        }
        if (gettype($module) !== 'string') {
            throw new MerqueoException('El módulo debe ser una cadena.');
        }
        if (!is_null($quantity)) {
            if ((gettype($quantity) !== 'integer' || $quantity < 1)) {
                throw new MerqueoException('La cantidad a almacenar no puede ser menor a 1.');
            }
            if ($quantity > $storeProductWarehouse->quantity) {
                throw new MerqueoException('La cantidad a retirar es superior a la cantidad almacenada.');
            }
        }

        if (!empty($quantity)) {
            $storeProductWarehouse->picking_stock -= $quantity;
        } else {
            $quantity = $storeProductWarehouse->picking_stock;
            $storeProductWarehouse->picking_stock = 0;
        }
        Event::fire('storeProductWarehouse.onPullQuantity', [$storeProductWarehouse, $module, $quantity]);
        $storeProductWarehouse->save();

        return $quantity;
    }

    /**
     * Ingresa cantidades al inventario de picking
     * @param $storeProductWarehouse
     * @param $module
     * @param $quantity
     * @return mixed
     * @throws MerqueoException
     */
    public static function pushTo($storeProductWarehouse, $module, $quantity)
    {
        if (!is_a($storeProductWarehouse, StoreProductWarehouse::class)) {
            throw new MerqueoException('Se debe ingresar un objeto tipo StoreProductWarehouse');
        }
        if (gettype($module) !== 'string') {
            throw new MerqueoException('El módulo debe ser una cadena.');
        }
        if (!is_null($quantity)) {
            if ((gettype($quantity) !== 'integer' || $quantity < 1)) {
                throw new MerqueoException('La cantidad a almacenar no puede ser menor a 1.');
            }
        }

        if (!empty($quantity)) {
            $storeProductWarehouse->picking_stock += $quantity;
        }

        Event::fire('storeProductWarehouse.onPushQuantity', [$storeProductWarehouse, $module, $quantity]);
        $storeProductWarehouse->save();

        return $quantity;
    }

    /**
     * Encola una tarea que valida la visibilidad de un producto en la tienda.
     */
    public function queueVisibilityTask()
    {
        Queue::push(\jobs\RefreshVisibility::class, ['id' => $this->getKey()], 'quick');
    }

    public function saveStoreProductWarehouseLog($changes)
    {

        $storeProductWarehouseLog = new \StoreProductWarehouseLog();
        $storeProduct = \StoreProduct::find($this->store_product_id);

        if (Session::has('admin_id')) {
            $storeProductWarehouseLog->admin_id = Session::get('admin_id');
        }

        $storeProductWarehouseLog->store_product_warehouse_id = $this->id;
        $storeProductWarehouseLog->fields = json_encode($changes);
        $storeProductWarehouseLog->reception_stock = $this->reception_stock;
        $storeProductWarehouseLog->picking_stock = $this->picking_stock;
        $storeProductWarehouseLog->picked_stock = $this->picked_stock;
        $storeProductWarehouseLog->storage_stock = $storeProduct->getStorageStock($this->warehouse_id);
        $storeProductWarehouseLog->return_stock = $this->return_stock;
        $storeProductWarehouseLog->current_stock = $storeProduct->getCurrentStock($this->warehouse_id);
        $storeProductWarehouseLog->committed_stock = $storeProduct->getCommittedStock($this->warehouse_id);
        $storeProductWarehouseLog->save();

        return $storeProductWarehouseLog;
    }
}
