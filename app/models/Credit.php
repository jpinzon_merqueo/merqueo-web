<?php


use Illuminate\Database\Eloquent\Model;

/**
 * Class Credit
 * @property string description
 * @property string description_admin
 */
class Credit extends Model
{
    protected $table = "credits";

    protected $fillable = ['name', 'description', 'description_admin', 'responsible_area', 'type'];


    public function credits()
    {
        return $this->belongsToMany(Credit::class, "user_credits")->withPivot(['amount', 'expiration_date', 'coupon_id', 'referrer_id', 'external_service_id', 'current_amount', 'claim_motive', 'text_products_related', 'status']);
    }

}