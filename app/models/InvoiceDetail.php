<?php

/**
 * Model InvoiceDetail
 */
class InvoiceDetail extends Illuminate\Database\Eloquent\Model
{
    /**
     * @var string
     */
    protected $table = "invoice_details";

    /**
     * Relacion con ProductInvoice
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function productInvoice()
    {
        return $this->belongsTo('ProductInvoice');
    }
}