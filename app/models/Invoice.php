<?php

/**
 * Model Invoice
 */
class Invoice extends Illuminate\Database\Eloquent\Model
{
    /**
     * @var string
     */
    protected $table = "invoices";

    /**
     * Relación con InvoiceDetail
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function invoiceDetails(){
        return $this->hasMany('InvoiceDetail');
    }

    /**
     * Relación con CustomerInvoice
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function customerInvoice() {
        return $this->belongsTo('CustomerInvoice');
    }
}
