<?php

namespace delivery_times;

use Carbon\Carbon;

/**
 * Class DeliveryTimeConverter
 * @package delivery_times
 */
class DeliveryTimeConverter implements \JsonSerializable
{

    /**
     * @var array
     */
    protected $data;

    /**
     * @var bool
     */
    protected $show_delivery_amount;

    /**
     * @var Carbon[]
     */
    protected $default_dates;

    /**
     * @var bool
     */
    protected $any_day_selected = false;

    /**
     * DeliveryTimeConverter constructor.
     *
     * @param array $data
     * @param bool $show_delivery_amount
     * @param array $default_dates
     */
    public function __construct(array $data, $show_delivery_amount = true, $default_dates = [])
    {
        $this->data = $data;
        $this->show_delivery_amount = $show_delivery_amount;
        $this->default_dates = $default_dates;
    }

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize()
    {
        return  $this->selectFirstSlot($this->serialize());
    }

    /**
     * @return array
     */
    public function getSelectedItem()
    {
        $items = $this->jsonSerialize();
        $selected_item = [];
        foreach ($items as $item) {
            if ($item['selected']) {
                foreach ($item['times'] as $time) {
                    if ($time['selected']) {
                        $selected_item = $item;
                        unset($selected_item['times']);
                        $selected_item['times'][] = $time;

                        return $selected_item;
                    }
                }
            }
        }

        return $selected_item;
    }

    /**
     * Determina si el usuario posee domocilio gratis.
     *
     * @return bool
     */
    public function hasFreeDelivery()
    {
        foreach ($this->data['time'] as $date) {
            foreach ($date as $time) {
                if ($time['delivery_amount'] > 0) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * @return array
     */
    private function serialize()
    {
        $days = [];
        foreach ($this->data['days'] as $format => $name) {

            $item = [
                'day' => $name,
                'date' => format_date('short_with_moth_name', $format),
                'value' => $format,
                'selected' => false,
                'times' => [],
            ];

            foreach ($this->data['time'][$format] as $slot => $slot_info) {
                $text = $this->show_delivery_amount
                    ? $slot_info['text']
                    : $this->removeDeliveryAmountText($slot_info['text']);

                $item['times'][] = [
                    'time' => (string) $slot,
                    'value' => $text,
                    'selected' => false,
                    'delivery_amount' => $slot_info['delivery_amount'],
                    'is_new' => $slot_info['is_new'],
                    'is_express' => isset($slot_info['is_express']) ? $slot_info['is_express'] : 0
                ];
            }

            $days[] = $item;
        }

        return $days;
    }

    /**
     * Selecciona el slot configurado, si no hay
     * ninguno toma el primer item del listado.
     *
     * @param $slots
     * @return array
     */
    private function selectFirstSlot($slots)
    {
        foreach ($this->default_dates as $default_date) {
            foreach ($slots as $slot_index => $slot) {
                if (!$this->isDateSelected($default_date, $slot['value'])) {
                    continue;
                }

                foreach ($slot['times'] as $time_index => $time) {
                    if (!$this->isTimeSelected($default_date, $time['time'])) {
                        continue;
                    }

                    $slots[$slot_index]['selected'] = true;
                    $slots[$slot_index]['times'][$time_index]['selected'] = true;

                    return $slots;
                }
            }
        }

        $slots[0]['selected'] = true;
        $slots[0]['times'][0]['selected'] = true;

        return $slots;
    }

    /**
     * Elimina la cadena indica el costo del domicilio.
     *
     * @param $text
     * @return string
     */
    private function removeDeliveryAmountText($text)
    {
        $match = preg_match('/(.*)( \(.+\))/', $text, $matches);
        return $match ? $matches[1] : $text;
    }

    /**
     * Determina si la fecha coincide con la fecha por defecto.
     *
     * @param Carbon $default_date
     * @param $slot_date
     * @return bool
     */
    private function isDateSelected(Carbon $default_date, $slot_date)
    {
        return $slot_date === $default_date->format('Y-m-d');
    }

    /**
     * Determina si la hora inicial del rango coincide
     * con la fecha seleccionada por defecto.
     *
     * @param Carbon $default_date
     * @param $slot_date
     * @return bool
     */
    private function isTimeSelected(Carbon $default_date, $slot_date)
    {
        list($begin_range) = explode(' - ', $slot_date);
        return $begin_range === $default_date->format('H:00:00');
    }
}
