<?php

/**
 * @property string $type Domicilio gratis, Descuento, Crédito, Cupón.
 * @property int $percentage_amount
 * @property float $amount
 * @property string $description
 * @property Order $order
 * @property Coupon $coupon
 */
class OrderDiscount extends \Illuminate\Database\Eloquent\Model
{

    protected $table = 'order_discounts';

    /**
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo('Order');
    }

    /**
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function coupon()
    {
        return $this->belongsTo('Coupon');
    }
    /**
     * Guarda descuento en pedido
     *
     * @param Order $order Order
     * @param array $order_discount_data Datos de descuento
     */
    public static function saveDiscount(Order $order, $order_discount_data)
    {
        foreach ($order_discount_data as $type => $data) {
            $order_discount = new OrderDiscount();
            $order_discount->order_id = $order->id;
            $order_discount->type = $type;
            $order_discount->description = $data['description'];

            switch ($type) {
                case 'Crédito':
                    $order_discount->amount = $order->discount_amount;
                    $order_discount->percentage_amount = $order->discount_percentage_amount;
                    break;

                case 'Descuento':
                    $order_discount->amount = $order->discount_amount;
                    $order_discount->percentage_amount = $order->discount_percentage_amount;
                    break;

                case 'Domicilio gratis':
                    $order_discount->amount = $data['amount'];
                    break;

                case 'Cupón':
                    $order_discount->coupon_id = $data['coupon_id'];
                    $order_discount->amount = $order->discount_amount;
                    $order_discount->percentage_amount = $order->discount_percentage_amount;
                    break;
            }

            $order_discount->save();
        }
    }

    /**
     * Elimina descuentos en pedido
     *
     * @param Order $order Order
     */
    public static function removeDiscount(Order $order)
    {
        OrderDiscount::where('order_id', $order->id)->where(function($query){
            $query->where('type', 'Descuento')
                ->orWhere('type', 'Crédito')
                ->orWhere('type', 'Cupón');
        })->delete();
    }
}
