<?php

use exceptions\MerqueoException;

/**
 * @property int id
 * @property int admin_id
 * @property int origin_warehouse_id
 * @property int destiny_warehouse_id
 * @property datetime received_date
 * @property string $spanishStatus
 * @property string status
 * @property Warehouse destinyWarehouse
 * @property Warehouse originWarehouse
 * @property mixed productTransferDetails
 * @property datetime storage_date
 */
class ProductTransfer extends Illuminate\Database\Eloquent\Model
{
    protected $table = 'product_transfers';

    public static function boot()
    {
        parent::boot();

        static::updating(function ($productTransfer) {
            if ($productTransfer->isDirty()) {
                $dirtyProductTransfer = $productTransfer->getDirty();
                $originalProductTransfer = $productTransfer->getOriginal();
                $original = $dirty = [];
                foreach ($dirtyProductTransfer as $att => $value) {
                    $dirty[$att] = $value;
                    $original[$att] = $originalProductTransfer[$att];
                }
                $productTransferLog = new ProductTransferLog;
                if (Session::has('admin_id')) {
                    $productTransferLog->admin_id = Session::get('admin_id');
                }
                $productTransferLog->transfer_id = $productTransfer->id;
                $productTransferLog->original_attributes = json_encode($original);
                $productTransferLog->changed_attributes = json_encode($dirty);
                $productTransferLog->save();
            }
        });
    }

    public function productTransferDetails()
    {
        return $this->hasMany(ProductTransferDetail::class, 'transfer_id');
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

    public function originWarehouse()
    {
        return $this->belongsTo(Warehouse::class, 'origin_warehouse_id');
    }

    public function destinyWarehouse()
    {
        return $this->belongsTo(Warehouse::class, 'destiny_warehouse_id');
    }

    public function productTransferLogs()
    {
        return $this->hasMany(ProductTransferLog::class, 'transfer_id');
    }

    /**
     * Obtiene los estados en español
     *
     * @param $value
     * @return string
     */
    public function getSpanishStatusAttribute($value)
    {
        $value = $this->status;
        $status = '';
        switch ($value) {
            case 'Initiated':
                $status = 'Iniciado';
                break;
            case 'In process':
                $status = 'En proceso';
                break;
            case 'Received':
                $status = 'Recibido';
                break;
            case 'Storaged':
                $status = 'Almacenado';
                break;
            case 'Cancelled':
                $status = 'Cancelado';
                break;
            case 'Cancelled_storaged':
                $status = 'Cancelado y almacenado';
                break;
        }
        $this->attributes['status_class'] = 'danger';
        return $status;
    }

    /**
     * @return string
     */
    public function getStatusClassAttribute()
    {
        $value = $this->status;
        $class = '';
        switch ($value) {
            case 'Initiated':
                $class = 'default';
                break;
            case 'In process':
                $class = 'primary';
                break;
            case 'Received':
                $class = 'info';
                break;
            case 'Storaged':
                $class = 'success';
                break;
            case 'Cancelled':
                $class = 'danger';
                break;
            case 'Cancelled_storaged':
                $class = 'success';
                break;
        }
        return $class;
    }

    /**
     * Muestra la fecha de recibido formateada.
     *
     * @param $value
     * @return string
     */
    public function getReceivedDateFormattedAttribute($value)
    {
        $value = $this->received_date;
        if (!empty($value)) {
            $date = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $value);
            return $date->format('d M Y g:i a');
        }
        return 'No tiene fecha de recepción';
    }

    public function getCreatedAtDateFormattedAttribute($value)
    {
        $value = $this->created_at;
        if (!empty($value)) {
            $date = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $value);
            return $date->format('d M Y g:i a');
        }
        return 'No tiene fecha de creación';
    }

    /**
     * Muestra la fecha de almacenado formateada.
     *
     * @param $value
     * @return string
     */
    public function getStorageDateFormattedAttribute($value)
    {
        $value = $this->storage_date;
        if (!empty($value)) {
            $date = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $value);
            return $date->format('d M Y g:i a');
        }
        return 'No tiene fecha de almacenado';
    }

    /**
     * Crea un traslado
     *
     * @param Admin $admin
     * @param Warehouse $originWarehouse
     * @param Warehouse $destinyWarehouse
     * @param array $addedProducts
     * @param string $status
     * @return mixed
     * @throws MerqueoException
     * @throws Exception
     */
    public static function createTransfer(
        \Admin $admin,
        \Warehouse $originWarehouse,
        \Warehouse $destinyWarehouse,
        array $addedProducts,
        $status = 'Initiated'
    ) {
        try {
            DB::beginTransaction();
            foreach ($addedProducts as $product) {
                $warehouseStorage = WarehouseStorage::where('warehouse_id', $originWarehouse->id)
                    ->where('store_product_id', $product['store_product_id'])
                    ->where('position', $product['position_from'])
                    ->where('position_height', $product['position_height_from'])
                    ->lockForUpdate()
                    ->first();

                if ($warehouseStorage) {
                    $warehouseStorageLog = new WarehouseStorageLog();
                    $warehouseStorageLog->admin_id = Session::get('admin_id');
                    $warehouseStorageLog->warehouse_id = $originWarehouse->id;
                    $warehouseStorageLog->store_product_id = $product['store_product_id'];
                    $warehouseStorageLog->module = 'Traslado de productos';
                    $warehouseStorageLog->quantity_from = $warehouseStorage->quantity;
                    $warehouseStorageLog->position_from = $warehouseStorage->position;
                    $warehouseStorageLog->position_height_from = $warehouseStorage->position_height;

                    $warehouseStorage->quantity -= $product['transfer_quantity'];
                    if ($warehouseStorage->quantity >= 0) {
                        $warehouseStorage->save();
                        $warehouseStorageLog->movement = 'update';

                        $warehouseStorageLog->quantity_to = $warehouseStorage->quantity;
                        $warehouseStorageLog->position_to = $warehouseStorage->position;
                        $warehouseStorageLog->position_height_to = $warehouseStorage->position_height;

                        if ($warehouseStorage->quantity == 0) {
                            $warehouseStorage->delete();
                            $warehouseStorageLog->movement = 'remove';
                        }
                        $warehouseStorageLog->save();

                        $detail = new \ProductTransferDetail();
                        $detail->store_product_id = $product['store_product_id'];
                        $detail->position_height_from = $product['position_height_from'];
                        $detail->position_from = $product['position_from'];
                        $detail->transferred_quantity = $product['transfer_quantity'];
                        $detail->quantity_received = 0;
                        $detail->quantity_storaged = 0;
                        $detail->expiration_date = $product['expiration_date'];
                        $detail->storage_type = $product['storage_type'];
                        $detail->status = 'Not received';
                        $details[] = $detail;
                    } else {
                        $product = StoreProduct::with('product')->find($product['store_product_id']);
                        throw new MerqueoException("Las cantidades a transferir del producto: 
                    {$product->product->name} {$product->product->quantity} {$product->product->unit} 
                    son superiores a las almacendas");
                    }
                } else {
                    throw new MerqueoException("No se encuentra el producto: 
                {$product->product->name} {$product->product->quantity} {$product->product->unit} 
                en la ubicación: {$product['position_from']} {$product['position_height_from']}.");
                }
            }

            $productTransfer = new ProductTransfer();
            $productTransfer->admin_id = $admin->id;
            $productTransfer->origin_warehouse_id = $originWarehouse->id;
            $productTransfer->destiny_warehouse_id = $destinyWarehouse->id;
            $productTransfer->status = $status;
            $productTransfer->save();
            $productTransfer->productTransferDetails()->saveMany($details);
            DB::commit();
            return $productTransfer;
        } catch (\Illuminate\Database\QueryException $e) {
            throw $e;
        } catch (MerqueoException $e) {
            throw $e;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function logs()
    {
        $header_logs = $this->productTransferLogs;
        /*$detail_logs = $this->productTransferDetails->filter(function ($detail) {
            if (!empty($detail->productTransferDetailLogs)) {
                return
            }
            return $deta
        });*/
    }

    /**
     * Actualiza la traslado al estado correcpondiente, llama la función para cada estado.
     *
     * @param $status
     * @return bool|int
     * @throws MerqueoException
     */
    public function updateStatus($status)
    {
        switch ($status) {
            case 'In proccess':
                return $this->setStatusInProcess();
                break;

            case 'Received':
                return $this->setStatusReceived();
                break;

            case 'Cancelled':
                return $this->setStatusCancelled();
                break;

            case 2:
                return 2;
                break;
        }
    }

    /**
     * Actualiza el estado de la traslado a en progreso
     *
     * @return bool
     * @throws MerqueoException
     */
    public function setStatusInProcess()
    {
        if ($this->status !== 'Initiated') {
            throw new MerqueoException("El traslado no está inicado y no se puede cambiar a En proceso.");
        }
        $detailsCount = $this->productTransferDetails->count();
        $pendingDetailsCount = $this->productTransferDetails->filter(function ($transferDetail) {
            return $transferDetail->status == 'Not received';
        })
            ->count();

        if ($detailsCount !== $pendingDetailsCount) {
            throw new MerqueoException("Los productos del traslado no estan en estado 'No recibido'.");
        }
        $this->status = 'In process';
        $this->save();
        return true;
    }

    /**
     * Actualiza el estado de la transferencia a recibido
     *
     * @return bool
     * @throws MerqueoException
     * @throws Exception
     */
    public function setStatusReceived()
    {
        if ($this->status !== 'In process') {
            throw new MerqueoException("El traslado no está En proceso y no se puede cambiar a Recibido.");
        }
        $receivedProducts = $this->productTransferDetails->filter(function ($transferDetail) {
            return $transferDetail->quantity_received > 0
                && in_array($transferDetail->status, ['Received', 'Partially received']);
        });

        if ($receivedProducts->count() == 0) {
            throw new MerqueoException("No hay productos para ser recibidos.");
        }
        try {
            DB::beginTransaction();
            $self = $this;
            $receivedProducts->each(function ($tranferDetail) use ($self) {
                $product = $tranferDetail->storeProduct->product;
                $store = Store::getActiveStores($self->destinyWarehouse->city_id)->first();
                $storeProductWarehouse = $product->storeProduct()->where('store_id', $store->id)->first()
                    ->storeProductWarehouses()
                    ->where('warehouse_id', $self->destinyWarehouse->id)
                    ->lockForUpdate()
                    ->first();

                if (empty($storeProductWarehouse)) {
                    throw new MerqueoException("El producto {$tranferDetail->storeProduct->product->name} 
                    {$tranferDetail->storeProduct->product->quantity} 
                    {$tranferDetail->storeProduct->product->unit} no está creado en la bodega de destino.");
                }

                $storeProductWarehouse->reception_stock += $tranferDetail->quantity_received;
                $activable = $tranferDetail->storeProduct->getCurrentStock($self->destiny_warehouse_id);

                if ($activable) {
                    $storeProductWarehouse->status = 1;
                } else {
                    $storeProductWarehouse->status = 0;
                }

                $storeProductWarehouse->save();
            });

            $this->status = 'Received';
            $this->received_date = \Carbon\Carbon::now();
            $this->save();
            DB::commit();
            return true;
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollBack();
            throw $e;
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    /**
     * Actualiza el estado del traslado a cancelado
     *
     * @return bool
     * @throws MerqueoException
     * @throws Exception
     */
    public function setStatusCancelled()
    {
        if (in_array($this->status, ['Received', 'Storaged', 'Cancelled', 'Cancelled_storaged'])) {
            throw new MerqueoException(
                "El traslado no puede ser cancelada debido a que el estatus del traslado es {$this->status}."
            );
        }

        try {
            DB::beginTransaction();
            $self = $this;
            $this->productTransferDetails->each(function ($detail) use ($self) {
                $detail->quantity_received = $detail->transferred_quantity;
                $detail->status = 'Received';
                $detail->save();

                $storeProductWarehouse = $detail->storeProduct->storeProductWarehouses()
                    ->where('warehouse_id', $self->origin_warehouse_id)
                    ->lockForUpdate()
                    ->first();

                $storeProductWarehouse->reception_stock += $detail->transferred_quantity;
                $activable = $detail->storeProduct->getCurrentStock($self->origin_warehouse_id);

                if ($activable) {
                    $storeProductWarehouse->status = 1;
                } else {
                    $storeProductWarehouse->status = 0;
                }

                $storeProductWarehouse->save();
            });

            $this->status = 'Cancelled';
            $this->received_date = \Carbon\Carbon::now();
            $this->save();
            DB::commit();
            return true;
        } catch (\Illuminate\Database\QueryException $e) {
            DB::rollBack();
            throw $e;
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    /**
     * @return bool
     * @throws MerqueoException
     */
    public function setStatusStoraged()
    {
        if (empty($this->details_to_storage)) {
            throw new MerqueoException("No se han definido los productos para ser almacenados.");
        }
        $warehouseId = null;
        if ($this->status == 'Cancelled') {
            $warehouseId = $this->originWarehouse->id;
        } elseif ($this->status == 'Received') {
            $warehouseId = $this->destinyWarehouse->id;
        } else {
            throw new MerqueoException("El traslado no puede ser almacenado porque no está en el estado correcto.");
        }

        $this->details_to_storage->each(function ($detail) use ($warehouseId) {
            $detail->storeDetail($warehouseId);
        });

        $this->load('productTransferDetails');

        $currentDetails = $this->productTransferDetails->count();
        $storagedDetails = $this->productTransferDetails->filter(function ($detail) {
            return $detail->quantity_received == $detail->quantity_storaged;
        })->count();

        if ($currentDetails == $storagedDetails) {
            if ($this->status == 'Cancelled') {
                $this->status = 'Cancelled_storaged';
            } elseif ($this->status == 'Received') {
                $this->status = 'Storaged';
            }
        }
        $this->storage_date = \Carbon\Carbon::now();
        unset($this->details_to_storage);
        $this->save();
        return true;
    }

    public function toArray($options = 0)
    {
        $result = parent::toArray($options);
        $result['spanish_status'] = $this->spanishStatus;
        $result['status_class'] = $this->StatusClass;

        return $result;
    }
}