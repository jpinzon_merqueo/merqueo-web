<?php

class Brand extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'brands';


    public function getSubbrands()
    {
    	$subbrands = Subbrand::where('brand_id', $this->id)->get();
    	return $subbrands;
    }
}