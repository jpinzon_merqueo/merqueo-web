<?php

namespace carts;

/**
 * Class HelpCenterCartAdapter
 * @package cart
 */
class IosCartAdapter extends HelpCenterCartAdapter
{
    /**
     * @param \CartProduct $cart_product
     * @param $item
     */
    protected function getCartProductDetails(\CartProduct $cart_product, $item)
    {
        $cart_product->quantity_full_price += empty($item['quantity_full_price']) ? 0 : $item['quantity_full_price'];
        $cart_product->quantity += empty($item['quantity_special_price']) ? 0 : $item['quantity_special_price'];
        if (!empty($item['full_price'])) {
            $cart_product->price = $item['full_price'];
        }

        if (!empty($item['special_price'])) {
            $cart_product->special_price = $item['special_price'];
        }
    }

    /**
     * @return array
     */
    protected function sanitizeData()
    {
        $data = [];
        foreach ($this->data as $item) {
            $has_full_price = !empty($item['quantity_full_price']);
            $has_special_price = !empty($item['quantity_special_price']);
            if (!empty($item['id']) && ($has_full_price || $has_special_price)) {
                $data[] = $item;
            }
        }

        return $data;
    }
}
