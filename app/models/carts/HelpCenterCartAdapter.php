<?php

namespace carts;

use exceptions\MerqueoException;
use Illuminate\Support\Collection;

/**
 * Class HelpCenterCartAdapter
 * @package cart
 */
class HelpCenterCartAdapter
{
    protected $data;
    protected $marketplaceItems;
    protected $merqueoCartProducts;

    /**
     * HelpCenterCartAdapter constructor.
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * @param \Warehouse $warehouse
     * @return \Cart[]
     * @throws MerqueoException
     */
    public function convert(\Warehouse $warehouse)
    {
        $data = $this->sanitizeData();
        $product_ids = array_map(function ($item) {
            return $item['id'];
        }, $data);

        if (empty($product_ids)) {
            throw new MerqueoException('Se debe agregar algún producto al carrito.');
        }

        $merqueo_cart = new \Cart();
        $marketplace_products = [];
        $merqueo_products = [];

        $products = \StoreProduct::with('product')
            ->with('alliedStore')
            ->with([
                'storeProductWarehouses' => function ($query) use ($warehouse) {
                    $query->where('warehouse_id', $warehouse->id);
                }
            ])
            ->whereIn('id', $product_ids)
            ->get();

        foreach ($data as $item) {
            $store_product = binary_search($item['id'], $products, 'id');
            if (empty($store_product)) {
                continue;
            }

            $cart_product = new \CartProduct();
            $cart_product->storeProduct()->associate($store_product);
            $this->getCartProductDetails($cart_product, $item);

            if (empty($store_product->allied_store_id)) {
                $merqueo_products[] = $cart_product;
            } else {
                $marketplace_products[$store_product->allied_store_id][] = $cart_product;
            }
        }

        $cart = [];
        if (count($merqueo_products)) {
            $merqueo_cart->setRelation('products', $merqueo_products);
            $cart[] = $merqueo_cart;
        }

        return array_merge($cart, $this->marketplaceCarts($marketplace_products));
    }

    /**
     * @param $items
     * @return array
     */
    private function marketplaceCarts($items)
    {
        $carts = [];
        foreach ($items as $allied_store_id => $products) {
            $cart = new \Cart();
            $cart->alliedStore = $products[0]->storeProduct->alliedStore;
            $cart->setRelation('products', new Collection($products));
            $carts[] = $cart;
        }

        return $carts;
    }

    /**
     * @return array
     */
    protected function sanitizeData()
    {
        $data = [];
        foreach ($this->data as $item) {
            if (!empty($item['id']) && !empty($item['details']) && count($item['details']) <= 2) {
                $data[] = $item;
            }
        }

        return $data;
    }

    /**
     * @param \CartProduct $cart_product
     * @param $item
     */
    protected function getCartProductDetails(\CartProduct $cart_product, $item)
    {
        foreach ($item['details'] as $detail) {
            if (!empty($detail['has_special_price'])) {
                $cart_product->quantity += $detail['quantity'];
                $cart_product->special_price = $detail['price'];
            } else {
                $cart_product->quantity_full_price += $detail['quantity'];
                $cart_product->quantity += $detail['quantity'];
                $cart_product->price = $detail['price'];
            }
        }
    }
}
