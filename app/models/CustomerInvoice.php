<?php

/**
 * Model CustomerInvoice
 */
class CustomerInvoice extends Illuminate\Database\Eloquent\Model
{
    /**
     * @var string
     */
    protected $table = "customer_invoice";
}