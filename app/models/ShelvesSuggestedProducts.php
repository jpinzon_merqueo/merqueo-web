<?php

class ShelvesSuggestedProducts extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'shelves_suggested_products';
    protected $hidden = array('created_at','updated_at');

    public function shelf() {
        return $this->belongsTo(Shelf::class);
    }
}
