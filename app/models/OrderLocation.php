<?php

class OrderLocation extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'order_locations';

    public function Order(){

    	return $this->hasOne('Order');
    }

    public function Order_Shopper(){

    	return $this->belongsToMany('shopers','order_locations','order_id', 'shopper_locations_id');
    }

}