<?php

class OrderReturn extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'order_returns';
    protected $hidden = array('created_at','updated_at');

    /**
     * Relacion con el modelo OrderReturnDetail
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orderReturnDetail()
    {
        return $this->hasMany('OrderReturnDetail');
    }

    /**
     * Relación con 3el modelo Order
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo('Order');
    }

    /**
     * Crea devolucion de pedido
     */
    public static function saveOrderReturn($id , $old_order_status)
    {
        if (!$order = Order::with('orderGroup')->find($id))
            return ['status' => false, 'message' => 'El pedido no existe.'];

        /*if ($order->type != 'Merqueo')
            return ['status' => false, 'message' => 'El pedido no es de Merqueo.'];*/

        $order_products = OrderProduct::join('store_product_warehouses', 'order_products.store_product_id', '=', 'store_product_warehouses.store_product_id')
            ->where('order_products.order_id', $id)
            ->where('order_products.fulfilment_status', 'Fullfilled')
            ->whereNotNull('order_products.quantity_returned')
            ->where('store_product_warehouses.warehouse_id', $order->orderGroup->warehouse_id)
            ->where('store_product_warehouses.manage_stock', 1)
            ->get();
        $order_products_returned = OrderProduct::join('store_product_warehouses', 'order_products.store_product_id', '=', 'store_product_warehouses.store_product_id')
            ->where('order_products.order_id', $id)
            ->where('order_products.fulfilment_status', 'Returned')
            ->where('store_product_warehouses.warehouse_id', $order->orderGroup->warehouse_id)
            ->where('store_product_warehouses.manage_stock', 1)
            ->get();

        if($order_products_returned){
            foreach ($order_products_returned as $order_product_returned){
                $order_products[] = $order_product_returned;
            }
        }

        $order_product_groups = OrderProductGroup::join('store_product_warehouses', 'order_product_group.store_product_id', '=', 'store_product_warehouses.store_product_id')
            ->where('order_id', $id)
            ->where('fulfilment_status', 'Fullfilled')
            ->whereNotNull('quantity_returned')
            ->where('store_product_warehouses.warehouse_id', $order->orderGroup->warehouse_id)
            ->where('store_product_warehouses.manage_stock', 1)
            ->get();
        $order_product_groups_returned = OrderProductGroup::join('store_product_warehouses', 'order_product_group.store_product_id', '=', 'store_product_warehouses.store_product_id')
            ->where('order_product_group.order_id', $id)
            ->where('order_product_group.fulfilment_status', 'Returned')
            ->where('store_product_warehouses.warehouse_id', $order->orderGroup->warehouse_id)
            ->where('store_product_warehouses.manage_stock', 1)
            ->get();

        if($order_product_groups_returned){
            foreach ($order_product_groups_returned as $order_product_group_returned){
                $order_product_groups[] = $order_product_group_returned;
            }
        }

        if (!count($order_products) && !count($order_product_groups))
            return ['status' => false, 'message' => 'No hay productos devueltos en el pedido.'];

        if(count($order_product_groups))
        {
            foreach ($order_product_groups as $key => $order_product_group){
                $order_product_group->type = OrderProduct::where('id', $order_product_group->order_product_id)->pluck('type');
                $order_products[] = $order_product_group;
            }
        }

        //crear encabezado de devolucion a bodega
        $order_return = new OrderReturn;
        $order_return->order_id = $order->id;
        $order_return->old_order_status = $old_order_status;
        if (!empty($order->vehicle_id))
        {
            $vehicle = Vehicle::find($order->vehicle_id);
            $driver = Driver::find($order->driver_id);
            $transporter = Transporter::find($vehicle->transporter_id);
            $order_return->transporter_name = $transporter->fullname;
            $order_return->driver_name = $driver->first_name.' '.$driver->last_name;
            $order_return->vehicle_plate = $vehicle->plate;
        }
        if (Input::has('reject_reason'))
            $order_return->reject_reason = Input::get('reject_reason').' - '.Input::get('reject_reason_futher');
        if (Session::has('admin_id'))
            $order_return->admin_id =  Session::get('admin_id');
        else $order_return->driver_id =  $order->driver_id;
        $order_return->status = 'Pendiente';
        $order_return->date = date('Y-m-d H:i:s');
        $order_return->save();

        //crear detalle de devolucion a bodega
        foreach ($order_products as $order_product)
        {
            if ($order_product->type == 'Agrupado')
            {
                $order_product_group = OrderProductGroup::where('order_product_id', $order_product->order_product_id)
                                                        ->where('store_product_id', $order_product->store_product_id)
                                                        ->where('fulfilment_status', 'Fullfilled')->whereNotNull('quantity_returned')->get();

                if(!count($order_product_group))
                    $order_product_group = OrderProductGroup::where('order_product_id', $order_product->order_product_id)
                                                            ->where('store_product_id', $order_product->store_product_id)
                                                            ->where('fulfilment_status', 'Returned')
                                                            ->get();

                foreach($order_product_group as $product_group)
                {

                    $quantity = ($product_group->fulfilment_status == 'Returned') ? $product_group->quantity : $product_group->quantity_returned;
                    if ($product_group->reference_returned)
                    {
                        $store_product = StoreProduct::join('products', 'products.id', '=', 'product_id')
                                                ->where('store_id', $order->store_id)
                                                ->where('products.reference', $product_group->reference_returned)
                                                ->select('store_products.id', 'products.name', 'products.quantity', 'products.unit', 'products.image_medium_url', 'products.reference')
                                                ->first();

                        if(count($store_product)){
                            for($i = 1; $i <= $quantity; $i++)
                            {
                                $order_return_detail = new OrderReturnDetail;
                                $order_return_detail->order_return_id = $order_return->id;
                                $order_return_detail->store_product_id = $store_product->id;
                                $order_return_detail->product_name = $store_product->name;
                                $order_return_detail->product_reference = $store_product->reference;
                                $order_return_detail->product_quantity = $store_product->quantity;
                                $order_return_detail->product_unit = $store_product->unit;
                                $order_return_detail->product_image_url = $store_product->image_medium_url;
                                $order_return_detail->status = 'Pendiente';
                                $order_return_detail->save();
                            }
                        }
                    }
                    else
                    {
                        for($i = 1; $i <= $quantity; $i++)
                        {
                            $order_return_detail = new OrderReturnDetail;
                            $order_return_detail->order_return_id = $order_return->id;
                            $order_return_detail->store_product_id = $product_group->store_product_id;
                            $order_return_detail->product_name = $product_group->product_name;
                            $order_return_detail->product_reference = $product_group->reference;
                            $order_return_detail->product_quantity = $product_group->product_quantity;
                            $order_return_detail->product_unit = $product_group->product_unit;
                            $order_return_detail->product_image_url = $product_group->product_image_url;
                            $order_return_detail->status = 'Pendiente';
                            $order_return_detail->save();
                        }
                    }
                }
            }else{
                $quantity = ($order_product->fulfilment_status == 'Returned') ? $order_product->quantity : $order_product->quantity_returned;
                if ($order_product->reference_returned)
                {
                    $store_product = StoreProduct::join('products', 'products.id', '=', 'product_id')
                                            ->where('store_id', $order->store_id)
                                            ->where('products.reference', $order_product->reference_returned)
                                            ->select('store_products.id', 'products.name', 'products.quantity', 'products.unit', 'products.image_medium_url', 'products.reference')
                                            ->first();

                    if(count($store_product)){
                        for($i = 1; $i <= $quantity; $i++)
                        {
                            $order_return_detail = new OrderReturnDetail;
                            $order_return_detail->order_return_id = $order_return->id;
                            $order_return_detail->store_product_id = $store_product->id;
                            $order_return_detail->product_name = $store_product->name;
                            $order_return_detail->product_reference = $store_product->reference;
                            $order_return_detail->product_quantity = $store_product->quantity;
                            $order_return_detail->product_unit = $store_product->unit;
                            $order_return_detail->product_image_url = $store_product->image_medium_url;
                            $order_return_detail->status = 'Pendiente';
                            $order_return_detail->save();
                        }
                    }
                }else{
                    for($i = 1; $i <= $quantity; $i++)
                    {
                        $order_return_detail = new OrderReturnDetail;
                        $order_return_detail->order_return_id = $order_return->id;
                        $order_return_detail->store_product_id = $order_product->store_product_id;
                        $order_return_detail->product_name = $order_product->product_name;
                        $order_return_detail->product_reference = $order_product->reference;
                        $order_return_detail->product_quantity = $order_product->product_quantity;
                        $order_return_detail->product_unit = $order_product->product_unit;
                        $order_return_detail->product_image_url = $order_product->product_image_url;
                        $order_return_detail->status = 'Pendiente';
                        $order_return_detail->save();
                    }
                }
            }
        }

        $log = new OrderLog();
        $log->type = 'Devolución a bodega # '.$order_return->id.' creada.';
        if (Session::has('admin_id'))
            $log->admin_id =  Session::get('admin_id');
        else $log->driver_id =  $order->driver_id;
        $log->order_id = $order_return->order_id ;
        $log->save();

        return ['status' => true, 'message' => 'Se ha registrado la devolución a bodega con éxito.'];
    }

    /**
     * Actualiza detalle de devolucion de pedido
     */
    public static function updateOrderReturn($id)
    {
        if (!$order = Order::with('orderGroup')->find($id))
            return ['status' => false, 'message' => 'El pedido no existe.'];

        /*if ($order->type != 'Merqueo')
            return ['status' => false, 'message' => 'El pedido no es de Merqueo.'];*/

        $order_products = OrderProduct::join('store_product_warehouses', 'order_products.store_product_id', '=', 'store_product_warehouses.store_product_id')
            ->where('order_products.order_id', $id)
            ->whereIn('order_products.fulfilment_status', ['Fullfilled', 'Returned'])
            ->whereNotNull('order_products.quantity_returned')
            ->where('store_product_warehouses.warehouse_id', $order->orderGroup->warehouse_id)
            ->where('store_product_warehouses.manage_stock', 1)
            ->get();
            $order_products_returned = OrderProduct::join('store_product_warehouses', 'order_products.store_product_id', '=', 'store_product_warehouses.store_product_id')
            ->where('order_products.order_id', $id)
            ->where('order_products.fulfilment_status', 'Returned')
            ->where('store_product_warehouses.warehouse_id', $order->orderGroup->warehouse_id)
            ->where('store_product_warehouses.manage_stock', 1)
            ->get();

        if($order_products_returned){
            foreach ($order_products_returned as $order_product_returned){
                $order_products[] = $order_product_returned;
            }
        }

        $order_product_groups = OrderProductGroup::join('store_product_warehouses', 'order_product_group.store_product_id', '=', 'store_product_warehouses.store_product_id')
            ->where('order_product_group.order_id', $id)
            ->whereIn('order_product_group.fulfilment_status', ['Fullfilled', 'Returned'])
            ->whereNotNull('order_product_group.quantity_returned')
            ->where('store_product_warehouses.warehouse_id', $order->orderGroup->warehouse_id)
            ->where('store_product_warehouses.manage_stock', 1)
            ->get();
        $order_product_groups_returned = OrderProductGroup::join('store_product_warehouses', 'order_product_group.store_product_id', '=', 'store_product_warehouses.store_product_id')
            ->where('order_product_group.order_id', $id)
            ->where('order_product_group.fulfilment_status', 'Returned')
            ->where('store_product_warehouses.warehouse_id', $order->orderGroup->warehouse_id)
            ->where('store_product_warehouses.manage_stock', 1)
            ->get();

        if($order_product_groups_returned){
            foreach ($order_product_groups_returned as $order_product_group_returned){
                $order_product_groups[] = $order_product_group_returned;
            }
        }

        if (!count($order_products) && !count($order_product_groups))
            return ['status' => false, 'message' => 'No hay productos devueltos en el pedido.'];

        if(count($order_product_groups))
        {
            foreach ($order_product_groups as $key => $order_product_group){
                $order_product_group->type = OrderProduct::where('id', $order_product_group->order_product_id)->pluck('type');
                $order_products[] = $order_product_group;
            }
        }

        if (!$order_return = OrderReturn::where('order_id', $order->id)->first()){

            //crear devolucion a bodega
            $order_return = new OrderReturn;
            $order_return->order_id = $order->id;
            if (!empty($order->vehicle_id))
            {
                $vehicle = Vehicle::find($order->vehicle_id);
                $driver = Driver::find($order->driver_id);
                $transporter = Transporter::find($vehicle->transporter_id);
                $order_return->transporter_name = $transporter->fullname;
                $order_return->driver_name = $driver->first_name.' '.$driver->last_name;
                $order_return->vehicle_plate = $vehicle->plate;
            }
            if (Session::has('admin_id'))
                $order_return->admin_id =  Session::get('admin_id');
            else $order_return->driver_id =  $order->driver_id;
            $order_return->status = 'Pendiente';
            $order_return->date = date('Y-m-d H:i:s');
            $order_return->save();

            $log = new OrderLog();
            $log->type = 'Devolución a bodega # '.$order_return->id.' creada.';
            if (Session::has('admin_id'))
                $log->admin_id =  Session::get('admin_id');
            else $log->driver_id =  $order->driver_id;
            $log->order_id = $order_return->order_id ;
            $log->save();

        }else if ($order_return->status != 'Pendiente')
                return ['status' => false, 'message' => 'El estado de la devolución a bodega no es valido.'];

        //eliminar detalle de devolucion a bodega
        OrderReturnDetail::where('order_return_id', $order_return->id)->delete();

        //crear nuevo detalle de devolucion a bodega
        foreach ($order_products as $order_product)
        {
            if ($order_product->type == 'Agrupado')
            {
                $order_product_group = OrderProductGroup::where('order_product_id', $order_product->order_product_id)
                                                        ->where('store_product_id', $order_product->store_product_id)
                                                        ->whereNotNull('quantity_returned')->get();

                foreach($order_product_group as $product_group){

                    $quantity = ($product_group->fulfilment_status == 'Returned') ? $product_group->quantity : $product_group->quantity_returned;
                    if($product_group->reference_returned){
                        $store_product = StoreProduct::join('products', 'products.id', '=', 'product_id')
                                                ->where('store_id', $order->store_id)
                                                ->where('products.reference', $product_group->reference_returned)
                                                ->select('store_products.id', 'products.name', 'products.quantity', 'products.unit', 'products.image_medium_url', 'products.reference')
                                                ->first();

                        for($i = 1; $i <= $quantity; $i++)
                        {
                            $order_return_detail = new OrderReturnDetail;
                            $order_return_detail->order_return_id = $order_return->id;
                            $order_return_detail->store_product_id = $store_product->id;
                            $order_return_detail->product_name = $store_product->name;
                            $order_return_detail->product_reference = $store_product->reference;
                            $order_return_detail->product_quantity = $store_product->quantity;
                            $order_return_detail->product_unit = $store_product->unit;
                            $order_return_detail->product_image_url = $store_product->image_medium_url;
                            $order_return_detail->status = 'Pendiente';
                            $order_return_detail->save();
                        }
                    }else{
                        for($i = 1; $i <= $quantity; $i++)
                        {
                            $order_return_detail = new OrderReturnDetail;
                            $order_return_detail->order_return_id = $order_return->id;
                            $order_return_detail->store_product_id = $product_group->store_product_id;
                            $order_return_detail->product_name = $product_group->product_name;
                            $order_return_detail->product_reference = $product_group->reference;
                            $order_return_detail->product_quantity = $product_group->product_quantity;
                            $order_return_detail->product_unit = $product_group->product_unit;
                            $order_return_detail->product_image_url = $product_group->product_image_url;
                            $order_return_detail->product_quantity_returned = $product_group->quantity_returned;
                            $order_return_detail->status = 'Pendiente';
                            $order_return_detail->save();
                        }
                    }
                }
            }else{
                $quantity = ($order_product->fulfilment_status == 'Returned') ? $order_product->quantity : $order_product->quantity_returned;
                if($order_product->reference_returned){
                    $store_product = StoreProduct::join('products', 'products.id', '=', 'product_id')
                                            ->where('store_id', $order->store_id)
                                            ->where('products.reference', $order_product->reference_returned)
                                            ->select('store_products.id', 'products.name', 'products.quantity', 'products.unit', 'products.image_medium_url', 'products.reference')
                                            ->first();

                    for($i = 1; $i <= $quantity; $i++)
                    {
                        $order_return_detail = new OrderReturnDetail;
                        $order_return_detail->order_return_id = $order_return->id;
                        $order_return_detail->store_product_id = $store_product->id;
                        $order_return_detail->product_name = $store_product->name;
                        $order_return_detail->product_reference = $store_product->reference;
                        $order_return_detail->product_quantity = $store_product->quantity;
                        $order_return_detail->product_unit = $store_product->unit;
                        $order_return_detail->product_image_url = $store_product->image_medium_url;
                        $order_return_detail->status = 'Pendiente';
                        $order_return_detail->save();
                    }
                }else{
                    for($i = 1; $i <= $quantity; $i++)
                    {
                        $order_return_detail = new OrderReturnDetail;
                        $order_return_detail->order_return_id = $order_return->id;
                        $order_return_detail->store_product_id = $order_product->store_product_id;
                        $order_return_detail->product_name = $order_product->product_name;
                        $order_return_detail->product_reference = $order_product->reference;
                        $order_return_detail->product_quantity = $order_product->product_quantity;
                        $order_return_detail->product_unit = $order_product->product_unit;
                        $order_return_detail->product_image_url = $order_product->product_image_url;
                        $order_return_detail->product_quantity_returned = $order_product->quantity_returned;
                        $order_return_detail->status = 'Pendiente';
                        $order_return_detail->save();
                    }
                }
            }
        }

        $log = new OrderLog();
        $log->type = 'Detalle de devolución a bodega # '.$order_return->id.' actualizada.';
        $log->admin_id = Session::get('admin_id');
        $log->order_id = $order_return->order_id ;
        $log->save();

        return ['status' => true, 'message' => 'Se ha actualizado la devolución a bodega con éxito.'];
    }

    /**
     * Actualiza el detalle de la devolución de un pedido que se le agregó nota crédito
     */
    public static function updateOrderReturnFromCreditNote($id)
    {
        if (!$order = Order::with('orderGroup')->find($id)) {
            return ['status' => false, 'message' => 'El pedido no existe.'];
        }

        $order_products = OrderProduct::join('store_product_warehouses', 'order_products.store_product_id', '=', 'store_product_warehouses.store_product_id')
            ->where('order_products.order_id', $id)
            ->whereNotNull('order_products.quantity_credit_note')
            ->whereNotNull('order_products.reason_credit_note')
            ->where('store_product_warehouses.warehouse_id', $order->orderGroup->warehouse_id)
            ->where('store_product_warehouses.manage_stock', 1)
            ->get();
        $order_products_returned = OrderProduct::join('store_product_warehouses', 'order_products.store_product_id', '=', 'store_product_warehouses.store_product_id')
            ->where('order_products.order_id', $id)
            ->where('order_products.fulfilment_status', 'Returned')
            ->where('store_product_warehouses.warehouse_id', $order->orderGroup->warehouse_id)
            ->where('store_product_warehouses.manage_stock', 1)
            ->get();

        if($order_products_returned){
            foreach ($order_products_returned as $order_product_returned){
                $order_products[] = $order_product_returned;
            }
        }

        $order_product_groups = OrderProductGroup::join('store_product_warehouses', 'order_product_group.store_product_id', '=', 'store_product_warehouses.store_product_id')
            ->where('order_product_group.order_id', $id)
            ->whereNotNull('order_product_group.quantity_credit_note')
            ->whereNotNull('order_product_group.reason_credit_note')
            ->where('store_product_warehouses.warehouse_id', $order->orderGroup->warehouse_id)
            ->where('store_product_warehouses.manage_stock', 1)
            ->get();
        $order_product_groups_returned = OrderProductGroup::join('store_product_warehouses', 'order_product_group.store_product_id', '=', 'store_product_warehouses.store_product_id')
            ->where('order_product_group.order_id', $id)
            ->where('order_product_group.fulfilment_status', 'Returned')
            ->where('store_product_warehouses.warehouse_id', $order->orderGroup->warehouse_id)
            ->where('store_product_warehouses.manage_stock', 1)
            ->get();

        if($order_product_groups_returned){
            foreach ($order_product_groups_returned as $order_product_group_returned){
                $order_product_groups[] = $order_product_group_returned;
            }
        }

        $order_return = OrderReturn::where('order_id', $order->id)->first();
        if (!count($order_products) && !count($order_product_groups)) {
            OrderReturnDetail::where('order_return_id', $order_return->id)->delete();
            return ['status' => true, 'message' => 'Se eliminó el registro de devolucion a bodega.'];
        }

        if(count($order_product_groups)) {
            foreach ($order_product_groups as $key => $order_product_group) {
                $order_product_group->type = OrderProduct::where('id', $order_product_group->order_product_id)->pluck('type');
                $order_products[] = $order_product_group;
            }
        }

        if (!$order_return) {
            //crear devolucion a bodega
            $order_return = new OrderReturn;
            $order_return->order_id = $order->id;
            if (!empty($order->vehicle_id))
            {
                $vehicle = Vehicle::find($order->vehicle_id);
                $driver = Driver::find($order->driver_id);
                $transporter = Transporter::find($vehicle->transporter_id);
                $order_return->transporter_name = $transporter->fullname;
                $order_return->driver_name = $driver->first_name.' '.$driver->last_name;
                $order_return->vehicle_plate = $vehicle->plate;
            }
            if (Session::has('admin_id')) {
                $order_return->admin_id = Session::get('admin_id');
            } else {
                $order_return->driver_id =  $order->driver_id;
            }
            $order_return->reject_reason = 'Devolución proveniente de nota crédito #' .$order->credit_note_number . '.';
            $order_return->status = 'Pendiente';
            $order_return->date = date('Y-m-d H:i:s');
            $order_return->save();

            $log = new OrderLog();
            $log->type = 'Devolución a bodega # '.$order_return->id.' creada.';
            if (Session::has('admin_id')) {
                $log->admin_id =  Session::get('admin_id');
            } else {
                $log->driver_id =  $order->driver_id;
            }
            $log->order_id = $order_return->order_id ;
            $log->save();

        } else if ($order_return->status != 'Pendiente') {
            return ['status' => false, 'message' => 'El estado de la devolución a bodega no es valido.'];
        }

        //eliminar detalle de devolucion a bodega
        OrderReturnDetail::where('order_return_id', $order_return->id)->delete();

        //crear nuevo detalle de devolucion a bodega
        foreach ($order_products as $order_product) {
            if ($order_product->type == 'Agrupado') {
                $order_product_group = OrderProductGroup::where('order_product_id', $order_product->order_product_id)
                    ->where('store_product_id', $order_product->store_product_id)
                    ->whereNotNull('quantity_credit_note')->get();

                foreach($order_product_group as $product_group) {
                    $quantity = $product_group->quantity_credit_note;
                    if($product_group->reference_returned) {
                        $store_product = StoreProduct::join('products', 'products.id', '=', 'product_id')
                            ->where('store_id', $order->store_id)
                            ->where('products.reference', $product_group->reference_returned)
                            ->select('store_products.id', 'products.name', 'products.quantity', 'products.unit', 'products.image_medium_url', 'products.reference')
                            ->first();

                        for($i = 1; $i <= $quantity; $i++) {
                            $order_return_detail = new OrderReturnDetail;
                            $order_return_detail->order_return_id = $order_return->id;
                            $order_return_detail->store_product_id = $store_product->id;
                            $order_return_detail->product_name = $store_product->name;
                            $order_return_detail->product_reference = $store_product->reference;
                            $order_return_detail->product_quantity = $store_product->quantity;
                            $order_return_detail->product_unit = $store_product->unit;
                            $order_return_detail->product_image_url = $store_product->image_medium_url;
                            $order_return_detail->status = 'Pendiente';
                            $order_return_detail->save();
                        }
                    } else {
                        for($i = 1; $i <= $quantity; $i++) {
                            $order_return_detail = new OrderReturnDetail;
                            $order_return_detail->order_return_id = $order_return->id;
                            $order_return_detail->store_product_id = $product_group->store_product_id;
                            $order_return_detail->product_name = $product_group->product_name;
                            $order_return_detail->product_reference = $product_group->reference;
                            $order_return_detail->product_quantity = $product_group->product_quantity;
                            $order_return_detail->product_unit = $product_group->product_unit;
                            $order_return_detail->product_image_url = $product_group->product_image_url;
                            $order_return_detail->product_quantity_returned = $product_group->quantity_credit_note;
                            $order_return_detail->status = 'Pendiente';
                            $order_return_detail->save();
                        }
                    }
                }
            } else {
                $quantity = $order_product->quantity_credit_note;
                if($order_product->reference_returned){
                    $store_product = StoreProduct::join('products', 'products.id', '=', 'product_id')
                        ->where('store_id', $order->store_id)
                        ->where('products.reference', $order_product->reference_returned)
                        ->select('store_products.id', 'products.name', 'products.quantity', 'products.unit', 'products.image_medium_url', 'products.reference')
                        ->first();

                    for($i = 1; $i <= $quantity; $i++)
                    {
                        $order_return_detail = new OrderReturnDetail;
                        $order_return_detail->order_return_id = $order_return->id;
                        $order_return_detail->store_product_id = $store_product->id;
                        $order_return_detail->product_name = $store_product->name;
                        $order_return_detail->product_reference = $store_product->reference;
                        $order_return_detail->product_quantity = $store_product->quantity;
                        $order_return_detail->product_unit = $store_product->unit;
                        $order_return_detail->product_image_url = $store_product->image_medium_url;
                        $order_return_detail->status = 'Pendiente';
                        $order_return_detail->save();
                    }
                }else{
                    for($i = 1; $i <= $quantity; $i++)
                    {
                        $order_return_detail = new OrderReturnDetail;
                        $order_return_detail->order_return_id = $order_return->id;
                        $order_return_detail->store_product_id = $order_product->store_product_id;
                        $order_return_detail->product_name = $order_product->product_name;
                        $order_return_detail->product_reference = $order_product->reference;
                        $order_return_detail->product_quantity = $order_product->product_quantity;
                        $order_return_detail->product_unit = $order_product->product_unit;
                        $order_return_detail->product_image_url = $order_product->product_image_url;
                        $order_return_detail->product_quantity_returned = $order_product->quantity_credit_note;
                        $order_return_detail->status = 'Pendiente';
                        $order_return_detail->save();
                    }
                }
            }
        }

        $log = new OrderLog();
        $log->type = 'Detalle de devolución a bodega # '.$order_return->id.' actualizada desde nota crédito #'.$order->credit_note_number;
        $log->admin_id = Session::get('admin_id');
        $log->order_id = $order_return->order_id ;
        $log->save();

        return ['status' => true, 'message' => 'Se ha actualizado la devolución a bodega con éxito.'];
    }

    /**
     * Elimina devolucion de pedido
     *
     * @param int $id ID de pedido
     * @param string $order_status Estado de pedido
     * @return boolean True si se elimina o si no existia devolucion a eliminar
     *
     */
    public static function deleteOrderReturn($id, $order_status)
    {
        if (!$order = Order::find($id))
            return false;

        $order_return_deleted = false;

        //eliminar devolucion si esta pendiente
        if ($order_return = OrderReturn::where('order_id', $order->id)->orderBy('id', 'desc')->first())
        {
            if ($order_return->status == 'Pendiente') {
                $order_return->delete();
                OrderReturnDetail::where('order_return_id', $order_return->id)->delete();

                $log = new OrderLog();
                $log->type = 'Devolución a bodega # ' . $order_return->id . ' eliminada.';
                if (Session::has('admin_id'))
                    $log->admin_id = Session::get('admin_id');
                else $log->driver_id = $order->driver_id;
                $log->order_id = $order_return->order_id;
                $log->save();

                $order_return_deleted = true;
            }
        }else $order_return_deleted = true;

        //validar si tiene pedido padre con devolucion pendiente para eliminarla si se entrego el mismo dia
        if (($order_status == 'Delivered') && !empty($order->parent_order_id)
        && $order_return = OrderReturn::where('order_id', $order->parent_order_id)->where('status', 'Pendiente')->first())
        {
            $order_parent = Order::find($order->parent_order_id);
            $management_date = new DateTime($order_parent->management_date);
            $order_return_date = new DateTime($order_return->date);
            if ($management_date->format('Y-m-d') == $order_return_date->format('Y-m-d'))
            {
                $order_return->delete();
                OrderReturnDetail::where('order_return_id', $order_return->id)->delete();

                $log = new OrderLog();
                $log->type = 'Devolución a bodega # '.$order_return->id.' eliminada.';
                if (Session::has('admin_id'))
                    $log->admin_id =  Session::get('admin_id');
                else $log->driver_id =  $order->driver_id;
                $log->order_id = $order_return->order_id ;
                $log->save();
            }
        }

        return $order_return_deleted;
    }

}
