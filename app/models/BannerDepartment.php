<?php

/**
 * Class Banner
 */
class BannerDepartment extends \Illuminate\Database\Eloquent\Model
{
    /**
     * {@inheritdoc}
     */
	protected $table = 'banner_departments';

}
