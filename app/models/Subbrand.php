<?php
class Subbrand extends \Illuminate\Database\Eloquent\Model
{
	protected $table = 'subbrands';

	public function getSubbrands()
	{
		$subbrand = Subbrand::where('brand_id', $this->id)->get();
		return $subbrand;
	}
}
?>