<?php

class PasswordReminder extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'password_reminders';
    protected $hidden = array('created_at','updated_at');

}