<?php

/**
 * Class OrderValidationReason
 */
class OrderValidationReason extends \Illuminate\Database\Eloquent\Model
{
    const NEW_CREDIT_CARD = 1;
    const FRAUD = 2;
    const MARKETPLACE = 4;

    /**
     * @var string
     */
    protected $table = 'order_validation_reasons';
}
