<?php

use exceptions\MerqueoException;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Warehouse
 * @property int $id
 * @property City $city
 * @property string storage_positions
 */
class Warehouse extends Model
{
    protected $table = 'warehouses';

    const STATUS_ACTIVE = 1;

    const WAREHOUSE_DARKSUPERMARKET_ID  = [1, 4, 12, 14, 15];

    /**
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function zones()
    {
        return $this->hasMany('Zone', 'warehouse_id');
    }

    /**
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo('City', 'city_id');
    }

    /**
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function warehouseStorage()
    {
        return $this->hasMany('WarehouseStorage', 'warehouse_id');
    }

    /**
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function storeProductWarehouse()
    {
        return $this->hasMany('StoreProductWarehouse', 'warehouse_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function slotWarehouse()
    {
        return $this->hasOne(SlotWarehouse::class);
    }

    /**
     *  Valida que una posicion en una bodega no este bloqueada
     * @param int $warehouse_id
     * @param string $action
     * @param string $storageType
     * @param string $heightPosition
     * @param int $position
     * @return bool
     */
    public static function validateBlockedPositions($warehouseId, $action, $storageType, $heightPosition, $position)
    {
        $response = false;

        $blockPositions = InventoryCountingPosition::where('warehouse_id', $warehouseId)
            ->where('action', $action)
            ->where('storage_type', $storageType)
            ->where('status', 'Pendiente')
            ->get();

        foreach ($blockPositions as $blockPosition) {
            $heightPositionBlock = strpos($blockPosition->height_positions, $heightPosition);
            if ($heightPositionBlock !== false) {
                $rangePositionsBlock = explode("-", $blockPosition->positions);
                if ($position >= $rangePositionsBlock[0] && $position <= $rangePositionsBlock[1]) {
                    return true;
                }
            }
        }

        return $response;
    }

    /**
     * Obtiene las posiciones de inventario según almacenamiento
     * y tipo de almacenamiento
     *
     * @param $storage
     * @param $storageType
     * @return mixed
     * @throws MerqueoException
     */
    public function getStoragePosition($storage, $storageType)
    {
        if ($storage == 'Almacenamiento') {
            if ($storageType == 'Frío') {
                return $this->coldStoragePositions;
            }
            if ($storageType == 'Seco') {
                return $this->dryStoragePositions;
            }
        }

        if ($storage == 'Alistamiento') {
            if ($storageType == 'Frío') {
                return $this->pickingColdStoragePositions;
            }
            if ($storageType == 'Seco') {
                return $this->pickingDryStoragePositions;
            }
        }

        throw new MerqueoException('Los tipos de almacenamiento consultados no existen.');
    }

    /**
     * VAlida la existencias de posiciones en la bodega.
     * @param $positions
     * @param $storage
     * @param $storageType
     * @return bool
     * @throws MerqueoException
     */
    public function validateHeightPositions($positions, $storage, $storageType)
    {
        $storagePositions = $this->getStoragePosition($storage, $storageType);
        if (
            count(array_intersect($positions, $storagePositions['height_positions']))
            !== count($positions)
        ) {
            throw new MerqueoException('Las posiciones en altura ingresadas no están disponibles en la bodega');
        }

        return true;
    }

    /**
     * Obtiene las posiciones de picking en seco
     *
     * @return mixed
     */
    public function getPickingDryStoragePositionsAttribute()
    {
        $picking = json_decode($this->storage_positions, true);
        $pickingDryStoragePosition = $picking['picking']['Seco'];
        return $pickingDryStoragePosition;
    }

    /**
     * Obtiene las posiciones de picking en frío
     *
     * @return mixed
     */
    public function getPickingColdStoragePositionsAttribute()
    {
        $picking = json_decode($this->storage_positions, true);
        $pickingColdStoragePosition = $picking['picking']['Frío'];
        return $pickingColdStoragePosition;
    }

    /**
     * Obtiene las posiciones en almacenamiento en seco
     * @return mixed
     */
    public function getDryStoragePositionsAttribute()
    {
        $storage = json_decode($this->storage_positions, true);
        $dryStoragePosition = $storage['storage']['Seco'];
        return $dryStoragePosition;
    }

    /**
     * Obtiene las posiciones en almacenamiento en frío
     * @return mixed
     */
    public function getColdStoragePositionsAttribute()
    {
        $storage = json_decode($this->storage_positions, true);
        $coldStoragePosition = $storage['storage']['Frío'];
        return $coldStoragePosition;
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function toArray($options = 0)
    {
        $result = parent::toArray($options);
        $result['cold_storage_position'] = $this->ColdStoragePositions;
        $result['dry_storage_position'] = $this->DryStoragePositions;
        $result['cold_picking_position'] = $this->PickingColdStoragePositions;
        $result['dry_picking_position'] = $this->PickingDryStoragePositions;

        return $result;
    }

    /**
     * Obtener stock comprometido de producto en bodega
     *
     * @param int $warehouse_id ID de la bodega
     * @param boolean $groupByProduct Bandera para saber si se debe agrupar por producto o bodega
     * @return int $committed_stock Stock comprometido
     */
    public static function getCommittedStock($warehouseId, $groupByProduct = false)
    {
        $simple_committed_stock = Order::whereIn('orders.status', ['Validation', 'Initiated', 'Enrutado', 'In Progress'])
            ->where('order_products.fulfilment_status', 'Pending')
            ->where('order_products.type', 'Product')
            ->where('store_product_warehouses.warehouse_id', $warehouseId)
            ->where('order_groups.warehouse_id', $warehouseId)
            ->join('order_groups', 'orders.group_id', '=', 'order_groups.id')
            ->join('order_products', 'order_products.order_id', '=', 'orders.id')
            ->join('store_products', 'store_products.id', '=', 'order_products.store_product_id')
            ->join('products', 'products.id', '=',  'store_products.product_id')
            ->join('store_product_warehouses', 'store_product_warehouses.store_product_id', '=', 'store_products.id');
        if (!$groupByProduct)
            $simple_committed_stock = $simple_committed_stock->sum('order_products.quantity');
        else {
            $simple_committed_stock = $simple_committed_stock
                ->select(
                    DB::raw('SUM(order_products.quantity) quantity'),
                    'products.id AS product_id',
                    'products.reference',
                    'products.quantity AS product_quantity',
                    'products.unit',
                    'products.name',
                    'products.image_small_url',
                    DB::raw('CONCAT(store_product_warehouses.storage_position,"-", store_product_warehouses.storage_height_position) picking_position'),
                    'store_product_warehouses.picking_stock',
                    'store_product_warehouses.maximum_picking_stock'
                )
                ->groupBy('order_products.store_product_id')->get();

            $simple_committed_stock->each(function ($order) use ($warehouseId) {
                $order = WarehouseStorage::getPositionNextExpiration($order, $warehouseId);
            });
        }

        $group_committed_stock = Order::whereIn('orders.status', ['Validation', 'Initiated', 'Enrutado', 'In Progress'])
            ->where('order_product_group.fulfilment_status', 'Pending')
            ->where('order_products.type', 'Agrupado')
            ->where('store_product_warehouses.warehouse_id', $warehouseId)
            ->where('order_groups.warehouse_id', $warehouseId)
            ->join('order_groups', 'orders.group_id', '=', 'order_groups.id')
            ->join('order_products', 'order_products.order_id', '=', 'orders.id')
            ->join('store_products', 'store_products.id', '=', 'order_products.store_product_id')
            ->join('products', 'products.id', '=',  'store_products.product_id')
            ->join('order_product_group', 'order_product_group.order_product_id', '=', 'order_products.id')
            ->join('store_product_warehouses', 'store_product_warehouses.store_product_id', '=', 'order_product_group.store_product_id');
        if (!$groupByProduct)
            $group_committed_stock = $group_committed_stock->sum('order_product_group.quantity');
        else {
            $group_committed_stock = $group_committed_stock
                ->select(
                    DB::raw('SUM(order_product_group.quantity) quantity'),
                    'products.id AS product_id',
                    'products.reference',
                    'products.quantity AS product_quantity',
                    'products.unit',
                    'products.name',
                    'products.image_small_url',
                    DB::raw('CONCAT(store_product_warehouses.storage_position,"-", store_product_warehouses.storage_height_position) picking_position'),
                    'store_product_warehouses.picking_stock',
                    'store_product_warehouses.maximum_picking_stock'
                )
                ->groupBy('order_product_group.store_product_id')->get();

            $group_committed_stock->each(function ($order) use ($simple_committed_stock, $warehouseId) {
                $order = WarehouseStorage::getPositionNextExpiration($order, $warehouseId);
                $simple_committed_stock->push($order);
            });
        }

        if (count($simple_committed_stock) || count($group_committed_stock))
            return $simple_committed_stock;
        else
            return $simple_committed_stock + $group_committed_stock;
    }
}
