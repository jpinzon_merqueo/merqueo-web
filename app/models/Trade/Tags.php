<?php

namespace Trade;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Tags
 * @property int id
 * @property string title
 * @property string description
 * @property string button_text
 * @property string tag_text
 * @property string url_styles
 * @property string start_date
 * @property string expiration_date
 * @property int status
 * @property string created_at
 * @property string updated_at
 * @package App\Models\Trade
 * @author Jeremy Reyes B. <jreyes@merqueo.com>
 */
class Tags extends Model
{
    /**
     * @var int
     */
    const ENABLED_STATUS = 1;

    /**
     * @var int
     */
    const DISABLED_STATUS = 0;

    /**
     * @var string
     */
    protected $table = 'tags';
}