<?php

namespace Trade;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class StoreProductTags
 * @property int id
 * @property int store_product_id
 * @property int tag_id
 * @property string created_at
 * @property string updated_at
 * @package App\Models\Trade
 * @author Jeremy Reyes B. <jreyes@merqueo.com>
 */
class StoreProductTags extends Model
{
    /**
     * @var string
     */
    protected $table = 'store_product_tags';

    /**
     * @return BelongsTo
     */
    public function tags()
    {
        return $this->belongsTo(Tags::class, 'tag_id');
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeActiveTag(Builder $query)
    {
        $currentDatetime = Carbon::now()->toDateTimeString();

        return $query->select(
            // Necessary to get tags
            'store_product_tags.*',
            // Tags information
            'tags.title',
            'tags.description',
            'tags.button_text',
            'tags.tag_text',
            'tags.url_styles',
            'tags.updated_at AS last_updated'
        )
            ->join(
                'tags',
                'tags.id',
                '=',
                'store_product_tags.tag_id'
            )
            ->where('tags.status', Tags::ENABLED_STATUS)
            ->whereRaw("'$currentDatetime' BETWEEN tags.start_date AND tags.expiration_date")
            ->orderBy('tags.expiration_date')
            ->first();
    }
}