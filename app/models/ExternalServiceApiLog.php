<?php

use \Illuminate\Database\Eloquent\Model;

/**
 * Class ExternalServiceApiLog
 */
class ExternalServiceApiLog extends Model
{
    protected $table = 'external_service_api_log';

    protected $hidden = ['created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function admin()
    {
        return $this->belongsTo('Admin');
    }
}