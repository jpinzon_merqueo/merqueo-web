<?php

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $store_product_id
 * @property int $quantity
 * @property int $quantity_full_price
 * @property int $special_price
 * @property string $product_name
 * @property float $price
 * @property int $cart_id
 * @property int $added_by
 * @property StoreProduct $storeProduct
 * @property mixed cart_quantity
 * @property mixed cart_quantity_full_price
 * @property string store_id
 */
class CartProduct extends Model
{
    protected $table = 'cart_products';

    /**
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function cart()
    {
        return $this->belongsTo('Cart');
    }

    public function product()
    {
        return $this->belongsTo('Product');
    }

    public function store()
    {
        return $this->belongsTo('Store');
    }

    /**
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function storeProduct()
    {
        return $this->belongsTo(StoreProduct::class);
    }
}
