<?php

/**
 * @property int inventory_counting_position_id
 * @property int store_product_id
 * @property int warehouse_id
 * @property string storage
 * @property int position
 * @property int height_position
 * @property int quantity
 */
class InvestigationStock extends Eloquent
{

    protected $table = 'investigation_stocks';
    const TIPIFICATION_LIST = [
        'Sobrantes',
        'Faltante por pérdida',
        'Faltante por averías en recibo',
        'Faltante por averías en almacenamiento',
        'Faltante por averías en picking',
        'Faltante por averías en packing',
        'Faltante por averías en despacho',
        'Faltante por averías en entrega al cliente',
        'Faltante por obsoleto',
        'Faltante por lenta rotación',
        'Faltante por donación',
        'Faltante por cambio a proveedor',
        'Desconocido'
    ];

    protected $fillable = [
        'quantity',
        'warehouse_id',
        'store_product_id',
        'entity_id',
        'module',
        'inventory_counting_position_id',
        'storage',
        'position',
        'height_position',
        'admin_id',
        'product_group'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function inventoryCountingPosition()
    {
        return $this->belongsTo(InventoryCountingPosition::class, 'inventory_counting_position_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function storeProduct()
    {
        return $this->belongsTo(StoreProduct::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function admin()
    {
        return $this->belongsTo(Admin::class, 'admin_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function closeAdmin()
    {
        return $this->belongsTo(Admin::class, 'close_admin_id');
    }


    /**
     * Obtener productos en stock de investigación
     * @param int $warehouseId identificador de la bodega
     * @param string $module nombre del módulo del que se registró el stock de investigación
     * @param product $product nombre o referencia del producto en stock de investigación
     * @param date $startDate fecha inicio en que se creo el registro en stock de investigación
     * @param date $endDate fecha fin en que se creó el registro en stock de investigación
     * @return $investigationStocks listado de registros en stock de investigación
     */
    public function getStockInvestigation($warehouseId, $module, $status, $product = '', $startDate, $endDate)
    {
        $investigationStocks = InvestigationStock::whereHas('storeProduct.product', function ($query) use ($product) {
            if ($product != '') {
                $query->where(function ($query) use ($product) {
                    $query->orwhere('name', 'like', '%' . $product . '%')
                        ->orWhere('reference', 'like', '%' . $product . '%');
                });
            }
        })->with('storeProduct.product')
            ->where('warehouse_id', $warehouseId)
            ->whereBetween( DB::raw('DATE(created_at)'), [$startDate, $endDate]);
        if ($module) {
            $investigationStocks->where('module', $module);
        }
        if ($status) {
            $investigationStocks->where('status', $status);
        }


        return $investigationStocks->orderBy('id', 'Desc')->limit(300)->get();
    }

    /**
     * Obtener los movimientos de un producto en stock de investigación
     * @param int $storeProductId identificador del store_product
     * @param int $warehouseId identificador de la bodega
     * @param date $startDate fecha de creación inicial para la búsqueda
     * @param date $endDate fecha de creación final para la búsqueda
     * @return InvestigationStocks
     */
    public function getStorageStockInvestigation($storeProductId, $warehouseId, $startDate, $endDate)
    {

        return InvestigationStock::with('storeProduct.product', 'inventoryCountingPosition.admin', 'admin',
            'closeAdmin')
            ->where('store_product_id', $storeProductId)
            ->where('warehouse_id', $warehouseId)
            ->whereBetween( DB::raw('DATE(created_at)'), [$startDate, $endDate])
            ->orderBy('id', 'Desc')
            ->get();
    }

    /**
     * Actualiza el estado y la clasificación de un stock en investigación
     * @param int $investigationId identificador del stock en investigación
     * @param string $status estado del stock en investigación que puede ser pendiente o cerrado
     * @param string $typification clasificación del stock en investigación
     */
    public function updateInvestigationStock($investigationId, $status, $typification)
    {
        $investigationStock = $this::findOrfail($investigationId);
        $investigationStock->status = $status;
        $investigationStock->typification = $typification;
        $investigationStock->close_admin_id = Session::get('admin_id');
        $investigationStock->save();
        return $investigationStock;
    }

}
