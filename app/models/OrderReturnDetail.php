<?php

class OrderReturnDetail extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'order_return_details';
    protected $hidden = array('created_at','updated_at');

    /**
     * Relación con el modelo OrderReturn
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function orderReturn()
    {
        return $this->belongsTo('OrderReturn');
    }

    /**
     * Relación con el modelo StoreProduct
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function storeProduct()
    {
        return $this->belongsTo('StoreProduct');
    }

}