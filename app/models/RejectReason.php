<?php

/**
 * Class RejectReason
 * @property string $reason
 * @property string $attendant
 */
class RejectReason extends \Illuminate\Database\Eloquent\Model
{
    /**
     * @const Este tipo de razones de cancelación
     * deben aparecen en el help center.
     */
    const TYPE_USER = 'user';

    /**
     * @const Este tipo de razones de cancelación
     * aparecen en el dash y solo pueden ser
     * seleccionados por los usuarios internos.
     */
    const TYPE_ADMIN = 'admin';

    /**
     * @const Este tipo de razones de cancelación
     * no se muestran en ninguna parte, solo
     * se usan en cancelaciones internas.
     */
    const TYPE_HIDDEN = 'hidden';
    const TYPE_SERVICE = 'service';
    const TYPE_REFUND = 'refund';

    /**
     * {@inheritdoc}
     */
    protected $table = 'reject_reasons';
    public $timestamps = false;

    /**
     * @param $query
     * @return mixed
     */
    public function scopeHelpCenter($query)
    {
        return $query->where('type', self::TYPE_USER);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeHidden($query)
    {
        return $query->where('type', self::TYPE_HIDDEN);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeService($query)
    {
        return $query->where('type', self::TYPE_SERVICE);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeAdmin($query)
    {
        return $query->where('type', self::TYPE_ADMIN);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeRefund($query)
    {
        return $query->where('type', self::TYPE_REFUND);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeActive($query)
    {
        return $query->where('is_active', 1);
    }
}
