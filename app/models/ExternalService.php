<?php

use \Illuminate\Database\Eloquent\Model;

/**
 * Class ExternalService
 * @property ExternalServicePayment[] $payments
 * @property UserCreditCard $creditCard
 * @property User $user
 * @property string $params
 * @property string $user_email
 * @property string $status
 * @property integer id
 */
class ExternalService extends Model
{
    protected $table = 'external_services';
    protected $hidden = ['created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function payments()
    {
        return $this->hasMany('ExternalServicePayment');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo('City', 'user_city_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function logs()
    {
        return $this->hasMany('ExternalServiceLog');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function userCreditCard()
    {
        return $this->belongsTo('UserCreditCard', 'credit_card_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function credits()
    {
        return $this->hasMany('UserCredit', 'external_service_id');
    }

    /**
     * @param bool $verifyOrders
     * @return array
     */
    public function chargeCreditCard($verifyOrders = true)
    {
        $user_credit_card = $this->userCreditCard;

        // validamos si no tiene ordenes Delivered la tarjeta
        if (!$user_credit_card->haveOrders() && $verifyOrders) {

            $this->status = 'Validación';
            $this->save();

            $params = json_decode($this->params,true);

            // send email de validacion.
            $mail = [
                'template_name' => 'emails.soat_valid',
                'subject' => 'Estamos validando tu compra de SOAT ',
                'to' => [
                    ['email' => $params["email"], 'name' => $params["taker"]["firstName"]." ".$params["taker"]["lastName"] ],
                    ['email' => $params["buyer"]["email"], 'name' => $params["buyer"]["name"]  ]
                ],
                'vars' => []
            ];

            send_mail($mail);

            return [
                'status' => true,
                'message' => 'Tarjeta Pendiente de Validacion',
                'result' => 'PENDING',
            ];
        }

        $total = $this->total;

        if ($user_credit_card->user_id != $this->user_id) {
            return [
                'status' => false,
                'message' => 'Error',
                'result' => ''
            ];
        }

        $data['total_amount'] = $total;
        $data['tax_amount'] = 0; //$total * 0.16;
        $data['card_token'] = $this->cc_token;
        $data['order_id'] = $this->id;
        $data['user_id'] = $this->user_id;
        $data['installments_cc'] = $this->cc_installments ? $this->cc_installments : 1;
        $data['description'] = 'Compra en Merqueo';

        $external_service_payment = new ExternalServicePayment();


        // PayU
        if ($user_credit_card->created_at >= Config::get('app.payu.start_date')) {
            $data['total_base'] = 0;
            $data['iva'] = 0;
            $data['card_type'] = $this->cc_type;
            $data['security_code'] = $this->cc_type == 'AMEX' ? '0000' : '000';
            $data['device_session_id'] = md5(session_id() . microtime());
            $data['referenceCode'] = $this->reference;

            $user = $this->user;

            $data['buyer']['document_number'] = $user->identity_number;
            $data['buyer']['fullname'] = $this->user_name;
            $data['buyer']['email'] = $this->user_email;
            $data['buyer']['phone'] = $this->user_phone;
            $data['buyer']['address'] = $this->user_address;
            $data['buyer']['address_further'] = '';
            $city = City::find($this->user_city_id);
            $data['buyer']['city'] = $city->city;
            $data['buyer']['state'] = $city->state;

            $data['payer']['document_number'] = $this->cc_holder_document_number;
            $data['payer']['document_type'] = $this->cc_holder_document_type;
            $data['payer']['fullname'] = $this->cc_holder_name;
            $data['payer']['email'] = $this->cc_holder_email;
            $data['payer']['phone'] = $this->cc_holder_phone;
            $data['payer']['address'] = $this->cc_holder_address;
            $data['payer']['address_further'] = $this->cc_holder_address_further;
            $data['payer']['city'] = $this->cc_holder_city_id;

            $payu = new PayU();

            $result = $payu->createCreditCardCharge($data);
            //var_dump($result);
            if (!$result['status']) {
                $msg = isset($result['response']->transactionResponse->responseCode)
                    ? $payu->getErrorMessage($result['response']->transactionResponse->responseCode)
                    : 'No especificado.';

                $external_service_payment->cc_payment_status = 'Rechazada';
                $this->save();
                return [
                    'status' => false,
                    'message' => 'Cobro a tarjeta rechazado. Motivo: ' . $msg,
                    'result' => ''
                ];
            }

            if ($result['response']->transactionResponse->state == 'APPROVED') {
                $external_service_payment->cc_payment_status = 'Aprobada';
                $this->status = 'Aprobada';
                $this->payment_date = date('Y-m-d H:i:s');
                $this->managment_date = date('Y-m-d H:i:s');
                \ExternalServiceLog::make($this, 'Transacción aceptada.');
            } elseif ($result['response']->transactionResponse->state == 'DECLINED') {
                $external_service_payment->cc_payment_status = 'Declinada';
                return [
                    'status' => false,
                    'message' => 'Cobro a tarjeta Declinado.',
                    'result' => ''
                ];
            } elseif ($result['response']->transactionResponse->state == 'EXPIRED') {
                $external_service_payment->cc_payment_status = 'Expirada';
                return [
                    'status' => false,
                    'message' => 'Cobro a tarjeta Expirado.',
                    'result' => ''
                ];
            }

            $external_service_payment->cc_payment_description = $result['response']->transactionResponse->responseCode;
            $external_service_payment->cc_payment_transaction_id = $result['response']->transactionResponse->transactionId;
            $external_service_payment->cc_charge_id = $result['response']->transactionResponse->orderId;
            $external_service_payment->cc_payment_date = date('Y-m-d H:i:s');
        } else {
            if ($this->pending_payment_response) {
                $response = [
                    'status' => false,
                    'message' => 'No se puede cobrar el pedido por que tiene un cobro en proceso en TPAGA.',
                    'result' => ''
                ];
                return $response;
            }

            //bloquear pedido para no hacer mas de un cobro
            $this->pending_payment_response = 1;
            $this->save();

            $data['nit'] = '9008714448'; // NIT merqueo defecto
            $payment = new Payment();
            $result = $payment->createCreditCardCharge($data);
            //desbloquear pedido para no permitir cobro
            $this->pending_payment_response = 0;
            $this->save();

            if (!$result['status']) {
                $msg = isset($result['response']->errorMessage) ? $result['response']->errorMessage : 'No especificado.';
                $external_service_payment->cc_payment_status = 'Rechazada';
                $this->save();
                $external_service_payment->save();
                return [
                    'status' => false,
                    'message' => 'Cobro a tarjeta rechazado. Motivo: ' . $msg,
                    'result' => ''
                ];
            }

            $external_service_payment->cc_charge_id = $result['response']->id;
            $external_service_payment->cc_payment_status = 'Aprobada';
            $this->status = 'Aprobada';

            $external_service_payment->cc_payment_transaction_id = $result['response']->paymentTransaction;
            $external_service_payment->cc_payment_date = date('Y-m-d H:i:s');
            $this->payment_date = date('Y-m-d H:i:s');
            $this->managment_date = date('Y-m-d H:i:s');
        }

        $this->save();
        $external_service_payment->externalService()->associate($this);
        $external_service_payment->save();

        return [
            'status' => true,
            'message' => 'El cobro fue realizado con éxito.',
            'result' => 'OK',
        ];
    }

    /**
     * @param $status
     */
    public function setStatusAttribute($status)
    {
        $available_states = ['Validación', 'Aprobada', 'Cancelado'];
        if (!in_array($status, $available_states)) {
            $available_states = implode(', ', $available_states);
            throw new InvalidArgumentException("'{$status}' no es un estado valido, disponibles '{$available_states}'");
        }

        $this->attributes['status'] = $status;
    }

    /**
     * @return StdClass
     */
    public function getTakerAttribute()
    {
        $params = json_decode($this->attributes['params']);
        $params->taker->email = $params->email;

        return $params->taker;
    }

    /**
     * @return StdClass
     */
    public function getBuyerAttribute()
    {
        $params = json_decode($this->attributes['params']);

        return $params->buyer;
    }

    /**
     * @return StdClass
     */
    public function getPlateAttribute()
    {
        $params = json_decode($this->attributes['params']);

        return $params->plate;
    }

    /**
     * @param $data
     * @return ExternalService
     */
    public static function saveExternal($data)
    {

        $external_service = new ExternalService();
        $external_service->reference = generate_reference();
        $external_service->user_id = $data['user_id'];
        $external_service->credit_card_id = $data['credit_card_id'];
        $external_service->type = $data['type'];
        $external_service->total = $data['amount'];
        $external_service->cc_installments = $data['cc_installments'];

        $user_credit_card = UserCreditCard::findOrFail($external_service->credit_card_id);

        $external_service->cc_token = $user_credit_card->card_token;
        $external_service->cc_holder_name = $user_credit_card->holder_name;
        $external_service->cc_holder_document_type = $user_credit_card->holder_document_type;
        $external_service->cc_holder_document_number = $user_credit_card->holder_document_number;
        $external_service->cc_holder_phone = $user_credit_card->holder_phone;
        $external_service->cc_holder_email = $user_credit_card->holder_email;
        $external_service->cc_holder_address = $user_credit_card->holder_address;
        $external_service->cc_holder_address_further = $user_credit_card->holder_address_further;
        $external_service->cc_holder_city_id = $user_credit_card->holder_city;
        $external_service->cc_last_four = $user_credit_card->last_four;
        $external_service->cc_type = $user_credit_card->type;
        $external_service->cc_country = $user_credit_card->country;
        $external_service->cc_expiration_month = $user_credit_card->expiration_month;
        $external_service->cc_expiration_year = $user_credit_card->expiration_year;

        $external_service->user_name = $data['buyer']['name'];
        $external_service->user_email = $data['buyer']['email'];
        $external_service->user_address = $data['buyer']['address'];
        $external_service->user_phone = $data['buyer']['phone'];
        $external_service->user_city_id = $data['user_city_id'];
        $external_service->params = json_encode($data);
        $external_service->save();

        return $external_service;
    }

    /**
     * Cancela el servicio.
     *
     * @param RejectReason $reject_reason
     * @param $comments
     * @throws \exceptions\MerqueoException
     */
    public function cancel(RejectReason $reject_reason, $comments)
    {
        $last_payment = $this->payments()->where('cc_payment_status', 'Aprobada')->latest()->first();
        $has_refunds = $last_payment ? $last_payment->refunds()->count() > 0 : false;

        if (!($this->status === 'Validación' ||  $has_refunds)) {
            throw new \exceptions\MerqueoException(
                'El estado del servicio debe ser "Validación" o debe tener reembolsos.'
            );
        }

        $this->status = 'Cancelado';
        $this->managment_date = date("Y-m-d H:i:s");
        $this->reject_comments = $comments;
        $this->reject_reason = $reject_reason->reason;
        $this->save();
        if (!empty($last_payment)) {
            $this->removeCredit();
        }

        Event::fire('soat.cancelled', [$this, $reject_reason]);
    }

    /**
     * Genera un reembolso.
     *
     * @param RejectReason $reason
     * @param Admin $admin
     * @return array|ExternalServicePaymentRefund
     * @throws \exceptions\MerqueoException
     */
    public function makeRefund(RejectReason $reason, Admin $admin)
    {
        $last_payment = $this->payments()->where('cc_payment_status', 'Aprobada')->latest()->first();

        if (!$last_payment) {
            throw new \exceptions\MerqueoException('El servicio no tiene un pago aprobado.');
        }

        $has_refunds = $last_payment ? $last_payment->refunds()->where('status', 'Pendiente')->count() > 0 : false;

        if ($has_refunds) {
            throw new \exceptions\MerqueoException('El servicio ya tiene un reembolso pendiente.');
        }

        $payu = new PayU();
        $refund = $payu->refundCreditCardCharge($reason->reason, null, $last_payment);
        if (!$refund['status']) {
            throw new \exceptions\MerqueoException($refund['response']->error);
        }

        $refund = new ExternalServicePaymentRefund();
        $refund->status = 'Pendiente';
        $refund->reason = $reason->reason;
        $refund->date = \Carbon\Carbon::create();
        $refund->admin()->associate($admin);
        $refund->externalServicePayment()->associate($last_payment);
        $refund->save();

        Event::fire('soat.refund', [$this, $reason]);

        return $refund;
    }

    /**
     * @return bool
     */
    public function canMakeRefund()
    {
        return $this->payments()
            ->where('cc_payment_status', 'Aprobada')
            ->count() > 0;
    }

    /**
     * @param ExternalServicePaymentRefund $refund
     * @param PayU $payment_manager
     * @throws \exceptions\MerqueoException
     */
    public function validateStateFromRefund(ExternalServicePaymentRefund $refund, PayU $payment_manager)
    {
        if ($this->status !== 'Aprobada') {
            throw new \exceptions\MerqueoException('El servicio no se encuentra aprobado.');
        }

        $refund->reloadState($payment_manager);

        if ($refund->status === 'Aprobada') {
            $reject_reason = new RejectReason();
            $reject_reason->reason = $refund->reason;
            $this->cancel($reject_reason, 'Reembolso.');
        }
    }

    /**
     * Retorna el credito asignado a un usuario por la compra de SOAT.
     */
    private function removeCredit()
    {
        $credits = $this->credits()
            ->where('status', 1)
            ->where('type', 1)
            ->with('user')
            ->get();

        foreach ($credits as $credit) {
            UserCredit::removeCredit($credit->user, $credit->amount, 'external_service', null, null, $this);
        }
    }
}