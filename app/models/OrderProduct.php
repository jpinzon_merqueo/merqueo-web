<?php

/**
 * @property \Illuminate\Support\Collection|OrderProductGroup[] orderProductGroup
 * @property StoreProduct storeProduct
 * @property mixed price
 * @property mixed base_price
 * @property mixed original_price
 */
class OrderProduct extends \Illuminate\Database\Eloquent\Model
{

    /**
     * @var string
     */
    protected $table = 'order_products';

    /**
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function storeProduct()
    {
        return $this->belongsTo(StoreProduct::class);
    }

    /**
     * @return Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function orderProductScore()
    {
        return $this->hasOne(OrderProductScore::class);
    }

    /**
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orderProductGroup()
    {
        return $this->hasMany(OrderProductGroup::class);
    }

    /**
     * @return mixed
     */
    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    /**
     * Agrega un producto al pedido
     *
     * @param obj    $store_product     Objeto del producto que se desea agregar.
     * @param int    $order_id          id de la orden a la que pertenecerá el producto.
     * @param int    $store_id          id de la tienda del pedido.
     * @param int    $quantity          Cantidad del producto para agregar.
     * @param string $fulfilment_status estado en el que se agregará el pedido
     */
    public static function addProduct(
        $store_product,
        $order,
        $store_id,
        $quantity,
        $fulfilment_status = 'Pending',
        $reason_returned = null
    ) {
        if (is_a($store_product, 'StoreProduct') && is_a($order, 'Order')) {
            if (!in_array($fulfilment_status, ['Fullfilled', 'Not Available', 'Pending', 'Replacement', 'Enrutado', 'Returned'])) {
                $response = [
                    'status'  => false,
                    'message' => 'El estado del producto no es válido',
                ];

                return $response;
            }

            //validar producto en promocion en pedidos anteriores solo en merqueo super
            if (!empty($store_product->special_price)) {
                $count_products = Order::join('order_products', 'order_products.order_id', '=', 'orders.id')
                    ->where('store_product_id', $store_product->product_id)
                    ->where('price', $store_product->special_price)
                    ->where('user_id', $order->user_id)
                    ->where('status', '<>', 'Cancelled')
                    ->where(DB::raw('DATEDIFF(NOW(), date)'), '<=', Config::get('app.minimum_promo_days'))
                    ->groupBy('order_products.store_product_id')
                    ->count();
                if ($count_products) {
                    $response = [
                        'status'  => false,
                        'message' => 'El usuario ya realizó un pedido con el producto '.$store_product->name.' en promoción, por favor eliminalo del carrito de compras para continuar.',
                    ];

                    return $response;
                }
            }

            //validar precio especial y cantidad del producto
            $user_has_orders = Order::select('orders.id')
                ->where('user_id', $order->user_id)
                ->where('status', 'Delivered')
                ->orderBy('id', 'desc')
                ->count();

            if ($quantity > 0 && $store_product->special_price > -1) {
                //validar primera compra
                if ($user_has_orders > 1 && $store_product->first_order_special_price) {
                    $response = [
                        'status'  => false,
                        'message' => 'La promoción del producto '.$store_product->name.' aplica solo para primera compra, por favor eliminalo del carrito para continuar.',
                    ];

                    return $response;
                }
                //validar cantidad del producto por pedido
                if ($store_product->quantity_special_price) {
                    if ($quantity > $store_product->quantity_special_price) {
                        $response = [
                            'status'  => false,
                            'message' => 'El producto '.$store_product->name.' por estar en promoción puedes agregar máximo '.$store_product->quantity_special_price.' unidades en tu pedido, por favor disminuye la cantidad en el carrito para continuar.',
                        ];

                        return $response;
                    }
                }
            }
            $order_product                    = new OrderProduct();
            $order_product->store_id          = $store_id;
            $order_product->order_id          = $order->id;
            $order_product->store_product_id  = $store_product->id;
            $order_product->reference         = $store_product->reference;
            $price                            = $store_product->special_price ? $store_product->special_price : $store_product->price;
            $order_product->price             = $price;
            $order_product->original_price    = $store_product->price;
            $order_product->base_price        = $price / (1 + ($store_product->iva / 100));
            $order_product->iva               = $store_product->iva;
            $order_product->base_cost         = $store_product->base_cost;
            $order_product->cost              = $store_product->cost;
            $order_product->quantity_original = $quantity;
            $order_product->quantity          = $quantity;
            $order_product->fulfilment_status = $fulfilment_status;
            $order_product->reason_returned   = $reason_returned;
            $order_product->type              = $store_product->type == 'Agrupado' ? 'Agrupado' : 'Product';
            $product_with_special_price       = !empty($store_product->special_price);

            if ($product_with_special_price && $store_product->type === 'Simple') {
                $discount                         = $store_product->price - $price;
                $order_product->provider_discount = $discount * $store_product->provider_discount / 100;
                $order_product->merqueo_discount  = $discount * $store_product->merqueo_discount / 100;
                $order_product->seller_discount   = $discount * $store_product->seller_discount / 100;
            }

            $order_product->product_name      = $store_product->name;
            $order_product->product_image_url = $store_product->image_medium_url;
            $order_product->product_quantity  = $store_product->quantity;
            $order_product->product_unit      = $store_product->unit;
            if ($order_product->save()) {
                if ($store_product->type == 'Agrupado') {
                    $store_products_group = StoreProductGroup::select('products.*', 'store_products.*', 'store_product_group_id', 'store_product_group.quantity AS group_quantity')
                        ->join('store_products', 'store_product_group.store_product_group_id', '=', 'store_products.id')
                        ->join('products', 'store_products.product_id', '=', 'products.id')
                        ->where('store_product_id', $store_product->id)
                        ->get();

                    foreach ($store_products_group as $store_product_group) {
                        $order_product_group                    = new OrderProductGroup();
                        $order_product_group->order_id          = $order_product->order_id;
                        $order_product_group->order_product_id  = $order_product->id;
                        $order_product_group->store_product_id  = $store_product_group->store_product_group_id;
                        $order_product_group->fulfilment_status = $fulfilment_status;
                        $order_product_group->reference         = $store_product_group->reference;
                        $order_product_group->product_name      = $store_product_group->name;
                        $order_product_group->product_quantity  = $store_product_group->quantity;
                        $order_product_group->product_unit      = $store_product_group->unit;
                        $order_product_group->quantity          = $order_product->quantity * $store_product_group->group_quantity;
                        $order_product_group->price             = $store_product_group->price;
                        $order_product_group->base_price        = $store_product_group->base_price;
                        $order_product_group->iva               = $store_product_group->iva;
                        $order_product_group->product_image_url = $store_product_group->image_medium_url;

                        if ($product_with_special_price) {
                            $amount = $store_product_group->discount_amount;

                            $order_product->provider_discount += $order_product_group->provider_discount = $amount * $store_product_group->provider_discount / 100;
                            $order_product->merqueo_discount += $order_product_group->merqueo_discount   = $amount * $store_product_group->merqueo_discount / 100;
                            $order_product->seller_discount += $order_product_group->seller_discount     = $amount * $store_product_group->seller_discount / 100;
                        }

                        $order_product_group->save();
                    }

                    $order_product->save();
                }

                return [
                    'status'  => true,
                    'message' => 'Producto agregado.',
                    'obj'     => $order_product,
                ];
            } else {
                return [
                    'status'  => false,
                    'message' => 'Error al guardar el producto.',
                ];
            }
        } else {
            return [
                'status'  => false,
                'message' => 'El objeto del producto no es válido.',
            ];
        }
    }

    /**
     * Guarda producto en detalle de pedido
     *
     * @param  int          $price         Precio del producto
     * @param  int          $quantity      Cantidad comprada
     * @param  object       $store_product Objeto del producto en la tienda
     * @param  object       $order         Objeto del pedido
     * @return OrderProduct $order_product Objeto del producto en pedido
     */
    public static function saveProduct($price, $quantity, $store_product, $order)
    {
        $ivaDiscount = $store_product->iva / 100;
        $additionalIvaDiscount = $store_product->product->additional_iva / 100;
        $original_store_product = StoreProduct::select('price', 'special_price')
            ->find($store_product->id);
        $store_product->type    = $store_product->type
            ?: (!empty($store_product->product->type) ? $store_product->product->type : '');
        $order_product                                   = new OrderProduct();
        $order_product->order_id                         = $order->id;
        $order_product->store_id                         = $store_product->store_id;
        $order_product->store_product_id                 = $store_product->id;
        $order_product->reference                        = $store_product->reference ?: $store_product->product->reference;
        $order_product->price                            = $price;
        $order_product->original_price                   = $original_store_product->price;
        $order_product->base_price                       = $price / (1 + ($ivaDiscount + $additionalIvaDiscount));
        $order_product->iva                              = $store_product->iva;
        $order_product->base_cost                        = $store_product->base_cost;
        $order_product->cost                             = $store_product->cost;
        $order_product->additional_iva                   = $store_product->additional_iva;
        $order_product->quantity_original                = $quantity;
        $order_product->quantity                         = $quantity;
        $order_product->is_gift                          = $store_product->is_gift ? 1 : 0;
        $order_product->parent_id                        = isset($store_product->parent_id) ? $store_product->parent_id : 0;
        $order_product->sampling_id                      = isset($store_product->sampling_id) ? $store_product->sampling_id : null;
        $order_product->campaign_gift_id                 = isset($store_product->campaign_gift_id) ? $store_product->campaign_gift_id : null;
        $order_product->picking_date                     = isset($store_product->picking_date) ? $store_product->picking_date : null;
        $order_product->fulfilment_status                = $store_product->is_gift ? $store_product->fulfilment_status : 'Pending';
        $order_product->type                             = $store_product->type == 'Agrupado' ? 'Agrupado' : ($store_product->type == 'Muestra' ? 'Muestra' : 'Product');
        $order_product->product_name                     = $store_product->name ?: $store_product->product->name;
        $order_product->product_delivery_discount_amount = $store_product->delivery_discount_amount;
        $order_product->product_image_url                = $store_product->image_medium_url
            ?: (!empty($store_product->product->image_medium_url) ? $store_product->product->image_medium_url : '');
        $order_product->product_quantity = $store_product->quantity
            ?: (!empty($store_product->product->quantity) ? $store_product->product->quantity : '');
        $product_with_special_price = !empty($store_product->special_price) && $price < $original_store_product->price;
        $discount                   = $original_store_product->price - $price;

        if ($product_with_special_price && $store_product->type === 'Simple') {
            $order_product->provider_discount = $discount * $store_product->provider_discount / 100;
            $order_product->merqueo_discount  = $discount * $store_product->merqueo_discount / 100;
            $order_product->seller_discount   = $discount * $store_product->seller_discount / 100;
        }

        if ($product_with_special_price) {
            $order_product->quantity_special_price_stock = $store_product->quantity_special_price ?: 0;
        }

        $order_product->product_unit = $store_product->unit
            ?: (!empty($store_product->product->unit) ? $store_product->product->unit : '');
        $order_product->product_comment = '';
        $order_product->save();
        
        // Guardar producto agrupado.
        if ($store_product->type == 'Agrupado') {
            $store_products_group = StoreProductGroup::select(
                'products.*', 'store_products.*', 'store_product_group_id', 'store_product_group.seller_discount',
                'store_product_group.quantity AS group_quantity', 'store_product_group.discount_amount',
                'store_product_group.provider_discount', 'store_product_group.merqueo_discount'
            )
                ->join('store_products', 'store_product_group.store_product_group_id', '=', 'store_products.id')
                ->join('products', 'store_products.product_id', '=', 'products.id')
                ->where('store_product_id', $store_product->id)
                ->get();

            foreach ($store_products_group as $store_product_group) {
                $discountAmount = 0;
                $order_product_group                    = new OrderProductGroup();
                $order_product_group->order_id          = $order_product->order_id;
                $order_product_group->order_product_id  = $order_product->id;
                $order_product_group->store_product_id  = $store_product_group->store_product_group_id;
                $order_product_group->fulfilment_status = 'Pending';
                $order_product_group->reference         = $store_product_group->reference;
                $order_product_group->product_name      = $store_product_group->name;
                $order_product_group->product_quantity  = $store_product_group->quantity;
                $order_product_group->product_unit      = $store_product_group->unit;
                $order_product_group->quantity          = $order_product->quantity * $store_product_group->group_quantity;
                $order_product_group->quantity_original = $order_product_group->quantity;
                $order_product_group->iva               = $store_product_group->iva;
                $order_product_group->product_image_url = $store_product_group->image_medium_url;

                if ($product_with_special_price) {
                    $discountAmount                         = $store_product_group->discount_amount;
                    $order_product_group->provider_discount = $discountAmount * $store_product_group->provider_discount / 100;
                    $order_product_group->merqueo_discount  = $discountAmount * $store_product_group->merqueo_discount / 100;
                    $order_product_group->seller_discount   = $discountAmount * $store_product_group->seller_discount / 100;
                }

                $order_product_group->original_price = $store_product_group->price;
                $order_product_group->price          = $store_product_group->price - ($discountAmount / $store_product_group->group_quantity);
                $order_product_group->base_price     = $order_product_group->price / (1 + ($store_product_group->iva / 100));
                $order_product_group->save();
            }

            $order_product->save();
        }

        return $order_product;
    }

    /**
     * Funcion que elimina los productos de sampling asociados a un producto del pedido
     */
    public function deleteSamplingProductChild()
    {
        $child_products = self::where('parent_id', $this->store_product_id)
            ->where('type', 'Muestra')
            ->where('order_id', $this->order_id)
            ->get();

        if (count($child_products)) {
            foreach ($child_products as $child_product) {
                $child_product->delete();
            }
        }
    }

    // *************************************************************************
    // Accessors
    // *************************************************************************

    /**
     * Obtiene el valor total de este producto.
     */
    public function getTotalAttribute()
    {
        return $this->quantity * $this->price;
    }

    /**
     * Permite actualizar los precios de los productos para el convenio con Domicilios.com
     */
    public function darkSuperMarketUpdate()
    {
        $this->price      = $this->storeProduct->price;
        $this->base_price = $this->price / (1 + ($this->iva / 100));
        $this->original_price = $this->storeProduct->price;
        $this->save();
    }


    /**
     * Obtiene el total del precio base multiplicando la cantidad del producto por el precio base
     *
     * @return float
     */
    public function getTotalBasePriceAttribute() {
        return round($this->quantity * $this->base_price);
    }

    /**
     * Obtiene el precio original del producto sin iva
     *
     * @return float
     */
    public function getBaseOriginalPriceAttribute(){
        return round($this->original_price / (($this->iva / 100) + 1));
    }

    /**
     * Obtiene el total del precio original del producto multiplicando la cantidad por el precio base original
     *
     * @return float
     */
    public function getTotalBaseOriginalPriceAttribute()
    {
        return round($this->quantity * $this->base_original_price);
    }

    /**
     * Obtiene el descuento total aplicado al producto
     *
     * @return mixed
     */
    public function getTotalProductDiscountAttribute()
    {
        return $this->total_base_original_price - $this->total_base_price;
    }

    /**
     * Obtiene el sub total del producto
     *
     * @return mixed
     */
    public function getSubTotalProductAttribute()
    {
        return $this->total_base_original_price - $this->total_product_discount;
    }

    /**
     * Obtiene total del iva aplicado al precio base originaL
     *
     * @return float
     */
    public function getTotalIvaBaseOriginalPriceAttribute()
    {
        return round($this->sub_total_product * ($this->iva / 100));
    }

    /**
     * Obtiene el total final del producto aplicando descuentos y el iva
     *
     * @return mixed
     */
    public function getTotalProductAttribute()
    {
        return $this->sub_total_product + $this->total_iva_base_original_price;
    }
}
