<?php

class ProviderOrderReceptionDetailProductGroup extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'provider_order_reception_detail_product_group';
    protected $hidden = array('created_at','updated_at');

    public static function boot()
    {
        parent::boot();

        static::creating(function ($providerOrderReceptionDetailProductGroup) {
            $base_cost = $providerOrderReceptionDetailProductGroup->base_cost;
            $providerOrderReceptionDetailProductGroup->original_base_cost = $base_cost;
        });
    }

    public function storeProduct()
    {
    	return $this->belongsTo('StoreProduct', 'store_product_id');
    }

    public function providerOrderReceptionDetail($value='')
    {
    	return $this->belongsTo('providerOrderReceptionDetail', 'reception_detail_id');
    }

    public function providerOrderReceptionValidate()
    {
        return $this->hasMany('ProviderOrderReceptionValidate', 'reception_detail_grouped_id');
    }
}