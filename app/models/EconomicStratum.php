<?php
/**
* Class EconomicStratum
*/
class EconomicStratum extends \Illuminate\Database\Eloquent\Model {
	/**
	* @var string $table
	*/
    protected $table = 'economic_stratum';

    /**
    * Devuelve el estrato de la direccion del usuario
    * @param $latitude latitud de la direccion
    * @param $longitude longitud de la direccion
    */
    public static function getStratum($latitude, $longitude)
    {
        $stratums = EconomicStratum::all();       
        foreach ($stratums as $stratum) {
            if (point_in_polygon($stratum->zone, $latitude, $longitude)) {
                return $stratum;
            }
        }
		return null;
    }

}