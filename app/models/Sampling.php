<?php

class Sampling extends \Illuminate\Database\Eloquent\Model
{

    protected $table = 'sampling';
    private static $samples = [];

    /**
     * Retorna la lista de productos de muestra según una bodega
     *
     * @param int $warehose_id id de la bodega
     * @param Object resultado de la consulta
     */
    public static function getSampleProducts($warehouse_id)
    {
        return self::select(
            'sampling.*',
            DB::raw("CONCAT_WS(' ', products.name, products.quantity, products.unit) AS product_name"),
            'products.image_small_url'
        )
            ->join('store_products', 'store_products.id', '=', 'sampling.store_product_id')
            ->join('store_product_warehouses', function ($join) {
                $join->on('store_product_warehouses.store_product_id', '=', 'store_products.id')
                    ->on('store_product_warehouses.warehouse_id', '=', 'sampling.warehouse_id');
            })
            ->join('products', 'store_products.product_id', '=', 'products.id')
            ->where('sampling.warehouse_id', $warehouse_id)
            //->where('type', 'Simple')
            ->get();
    }

    public static function getSampligDelivered($warehouse_id, $store_product_id) {
        /*$count_order_products = \OrderProduct::with(['order' => function ($query) use ($warehouse_id){
            return $query->with(['orderGroup' => function($q) use ($warehouse_id){ return $q->where('warehouse_id', $warehouse_id); }])->whereStatus('Delivered');
        }, 'orderProductGroup' => function($query) use ($store_product_id){
            return $query->where('store_product_id', $store_product_id)->where('fulfilment_status', 'Fullfilled')->groupBy('order_product_id');
        }])->where('store_product_id', $store_product_id)->where('fulfilment_status', 'Fullfilled')->get()->count();*/
        $count_order_products = OrderProduct::join('orders', 'orders.id', '=',  DB::raw("order_products.order_id AND orders.status = 'Delivered'"))
            ->join('order_groups', 'orders.group_id', '=', DB::raw("order_groups.id AND order_groups.warehouse_id = ". $warehouse_id))
            ->leftJoin('order_product_group', 'order_products.id', '=', DB::raw("order_product_group.order_product_id AND order_product_group.fulfilment_status = 'Fullfilled'"))
            ->where('order_products.store_product_id', $store_product_id)->where('order_products.fulfilment_status', 'Fullfilled')
            ->get()->count();
        return $count_order_products;
    }

    /**
     * Permite establecer las relaciones entre los productos de muestra y los agregados al carrito
     *
     * @param Object $order_group
     * @param Object $products objeto con los productos del pedido
     * @param Object $order objeto que contiene la información del pedido
     */
    public static function getSamplingRelations($order_group, $products, $order)
    {
        try {
            $departments_selected = [];
            $shelves_selected = [];
            $products_selected = [];
            $product_in_shelves = [];
            $product_in_departments = [];

            foreach ($products as $key => $product) {
                $products_selected[] = $product->store_product_id;
                $shelves_selected[] = $product->shelf_id;
                $departments_selected[] = $product->department_id;
                $product_in_shelves[$product->shelf_id] = $product->store_product_id;
                $product_in_departments[$product->department_id] = $product->store_product_id;
            }
            self::getProductsInDepartments($order_group->warehouse_id, $departments_selected, $product_in_departments);
            self::getProductsInShelves($order_group->warehouse_id, $shelves_selected, $product_in_shelves);
            self::getProducts($order_group->warehouse_id, $products_selected);
            self::getProductsHistories($order_group->warehouse_id, $order, $products_selected);
            self::getProductsInZones($order_group->zone_id);

            $parents = [];
            foreach (self::$samples as $key => $store_product) {
                if (!in_array($store_product->parent_id, $parents) && $store_product->qty_sampling > 0) {

                    $order_product_sampling = OrderProduct::join('orders', 'orders.id', '=', 'order_id')
                        ->where('user_id', $order->user_id)
                        ->where('order_products.type', 'Muestra')
                        ->where('store_product_id', $store_product->store_product_id)
                        ->first();

                    if (empty($order_product_sampling)) {
                        $sampling = self::find($store_product->sampling_id);
                        $sampling->decreaseQuantity();
                        $parents[] = $store_product->parent_id;
                        $store_product->type = 'Muestra';
                        OrderProduct::saveProduct(0, 1, $store_product, $order);
                    }
                }
            }
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Devuelve un objeto con los productos de muestra de una bodega segun su tipo de asociación
     *
     * @param int $warehose_id id de la bodega
     * @param String $type tipo de asociación del producto de muestra
     * @return Object resultado de la consulta
     */
    private static function getSamplingProductByType($warehouse_id, $type)
    {
        return self::select('products.*', 'store_products.*', 'store_products.id AS store_product_id', 'sampling.id AS sampling_id', 'sampling.zone_ids', 'sampling_details.related_ids', 'sampling.store_product_id', 'sampling.quantity AS qty_sampling')
            ->join('store_products', 'store_products.id', '=', 'sampling.store_product_id')
            ->join('store_product_warehouses', function ($join) {
                $join->on('store_product_warehouses.store_product_id', '=', 'store_products.id')
                    ->on('store_product_warehouses.warehouse_id', '=', 'sampling.warehouse_id');
            })
            ->join('sampling_details', 'sampling_id', '=', 'sampling.id')
            ->join('products', 'store_products.product_id', '=', 'products.id')
            ->where('sampling.warehouse_id', $warehouse_id)
            ->where('sampling_details.type', $type)
            ->where('sampling.status', 1)
            ->where('sampling.quantity', '>', 0)
            ->whereRaw("sampling.quantity IS NOT NULL")
            ->get();
    }

    /**
     * Determina que los productos de muestra según los departamentos de los productos del pedido
     *
     * @param int $warehose_id id de la bodega
     * @param array $departments_selected departamenos seleccionados en el pedido
     * @param array $product_in_departments productos del pedido que pertenecen al departamento seleccionado
     */
    private static function getProductsInDepartments($warehouse_id, $departments_selected, $product_in_departments)
    {
        $departments = self::getSamplingProductByType($warehouse_id, 'Departamentos');

        if (count($departments) > 0) {
            foreach ($departments as $key => $department) {
                $ids = explode(',', $department->related_ids);
                $array_result = array_intersect($ids, $departments_selected);
                if (count($array_result) > 0) {
                    $department->parent_id = $product_in_departments[array_shift($array_result)];
                    self::$samples[$department->store_product_id] = $department;
                }
            }
        }
    }

    /**
     * Determina que los productos de muestra según los pasillos de los productos del pedido
     *
     * @param int $warehose_id id de la bodega
     * @param array $shelves_selected pasillos seleccionados en el pedido
     * @param array $product_in_shelves productos del pedido que pertenecen al pasillo seleccionado
     */
    private static function getProductsInShelves($warehouse_id, $shelves_selected, $product_in_shelves)
    {
        $shelves = self::getSamplingProductByType($warehouse_id, 'Pasillos');
        if (count($shelves) > 0) {
            foreach ($shelves as $key => $shelve) {
                $ids = explode(',', $shelve->related_ids);
                $array_result = array_intersect($ids, $shelves_selected);
                if (count($array_result) > 0) {
                    $shelve->parent_id = $product_in_shelves[array_shift($array_result)];
                    self::$samples[$shelve->store_product_id] = $shelve;
                }
            }
        }
    }

    /**
     * Determina que los productos de muestra según los productos  del pedido
     *
     * @param int $warehose_id id de la bodega
     * @param array $products_selected productos del pedido
     */
    private static function getProducts($warehouse_id, $products_selected)
    {
        $products = self::getSamplingProductByType($warehouse_id, 'Productos');
        if (count($products) > 0) {
            foreach ($products AS $key => $product) {
                $ids = explode(',', $product->related_ids);
                $array_result = array_intersect($ids, $products_selected);
                if (count($array_result) > 0) {
                    $product->parent_id = array_shift($array_result);
                    self::$samples[$product->store_product_id] = $product;
                }

            }
        }
    }

    /**
     * Determina que los productos de muestra según la zona de los productos  del pedido
     *
     * @param array $zone zona del pedido
     */
    private static function getProductsInZones($zone)
    {

        if (count(self::$samples) > 0) {
            foreach (self::$samples AS $key => $product) {
                if (!is_null($product->zone_ids)) {
                    $ids = explode(',', $product->zone_ids);
                    if (!in_array($zone, $ids)) {
                        unset(self::$samples[$key]);
                    }
                }
            }
        }
    }

    private static function getProductsHistories($warehouse_id, $order, $products_selected)
    {
        $products = self::getSamplingProductByType($warehouse_id, 'Producto Comprado');
        if (count($products)) {
            foreach ($products AS $key => $product) {
                $ids = explode(',', $product->related_ids);
                $order_product_sampling = \OrderProduct::join('orders', 'order_id', '=', 'orders.id')
                    ->select(DB::raw("DATEDIFF(CURDATE(), orders.created_at) AS date_bought"), 'order_products.*')
                    ->where('orders.status', 'Delivered')
                    ->where('user_id', $order->user_id)
                    ->where('sampling_id', $product->sampling_id)
                    ->groupBy('order_id', 'store_product_id')
                    ->having('date_bought', '<=', 45)
                    ->first();

                if (count($order_product_sampling) == 0) {
                    $order_product = \OrderProduct::join('orders', 'order_id', '=', 'orders.id')
                        ->select(DB::raw("DATEDIFF(CURDATE(), orders.created_at) AS date_bought"), 'order_products.*')
                        ->where('user_id', $order->user_id)
                        ->whereIn('store_product_id', $ids)
                        ->groupBy('order_id', 'store_product_id')
                        ->having('date_bought', '<=', 45)
                        ->first();

                    if(count($order_product) > 0){
                        $product->parent_id = array_shift($products_selected);
                        self::$samples[$product->store_product_id] = $product;
                    }
                }

            }
        }
    }

    public function decreaseQuantity()
    {
        $this->quantity--;
        if ($this->quantity == 0) {
            $this->status = 0;
        }
        $this->save();
    }

}

