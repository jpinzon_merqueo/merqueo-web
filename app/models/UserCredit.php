<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property integer id
 * @property User user
 * @property mixed user_id
 * @property integer credit_id
 * @property Credit credit
 * @property Order order
 * @property integer order_id
 * @property string claim_motive
 * @property string text_products_related
 * @property Coupon coupon
 * @property User referrer
 * @property string expiration_date;
 * @property ExternalService external
 * @property string description
 * @property string description_admin
 */
class UserCredit extends Model
{
    /**
     * @var int
     */
    const STATUS_ACTIVE_CREDIT = 1;

    /**
     * @var string
     */
    protected $table = 'user_credits';

    /**
     * @var array
     */
    protected $fillable = [
        'amount',
        'expiration_date',
        'coupon_id',
        'referrer_id',
        'order_id',
        'external_service_id',
        'current_amount',
        //'claim_motive',
        'text_products_related',
        'status',
        'user_id',
        'credit_id'
    ];

    const COMPENSATION_ADD = 'compensation_add';
    const SAC_CLAIM_ADD = 'sac_claim_add';
    const SAC_RESTITUTION_ADD = 'sac_restitution_add';
    const SAC_CLIENT_ADD = 'sac_client_add';
    const ORDER_ADD = 'order_add';
    const COUPON_ADD = 'coupon_add';
    const PRODUCT_ADD = 'product_add';
    const REFERRED_ADD = 'referred_add';
    const REFERRED_BY_ADD = 'referred_by_add';
    const EXTERNAL_SERVICE_ADD = 'external_service_add';
    const MARKETING_ADD = 'marketing_add';
    const SAC_CLAIM_REMOVE = 'sac_claim_remove';
    const SAC_RESTITUTION_REMOVE = 'sac_restitution_remove';
    const SAC_CLIENT_REMOVE = 'sac_client_remove';
    const ORDER_REMOVE = 'order_remove';
    const EXTERNAL_SERVICE_REMOVE = 'external_service_remove';

    /**
     * @var array
     */
    protected $with = ['user', 'credit'];


    /**
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return BelongsTo
     */
    public function coupon()
    {
        return $this->belongsTo(Coupon::class);
    }

    /**
     * @return BelongsTo
     */
    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    /**
     * @return BelongsTo
     */
    public function credit()
    {
        return $this->belongsTo(Credit::class);
    }

    /**
     * @return BelongsTo
     */
    public function referrer()
    {
        return $this->belongsTo(User::class, 'referrer_id');
    }

    /**
     * @return BelongsTo
     */
    public function external()
    {
        return $this->belongsTo(ExternalService::class, "external_service_id");
    }

    public function getDescriptionAttribute()
    {
        if (is_null($this->attributes['credit_id']))
            return $this->attributes['description'];

        return strtr($this->credit->description, $this->replace());
    }

    public function getDescriptionAdminAttribute()
    {
        if (is_null($this->attributes['credit_id']))
            return $this->attributes['description_admin'];

        return strtr($this->credit->description_admin, $this->replace());
    }

    private function replace()
    {
        return [
            ":amount" => currency_format($this->amount),
            ":user_name" => $this->user->first_name,
            ":order_id" => !is_null($this->order) ? $this->order->id : null,
            ":order_reference" => !is_null($this->order) ? $this->order->reference : null,
            //":claim_motive" => !is_null($this->credit_reason) ? $this->credit_reason->reason : null,
            ":text_products_related" => $this->text_products_related,
            ":coupon_code" => !is_null($this->coupon) ? $this->coupon->code : null,
            ":user_referrer_name" => !is_null($this->referrer) ? $this->referrer->first_name : null,
            ":user_referrer_d" => !is_null($this->referrer) ? $this->referrer->id : null,
            ":expiration_date" => $this->expiration_date,
            ":external_id" => !is_null($this->external) ? $this->external->id : null,
            ":user_credit_current_id" => $this->id
        ];
    }

    /*
     * Obtiene credito disponible del usuario
     *
     * @param int $userId ID del usuario
     * @param int $countryId ID del País, Colombia por defecto
     * @return float Credito disponible
     */
    public static function getCreditAvailable($userId, $countryId = Country::COLOMBIA)
    {

        return (float)self::whereCreditType(1)->where('user_id', $userId)
            ->where('expiration_date', '>=', date('Y-m-d'))
            ->where('status', 1)
            ->where('country_id', $countryId)
            ->sum('current_amount');
    }

    private static function whereCreditType($type)
    {
        return self::whereHas('credit', function ($q) use ($type) {
            return $q->where('type', $type);
        });
    }

    /*
     * Obtiene movimientos de credito del usuario
     *
     * @param int $user_id ID del usuario
     * @param int $type Tipo de movimiento a obtener
     * @param int $page Numero de pagina
     *
     * @return UserCredit
     */
    public static function getCreditMovements($user_id, $type = 'admin', $page = 1, $rows = 10)
    {
        if ($type == 'admin')
            return UserCredit::where('user_id', $user_id)->where('status', 1)->orderBy('created_at', 'desc')->get();

        $begin = $page == 1 ? 0 : ($page - 1) * $rows;

        $movements = [];
        $user_credits = UserCredit::whereCreditType(1)->where('user_id', $user_id)->where('status', 1)
            ->orderBy('created_at', 'desc')->limit($rows)->offset($begin)
            ->get();
        if (count($user_credits)) {
            foreach ($user_credits as $user_credit) {
                $movements[] = [
                    'description' => $user_credit->description,
                    'initial_amount' => currency_format($user_credit->amount),
                    'current_amount' => currency_format($user_credit->current_amount),
                    'current_amount_value' => (int)$user_credit->current_amount,
                    'expiration_date' => 'Vence ' . format_date('normal', $user_credit->expiration_date),
                    'expired' => date('Y-m-d') > $user_credit->expiration_date && $user_credit->current_amount ? 1 : 0,
                ];
            }
        }

        return $movements;
    }

    /**
     * Agrega credito a un usuario
     *
     * @param User $user Objeto usuario
     * @param int $amount Valor de credito
     * @param string $expiration_date Fecha de expiracion
     * @param string $type Origen del credito
     * @param Order $object Objeto de pedido o cupon
     * @param null $claim_motive
     * @param null $text_products_related
     * @return UserCredit
     * @deprecated pls use UserCreditRepository->add()
     */
    public static function addCredit(
        User $user,
        $amount,
        $expiration_date,
        $type,
        $object = null,
        $claim_motive = null,
        $text_products_related = null)
    {
        $user_credit = new UserCredit();
        $user_credit->user_id = $user->id;
        $user_credit->amount = $amount;
        $user_credit->current_amount = $amount;
        //$user_credit->claim_motive = $claim_motive;
        $user_credit->text_products_related = substr($text_products_related, 0, -2);
        switch ($type) {
            case 'admin':
                if (!is_null($claim_motive) && !is_null($text_products_related)) {
                    $user_credit->text_products_related = $text_products_related;
                    //$user_credit->claim_motive = $claim_motive;
                    $user_credit->credit_id = Credit::where('name', self::SAC_CLAIM_ADD)->first()->id;
                } else if (!is_null($claim_motive)) {
                    //$user_credit->claim_motive = $claim_motive;
                    $user_credit->credit_id = Credit::where('name', self::SAC_RESTITUTION_ADD)->first()->id;
                } else {
                    //sac_client
                    $user_credit->credit_id = Credit::where('name', self::SAC_CLAIM_ADD)->first()->id;
                }
                break;

            case 'order':
                $user_credit->order_id = $object->id;
                $user_credit->credit_id = Credit::where('name', self::ORDER_ADD)->first()->id;
                break;

            case 'coupon':
                $user_credit->coupon_id = $object->id;
                $user_credit->credit_id = Credit::where('name', self::COUPON_ADD)->first()->id;
                break;

            case 'product':
                $user_credit->credit_id = Credit::where('name', self::PRODUCT_ADD)->first()->id;
                break;

            case 'referred':
                $user_credit->credit_id = Credit::where('name', self::REFERRED_ADD)->first()->id;
                break;

            case 'referred_by':
                $user_credit->credit_id = Credit::where('name', self::REFERRED_BY_ADD)->first()->id;
                break;

            case 'external_service':
                $user_credit->external_service_id = $object->id;
                $user_credit->credit_id = Credit::where('name', self::EXTERNAL_SERVICE_ADD)->first()->id;
                break;

            case 'compensation':
                $user_credit->credit_id = Credit::where('name', self::COMPENSATION_ADD)->first()->id;
                break;

            case 'marketing':
                $user_credit->credit_id = Credit::where('name', self::MARKETING_ADD)->first()->id;
                break;
        }
        $user_credit->expiration_date = $expiration_date;
        $user_credit->admin_id = Session::get('admin_id');
        $user_credit->status = 1;

        $user_credit->save();

        return $user_credit;
    }

    /**
     * Quita credito a un usuario
     *
     * @param User $user Objeto usu ario
     * @param int $amount Valor de credito a quitar
     * @param string $type Origen del credito a quitar
     * @param null $order
     * @param int $coupon_id ID de cupon si aplica
     * @param null $external_service
     * @param null $claim_motive
     * @param null $text_products_related
     * @return UserCredit
     */
    public static function removeCredit(
        User $user,
        $amount,
        $type,
        $order = null,
        $coupon_id = null,
        $external_service = null,
        $claim_motive = null,
        $text_products_related = null)
    {
        $amount_left = $amount;
        //obtener credito disponible proximo a vencer
        while ($amount_left > 0) {
            $user_credit = new UserCredit();
            $user_credit->user_id = $user->id;

            if (!$user_credit_current = self::getNextCreditMovementAvailable($user->id))
                return false;

            $user_credit->user_credit_id = $user_credit_current->id;

            $user_credit->amount = $user_credit_current->current_amount;
            if ($user_credit_current->current_amount >= $amount_left) {
                $user_credit->current_amount = $user_credit_current->current_amount - $amount_left;
                $user_credit_current->current_amount = $user_credit_current->current_amount - $amount_left;
                $amount_left = 0;
            } else {
                $user_credit->current_amount = 0;
                $amount_left = $amount_left - $user_credit_current->current_amount;
                //guardar credito agotado
                $user_credit_current->current_amount = 0;
            }
            $user_credit_current->save();

            switch ($type) {
                case 'admin':
                    if (!is_null($claim_motive) && !is_null($text_products_related)) {
                        $user_credit->text_products_related = $text_products_related;
                        //$user_credit->claim_motive = $claim_motive;
                        $user_credit->credit_id = Credit::where('name', self::SAC_CLAIM_REMOVE)->first()->id;
                    } else if (!is_null($claim_motive)) {
                        //$user_credit->claim_motive = $claim_motive;
                        $user_credit->credit_id = Credit::where('name', self::SAC_RESTITUTION_REMOVE)->first()->id;
                    } else {
                        $user_credit->credit_id = Credit::where('name', self::SAC_CLIENT_REMOVE)->first()->id;
                    }
                    break;

                case 'order':
                    $user_credit->order_id = $order->id;
                    $user_credit->coupon_id = $coupon_id ? $coupon_id : null;
                    $user_credit->credit_id = Credit::where('name', self::ORDER_REMOVE)->first()->id;
                    break;

                case 'external_service':
                    $user_credit->external_service_id = $external_service->id;
                    $user_credit->credit_id = Credit::where('name', self::EXTERNAL_SERVICE_REMOVE)->first()->id;
                    break;
            }
            $user_credit->expiration_date = null;
            $user_credit->admin_id = Session::get('admin_id');
            $user_credit->status = 1;

            $user_credit->save();
        }

        return $user_credit;
    }

    /**
     * Agrega credito al usuario por ser referido.
     *
     * @param User $referred
     * @param User $user_code
     * @return UserCredit
     */
    public static function addFromReferral(User $referred, User $user_code)
    {
        $expiration_date = date('Y-m-d',
            strtotime('+' . Config::get('app.referred.amount_expiration_days') . ' day')
        );
        $amount = \Config::get('app.referred.amount');
        return self::addCredit($referred, (int)$amount, $expiration_date, 'referred', $user_code);
    }

    /**
     * Actualiza credito cargado y deducido de un pedido
     *
     * @param Order $order Objeto pedido
     * @param int $amount Valor de nuevpo credito
     */
    public static function updateCredit(Order $order, $amount)
    {
        $user_credit = UserCredit::whereCreditType(0)
            ->where('order_id', $order->id)
            ->where('status', 1)
            ->where('current_amount', '>', 0)
            ->first();

        if ($user_credit) {
            //eliminar movimiento cargo
            $user_credit_charge = UserCredit::find($user_credit->user_credit_id);
            $user_credit_charge->status = 0;
            $user_credit_charge->save();
            //eliminar movimiento deduccion
            $user_credit->status = 0;
            $user_credit->save();
            //crear nuevos movimientos
            $user = User::find($order->user_id);
            if (empty($user_credit->coupon_id)) {
                UserCredit::addCredit($user, $amount, $user_credit_charge->expiration_date, 'order', $order);
                UserCredit::removeCredit($user, $amount, 'order', $order);
            } else {
                UserCredit::addCredit(
                    $user,
                    $amount,
                    $user_credit_charge->expiration_date,
                    'order',
                    $order,
                    $user_credit->coupon_id);

                UserCredit::removeCredit($user, $amount, 'order', $order, $user_credit->coupon_id);
            }
        }
    }

    /**
     * Valida si se le retorna credito al pedido al cancelarlo
     *
     * @param Order $order
     * @param $reject_reason
     * @param $further_reject_reason
     * @return bool
     */
    public static function validateReturnCredit(Order $order, $reject_reason, $further_reject_reason)
    {
        if ($reject_reason == 'Cancelar') {
            $valid_reject_reason = true;
            if (in_array($further_reject_reason, [
                'Validación - Tiene más de un perfil',
                'Validación - Misma dirección del promotor',
                'Validación - Misma dirección de un referido',
                'Pedido duplicado',
                'Validación no aprobada'
            ])) {
                $valid_reject_reason = false;
            }

            if ($order->temporarily_cancelled == 0 && $valid_reject_reason) {
                return true;
            }
        }

        return false;
    }

    /**
     * Retorna credito de un pedido a la cuenta del usuario
     *
     * @param Order $order Objeto pedido
     */
    public static function returnCredit(Order $order)
    {
        $userCredits = UserCredit::where('order_id', $order->id)
            ->where('status', self::STATUS_ACTIVE_CREDIT)
            ->join('credits', 'credits.id', '=','user_credits.credit_id')
            ->orderBy('expiration_date', 'desc')
            ->get();

        if (count($userCredits) > 1) {
            return;
        }

        $user_credit = UserCredit::whereCreditType(0)->where('order_id', $order->id)->where('status', 1)->first();
        if ($user_credit) {
            //retornar credito
            $user = User::find($order->user_id);
            //obtener ultimo movimiento de cargo para obtener fecha de expiracion
            $last_user_credit = UserCredit::whereCreditType(0)
                ->where('user_id', $user->id)
                ->where('type', 1)
                ->where('status', 1)
                ->orderBy('expiration_date', 'desc')
                ->first();

            UserCredit::addCredit(
                $user,
                $order->discount_amount,
                $last_user_credit->expiration_date,
                'order',
                $order);
        }
    }

    /**
     * Elimina descuento de credito de un pedido y cupon si utilizo
     *
     * @param Order $order Objeto pedido
     */
    public static function removeCreditDiscount($order)
    {
        $user_credit = UserCredit::whereCreditType(0)
            ->where('order_id', $order->id)
            ->where('status', 1)
            ->first();

        if ($user_credit) {
            UserCredit::where('order_id', $order->id)
                ->where('coupon_id', $user_credit->coupon_id)
                ->update(array('status' => 0));

            UserCoupon::where('user_id', $order->user_id)
                ->where('coupon_id', $user_credit->coupon_id)
                ->delete();
        }
    }

    /**
     * Elimina registros de user_coupon, debería ser usada cuando
     * se cancela un orden
     */
    public static function removeRedeemedCoupons($order)
    {

        // Se buscan las ordenes padres
        $parent_order_id = $order->parent_order_id;
        if (!is_null($parent_order_id)) {
            $parent_order = Order::find($parent_order_id);
            UserCredit::removeRedeemedCoupons($parent_order);
        }

        $redeemed_coupons = UserCredit::where('order_id', '=', $order->id)
            ->whereNotNull('coupon_id')
            ->whereNotNull('user_id')
            ->select('coupon_id', 'user_id')
            ->get();
        foreach ($redeemed_coupons as $redeemed_coupon) {
            UserCoupon::where('user_id', '=', $redeemed_coupon->user_id)
                ->whereNotNull('coupon_id')
                ->where('coupon_id', '=', $redeemed_coupon->coupon_id)
                ->delete();
        }

    }

    /**
     * Obtiene credito disponible proximo a vencer del usuario
     * @param $user_id
     * @return mixed
     */
    private static function getNextCreditMovementAvailable($user_id)
    {
        return self::whereCreditType(1)->where('user_id', $user_id)
            ->where('expiration_date', '>=', date('Y-m-d'))->where('status', 1)
            ->where('current_amount', '>', 0)
            ->orderBy('expiration_date', 'asc')
            ->first();
    }

    /**
     * {@inheritDoc}
     */
    protected static function boot()
    {
        parent::boot();

        parent::saved(function (UserCredit $credit) {
            UpdateLeanplumUser::upload($credit->user_id);
        });
    }
}
