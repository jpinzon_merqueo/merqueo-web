<?php

use Illuminate\Database\Eloquent\Model;

/**
 * Class OrderPaymentLog
 */
class OrderPaymentLog extends Model
{
    /**
     * @var string $table
     */
    protected $table = 'order_payment_log';
}