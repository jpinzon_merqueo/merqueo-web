<?php

class AdminLog extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'admin_log';

    /**
     * Registra log de admin
     *
     * @param string $table Nombre de la tabla
     * @param int $id ID del registro
     * @param string $action Tipo de accion
     */
    public static function saveLog($table, $id, $action)
    {
        $admin_log = new AdminLog;
        $admin_log->setAttribute('table', $table);
        $admin_log->row_id = $id;
        $admin_log->action = $action;
        $admin_log->admin_id = Session::has('admin_id') ? Session::get('admin_id') : 133;
        $admin_log->ip = get_ip();
        $admin_log->save();
    }

}