<?php

use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class City
 * @property City[]|Collection neighborhoods
 * @property Zone zones
 * @property Zone[] zoneList
 * @property Store[] stores
 * @property City $prent
 */
class City extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'cities';

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    /**
     * @deprecated La bodega esta asociada a la ciudad y la zona a la bodega.
     * @return Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function zones()
    {
        return $this->hasOne('Zone');
    }

    /**
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function stores()
    {
        return $this->hasMany(Store::class);
    }

    /**
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function warehouses()
    {
        return $this->hasMany(\Warehouse::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo('City', 'parent_city_id');
    }


    public static function getMainCities()
    {
        $cities = City::whereNull('parent_city_id')
            ->active()
            ->main()
            ->get();

        return $cities;
    }

    public function scopeMain($query)
    {
        return $query->where('is_main', 1);
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    /**
     * @return HasMany
     */
    public function neighborhoods()
    {
        return $this->hasMany(City::class, 'parent_city_id');
    }

    /**
     * @return BelongsTo
     */
    public function storeCovered()
    {
        return $this->belongsTo(Store::class, 'coverage_store_id');
    }

    /**
     * @return BelongsTo
     */
    public function countries()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }
}
