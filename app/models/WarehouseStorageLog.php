<?php

/**
 * @property int admin_id
 * @property int warehouse_id
 * @property int store_product_id
 * @property string movement
 * @property string module
 * @property int quantity_from
 * @property int position_from
 * @property string position_height_from
 * @property int quantity_to
 * @property int position_to
 * @property string position_height_to
 */
class WarehouseStorageLog extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'warehouse_storage_log';


}