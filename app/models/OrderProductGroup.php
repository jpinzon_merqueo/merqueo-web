<?php

class OrderProductGroup extends \Illuminate\Database\Eloquent\Model
{

    /**
     * @var string
     */
    protected $table = 'order_product_group';

    /**
     * Actualizar estado de producto agrupado en pedido
     */
    public static function updateProductStatus($order_product_id)
    {
        if (!$order_product = OrderProduct::find($order_product_id)) {
            return false;
        }

        $order_products = OrderProductGroup::where('order_product_id', $order_product_id)->get();
        if (count($order_products)) {
            $fullfilled = $not_available = $returned = $pending = $missing = 0;
            foreach ($order_products as $order_product_group) {
                if ($order_product_group->fulfilment_status == 'Pending') {
                    $order_product->fulfilment_status = 'Pending';
                    $pending++;
                    break;
                }
                if ($order_product_group->fulfilment_status == 'Fullfilled') {
                    $fullfilled++;
                }

                if ($order_product_group->fulfilment_status == 'Not Available') {
                    $not_available++;
                }

                if ($order_product_group->fulfilment_status == 'Missing') {
                    $missing++;
                }

                if ($order_product_group->fulfilment_status == 'Returned') {
                    $returned++;
                }

            }

            if ($not_available && !$fullfilled && !$returned && !$pending && !$missing) {
                $order_product->fulfilment_status = 'Not Available';
            } else if ($returned && !$fullfilled && !$not_available && !$pending && !$missing) {
                $order_product->fulfilment_status = 'Returned';
            } else if ($fullfilled && !$not_available && !$pending && !$missing) {
                $order_product->fulfilment_status = 'Fullfilled';
            } else if ($not_available && !$pending && !$missing) {
                $order_product->fulfilment_status = 'Not Available';
            } else if ($missing && !$pending) {
                $order_product->fulfilment_status = 'Missing';
            }

            $order_product->save();
        }
    }

    /**
     * Actualiza la cantidad del combo a entregar
     * @param $order_product_id del combo en pedido
     */
    public static function updateComboStatus($order_product_id)
    {

        if (!$order_product = OrderProduct::find($order_product_id)) {
            return false;
        }

        $order_products = OrderProductGroup::where('order_product_id', $order_product_id)->get();
        if (count($order_products)) {
            $fullfilled    = $not_available    = $incomplete    = 0;
            $combo_request = $order_product->quantity;
            foreach ($order_products as $order_product_group) {
                $product_quantity_in_combo = StoreProductGroup::join('store_products', 'store_products.id', '=', 'store_product_group.store_product_id')
                    ->join('store_product_warehouses', 'store_products.id', '=', 'store_product_warehouses.store_product_id')
                    ->where('store_product_group.store_product_id', $order_product->store_product_id)
                    ->where('store_product_group_id', $order_product_group->store_product_id)
                    ->where('store_product_warehouses.status', 1)
                    ->pluck('quantity');

                $total_product_request = $product_quantity_in_combo * $combo_request;
                if ($order_product_group->quantity == $total_product_request && $order_product_group->fulfilment_status == 'Fullfilled') {
                    $fullfilled++;
                }

                if ($order_product_group->quantity < $total_product_request && $order_product_group->fulfilment_status == 'Fullfilled') {
                    $incomplete++;
                }

                if ($order_product_group->quantity == $total_product_request && $order_product_group->fulfilment_status == 'Not Available') {
                    $not_available++;
                }
            }

            if ($not_available && !$fullfilled && !$incomplete) {
                $order_product->fulfilment_status = 'Not Available';
                foreach ($order_products as $order_product_group) {
                    $order_product_group->fulfilment_status = 'Not Available';
                    $order_product_group->save();
                }
            } elseif ($fullfilled && !$incomplete && !$not_available) {
                $order_product->fulfilment_status = 'Fullfilled';
            } elseif ($incomplete && $combo_request == 1 && count($order_products) == 1) {
                $order_product->fulfilment_status = 'Not Available';
                foreach ($order_products as $order_product_group) {
                    $order_product_group->fulfilment_status = 'Not Available';
                    $order_product_group->save();
                }
            } elseif ($incomplete && $combo_request > 1 && count($order_products) == 1) {
                foreach ($order_products as $order_product_group) {
                    $order_product_group->fulfilment_status = 'Fullfilled';
                    $order_product_group->quantity          = $product_quantity_in_combo;
                    $order_product_group->save();
                }
                $order_product->quantity          = $order_product->quantity - 1;
                $order_product->fulfilment_status = 'Fullfilled';
            } elseif ($fullfilled && !$incomplete && $not_available && $combo_request == 1 && count($order_products) > 1) {
                foreach ($order_products as $order_product_group) {
                    $order_product_group->fulfilment_status = 'Not Available';
                    $order_product_group->save();
                }
                $order_product->fulfilment_status = 'Not Available';
            } elseif ($fullfilled && $incomplete && !$not_available && $combo_request > 1 && count($order_products) > 1) {
                $final_quantity = false;
                foreach ($order_products as $order_product_group) {
                    $product_quantity_in_combo = StoreProductGroup::join('store_products', 'store_products.id', '=', 'store_product_group.store_product_id')
                        ->join('store_product_warehouses', 'store_products.id', '=', 'store_product_warehouses.store_product_id')
                        ->where('store_product_group.store_product_id', $order_product->store_product_id)
                        ->where('store_product_group_id', $order_product_group->store_product_id)
                        ->where('store_product_warehouses.status', 1)
                        ->pluck('quantity');

                    $total_product_request = $product_quantity_in_combo * $combo_request;
                    if ($total_product_request > $order_product_group->quantity) {
                        $combo          = $order_product_group->quantity / $product_quantity_in_combo;
                        $final_quantity = $order_product_group->quantity;

                        if ($order_product_group->quantity < $final_quantity) {
                            $final_quantity = $order_product_group->quantity;
                        }

                    }
                }

                if ($final_quantity) {
                    foreach ($order_products as $order_product_group) {
                        $order_product_group->fulfilment_status = 'Fullfilled';
                        $order_product_group->quantity          = $final_quantity;
                        $order_product_group->save();
                    }

                    if ($combo_request > 1) {
                        $order_product->quantity = $combo;
                    }

                }
                $order_product->fulfilment_status = 'Fullfilled';
            }
            $order_product->save();
        }
    }

    /**
     * Agregar producto individual de producto agrupado a pedido
     *
     * @param obj $order_product Objeto del producto en pedido
     * @param obj $store_product Objeto del producto que se desea agregar
     * @param int $order_id      id del pedido al que pertenecerá el producto
     * @param int $quantity      Cantidad del producto para agregar
     */
    public static function addProductGroup($order_product, $store_product, $order_id, $quantity)
    {
        if ($order_product && $order_product->type == 'Agrupado') {
            $products_group = StoreProductGroup::where('store_product_id', $store_product->id)
                ->join('store_products', 'store_products.id', '=', 'store_product_group.store_product_group_id')
                ->join('products AS p', 'store_products.product_id', '=', 'p.id')
                ->select(
                    'store_products.id AS store_product_id',
                    'p.name AS product_name',
                    'p.quantity AS product_quantity',
                    'p.unit AS product_unit',
                    'store_product_group.price',
                    'store_products.base_price',
                    'store_products.iva',
                    'store_product_group.quantity',
                    'p.image_medium_url AS product_image_url'
                )
                ->get();

            $products_group = $products_group->toArray();
            foreach ($products_group as $key => &$prodgp) {
                $prodgp['order_id']          = $order_id;
                $prodgp['order_product_id']  = $order_product->id;
                $prodgp['fulfilment_status'] = 'Pending';
                $prodgp['quantity']          = $quantity * $prodgp['quantity'];
                $prodgp['created_at']        = Carbon\Carbon::now()->format('Y-m-d h:i:s');
                $prodgp['updated_at']        = Carbon\Carbon::now()->format('Y-m-d h:i:s');
            }

            $result = DB::table('order_product_group')->insert($products_group);

            return $result;

        }

        return false;
    }

    /**
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function storeProduct()
    {
        return $this->belongsTo(StoreProduct::class);
    }
}
