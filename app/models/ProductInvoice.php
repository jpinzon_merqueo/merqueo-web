<?php

/**
 * Model ProductInvoice
 */
class ProductInvoice extends Illuminate\Database\Eloquent\Model
{
    /**
     * @var string
     */
    protected $table = "product_invoice";
}