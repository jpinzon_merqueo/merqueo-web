<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

/**
 * Class Admin
 * @property string $services_list
 * @method static \Illuminate\Database\Query\Builder hasCustomerServiceRol()
 */
class Admin extends \Illuminate\Database\Eloquent\Model implements UserInterface, RemindableInterface
{

    use UserTrait, RemindableTrait;

    protected $table = 'admin';

    const ALLOWED_ADMIN_IDS = [1, 709, 708];
    const ACCOUNTING_ROLE_IDS = [3, 4];

    /**
     * @return mixed
     */
    public function city()
    {
        return $this->belongsTo(City::class);
    }

    /**
     * @param CustomerService $service
     * @return bool
     */
    public function hasService(CustomerService $service)
    {
        return in_array($service->id, explode(',', $this->services_list));
    }

    /**
     * Obtiene los usuarios registrados con el rol "Servicio al cliente".
     *
     * @param $query
     * @return mixed
     */
    public function scopeHasCustomerServiceRol($query)
    {
        return $query->where('role_id', Role::SAC_AGENT);
    }

    /**
     * @return mixed
     */
    public function calls()
    {
        return $this->hasMany(\tickets\TicketCall::class);
    }

    /**
     * Obtiene un arreglo con los permisos disponibles del usuario.
     *
     * @return array
     */
    public function getCustomerServicePermissions()
    {
        return explode(',', $this->services_list);
    }

    /**
     * Obtiene un arreglo con los usuarios asociados a las novedades de los transportadores.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function TrasnporterNew()
    {
        return $this->hasMany('TransporterNew', 'admin_id');
    }
}
