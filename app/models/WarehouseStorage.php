<?php

use exceptions\MerqueoException;

/**
 * Class WarehouseStorage
 * @property null quantity
 * @property mixed id
 * @property mixed warehouse_id
 * @property mixed store_product_id
 * @property mixed admin_id
 * @property mixed storage_type
 * @property mixed type
 * @property mixed expiration_date
 * @property mixed position
 * @property mixed position_height
 */
class WarehouseStorage extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'warehouse_storages';
    protected $hidden = array('created_at','updated_at');

    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class);
    }

    public function storeProduct()
    {
        return $this->belongsTo(storeProduct::class);
    }

    /**
     * Al guardar el objeto
     */
    public static function boot()
    {
        parent::boot();

        static::created(function ($warehouse_storage) {
            if ($warehouse_storage->type == 'Producto') {
                $store_product_warehouse = StoreProductWarehouse::where(
                    'store_product_id',
                    $warehouse_storage->store_product_id
                )
                    ->where('warehouse_id', $warehouse_storage->warehouse_id)
                    ->first();
                StoreProductWarehouse::validateStockStatus($store_product_warehouse);
            }
        });

        static::updated(function ($warehouse_storage) {
            if ($warehouse_storage->type == 'Producto') {
                $store_product_warehouse = StoreProductWarehouse::where(
                    'store_product_id',
                    $warehouse_storage->store_product_id
                )
                    ->where('warehouse_id', $warehouse_storage->warehouse_id)
                    ->first();
                StoreProductWarehouse::validateStockStatus($store_product_warehouse);
            }
        });

        static::deleted(function ($warehouse_storage) {
            if ($warehouse_storage->type == 'Producto') {
                $store_product_warehouse = StoreProductWarehouse::where(
                    'store_product_id',
                    $warehouse_storage->store_product_id
                )
                    ->where('warehouse_id', $warehouse_storage->warehouse_id)
                    ->first();
                StoreProductWarehouse::validateStockStatus($store_product_warehouse);
            }
        });
    }

    /**
    * funcion para obtener el recibo segun el id
    *
    * @param int $invoice_number id del recibo
    *
    */
    public static function getProviderOrderReception($reception_id)
    {

        $warehouse_id = Session::get('admin_warehouse_id') ? Session::get('admin_warehouse_id') : 1;
        $admin = Admin::find(Session::get('admin_id'));
    	$provider_order_reception = ProviderOrderReception::select('provider_order_receptions.id', 'provider_order_receptions.status', 'provider_order_receptions.received_date', 'provider_order_receptions.invoice_number', 'provider_orders.provider_name', 'provider_orders.warehouse_id', 'warehouses.warehouse')
										->join('provider_orders', 'provider_orders.id', '=', 'provider_order_receptions.provider_order_id')
                                        ->join('warehouses', 'warehouses.id', '=', 'provider_orders.warehouse_id')
										//->where('provider_order_receptions.status', 'Recibido')
										->where('provider_order_receptions.id', $reception_id);
        /*if($admin->admin_designation != 'Super Admin'){
            $provider_order_reception = $provider_order_reception->where('provider_orders.warehouse_id', $warehouse_id);
        }*/

		$provider_order_reception = $provider_order_reception->get()->first();
		if($provider_order_reception && $provider_order_reception->status == 'Recibido'){
            $provider_order_reception = $provider_order_reception->toArray();
            if (get_config_option('block_reception_return_storage', $provider_order_reception['warehouse_id']))
                return array( 'status' => false, 'message' => 'El almacenamiento de productos esta deshabilitado, por favor contacte al administrador.');
            if(Session::get('admin_designation') != 'Super Admin' && $provider_order_reception['warehouse_id'] != $warehouse_id){
                return array( 'status' => false, 'message' => 'Usted no pertenece a la bodega  de este recibo.' );
            }
            $provider_order_reception['total_products'] = self::getQuantityProducts($reception_id);

			return array( 'status' => true,  'data' => $provider_order_reception );
        }else{
            if(empty($provider_order_reception) ){
                return array( 'status' => false, 'message' => 'El recibo no existe.' );
            }elseif($provider_order_reception->status != 'Recibido'){
                return array( 'status' => false, 'message' => 'El recibo esta en estado '.$provider_order_reception->status.'.' );

            }
        }

    }

    /**
    * funcion cuenta la cantidad del productos asociados al recibo
    *
    * @param int $reception_id id del recibo
    *
    */
    public static function getQuantityProducts($reception_id)
    {
        try{

            $singles = ProviderOrderReceptionDetail::select( DB::raw('sum(provider_order_reception_details.quantity_received-provider_order_reception_details.quantity_storaged) AS quantity_received') )
                            ->join('store_products', 'store_products.id','=', 'provider_order_reception_details.store_product_id')
                            ->whereIn('provider_order_reception_details.status', array('Recibido', 'Parcialmente Recibido'))
                            ->where('provider_order_reception_details.type', 'Simple')
                            ->where('reception_id', $reception_id)
                            //->get();
                            ->pluck('quantity_received');

            $groups = ProviderOrderReceptionDetail::select(
                            DB::raw('ROUND(sum( (provider_order_reception_detail_product_group.quantity_received-provider_order_reception_detail_product_group.quantity_storaged) )) AS quantity_received')
                        )
                            ->join('provider_order_reception_detail_product_group', 'provider_order_reception_detail_product_group.reception_detail_id', '=', 'provider_order_reception_details.id')
                            ->join('store_products', 'store_products.id','=', 'provider_order_reception_detail_product_group.store_product_id')
                            ->whereIn('provider_order_reception_detail_product_group.status', array('Recibido', 'Parcialmente Recibido'))
                            ->whereIn('provider_order_reception_details.status', array('Recibido', 'Parcialmente Recibido'))
                            ->where('provider_order_reception_details.type', 'Proveedor')
                            ->where('provider_order_reception_details.reception_id', $reception_id)

                            //->get()
                            ->pluck('quantity_received');
            

            $singles = is_null($singles) ? 0 : $singles;
            $groups = is_null($groups) ? 0 : $groups;

            return $singles + $groups ;
        }catch(\Exception $e){
            return [ 'result' => false, 'message' => "Existe una inconsistencia con las unidades almacenadas" ];
        }
    }

    /**
    * funcion que trae los detalles de un produto del recibo
    *
    * @param int $reception_id id del recibo
    * @param varchar $reference texto con la referencia o nombre del producto a buscar
    * @param varchar $storage texto con el tipo de almacenamiento (Seco, Frío)
    * @return \ProviderOrderReceptionDetail
    */
    public static function getProviderOrderReceptionProducts($reception_id, $storage, $reference = null)
    {
        $storage = $storage == 'Seco' ? array($storage):array('Refrigerado','Congelado');

    	$products = ProviderOrderReceptionDetail::select('store_products.id',  'provider_order_reception_details.quantity_received','provider_order_reception_details.quantity_storaged',
                        DB::raw("IFNULL(provider_order_reception_details.expiration_date, provider_order_reception_detail_product_group.expiration_date) as expiration_date"),
                        DB::raw("IF(provider_order_reception_details.`type`= 'Proveedor', provider_order_reception_detail_product_group.quantity_received,`provider_order_reception_details`.`quantity_received`) AS quantity_received"),
                        DB::raw("IF(provider_order_reception_details.`type`= 'Proveedor', provider_order_reception_detail_product_group.quantity_storaged,`provider_order_reception_details`.`quantity_storaged`) AS quantity_storaged"),
                        DB::raw("CONCAT_WS( ' ', products.name, products.quantity, products.unit) AS name "),
                        DB::raw("provider_order_reception_details.store_product_id AS parent_store_product_id"),
                        'products.reference', 'products.image_small_url', 'provider_order_reception_details.type')

                        ->leftJoin('provider_order_reception_detail_product_group', 'provider_order_reception_details.id', '=', 'provider_order_reception_detail_product_group.reception_detail_id')
						->join('provider_order_receptions', 'provider_order_receptions.id', '=', 'provider_order_reception_details.reception_id')
						->join('store_products', 'store_products.id', '=', DB::raw("IF(provider_order_reception_details.`type`= 'Proveedor', provider_order_reception_detail_product_group.store_product_id,`provider_order_reception_details`.`store_product_id`)"))
						->join('products', 'products.id', '=', 'store_products.product_id')

                        ->whereIn('store_products.storage', $storage)
                        ->where('provider_order_receptions.id', $reception_id );
        if(!is_null($reference))
            $products->whereRaw("( products.reference like '%".$reference."%' OR products.name LIKE '%".$reference."%')");

            $products = $products->whereRaw("IF(provider_order_reception_details.type= 'Proveedor', provider_order_reception_detail_product_group.status, provider_order_reception_details.status) IN ('Recibido', 'Parcialmente Recibido')")
                        ->groupBy('store_products.id')
                        //->toSql();
						->get();

    	if(count($products)){
            $products = $products->toArray();
            foreach ($products as $key => $product) {
                # code...
                $product['quantity_to_storage'] = $product['quantity_received'] - $product['quantity_storaged'];
                if($product['quantity_to_storage'] > 0){
                    $products[$key] = $product;
                }else{
                    unset($products[$key]);
                }
            }
	    	return $products;
        }
    }

    /**
     * Obtiene un lote especifico de un producto
     *
     * @param int $storeProductId Posicion en altura
     * @param int $warehouseId ID de la bodega
     * @param int $position int Numero de posicion
     * @param string $positionHeight Posicion en altura
     * @param string $expirationDate Posicion en altura
     * @return WarehouseStorage
     */
    public static function getProductLote($storeProductId, $warehouseId, $position, $positionHeight, $expirationDate)
    {
        return WarehouseStorage::where('warehouse_id', $warehouseId)
                                ->where('position', $position)
                                ->whereRaw("position_height = '".$positionHeight."'")
                                ->where('expiration_date', $expirationDate)
                                ->where('store_product_id', $storeProductId)
                                ->lockForUpdate()
                                ->first();
    }

    /**
     * Obtiene productos en una posicion de la bodega
     *
     * @param int $warehouse_id ID de la bodega
     * @param int $position int Numero de posicion
     * @param string $position_height Posicion en altura
     */
    public static function getProductsPosition($warehouse_id, $position, $position_height)
    {
        return WarehouseStorage::where('warehouse_storages.warehouse_id', $warehouse_id)
                                ->where('position', $position)
                                ->whereRaw("position_height = '".$position_height."'")
                                ->leftJoin('store_products', 'warehouse_storages.store_product_id', '=', 'store_products.id')
                                ->leftJoin('products', 'store_products.product_id', '=', 'products.id')
                                ->select('warehouse_storages.id', 'warehouse_storages.type', 'products.image_small_url', 'products.reference', 'products.quantity AS product_quantity', 'products.unit',
                                'products.name AS name', 'warehouse_storages.position', 'warehouse_storages.position_height', 'warehouse_storages.expiration_date', 'warehouse_storages.quantity',
                                'warehouse_storages.created_at', 'store_products.handle_expiration_date')
                                ->orderBy(DB::raw("FIELD(warehouse_storages.type, 'Producto', 'Sampling', 'Canastilla merqueo', 'Canastilla proveedor', 'Estiva')"))
                                ->orderBy('products.name')
                                ->orderBy('warehouse_storages.expiration_date')
                                ->orderBy('warehouse_storages.created_at')
                                ->get();
    }

    /**
     * Valida que la posicion exista dentro de la bodega
     *
     * @param int $warehouse_id ID de la bodega
     * @param string $type Tipo de posicion
     * @param string $storage_type Tipo de almacenamiento
     * @param int $position int Numero de posicion
     * @param string $position_height Posicion en altura
     */
    public static function validatePosition($warehouse_id, $position_type, $storage_type, $position, $position_height)
    {
        $result = [
            'status' => false,
            'message' => 'Error generico'
        ];

        if (!$warehouse_storage = Warehouse::find($warehouse_id)){
            $result['message'] = 'La bodega no existe.';
            return $result;
        }

        $storage_positions = json_decode($warehouse_storage->storage_positions, true);

        if (!isset($storage_positions[$position_type])){
            $result['message'] = 'El tipo de posición es incorrecto.';
            return $result;
        }

        if (!isset($storage_positions[$position_type][$storage_type])){
            $result['message'] = 'El tipo de almacenamiento es incorrecto.';
            return $result;
        }

        $position= intval($position);
        if ($position < $storage_positions[$position_type][$storage_type]['start_position'] || $position > $storage_positions[$position_type][$storage_type]['end_position']){
            $result['message'] = 'La posición '.$position.' es incorrecta, debe estar entre '.$storage_positions[$position_type][$storage_type]['start_position'].' y '.$storage_positions[$position_type][$storage_type]['end_position'].'.';

            return $result;
        }

        $position_height = trim($position_height);
        if (!in_array($position_height, $storage_positions[$position_type][$storage_type]['height_positions'])){
            $result['message'] = 'La posición en altura '.$position_height.' es incorrecta, las posiciones validas son: '.trim(implode(', ', $storage_positions[$position_type][$storage_type]['height_positions']), ', ').'.';
            return $result;
        }

        return ['status' => true, 'message' => 'Posición valida.'];
    }

    /**
     * Saca una cantidad de producto de una posición en WarehouseStorage
     * @param $warehouseStorage
     * @param $module
     * @param null $quantity
     * @return null
     * @throws MerqueoException
     */
    public static function pullFrom($warehouseStorage, $module, $quantity = null)
    {
        if (!is_a($warehouseStorage, WarehouseStorage::class)) {
            throw new MerqueoException('El debe ingresar un objeto tipo WarehouseStorage');
        }
        if (gettype($module) !== 'string') {
            throw new MerqueoException('El módulo debe ser una cadena.');
        }
        if (!is_null($quantity)) {
            if ((gettype($quantity) !== 'integer' || $quantity < 1)) {
                throw new MerqueoException('La cantidad a almacenar no puedes ser menor a 1.');
            }
            if ($quantity > $warehouseStorage->quantity) {
                throw new MerqueoException('La cantidad a retirar es superior a la cantidad almacenada.');
            }
        }

        if (!empty($quantity)) {
            $warehouseStorage->quantity -= $quantity;
        } else {
            $quantity = $warehouseStorage->quantity;
            $warehouseStorage->quantity = 0;
        }

        Event::fire('warehouseStorage.onPullQuantity', [$warehouseStorage, $module]);
        $warehouseStorage->save();
        if ($warehouseStorage->quantity == 0) {
            $warehouseStorage->delete();
        }

        return $quantity;
    }

    /**
     * Ingresa una cantidad de producto a una posición en WarehouseStorage
     * @param $warehouseStorageFrom
     * @param $module
     * @param $position
     * @param $positionHeight
     * @param $quantity
     * @return mixed
     * @throws MerqueoException
     */
    public static function pushTo($warehouseStorageFrom, $module, $position, $positionHeight, $quantity)
    {
        if (!is_a($warehouseStorageFrom, WarehouseStorage::class)) {
            throw new MerqueoException('Se debe ingresar un objeto tipo WarehouseStorage');
        }

        $warehouseStorageTo = WarehouseStorage::where('warehouse_id', $warehouseStorageFrom->warehouse_id)
            ->where('store_product_id', $warehouseStorageFrom->store_product_id)
            ->where('storage_type', $warehouseStorageFrom->storage_type)
            ->where('expiration_date', $warehouseStorageFrom->expiration_date)
            ->where('position', $position)
            ->where('position_height', $positionHeight)
            ->first();

        if (empty($warehouseStorageTo)
            || $warehouseStorageTo->expiration_date !== $warehouseStorageFrom->expiration_date) {
            $warehouseStorageTo = new WarehouseStorage;
            $warehouseStorageTo->warehouse_id = $warehouseStorageFrom->warehouse_id;
            $warehouseStorageTo->store_product_id = $warehouseStorageFrom->store_product_id;
            $warehouseStorageTo->admin_id = Session::get('admin_id');
            $warehouseStorageTo->storage_type = $warehouseStorageFrom->storage_type;
            $warehouseStorageTo->type = $warehouseStorageFrom->type;
            $warehouseStorageTo->expiration_date = $warehouseStorageFrom->expiration_date;
            $warehouseStorageTo->quantity = $quantity;
            $warehouseStorageTo->position = $position;
            $warehouseStorageTo->position_height = $positionHeight;
            Event::fire('warehouseStorage.onPushQuantity', [$warehouseStorageTo, $module]);
            $warehouseStorageTo->save();
        } else {
            if ($warehouseStorageTo->expiration_date == $warehouseStorageFrom->expiration_date) {
                $warehouseStorageTo->quantity += $quantity;
                Event::fire('warehouseStorage.onPushQuantity', [$warehouseStorageTo, $module]);
                $warehouseStorageTo->save();
            }
        }

        return $quantity;
    }
    /**
     * Obtiene la cantidad de productos y posición en almacenamiento de un determinado producto con prioridad la fecha
     * de vencimiento más próxima
     *
     * @param Order $order Objeto orden con el producto a consultar
     * @param int $warehouseId Id de la bodega en la que se buscará el producto
     * @return Order $order Objeto con los nuevos campos consultados
     *
     */
    public static function getPositionNextExpiration($order, $warehouseId)
    {
        $warehouse_position = WarehouseStorage::join('store_products', 'store_products.id', '=', 'warehouse_storages.store_product_id')
            ->join('products', 'products.id', '=', 'store_products.product_id')
            ->where('products.id', '=', $order->product_id)
            ->where('warehouse_storages.warehouse_id', '=', $warehouseId)
            ->where('warehouse_storages.quantity', '>', '0')
            ->select(
                DB::raw('CONCAT(warehouse_storages.position,"-", warehouse_storages.position_height) warehouse_position'),
                'warehouse_storages.quantity',
                'warehouse_storages.expiration_date'
            )
            ->orderBy('warehouse_storages.expiration_date', 'ASC')
            ->first();

        if ($warehouse_position) {
            $order->warehouse_position = $warehouse_position->warehouse_position;
            $order->warehouse_storage = $warehouse_position->quantity;
            $order->expiration_date = $warehouse_position->expiration_date == null ? "No registra" : $warehouse_position->expiration_date;
        } else {
            $order->warehouse_position = "No registra";
            $order->warehouse_storage = "No registra";
            $order->expiration_date = "No registra";
        }

        return $order;
    }

}


