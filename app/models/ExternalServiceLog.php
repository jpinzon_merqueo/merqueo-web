<?php

use \Illuminate\Database\Eloquent\Model;

/**
 * Class ExternalServiceLog
 */
class ExternalServiceLog extends Model
{
    protected $table = 'external_service_log';

    protected $fillable = ['type'];

    protected $hidden = ['created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function admin()
    {
        return $this->belongsTo('Admin');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function externalService()
    {
        return $this->belongsTo('ExternalService');
    }

    /**
     * @param ExternalService $external_service
     * @param $type
     */
    public static function make(ExternalService $external_service, $type)
    {
        $log = new ExternalServiceLog(compact('type'));
        $log->externalService()->associate($external_service);
        if (\BackendAuth::check()) {
            $log->admin()->associate(\BackendAuth::user());
        }

        if (\Auth::check() && \Auth::user()->id == $external_service->user_id) {
            $log->user()->associate(\Auth::user());
        }

        $log->save();
    }
}