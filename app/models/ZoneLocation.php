<?php

/**
 * @property string $delivery_zone
 * @property ZoneLocationSlot[] $zoneLocationSlots
 * @property string $name
 * @property int $status
 */
class ZoneLocation extends \Illuminate\Database\Eloquent\Model
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function zoneLocationSlots()
    {
        return $this->hasMany('ZoneLocationSlot');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function zone()
    {
        return $this->belongsTo('Zone');
    }
}
