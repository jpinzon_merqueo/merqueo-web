<?php

class ProviderOrderDetailProductGroup extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'provider_order_detail_product_group';

    public function storeProduct()
    {
    	return $this->belongsTo('StoreProduct', 'store_product_id');
    }
}