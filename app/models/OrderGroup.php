<?php

/**
 * @property string $user_address_latitude
 * @property string $user_address_longitude
 * @property float $delivery_amount
 * @property float total_amount
 * @property Zone $zone
 * @property float|int|mixed $discount_amount
 * @property Order[]|\Illuminate\Support\Collection orders
 */
class OrderGroup extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'order_groups';

    public function orderProducts()
    {
        return $this->hasManyThrough('OrderProduct', 'Order', 'group_id', 'order_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders()
    {
        return $this->hasMany('Order', 'group_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function userDevice()
    {
        return $this->belongsTo('UserDevice');
    }
    /**
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */
    public function locality()
    {
        return $this->belongsTo('Locality');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function zone()
    {
        return $this->belongsTo('Zone');
    }

    /**
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo(City::class, 'user_city_id');
    }

    /**
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function address()
    {
        return $this->belongsTo('UserAddress', 'address_id');
    }

    public function getStoresAttribute()
    {
        $stores = array();

        foreach ($this->orders as $order) {
            $stores[] = $order->store;
        }

        return $stores;
    }

    /**
     * Devuelve los nombres de las tiendas
     *
     * @return integer Número de órdenes
     */
    public function getstoreNamesAttribute()
    {
        $names = array();

        foreach ($this->stores as $store) {
            $names[] = $store->name;
        }

        sort($names);

        return implode(", ", $names);
    }

    /**
     * Devuelve el descuento de precios especiales del grupo
     *
     * @return double Descuento
     */
    public function getSpecialPriceAttribute()
    {
        $specialPrice = 0;

        foreach ($this->orderProducts as $orderProduct) {
            if ($orderProduct->promo_type) {
                $specialPrice += ($orderProduct->original_price - $orderProduct->price) * $orderProduct->quantity;
            }
        }

        return $specialPrice;
    }

    /**
     * Devuelve la cantidad de órdenes del grupo
     *
     * @return integer Número de órdenes
     */
    public function getOrderCountAttribute()
    {
        $orderCount = OrderGroup::select(DB::raw('COUNT(orders.id) as count'))
            ->join('orders', 'orders.group_id', '=', 'order_groups.id')
            ->where('order_groups.id', $this->id)
            ->first();

        return $orderCount->count;
    }

    /**
     * Devuelve la cantidad de ítems del grupo
     *
     * @return integer Número de items
     */
    public function getItemCountAttribute()
    {
        return count($this->orderProducts);
    }

    /**
     * Devuelve el número de órdenes entregadas del grupo
     *
     * @return integer Número de órdenes entregadas
     */
    public function getDeliveredAttribute()
    {
        return $this->getOrderCountByStatus('Delivered');
    }

    /**
     * Devuelve el número de órdenes canceladas del grupo
     *
     * @return integer Número de órdenes canceladas
     */
    public function getCancelledAttribute()
    {
        return $this->getOrderCountByStatus('Cancelled');
    }

    /**
     * Devuelve el número de órdenes del grupo por estado
     *
     * @return integer Número de órdenes por estado
     */
    private function getOrderCountByStatus($status)
    {
        $orderCount = OrderGroup::select(DB::raw('COUNT(orders.id) as count'))
            ->join('orders', 'orders.group_id', '=', 'order_groups.id')
            ->where('order_groups.id', $this->id)
            ->where('orders.status', $status)
            ->first();

        return $orderCount->count;
    }

    /**
     * Actualiza los valores total dependiendo de sus ordenenes hijas.
     */
    public function updateTotals()
    {
        $total_total_amount = 0;
        $total_delivery_amount = 0;
        $total_discount_amount = 0;
        $this->load('orders');
        foreach ($this->orders as $order) {
            if ($order->status === \orders\OrderStatus::CANCELED) {
                continue;
            }

            $total_total_amount += $order->total_amount;
            $total_delivery_amount += $order->delivery_amount;
            $total_discount_amount += $order->discount_amount;
        }

        $this->total_amount = $total_total_amount;
        $this->delivery_amount = $total_delivery_amount;
        $this->discount_amount = $total_discount_amount;
    }

    /**
     * Agrega un descuento al domicilio.
     *
     * @param int $discount_amount
     */
    public function addDeliveryDiscount($discount_amount)
    {
        foreach ($this->orders as $order) {
            if ($order->delivery_amount) {
                $order->delivery_amount -= $discount_amount;
                if ($order->delivery_amount < 0) {
                    $order->delivery_amount = 0;
                }
                $order->save();
                break;
            }
        }
    }
}
