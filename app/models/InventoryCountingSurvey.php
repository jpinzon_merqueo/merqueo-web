<?php

class InventoryCountingSurvey extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'inventory_counting_survey';

    public function admin() {
      return $this->belongsTo('Admin');
    }

    public function inventoryCounting() {
      return $this->belongsTo('InventoryCounting');
    }

}
