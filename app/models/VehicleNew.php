<?php

class VehicleNew extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'vehicle_news';

    public function admin() {
      return $this->belongsTo('Admin');
    }

    public function vehicle() {
        return $this->belongsTo('Vehicle');
    }

}
