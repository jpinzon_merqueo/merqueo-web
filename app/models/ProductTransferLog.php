<?php

class ProductTransferLog extends Illuminate\Database\Eloquent\Model
{
    protected $table = 'product_transfer_logs';

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }
}