<?php

use \Illuminate\Database\Eloquent\Model;

/**
 * Class DeliveryWindow
 * @property string $hourStart
 * @property string $hourEnd
 * @property City $city
 * @property string $shifts
 * @property string $delivery_window
 * @property int $status
 */
class ProviderOrderDeliveryWindow  extends Model
{
    protected $fillable = ['hour_start', 'hour_end', 'status', 'delivery_window'];

    protected $table = 'provider_order_delivery_windows';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class);
    }

    /**
     * Función para validar la exitencia de una franja horaria
     *
     * @param $hourStart
     * @param $hourEnd
     * @param $shift
     * @param $city_id
     * @param null $id_window
     * @return array
     */
    public static function exist($hourStart, $hourEnd, $warehouseId, $id_window = null)
    {
        if($hourStart >= $hourEnd){
            return [
                'status' => false,
                'message' => 'La hora de inicio no puede ser mayor o igual a la hora finalización.'
            ];
        }



        $delivery_windows = self::select('*')
            ->where('hour_start', $hourStart)
            ->where('hour_end', $hourEnd)
            ->where('warehouse_id', $warehouseId);

        if(!empty($id_window)) {
            $delivery_windows->where('id', '<>', $id_window);
        }

        $delivery_windows =    $delivery_windows->get();

        if(count($delivery_windows)){
            return [
                'status' => false,
                'message' => 'Ya existe una franja horaria con estos parametros.'
            ];
        }

        $hourStart_between = self::select('*')
            ->whereRaw("hour_start < '{$hourStart}' AND hour_end > '{$hourStart}'")
            ->where('warehouse_id', $warehouseId);

        if(!empty($id_window)){
            $hourStart_between->where('id', '<>', $id_window);
        }

        $hourStart_between = $hourStart_between->get();

        if(count($hourStart_between)){
            return [
                'status' => false,
                'message' => 'La hora de inicio se encuentra contemplada en una franja horaria existente.'
            ];
        }

        $hourEnd_between = self::select('*')
            ->whereRaw("hour_start < '{$hourEnd}' AND  hour_end > '{$hourEnd}'")
            ->where('warehouse_id', $warehouseId);

        if(!empty($id_window)){
            $hourEnd_between->where('id', '<>', $id_window);
        }

        $hourEnd_between = $hourEnd_between->get();

        if(count($hourEnd_between)){
            return [
                'status' => false,
                'message' => 'La hora de finalización se encuentra contemplada en una franja horaria existente.'
            ];
        }

        return [ 'status'=> true ];
    }


    /**
     * Retorna las franjas activas por bodega
     *
     * @param $warehouseId
     * @return mixed
     *
     */
    public static function getDeliveryWindowsByWarehouse($warehouseId)
    {
        $delivery_windows = self::where('warehouse_id', $warehouseId)
            ->where('status', 1)
            ->orderBy('hour_start', 'ASC')
            ->with('warehouse')
            ->get();

        return $delivery_windows;
    }
}
