<?php

class CreditCardBin extends \Illuminate\Database\Eloquent\Model
{
    /**
     * @var string
     */
    protected $table = 'credit_card_bines';

    /**
     * @var array
     */
    protected $fillable = [
        'bin',
        'reference',
    ];
}
