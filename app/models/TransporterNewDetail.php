<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 21/08/2018
 * Time: 18:22
 */

class TransporterNewDetail extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'transporter_new_details';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function transporterNew() {
        return $this->belongsTo('TransporterNew');
    }

    /**
     * @return \Illuminate\Support\Collection|null|static
     */
    public function orderProducts() {
        if($this->product_type == 'Agrupado'){
            return OrderProductGroup::find($this->order_product_id);
        }else{
            return OrderProduct::find($this->order_product_id);
        }
    }
}