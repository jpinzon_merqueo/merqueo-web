<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class RoutesLog extends Model
{
    protected $table = 'routes_log';

    public static function add(array $data ){
        $log = new RoutesLog();
        $log->type = $data['type'];
        $log->planning = $data['planning'];
        $log->admin_id = \Illuminate\Support\Facades\Session::get('admin_id');
        $log->request = $data['request'];
        $log->response = $data['response'];
        $log->hour_start = $data['hour_start'];
        $log->hour_end = $data['hour_end'];
        $log->save();
    }
 
}
