<?php

use \Illuminate\Database\Eloquent\Model;

/**
 * Class ProductDetail
 * @property string $type
 * @property string $name
 * @property string $descripcion
 */
class ProductDetail extends Model
{
    const SPECIFICATION = 'Especificación';

    /**
     * {@inheritdoc}
     */
    protected $fillable = [
        'type', 'name', 'description'
    ];

    protected $hidden = [
        'product_id', 'created_at', 'updated_at'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
