<?php

/**
 * @property int $delivery_window_id
 * @property int $day
 * @property int $status
 */
class ZoneLocationSlot extends \Illuminate\Database\Eloquent\Model
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function zoneLocation()
    {
        return $this->belongsTo('ZoneLocation');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function deliveryWindow()
    {
        return $this->belongsTo('DeliveryWindow');
    }
}