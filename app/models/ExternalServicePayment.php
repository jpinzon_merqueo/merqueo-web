<?php

use \Illuminate\Database\Eloquent\Model;

/**
 * Class ExternalServicePayment
 * @property ExternalService $externalService
 * @property string $cc_charge_id
 * @property ExternalServicePaymentRefund[] refunds
 */
class ExternalServicePayment extends Model
{
    protected $table = 'external_service_payments';

    protected $hidden = ['created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function admin()
    {
        return $this->belongsTo('Admin');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function externalService()
    {
        return $this->belongsTo('ExternalService');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function refunds()
    {
        return $this->hasMany('ExternalServicePaymentRefund');
    }
}