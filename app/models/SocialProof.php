<?php

use \Illuminate\Database\Eloquent\Model;

/**
 * Class SocialProof
 */
class SocialProof extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'product_id',
        'total',
    ];
}
