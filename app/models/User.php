<?php

use App\Models\PaymentMethod;
use Carbon\Carbon;
use exceptions\MerqueoException;
use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use exceptions\ReferenceCodeException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Query\Builder;
use Merqueo\Backend\Auth\AccountKit\AccountKit;
use users\UserAttributes;

/**
 * Class User
 * @property UserAddress[] $userAddresses
 * @property string $first_name
 * @property string $email
 * @property string $last_name
 * @property string $phone
 * @property string $created_at
 * @property UserCreditCard[] $creditCards
 * @property int $id
 * @property bool $phone_validated_date
 * @property Order[] orders
 * @property Carbon free_delivery_expiration_date
 */
class User extends Model implements UserInterface, RemindableInterface
{
    use UserTrait, RemindableTrait, UserAttributes;

    /**
     * @type string
     */
    const DARKSUPERMARKET_TYPE = [
        'domicilios.com',
        'uberEats',
        'convenio-domicilios.com',
        'ifood',
        'frutos-del-bosque'
    ];

    const DARKSUPERMAKET_MEXICO = [659046, 659047];

    const SMALLER_AMOUNTS_USER = 522476;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password');

    /**
     * @var null
     */
    protected $ordersTotal = null;

    /**
     * @var null
     */
    protected $discountTotal = null;

    /**
     * The parameters convert to float.
     *
     * @var array
     */
    private $parametersCast = ["price","special_price","delivery_discount_amount"];

    /**
     * {@inheritDoc}
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        self::saved(function (User $user) {
            UpdateLeanplumUser::upload($user->id);
        });

        self::creating(function (User $model) {

            try {
                $city_slug = 'bogota';
                $store_id = 63;
                $store_slug = 'super-ahorro';

                $url_web = route('frontStore.all_departments', [
                    'city_slug' => $city_slug,
                    'store_slug' => $store_slug
                ]);

                $dynamic_link = FirebaseClient::generateDynamicLink([
                    'url' => $url_web,
                    'type' => 'referral',
                    'store_id' => $store_id,
                    'referral_code' => $model->referral_code
                ]);

                $model->referral_url = $dynamic_link->shortLink;
            } catch (Exception $e) {
                ErrorLog::add($e);
            }
        });
    }

    /**
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function carts()
    {
        return $this->hasMany('Cart');
    }

    /**
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders()
    {
        return $this->hasMany('Order');
    }

    /**
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function addresses()
    {
        return $this->hasMany('UserAddress');
    }

    /**
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function creditCards()
    {
        return $this->hasMany('UserCreditCard', 'user_id');
    }

    /**
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function userAddresses()
    {
        return $this->hasMany(UserAddress::class);
    }

    /**
     * @return HasMany
     */
    public function externalServices()
    {
        return $this->hasMany('ExternalService');
    }

    /**
     * @return BelongsTo
     */
    public function referredBy()
    {
        return $this->belongsTo('User', 'referred_by');
    }

    /**
     * @return HasMany
     */
    public function referrals()
    {
        return $this->hasMany('User', 'referred_by');
    }

    /**
     * @return HasMany
     */
    public function orderGroups()
    {
        return $this->hasMany('OrderGroup');
    }

    /**
     * @return HasMany
     */
    public function userCredits()
    {
        return $this->hasMany('UserCredit');
    }

    public static function add($first_name, $last_name, $phone, $email, $password = null, $type = 'merqueo.com')
    {
        $user = new User;

        $user->first_name = $first_name;
        $user->last_name = $last_name;
        $user->phone = $phone;
        $user->email = $email;
        $user->type = $type;
        $password = $password === null ? $email : $password;
        $user->password = Hash::make($password);
        $user->referral_code = self::generateReferralCode($first_name, $last_name);

        $user->save();

        return $user;
    }

    /**
     * Devuelve 1 si el usuario ha comprado en una tienda, de lo contrario 0
     *
     * @param int $store_id Id de la tienda
     *
     * @return boolean El usuario ha comprado en la tienda
     */
    private function hasOrderedFromStore($store_id)
    {
        $orderCount = Order::select(DB::raw('COUNT(id) as count'))
            ->where('store_id', $store_id)
            ->where('status', 'Delivered')
            ->where('user_id', $this->id)
            ->from('orders')
            ->first();

        if ($orderCount->count > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    /*
     * Obtiene credito disponible del usuario
     *
     * @param int|null id del País (optional)
     */
    public function getCreditAvailable($countryId = null)
    {
        return UserCredit::getCreditAvailable($this->id, $countryId);
    }

    /**
     * Obtiene direcciones
     *
     * @param int $store_id ID de la tienda
     * @return array $addresses Direcciones del usuario
     */
    public function getAddresses()
    {
        $addresses = UserAddress::select(
            'user_address.id',
            'city_id',
            'label',
            'address',
            'address_further',
            'neighborhood',
            'user_address.latitude',
            'user_address.longitude',
            'address_1',
            'address_2',
            'address_3',
            'address_4',
            DB::raw('IF (parent_city_id IS NULL, city_id, parent_city_id) AS parent_city_id')
        )
            ->join('cities', 'city_id', '=', 'cities.id')
            ->where('user_id', $this->id)
            ->get();
        if (!$addresses)
            $addresses = [];

        return $addresses;
    }

    /**
     * Obtiene productos comprados del usuario
     *
     * @param int $store_id ID de la tienda
     * @param int $warehouse_id ID de bodega
     * @param string $special_price_fields Campos de precio promocion
     * @param int $page Número de pagina
     * @param int $rows Número items por de pagina
     * @return array $user_products Productos comprados del usuario
     */
    public function getProducts($store_id, $warehouse_id, $special_price_fields = null, $page = 1, $rows = 60)
    {
        if (!$special_price_fields) {
            $user_orders_info = User::getOrdersInfo($this->id);
            $has_orders = $user_orders_info->qty ? $user_orders_info->qty : false;
            $special_price_fields = StoreProduct::getRawSpecialPriceQuery($has_orders);
        }

        $begin = $page == 1 ? 0 : ($page - 1) * $rows;

        $user_products = StoreProduct::select(
            'store_products.id',
            'store_products.public_price',
            'products.slug',
            'products.name',
            'store_products.price',
            $special_price_fields,
            DB::raw('MAX(DATE(orders.date)) AS last_order_date'),
            'store_products.first_order_special_price',
            'products.image_large_url',
            'products.image_medium_url',
            'products.image_small_url',
            'products.image_app_url',
            'store_products.is_best_price',
            'products.quantity',
            'products.unit',
            'products.description',
            'products.nutrition_facts',
            'store_products.store_id',
            'store_products.department_id',
            'store_products.shelf_id',
            'shelves.has_warning',
            StoreProduct::getRawDeliveryDiscountByDate(),
            DB::raw('COUNT(products.id) AS quantity_products'),
            DB::raw('IF(store_products.allied_store_id IS NULL, 0, ' . Config::get('app.marketplace_delivery_days') . ') AS marketplace_delivery_days'),
            'departments.slug as department_slug',
            'shelves.slug as shelf_slug',
            'store_products.provider_id',
            'products.type'
        )
            ->join('products', 'store_products.product_id', '=', 'products.id')
            ->join('order_products', 'store_products.id', '=', 'order_products.store_product_id')
            ->join('orders', 'orders.id', '=', 'order_products.order_id')
            ->join('departments', 'departments.id', '=', 'store_products.department_id')
            ->join('shelves', 'shelves.id', '=', 'store_products.shelf_id')
            ->join('stores', 'stores.id', '=', 'departments.store_id')
            ->join('store_product_warehouses', 'order_products.store_product_id', '=', 'store_product_warehouses.store_product_id')
            ->pum()
            ->where('user_id', $this->id)
            ->where('order_products.store_id', $store_id)
            ->where('orders.status', '<>', 'Cancelled')
            ->where('store_product_warehouses.status', 1)
            ->where('store_product_warehouses.is_visible', 1)
            ->where('store_product_warehouses.warehouse_id', $warehouse_id)
            ->whereRaw('IF(store_product_warehouses.manage_stock = 1, IF(store_product_warehouses.is_visible_stock = 0, IF( store_product_warehouses.current_stock > 0, TRUE, FALSE), TRUE), TRUE)')
            ->where('products.type', '<>', 'Proveedor')
            ->whereNotExists(function ($query) {
                $query->select('store_products.id')
                    ->from('sampling')
                    ->whereRaw('store_products.id = sampling.store_product_id');
            })
            ->groupBy('products.id')
            ->orderBy('quantity_products', 'desc')
            ->limit($rows)
            ->offset($begin)
            ->get();

        $user_products = castValueToFloat($user_products, $this->parametersCast);
        $user_products = setToNullIfZero($user_products, ['special_price']);

        return $user_products->count() ? $user_products : false;
    }

    /**
     * Obtiene ultimos pedidos del cliente
     *
     * @param string $only_pending Solo pedidos pendientes
     * @param bool $skip_pending Omitir pedidos pendientes
     * @return array|Builder $orders_user Datos de pedidos
     */
    public function getOrders($only_pending = null, $skip_pending = null, $countryId = null)
    {
        $headers = getallheaders();
        $user_agent = isset($headers['User-Agent']) && strstr($headers['User-Agent'], 'iOS') ? 'iOS' : 'Android';

        $orders_user = [];

        $orders = Order::join('order_groups', 'group_id', '=', 'order_groups.id')
            ->join('stores', 'orders.store_id', '=', 'stores.id')
            ->join('cities', 'stores.city_id', '=', 'cities.id')
            ->where('orders.is_hidden', 0)
            ->where('orders.user_id', $this->id)
            ->orderBy('orders.id', 'DESC');

        $is_version_2115 = version_compare(Input::get('app_version'), '2.1.15', '>=') && $user_agent == 'Android';

        if ($is_version_2115) {
            $orders->select(
                'orders.id',
                'orders.store_id',
                'orders.reference',
                'orders.status',
                'orders.date',
                DB::raw('orders.total_amount + orders.delivery_amount - orders.discount_amount AS total_amount')
            );
        } else {
            $orders->select(
                'orders.id',
                'orders.reference',
                'orders.status',
                'orders.store_id',
                'orders.date',
                'orders.total_products',
                'orders.total_amount',
                'orders.delivery_date',
                'orders.delivery_time',
                'orders.driver_id',
                'orders.total_amount AS subtotal',
                'orders.delivery_amount',
                'orders.discount_amount',
                'orders.payment_method',
                'user_score',
                'order_groups.user_city_id AS city_id',
                DB::raw("CONCAT(order_groups.user_address, ' ', order_groups.user_address_further) AS user_address"),
                'user_score_date',
                'stores.name AS store_name',
                'stores.app_logo_small_url AS store_logo_url',
                'stores.is_storage',
                'orders.driver_id',
                'orders.management_date'
            );
        }

        if ($only_pending) {
            $orders->where(function ($query) {
                $query->whereNotIn('orders.status', ['Delivered', 'Cancelled'])
                    ->orWhereRaw("TIMESTAMPDIFF(HOUR, management_date, '" . date('Y-m-d H:i:s') . "') < 12");
            });
        }

        if ($skip_pending) {
            $orders->whereIn('orders.status', ['Delivered', 'Cancelled']);
        }

        if (!empty($countryId)) {
            $orders->where('cities.country_id', $countryId);
        }

        $orders = $orders->paginate(10);

        if (version_compare(Input::get('app_version'), '2.1.11', '<') && $user_agent == 'iOS') {
            $status = [
                'Validation' => 'Iniciado',
                'Initiated' => 'Iniciado',
                'Enrutado' => 'En Proceso',
                'In Progress' => 'En Proceso',
                'Alistado' => 'En Proceso',
                'Dispatched' => 'En Camino',
                'Delivered' => 'Entregado',
                'Cancelled' => 'Cancelado'
            ];
        } else {
            $status = [
                'Validation' => 'Recibido',
                'Initiated' => 'Recibido',
                'Enrutado' => 'Recibido',
                'In Progress' => 'Recibido',
                'Alistado' => 'Alistado',
                'Dispatched' => 'Despachado',
                'Delivered' => 'Entregado',
                'Cancelled' => 'Cancelado'
            ];
        }

        if ($orders->count()) {
            foreach ($orders as $order) {
                if ($order->type == 'Marketplace') {
                    $order->delivery_date_message = 'La fecha de entrega de este pedido es diferente a la que seleccionaste';
                }

                $order->total_amount = $order->total_amount;
                $order->status_value = $order->status;
                $order->status_name = $order->status = $status[$order->status];
                if ($is_version_2115) {
                    $order->status = $order->status_value;
                    $order->date = format_date('normal_long_with_time', $order->date);
                } else {
                    $order->discount_amount = $order->discount_amount;
                    $order->subtotal = $order->subtotal;
                    $order->total_amount = $order->total_amount + $order->delivery_amount - $order->discount_amount;                
                }

                if ($is_version_2115) {
                    continue;
                }

                //no enviar productos
                if (version_compare(Input::get('app_version'), '2.1.8', '>=') && $user_agent == 'Android') {
                    $order->products = [];
                    $orders_user[] = $order->toArray();
                    continue;
                }

                $order_products = OrderProduct::select(
                    'store_product_id AS id',
                    'price',
                    'quantity AS quantity_cart',
                    'product_name AS name',
                    'product_image_url AS image_url',
                    'product_quantity AS quantity',
                    'product_unit AS unit',
                    'fulfilment_status AS status'
                )
                    ->where('order_id', $order->id)
                    ->where('is_gift', 0)
                    ->orderBy(DB::raw("FIELD(fulfilment_status, 'Not Available', 'Fullfilled', 'Replacement')"))
                    ->get();
                //obtener productos regalo
                if ($order->date > '2018-01-01 00:00:00') {
                    foreach ($order_products as $order_product) {
                        if ($order_product->status == 'Not Available') {
                            $order_product_gift = OrderProduct::select(
                                'store_product_id AS id',
                                'price',
                                'quantity AS quantity_cart',
                                'product_name AS name',
                                'product_image_url AS image_url',
                                'product_quantity AS quantity',
                                'product_unit AS unit',
                                'fulfilment_status AS status'
                            )
                                ->where('parent_id', $order_product->id)
                                ->where('order_id', $order->id)
                                ->where('is_gift', 1)
                                ->first();
                            if ($order_product_gift) {
                                $order_product_gift->message = 'Lamentamos que este producto no esté disponible, sin embargo te damos este completamente gratis.';
                                $order_product->gift = $order_product_gift->toArray();
                            }
                        }
                    }
                }

                $countryCode = Country::join(
                    'cities',
                    'countries.id',
                    '=',
                    'cities.country_id'
                )->join(
                    'city_store',
                    'cities.id',
                    '=',
                    'city_store.city_id'
                )->where('city_store.store_id', '=', $order->store_id)->first()->country_code;

                if ($countryCode === Country::COUNTRY_CODE_MEXICO
                    && $order['payment_method'] === PaymentMethod::DATAFONO
                ) {
                    $order['payment_method'] = PaymentMethod::TERMINAL;
                }

                $order->products = $order_products->toArray();

                $orders_user[] = $order->toArray();
            }
        }

        if ($is_version_2115) {
            return $orders;
        }

        // foreach ($orders_user as $i => $orderUser) {
        //     if ($countryCode === Country::COUNTRY_CODE_MEXICO
        //         && $orderUser['payment_method'] === PaymentMethod::DATAFONO
        //     ) {
        //         $orders_user[$i]['payment_method'] = PaymentMethod::TERMINAL;
        //     }
        // }

        return $orders_user;
    }

    /**
     * Obtiene cantidad de pedidos realizados, ultima fecha de pedido y pedido pendiente de calificacion
     *
     * @param $user_id
     * @return int Cantidad de pedidos
     */
    public static function getOrdersInfo($user_id)
    {
        return Order::join('order_groups', 'group_id', '=', 'order_groups.id')
            ->where('order_groups.source', '<>', 'Reclamo')
            ->where('orders.status', 'Delivered')
            ->where('orders.user_id', $user_id)
            ->select(
                DB::raw(
                    "COUNT(orders.id) AS qty, 
                    MAX(management_date) AS last_order_date, 
                    MAX(
                        IF(user_score_date IS NULL AND DATE(management_date) > '" . date('Y-m-d', strtotime('-5 days')) . "', orders.id, FALSE)
                    ) AS order_id_pending_score"
                )
            )
            ->first();
    }

    /**
     * Obtiene la fecha del ultimo pedido realizado por un
     * usuario y la cantidad de los mismo teniendo en
     * cuenta una fecha limite.
     *
     * @param $userId
     * @param $promoBeginLimit
     * @return array
     */
    public static function getLastOrderDateFromDate($userId, $promoBeginLimit)
    {
        $limit = Order::join('order_groups', 'group_id', '=', 'order_groups.id')
            ->where('order_groups.source', '<>', 'Reclamo')
            ->where('orders.status', 'Delivered')
            ->where('orders.user_id', $userId)
            ->where('management_date', '<=', $promoBeginLimit->toDateString())
            ->select(DB::raw('MAX(management_date) AS last_order_date, COUNT(orders.id) AS total_orders'))
            ->first();

        if (empty($limit->last_order_date)) {
            return null;
        }

        return [new Carbon\Carbon($limit->last_order_date), $limit->total_orders];
    }

    /**
     * Valida y carga referido a usuario
     *
     * @param object $user Usuario
     * @param string $referral_code Codigo de referido
     * @param bool $use_only_session_value
     * @return array $referred Datos de referido
     */
    public static function validateReferred($user, $referral_code = null, $use_only_session_value = false)
    {
        //validar codigo de referido
        $referred['success'] = $referred['limit'] = false;
        //$referred['referred_free_delivery_days'] = Config::get('app.referred.free_delivery_days');
        $referred['amount'] = Config::get('app.referred.amount');
        $referred['amount_expiration_days'] = Config::get('app.referred.amount_expiration_days');
        $referred['referred_by_amount'] = Config::get('app.referred.referred_by_amount');
        if (!$referral_code) {
            if (!$use_only_session_value && Input::has('promo_code')) {
                $referral_code = Input::get('promo_code');
            }
            if (Session::has('referred_code')) {
                $referral_code = Session::get('referred_code');
            }
        }
        if ($referral_code && !empty($referral_code)) {
            //referido
            $referrer = User::where('referral_code', trim($referral_code))->where('id', '<>', $user->id)->where('referral_code_blocked', '=', 0)->first();
            if ($referrer) {
                if ($referrer->total_referrals < Config::get('app.referred.limit')) {
                    //registrar credito a nuevo usuario
                    $expiration_date = date('Y-m-d', strtotime('+' . $referred['amount_expiration_days'] . ' day'));
                    UserCredit::addCredit($user, $referred['amount'], $expiration_date, 'referred', $referrer);

                    $user->referred_by = $referrer->id;
                    $user->save();

                    $referred['referrer_name'] = $referrer->first_name;
                    $referred['amount_expiration_date'] = $expiration_date;
                    $referred['success'] = true;
                } else {
                    $referred['limit'] = true;
                }
            }
        }

        $referred['error'] = !$referred['success'] && !$referred['limit'] && Input::has('promo_code') ? true : false;

        return $referred;
    }

    /**
     * Obtener dias de domicilio gratis, crédito disponible e informacion de cupon activo si lo hay
     *
     * @param int $user_id ID usuario
     * @param null $coupon_code
     * @param bool $is_new_device
     * @param bool $ignore_referred_code
     * @return array $credit Datos de usuario disponible
     */
    public static function getDiscounts(
        $user_id = null,
        $coupon_code = null,
        $is_new_device = true,
        $ignore_referred_code = false
    ) {
        $discounts = [
            'coupon_code' => 0,
            'amount' => 0,
            'free_delivery_days' => 0,
            'free_delivery_next_order' => 0,
            'minimum_discount_amount' => Config::get('app.minimum_discount_amount')
        ];

        $user_id = intval($user_id);
        if (Auth::check() || $user_id) {
            $user = ($user_id != null)
                ? ($user_id instanceof User ? $user_id : User::find($user_id))
                : (Auth::check() ? Auth::user() : null);

            $city = City::find(Input::get('city_id'));
            $countryId = null;
            if ($city) {
                $countryId = $city->country_id;
            }

            // Crédito disponible
            $discounts['amount'] = $user->getCreditAvailable($countryId);

            // Domicilio gratis
            if ($user->free_delivery_expiration_date >= date('Y-m-d')) {
                $diff = date_diff(new DateTime($user->free_delivery_expiration_date . ' 00:00:00'), new DateTime(date('Y-m-d 00:00:00')));
                $discounts['free_delivery_days'] = !intval($diff->format('%a')) ? 1 : intval($diff->format('%a'));
            }

            $valid_free_delivery = $user->free_delivery_next_order && (!$user->free_delivery_next_order_expiration_date ||
                $user->free_delivery_next_order_expiration_date >= date('Y-m-d'));
            if (!$discounts['free_delivery_next_order'] && $valid_free_delivery) {
                $discounts['free_delivery_next_order'] = 1;
                if ($user->free_delivery_next_order_expiration_date) {
                    $diff = date_diff(new DateTime($user->free_delivery_next_order_expiration_date . ' 00:00:00'), new DateTime(date('Y-m-d 00:00:00')));
                    $discounts['free_delivery_days'] = !intval($diff->format('%a')) ? 1 : intval($diff->format('%a'));
                }
            }
        } else {
            // Domicilio gratis primera compra
            $discounts['free_delivery_next_order'] = $is_new_device;
        }

        // Código de referido
        if (!$ignore_referred_code && (Session::has('referred_code') || Input::get('coupon_code') || $coupon_code)) {
            $referred_code = 0;

            if (Session::has('referred_code')) {
                $referred_code = Session::get('referred_code');
            }

            if (Input::has('coupon_code')) {
                $referred_code = Input::get('coupon_code');
            }

            if ($coupon_code) {
                $referred_code = $coupon_code;
            }

            $user_id = $user_id ? $user_id : 0;
            $referrer = User::where('referral_code', $referred_code)
                ->where('id', '<>', $user_id)
                ->where('status', 1)
                ->first();

            if ($referrer) {
                $discounts['amount'] += Config::get('app.referred.amount');
            }
        }
        // Cupón activo
        if (Session::has('coupon_code') || Input::get('coupon_code') || $coupon_code) {
            if (Session::has('coupon_code')) {
                $coupon_code = Session::get('coupon_code');
            }

            if (Input::has('coupon_code')) {
                $coupon_code = Input::get('coupon_code');
            }

            $coupon = Coupon::where('code', $coupon_code)
                ->where('status', 1)
                ->first();

            if ($coupon) {
                $discounts['coupon'] = $coupon;
            }
        }

        if ($discounts['amount'] < 0) {
            $discounts['amount'] = 0;
        }

        return $discounts;
    }

    /**
     * Genera codigo de referido
     *
     * @param string $first_name Nombre
     * @param string $last_name Apellido
     *
     * @return string $code Codigo de referido
     */
    public static function generateReferralCode($first_name, $last_name)
    {
        $unwanted_array = array(
            'Š' => 'S',
            'š' => 's',
            'Ž' => 'Z',
            'ž' => 'z',
            'À' => 'A',
            'Á' => 'A',
            'Â' => 'A',
            'Ã' => 'A',
            'Ä' => 'A',
            'Å' => 'A',
            'Æ' => 'A',
            'Ç' => 'C',
            'È' => 'E',
            'É' => 'E',
            'Ê' => 'E',
            'Ë' => 'E',
            'Ì' => 'I',
            'Í' => 'I',
            'Î' => 'I',
            'Ï' => 'I',
            'Ñ' => 'N',
            'Ò' => 'O',
            'Ó' => 'O',
            'Ô' => 'O',
            'Õ' => 'O',
            'Ö' => 'O',
            'Ø' => 'O',
            'Ù' => 'U',
            'Ú' => 'U',
            'Û' => 'U',
            'Ü' => 'U',
            'Ý' => 'Y',
            'Þ' => 'B',
            'ß' => 'Ss',
            'à' => 'a',
            'á' => 'a',
            'â' => 'a',
            'ã' => 'a',
            'ä' => 'a',
            'å' => 'a',
            'æ' => 'a',
            'ç' => 'c',
            'è' => 'e',
            'é' => 'e',
            'ê' => 'e',
            'ë' => 'e',
            'ì' => 'i',
            'í' => 'i',
            'î' => 'i',
            'ï' => 'i',
            'ð' => 'o',
            'ñ' => 'n',
            'ò' => 'o',
            'ó' => 'o',
            'ô' => 'o',
            'õ' => 'o',
            'ö' => 'o',
            'ø' => 'o',
            'ù' => 'u',
            'ú' => 'u',
            'û' => 'u',
            'ý' => 'y',
            'þ' => 'b',
            'ÿ' => 'y',
            'Ğ' => 'G',
            'İ' => 'I',
            'Ş' => 'S',
            'ğ' => 'g',
            'ı' => 'i',
            'ş' => 's',
            'ü' => 'u'
        );
        $first_name = strtr($first_name, $unwanted_array);
        $last_name = strtr($last_name, $unwanted_array);

        $first_name = str_replace(' ', '', $first_name);
        $last_name = substr($last_name, 0, 2);
        $code_prefix = $first_name . $last_name;
        $i = 0;
        while (1) {
            if ($i != 0) {
                $code = $code_prefix . $i;
            } else {
                $code = $code_prefix;
            }
            $count = User::where('referral_code', $code)->count();
            if ($count == 0) {
                return $code;
            }
            $i++;
        }
    }

    /**
     * @param $expiration_date
     */
    public function setNextDeliveryFree($expiration_date)
    {
        $this->free_delivery_next_order = 1;
        $this->free_delivery_next_order_expiration_date = empty($expiration_date) ? null : $expiration_date;
    }

    /**
     * Retorna la consulta que determina cuales son los pedidos
     * del usuario en los cuales el método de pago fue Visa.
     *
     * @return HasMany
     */
    public function ordersWithVisa()
    {
        return $this->orders()
            ->join('order_groups', 'order_groups.id', '=', 'orders.group_id')
            ->where('orders.cc_type', 'VISA')
            ->where('orders.is_hidden', 0)
            //->where('source', '<>', 'Reclamo')
            ->where('orders.status', 'Delivered');
    }

    /**
     * Valida si un usuario tiene pedidos con una tarjeta especifica
     *
     * @return Object
     */
    public function hasOrderWithCreditCard($holder_document_number, $last_four)
    {
        return $this->orders()
            ->where('cc_holder_document_number', $holder_document_number)
            ->where('cc_last_four', $last_four)
            ->where('cc_type', 'VISA')
            ->where('orders.status', '<>', 'Cancelled')
            ->first();
    }

    /**
     * Obtiene los datos basicos usados en el
     * seguimiento de los eventos.
     *
     * @return array
     */
    public function getAnalyticsBasicProperties()
    {
        $city = empty(Session::get('city_id'))
            ? '' : City::find(Session::get('city_id'));

        return [
            'userId' => $this->id,
            'email' => $this->email,
            'firstName' => $this->first_name,
            'lastName' => $this->last_name,
            'phone' => $this->phone,
            'city' => empty($city) ? '' : $city->city,
            'orderCount' => Session::get('total_orders') ?: 0,
            'hasOrdered' => Session::has('last_order_date') ? 1 : 0,
            'userValidated' => empty($this->phone_validated_date) ? 0 : 1,
            'lastOrder' => Session::get('last_order_date') ? '"' . Session::get('last_order_date') . '"' : '',
        ];
    }

    /**
     * @param $referer_code Codigo de refrido
     * @param $is_new_device Es dispositivo nuevo
     * @return User
     * @throws ReferenceCodeException
     */
    public function validateReferrerCode($referer_code, $is_new_device)
    {
        $referrer = User::select('total_referrals', 'id')
            ->where('referral_code', $referer_code)
            ->where('id', '<>', $this->id)
            ->where('status', 1)
            ->first();

        if (empty($referrer)) {
            throw new ReferenceCodeException('El código no es valido.');
        }

        if (!$is_new_device) {
            throw new ReferenceCodeException('No se pudo validar el código de referido por incumplimiento de nuestros términos y condiciones.');
        }

        if (!empty($this->referred_by)) {
            throw new ReferenceCodeException('Ya agregaste un código de referido.');
        }

        $total_orders = $this->orders()->where('status', 'Delivered')->count();
        if ($total_orders > 0 || $this->free_delivery_expiration_date) {
            throw new ReferenceCodeException('No puedes agregar un código referido por que ya hiciste tu primer pedido.');
        }

        if ($referrer->total_referrals >= Config::get('app.referred.limit')) {
            throw new ReferenceCodeException(
                'El código referido que ingresaste no es válido ' .
                    'porque ha alcanzado el limite permitido de referidos por cuenta.'
            );
        }

        return $referrer;
    }

    /**
     * @param Store $store
     * @param array $user_discounts
     * @param $order_amount
     * @return bool
     */
    public function hasFreeDelivery(Store $store, array $user_discounts, $order_amount)
    {
        if ($user_discounts['free_delivery_days'] || $user_discounts['free_delivery_next_order']) {
            return true;
        }

        $discounts = Discount::where('type', 'free_delivery')
            ->where('status', 1)
            ->where(function ($query) use ($store) {
                $query->where('store_ids', 'LIKE', "%{$store->id}%")
                    ->orWhere('city_id', 'LIKE', "%{$store->city_id}%");
            })
            ->get();

        if (!$discounts->count()) {
            return false;
        }

        foreach ($discounts as $discount) {
            $has_bottom_and_upper_limits = is_numeric($discount->minimum_order_amount) && is_numeric($discount->maximum_order_amount);
            $has_bottom_limits = is_numeric($discount->minimum_order_amount) && !is_numeric($discount->maximum_order_amount);
            $has_upper_limits = !is_numeric($discount->minimum_order_amount) && is_numeric($discount->maximum_order_amount);

            if (
                $has_bottom_and_upper_limits &&
                $order_amount >= $discount->minimum_order_amount &&
                $order_amount < $discount->maximum_order_amount
            ) {
                return true;
            }

            if ($has_bottom_limits && $order_amount >= $discount->minimum_order_amount) {
                return true;
            }

            if ($has_upper_limits && $order_amount < $discount->maximum_order_amount) {
                return true;
            }
        }

        return false;
    }

    /**
     * Valida duplicidad de pedido
     *
     * @return Order
     */
    public function validateOrderDuplicate()
    {
        return Order::select('orders.id')->join('order_groups', 'group_id', '=', 'order_groups.id')
            ->where('orders.user_id', $this->id)->whereIn('status', ['Validation', 'Initiated'])
            ->whereRaw("TIMESTAMPDIFF(SECOND, orders.created_at, '" . date('Y-m-d H:i:s') . "') <= 120")
            ->where('ip', get_ip())
            ->first();
    }


    /**
     * Informacion del ultimo pedido del usuario
     */
    public function getLastOrder()
    {
        return Order::join('order_groups', 'group_id', '=', 'order_groups.id')
            ->where('orders.user_id', '=', $this->id)
            ->leftjoin('stores', 'stores.id', '=', 'orders.store_id')
            ->leftjoin('cities', 'cities.id', '=', 'order_groups.user_city_id')
            ->select('stores.slug as store_slug', 'stores.id as store_id', 'cities.id as city_id', 'cities.slug as city_slug')
            ->orderBy('orders.id', 'DESC')
            ->first();
    }

    /**
     * @param $code
     * @param User|null $user
     * @return string
     *
     * @throws MerqueoException
     */
    public static function validatePhoneNumber($code, User $user = null)
    {
        $account_kit = app(AccountKit::class);
        $phone = $account_kit->getPhoneNumber($code);

        $query = User::where('phone', $phone);

        if ($user) {
            $query->where('id', '<>', $user->id);
        }

        $phone_exists = $query->count() > 0;

        if ($phone_exists) {
            RegisterErrorLog::add(new Exception("El número de celular ya se encuentra en uso; cel: {$phone}"));
            throw new MerqueoException(
                'El número celular ya se encuentra en uso. Escríbenos a ' . Config::get('app.admin_email')
                    . ' si necesitas ayuda para la creación de tu cuenta.'
            );
        }

        if ($user) {
            $user->phone = $phone;
            $user->phone_validated_date = Carbon::create();
            $user->save();
        }

        return $phone;
    }



    public function credits()
    {
        return $this->belongsToMany(Credit::class, "user_credits")->withPivot(['amount', 'expiration_date', 'coupon_id', 'referrer_id', 'external_service_id', 'current_amount', 'claim_motive', 'text_products_related', 'status']);
    }

    /***
     * @param $userId
     * @param $userCreditCardId
     */
    public static function preChargeCreditCard($userId, $userCreditCardId, $source)
    {
        $user = User::find($userId);
        $userCreditCard = UserCreditCard::find($userCreditCardId);
        $preCharge = User::chargeCreditCard($user, $userCreditCard);
        $message = 'Tuvimos algunos inconvenientes con el método de pago elegido para tu pedido. Por favor intenta con otro medio de pago.';
        if (!$preCharge['status']) {
            if ($source) {
                send_sms($userCreditCard->user_phone, $message);
            }
            return array('status' => false, 'message' => $message);
        } else {
            $orderPayments = $preCharge['order_payment'];
            if ($source) {
                send_sms($userCreditCard->user_phone, $message);
            }

            if ($orderPayments && !empty($orderPayments->cc_charge_id)) {
                $date = Carbon::now()->addSeconds(10);
                Queue::later($date, \jobs\preChargeCreditCard::class, ['orderId' => null], 'quick');
            }
            return array('status' => true, 'message' => 'Hemos realizado un cargo de $' . Config::get('app.payu.minimum_charge') . ' pesos a tu tarjeta para garantizar la seguridad de tu pedido. Este es un cobro temporal que será devuelto.');
        }
    }

    /***
     * @param $userId
     * @param $userCreditCardId
     */
    public static function chargeCreditCard($user, $userCreditCard)
    {
        $date = $date = Carbon::now();
        $data = [];
        $data['total_amount'] = Config::get('app.payu.minimum_charge');
        $data['card_token'] = $userCreditCard->card_token;
        $data['tax_amount'] = 0; //$total * 0.16;
        $data['order_id'] = $user->id . $date->format('Ymd');
        $data['user_id'] = $user->id;
        $data['installments_cc'] = 1;
        $data['description'] = 'Merqueo - Cobro para validación de tarjetas';

        $data['total_base'] = 0;
        $data['iva'] = 0;
        $data['card_type'] = $userCreditCard->type;
        $data['security_code'] = $userCreditCard->type == 'AMEX' ? '0000' : '000';
        $data['device_session_id'] = md5(session_id() . microtime());
        $referenceCode = $user->id . $date->format('Ymd');
        $data['referenceCode'] = (string) $referenceCode;

        $orderGroup = OrderGroup::where('user_id', $user->id)->first();
        $city = !empty($orderGroup) ? City::find($orderGroup->user_city_id) : '';
        $data['buyer']['document_number'] = $user->identity_number;
        $data['buyer']['fullname'] = $user->first_name . ' ' . $user->last_name;
        $data['buyer']['email'] = $user->email;
        $data['buyer']['phone'] = $user->phone;
        $data['buyer']['address'] = $userCreditCard->holder_address;
        $data['buyer']['address_further'] = $userCreditCard->holder_address_further;
        $data['buyer']['city'] = !empty($city) ? $city->city : 'Bogotá D.C';
        $data['buyer']['state'] = !empty($city) ? $city->state : 'Cundinamarca';

        $data['payer']['document_number'] = $userCreditCard->holder_document_number;
        $data['payer']['document_type'] = $userCreditCard->holder_document_type;
        $data['payer']['fullname'] = $userCreditCard->holder_name;
        $data['payer']['email'] = $userCreditCard->holder_email;
        $data['payer']['phone'] = $userCreditCard->holder_phone;
        $data['payer']['address'] = $userCreditCard->holder_address;
        $data['payer']['address_further'] = $userCreditCard->holder_address_further;
        $data['payer']['city'] = $userCreditCard->holder_city;

        $orderPayment = new OrderPayment();
        $payu = new PayU();
        $result = $payu->createCreditCardCharge($data, true);
        if (!$result['status']) {
            $msg = isset($result['response']->transactionResponse->responseCode) ? $payu->getErrorMessage($result['response']->transactionResponse->responseCode) : 'No especificado.';
            return ['status' => false, 'message' => 'Cobro a tarjeta rechazado. Motivo: ' . $msg];
        }

        if ($result['response']->transactionResponse->state == 'APPROVED') {
            $orderPayment->cc_payment_status = 'Aprobada';
        } else if ($result['response']->transactionResponse->state == 'DECLINED') {
            $orderPayment->cc_payment_status = 'Declinada';
        } else if ($result['response']->transactionResponse->state == 'EXPIRED') {
            $orderPayment->cc_payment_status = 'Expirada';
        }

        $orderPayment->cc_payment_description = $result['response']->transactionResponse->responseCode;
        $orderPayment->cc_payment_transaction_id = $result['response']->transactionResponse->transactionId;
        $orderPayment->cc_charge_id = $result['response']->transactionResponse->orderId;
        $orderPayment->cc_payment_date = date('Y-m-d H:i:s');
        $orderPayment->admin_id = 64;
        $orderPayment->save();
        DB::commit();
        return ['status' => true, 'message' => 'El cobro fue realizado con éxito.', 'order_payment' => $orderPayment];
    }

    /**
     * @return bool
     */
    public function hasFreeDeliveryByExpirationDate()
    {
        if (!$this->free_delivery_expiration_date) {
            return false;
        }

        return Carbon::now()->lessThan($this->free_delivery_expiration_date);
    }


    /**
     * @return bool
     */
    public function hasFreeDeliveryOnNextOrder()
    {
        if (!$this->free_delivery_next_order) {
            return false;
        }

        if (!$this->free_delivery_next_order_expiration_date) {
            return true;
        }

        return $this->free_delivery_next_order_expiration_date->greaterThanOrEqualTo(Carbon::now());
    }

    /**
     * Give the number of order this items has bought.
     *
     * @return int
     */
    public function deliveredOrders()
    {

        return $this->orderGroups()
            ->whereHas('orders', function ($query) {
                $query->where('status', 'Delivered');
            })
            ->count();
    }

    /**
     * @return int
     */
    public function activeOrders()
    {
        return $this->orderGroups()
            ->whereHas('orders', function ($query) {
                $query->whereNotIn('status', ['Cancelled', 'Delivered']);
            })
            ->count();
    }
}
