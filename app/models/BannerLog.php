<?php
class BannerLog extends \Illuminate\Database\Eloquent\Model
{
	protected $table = 'banner_logs';

	public static function getFormattedLogs($banner_id)
	{
		$banner_logs = BannerLog::where('banner_id', $banner_id)->get();
		$changes = [];
		$i = 0;
		if ( !empty($banner_logs) ) {
			foreach ($banner_logs as $key => &$log) {
				if ( !empty($log->reason) ) {
					$reasons = json_decode($log->reason);
					$admin = Admin::find($log->admin_id);
					$changes[$i]['admin'] = $admin->fullname;
					$changes[$i]['created_at'] = date_format($log->created_at, 'd/m/Y h:i:s A');
					if ( isset($reasons->original->title) && isset($reasons->changes->title) ) {
						$changes[$i]['changes']['title']['original'] = $reasons->original->title;
						$changes[$i]['changes']['title']['change'] = $reasons->changes->title;
					}
					if ( isset($reasons->original->url) && isset($reasons->changes->url) ) {
						$changes[$i]['changes']['url']['original'] = $reasons->original->url;
						$changes[$i]['changes']['url']['change'] = $reasons->changes->url;
					}
					if ( isset($reasons->original->target) && isset($reasons->changes->target) ) {
						$changes[$i]['changes']['target']['original'] = $reasons->original->target;
						$changes[$i]['changes']['target']['change'] = $reasons->changes->target;
					}
					if ( isset($reasons->original->image_web_url) && isset($reasons->changes->image_web_url) ) {
						$changes[$i]['changes']['image_web_url']['original'] = $reasons->original->image_web_url;
						$changes[$i]['changes']['image_web_url']['change'] = $reasons->changes->image_web_url;
					}
					if ( isset($reasons->original->image_app_url) && isset($reasons->changes->image_app_url) ) {
						$changes[$i]['changes']['image_app_url']['original'] = $reasons->original->image_app_url;
						$changes[$i]['changes']['image_app_url']['change'] = $reasons->changes->image_app_url;
					}
					if ( isset($reasons->original->position) && isset($reasons->changes->position) ) {
						$changes[$i]['changes']['position']['original'] = $reasons->original->position;
						$changes[$i]['changes']['position']['change'] = $reasons->change->position;
					}
					if ( isset($reasons->original->city_id) && isset($reasons->changes->city_id) ) {
						$original = City::find($reasons->original->city_id);
						$changed = City::find($reasons->changes->city_id);
						$changes[$i]['changes']['city_id']['original'] = $original->city;
						$changes[$i]['changes']['city_id']['change'] = $changed->city;
					}
					if ( isset($reasons->original->store_id) && isset($reasons->changes->store_id) ) {
						$original = Store::find($reasons->original->store_id);
						$changed = Store::find($reasons->changes->store_id);
						$changes[$i]['changes']['store_id']['original'] = $original->name;
						$changes[$i]['changes']['store_id']['change'] = $changed->name;
					}
					if ( isset($reasons->original->status) && isset($reasons->changes->status) ) {
						$changes[$i]['changes']['status']['original'] = $reasons->original->status;
						$changes[$i]['changes']['status']['change'] = $reasons->changes->status;
					}
					if ( isset($reasons->original->status) && isset($reasons->changes->status) ) {
						$changes[$i]['changes']['status']['original'] = $reasons->original->status;
						$changes[$i]['changes']['status']['change'] = $reasons->changes->status;
					}
					if ( isset($reasons->original->associated_store_product_id) && isset($reasons->changes->associated_store_product_id) ) {
						$associated_product_original = Product::join('store_products', 'products.id', '=', 'store_products.product_id')
														->where('store_products.id', $reasons->original->associated_store_product_id)
														->select('products.id', 'products.name', 'store_products.id AS store_product_id')
														->first();

						$associated_product_changed = Product::join('store_products', 'products.id', '=', 'store_products.product_id')
														->where('store_products.id', $reasons->changes->associated_store_product_id)
														->select('products.id', 'products.name', 'store_products.id AS store_product_id')
														->first();

						$changes[$i]['changes']['associated_store_product_id']['original'] = $associated_product_original->id.' | '.$associated_product_original->name.' | '.$associated_product_original->store_product_id;
						$changes[$i]['changes']['associated_store_product_id']['change'] = $associated_product_changed->id.' | '.$associated_product_changed->name.' | '.$associated_product_changed->store_product_id;
					}
					if ( isset($reasons->original->deeplink_type) && isset($reasons->changes->deeplink_type) ) {
						$changes[$i]['changes']['deeplink_type']['original'] = $reasons->original->deeplink_type;
						$changes[$i]['changes']['deeplink_type']['change'] = $reasons->changes->deeplink_type;
					}
					if ( isset($reasons->original->deeplink_city_id) && isset($reasons->changes->deeplink_city_id) ) {
						$original = City::find($reasons->original->deeplink_city_id);
						$changed = City::find($reasons->changes->deeplink_city_id);
						$changes[$i]['changes']['deeplink_city_id']['original'] = $original->city;
						$changes[$i]['changes']['deeplink_city_id']['change'] = $changed->city;
					}
					if ( isset($reasons->original->deeplink_store_id) && isset($reasons->changes->deeplink_store_id) ) {
						$original = Store::find($reasons->original->deeplink_store_id);
						$changed = Store::find($reasons->changes->deeplink_store_id);
						$changes[$i]['changes']['deeplink_store_id']['original'] = $original->name;
						$changes[$i]['changes']['deeplink_store_id']['change'] = $changed->name;
					}
					if ( isset($reasons->original->deeplink_department_id) && isset($reasons->changes->deeplink_department_id) ) {
						$original = Department::find($reasons->original->deeplink_department_id);
						$changed = Department::find($reasons->changes->deeplink_department_id);
						$changes[$i]['changes']['deeplink_department_id']['original'] = $original->name;
						$changes[$i]['changes']['deeplink_department_id']['change'] = $changed->name;
					}
					if ( isset($reasons->original->deeplink_shelf_id) && isset($reasons->changes->deeplink_shelf_id) ) {
						$original = Shelf::find($reasons->original->deeplink_shelf_id);
						$changed = Shelf::find($reasons->changes->deeplink_shelf_id);
						$changes[$i]['changes']['deeplink_shelf_id']['original'] = $original->name;
						$changes[$i]['changes']['deeplink_shelf_id']['change'] = $changed->name;
					}
					if ( isset($reasons->original->deeplink_store_product_id) && isset($reasons->changes->deeplink_store_product_id) ) {
						$associated_product_original = Product::join('store_products', 'products.id', '=', 'store_products.product_id')
														->where('store_products.id', $reasons->original->deeplink_store_product_id)
														->select('products.id', 'products.name', 'store_products.id AS store_product_id')
														->first();

						$associated_product_changed = Product::join('store_products', 'products.id', '=', 'store_products.product_id')
														->where('store_products.id', $reasons->changes->deeplink_store_product_id)
														->select('products.id', 'products.name', 'store_products.id AS store_product_id')
														->first();

						$changes[$i]['changes']['deeplink_store_product_id']['original'] = $associated_product_original->id.' | '.$associated_product_original->name.' | '.$associated_product_original->store_product_id;
						$changes[$i]['changes']['deeplink_store_product_id']['change'] = $associated_product_changed->id.' | '.$associated_product_changed->name.' | '.$associated_product_changed->store_product_id;
					}
					$i++;
				}
			}
			return $changes;
		}
	}
}
