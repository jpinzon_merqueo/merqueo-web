<?php

class Configuration extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'config';

    protected $fillable = ['city_id', 'key', 'value', 'type', 'warehouse_id'];
}