<?php

/**
 * Class CustomerService
 * @property mixed $description
 */
class CustomerService extends \Illuminate\Database\Eloquent\Model
{
    const TICKET = 'Tickets';
    const PQR = 'PQR';
    const MAIL_TICKET = 'PQR';

    protected $table = 'customer_services';
    protected $fillable = ['description', 'priority', 'color'];
    protected $casts = ['id' => 'int'];
}
