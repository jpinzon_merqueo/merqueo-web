<?php

namespace models;

use Illuminate\Database\Eloquent\Model;

class CmsHistory extends Model
{
    protected $table = 'cms_history';

    public function document()
    {
        return $this->belongsTo('models\CmsDocument');
    }

    public function admin()
    {
        return $this->belongsTo('models\Admin');
    }

    public static function build($cmsDocumentId, $adminId)
    {
        $cmsHistory = new CmsHistory();
        $cmsHistory->cms_document_id = $cmsDocumentId;
        $cmsHistory->admin_id = $adminId;
        return $cmsHistory;
    }
}
