<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 15/08/2018
 * Time: 12:48
 */

class OrderRejectReasonLog extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'orden_reject_reason_logs';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function orders(){
        return $this->belongsTo('orders');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function rejectReasons(){
        return $this->belongsTo('reject_reasons');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function admin(){
        return $this->belongsTo('admin');
    }
}