<?php

use Illuminate\Database\Eloquent\Relations\Pivot;

/**
 * Es necesaria una clase pivot para poder obtener los
 * objetos relacionados por medio del query "Builder".
 *
 * @property Department $department
 * @property Shelf $shelf
 * @property float $price
 * @property float quantity_special_price
 */
class StoreProductPivot extends Pivot
{
    
    protected $fillable = [
        'id', 'store_id', 'product_id', 'price',
        'special_price', 'quantity_special_price',
        'shelf_id', 'department_id', 'current_stock',
    ];

    protected $casts = [
        'price' => 'float',
        'special_price' => 'float',
        'current_stock' => 'int'
    ];

    /**
     * Relación con los departamentos.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function department()
    {
        return $this->belongsTo(Department::class);
    }


    /**
     * Relación con los estantes.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function shelf()
    {
        return $this->belongsTo(Shelf::class);
    }

    /**
     * Description
     * @return type
     */
    public function getDiscountAttribute()
    {
        $is_product_on_sale = !(empty($product->pivot->special_price) || empty($product->pivot->price));
        if ($is_product_on_sale) {
            return 1 - $this->special_price / $this->price;
        }

        return 0;
    }
}
