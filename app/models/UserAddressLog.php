<?php

class UserAddressLog extends \Illuminate\Database\Eloquent\Model {

    const ZONE_ISSUE = 'no_coverage_zone';

    protected $table = 'user_address_log';

    /**
     * @param $latitude
     * @param $longitude
     * @param string $type
     * @param User|null $user
     */
    public static function make($latitude, $longitude, $type = 'no_coverage', User $user = null)
    {
        $log = new UserAddressLog();
        $log->latitude = $latitude;
        $log->longitude = $longitude;
        $log->type = $type;
        if (!empty($user)) {
            $log->user_id = $user->id;
        }
        $log->save();
    }
}
