<?php

/**
 * @property string $code
 * @property string $amount
 * @property string $type_use
 * @property string $id
 * @property int $number_uses
 * @property bool $campaign_validation
 * @property string $payment_method
 * @property int|null $minimum_order_amount
 * @property int|null $maximum_order_amount
 * @property int $department_id
 * @property int $shelf_id
 * @property int $store_product_id
 * @property int $store_id
 * @property string $type
 * @property string $status
 * @property string $expiration_date
 * @property string $redeem_on
 */
class Coupon extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'coupons';

    public function userCredit()
    {
        return $this->hasMany('UserCredit', 'coupon_id');
    }

    /**
     * Toma el valor de un cupón, lo redime y descuenta el monto al pedido
     *
     * @param Order $order
     * @throws \exceptions\CouponException
     */
    public function addToOrder(Order $order)
    {
        $validator = new \coupon\CouponValidator($this);
        $validator->validateAgainstOrder($order);
        $amount = $this->amount;
        if ($this->type === 'Discount Percentage') {
            $order->discount_percentage_amount = $amount;
            $amount = $order->total_amount * $this->amount / 100;
        }

        $order->discount_amount += $amount;
        $order->save();

        $this->loadAsCredit($order->user, $amount);
        UserCredit::removeCredit($order->user, $amount, 'order', $order, $this);
    }

    /**
     *  Cargar credito al cliente y marcarlo como utilizado
     *
     * @param User $user
     * @param $amount
     * @param $expiration_date
     */
    public function loadAsCredit(User $user, $amount, $expiration_date = null)
    {
        $user_coupon = new UserCoupon();
        $user_coupon->coupon_id = $this->id;
        $user_coupon->user_id = $user->id;
        $user_coupon->save();

        $expiration_date = $expiration_date ?: date('Y-m-d H:i:s', strtotime('+1 day'));
        UserCredit::addCredit($user, $amount, $expiration_date, 'coupon', $this);

        if (!$user->first_coupon_used && $this->type_use === 'Only The First Order') {
            $user->first_coupon_used = 1;
            $user->save();
        }
    }

    /**
     * Valida redencion de cupon en pedido
     *
     * @param Order $order
     * @return Order
     */
    public function validateOnOrder(Order $order)
    {
        $this->validateTotalAmounts($order);

        $discount_amount = 0;
        //validar cupon para tienda / categoria / subcategoria / producto especifico
        if ($this->redeem_on == 'Specific Store')
        {
            $order_products = $order->orderProducts()->whereIn('fulfilment_status', ['Fullfilled', 'Pending'])->get();
            foreach($order_products as $order_product){
                $store_product = $order_product->storeProduct;
                $is_valid = false;
                if ($store_product->store_id == $this->store_id){
                    if ($this->department_id){
                        if ($store_product->department_id == $this->department_id){
                            if ($this->shelf_id){
                                if ($store_product->shelf_id == $this->shelf_id){
                                    if ($this->store_product_id){
                                        if ($store_product->id == $this->store_product_id)
                                            $is_valid = true;
                                    }else $is_valid = true;
                                }
                            }else $is_valid = true;
                        }
                    }else $is_valid = true;
                }
                if ($is_valid){
                    if ($this->type == 'Discount Percentage')
                        $discount_amount += round(($order_product->price * $order_product->quantity) * ($this->amount / 100), 0);
                    if ($this->type == 'Credit Amount')
                        $discount_amount += round($order_product->price * $order_product->quantity, 0);
                }
            }
            $coupon = $this;
            $store = Store::select('stores.name AS store_name', 'departments.name AS department_name', 'shelves.name AS shelf_name',
                DB::raw("CONCAT(products.name, ' ', products.quantity, ' ', products.unit) AS product_name"), 'cities.city')
                ->leftJoin('departments', function($join) use ($coupon){
                    $join->on('departments.store_id', '=', 'stores.id');
                    $join->on('departments.id', '=', DB::raw($coupon->department_id ? $coupon->department_id : 0));
                })
                ->leftJoin('shelves', function($join) use ($coupon){
                    $join->on('shelves.department_id', '=', 'departments.id');
                    $join->on('shelves.id', '=', DB::raw($coupon->shelf_id ? $coupon->shelf_id : 0));
                })
                ->leftJoin('store_products', function ($join) use ($coupon) {
                    $join->on('store_products.shelf_id', '=', 'shelves.id');
                    $join->on('store_products.id', '=', DB::raw($coupon->store_product_id ? $coupon->store_product_id : 0));
                })
                ->leftJoin('products', 'store_products.product_id', '=', 'products.id')
                ->join('cities', 'cities.id', '=', 'stores.city_id')
                ->where('stores.id', $coupon->store_id)
                ->first();
            $discount_message = '';
            if (!empty($store->department_name) && empty($store->shelf_name) && empty($store->product_name))
                $discount_message = $store->department_name;
            if (!empty($store->shelf_name) && empty($store->product_name))
                $discount_message = $store->shelf_name;
            if (!empty($store->product_name))
                $discount_message = $store->product_name;
            $discount_message .= ' en '.$store->city;

            if ($discount_amount){
                $order->discount_amount = $discount_amount;
                if ($this->type == 'Discount Percentage')
                    $order->discount_percentage_amount = $this->amount;
                else if ($discount_amount > $this->amount)
                    $order->discount_amount = $this->amount;
            }else{
                return $this->errorReedemOnOrder($order, 'El cupón de descuento '.$this->code.' aplica exclusivamente para '.$discount_message);
            }
        }
        //validar metodo de pago y tipo de tarjeta
        if ($this->payment_method){
            $config['payment_methods_names'] = OrderPayment::getPaymentMethods();
            if (($order->credit_card_id && $this->payment_method != 'Tarjeta de crédito' && $this->payment_method != $order->creditCard->type)
                || (!$order->credit_card_id && $this->payment_method != $order->payment_method)){
                return $this->errorReedemOnOrder($order, 'Para redimir el cupón de descuento '.$this->code.' el metodo de pago debe ser con '.$config['payment_methods_names'][$this->payment_method].trim(' '.$this->cc_bank));
            }else{
                if ($order->credit_card_id && !empty($this->cc_bin)){
                    if (empty($payment)) $payment = new \Payment();
                    $result = $payment->retriveCreditCard($order->user->customer_token, $order->creditCard->card_token, $order->user->id);
                    if ($result['status']){
                        $bin = $result['response']->bin;
                        $allowed_bines = explode(',', $this->cc_bin);
                        if (!in_array($bin, $allowed_bines)){
                            return $this->errorReedemOnOrder($order, 'Para redimir el cupón de descuento '.$this->code.' el metodo de pago debe ser con '.$config['payment_methods_names'][$this->payment_method].' '.$this->cc_bank);
                        }
                    }else{
                        return $this->errorReedemOnOrder($order, 'No pudimos obtener información de tu tarjeta de crédito para la validación del cupón, por favor verificala e intenta de nuevo.');
                    }
                }
            }
        }

        //si el cupon no es de productos especificos se aplica el cupon
        if ($this->redeem_on != 'Specific Store'){
            if ($this->type == 'Credit Amount'){
                if ($this->amount > $order->total_amount)
                    $this->amount = $order->total_amount;
                $order->discount_amount = $this->amount;
            }
            if ($this->type == 'Discount Percentage') {
                $order->discount_percentage_amount = $this->amount;
                $order->discount_amount = round($order->total_amount * ($this->amount / 100), 0);
            }
        }

        return $order;
    }




    /**
     * Verifica si el cupon cumple con los totales requeridos
     *
     * @param Order $order
     * @param $returnErrStr
     * @return Order|string
     */
    public function validateTotalAmounts(Order $order, $returnErrStr = false)
    {
        $hasErrors = false;
        $formatStr = "Para redimir el cupón de descuento '%s' el total del pedido debe %s $%s %s";
        //si el cupon cumple con totales requerido
        if ($this->minimum_order_amount && !$this->maximum_order_amount && $order->total_amount < $this->minimum_order_amount){
            $hasErrors = true;
            $strErr = sprintf($formatStr, $this->code, 'ser mayor o igual a', number_format($this->minimum_order_amount, 0, ',', '.'), '');
        }else if ($this->maximum_order_amount && !$this->minimum_order_amount && $order->total_amount > $this->maximum_order_amount){
            $hasErrors = true;
            $strErr = sprintf($formatStr, $this->code, 'ser menor o igual a', number_format($this->minimum_order_amount, 0, ',', '.'), '');
        }else if ($this->minimum_order_amount && $this->maximum_order_amount && ($order->total_amount < $this->minimum_order_amount || $order->total_amount > $this->maximum_order_amount)){
            $hasErrors = true;
            $strExt = sprintf('y $%s', number_format($this->maximum_order_amount, 0, ',', '.') );
            $strErr = sprintf($formatStr, $this->code, 'estar entre', number_format($this->minimum_order_amount, 0, ',', '.'), $strExt);
        }

        if ($hasErrors){
            return $returnErrStr ? $strErr : $this->errorReedemOnOrder($order, $strErr);
        }
    }
    /**
     * Setea pedido sin descuento de cupon por ni cumplir condiciones
     *
     * @param Order $order
     * @param $error
     * @return Order
     */
    private function errorReedemOnOrder(Order $order, $error)
    {
        $order->discount_amount = null;
        $order->discount_percentage_amount = null;

        return $order;
    }
}
