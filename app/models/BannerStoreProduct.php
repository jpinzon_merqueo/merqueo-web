<?php

use \Illuminate\Database\Eloquent\Model;

/**
 * Class BannerStoreProduct
 */
class BannerStoreProduct extends Model
{
    protected $table = 'banner_store_product';

    /**
     * Relación con "storeProduct"
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
	public function storeProduct()
    {
        return $this->belongsTo(StoreProduct::class);
    }
}
