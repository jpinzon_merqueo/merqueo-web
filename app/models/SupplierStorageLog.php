<?php

class SupplierStorageLog extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'supplier_storage_log';

    public static function log($provider_order_reception_id, $store_product_warehouse_id, $quantity, $storaged_quantity, $storage, $position, $position_height){
    	$supplier_storage_log = SupplierStorageLog::findOrNew(0);

    	$supplier_storage_log->admin_id = Session::get('admin_id');
    	$supplier_storage_log->provider_order_reception_id = $provider_order_reception_id;
    	$supplier_storage_log->store_product_warehouse_id = $store_product_warehouse_id;
    	$supplier_storage_log->quantity = $quantity;
    	$supplier_storage_log->storaged_quantity = $storaged_quantity;
    	$supplier_storage_log->storage = $storage;
    	$supplier_storage_log->position = $position;
    	$supplier_storage_log->position_height = $position_height;
    	$supplier_storage_log->storage_date = date('Y-m-d H:i:s');
    	$supplier_storage_log->save();
    }

}