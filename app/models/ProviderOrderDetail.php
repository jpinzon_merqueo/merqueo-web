<?php

class ProviderOrderDetail extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'provider_order_details';
    protected $hidden = array('created_at','updated_at');

    public function storeProduct()
    {
    	return $this->belongsTo('StoreProduct', 'store_product_id');
    }

    public function providerOrderDetailProductGroup()
    {
        return $this->hasMany('ProviderOrderDetailProductGroup', 'provider_order_detail_id');
    }

    public function providerOrder()
    {
        return $this->belongsTo('ProviderOrder', 'provider_order_id');
    }
}