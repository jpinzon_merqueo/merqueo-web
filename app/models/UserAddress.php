<?php

/**
 * @property string $latitude
 * @property string $longitude
 */
class UserAddress extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'user_address';

    /**
     * Validar si sale la direccion en el sur
     *
     * @param string $result Datos de primera direccion encontrada
     * @param array $post_data Datos de direccion
     * @return array $response Posibles direcciones
     */
    static function validateAddress($result, $request_data)
    {
        $response = ['status' => false];

        if (!isset($request_data['app_version']))
            return $response;

        $app_version = intval(str_replace('.', '', $request_data['app_version']));
        if ($app_version > 212)
        {
            $address_group[] = [
                'latitude' => strval($result['result']['latitude']),
                'longitude' => strval($result['result']['longitude']),
                'address' => $request_data['address'],
                'address_html' => $request_data['address']
            ];
            if (isset($request_data['address_structure']))
                $address_group[0]['address_structure'] = json_encode(json_decode($request_data['address_structure']));
            $message ='Dirección encontrada.';

            //validar direccion en el sur
            if (!strpos(strtolower($request_data['address']), 'sur'))
            {
                $address_south = '';
                $address = explode(' ', $request_data['address']);
                foreach ($address as $key => $value)
                {
                    if($key == 1) $address_south .= $value.' SUR ';
                    else $address_south .= $value.' ';
                }
                if (isset($request_data['address_structure']))
                {
                    if ($address_structure = json_decode($request_data['address_structure'])) {
                        $address_structure->add2 = $address_structure->add2 . ' SUR ';
                        $address_structure = json_encode($address_structure);
                    }
                }

                $inputs = array(
                    'address' => $address_south,
                    'city' => $request_data['city'],
                    'use_google' => false
                );
                $request = Request::create('/api/location', 'GET', $inputs);
                Request::replace($request->input());
                $result = json_decode( Route::dispatch($request)->getContent(), true);
                if ($result['status']){
                    //validar que no sea la misma direccion
                    if ($address_group[0]['latitude'] != $result['result']['latitude']){
                        $address_html = str_replace('SUR', '<font color="red"><b>SUR</b></font>', $address_south);
                        $address_group[] = [
                            'latitude' => strval($result['result']['latitude']),
                            'longitude' => strval($result['result']['longitude']),
                            'address' => $address_south,
                            'address_html' => $address_html
                        ];
                        if (isset($address_structure) && $address_structure)
                            $address_group[1]['address_structure'] = $address_structure;
                        $message ='Encontramos tu dirección en dos zonas diferentes, selecciona la correcta';
                    }
                }
            }
            $response = array(
                'status' => true,
                'message' => $message,
                'result' => ['addresses' => $address_group]
            );
        }

        return $response;
    }

    /**
     * Validacion de direccion con expresión regular
     */
    static function isCorrectAddress($address)
    {

        $patron = '/(?<address>\D+)(?<number> +[\d]+[\w* *]*) (?<hash>#|No) (?<Homenumber> *[\d *]+[\w]* *- *[\d]+[\w]*)(?<other>.*)/';
        preg_match($patron, $address, $matches);

        if(!empty($matches['address']) && !empty($matches['number']) && !empty($matches['hash']) && !empty($matches['Homenumber']) ){
            return true;
        }else{
            $user_address_log = new UserAddressLog;
            $user_address_log->address = $address;
            $user_address_log->type = 'invalid_address';
            $user_address_log->save();
            return false;
        }
  
    }

}