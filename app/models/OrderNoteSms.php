<?php

class OrderNoteSms extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'order_notes_sms';
    protected $hidden = array('created_at', 'updated_at');

}