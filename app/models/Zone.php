<?php

/**
 * Class Zone
 * @property int $warehouse_id
 * @property Warehouse $warehouse
 * @property int $id
 * @property Zone[]|\Illuminate\Database\Eloquent\Collection $slotZones
 * @property DeliveryWindow $deliveryWindow
 * @property ZoneLocation[] $zoneLocations
 * @property string $delivery_zone
 */
class Zone extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'zones';

    const EXPRESS_ZONE = 179;

    const STATUS_ACTIVE = 1;

    /**
     * @type string
     */
    const ZONE_TYPE_DARKSUPERMARKET = 'Darksupermarket';

    public function shopper()
    {
    	return $this->belongsTo('ShopperZone');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function warehouse()
    {
        return $this->belongsTo('Warehouse', 'warehouse_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function zoneLocations()
    {
        return $this->hasMany('ZoneLocation');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orderGroups()
    {
        return $this->hasMany('OrderGroup');
    }

    /**
     * Obtener zona con base a coordenada
     *
     * @param int $city_id ID ciudad
     * @param string $latitude Latitud
     * @param string $longitude Longitud
     * @param bool $zones_express Zonas Express
     * @return Zone
     * @throws \exceptions\CoverageException
     */
    public static function getByLatLng($city_id, $latitude, $longitude, $zones_express = false)
    {
        $zones = Zone::select('zones.*')
            ->join('warehouses', 'warehouses.id', '=', 'warehouse_id')
            ->where('warehouses.city_id', $city_id)
            ->where('zones.status', 1)
            ->whereNotIn('warehouses.id', Warehouse::WAREHOUSE_DARKSUPERMARKET_ID)
            ->get();

        if( (Auth::check() && in_array(Auth::user()->type, User::DARKSUPERMARKET_TYPE)) || $zones_express){
            $zones = Zone::select('zones.*')
                ->join('warehouses', 'warehouses.id', '=', 'warehouse_id')
                ->where('warehouses.city_id', $city_id)
                ->whereIn('warehouses.id', Warehouse::WAREHOUSE_DARKSUPERMARKET_ID)
                ->where('zones.status', 1)
                ->get();
        }

        foreach ($zones as $zone) {
            if (point_in_polygon($zone->delivery_zone, $latitude, $longitude)) {
                return $zone;
            }
        }

        throw new \exceptions\CoverageException($latitude, $longitude);
    }

    public static function getActiveById($id){
        return Zone::where('id', $id)
            ->where('status', 1)
            ->first();
    }

    /**
     * Valida que una tienda tenga cobertura en una direccion usando la latitud y longitud
     *
     * @param \Carbon\Carbon $delivery_date
     * @param DeliveryWindow|null $delivery_window
     * @param string|float $latitude
     * @param string|float $longitude
     * @return void
     * @throws \exceptions\MerqueoException
     */
    public function validateSlotConstrains(
        \Carbon\Carbon $delivery_date,
        DeliveryWindow $delivery_window,
        $latitude,
        $longitude
    ) {
        $current_date = \Carbon\Carbon::create();
        $today = $current_date->hour == Store::SAME_DAY_HOUR_23 ? $current_date->addDay() : \Carbon\Carbon::today();
        $day_of_week = $today->toDateString() === $delivery_date->toDateString() && (in_array($delivery_window->shifts, DeliveryWindow::SHIFTS_ONTIME))
            ? SlotWarehouse::SAME_DAY : $delivery_date->dayOfWeek;
        $error_message = 'La entrega en la dirección y el horario que seleccionaste no esta disponible en este ' .
            'momento, por favor selecciona otro día o hora de entrega.';

        $slot = $this->warehouse->slotWarehouse()
            ->where('day', $day_of_week)
            ->whereHas('deliveryWindow', function ($query) use ($delivery_window) {
                $query->where('id', $delivery_window->id);
            })
            ->with(['deliveryWindow.zoneLocationSlots' => function ($query) use ($day_of_week) {
                $query->where('day', $day_of_week)
                    ->where('status', 1)
                    ->whereHas('zoneLocation', function ($query) {
                        $query->where('status', 1);
                    })
                    ->with('zoneLocation');
            }])
            ->first();


        if (empty($slot)) {
            throw new \exceptions\MerqueoException($error_message);
        }

        foreach ($slot->deliveryWindow->zoneLocationSlots as $zone_location_slot) {
            if (point_in_polygon($zone_location_slot->zoneLocation->delivery_zone, $latitude, $longitude)) {
                throw new \exceptions\MerqueoException($error_message);
            }
        }
    }

    /**
     * Obtiene numero de maximo de pedidos por horario y ruta
     *
     * @param $delivery_window_id
     * @return int
     */
    public function getOrdersPerRoute($delivery_window_id)
    {
        $order_per_route = ZoneDeliveryWindow::where('zone_id', $this->id)
            ->where('delivery_window_id', $delivery_window_id)
            ->first();

        return $order_per_route
            ? $order_per_route->maximum_orders_per_route
            : 0;
    }

    /**
     * @return bool
     */
    public function canShowSameDay()
    {
        $current_date = \Carbon\Carbon::now();
        if (Auth::check() && in_array(Auth::user()->type, User::DARKSUPERMARKET_TYPE)) {
            return !empty($this->delivery_on_same_day);
        }

        return !empty($this->delivery_on_same_day) && ($current_date->hour < Store::SAME_DAY_LIMIT_HOUR || $current_date->hour == Store::SAME_DAY_HOUR_23);
    }
}
