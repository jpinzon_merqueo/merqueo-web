<?php

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;


/**
 * @property Store $store
 * @property Department $department
 * @property Shelf $shelf
 * @property string $name
 * @property string $type
 * @property string $description
 * @property mixed image_app_url
 * @property mixed image_large_url
 * @property mixed image_medium_url
 * @property mixed image_small_url
 * @property mixed nutrition_facts
 * @property mixed quantity
 * @property mixed slug
 * @property mixed unit
 */
class Product extends \Illuminate\Database\Eloquent\Model
{
    /**
     * Posibles estados de los productos.
     */
    const ACTIVE = 1;
    const INACTIVE = 0;

    protected $table = 'products';
    protected $hidden = array('created_at', 'updated_at');

    public function storeProduct()
    {
        return $this->hasMany('StoreProduct', 'product_id');
    }

    public function department()
    {
        return $this->belongsTo('Department');
    }

    public function shelf()
    {
        return $this->belongsTo('Shelf');
    }

    public function category()
    {
        return $this->belongsTo('Category');
    }

    public function subCategory()
    {
        return $this->belongsTo('Subcategory', 'subcategory_id');
    }

    public function subBrand()
    {
        return $this->belongsTo('Subbrand', 'subbrand_id');
    }

    public function maker()
    {
        return $this->belongsTo('Maker');
    }

    /**
     * Relación con las marcas
     * @return Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    /**
     * {@inheritdoc}
     */
    public function newPivot(Model $parent, array $attributes, $table, $exists)
    {
        if ($parent instanceof Store) {
            return new StoreProductPivot($parent, $attributes, $table, $exists);
        }

        return parent::newPivot($parent, $attributes, $table, $exists);
    }

    /**
     * Actualizar costo promedio del producto
     */
    public function updateCostAverage()
    {
        $product = ProviderOrderDetail::join('provider_orders', 'provider_order_details.provider_order_id', '=', 'provider_orders.id')
            ->join('provider_order_receptions', 'provider_order_receptions.provider_order_id', '=', 'provider_orders.id')
            ->join('provider_order_reception_details', function ($join) {
                $join->on('provider_order_reception_details.reception_id', '=', 'provider_order_receptions.id');
                $join->on('provider_order_details.product_id', '=', 'provider_order_reception_details.product_id');
            })
            ->whereIn('provider_order_reception_details.status', ['Recibido', 'Parcialmente recibido'])
            ->where('provider_order_reception_details.product_id', $this->id)
            ->groupBy('provider_order_reception_details.product_id')
            ->select(DB::raw('ROUND(AVG(cost)) AS average_cost'))
            ->first();
        if ($product) {
            $this->cost_average = $product->average_cost;
            $this->save();
        }
    }

    /**
     * Funcion para obtener el slug del producto compuesto por (nombre, cantidad presentación, unidad de medida)
     * @return [type] [description]
     */
    public function getSlug()
    {
        if (!empty($this->name) && !empty($this->quantity) && !empty($this->unit)) {
            $this->slug = Str::slug(trim(utf8_encode($this->name . ' ' . $this->quantity . ' ' . $this->unit)));
            return $this->slug;
        }
        return false;
    }

    /**
     * Funcion estatica para crear slugs de productos
     * @param  [type] $name     [description]
     * @param  [type] $quantity [description]
     * @param  [type] $unit     [description]
     * @return [type]           [description]
     */
    static function createSlug($name, $quantity, $unit)
    {
        return Str::slug(trim($name . ' ' . $quantity . ' ' . $unit));
    }

    /**
     * Modificacion al guardar el evento
     */
    public static function boot()
    {
        parent::boot();

        static::updated(function ($product) {

            if (Config::get('app.aws.elasticsearch.is_enable')) {
                $aws_elasticsearch = new AWSElasticsearch();
                $aws_elasticsearch->update_products(['product_id' => $product->id]);
            }

            if (Request::server('REQUEST_URI') == '/admin/products/save' || Request::server('REQUEST_URI') == '/admin/products/import') {
                if ($product->isDirty()) {
                    $dirty_product = $product->getDirty();
                    $original_product = $product->getOriginal();
                    $original = $dirty = [];
                    foreach ($dirty_product as $att => $value) {
                        $dirty[$att] = $value;
                        $original[$att] = $original_product[$att];
                    }
                    $changes = ['original' => $original, 'changes' => $dirty, 'url' => get_current_request()];
                    $product_log = new ProductLog;
                    if (Session::has('admin_id'))
                        $product_log->admin_id = Session::get('admin_id');
                    $product_log->product_id = $product->id;
                    $product_log->reason = json_encode($changes);
                    $product_log->save();
                }
            }
        });
    }

    /** Agregar una condición a la consulta; los
     * productos deben estar activos
     * @param type $query
     * @return type
     */
    public function scopeActive($query)
    {
        return $query->where('products.status', self::ACTIVE);
    }

    /**
     * @param $query
     * @param $test_name
     * @return mixed
     */
    public function scopeAbTest($query, $test_name)
    {
        $table_name = str_replace('-', '_', snake_case($test_name));
        return $query->leftJoin(\DB::raw("ab_testings as $table_name"), function ($join) use ($table_name, $test_name) {

            $join->on("$table_name.identifier", '=', 'products.id')
                ->where("$table_name.name", '=', $test_name)
                ->where("$table_name.table_name", '=', 'products');
        });
    }

    /**
     * @param $test_name
     * @return mixed
     */
    public static function abTestingName($test_name)
    {
        $table_name = str_replace('-', '_', snake_case($test_name));
        return \DB::raw("{$table_name}.additional_params as {$table_name}_additional_params");
    }

    /**
     * Retorna una descripción de los productos
     * valida para "facebook feed".
     * @return type
     */
    public function getDescriptionCapAttribute()
    {
        return ucfirst(strtolower(strip_tags($this->description)));
    }

    /**
     * Listado de productos con los datos necesarios para elasticsearch.
     *
     * @return Product[]
     */
    public static function elasticSearchProducts()
    {
        return DB::select(
            DB::raw('
                SELECT
                    store_products.id AS id,
                    stores.city_id,
                    stores.slug AS store_slug,
                    cities.slug AS city_slug,
                    stores.id AS store_id,
                    departments.id AS department_id,
                    departments.slug AS department_slug,
                    shelves.id AS shelf_id,
                    shelves.slug AS shelf_slug,
                    products.name,
                    products.slug AS product_slug,
                    quantity,
                    unit,
                    price,
                    CASE WHEN quantity_special_price > 0 THEN
                        special_price
                    ELSE
                        NULL
                    END AS special_price,
                    quantity_special_price,
                    CAST(first_order_special_price AS SIGNED INTEGER)
                        AS first_order_special_price,
                    departments.name AS department,
                    shelves.name AS shelf,
                    brands.name AS brand,
                    products.description,
                    nutrition_facts,
                    shelves.has_warning AS warning,
                    CONCAT("/domicilios-", stores.slug, "/", departments.slug, "/", shelves.slug, "/", products.slug)
                        AS all_product_slug,
                    products.slug,
                    image_small_url,
                    image_medium_url,
                    image_large_url,
                    image_app_url,
                    store_products.delivery_discount_amount,
                    store_products.is_best_price,
                    store_products.status
                FROM
                    products, store_products, brands, shelves, departments, stores, cities
                WHERE
                    products.id = store_products.product_id AND store_products.store_id = stores.id
                    AND shelves.department_id = departments.id
                    AND store_products.shelf_id = shelves.id AND products.brand_id = brands.id
                    AND stores.city_id = cities.id AND stores.status = 1
                    AND products.status = 1 AND departments.status = 1 AND shelves.status = 1
                ORDER BY departments.id, shelves.id, products.name
            ')
        );
    }

    /**
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function images()
    {
        return $this->hasMany(\ProductImage::class);
    }

    /**
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function storeProducts()
    {
        return $this->hasMany(\StoreProduct::class);
    }

    /**
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function details()
    {
        return $this->hasMany(ProductDetail::class);
    }

    /**
     * @return string
     */
    public function getFullNameAttribute()
    {
        return "$this->name $this->quantity $this->unit";
    }

    /**
     * Agrega la imagen principal al listado de imagenes
     * del producto en caso de que cuente con registros en product_images.
     *
     * @return bool
     */
    public function populatePrimaryImage()
    {
        if (!count($this->images)) {
            return false;
        }
        $product_image = new \ProductImage();
        $product_image->product_id = $this->id;
        $product_image->image_app_url = $this->image_app_url;
        $product_image->image_large_url = $this->image_large_url;
        $product_image->image_medium_url = $this->image_medium_url;
        $product_image->image_small_url = $this->image_small_url;

        $this->images->prepend($product_image);

        return true;
    }
    /**
     * obtiene los productos en stock de almacenamiento
     * @param int $warehouseId id de la bodega
     * @param int $cityId id de la ciudad
     * @param string $search palabra a buscar que puede ser nombre, plu o referencia del producto
     * @param bool $searchCreate true si la búsqueda es para crear un nuevo producto
     * return products
     */
    public function getProductStockStorage($warehouseId = null, $cityId = null, $search = null, $searchCreate = false){
        $select = ['products.*', 'store_products.*',
            'store_products.id AS store_product_id',
            'departments.name AS department',
            'products.quantity AS product_quantity',
            'warehouses.warehouse',
            'cities.city'       
        ];
        $products = $this::join('store_products', 'store_products.product_id', '=', 'products.id')
	                    ->join('departments', 'departments.id', '=', 'store_products.department_id')
	                    ->join('shelves', 'shelves.id', '=', 'store_products.shelf_id');   
                    if($searchCreate){
                        array_push($select,'store_product_warehouses.*');
                        $products = $products   -> join('store_product_warehouses', 'store_product_warehouses.store_product_id', '=', 'store_products.id')
                                                -> join('warehouses', 'warehouses.id', '=', 'store_product_warehouses.warehouse_id')
                                                -> leftJoin('providers', 'providers.id', '=', 'store_products.provider_id')
                                                -> join('cities', 'cities.id', '=', 'warehouses.city_id');
                                            }
                    else{
                        array_push($select, 'product_stock_updates.quantity');
                        array_push($select, 'product_stock_updates.quantity_stock_before');
                        array_push($select, 'product_stock_updates.quantity_stock_after');
                        array_push($select, 'fullname');
                        array_push($select, 'admin.fullname AS admin_fullname');
                        array_push($select, 'product_stock_updates.type AS movement_type');
                        array_push($select, DB::RAW('DATE_FORMAT(product_stock_updates.created_at, "%d/%m/%Y %H:%i:%s %p") AS date'));

                        array_push($select,DB::raw('providers.name AS provider_name'));
                        $products = $products    ->join('product_stock_updates', 'product_stock_updates.store_product_id', '=', 'store_products.id')
                                                -> join('stores', 'stores.id', '=', 'store_products.store_id')
                                                -> join('warehouses', 'product_stock_updates.warehouse_id', '=', 'warehouses.id')
                                                -> join('providers', 'providers.id', '=', 'store_products.provider_id')
                                                -> join('cities', 'cities.id', '=', 'stores.city_id')
                                                -> join('admin', 'admin.id', '=', 'product_stock_updates.admin_id');
                    }
                    $products->select($select);
                    if(!is_null($warehouseId) && !is_null($cityId) &&!is_null($search)){
                        if($searchCreate){
                            $products = $products->where('store_product_warehouses.warehouse_id', $warehouseId)
                                            ->where('store_product_warehouses.status', 1)
                                            ->where('products.type', 'Simple')
                                            ->where(function($query) use ($search){
                                                $query->where('store_products.provider_plu', 'LIKE', '%'.$search.'%');
                                                $query->orWhere('products.reference', 'LIKE', '%'.$search.'%');
                                                $query->orWhere('products.name', 'LIKE', '%'.$search.'%');
                                            })
                                            ->where('cities.id', $cityId);
                        } 
                        else{
                            $products = $products->where('product_stock_updates.warehouse_id', $warehouseId)
                                            ->where(function($query) use ($search){
                                                $query->where('store_products.provider_plu', 'LIKE', '%'.$search.'%');
                                                $query->orWhere('products.reference', 'LIKE', '%'.$search.'%');
                                                $query->orWhere('products.name', 'LIKE', '%'.$search.'%');
                                            })
                                            ->where('cities.id', $cityId);
                        }           
                    }    
                    $products = $products  ->limit(80);
                    if(!$searchCreate)
                    $products = $products->orderBy('product_stock_updates.created_at','DESC');                     
                    $products = $products->get();
        return $products;
    }
}
