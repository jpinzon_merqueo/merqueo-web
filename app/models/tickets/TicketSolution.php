<?php

namespace tickets;

use exceptions\MerqueoException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * Class TicketCallSolution
 * @property int $id
 * @property \Order|null $complaint_order
 * @property \Coupon|null $coupon
 * @package tickets
 */
class TicketSolution extends Model
{
    const COMPLAINT_ORDER = 1;
    const VOUCHER = 2;
    const CREDIT = 3;
    const FREE_DELIVERY = 4;
    const OTHER = 5;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function layouts()
    {
        return $this->hasMany(TicketLayout::class);
    }

    /**
     * @param \Order $order
     * @param $solution_identifier
     * @param $amount
     * @param $expiration_date
     * @throws MerqueoException
     */
    public function manage(\Order $order, $solution_identifier, $amount, $expiration_date)
    {
        switch ($this->id) {
            case self::COMPLAINT_ORDER:
                $this->validate_complaint_order($order->user, $solution_identifier);
                break;

            case self::VOUCHER:
                $this->validate_voucher($solution_identifier);
                break;

            case self::CREDIT:
                $this->add_credit_to_user($order, $amount, $expiration_date);
                break;

            case self::FREE_DELIVERY:
                $this->give_free_next_delivery($order->user, $expiration_date);
                break;

            case self::OTHER:
                $this->other_action();
                break;

            default:
                throw new MerqueoException('La solución asignada no es valida.');
        }
    }

    /**
     * @param \User $user
     * @param $order_id
     * @throws ModelNotFoundException
     * @return \Order
     */
    private function validate_complaint_order(\User $user, $order_id)
    {
        $this->complaint_order = \Order::where('user_id', $user->id)->findOrFail($order_id);
        return $this->complaint_order;
    }

    /**
     * @param $voucher_code
     * @throws ModelNotFoundException
     * @return \Coupon
     */
    private function validate_voucher($voucher_code)
    {
        $this->coupon = \Coupon::where('code', $voucher_code)->firstOrFail();
        return $this->coupon;
    }

    /**
     * @param $order
     * @param $amount
     * @param $expiration_date
     * @return \UserCredit
     */
    private function add_credit_to_user($order, $amount, $expiration_date)
    {
        return \UserCredit::addCredit($order->user, $amount, $expiration_date, 'order', $order);
    }

    /**
     * @param \User $user
     * @param string $expiration_date
     */
    private function give_free_next_delivery($user, $expiration_date)
    {
        $user->setNextDeliveryFree($expiration_date);
        $user->save();
    }


    private function other_action()
    {
        // TODO validar que que sucede en este caso.
    }
}