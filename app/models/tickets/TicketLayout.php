<?php

namespace tickets;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TicketLayout
 * @property string $name
 * @property string $body
 * @property string $email_subject
 * @package tickets
 */
class TicketLayout extends Model
{
    const NUM_DYNAMIC_VARS = 10;

    /**
     * {@inheritdoc}
     */
    protected $fillable = [
        'name',
        'body',
        'email_subject',
        'ticket_solution_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function solution()
    {
        return $this->belongsTo(TicketSolution::class, 'ticket_solution_id');
    }

    /**
     * @param \User $user
     * @param \Order $order
     * @param string $content
     * @param \Order|null $complain_order
     * @param \Coupon|null $coupon
     * @return string
     */
    public function renderTemplate(
        \User $user,
        \Order $order,
        $content = '',
        \Order $complain_order = null,
        \Coupon $coupon = null
    ) {
        $variables = self::getReplaceableVariables($user, $order, $content, $complain_order, $coupon);
        return $this->renderFromVariables($variables);
    }

    /**
     * @param $common_variables
     * @return string
     */
    public function renderFromVariables($common_variables)
    {
        $this->body = html_entity_decode($this->body);
        foreach ($common_variables as $name => $value) {
            $this->body = preg_replace('/{{' . $name . '}}/', preg_quote($value ?: ''), $this->body);
        }

        return $this->body;
    }

    /**
     * @param \User $user
     * @param \Order $order
     * @param $content
     * @param \Order|null $complain_order
     * @param \Coupon|null $coupon
     * @return array
     */
    public static function getReplaceableVariables(
        \User $user,
        \Order $order,
        $content = '',
        \Order $complain_order = null,
        \Coupon $coupon = null
    ) {
        $admin = \Admin::findOrNew(\Session::get('admin_id'));
        $last_credit = ($order->exists ? $order->userCredit()->latest()->first() : new \UserCredit()) ?: new \UserCredit();
        $coupon_time_diff = static::getDifferenceInDays(!empty($coupon) ? $coupon->expiration_date : '');
        $last_credit_diff = static::getDifferenceInDays($last_credit->expiration_date);

        try {
            $store = \Store::where('city_id', $user->addresses()->first()->city_id)->firstOrFail();
            $minimum_order_amount = currency_format($store->minimum_order_amount);
        } catch (\Exception $exception) {
            $minimum_order_amount = '$30.000';
        }

        $free_delivery_amount = currency_format(
            \Discount::where('type', 'free_delivery')
                ->firstOrFail()
                ->minimum_order_amount
        );

        return [
            'NOMBRE_USUARIO' => $user->first_name,
            'APELLIDO_USUARIO' => (empty($user->last_name) ? '' : $user->last_name),
            'CORREO_USUARIO' => $user->email,
            'TELEFONO_USUARIO' => (empty($user->phone) ? '' : $user->phone),
            'REFERENCIA_PEDIDO' => (empty($order->reference) ? '' : $order->reference),

            'NUMERO_TAJETA_CREDITO' => (empty($order->cc_last_four) ? "" : "****{$order->cc_last_four}"),

            'VALOR_MINIMO_PEDIDO' => $minimum_order_amount,

            'CODIGO_CUPON' => (empty($coupon) ? 'CUPÓN INVALIDO' : $coupon->code),
            'VALOR_CUPON' => (empty($coupon) ? 'CUPÓN INVALIDO' : $coupon->amount),

            'DIAS_RESTANTES_CUPON' => $coupon_time_diff,

            'FECHA_PEDIDO_RECLAMO' => (empty($complain_order)
                ? 'MODIFICAR'
                : format_date('normal_long', explode(' ', $complain_order->delivery_date)[0])
            ),
            'FRANJA_PEDIDO_RECLAMO' => (empty($complain_order->delivery_time)
                ? 'MODIFICAR'
                : $complain_order->delivery_time
            ),

            'MONTO_DOMICILIO_GRATIS' => $free_delivery_amount,
            'FECHA_ACTUAL' => format_date('normal_long', date('Y-m-d')),

            'CONTENIDO_ADICIONAL_1' => $content,
            'NOMBRE_ASESOR' => $admin->fullname,
            'EMAIL_ASESOR' => $admin->username,

            'MONTO_ULTIMO_CRÉDITO_ASOCIADO' => currency_format($last_credit->amount),
            'FECHA_EXPIRACIÓN_ULTIMO_CRÉDITO_ASOCIADO' => $last_credit->expiration_date
                ? format_date('normal_long', $last_credit->expiration_date) : '',
            'DÍAS_RESTANTES_ULTIMO_CRÉDITO_ASOCIADO' => $last_credit_diff,
        ];
    }

    /**
     * @param $expiration_date
     * @return int
     */
    private static function getDifferenceInDays($expiration_date)
    {
        $now = Carbon::create();
        $new_date = new Carbon(!empty($expiration_date) ? $expiration_date : '');
        return $now->diffInDays($new_date);
    }
}
