<?php

namespace tickets;

use Carbon\Carbon;
use exceptions\MerqueoException;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TicketCall
 * @property Carbon $management_date
 * @property string $status
 * @property int $reason_id
 * @property int $solution_id
 * @property Ticket $ticket
 * @property string $comments
 * @property string $user_score
 * @property int $id
 * @property \Admin $admin
 * @property TicketSolution $solution
 * @property string $email_body
 * @property string $token
 * @method \Illuminate\Database\Eloquent\Builder pending()
 * @method \Illuminate\Database\Eloquent\Builder userActiveCalls(\Admin $admin)
 * @package tickets
 */
class TicketCall extends Model
{
    const RE_ASSIGNED = 'Re asignado';
    const PENDING = 'Pendiente';
    const ATTENDED = 'Contesta';
    const NOT_ATTENDED = 'No contesta';
    const OPEN = 'Abierto';
    const RESOLVED = 'Resuelto';

    /**
     * {@inheritdoc}
     */
    protected $dates = [
        'management_date'
    ];

    /**
     * {@inheritdoc}
     */
    protected $fillable = [
        'comments',
        'status',
        'solution_id',
        'reason_id'
    ];

    /**
     * {@inheritdoc}
     */
    protected $hidden = [
        'token'
    ];

    /**
     * {@inheritdoc}
     */
    protected static function boot()
    {
        parent::boot();
        self::creating(function (TicketCall $model) {
            $model->token = generate_token();
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function ticket()
    {
        return $this->belongsTo(Ticket::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function admin()
    {
        return $this->belongsTo(\Admin::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function solution()
    {
        return $this->belongsTo(TicketSolution::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function reason()
    {
        return $this->belongsTo(TicketReason::class);
    }

    /**
     * @param $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('status', self::PENDING)->whereNull('management_date');
    }

    /**
     * @param $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePending($query)
    {
        return $query->where('status', self::PENDING);
    }

    /**
     * @param $query
     * @param \Admin $admin
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeUserActiveCalls($query, \Admin $admin)
    {
        return $query->where('admin_id', $admin->id)
            ->where('ticket_calls.status', self::PENDING)
            ->whereHas('ticket', function ($query) use ($admin) {
                $query->where('status', Ticket::ASSIGNED);
            });
    }

    /**
     * @param $status
     * @param $comments
     * @return TicketCall
     * @throws MerqueoException
     */
    public function saveStatus($status, $comments, $reason = NULL)
    {

        $this->status = empty($status) ? self::RE_ASSIGNED : $status;

        if (!in_array($this->status, [self::ATTENDED, self::NOT_ATTENDED, self::RE_ASSIGNED])) {
            throw new MerqueoException('El estado del ticket no es valido.');
        }
        $this->reason_id = $reason;
        $this->comments = $comments;
        $this->management_date = Carbon::create();
        $this->save();

        return $this;
    }
}
