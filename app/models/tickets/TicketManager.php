<?php

namespace tickets;

class TicketManager
{

    /**
     * @var \User
     */
    private $user;

    /**
     * @var TicketCall
     */
    private $ticket_call;

    /**
     * TicketCallSolution constructor.
     * @param \User $user
     * @param TicketCall $ticket_call
     */
    public function __construct(\User $user, TicketCall $ticket_call)
    {
        $this->user = $user;
        $this->ticket_call = $ticket_call;
    }
}