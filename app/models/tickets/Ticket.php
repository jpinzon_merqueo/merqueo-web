<?php

namespace tickets;

use Carbon\Carbon;
use exceptions\MerqueoException;
use \Illuminate\Database\Eloquent\Model;

/**
 * Class Ticket
 * @property \Order $order
 * @property \Admin $admin
 * @property \CustomerService $customerService
 * @property TicketCall[] $calls
 * @property TicketCall $lastCall
 * @property TicketCall $activeCall
 * @property string $status
 * @property int $id
 * @package tickets
 */
class Ticket extends Model
{
    const CREATED = 'Creado';
    const ASSIGNED = 'Asignado';
    const PENDING = 'Pendiente';
    const SOLVED = 'Solucionado';
    const OPEN = 'Abierto';
    const RESOLVED = 'Resuelto';

    /**
     * @todo Debe ser un parámetro dinámico.
     */
    const TIME_RANGE = 30;

    /**
     * @todo Debe ser un parámetro de configuración.
     */
    const QUEUE_SIZE = 5;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(\User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo(\Order::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function customerService()
    {
        return $this->belongsTo(\CustomerService::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function calls()
    {
        return $this->hasMany(TicketCall::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function lastCall()
    {
        return $this->hasOne(TicketCall::class)->orderBy('id', 'desc');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function activeCall()
    {
        return $this->hasOne(TicketCall::class)->active()->orderBy('id', 'desc')->groupBy('ticket_id');
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeJustCreated($query)
    {
        return $query->where('status', self::CREATED);
    }

    /**
     * @param int $limit
     * @param bool $force_refresh
     * @param array $types
     * @return Ticket[]
     */
    public static function getFreeTickets($limit = 1, array $types, $force_refresh = true)
    {
        // Los registros en estado 'Pending' son modificados despues de determinado
        // tiempo y pasan a 'Created' de nuevo para ser asignados.
        if ($force_refresh) {
            self::refreshPendingTickets();
        }

        if (empty($types)) {
            return [];
        }

        return Ticket::where('status', self::CREATED)
            ->whereIn('customer_service_id', $types)
            ->limit($limit)
            ->get();
    }

    /**
     * @param \Order $order
     * @param string $type
     * @return Ticket
     */
    public static function createTicket(\Order $order = NULL, $type, $message = NULL)
    {
        $ticket = new Ticket();
        if ($order)
            $ticket->order()->associate($order);

        if($message){
            $user_id = \User::where('email', trim($message->from_email))->first();
            if($user_id){
                $ticket->user_id = $user_id->id;
            }

            $subject = $message->subject."<br>";
            $from = $message->from."<br>";
            $to = $message->to."<br>";
            $date = $message->date."<br>";
            $body = $message->body;
            $message_complete = $subject.$from.$to.$date.$body;
            $ticket->message = $message_complete;
            $ticket->user_from_email = trim($message->from_email);
            $ticket->user_from_name = trim($message->from_user);
        }

        $service = \CustomerService::where('description', $type)->firstOrFail();
        $ticket->customerService()->associate($service);
        $ticket->save();

        if ($type === \CustomerService::PQR && $order) {
            $last_ticket = Ticket::where('id', '<>', $ticket->id)
                                 ->where('order_id', $order->id)
                                 ->latest()->has('calls.admin')->first();

            if (!empty($last_ticket)) {
                $call = $last_ticket->calls()->latest()->with('admin')->first();
                $ticket->assignTicket($call->admin);
            }
        }

        return $ticket;
    }

    /**
     * @return mixed
     */
    private static function refreshPendingTickets()
    {
        return Ticket::where('status', self::PENDING)
            ->whereIn('id', function ($query) {
                $condition_field = \DB::raw('management_date + INTERVAL ' . self::TIME_RANGE . ' MINUTE');
                $query->select('ticket_id')
                    ->from('ticket_calls')
                    ->where($condition_field, '<', date('Y-m-d G:i:s'))
                    ->join(\DB::raw('(SELECT id AS max_id FROM ticket_calls) max_date_table'), function ($join) {
                        $join->on('max_date_table.max_id', '=', 'ticket_calls.id');
                    });
            })->update(['status' => self::CREATED]);
    }

    /**
     * @param \Admin $admin
     * @return TicketCall
     */
    public function assignTicket(\Admin $admin)
    {
        $this->status = self::ASSIGNED;
        $this->save();

        $ticket_call = new TicketCall();
        $ticket_call->ticket()->associate($this);
        $ticket_call->admin()->associate($admin);
        $ticket_call->save();

        return $ticket_call;
    }

    /**
     * @param \Admin $admin
     * @return TicketCall
     */
    public function moveTicket(\Admin $admin)
    {
        $this->activeCall->status = TicketCall::RE_ASSIGNED;
        $this->activeCall->management_date = Carbon::create();
        $this->activeCall->save();

        return $this->assignTicket($admin);
    }

    /**
     * @return int
     */
    public function totalActiveCalls()
    {
        return $this->calls()->active()->count();
    }

    /**
     * @throws MerqueoException
     */
    public function validateTicketAssignment()
    {
        $total_pending_ticket_calls = $this->totalActiveCalls();
        if ($total_pending_ticket_calls > 0) {
            throw new MerqueoException('Este ticket ya ha sido asignado.');
        }
    }

    /**
     * @param string $status
     * @return Ticket
     * @throws MerqueoException
     */
    public function saveStatus($status)
    {
        if (!in_array($status, [self::PENDING, self::SOLVED, self::RESOLVED])) {
            throw new MerqueoException('El estado del ticket seleccionado no es valido.');
        }
        $this->status = $status;
        $this->save();

        return $this;
    }

    /**
     * @param \Admin $admin
     * @param TicketCall $call
     * @throws MerqueoException
     */
    public static function validateUserQueue(\Admin $admin, TicketCall $call)
    {
        $current_call = $admin->calls()->userActiveCalls($admin)->first();
        if ($current_call->id !== $call->id) {
            throw new MerqueoException('Se debe solucionar el primer ticket asignado.');
        }
    }

    /**
     * @return bool
     */
    public function isSolved()
    {
        return $this->status === self::SOLVED || $this->status === self::RESOLVED;
    }

    /**
     * @return bool
     */
    public function isAssigned()
    {
        return $this->status === self::ASSIGNED;
    }
}
