<?php

use Symfony\Component\HttpKernel\Exception\HttpException;
use \Illuminate\Database\Eloquent\Model;

/**
 * @property OrderProduct $OrderProduct
 * @property string $problem
 */
class OrderProductScore extends Model
{
    /**
     * {@inheritdoc}
     */
    protected $fillable = ['reason', 'order_product_id'];

    /**
     * Relación con la tabla "order_products".
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function orderProduct()
    {
        return $this->belongsTo(OrderProduct::class);
    }

    /**
     * Almacena la califación de los productos del pedido.
     *
     * @param $order_products
     */
    public static function saveProductScores($order_products)
    {
        foreach ($order_products as $product) {
            $product_score = new \OrderProductScore([
                'order_product_id' => $product['id'],
                'reason' => $product['reason'],
            ]);
            $product_score->save();
        }
    }

    /**
     * Valida la calificación de los producto del pedido entregado.
     *
     * @param $input
     * @throws HttpException
     * @todo Agregar excepciones personalizadas.
     */
    public static function validateOrderProductScores($input)
    {
        $order_products = $input['order_products'];
        if (!is_array($order_products)) {
            throw new HttpException(400, 'El listado de los productos debe ser un arreglo.');
        }

        // Se debe validar que cada uno de los "order_product" exista.
        $product_list = [];
        foreach ($order_products as $product) {
            $product_list[] = empty($product['id']) ? 0 : $product['id'];

            if (empty($product['reason'])) {
                throw new HttpException(400, 'Se debe enviar algún comentario de los productos asociados.');
            }
        }

        $total_real_order_products = OrderProduct::whereIn('id', $product_list)->count();
        if (count($input['order_products']) !== $total_real_order_products) {
            throw new HttpException(400, 'Uno de los productos indicado no es valido.');
        }
    }
}
