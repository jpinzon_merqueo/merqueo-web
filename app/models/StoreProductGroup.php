<?php

/**
 * @property int $price
 * @property StoreProduct $storeProduct
 * @property float|int|mixed discount_amount
 */
class StoreProductGroup extends \Illuminate\Database\Eloquent\Model
{
    /**
     * @var string
     */
    protected $table = 'store_product_group';

    /**
     * StoreProductGroup constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function storeProduct()
    {
    	return $this->belongsTo('StoreProduct', 'store_product_id');
    }

    /**
     * StoreProductGroup constructor.
     */
    public function storeProductGroup()
    {
        return $this->belongsTo('StoreProduct', 'store_product_group_id');
    }
}
