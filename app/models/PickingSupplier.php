<?php

class PickingSupplier extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'picking_supplier';

    public static function deleteTask($id){
    	self::where('id', $id)
    					->delete();
    }

    public static function searchProducts($search, $warehouse_id, $type_storage){

    	$products = StoreProduct::select('store_products.id', DB::raw("CONCAT_WS(' ',products.name, products.quantity, products.unit) AS name"), 'products.reference', 'store_product_warehouses.minimum_picking_stock', 'store_product_warehouses.maximum_picking_stock', 'store_product_warehouses.picking_stock AS quantity_picking', 'products.image_large_url as image', 'store_product_warehouses.warehouse_id', 'store_product_warehouses.storage_position AS picking_position', 'store_product_warehouses.storage_height_position AS picking_position_height', 'store_products.storage',
			DB::raw("IF(store_product_warehouses.picking_stock <= store_product_warehouses.minimum_picking_stock OR store_product_warehouses.picking_stock < store_product_warehouses.maximum_picking_stock, store_product_warehouses.maximum_picking_stock-store_product_warehouses.picking_stock, 0) AS quantity_to_suppling"),
			DB::raw("IFNULL(admin.fullname, '') AS admin_fullname"), DB::raw("IFNULL(admin.id, 0) AS admin_id"))

    					->join('store_product_warehouses', 'store_products.id', '=', 'store_product_warehouses.store_product_id')
    					->join('products', 'store_products.product_id', '=', 'products.id')
    					->leftJoin('picking_supplier', function($join) use ($warehouse_id){
    						$join->on('picking_supplier.warehouse_id', '=', 'store_product_warehouses.warehouse_id');
    						$join->on('picking_supplier.store_product_id', '=', 'store_product_warehouses.store_product_id');
    						$join->on('picking_supplier.warehouse_id', '=', DB::raw($warehouse_id));
    						$join->on('picking_supplier.status','<>', DB::raw("'Success'"));
    					})
    					->leftJoin('admin', 'admin.id', '=', 'picking_supplier.picking_supplier_id')
    					->whereRaw("store_products.id IN (SELECT DISTINCT store_product_id FROM warehouse_storages WHERE warehouse_id =".$warehouse_id." )")
    					->where('store_product_warehouses.warehouse_id', $warehouse_id)
    					->where('products.type', 'Simple')
    					->whereIn('store_products.storage', $type_storage)
    					->where(function($query) use ($search){
    						$query->orWhere('products.name', 'LIKE', '%'.$search.'%');
    						$query->orWhere('products.reference', 'LIKE', '%'.$search.'%');
    					})
                        //->where('quantity_to_suppling','>', 0)
    					->groupby('store_products.id')
                        ->orderby('quantity_to_suppling')
                        //->having('quantity_to_suppling','>', 0)
    					->get();
    	return $products;
    }

    public static function getStoragedProducts($id_task){
    	return self::select('warehouse_storages.*', DB::raw("CONCAT_WS(' ',products.name, products.quantity, products.unit) AS name"), 'picking_supplier.quantity_to_suppling', 'products.reference', 'products.image_large_url as image')
								->join('warehouse_storages', function($join){
									$join->on('warehouse_storages.warehouse_id', '=', 'picking_supplier.warehouse_id');
									$join->on('warehouse_storages.store_product_id', '=', 'picking_supplier.store_product_id');
								})
								->join('store_products', 'store_products.id', '=', 'warehouse_storages.store_product_id')
								->join('products', 'products.id', '=', 'store_products.product_id')
								->where('picking_supplier.id', Input::get('id'))
								/*->where(function($query){
									$query->whereNull('warehouse_storages.expiration_date' )
									->orwhere('warehouse_storages.expiration_date','>', date('Y-m-d'));
								})*/
                                ->orderby(DB::raw("case when warehouse_storages.expiration_date is null then 1 else 0 end, warehouse_storages.expiration_date"), 'ASC')
								//->orderby('warehouse_storages.expiration_date', 'ASC')
                                ->orderby('warehouse_storages.created_at', 'ASC')
                                ->skip(0)
                                ->take(1)
								->get();
    }
}