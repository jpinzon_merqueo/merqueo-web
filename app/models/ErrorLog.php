<?php

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $url
 * @property int $response_http_code
 * @property string $message
 * @property string $request
 * @property string $ip
 */
class ErrorLog extends Model
{
    /**
     * @var string
     */
    protected $table = 'error_log';

    /**
     * @param Exception $exception
     * @param int $code
     * @return void
     */
    public static function add(Exception $exception, $code = 500)
    {
        $log = new ErrorLog();
        $log->url = URL::current() ?: self::getCurrentIp();
        $log->response_http_code = $code;
        $log->message = $exception->getMessage();
        $log->request = self::buildMessage($exception);
        $log->ip = self::getCurrentIp();

        $log->save();
    }

    /**
     * @param Exception $exception
     * @return string
     */
    public static function buildMessage(Exception $exception)
    {
        $requestHeaders = '';
        foreach (getallheaders() as $name => $value) {
            $requestHeaders .= "\n
$name: $value";
        }

        //encriptacion de tarjeta de credito
        if (Input::has('number_cc')){
            $numberCc = Input::get('number_cc');
            $numberCc = substr($numberCc, 0, 6).'******'.substr($numberCc, 12, 16);
            Input::merge(['number_cc' => $numberCc]);
            Input::merge(['code_cc' => '***']);
        }

        $params = json_encode(Input::get());
        $url = URL::current();
        $method = Request::method();
        $ip = self::getCurrentIp();

        $message = "\r\nURL: {$url} {$method}\r\n" .
            "HEADERS: {$requestHeaders}\r\n" .
            "PARAMS: {$params}\r\n" .
            "MESSAGE: {$exception->getMessage()}\r\n" .
            "FILE: {$exception->getFile()}\r\n" .
            "LINE: {$exception->getLine()}\r\n" .
            "TRACE: {$exception->getTraceAsString()}\r\n" .
            "IP: {$ip}\r\n" .
            "Private IP: " . private_ip();

        return $message;
    }

    /**
     * @return string
     */
    public static function getCurrentIp()
    {
        return get_ip() ?: private_ip();
    }
}
