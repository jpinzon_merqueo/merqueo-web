<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 08/06/2018
 * Time: 18:17
 */

class ZoneDeliveryWindow extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'zone_delivery_windows';

    /*public static function getMaximumOrdersPerRoute($zone_id, $delivery_window_id){
        return self::where('delivery_window_id', $delivery_window_id)
            ->where('zone_id', $zone_id)
            ->pluck('maximum_orders_per_route');
    }*/
    public static function getMaximumOrdersPerRoute($zone_id){
        $delivery_windows =  self::where('zone_id', $zone_id)
            ->get();
        $orders_per_routes = [];
        foreach ($delivery_windows as $delivery_window)
            $orders_per_routes[$delivery_window->delivery_window_id] = $delivery_window->maximum_orders_per_route;

        return $orders_per_routes;
    }
}