<?php

use \Illuminate\Database\Eloquent\Model;

class DeliveryExpress extends Model
{
    const MAXIMUM_WEIGHT_CAPACITY = 50000; //50kg
    const VOLUME_CAPACITY = 125000; //50x50x50 cms3
    const ERROR_RANGE_CAPACITY = 0.8; //20%
    const MAXIMUM_VOLUME_CAPACITY = self::VOLUME_CAPACITY * self::ERROR_RANGE_CAPACITY;

    /**
     * Valida las 4 condiciones necesaria para qué sea una orden Express
     * 1. Qué el usuario se encuentre dentro de la zona de cobertura de la bodega express
     * 2. Qué los pruductos del carrito se encuentren en bodega express
     * 3. Qué la orden no exceda la capacidad máxima de volumetría ( 50kg o 50x50x50 cms)
     * 4. Qué se encuentre dentro de la franja horaria de servicio en la bodega express
     *
     * @param $address
     * @param $cart_products
     * @param $dark_products
     * @param $zone_dark
     * @return array
     */
    public static function validateCoverageAndProductsDarkSupermarket($address, $cart_products, $dark_products, $zone_dark){

        if(!$address || !$cart_products || !$dark_products || !$zone_dark){
            return [];
        }

        //Valida zona de cobertura express
        if(!point_in_polygon($zone_dark->delivery_zone, $address->latitude, $address->longitude)){
            return [];
        }

        //Valida qué los productos del carrito se encuentren en bodega express
        if(count($cart_products) != self::validateAvailableProductsDarksupermarket($dark_products, $zone_dark)){
            return [];
        }

        //Valida qué volumetría de la orden no exceda su capacidad máxima
        if(!self::validateExpressVolume($cart_products)){
            return [];
        }

        //Valida franja horaria disponible de bodega express
        return self::getSlotWarehouseExpress($zone_dark);
    }

    /**
     * Valida volumetría de la orden no exceda su capacidad máxima express
     * 50 kg o 50x50x50 cms
     *
     * @param $cart_products
     * @return bool
     */
    private static function validateExpressVolume($cart_products){
        $store_products_ids = $products_quantity = [];
        foreach ($cart_products as $cart_product){
            $store_products_ids[] = $cart_product->store_product_id;
            $products_quantity[$cart_product->store_product_id] = $cart_product->quantity + $cart_product->quantity_full_price;
        }

        $products = StoreProduct::with('product')
            ->whereIn('store_products.id', $store_products_ids)
            ->get();

        $volume = $weight = $product_group_weight = $product_group_volume = 0;
        foreach ($products as $product){
            switch ($product->product->type){
                case 'Simple':
                    $weight += $product->product->weight * $products_quantity[$product->id];
                    $volume += $product->product->volume * $products_quantity[$product->id];
                    break;
                case 'Agrupado':
                    $product_groups = ProductGroup::select(
                        'product_group.quantity',
                        'products.volume',
                        'products.weight'
                    )
                        ->join('products', 'product_group_id', '=', 'products.id')
                        ->where('product_id', $product->product->id)
                        ->get();
                    foreach ($product_groups as $product_group){
                        $product_group_weight += $product_group->weight * $product_group->quantity;
                        $product_group_volume += $product_group->volume * $product_group->quantity;
                    }
                    $weight += $product_group_weight * $products_quantity[$product->id];
                    $volume += $product_group_volume * $products_quantity[$product->id];
                    break;
            }
        }

        return $weight <= self::MAXIMUM_WEIGHT_CAPACITY && $volume <= self::MAXIMUM_VOLUME_CAPACITY;
    }

    /**
     * Valida horario de servicio de bodega express.
     * Se le resta una hora a la hora final de servicio,
     * qué es la máxima en qué el horario está disponible para usuarios.
     *
     * @param Zone $zone_dark
     * @return array
     */
    private static function getSlotWarehouseExpress(Zone $zone_dark){
        $slot_warehouse_express = [];
        $slot_warehouses = $zone_dark->warehouse->slotWarehouse()
            ->join('delivery_windows', 'delivery_window_id', '=', 'delivery_windows.id')
            ->where('delivery_windows.shifts', 'EX')
            ->get();
        $current_date = \Carbon\Carbon::now();
        $hora = $current_date->hour;
        foreach ($slot_warehouses as $slot_warehouse){
            $hour_start = (int) \Carbon\Carbon::parse($slot_warehouse->hour_start)->format('H');
            $hour_end = (int) \Carbon\Carbon::parse($slot_warehouse->hour_end)->format('H');
            if( $hora >= $hour_start && $hora < $hour_end ){
                $slot_warehouse_express = $slot_warehouse;
                break;
            }
        }

        return $slot_warehouse_express;
    }

    /**
     * Valida productos activos en darksupermarket
     *
     * @param $dark_products
     * @param $zone_dark
     * @return mixed
     */
    private static function validateAvailableProductsDarksupermarket($dark_products, $zone_dark){
        $store_products_ids = [];
        foreach ($dark_products as $dark_product){
            $store_products_ids[] = $dark_product->store_product_id;
        }

        $dark_products_available = StoreProductWarehouse::where('store_product_warehouses.warehouse_id', $zone_dark->warehouse_id)
            ->whereIn('store_product_id', $store_products_ids)
            ->available(true)
            ->count();

        return $dark_products_available;
    }

}