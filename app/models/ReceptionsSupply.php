<?php

class ReceptionsSupply extends \Illuminate\Database\Eloquent\Model {
    protected $table = 'receptions_supply';

    public static function getBlockingProduct($order_reception_id, $storage){
        $storage = $storage == 'Seco' ? array($storage):array('Refrigerado','Congelado');
    	$product = self::select('products.name')
    		->join('store_products', 'store_products.id','=','receptions_supply.store_product_id')
    		->join('products', 'products.id','=','store_products.product_id')
    		->where('receptions_supply.type', 'Alistamiento')
    		->where('receptions_supply.admin_id', Session::get('admin_id') )
    		->where('receptions_supply.order_reception_id',$order_reception_id )
    		->whereIn('receptions_supply.status', array('En alistamiento','En proceso'))
            ->whereIn('store_products.storage', $storage)
    		->get();
    	if($product){
    		return $product->first();
    	}
    }

    public static function getAllProductOrderReception($order_reception_id, $storage){
        $storage = $storage == 'Seco' ? array($storage):array('Refrigerado','Congelado');
    	$products = self::select('store_products.id', 'store_products.picking_stock', 'store_products.minimum_picking_stock', 'store_products.maximum_picking_stock', 'provider_order_reception_details.quantity_received', 'provider_order_reception_details.expiration_date', 'products.name', 'products.reference', 'products.image_small_url',
            DB::raw('receptions_supply.quantity_to_supply AS quantity_for_picking_stock'), 'receptions_supply.quantity_to_storage', 'receptions_supply.quantity_storaged', 'receptions_supply.type', 'receptions_supply.status', 'receptions_supply.quantity_supplied', 'receptions_supply.date_suppliering', 'products.unit', DB::raw('receptions_supply.id AS supply_id'),'receptions_supply.order_reception_id' )
    		->join('store_products', 'store_products.id','=','receptions_supply.store_product_id')
    		->join('products', 'products.id','=','store_products.product_id')
    		->join('provider_order_reception_details', 'store_products.id', '=', 'provider_order_reception_details.store_product_id')
    		->join('provider_order_receptions', 'provider_order_receptions.id', '=', 'provider_order_reception_details.reception_id')
    		->where('receptions_supply.admin_id', Session::get('admin_id') )
    		->whereRaw('receptions_supply.order_reception_id = provider_order_receptions.id' )
    		->where('receptions_supply.order_reception_id',$order_reception_id )
            ->whereIn('store_products.storage', $storage)
    		->get();   
    	
    	if($products){
    		return $products->toArray();
    	}
    }

    public static function getProduct($supply_id){
        $products = self::select('store_products.id', 'store_products.picking_stock', 'store_products.minimum_picking_stock', 'store_products.maximum_picking_stock', 'provider_order_reception_details.quantity_received', 'provider_order_reception_details.expiration_date', 'products.name', 'products.reference', 'products.image_small_url',
            DB::raw('receptions_supply.quantity_to_supply AS quantity_for_picking_stock'), 'receptions_supply.quantity_to_storage', 'receptions_supply.quantity_storaged', 'receptions_supply.type', 'receptions_supply.status', 'receptions_supply.quantity_supplied', 'receptions_supply.date_suppliering', 'products.unit', DB::raw('receptions_supply.id AS supply_id'),'receptions_supply.order_reception_id' )
            ->join('store_products', 'store_products.id','=','receptions_supply.store_product_id')
            ->join('products', 'products.id','=','store_products.product_id')
            ->join('provider_order_reception_details', 'store_products.id', '=', 'provider_order_reception_details.store_product_id')
            ->join('provider_order_receptions', 'provider_order_receptions.id', '=', 'provider_order_reception_details.reception_id')
            ->where('receptions_supply.admin_id', Session::get('admin_id') )
            ->whereRaw('receptions_supply.order_reception_id = provider_order_receptions.id' )
            ->where('receptions_supply.id',$supply_id )
            ->get();
        
        if($products){
            return $products->first();
        }
    }
}