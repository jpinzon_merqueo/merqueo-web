<?php

class ProviderOrderReceptionDetail extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'provider_order_reception_details';
    protected $hidden = array('created_at','updated_at');

    public static function boot()
    {
        parent::boot();

        static::creating(function ($providerOrderReceptionDetail) {
            $base_cost = $providerOrderReceptionDetail->base_cost;
            $providerOrderReceptionDetail->original_base_cost = $base_cost;
        });
    }

    public function providerOrderReception()
    {
        return $this->belongsTo('ProviderOrderReception', 'reception_id');
    }

    public function storeProduct()
    {
        return $this->belongsTo('StoreProduct', 'store_product_id');
    }

    public function providerOrderReceptionDetailProductGroup()
    {
        return $this->hasMany('ProviderOrderReceptionDetailProductGroup', 'reception_detail_id');
    }

    public function providerOrderReceptionValidate()
    {
        return $this->hasMany('ProviderOrderReceptionValidate', 'reception_detail_id');
    }

    /**
     * Actualizar estado de producto agrupado en pedido
     */
    public function updateProductGroupedStatus()
    {
    	$reception_products = ProviderOrderReceptionDetailProductGroup::where('reception_detail_id', $this->id)->get();
        if (count($reception_products))
        {
            $recibido = $no_recibido = $danado = $pendiente = $parcialmente_recibido = 0;
            foreach($reception_products as $reception_product)
            {
                if ($reception_product->status == 'Pendiente'){
                    $this->status = 'Pendiente';
                    $pendiente++;
                    break;
                }
                if ($reception_product->status == 'Recibido')
                    $recibido++;
                if ($reception_product->status == 'No recibido')
                    $no_recibido++;
                if ($reception_product->status == 'Dañado')
                    $danado++;
                if ($reception_product->status == 'Parcialmente recibido')
                    $parcialmente_recibido++;
            }

            if ($no_recibido && !$recibido && !$parcialmente_recibido && !$pendiente && !$danado)
                $this->status = 'No recibido';
            else if ($recibido && $no_recibido)
                $this->status = 'Parcialmente recibido';
            else if ($recibido && !$pendiente && !$danado && !$parcialmente_recibido)
                $this->status = 'Recibido';
            else if ($parcialmente_recibido)
                $this->status = 'Parcialmente recibido';
            else if ($danado && !$pendiente)
                $this->status = 'Dañado';

            $this->save();
        }
    }
}