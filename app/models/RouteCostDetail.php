<?php
class RouteCostDetail extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'route_cost_details';

    public function admin() {
        return $this->belongsTo('Admin');
    }

    public function route() {
        return $this->belongsTo('Routes', 'route_id');
    }

}