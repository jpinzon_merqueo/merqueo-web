<?php

class ProviderContact extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'provider_contacts';

    /**
     * @param $query
     * @param bool $state
     * @return mixed
     */
    public function scopeActive($query, $state = true)
    {
        return $query->where('status', $state);
    }
}
