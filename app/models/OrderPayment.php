<?php

class OrderPayment extends \Illuminate\Database\Eloquent\Model {

    const CREDIT_CARD = 'Tarjeta de crédito';
    const AMERICAN_EXPRESS = 'AMEX';
    const VISA = 'VISA';
    const MASTERCARD = 'MASTERCARD';
    const CODENSA = 'CODENSA';
    const CASH = 'Efectivo';
    const POS = 'Datáfono';

    const TRANSACTION_TYPE_AUTHORIZE = 'authorize';
    const TRANSACTION_TYPE_CAPTURE = 'capture';

    const TRANSACTION_STATUS_APPROVED = 'Aprobada';
    const TRANSACTION_STATUS_DECLINED = 'Declinada';
    const TRANSACTION_STATUS_EXPIRED = 'Expirada';
    const TRANSACTION_STATUS_FAILED = 'Fallida';
    const TRANSACTION_STATUS_PENDING = 'Pendiente';

    protected $table = 'order_payments';

    /**
     * Obtiene los nombres completos de los métodos pago usado.
     *
     * @return array
     */
    public static function getPaymentMethods()
    {
        return [
            self::CREDIT_CARD => 'Tarjeta de crédito',
            self::AMERICAN_EXPRESS => 'Tarjeta de crédito American Express',
            self::VISA => 'Tarjeta de crédito Visa',
            self::MASTERCARD => 'Tarjeta de crédito Mastercard',
            //'CODENSA' => 'Tarjeta de crédito Codensa',
            self::CASH => 'Efectivo',
            self::POS => 'Datáfono'
        ];
    }

    /**
     * @param string $payment_method
     * @return bool
     */
    public static function hasCreditCardPaymentMethod($payment_method)
    {
        return !in_array($payment_method, [OrderPayment::CASH, OrderPayment::POS]);
    }
}
