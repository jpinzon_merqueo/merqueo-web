<?php

/**
 * @property mixed admin_id
 * @property int store_product_id
 * @property int warehouse_id
 * @property string type
 * @property int quantity
 * @property int quantity_stock_before
 * @property int quantity_stock_after
 * @property string reason
 */
class ProductStockUpdate extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'product_stock_updates';
    protected $hidden = array('created_at','updated_at');

}