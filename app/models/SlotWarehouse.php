<?php

/**
 * Class SlotWarehouse
 * @property string day
 * @property string delivery_window_id
 */
class SlotWarehouse extends \Illuminate\Database\Eloquent\Model
{
    /**
     * @var int Día festivo en los Slotwarehouses
     */
    const SAME_DAY = 7;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function deliveryWindow()
    {
        return $this->belongsTo(DeliveryWindow::class);
    }

    public static function boot()
    {
        parent::boot();

        static::updating(function ($slot_warehouse)
        {
            if ($slot_warehouse->isDirty())
            {
                $dirty_slot_warehouse = $slot_warehouse->getDirty();
                $original_slot_warehouse = $slot_warehouse->getOriginal();
                $original = $dirty = [];
                foreach ($dirty_slot_warehouse as $att => $value) {
                    $dirty[$att] = $value;
                    $original[$att] = $original_slot_warehouse[$att];
                }
                $changes = ['original' => $original, 'changes' => $dirty];
                $slot_warehouse_log = new SlotWarehouseLog;
                if (Session::has('admin_id')) {
                    $slot_warehouse_log->admin_id = Session::get('admin_id');
                }
                $change = (int)$slot_warehouse->number_products;
                $original = (int)$original_slot_warehouse['number_products'];
                $slot_warehouse_log->slot_warehouse_id = $slot_warehouse->id;
                $slot_warehouse_log->number_products_original = $original;
                $slot_warehouse_log->number_products_change = $change;
                $slot_warehouse_log->log = json_encode($changes);
                $slot_warehouse_log->save();
            }
        });
    }

    /**
     * @param $query
     * @param \Carbon\Carbon $date
     * @param $time_range
     * @return mixed
     */
    public function scopeDate($query, \Carbon\Carbon $date, $time_range)
    {
        $today = \Carbon\Carbon::today();
        $day = $date->dayOfWeek;

        if ($date->toDateString() === $today->toDateString()) {
            $day = self::SAME_DAY;
        }

        return $query->where('day', $day)
            ->whereHas('deliveryWindow', function ($query) use ($time_range) {
                $query->range($time_range)
                    ->available();
            });
    }

    /**
     * Retorna los slots por bodega segun los horarios establecidos en la configuración
     *
     * @param int $city_id
     * @param int $warehouseId
     * @return array
     */
    public function getSlots($warehouseId = null)
    {
        if ($warehouseId){
            $warehouse = Warehouse::find($warehouseId);
        }else {
            $warehouse = Warehouse::where('status', 1)
                ->where('warehouses.city_id', Session::get('admin_city_id'))
                ->first();
        }

        $express = $warehouse->id == Warehouse::WAREHOUSE_DARKSUPERMARKET_ID[0] ? true : false;
        $city = City::find($warehouse->city_id);
        $shift_delivery_windows = DeliveryWindow::getDeliveryWindowsByCity($city->id);
        $shift_delivery_windows_today = DeliveryWindow::getDeliveryWindowsByCity($city->id, true, $express);

        $dates = $dates_today = [];
        foreach($shift_delivery_windows AS $shift_delivery_window){
            $dates[$shift_delivery_window->id] = $this->getTimeRange($warehouse, $shift_delivery_window->hour_start, $shift_delivery_window->hour_end, $shift_delivery_window->id);
        }

        foreach($shift_delivery_windows_today AS $shift_delivery_window){
            $dates_today[$shift_delivery_window->id] = $this->getTimeRange($warehouse, $shift_delivery_window->hour_start, $shift_delivery_window->hour_end, $shift_delivery_window->id, true);
        }

        //obtener cantidad de productos en pedidos normales
        $slots = self::select('slot_warehouses.*')
            ->join('delivery_windows', 'slot_warehouses.delivery_window_id', '=', 'delivery_windows.id')
            ->where('delivery_windows.shifts', '<>', 'MD')
            ->where('slot_warehouses.warehouse_id', $warehouse->id)
            ->orderBy('slot_warehouses.id')
            ->get();
        foreach ($slots as $slot)
        {
            if (isset($dates[$slot->delivery_window_id])){
                $dates[$slot->delivery_window_id]['slots'][$slot->day]['numberProducts'] = $slot->number_products;
                //obtener productos actuales
                $slots_products = DB::table('slots_products')->where('warehouse_id', $warehouse->id)->where('delivery_window_id', $slot->delivery_window_id)->get();
                foreach ($slots_products as $slot_product)
                {
                    $dates[$slot_product->delivery_window_id]['slots'][$slot_product->day]['currentProducts'] = $slot_product->current_products;
                }
            }
        }

        $shifts = $express ? ['MD','EX'] : ['MD'];
        //obtener cantidad de productos en pedidos mismo dia
        $slots = self::select('slot_warehouses.*')
            ->join('delivery_windows', 'slot_warehouses.delivery_window_id', '=', 'delivery_windows.id')
            ->whereIn('delivery_windows.shifts', $shifts)
            ->where('slot_warehouses.warehouse_id', $warehouse->id)
            ->orderBy('slot_warehouses.id')
            ->get();
        foreach ($slots as $slot)
        {
            if (isset($dates_today[$slot->delivery_window_id])){
                $dates_today[$slot->delivery_window_id]['slots'][$slot->day]['numberProducts'] = $slot->number_products;
                $slots_products = DB::table('slots_products')->where('warehouse_id', $warehouse->id)->where('delivery_window_id', $slot->delivery_window_id)->get();
                foreach ($slots_products as $slot_product)
                {
                    $dates_today[$slot_product->delivery_window_id]['slots'][$slot_product->day]['currentProducts'] = $slot_product->current_products;
                }
            }
        }

        return ['dates' => $dates, 'today' => $dates_today, 'warehouse' => $warehouse, 'city' => $city];
    }

    /**
     * Retorna los rangos para cada fecha de los horarios
     *
     * @param Warehouse $warehouse
     * @param date $dateStart
     * @param date $dateEnd
     * @param int $deliveryWindowId
     * @param boolean $today
     * @return array
     */
    private function getTimeRange($warehouse, $dateStart, $dateEnd, $deliveryWindowId, $today = false)
    {
        $dates =  [];
        $dates['start'] = date('h:i A', strtotime($dateStart));
        $dates['end'] = date('h:i A', strtotime($dateEnd));
        if($today == false){
            for ($k = 0; $k < 7; $k++)
            {
                $dates['slots'][$k] = [
                    'warehouse' => $warehouse,
                    'deliveryWindowId' => $deliveryWindowId,
                    'day' => $k,
                    'currentProducts' => 0,
                    'numberProducts' => '',
                    'startTime' => $dateStart,
                    'endTime' => $dateEnd
                ];
            }
        }else{
            $dates['slots'][7] = [
                'warehouse' => $warehouse,
                'deliveryWindowId' => $deliveryWindowId,
                'day' => 7,
                'currentProducts' => 0,
                'numberProducts' => '',
                'startTime' => $dateStart,
                'endTime' => $dateEnd
            ];
        }
        return $dates;
    }
}
