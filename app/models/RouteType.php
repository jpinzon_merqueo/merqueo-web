<?php
class RouteType extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'route_types';

    public function admin() {
        return $this->belongsTo('Admin');
    }

    public function vehiclesFullRoute() {
        return $this->hasMany('Vehicle', 'full_route_type_id');
    }

    public function vehiclesHalfRoute() {
        return $this->hasMany('Vehicle', 'half_route_type_id');
    }

    public function vehiclesPartialRoute() {
        return $this->hasMany('Vehicle', 'partial_route_type_id');
    }

    public function getKey() {
        return $this->name . ' - ' . $this->cost;
    }

}