<?php

class InventoryCounting extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'inventory_counting';

    /**
     * Obtiene el usuario administrador que creó el conteo
     *
     */
    public function admin() {
      return $this->belongsTo('Admin');
    }

    public function warehouse() {
      return $this->belongsTo('Warehouse', 'warehouse_id');
    }

    public function surveyAdmin() {
      return $this->belongsTo('Admin', 'survey_admin_id');
    }

    public function inventoryCountingDetails() {
        return $this->hasMany('InventoryCountingDetail');
    }

    public function inventoryCountingSurveys() {
        return $this->hasMany('InventoryCountingSurvey');
    }

}
