<?php

class Vehicle extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'vehicles';

    public function driver() {
        return $this->belongsTo('Driver', 'driver_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function drivers(){
        return $this->hasMany('VehicleDrivers', 'vehicle_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function transporter() {
        return $this->belongsTo('Transporter', 'transporter_id');
    }

    public function vehicleNews() {
        return $this->hasMany('VehicleNew', 'vehicle_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function TrasnporterNew(){
        return $this->hasMany('TransporterNew', 'vehicle_id');
    }

    /**
     * Obtener saldos de conductor
     *
     * @param int $vehicle_id ID conductor
     * @return int Saldo de conductor, balance.
     */
    public static function getBalance($vehicle_id)
    {
        return VehicleMovement::where('vehicle_id', $vehicle_id)->where('status', 1)->sum('driver_balance');
    }

    /**
     * Función para generar alertas de de transportadores cuando
     * entrega más de 2 pedidos en menos de 15 minutos
     * @param  Order $order Objeto orders para validar fechas de entrega
     *
     * @return [type]        [description]
     */
    public function onDeliveredOrder($order)
    {
        $last_order = Order::where('vehicle_id', $this->id)
                        ->whereIn('status', ['Delivered', 'Cancelled'])
                        ->where(DB::raw('DATE(management_date)'), '=', DB::raw('DATE("'. $order->management_date .'")'))
                        ->orderBy('management_date', 'DESC')
                        ->skip(1)
                        ->first();

        if ( $last_order && !empty($order->id) ) {
            $last_order_date = Carbon\Carbon::parse($last_order->management_date);
            $order_date = Carbon\Carbon::parse($order->management_date);
            $reasons = TransporterAlert::getReasons();

            // Cierre de 2 o más pedidos en menos de 15 minutos
            if ( $order_date->diffInMinutes($last_order_date) <= 15 ) {
                $this->addAlert( $reasons[5], 'Pendiente', $order->id );
            }

            // Saltos de secuencia
            $last_order->planning_sequence++;
            if ( ($order->route_id == $last_order->route_id) && ($last_order->planning_sequence != $order->planning_sequence)  ) {
                $this->addAlert( $reasons[0], 'Pendiente', $order->id );
            }

            // Ultima entrega > 21 minutos
            if ( $order_date->diffInMinutes($last_order_date) >= 21 ) {
                $this->addAlert( $reasons[1], 'Pendiente', $order->id );
            }
        }
        return;
    }

    public function addAlert($reason, $status, $order_id = null)
    {
        if ( is_string($reason) ) {
            $new_alert = new TransporterAlert;
            $new_alert->vehicle_id = $this->id;
            $new_alert->order_id = $order_id;
            $new_alert->reason = $reason;
            $new_alert->status = $status;
            $new_alert->save();
        }elseif ( is_int($reason) ) {
            $reasons = TransporterAlert::getReasons();
            $new_alert = new TransporterAlert;
            $new_alert->vehicle_id = $this->id;
            $new_alert->order_id = $order_id;
            $new_alert->reason = $reasons[$reason];
            $new_alert->status = $status;
            $new_alert->save();
        }
        
        //$message = $new_alert->getPusherFormattedInfo();
        //send_notification($app_version, $os, $this->device_id, $this->device_id, $data);
        //Pusherer::send_push('transporter-alert-channel', 'transporter-alert-event',  $message, 'transporter' );

    }

    /**
     * Calcula el costo del vehiculo para la ruta
     * @param int $orders_number
     * @return number
     */
    public function getCost($ordersNumber)
    {
        if ($this->payment_type == 0) {
            if (Config::get('app.base_quantity_orders_for_type_route') <= $ordersNumber) {
                return $this->full_route_cost;
            } else {
                return $this->half_route_cost;
            }
        } else {
            if ($ordersNumber < $this->base_order) {
                return $this->base_cost;
            } else {
                return $this->base_cost + ($this->base_cost + $this->variable_cost);
            }
        }
    }
}
