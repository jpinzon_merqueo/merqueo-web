<?php

class Locality extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'localities';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function orderGroup()
    {
        return $this->hasMany('OrderGroup', 'locality_id');
    }
}

