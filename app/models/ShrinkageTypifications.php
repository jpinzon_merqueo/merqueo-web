<?php

class ShrinkageTypifications extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'shrinkage_typifications';
    protected $hidden = array('created_at','updated_at');

}