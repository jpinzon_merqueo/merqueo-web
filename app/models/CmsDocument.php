<?php

namespace models;

use Illuminate\Database\Eloquent\Model;

class CmsDocument extends Model
{
    protected $fillable = [
        'slug', 'title', 'content'
    ];

    public function history()
    {
        return $this->hasMany('models\CmsHistory');
    }

    public static function build($slug, $title, $content)
    {
        $instance = new CmsDocument();
        $instance->slug = $slug;
        $instance->title = $title;
        $instance->content = $content;

        return $instance;
    }
}
