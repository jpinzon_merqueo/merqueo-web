<?php

use \Illuminate\Database\Eloquent\Model;

/**
 * Class DeliveryWindow
 * @property string $hour_start
 * @property string $hour_end
 * @property City $city
 * @property string $shifts
 * @property string $delivery_window
 * @property int $delivery_time_minutes
 * @property int $status
 */
class DeliveryWindow  extends Model
{
    const SHIFT_AM = 'AM';
    const SHIFT_AM_PM = 'AM y PM';
    const SHIFT_PM = 'PM';
    const SHIFT_SAME_DAY = 'MD';
    const SHIFT_EXPRESS = 'EX';
    const SHIFT_FAST_DELIVERY = 'ER';
    const START_PLANNING = '23:00:00';
    const DELIVERY_WINDOW_EX_ID = 16;

    const SHIFTS = [
        self::SHIFT_AM,
        self::SHIFT_AM_PM,
        self::SHIFT_PM,
        self::SHIFT_SAME_DAY,
        self::SHIFT_EXPRESS,
        self::SHIFT_FAST_DELIVERY
    ];

    const SHIFTS_ONTIME = [
        self::SHIFT_SAME_DAY,
        self::SHIFT_EXPRESS,
        self::SHIFT_FAST_DELIVERY
    ];

    protected $fillable = ['hour_start', 'hour_end', 'shifts', 'status', 'delivery_window'];

    protected $table = 'delivery_windows';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo('City');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function zoneLocationSlots()
    {
        return $this->hasMany('ZoneLocationSlot');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function slotWarehouses()
    {
        return $this->hasMany('SlotWarehouse');
    }

    /**
     * @param $query
     * @param $range
     * @return mixed
     */
    public function scopeRange($query, $range)
    {
        $initial_date = date('H:i', strtotime($range[0]));
        $end_date = date('H:i', strtotime($range[1]));

        return $query->where('hour_start', $initial_date)
            ->where('hour_end', $end_date);
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeAvailable($query)
    {
        return $query->where('status', 1);
    }

    /**
     * @param $newValue
     */
    public function setShiftsAttribute($newValue)
    {
        if (!in_array($newValue, self::SHIFTS)) {
            throw new InvalidArgumentException('Available shift values: ' . implode(', ', self::SHIFTS));
        }

        $this->attributes['shifts'] = $newValue;
    }

    /**
     * Función para validar la exitencia de una franja horaria
     *
     * @param $hour_start
     * @param $hour_end
     * @param $shift
     * @param $city_id
     * @param null $id_window
     * @return array
     */
    public static function exist($hour_start, $hour_end, $shift, $city_id, $id_window = null)
    {
        if($hour_start > $hour_end){
            return [
                'status' => false,
                'message' => 'La hora de inicio no puede ser mayor a la hora finalización.'
            ];
        }

        $delivery_windows = self::select('*')
            ->where('hour_start', $hour_start)
            ->where('hour_end', $hour_end)
            ->where('city_id', $city_id);

        if(!empty($id_window)) {
            $delivery_windows->where('id', '<>', $id_window);
        }
        if($shift == 'MD') {
            $delivery_windows->where('shifts', $shift);
        }else{
            $delivery_windows->whereIn('shifts', ['AM','PM','AM y PM']);
        }
        $delivery_windows =    $delivery_windows->get();

        if(count($delivery_windows)){
            return [
                'status' => false,
                'message' => 'Ya existe una franja horaria con estos parametros.'
            ];
        }

        $hour_start_between = self::select('*')
            ->whereRaw("hour_start < '{$hour_start}' AND hour_end > '{$hour_start}'")
            ->where('city_id', $city_id);

        if(!empty($id_window)){
            $hour_start_between->where('id', '<>', $id_window);
        }
        if($shift == 'MD') {
            $hour_start_between->where('shifts', $shift);
        }else{
            $hour_start_between->whereIn('shifts', ['AM','PM','AM y PM']);
        }
        $hour_start_between = $hour_start_between->get();

        if(count($hour_start_between)){
            return [
                'status' => false,
                'message' => 'La hora de inicio se encuentra contemplada en una franja horaria existente.'
            ];
        }

        $hour_end_between = self::select('*')
            ->whereRaw("hour_start < '{$hour_end}' AND  hour_end > '{$hour_end}'")
            ->where('city_id', $city_id);

        if(!empty($id_window)){
            $hour_end_between->where('id', '<>', $id_window);
        }

        if($shift == 'MD' || $shift == 'EX') {
            $hour_end_between->where('shifts', $shift);
        }else{
            $hour_end_between->whereIn('shifts', ['AM','PM','AM y PM']);
        }
        $hour_end_between = $hour_end_between->get();

        if(count($hour_end_between)){
            return [
                'status' => false,
                'message' => 'La hora de finalización se encuentra contemplada en una franja horaria existente.'
            ];
        }

        return [ 'status'=> true ];
    }

    /**
     * Retorna las franjas horarias agrupadas por los turnos de planeación
     *
     * @param $city_id
     * @return array
     */
    public static function getDeliveryWindowsGroupByShifts($city_id)
    {
        $shifts = self::select(DB::raw("DISTINCT shifts"))
            ->where('status', 1)
            ->where('city_id', $city_id)
            ->orderBy('shifts', 'ASC')
            ->get();
        $delivery_windows  = [];
        foreach($shifts as $shift){
            $delivery_windows_by_shifts = self::where('shifts', $shift->shifts )
                ->where('status', 1)
                ->where('city_id', $city_id)
                ->orderBy('hour_start', 'ASC')
                ->get();

            if(count($delivery_windows_by_shifts)> 0){
                foreach($delivery_windows_by_shifts as $delivery_windows_by_shift){
                    if($shift->shifts == 'AM y PM'){
                        $delivery_windows['AM'][] = $delivery_windows_by_shift->id;
                        $delivery_windows['PM'][] = $delivery_windows_by_shift->id;
                    }else{
                        $delivery_windows[$shift->shifts][] = $delivery_windows_by_shift->id;
                    }
                }
            }
        }

        return $delivery_windows;
    }

    /**
     * Retorna las franjas activas por ciudad
     *
     * @param int $city_id
     * @param bool $md
     * @param bool $express
     * @return \Illuminate\Support\Collection|DeliveryWindow[]
     */
    public static function getDeliveryWindowsByCity($city_id, $md = false, $express = false)
    {
        $delivery_windows = self::select('delivery_windows.*', 'cities.city')
            ->join('cities', 'cities.id','=', 'delivery_windows.city_id')
            ->where('delivery_windows.city_id', $city_id)
            ->where('delivery_windows.status', 1)
            ->orderBy('hour_start', 'ASC');

        if ($md) {
            $delivery_windows->where('shifts', 'MD');
            if($express){
                $delivery_windows->orWhere('shifts', 'EX');
            }
        } else {
            $delivery_windows->whereIn('shifts', ['AM','PM','AM y PM']);
        }

        return $delivery_windows->get();
    }
}
