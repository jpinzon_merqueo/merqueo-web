<?php 
 
class ProviderOrderInvoice extends \Illuminate\Database\Eloquent\Model { 
 
    protected $table = 'provider_order_invoices'; 
    protected $hidden = array('created_at','updated_at'); 
 
}