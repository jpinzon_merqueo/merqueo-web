<?php

class Task extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'tasks';
    protected $hidden = array('created_at','updated_at');

}