<?php

/**
 * @property int $id
 * @property string $comment
 * @property string $status
 * @property string $created_at
 */
class OrderGroupCallcenter extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'order_group_callcenter';

    protected $fillable = [
        'status', 'comments', 'user_score'
    ];

    /**
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

    /**
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function customerService()
    {
        return $this->belongsTo(CustomerService::class);
    }

    /**
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function orderGroup()
    {
        return $this->belongsTo(OrderGroup::class, 'group_id');
    }
}
