<?php

use \Illuminate\Database\Eloquent\Builder;

/**
 * Class Banner
 * @property int $status
 * @property Store $store
 * @property string $category
 */
class Banner extends \Illuminate\Database\Eloquent\Model
{
    /**
     * {@inheritdoc}
     */
	protected $table = 'banners';

	/**
	 * @const int number position reserved
	 */
	 const NUMBER_POSITION = 6;

    /**
     * @var string
     */
	const CATEGORY_VIDEO = 'Video';

	const CATEGORY_INFORMATIVO = 'Informativo';


    public function city()
    {
      return $this->belongsTo('City', 'city_id');
    }

    public function store()
    {
      return $this->belongsTo('Store', 'store_id');
    }

    public function storeProduct()
    {
      return $this->belongsTo('StoreProduct', 'store_product_id');
    }

    public function bannerStoreProduct()
    {
      return $this->hasMany('BannerStoreProduct', 'banner_id');
    }

    public function deepLinkCity()
    {
      return $this->belongsTo('City', 'deeplink_city_id');
    }

    public function deepLinkStore() {
      return $this->belongsTo('Store', 'deeplink_store_id');
    }

    public function deepLinkDepartment()
    {
      return $this->belongsTo('Department', 'deeplink_department_id');
    }

    public function deepLinkShelf()
    {
        return $this->belongsTo('Shelf', 'deeplink_shelf_id');
    }

    public function deepLinkProduct()
    {
      return $this->belongsTo('Product', 'deeplink_product_id');
    }

    public function deepLinkStoreProduct()
    {
      return $this->belongsTo('StoreProduct', 'deeplink_store_product_id');
    }

    public function storeProductDeepLink()
    {
        return $this->belongsTo('StoreProduct', 'deeplink_store_product_id');
    }

    public function warehouses()
    {
        return $this->belongsToMany(Warehouse::class, 'banner_warehouses')
            ->withPivot('status');
    }

    public function departments()
    {
        return $this->belongsToMany(Department::class, 'banner_departments')
            ->withPivot('position');
    }

    public function bannerStoreProducts()
    {
        return $this->belongsToMany(StoreProduct::class, 'banner_store_product');
    }

    /**
     * @return bool
     */
    public function isProduct()
    {
        return $this->category === 'Producto';
    }

    /**
     * @return bool
     */
    public function isImage()
    {
        return $this->type === 'Imagen';
    }

    /**
     * @return bool
     */
    public function isInformative()
    {
        return $this->category === 'Informativo';
    }

    /**
     * @return bool
     */
    public function isDeeplink()
    {
        return $this->category === 'Deeplink';
    }

    /**
     * @return bool
     */
    public function isProductsGroup()
    {
        return $this->category === 'Conjunto de productos';
    }

    public static function boot() {
        parent::boot();

        static::updating(function ($banner) {
            if ( $banner->isDirty() ) {
                $dirty_banner = $banner->getDirty();
                $original_banner = $banner->getOriginal();
                $original = $dirty = [];
                foreach ($dirty_banner as $att => $value) {
                    $dirty[$att] = $value;
                    $original[$att] = $banner->getOriginal($att);
                }
                $changes = ['original' => $original, 'changes' => $dirty];
                $banner_log = new BannerLog;
                if ( Session::has('admin_id') ) {
                    $banner_log->admin_id = Session::get('admin_id');
                }
                $banner_log->banner_id = $banner->id;
                $banner_log->reason = json_encode($changes);
                $banner_log->save();
            }
        });
    }

    /**
     * Obtiene query de banners
     *
     * @param $query
     * @param Warehouse $warehouse
     * @param Store $store
     * @param Department|null $department
     * @param int $total_user_orders
     * @param string $platform
     * @param bool $allow_banner_query Se realiza un query que omite el filtro por home o departamento.
     * @return mixed
     */
    public function scopePublicQuery(
        $query,
        Warehouse $warehouse,
        Store $store,
        Department $department = null,
        $total_user_orders = 0,
        $platform = 'web',
        $allow_banner_query = false
    ) {
        $query = $query->validDateRange()
            ->numberOrders($total_user_orders)
            ->where('platforms', 'LIKE', "%{$platform}%")
            ->where('store_id', $store->id)
            ->whereHas('warehouses', function ($query) use ($warehouse) {
                $query->where('warehouse_id', $warehouse->id);
            })
            ->where(function ($query) use ($warehouse) {
                $query->whereIn('category', [self::CATEGORY_INFORMATIVO, self::CATEGORY_VIDEO])
                    ->orWhere(function ($query) use ($warehouse) {
                        $query->validProductRelated($warehouse);
                    });
            })
            ->orderBy('position');

        if ($allow_banner_query) {
            return $query;
        }

        if (empty($department)) {
            return $query->where('is_home', 1);
        }

        return $query->whereHas('departments', function ($query) use ($department) {
            $query->where('department_id', $department->id);
        });
    }

    /**
     * Obtiene los identificadores de productos relacionados a un banner.
     *
     * @return array
     */
    public function getRelatedStoreProductsId()
    {
        if ($this->category === 'Conjunto de productos') {
            return $this->bannerStoreProduct()
                ->lists('store_product_id');
        }

        if ($this->category === 'Producto') {
            return [$this->store_product_id];
        }

        if ($this->category === 'Deeplink' && !empty($this->deeplink_store_product_id)) {
            return [$this->deeplink_store_product_id];
        }

        return [];
    }

    /**
     * Obtiene los banners asociados a un producto en una bodega
     *
     * @param $query
     * @param StoreProductWarehouse $product_warehouse
     * @param bool $available
     * @return mixed
     */
    public function scopeStoreProductWarehouse($query, StoreProductWarehouse $product_warehouse, $available = true)
    {
        $store_product_id = $product_warehouse->store_product_id;
        $banners_to_update = [$store_product_id];
        $store_product_groups = StoreProductGroup::where('store_product_group_id', $store_product_id)
            ->with('storeProduct')
            ->get();

        foreach ($store_product_groups as $store_product_group) {
            $total_child_products = $store_product_group->storeProduct->storeProductGroup()->count();
            $query_group = $store_product_group->storeProduct->storeProductWarehouses()
                ->where('warehouse_id', $product_warehouse->warehouse_id);

            if ($available) {
                $query_group->available(false);
            }

            $total_product_available = $query_group->count();
            if ($total_product_available === $total_child_products) {
                $banners_to_update[] = $store_product_group->storeProduct->id;
            }
        }

        return $query->whereIn('category', ['Deeplink', 'Producto', 'Conjunto de productos'])
            ->where(function ($query) use ($banners_to_update) {
                $query->whereIn('store_product_id', $banners_to_update)
                    ->orWhereIn('deeplink_store_product_id', $banners_to_update)
                    ->orWhereHas('bannerStoreProducts', function ($query) use ($banners_to_update) {
                        $query->whereIn('store_product_id', $banners_to_update);
                    });
        });
    }

    /**
     * Valida fechas de visualizacion del banner
     *
     * @param $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeValidDateRange($query)
    {
        $now = Carbon\Carbon::create();

        return $query->where('show_end_date', '>', $now->toDateTimeString())
            ->where('show_start_date', '<', $now->toDateTimeString())
            ->where('status', 1);
    }

    /**
     * Valida que el producto asociado al banner tengan stock y este activo
     *
     * @param Builder $query
     * @param Warehouse $warehouse
     * @return mixed
     */
    public function scopeValidProductRelated(Builder $query, Warehouse $warehouse)
    {
        return $query->where(function ($query) use ($warehouse) {
            $query->productAvailable($warehouse)
                ->orWhere(function ($query) use ($warehouse) {
                    $query->deepLinkProductAvailable($warehouse);
                })
                ->orWhere(function ($query) use ($warehouse) {
                    $query->productGroupAvailable($warehouse);
                });
        });
    }

    /**
     * Valida que el producto asociado al banner tipo producto tenga stock y este activo
     *
     * @param $query
     * @param Warehouse $warehouse
     * @return mixed
     */
    public function scopeProductAvailable(Builder $query, Warehouse $warehouse)
    {
        return $query->where(function ($query) use ($warehouse) {
            $query->where('category', 'Producto')
                ->whereHas('storeProduct', function ($query) use ($warehouse) {
                    $query->inWarehouse($warehouse);
                });
        });
    }

    /**
     * Valida que el producto asociado al deeplink del banner tenga stock y este activo
     *
     * @param $query
     * @param Warehouse $warehouse
     * @return mixed
     */
    public function scopeDeepLinkProductAvailable(Builder $query, Warehouse $warehouse)
    {
        return $query->where(function ($query) use ($warehouse) {
            $query->where('category', 'Deeplink')
                ->where(function ($query) use ($warehouse) {
                    $query->whereNull('deeplink_store_product_id')
                        ->orWhere(function ($query) use ($warehouse) {
                            $query->whereHas('deepLinkStoreProduct', function ($query) use ($warehouse) {
                                $query->inWarehouse($warehouse);
                        });
                    });
                });
        });
    }

    /**
     * Valida que los conjuntos de productos del banner tenga stock y este activo
     *
     * @param $query
     * @param Warehouse $warehouse
     * @return mixed
     */
    public function scopeProductGroupAvailable(Builder $query, Warehouse $warehouse)
    {
        return $query->where(function ($query) use ($warehouse) {
            $query->where('category', 'Conjunto de productos')
                ->whereHas('bannerStoreProducts', function ($query) use ($warehouse) {
                    $query->inWarehouse($warehouse);
                });
        });
    }

    /**
     * @param $query
     * @param $total_user_order
     * @return mixed
     */
    public function scopeNumberOrders(Builder $query, $total_user_order)
    {
        $total_user_order = intval($total_user_order);

        return $query->whereRaw(
            "CASE
                WHEN user_orders_quantity_condition = '='
                    THEN user_orders_quantity = $total_user_order
                WHEN user_orders_quantity_condition = '<'
                    THEN $total_user_order < user_orders_quantity
                WHEN user_orders_quantity_condition = '>'
                    THEN $total_user_order > user_orders_quantity
                ELSE
                    TRUE
            END"
        );
    }
}
