<?php

use App\Services\OSRM;

class Routes extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'routes';

    const TIPIFICATION_LIST  = ['Varado','Zona insegura','Capacidad vehículo',
        'Otro','No asiste','Tarde Bodega','Descanso','Mantenimiento',
        'Despacho tarde','Posible tarde','Robo','Problema mecanico'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
        public function alliedStore() {
            return $this->belongsTo('AlliedStore');
        }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function admin() {
        return $this->belongsTo('Admin');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function adminReturn() {
        return $this->belongsTo('Admin');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city() {
        return $this->belongsTo('City');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function zone() {
        return $this->belongsTo('Zone');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function pickingGroup() {
        return $this->belongsTo('PickingGroup');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders() {
        return $this->hasMany('Order', 'route_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function routeType() {
        return $this->belongsTo('RouteType', 'route_type_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function routeCostDetails() {
        return $this->hasMany('RouteCostDetail', 'route_id');
    }

    /**
     * Funcion que trae los datos del reporte de excel para Marketplace
     * 
     * @param $cityId
     * @param $planningDate
     * @param $alliedStoreId
     * @param $shift
     * @param $orderType
     * @return mixed
     */
    public static function send_report_mail( $cityId, $planningDate, $alliedStoreId, $shift, $orderType )
    {
        $routes = self::select( DB::raw("routes.route AS 'Nombre de Ruta'"),  DB::raw("routes.picking_priority AS 'Secuencia'"),
            DB::raw("orders.id AS 'Numero de Pedido'"), DB::raw("DATE(orders.delivery_date) AS 'DIA DE ENTREGA'"),
            DB::raw("orders.delivery_time AS 'HORA'"), DB::raw("order_products.reference AS 'Referencia'"),
            DB::raw("order_products.product_name AS 'NOMBRE PRODUCTO'"), DB::raw("order_products.product_unit AS 'Unidad de medida'"),
            DB::raw("order_products.quantity AS 'Cantidad'"), DB::raw("order_products.base_price AS 'Precio'"),
            DB::raw("order_products.price AS 'Precio + IVA'"), DB::raw("( order_products.price * order_products.quantity ) AS 'TOTAL + IVA' ") )
                        ->where( 'city_id', $cityId )
                        ->join( 'orders', 'orders.route_id', '=', 'routes.id' )
                        ->join( 'order_products', 'order_products.order_id', '=', 'orders.id' )
                        //->join( 'order_product_group', 'order_products.id', '=', 'order_product_group.order_product_id' )
                        ->where( 'routes.planning_date', $planningDate )
                        ->where( 'routes.allied_store_id', $alliedStoreId )
                        ->where( 'routes.shift', $shift )
                        ->where( 'routes.status', 'Terminada' )
                        ->where( 'orders.type', $orderType )
                        ->where( 'orders.status', 'Enrutado' )
                        ->orderBy( 'picking_priority' )
                        ->get()
                        ->toArray();
        return $routes;

    }

    /**
    * Funcion que consulta la funcion de direcciones de Google Maps
    *
    * @param string $params parametros de url para enviar al servicio de  google
    * @param int $count conteo de las veces que se ha ejecutado la funcion si ocurre error
    * @return string JSON
    */
    public static function getRouteGoogleDirection($params, $count = 0)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://maps.googleapis.com/maps/api/directions/json?'.$params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        $json_response = curl_exec($ch);
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        $return = json_decode($json_response, true);
        if(empty($return) && $count < 3){
            $return = self::getRouteGoogleDirection($params, $count + 1);
        }
        return $return;
    }

    /**
    * Funcion que consulta la funcion de distance matrix de Google Maps
    *
    * @param string $params Parametros de url para enviar al servicio de  google
    * @param int $count Conteo de las veces que se ha ejecutado la funcion si ocurre error
    * @return string JSON
    */
    public static function getDistanceMatrixGoogle($params, $count = 0)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://maps.googleapis.com/maps/api/distancematrix/json?'.$params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        $json_response = curl_exec($ch);
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        $response = json_decode($json_response, true);
        if(!empty($response) && $count < 3){
            $response = self::getDistanceMatrixGoogle($params, $count+1);
        }
        return $response;
    }

    public static function getDistanceMatrix($origins, $destinations, $provider)
    {
        if ($provider) {
            $params = http_build_query([
                'origins' => $origins,
                'destinations' => trim($destinations, '|'),
                'key' => Config::get('app.planning_google_api_key'),
                'mode' => 'driving',
                'language' => 'es-Es'
            ]);
            return self::getDistanceMatrixGoogle($params);
        } else {
            $ORSM = new OSRM();
            return $ORSM->getDistanceMatrix($origins, trim($destinations, ';'));
        }
    }

    /**
    * Funcion que trae una ruta por id
    *
    * @param int $route_id
    * @return array
    */
    public static function getRoutesById($route_id)
    {
        return Order::select('orders.id', 'delivery_time', 'user_address AS address', 'user_address_neighborhood AS neighborhood',
            'user_address_latitude AS latitude', 'user_address_longitude AS longitude', 'planning_sequence','warehouses.warehouse','warehouses.id AS warehouse_id',
            DB::raw("ROUND(planning_distance / 1000, 1) AS planning_distance"),
            DB::raw("ROUND(planning_duration / 60) AS planning_duration"),
            'route_id', 'zones.name AS zone', 'route', 'routes.shift', 'routes.zone_id')
            ->join('routes', 'route_id', '=', 'routes.id')
            ->join('order_groups', 'group_id', '=', 'order_groups.id')
            ->join('zones', 'order_groups.zone_id', '=', 'zones.id')
            ->join('warehouses', 'warehouses.id', '=', 'routes.warehouse_id')
            ->join('cities', 'order_groups.user_city_id', '=', 'cities.id')
            ->where('routes.id', $route_id)
            ->orderBy('planning_sequence')
            ->get()
            ->toArray();
    }

    /**
     * Obtiene rutas por ciudad bodega dia y turno
     *
     * @param $zone_id
     * @param $planningDate
     * @param $shift
     * @param $cityId
     * @param $warehouseId
     * @return mixed
     */
    public static function getRoutesByZones($zoneId, $planningDate, $shift, $cityId, $warehouseId)
    {
        $routes = self::select('routes.*', 'cities.city',  DB::raw('COUNT(routes.id) AS routes'), 'warehouses.warehouse', 
            'warehouses.id AS warehouse_id', 'zones.maximum_orders_per_delivery_window')
            ->join('orders', 'orders.route_id', '=', 'routes.id')
            ->join('order_groups', 'orders.group_id', '=', 'order_groups.id')
            ->join('zones', 'routes.zone_id', '=', 'zones.id')
            ->join('warehouses', 'warehouses.id', '=', 'zones.warehouse_id')
            ->join('cities', 'warehouses.city_id', '=', 'cities.id')
            ->where('routes.zone_id', $zoneId)
            ->where('routes.planning_date', $planningDate )
            ->where('routes.shift', $shift)
            ->where('routes.city_id', $cityId)
            ->where('routes.warehouse_id', $warehouseId)
            ->groupBy('zones.warehouse_id')
            ->groupBy('routes.city_id')
            ->groupBy('routes.planning_date')
            ->groupBy('routes.shift')
            ->orderBy('routes.planning_date', 'desc')
            ->orderBy('routes.shift', 'desc')
            ->get();

        return $routes;
    }

    /**
     * Funcion que obtiene las rutas de una bodega en una franja horaria
     *
     * @param $planningDateStart
     * @param $planningDateEnd
     * @param $shift
     * @param $warehouseId
     * @return mixed
     */
    public static function getRoutes($planningDateStart, $planningDateEnd, $shift, $warehouseId)
    {
        $routes = self::select('routes.*')
            ->whereBetween('routes.planning_date', array($planningDateStart, $planningDateEnd) )
            ->where('routes.shift', $shift)
            ->where('routes.warehouse_id', $warehouseId)
            ->get();

        return $routes;
    }

    /**
     * Funcion que devuelve la primera orden disponible para pasar a una ruta
     *
     * @param $routeId
     * @return mixed
     */
    public static function getOrdersToRouteConstructor($routeId)
    {
        $order = Order::where('route_id', $routeId)
            ->where('status', 'Alistado')
            ->whereRaw('picking_date < planning_date')
            ->whereNull('baskets_to_route_date')
            ->orderBy('planning_sequence')
            //->toSql();
            ->get()
            ->first();

        return $order;
    }

    /**
    * Obtener la hora inicial de espacho
    */
    public function getDispatchTime()
    {
        switch ($this->shift) {
            case 'AM':
                return date('Y-m-d 05:30:00', strtotime($this->planning_date));
                break;
            case 'PM':
                return date('Y-m-d 10:30:00', strtotime($this->planning_date));
                break;
            case 'MD':
                return date('Y-m-d 16:30:00', strtotime($this->planning_date));
                break;
        }
    }

    /**
     * Obtiene los pedidos para la planeacion en una bodega
     *
     * @param $warehouseId
     * @param $planningDate
     * @return mixed
     */
    public static function getAllOrders($warehouseId, $planningDate)
    {
        return Order::select('cities.city', 'orders.delivery_window_id', 'delivery_windows.delivery_window as delivery_time',
            DB::raw('COUNT(orders.id) AS orders'), 'delivery_windows.city_id',
            DB::raw('SUM(IF(user_address_latitude = 1, 1, 0)) AS wrong_address_orders'),
            DB::raw('SUM(IF(zone_id IS NULL OR zone_id = 0, 1, 0)) AS no_zone_orders'))
            ->join('order_groups', 'order_groups.id', '=', 'orders.group_id')
            ->join('stores', 'stores.id', '=', 'orders.store_id')
            ->join('delivery_windows', 'delivery_windows.id', '=', 'orders.delivery_window_id')
            ->join('cities', 'cities.id', '=', 'delivery_windows.city_id')
            ->where(DB::raw('DATE(orders.delivery_date)'), $planningDate)
            ->whereIn('orders.status', ['Initiated', 'In Progress', 'Alistado'])
            ->whereRaw("orders.id NOT IN (SELECT orders.id
                FROM orders
                INNER JOIN order_groups ON order_groups.id = orders.group_id
                INNER JOIN order_products ON orders.id = order_products.order_id
                INNER JOIN store_products ON store_products.id = order_products.store_product_id
                INNER JOIN products ON products.id = store_products.product_id
                WHERE orders.`type` = 'Marketplace'
                    AND order_groups.warehouse_id = '".$warehouseId."'
                    AND products.size = 'Grande'
                    AND DATE(orders.delivery_date) = '".$planningDate."')")
            ->whereNull('orders.route_id')
            ->where('order_groups.warehouse_id', $warehouseId)
            ->groupBy('cities.id')
            ->groupBy('orders.delivery_window_id')
            ->orderBy('cities.id')
            ->orderBy('orders.delivery_date')
            ->get();
    }

    /**
     * Obtiene pedidos con direccion errada
     * 
     * @param $cities
     * @param $planningDate
     * @param $deliveryWindows
     * @param $warehouseId
     * @return mixed
     */
    public static function getOrdersWithWrongAddress($cities, $planningDate, $deliveryWindows, $warehouseId)
    {
        return Order::join('order_groups', 'group_id', '=', 'order_groups.id')
            ->whereIn('order_groups.user_city_id', $cities)
            ->whereRaw("DATE(orders.delivery_date) = '".$planningDate."'")
            ->whereIn('orders.delivery_window_id', $deliveryWindows)
            ->whereIn('orders.status', ['Initiated', 'In Progress', 'Alistado'])
            ->whereRaw("orders.id NOT IN (SELECT orders.id
                FROM orders
                INNER JOIN order_groups ON order_groups.id = orders.group_id
                INNER JOIN order_products ON orders.id = order_products.order_id
                INNER JOIN store_products ON store_products.id = order_products.store_product_id
                INNER JOIN products ON products.id = store_products.product_id
                WHERE orders.`type` = 'Marketplace'
                    AND order_groups.warehouse_id = '".$warehouseId."'
                    AND products.size = 'Grande'
                    AND DATE(orders.delivery_date) = '".$planningDate."')")
            ->where('order_groups.user_address_latitude', 1)
            ->where('order_groups.warehouse_id', $warehouseId)
            ->whereNull('orders.route_id')
            ->groupBy('orders.id')
            ->orderBy('products_quantity', 'desc')
            ->count();
    }

    /**
     * Obtiene pedidos sin zona asignada
     * 
     * @param $cities
     * @param $planningDate
     * @param $deliveryWindows
     * @param $warehouseId
     * @return mixed
     */
    public static function getOrdersWithWrongZone($cities, $planningDate, $deliveryWindows, $warehouseId)
    {
        return Order::join('order_groups', 'group_id', '=', 'order_groups.id')
            ->whereIn('order_groups.user_city_id', $cities)
            ->whereRaw("DATE(orders.delivery_date) = '".$planningDate."'")
            ->whereIn('orders.delivery_window_id', $deliveryWindows)
            ->whereIn('orders.status', ['Initiated', 'In Progress', 'Alistado'])
            ->whereRaw("orders.id NOT IN (SELECT orders.id
                                    FROM orders
                                    INNER JOIN order_groups ON order_groups.id = orders.group_id
                                    INNER JOIN order_products ON orders.id = order_products.order_id
                                    INNER JOIN store_products ON store_products.id = order_products.store_product_id
                                    INNER JOIN products ON products.id = store_products.product_id
                                    WHERE orders.`type` = 'Marketplace'
                                        AND order_groups.warehouse_id = '".$warehouseId."'
                                        AND products.size = 'Grande'
                                        AND DATE(orders.delivery_date) = '".$planningDate."')")
            ->whereNull('zone_id')
            ->where('order_groups.warehouse_id', $warehouseId)
            ->whereNull('orders.route_id')
            ->groupBy('orders.id')
            ->orderBy('products_quantity', 'desc')
            ->count();
    }

    /**
     * Obtiene ids de pedidos por zona
     * 
     * @param $shift
     * @param $zone
     * @param $warehouseId
     * @param $planningDate
     * @return array
     */
    public static function getOrdersIdByZones($shift, $zone, $warehouseId, $planningDate)
    {
        $shiftDeliveryWindows = DeliveryWindow::getDeliveryWindowsGroupByShifts( $zone->city_id );
        $shiftDeliveryWindows = $shiftDeliveryWindows[$shift];

        $orderIds = [];

        foreach($shiftDeliveryWindows as $deliveryTimeId)
        {

            $orders = Order::select('orders.id')
                ->join('order_groups', 'order_groups.id', '=', 'orders.group_id')
                ->join('delivery_windows', 'delivery_windows.id', '=', 'orders.delivery_window_id')
                ->join('cities', 'cities.id', '=', 'delivery_windows.city_id')
                ->join('zones', 'order_groups.zone_id', '=', 'zones.id')
                ->whereNull('orders.route_id')
                ->whereRaw("orders.id NOT IN (SELECT orders.id
                    FROM orders
                    INNER JOIN order_groups ON group_id = order_groups.id
                    INNER JOIN order_products ON orders.id = order_products.order_id
                    INNER JOIN store_products ON store_products.id = order_products.store_product_id
                    INNER JOIN products ON products.id = store_products.product_id
                    WHERE orders.`type` = 'Marketplace'
                        AND order_groups.warehouse_id = ".$warehouseId."
                        AND products.size = 'Grande'
                        AND DATE(orders.delivery_date) = '".$planningDate."')")
                ->whereIn('orders.status', ['Initiated', 'In Progress', 'Alistado'])
                ->where('order_groups.warehouse_id', $warehouseId)
                ->where('order_groups.zone_id', $zone->id)
                ->whereRaw("DATE(orders.delivery_date) = '".$planningDate."'")
                ->where('orders.delivery_window_id', $deliveryTimeId)
                ->get()
                ->toArray();

            if ($orders){
                $deliveryWindows = DeliveryWindow::find($deliveryTimeId);

                if ( $deliveryWindows->shifts == 'AM y PM' && $shift == 'AM' ){
                    $n = count($orders);
                    if ($n > 1){
                        $n = round($n / 2);
                        $orders = array_slice($orders, 0, $n );
                    }
                }

                foreach($orders as $order)
                    $orderIds[] = $order['id'];
            }
        }

        return $orderIds;
    }

    /**
     * Obtiene pedidos por zona
     * 
     * @param $zone
     * @param $warehouseId
     * @param $planningDate
     * @param $shift
     * @return mixed
     */
    public static function getOrderByZone($zone, $warehouseId, $planningDate, $shift)
    {
        $orderIds = self::getOrdersIdByZones($shift, $zone, $warehouseId, $planningDate);

        return Order::select(
            'orders.id',
            'orders.delivery_window_id',
            'user_address AS address',
            'user_address_neighborhood AS neighborhood',
            'user_city_id',
            'warehouses.latitude AS warehouses_latitude',
            'warehouses.longitude AS warehouses_longitude',
            'user_address_latitude AS latitude',
            'user_address_longitude AS longitude',
            'planning_sequence',
            'zone_id',
            'zones.name AS zone',
            'management_date',
            'delivery_windows.hour_start',
            'delivery_windows.delivery_window as delivery_time',
            DB::raw("(6371 * ACOS(
                COS(RADIANS(".$zone->latitude."))
                * COS(RADIANS(user_address_latitude))
                * COS(RADIANS(user_address_longitude)
                - RADIANS(".$zone->longitude."))
                + SIN(RADIANS(".$zone->latitude."))
                * SIN(RADIANS(user_address_latitude))
                   )) AS distance"))
            ->join('order_groups', 'group_id', '=', 'order_groups.id')
            ->join('zones', 'order_groups.zone_id', '=', 'zones.id')
            ->join('warehouses', 'warehouses.id', '=', 'order_groups.warehouse_id')
            ->join('cities', 'order_groups.user_city_id', '=', 'cities.id')
            ->join('delivery_windows', 'delivery_windows.id', '=', 'orders.delivery_window_id')
            ->where('order_groups.zone_id', $zone->id)
            ->whereNull('orders.route_id')
            ->whereIn('orders.status', ['Initiated', 'In Progress', 'Alistado'])
            ->whereRaw("orders.id NOT IN (SELECT orders.id
                    FROM orders
                    INNER JOIN order_groups ON group_id = order_groups.id
                    INNER JOIN order_products ON orders.id = order_products.order_id
                    INNER JOIN store_products ON store_products.id = order_products.store_product_id
                    INNER JOIN products ON products.id = store_products.product_id
                    WHERE orders.`type` = 'Marketplace'
                        AND order_groups.warehouse_id = ".$warehouseId."
                        AND products.size = 'Grande'
                        AND DATE(orders.delivery_date) = '".$planningDate."')")
            ->whereIn('orders.id', $orderIds)
            ->where('order_groups.warehouse_id', $warehouseId)
            ->whereRaw("DATE(orders.delivery_date) = '".$planningDate."'")
            ->orderBy('orders.delivery_date')
            ->orderBy('distance')
            ->get()
            ->toArray();
    }

    /**
     * Divide una ruta
     *
     * @param $routeId
     * @param $input
     * @return \Illuminate\Http\JsonResponse
     */
    public static function storeRouteDivision($routeId,$input)
    {
        $type = $input['divide_route_type'];
        try {
            $orders_ids = $input['order'];
            if (count($orders_ids)) {
                $route  = Routes::findOrFail($routeId);
                $oderFirst = Order::where('route_id',$routeId)->where('status','!=','Cancelled')->first();

                $vehicleID = $input['divide_route_vehicle_id'];
                if ($oderFirst->vehicle_id == $vehicleID) {
                    return Response::json([
                        'status' => 0,
                        'message' => 'El vehículo debe ser diferente.',
                    ]);
                }

                $vehicleDay = Routes::whereHas('orders', function ($query) use ($vehicleID) {
                    $query->where('vehicle_id', $vehicleID)
                        ->whereNotIn('status', ['Delivered','Cancelled']);

                })->where('shift', $route->shift)
                    ->where(DB::raw('DATE(planning_date)'), '=', \Carbon\Carbon::now()->toDateString())
                    ->get();

                if (count($vehicleDay)) {
                    return Response::json([
                        'status' => 0,
                        'message' => 'El vehículo ya tiene una ruta para la misma zona horaria.',
                    ]);
                }
                $routeCopy = $route->replicate();
                $routeCopy->route = $routeCopy->route . " - {$type}";
                $routeCopy->parent_id = $routeId;
                $routeCopy->route_division = $type;
                $routeCopy->route_division_reason = $input['route_division_reason'];

                $routeCopy->push();
                $update = [
                    'route_id'   => $routeCopy->id,
                    'driver_id'  => $input['divide_route_driver_id'],
                    'vehicle_id' => $vehicleID,
                ];
                $updated = Order::whereIn('id', $orders_ids)
                    ->where('route_id', $routeId)
                    ->update($update);
                return Response::json([
                    'status' => 1,
                    'message' => 'Se creó la ruta ' . $routeCopy->route . " con id: {$routeCopy->id}",
                ]);
            }
            return Response::json([
                'status' => 0,
                'message' => 'No se seleccionaron pedidos para ser reasignadas.',
            ]);
        } catch (Exception $e) {
            return Response::json([
                'status' => 0,
                'message' => 'Ruta no encontrada.'
            ]);
        }
    }

    /**
     * Obtiene ruta y pedidos de ruta
     *
     * @param $routeId
     * @return \Illuminate\Http\JsonResponse
     */
    public static function getRouteDivision($routeId)
    {
        try {
            $route  = Routes::findOrFail($routeId);
            $orders = $route->orders()
                ->orderBy('planning_sequence')
                ->get();
            $orders_count = $orders->count();
            $ordersCountValid   = 0;
            $ordersCountInvalid = 0;
            $orders->each(function($order) use (&$ordersCountInvalid, &$ordersCountValid) {
                $validStatus   = ['Alistado', 'Dispatched', 'Delivered', 'Cancelled'];
                $invalidStatus = ['Validation', 'Initiated', 'Enrutado', 'In Progress'];
                if (in_array($order->status, $validStatus)) {
                    $ordersCountValid++;
                }
                if (in_array($order->status, $invalidStatus)) {
                    $ordersCountInvalid++;
                }
            });
            if (!$orders->count()) {
                return Response::json([
                    'status' => 0,
                    'message' => 'No se encontraron pedidos asociados a esta ruta',
                ]);
            }
            if ($ordersCountInvalid) {
                return Response::json([
                    'status' => 0,
                    'message' => 'La ruta tiene pedidos en estado diferente a Alistado, Despachado, Entregado o Cancelado.',
                ]);
            }
            if (($ordersCountValid + $ordersCountInvalid) != $orders_count) {
                return Response::json([
                    'status' => 0,
                    'message' => 'La ruta tiene pedidos no gestionadas.',
                ]);
            }
            $hasVehicle  = TRUE;
            $hasDriver   = TRUE;
            $orders = $orders->filter(function($order) use (&$hasVehicle, &$hasDriver) {
                if ($order->status != 'Cancelled') {
                    if (empty($order->driver_id)) {
                        $hasDriver = FALSE;
                    }
                    if (empty($order->vehicle_id)) {
                        $hasVehicle = FALSE;
                    }
                }
                return $order->status == 'Alistado' || $order->status == 'Dispatched';
            });
            if (!$orders->count()) {
                return Response::json([
                    'status' => 0,
                    'message' => 'La ruta no tiene pedidos en estado Despachado o Alistado para ser reasignada a una nueva ruta.',
                ]);
            }
            if (!$hasDriver) {
                return Response::json([
                    'status' => 0,
                    'message' => 'Se le debe asignar el conductor a todas las pedidos asociadas a la ruta.',
                ]);
            }
            if (!$hasVehicle) {
                return Response::json([
                    'status' => 0,
                    'message' => 'Se le debe asignar un vehículo a todas las pedidos asociadas a la ruta.',
                ]);
            }
            $orders->each(function($order) {
                $order->load('orderGroup');
            });
            unset($route->orders);
            return Response::json([
                'status' => 1,
                'route'  => $route,
                'orders' => $orders->toArray(),
            ]);
        } catch (Exception $e) {
            return Response::json([
                'status' => 0,
                'message' => 'Ruta no encontrada.'
            ]);
        }
    }

    /**
     * Crea y retorna la ruta para los pedidos creados en el convenio con Domicilios.com
     * @param $warehouse
     * @param $zone
     * @param $shift
     * @return Routes
     */
    public static function getCurrentDarksupermarketRoute(
        $warehouse,
        $zone,
        $shift = DeliveryWindow::SHIFT_EXPRESS,
        $userType = null
    ) {
        $date = \Carbon\Carbon::now()->format('Y-m-d');
        $route = self::where('city_id', $warehouse->city_id)
            ->where('planning_date', $date)
            ->where('shift', $shift)
            ->where('warehouse_id', $warehouse->id)
            ->get();

        if (!count($route)) {
            $pickingGroup = PickingGroup::Where('warehouse_id', $warehouse->id)
                ->get()
                ->first();

            if (in_array($userType, User::DARKSUPERMARKET_TYPE)) {
                $pickingGroup = PickingGroup::Where('warehouse_id', $warehouse->id)
                    ->where('color', PickingGroup::EXPRESS_COLOR)
                    ->get()
                    ->first();
            }

            $route = new self();
            $route->planning_date = $date;
            $route->shift = $shift;
            $route->warehouse_id = $warehouse->id;
            $route->city_id = $warehouse->city_id;
            $route->status = 'Terminada';
            $route->admin_id = Config::get('app.dashboard_admin_id');
            $route->zone_id = $zone->id;
            $route->cont = 1;
            $route->picking_group_id = $pickingGroup->id;
            $route->warehouse_id = $warehouse->id;
            $route->picking_priority = self::where('city_id', $warehouse->city_id)
                    ->where('planning_date', $date)
                    ->where('shift', $shift)
                    ->max('picking_priority')+1;
            $route->route = sprintf('%s %s %s %s %s 1',
                date('d/m/Y', strtotime($date)),
                strtoupper(substr($warehouse->city->city, 0, 3)),
                $shift,
                $route->picking_priority,
                $zone->name
            );
            $route->save();
            return $route;
        }
        return $route->first();
    }

}
