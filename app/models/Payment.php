<?php

/**
 * Clase para pagos con tarjetas de crédito
 */
class Payment extends Eloquent
{
	private $url;
	private $api_key;

	/**
	 * Constructor
	 */
	public function __construct()
    {
    	$this->url = Config::get('app.tpaga.url');
        $this->api_key = Config::get('app.tpaga.api_key');
	}

    /**
	 * Crea un cliente
	 *
	 * @param array $data Datos del cliente
	 * @return array $response Respuesta del servicio
	 */
    public function createCustomer($data)
    {
    	$request = array(
			'firstName' => $data['first_name'],
			'lastName' => $data['last_name'],
			'phone' => $data['phone'],
			'email' => isset($data['email']) ? $data['email'] : Config::get('app.admin_email'),
            //'email' => Config::get('app.admin_email'),
			'address' => array(
				'addressLine1' => $data['address'],
				'addressLine2' => $data['address_further'],
				'postalCode' => '11001000',
				'city' => array(
					'name' => 'Bogotá',
					'state' => 'DC',
					'country' => 'CO'
				)
			)
		);

		$request = json_encode($request);
		//header('Content-Type: application/json'); echo $request; exit;

		$options = array(
		    'http' => array(
		        'header'  => array(
		        	'Authorization: Basic '.base64_encode($this->api_key.':'),
		        	'Content-type: application/json; charset=UTF-8'
				),
		        'method'  => 'POST',
		        'content' => $request,
        		'ignore_errors' => true
		    ),
		);
		$context = stream_context_create($options);
		$url = $this->url.'/customer';
		$response = file_get_contents($url, false, $context);
		if (!$response) $response = $http_response_header[0];
		$http_response_code = substr($http_response_header[0], 9, 3);

		//guardar log
		$log = new OrderPaymentLog;
		$log->user_id = $data['user_id'];
		$log->method = __FUNCTION__;
		$log->url = $url;
		$log->response_http_code = $http_response_code;
		$log->request = $request;
		$log->response = $response;
		$log->ip = get_ip();
		$log->created_at = $log->updated_at = date('Y-m-d H:i:s');
		$log->save();

		if ($http_response_code != '201'){
			$response = array(
	    		'status' => 0,
	    		'message' => $http_response_header[0],
	    		'response' => json_decode($response)
			);
		}else{
			$response = array(
	    		'status' => 1,
	    		'message' => 'Cliente creado con éxito',
	    		'response' => json_decode($response)
			);
		}

        return $response;
    }

	/**
	 * Asocia una tarjeta de crédito a un cliente
	 *
	 * @param array $data Datos de la tarjeta de crédito
	 * @return array $response Respuesta del servicio
	 */
    public function associateCreditCard($data)
    {
    	$request_data = array(
			'primaryAccountNumber' => $data['number_cc'],
			'expirationMonth' => $data['expiration_month_cc'],
			'expirationYear' => $data['expiration_year_cc'],
			'cardVerificationCode' => $data['code_cc'],
			'cardHolderName' => $data['name_cc'],
			'billingAddress' => array(
				'addressLine1' => $data['address'],
				'addressLine2' => $data['address_further'],
				'postalCode' => '11001000',
				'city' => array(
					'name' => 'Bogotá',
					'state' => 'DC',
					'country' => 'CO'
				)
			)
		);

		$request = json_encode($request_data);
		//header('Content-Type: application/json'); echo $request; exit;

		$options = array(
		    'http' => array(
		        'header'  => array(
		        	'Authorization: Basic '.base64_encode($this->api_key.':'),
		        	'Content-type: application/json; charset=UTF-8'
				),
		        'method'  => 'POST',
		        'content' => $request,
        		'ignore_errors' => true
		    ),
		);
		$context = stream_context_create($options);
		$url = $this->url.'/customer/'.$data['customer_token'].'/credit_card';
		$response = file_get_contents($url, false, $context);
		if (!$response) $response = $http_response_header[0];
		$http_response_code = substr($http_response_header[0], 9, 3);

		if ($http_response_code != '201'){

            if (DB::transactionLevel() == 1)
                DB::rollback();

			$response_result = array(
	    		'status' => 0,
	    		'message' => $http_response_header[0],
	    		'response' => json_decode($response)
			);
		}else{
			$response_result = array(
	    		'status' => 1,
	    		'message' => 'Tarjeta de crédito asociada con éxito',
	    		'response' => json_decode($response)
			);
		}

        //guardar log
        $log = new OrderPaymentLog;
        $log->user_id = $data['user_id'];
        $log->method = __FUNCTION__;
        $log->url = $url;
        $log->response_http_code = $http_response_code;
        $request_data['primaryAccountNumber'] = substr($request_data['primaryAccountNumber'], 0, 6).'******'.substr($request_data['primaryAccountNumber'], 12, 16);
        $request_data['cardVerificationCode'] = '***';
        $log->request = json_encode($request_data);
        $log->response = $response;
        $log->ip = get_ip();
        $log->created_at = $log->updated_at = date('Y-m-d H:i:s');
        $log->save();

        return $response_result;
    }

	/**
	 * Registra cargo a la tarjeta de crédito
	 *
	 * @param array $data Datos de la tarjeta de crédito
	 * @return array $response Respuesta del servicio
	 */
    public function createCreditCardCharge($data)
    {
		$request = array(
			'amount' => $data['total_amount'],
			'taxAmount' => $data['tax_amount'],
			'currency' => 'COP',
			'creditCard' => $data['card_token'],
			'installments' => $data['installments_cc'],
			'orderId' => $data['order_id'],
			'description' => $data['description'],
			'thirdPartyId' => $data['nit']
		);

		$request = json_encode($request);
		//header('Content-Type: application/json'); echo $request; exit;

		$options = array(
		    'http' => array(
		        'header'  => array(
		        	'Authorization: Basic '.base64_encode($this->api_key.':'),
		        	'Content-type: application/json; charset=UTF-8'
				),
		        'method' => 'POST',
		        'content' => $request,
        		'ignore_errors' => true
		    ),
		);
		$context = stream_context_create($options);
		$url = $this->url.'/charge/credit_card';
		$response = file_get_contents($url, false, $context);
		if (!$response) $response = $http_response_header[0];
		$http_response_code = substr($http_response_header[0], 9, 3);

		//guardar log
		$log = new OrderPaymentLog;
		$log->order_id = $data['order_id'];
		$log->user_id = $data['user_id'];
		$log->method = __FUNCTION__;
		$log->url = $url;
		$log->response_http_code = $http_response_code;
		$log->request = $request;
		$log->response = $response;
		$log->ip = get_ip();
		$log->created_at = $log->updated_at = date('Y-m-d H:i:s');
		$log->save();

		if ($http_response_code != '201'){
			$response = array(
	    		'status' => 0,
	    		'message' => $http_response_header[0],
	    		'response' => json_decode($response)
			);
		}else{
			$response = array(
	    		'status' => 1,
	    		'message' => 'Cargo realizado con éxito',
	    		'response' => json_decode($response)
			);
		}

        return $response;
    }

	/**
	 * Elimina una tarjeta de crédito
	 *
	 * @param array $user_id ID del usuario
	 * @param array $customer_token Token del cliente
	 * @param array $card_token Token de la tarjeta de crédito
	 * @return array $response Respuesta del servicio
	 */
    public function deleteCreditCard($user_id, $customer_token, $card_token)
    {
    	$request = 'No aplica';
    	$options = array(
		    'http' => array(
		        'header'  => array(
		        	'Authorization: Basic '.base64_encode($this->api_key.':'),
		        	'Content-type: application/json; charset=UTF-8'
				),
		        'method' => 'DELETE',
		        'content' => $request,
        		'ignore_errors' => true
		    ),
		);
		$context = stream_context_create($options);
		$url = $this->url.'/customer/'.$customer_token.'/credit_card/'.$card_token;
		$response = file_get_contents($url, false, $context);
		if (!$response) $response = $http_response_header[0];
		$http_response_code = substr($http_response_header[0], 9, 3);

		//guardar log
		$log = new OrderPaymentLog;
		$log->user_id = $user_id;
		$log->method = __FUNCTION__;
		$log->url = $url;
		$log->response_http_code = $http_response_code;
		$log->request = $request;
		$log->response = $response;
		$log->ip = get_ip();
		$log->created_at = $log->updated_at = date('Y-m-d H:i:s');
		$log->save();

		if ($http_response_code != '204'){
			$response = array(
	    		'status' => 0,
	    		'message' => $http_response_header[0],
	    		'response' => json_decode($response)
			);
		}else{
			$response = array(
	    		'status' => 1,
	    		'message' => 'Tarjeta eliminada con éxito',
	    		'response' => json_decode($response)
			);
		}

        return $response;
    }

	/**
	 * Registra reembolso a la tarjeta de crédito
	 *
	 * @param int $user_id ID del usuario
	 * @param array $credit_card_charge_id ID del cargo de la tarjeta de crédito
	 * @param int $order_id ID del pedido
	 * @return array $response Respuesta del servicio
	 */
    public function refundCreditCardCharge($user_id, $credit_card_charge_id, $order_id = null)
    {
		$request = array(
			'id' => $credit_card_charge_id
		);

		$request = json_encode($request);
		//header('Content-Type: application/json'); echo $request; exit;

		$options = array(
		    'http' => array(
		        'header'  => array(
		        	'Authorization: Basic '.base64_encode($this->api_key.':'),
		        	'Content-type: application/json; charset=UTF-8'
				),
		        'method' => 'POST',
		        'content' => $request,
        		'ignore_errors' => true
		    ),
		);
		$context = stream_context_create($options);
		$url = $this->url.'/refund/credit_card';
		$response = file_get_contents($url, false, $context);
		if (!$response) $response = $http_response_header[0];
		$http_response_code = substr($http_response_header[0], 9, 3);

		//guardar log
		$log = new OrderPaymentLog;
		if ($order_id)
			$log->order_id = $order_id;
		$log->user_id = $user_id;
		$log->method = __FUNCTION__;
		$log->url = $url;
		$log->response_http_code = $http_response_code;
		$log->request = $request;
		$log->response = $response;
		$log->ip = get_ip();
		$log->created_at = $log->updated_at = date('Y-m-d H:i:s');
		$log->save();

		if ($http_response_code != '202'){
			$response = array(
	    		'status' => 0,
	    		'message' => $http_response_header[0],
	    		'response' => json_decode($response)
			);
		}else{
			$response = array(
	    		'status' => 1,
	    		'message' => 'Reembolso realizado con éxito',
	    		'response' => json_decode($response)
			);
		}

        return $response;
    }

    /**
     * Obtiene informacion de tarjeta de crédito
     *
     * @param string $customer_token ID del cliente
     * @param string $credit_card_id ID de la tarjeta de crédito
     * @param string $user_id ID del usuario
     * @return array $response Respuesta del servicio
     */
    public function retriveCreditCard($customer_token, $credit_card_id, $user_id = null)
    {
        $request = 'No aplica';

        $options = array(
            'http' => array(
                'header'  => array(
                    'Authorization: Basic '.base64_encode($this->api_key.':'),
                ),
                'method' => 'GET',
                'ignore_errors' => true
            ),
        );
        $context = stream_context_create($options);
        $url = $this->url.'/customer/'.$customer_token.'/credit_card/'.$credit_card_id;
        $response = file_get_contents($url, false, $context);
        if (!$response) $response = $http_response_header[0];
        $http_response_code = substr($http_response_header[0], 9, 3);

        //guardar log
        $log = new OrderPaymentLog;
        $log->user_id = $user_id;
        $log->method = __FUNCTION__;
        $log->url = $url;
        $log->response_http_code = $http_response_code;
        $log->request = $request;
        $log->response = $response;
        $log->ip = get_ip();
        $log->created_at = $log->updated_at = date('Y-m-d H:i:s');
        $log->save();

        if ($http_response_code != '200'){
            $response = array(
                'status' => 0,
                'message' => $http_response_header[0],
                'response' => json_decode($response)
            );
        }else{
            $response = array(
                'status' => 1,
                'message' => 'Tarjeta de crédito obtenida con éxito',
                'response' => json_decode($response)
            );
        }

        return $response;
    }

     /**
     * Obtiene informacion del banco de la tarjeta
     *
     * @param int $bin BIN de la tarjeta
     * @param array $post_data Datos POST del pedido
     * @return array $response Respuesta del servicio
     */
    public function getCreditCardInfo($bin, $post_data)
    {
        if (empty($bin))
            return false;

        try{

            $response = new stdClass;

            $url = 'https://binlist.net/json/'.$bin;

            $options = array(
                'http' => array(
                    'method'  => 'GET',
                    'ignore_errors' => true
                ),
            );
            $context = stream_context_create($options);
            $data = file_get_contents($url, false, $context);
            $http_response_code = substr($http_response_header[8], 9, 3);
            if ($http_response_code == 200)
            {
                $bin_data = json_decode($data);
                $foreign_allowed_bins = array(377813, 532004, 513689);
                if (!in_array($bin, $foreign_allowed_bins))
                    $response->is_valid = strstr($bin_data->country->name, 'Colombia') ? true : false;
                else $response->is_valid = true;
                if (Config::get('app.debug'))
                    $response->is_valid = $bin == '411111' ? true : false;
                $response->country_name = $bin_data->country->name;
            }else{
                $response->is_valid = true;
                $response->country_name = 'Validar manualmente';
            }

            //guardar log
            $log = new BinLog;
            $log->user_id = $post_data['user_id'];
            $log->url = $url;
            $log->response_http_code = $http_response_code;
            $log->response = $data;
            $log->ip = get_ip();
            $log->created_at = $log->updated_at = date('Y-m-d H:i:s');
            $log->save();

            return $response;
        }catch(Exception $e){

            $response->is_valid = true;
            $response->country_name = 'Validar manualmente';

            return $response;
        }
    }

}