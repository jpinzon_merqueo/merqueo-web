<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentMethod extends Model
{
    const DATAFONO = 'Datáfono';
    const TERMINAL = 'Terminal';
}
