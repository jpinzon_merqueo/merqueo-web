<?php

class Provider extends \Illuminate\Database\Eloquent\Model
{
    const MERCARDERIA = 7;

    protected $table = 'providers';

    /**
     * @return mixed
     */
    public function contacts()
    {
        return $this->hasMany(ProviderContact::class);
    }

    public function providerOrders()
    {
        return $this->hasMany(ProviderOrder::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo('City', 'city_id');
    }

    /**
     * Crea el log de proveedores despues de guardarlo
    */
    public static function boot() {
        parent::boot();

        static::updating(function ($provider) {
            if ($provider->isDirty()) {
                $provider_original = $provider->getOriginal();

                $provider_log = new ProviderLog;
                $provider_log->provider_id = $provider->id;
                $provider_log->admin_id = Session::get('admin_id');
                $provider_log->delivery_time_hours_original = $provider_original['delivery_time_hours'];
                $provider_log->delivery_time_hours_change = $provider->delivery_time_hours;
                $provider_log->financial_condition_original = $provider_original['financial_condition'];
                $provider_log->financial_condition_change = $provider->financial_condition;
                $provider_log->credit_limit_original = $provider_original['credit_limit'];
                $provider_log->credit_limit_change = $provider->credit_limit;
                $provider_log->payment_days_original = $provider_original['payment_days'];
                $provider_log->payment_days_change = $provider->payment_days;

                $provider_log->save();
            }
        });
    }
}
