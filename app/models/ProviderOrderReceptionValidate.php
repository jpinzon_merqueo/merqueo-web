<?php

class ProviderOrderReceptionValidate extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'provider_order_reception_validates';
    public $type_list = ['Error de auxiliar','Error de proveedor','Error de recibo virtual'];
    public $tipification_list = ['Error de auxiliar','Error de proveedor','Error de recibo virtual'];

    public function providerOrderReceptionDetail()
    {
    	return $this->belongsTo('ProviderOrderReceptionDetail', 'reception_detail_id');
    }

    public function providerOrderReceptionDetailProductGroup()
    {
    	return $this->belongsTo('ProviderOrderReceptionDetailProductGroup', 'reception_detail_grouped_id');
    }

}