<?php

namespace coupon;

use exceptions\CouponException;
use DB;
use Order;
use User;
use UserCoupon;

/**
 * Class CouponValidator
 * @package coupon
 */
class CouponValidator
{
    /**
     * @var \Coupon
     */
    protected $coupon;

    public function __construct(\Coupon $coupon)
    {
        $this->coupon = $coupon;
    }

    /**
     * Valida que el cupón se pueda redimir
     *
     * @param Order $order
     * @throws CouponException
     */
    public function validateAgainstOrder(Order $order)
    {
        if (empty($order->user)) {
            throw new CouponException('El usuario asociado con el pedido no es valido.');
        }

        $user = $order->user;

        if (!$this->coupon->status) {
            throw new CouponException('El cupón ya no esta activo.');
        }

        if (date('Y-m-d') > $this->coupon->expiration_date) {
            throw new CouponException('El cupón ya no esta vigente.');
        }

        $this->validateUses($user);
        $this->validateTypeUse($user, $order);
        $this->validateCampaign($user);
        $this->validateOnSpecificStore($order);
        $this->validateAmount($order->total_amount, $order->payment_method);
    }

    /**
     * Valida que el cupón no se haya usado más veces de lo permitido
     *
     * @param User $user
     * @throws CouponException
     */
    private function validateUses(User $user)
    {
        $uses = UserCoupon::where('coupon_id', $this->coupon->id)->count();

        if ($uses >= $this->coupon->number_uses) {
            throw new CouponException('El cupón ya fue utilizado.');
        }

        $used_coupon = UserCoupon::where('user_id', $user->id)
            ->where('coupon_id', $this->coupon->id)
            ->count();

        if ($used_coupon > 0) {
            throw new CouponException('Ya redimiste este cupón.');
        }
    }

    /**
     * Valida el tipo de uso del cupón
     *
     * @param User $user
     * @param Order|null $order
     * @return bool
     * @throws CouponException
     */
    private function validateTypeUse(User $user, Order $order = null)
    {
        if ($this->coupon->type_use !== 'Only The First Order') {
            return true;
        }

        $count_orders = Order::where('user_id', $user->id)->where('status', '<>', 'Cancelled');

        if (!empty($order)) {
            $count_orders->where('id', '<>', $order->id);
        }

        $count_orders = $count_orders->count();

        if ($count_orders) {
            throw new CouponException('Este cupón solo podias utilizarlo en tu primera compra.');
        }

        if ($user->created_at < date('Y-m-d H:i:s') && $this->coupon->type_use == 'Only New Customers') {
            $has_orders = Order::where('user_id', $user->id);

            if (!empty($order)) {
                $has_orders->where('id', '<>', $order->id);
            }

            $has_orders = $has_orders->count() > 0;

            if ($has_orders) {
                throw new CouponException('Este cupón solo podias utilizarlo al crear tu cuenta de Merqueo.');
            }
        }
    }

    /**
     * Valida la campaña donde se redime el cupón.
     *
     * @param User $user
     * @return bool
     * @throws CouponException
     */
    private function validateCampaign(User $user)
    {
        if (!empty($this->coupon->campaign_validation)) {
            return true;
        }

        $campaign = \Coupon::select('coupons.id')
            ->join('user_coupons', function ($join) use ($user) {
                $join->on('user_coupons.coupon_id', '=', 'coupons.id');
                $join->on('user_coupons.user_id', '=', DB::raw($user->id));
            })
            ->where('coupons.campaign_validation', $this->coupon->campaign_validation)
            ->first();

        if ($campaign) {
            throw new CouponException('Ya usaste un cupón de esta campaña.');
        }
    }

    /**
     * Valida que los productos del pedido cumplan los requisitos del cupón
     *
     * @param Order $order
     * @return null
     * @throws CouponException
     */
    private function validateOnSpecificStore(Order $order)
    {
        if ($this->coupon->redeem_on !== 'Specific Store') {
            return null;
        }

        $order_products = \OrderProduct::select(
            'products.id', 'store_products.department_id', 'store_products.shelf_id', 'store_products.store_id',
            'order_products.price', 'order_products.quantity AS cart_quantity'
        )
            ->join('store_products', 'store_products.id', '=', 'order_products.store_product_id')
            ->join('shelves', 'shelves.id', '=', 'store_products.shelf_id')
            ->join('departments', 'departments.id', '=', 'shelves.department_id')
            ->where('order_id', $order->id)
            ->get();

        $discount_amount = 0;
        foreach ($order_products as $product) {
            $is_valid = false;
            if ($product->store_id == $this->coupon->store_id) {
                if ($this->coupon->department_id) {
                    if ($product->department_id == $this->coupon->department_id) {
                        if ($this->coupon->shelf_id) {
                            if ($product->shelf_id == $this->coupon->shelf_id) {
                                if ($this->coupon->store_product_id) {
                                    if ($product->id == $this->coupon->store_product_id) {
                                        $is_valid = true;
                                    }
                                } else {
                                    $is_valid = true;
                                }
                            }
                        } else {
                            $is_valid = true;
                        }
                    }
                } else {
                    $is_valid = true;
                }
            }
            if ($is_valid) {
                if ($this->coupon->type === 'Discount Percentage') {
                    $discount_amount += round(($product->price * $product->cart_quantity) * ($this->coupon->amount / 100));
                }
                if ($this->coupon->type === 'Credit Amount') {
                    $discount_amount += round($product->price * $product->cart_quantity);
                }
            }
        }

        if (!$discount_amount) {
            $store = \Store::select('stores.name AS store_name', 'departments.name AS department_name',
                'shelves.name AS shelf_name',
                DB::raw("CONCAT(products.name, ' ', products.quantity, ' ', products.unit) AS product_name"),
                'cities.city')
                ->leftJoin('departments', function ($join) {
                    $join->on('departments.store_id', '=', 'stores.id');
                    $join->on('departments.id', '=', DB::raw($this->coupon->department_id ?: 0));
                })
                ->leftJoin('shelves', function ($join) {
                    $join->on('shelves.department_id', '=', 'departments.id');
                    $join->on('shelves.id', '=', DB::raw($this->coupon->shelf_id ?: 0));
                })
                ->leftJoin('store_products', function ($join) {
                    $join->on('store_products.shelf_id', '=', 'shelves.id');
                    $join->on('store_products.id', '=', DB::raw($this->coupon->store_product_id ?: 0));
                })
                ->leftJoin('products', 'store_products.product_id', '=', 'products.id')
                ->join('cities', 'cities.id', '=', 'stores.city_id')
                ->where('stores.id', $this->coupon->store_id)
                ->first();

            $message = '';
            if (!empty($store->department_name) && empty($store->shelf_name) && empty($store->product_name)) {
                $message = $store->department_name;
            } else {
                if (!empty($store->shelf_name) && empty($store->product_name)) {
                    $message = $store->shelf_name;
                } else {
                    if (!empty($store->product_name)) {
                        $message = $store->product_name;
                    }
                }
            }

            $message .= " en {$store->city}";

            throw new CouponException("Este cupón aplica exclusivamente para {$message} y deben estar en el pedido.");
        }
    }

    /**
     * Valida que el monto del pedido.
     *
     * @param $amount
     * @param $payment_method
     * @throws CouponException
     */
    private function validateAmount($amount, $payment_method)
    {
        $maximum_amount = currency_format($this->coupon->maximum_order_amount);
        $minimum_amount = currency_format($this->coupon->minimum_order_amount);

        if ($this->coupon->minimum_order_amount && !$this->coupon->maximum_order_amount && $amount < $this->coupon->minimum_order_amount) {
            throw new CouponException("Para redimir el cupón el total del pedido debe ser mayor o igual a {$minimum_amount}.");
        } else {
            if ($this->coupon->maximum_order_amount && !$this->coupon->minimum_order_amount && $amount > $this->coupon->maximum_order_amount) {
                throw new CouponException("Para redimir el cupón el total del pedido debe ser menor o igual a $maximum_amount");
            } else {
                if ($this->coupon->minimum_order_amount && $this->coupon->maximum_order_amount && ($amount > $this->coupon->maximum_order_amount || $amount < $this->coupon->minimum_order_amount)) {
                    throw new CouponException("Para redimir el cupón el total del pedido debe estar entre {$minimum_amount} y {$maximum_amount}.");
                }
            }
        }

        if (!empty($this->coupon->payment_method) && $this->coupon->payment_method !== $payment_method) {
            $methods = \OrderPayment::getPaymentMethods();
            $method_name = empty($methods[$this->coupon->payment_method]) ? '' : $methods[$this->coupon->payment_method];

            throw new CouponException(
                "Este cupón solo es valido para pedidos con el método de pago: " .
                "{$method_name} {$this->coupon->cc_bank}"
            );
        }
    }
}
