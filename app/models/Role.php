<?php
/**
* 
*/
class Role extends \Illuminate\Database\Eloquent\Model
{
    /**
     * @var int
     */
    const SAC_AGENT = 28;

	protected $table = 'roles';

	public function getPermissionsValuesAttributes()
	{
		return json_decode($this->permissions, true);
	}
}
