<?php

use exceptions\MerqueoException;

/**
 * @property mixed storeProduct
 * @property mixed productTransfer
 */
class ProductTransferDetail extends Illuminate\Database\Eloquent\Model
{
    protected $table = 'product_transfer_details';

    public static function boot()
    {
        parent::boot();

        static::updating(function ($productTransfer_detail) {
            if ( $productTransfer_detail->isDirty() ) {
                $dirty_productTransfer_detail = $productTransfer_detail->getDirty();
                $original_productTransfer_detail = $productTransfer_detail->getOriginal();
                $original = $dirty = [];
                foreach ($dirty_productTransfer_detail as $att => $value) {
                    $dirty[$att] = $value;
                    $original[$att] = $original_productTransfer_detail[$att];
                }
                $productTransfer_detail_log = new ProductTransferDetailLog();
                if ( Session::has('admin_id') ) {
                    $productTransfer_detail_log->admin_id = Session::get('admin_id');
                }
                $productTransfer_detail_log->transfer_detail_id = $productTransfer_detail->id;
                $productTransfer_detail_log->original_attributes = json_encode($original);
                $productTransfer_detail_log->changed_attributes = json_encode($dirty);
                $productTransfer_detail_log->save();
            }
        });
    }

    public function productTransfer()
    {
        return $this->belongsTo(ProductTransfer::class);
    }

    public function storeProduct()
    {
        return $this->belongsTo(StoreProduct::class, 'store_product_id');
    }

    public function productTransferDetailLogs()
    {
        return $this->hasMany(ProductTransferDetailLog::class);
    }

    /**
     * Obtiene el status en español
     *
     * @param $value
     * @return string
     */
    public function getSpanishStatusAttribute($value)
    {
        $status = '';
        $value = $this->status;
        switch ($value){
            case 'Damaged':
                $status = 'Dañado';
                break;
            case 'Not received':
                $status = 'No recibido';
                break;
            case 'Partially received':
                $status = 'Parcialmente recibido';
                break;
            case 'Pending':
                $status = 'Pendiente';
                break;
            case 'Received':
                $status = 'Recibido';
                break;
        }

        return $status;
    }

    public function getStatusClassAttribute($value)
    {
        $class = '';
        $value = $this->status;
        switch ($value){
            case 'Damaged':
                $class = 'danger';
                break;
            case 'Not received':
                $class = 'danger';
                break;
            case 'Partially received':
                $class = 'warning';
                break;
            case 'Pending':
                $class = 'default';
                break;
            case 'Received':
                $class = 'success';
                break;
        }

        return $class;
    }

    /**
     * Muestra la fecha de expiración
     *
     * @param $value
     * @return string
     */
    public function getExpirationDateFormattedAttribute($value)
    {
        $value = $this->expiration_date;
        if (!empty($value)) {
            $date = Carbon\Carbon::createFromFormat('Y-m-d', $value);
            return $date->format('d M Y');
        }
        return 'No tiene fecha de expiracion';
    }

    /**
     * Función para actualizar las unidades recibidas de la transferencia
     *
     * @param $quantity
     * @return bool
     * @throws MerqueoException
     */
    public function updateQuantityReceived($quantity)
    {
        if ($this->transferred_quantity == $quantity) {
            $this->status = 'Received';
        } else if ($quantity == 0) {
            $this->status = 'Not received';
        } else if ($this->transferred_quantity > $quantity) {
            $this->status = 'Partially received';
        } else if ($this->transferred_quantity < $quantity) {
            throw new MerqueoException('Cantidad recibida superior a la cantidad transferida.');
        }

        $this->quantity_received = $quantity;

        return $this->save();
    }

    /**
     * Función para actualizar la fecha de expiracion del detalle
     *
     * @param $expiration_date
     * @return bool
     * @throws MerqueoException
     */
    public function updateExpirationDate($expiration_date)
    {
        $dt = \Carbon\Carbon::createFromFormat('Y-m-d', $expiration_date);
        $now = \Carbon\Carbon::now();
        if ($dt->lt($now)) {
            throw new MerqueoException('La fecha ingresada es menor a la fecha actual.');
        }

        $this->expiration_date = $expiration_date;
        return $this->save();
    }

    public function storeDetail($warehouse_id)
    {
        $product = $this->storeProduct->product;
        $warehouse = Warehouse::findOrFail($warehouse_id);
        $store = Store::getActiveStores($warehouse->city_id)->first();
        $store_product = $product->storeProduct()->where('store_id', $store->id)->first();
        $warehouse_storage = WarehouseStorage::where('store_product_id', $store_product->id)
            ->where('warehouse_id', $warehouse_id)
            ->where('position', $this->position)
            ->where('position_height', $this->position_height)
            ->where('storage_type', $this->storage_type);

        if (!empty($this->expiration_date)) {
            $warehouse_storage = $warehouse_storage->where('expiration_date', $this->expiration_date);
        }

        $warehouse_storage = $warehouse_storage->first();

        if (!empty($warehouse_storage)) {
            $quantity_from = $warehouse_storage->quantity;
            $warehouse_storage->quantity += $this->quantity_to_storage;
            $warehouse_storage->save();
            $quantity_to = $warehouse_storage->quantity;

            if ($this->quantity_to_storage + $this->quantity_storaged > $this->quantity_received) {
                throw new MerqueoException('Las cantidades a almacenar son mayores a las cantidades recibidas.');
            }

            $this->quantity_storaged += $this->quantity_to_storage;
            $quantity_to_storage = $this->quantity_to_storage;
            unset($this->quantity_to_storage);
            unset($this->position);
            unset($this->position_height);
            unset($this->storage_type);
            $this->save();

            $warehouse_storage_log = new WarehouseStorageLog();
            $warehouse_storage_log->admin_id = Session::get('admin_id');
            $warehouse_storage_log->warehouse_id = $warehouse_id;
            $warehouse_storage_log->store_product_id = $store_product->id;
            $warehouse_storage_log->module = 'Traslado de productos';
            $warehouse_storage_log->movement = 'update';
            $warehouse_storage_log->quantity_from = $quantity_from;
            $warehouse_storage_log->position_from = $warehouse_storage->position;
            $warehouse_storage_log->position_height_from = $warehouse_storage->position_height;
            $warehouse_storage_log->quantity_to = $quantity_to;
            $warehouse_storage_log->position_to = $warehouse_storage->position;
            $warehouse_storage_log->position_height_to = $warehouse_storage->position_height;
            $warehouse_storage_log->save();
        } else {
            $warehouse_storage = new WarehouseStorage;
            $warehouse_storage->warehouse_id = $warehouse_id;
            $warehouse_storage->store_product_id = $store_product->id;
            $warehouse_storage->admin_id = Session::get('admin_id');
            $warehouse_storage->storage_type = $this->storage_type;
            $warehouse_storage->type = 'Producto';
            $warehouse_storage->quantity = $this->quantity_to_storage;
            $warehouse_storage->expiration_date = $this->expiration_date;
            $warehouse_storage->position = $this->position;
            $warehouse_storage->position_height = $this->position_height;
            $warehouse_storage->save();
            if ($this->quantity_to_storage + $this->quantity_storaged > $this->quantity_received) {
                throw new MerqueoException('Las cantidades a almacenar son mayores a las cantidades recibidas.');
            }
            $this->quantity_storaged += $this->quantity_to_storage;
            $quantity_to_storage = $this->quantity_to_storage;
            unset($this->quantity_to_storage);
            unset($this->position);
            unset($this->position_height);
            unset($this->storage_type);
            $this->save();

            $warehouse_storage_log = new WarehouseStorageLog();
            $warehouse_storage_log->admin_id = Session::get('admin_id');
            $warehouse_storage_log->warehouse_id = $warehouse_id;
            $warehouse_storage_log->store_product_id = $store_product->id;
            $warehouse_storage_log->module = 'Traslado de productos';
            $warehouse_storage_log->movement = 'storage';
            $warehouse_storage_log->quantity_to = $warehouse_storage->quantity;
            $warehouse_storage_log->position_to = $warehouse_storage->position;
            $warehouse_storage_log->position_height_to = $warehouse_storage->position_height;

            $warehouse_storage_log->save();
        }

        $store_product_warehouse = $store_product->storeProductWarehouses()
            ->where('warehouse_id', $warehouse_id)
            ->first();

        $store_product_warehouse->reception_stock -= $quantity_to_storage;
        $store_product_warehouse->save();

        $activable = $store_product->getCurrentStock($warehouse_id);
        if ($activable) {
            $store_product_warehouse->status = 1;
        } else{
            $store_product_warehouse->status = 0;
        }
        $store_product_warehouse->save();

        return true;
    }

    public function toArray($options = 0)
    {
        $result = parent::toArray($options);
        $result['spanish_status'] = $this->spanishStatus;
        $result['status_class'] = $this->statusClass;
        $result['expiration_date_formatted'] = $this->expirationDateFormatted;

        return $result;
    }
}