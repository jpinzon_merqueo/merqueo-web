<?php

class Driver extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'drivers';

    protected $appends = ['full_name'];

    public function transporter()
    {
        return $this->belongsTo('Transporter', 'transporter_id');
    }

    public function order()
    {
        return $this->hasMany('Order');
    }
    public function vehiclesDrivers(){
        return $this->hasMany('VehicleDrivers','driver_id');
    }

    /**
     * Obtiene conductor con calificacion
     *
     * @param array $id ID del conductor
     * @return array $driver Datos de conductor
     */
    public static function getDriver($id)
    {
        $driver = Driver::find($id);
        if ($driver){
            $result = Order::selectRaw('(SUM(user_score) / COUNT(id)) AS average')->where('driver_id', $driver->id)
            ->where('status', 'Delivered')->where('user_score', '>', 0)->groupBy('driver_id')->first();
            $driver->score_average = $result ? round($result->average, 1) : 5;
            if ($driver->score_average == 0) $driver->score_average_image = 'score_0.png';
            else if ($driver->score_average < 1) $driver->score_average_image = 'score_0x.png';
            else if ($driver->score_average == 1) $driver->score_average_image = 'score_1.png';
            else if ($driver->score_average > 1 && $driver->score_average < 2) $driver->score_average_image = 'score_1x.png';
            else if ($driver->score_average == 2) $driver->score_average_image = 'score_2.png';
            else if ($driver->score_average > 2 && $driver->score_average < 3) $driver->score_average_image = 'score_2x.png';
            else if ($driver->score_average == 3) $driver->score_average_image = 'score_3.png';
            else if ($driver->score_average > 3 && $driver->score_average < 4) $driver->score_average_image = 'score_3x.png';
            else if ($driver->score_average == 4) $driver->score_average_image = 'score_4.png';
            else if ($driver->score_average > 4 && $driver->score_average < 5) $driver->score_average_image = 'score_4x.png';
            else $driver->score_average_image = 'score_5.png';
            $driver->score_average = number_format($driver->score_average, 1, '.', ',');

            //shopper de servicio al cliente
            if ($driver->email == 'shoppertemporal@merqueo.com'){
                $driver->first_name = 'Alejandro';
                $driver->last_name = 'de Merqueo';
                $driver->score_average = '5.0';
                $driver->score_average_image = 'score_5.png';
            }

            return $driver;
        }

        return false;
    }

    public function getFullNameAttribute()
    {
        return ucwords(strtolower("{$this->last_name} {$this->first_name}"));
    }

}