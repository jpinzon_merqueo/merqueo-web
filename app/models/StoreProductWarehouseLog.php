<?php

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $storage_position
 * @property int $storage_height_position
 * @property int $minimum_picking_stock
 * @property int $maximum_picking_stock
 * @property bool $manage_stock
 * @property bool $is_visible_stock
 * @property int $ideal_stock
 * @property int $minimum_stock
 * @property int $reception_stock
 * @property int $picking_stock
 * @property int $picked_stock
 * @property int $return_stock
 * @property int $shrinkage_stock
 * @property bool $status
 */
class StoreProductWarehouseLog extends Model
{
    protected $table = 'store_product_warehouse_log';
}