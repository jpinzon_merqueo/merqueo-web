<?php

class OrderNote extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'order_notes';
    protected $hidden = array('created_at', 'updated_at');

}