<?php

class UserAppComment extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'user_app_comments';
    protected $hidden = array('created_at','updated_at');   

}