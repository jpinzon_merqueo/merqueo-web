<?php

class Shrinkage extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'shrinkages';
    protected $hidden = array('created_at','updated_at');

    /**
     * Relación con el modelo ShrinkageDetail
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function shrinkageDetail()
    {
        return $this->hasMany('ShrinkageDetail');
    }

    /**
     *  Genera el pdf de una factura
     *
     * @param $shrinkage
     * @return file Respuesta del servicio
     * @internal param object $data Datos de una pedido
     */
    public static function generatePdf($shrinkage)
    {
        $id = $shrinkage->id;
        $store_products = ShrinkageDetail::where('shrinkage_id', $id)
                                   ->join('store_products', 'store_products.id', '=', 'shrinkage_details.store_product_id')
                                   ->select('shrinkage_details.store_product_id',  'product_name', 'shrinkage_details.status', 'quantity_reported',
                                    DB::raw('IF(product_price is null, store_products.price, product_price) AS price'))
                                   ->get();

        Fpdf::AddPage();
        Fpdf::SetLeftMargin(6);
        // Logo
        Fpdf::Image(url('/').'/admin_asset/img/logo_pdf.png',10,18,50);
        Fpdf::SetFont('Arial','B',10);
        Fpdf::Cell(75);
        Fpdf::Cell(30,5,'MERQUEO S A S',0,0,'C');
        Fpdf::SetFont('Arial','B',8);
        Fpdf::SetDrawColor(237,237,237);
        Fpdf::Cell(33);
        Fpdf::Cell(52,6,'Merma Nro','LTR',0,'C');
        Fpdf::Ln(6);

        // Información de contacto
        Fpdf::SetFont('Arial','',9);
        Fpdf::Cell(76);
        Fpdf::Cell(35,5,'NIT : 900.871.444 - 8',0,0,'C');
        Fpdf::Cell(31);
        Fpdf::SetFont('Arial','',8);
        Fpdf::Cell(52,9,$id,'LBR',0,'C');
        Fpdf::Ln(4);

        Fpdf::Cell(75);
        Fpdf::Cell(40,5,'CLL 94A 11A 32 OFICINA 503',0,0,'C');
        Fpdf::Ln(4);

        Fpdf::Cell(78);
        Fpdf::Cell(30,5,'TEL: 3836167',0,0,'C');
        Fpdf::Ln(4);

        Fpdf::Cell(78);
        Fpdf::Cell(30,5,utf8_decode('BOGOTÁ - COLOMBIA'),0,0,'C');
        Fpdf::Ln(15);

        /////////////Detalle merma////////
        Fpdf::SetDrawColor(210,210,210);
        Fpdf::SetFillColor(241, 241, 241);
        Fpdf::SetFont('Arial','B',8);
        Fpdf::Cell(15,5,utf8_decode('ID'),1,0,'C',true);
        Fpdf::Cell(80,5,utf8_decode('Producto'),1,0,'C',true);
        Fpdf::Cell(30,5,utf8_decode('Estado'),1,0,'C',true);
        Fpdf::Cell(19,5,utf8_decode('Cantidad'),1,0,'C',true);
        Fpdf::Cell(25,5,utf8_decode('Valor Unitario'),1,0,'C',true);
        Fpdf::Cell(25,5,utf8_decode('Valor Total'),1,0,'C',true);
        Fpdf::Ln(5);

        $total = 0;
        foreach ($store_products as $key => $value) {
            Fpdf::SetFont('Arial','',7);

            /////////////Data///////////////////
            Fpdf::Cell(15,5,utf8_decode($value->store_product_id),1,0,'L',false);
            Fpdf::Cell(80,5,utf8_decode($value->product_name),1,0,'L',false);
            Fpdf::Cell(30,5,utf8_decode($value->status),1,0,'L',false);
            Fpdf::Cell(19,5,utf8_decode($value->quantity_reported),1,0,'C',false);
            Fpdf::Cell(25,5,'$ '.number_format($value->price, 0, ',', '.'),1,0,'R',false);
            Fpdf::Cell(25,5,'$ '.number_format($value->price * $value->quantity_reported, 0, ',', '.'),1,0,'R',false);
            Fpdf::Ln(5);
            $total += $value->price * $value->quantity_reported;
        }

        Fpdf::SetFillColor(241, 241, 241);
        Fpdf::SetFont('Arial','B',7);
        Fpdf::Cell(144);
        Fpdf::Cell(25,5,utf8_decode('Total'),1,0,'L',true);
        Fpdf::SetFont('Arial','',7);
        Fpdf::Cell(25,5,'$ '.number_format($total, 0, ',', '.'),1,0,'R',true);
        Fpdf::Ln(5);

        Fpdf::SetFont('Arial','',8);
        Fpdf::SetFillColor(237, 237, 237);
        Fpdf::SetFont('Arial','B',8);
        Fpdf::SetFont('Arial','I',8);
        Fpdf::Cell(20,5,utf8_decode('Página ').Fpdf::PageNo(),0,0,'C');
        Fpdf::Output();
        exit;
    }

}