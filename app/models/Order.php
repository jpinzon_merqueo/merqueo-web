<?php

use App\Models\PaymentMethod;
use Carbon\Carbon;
use exceptions\MerqueoException;
use Hashids\Hashids;
use Illuminate\Support\Facades\Config;
use orders\OrderStatus;
use Symfony\Component\HttpKernel\Exception\HttpException;
use contracts\ApiDashboardInterface;

/**
 * @property Carbon $management_date
 * @property int $id
 * @property int $user_score
 * @property Carbon $user_score_date
 * @property OrderGroup $orderGroup
 * @property string user_score_comments
 * @property int $user_id
 * @property User $user
 * @property string $user_score_typification
 * @property string $cc_last_four
 * @property string $delivery_date
 * @property string|Carbon $delivery_time
 * @property string $reference
 * @property double $total_amount
 * @property string $payment_method
 * @property string $status
 * @property string $user_identity_type
 * @property string $user_identity_number
 * @property string $user_business_name
 * @property string $credit_card_id
 * @property string $real_delivery_date
 * @property int $scheduled_delivery
 * @property int $store_id
 * @property string $invoice_number
 * @property int $posible_fraud
 * @property OrderProduct[] $orderProducts
 * @property int $allied_store_id
 * @property AlliedStore $alliedStore
 * @property int $delivery_amount
 * @property Store $store
 * @property int $discount_percentage_amount
 * @property int $discount_amount
 * @property UserCreditCard $creditCard
 * @property int $order_validation_reason_id
 * @property string $type
 * @property string cc_type
 * @property string cc_bin
 */
class Order extends \Illuminate\Database\Eloquent\Model
{
    use \orders\OrderAttributes;

    const MARKETPLACE = 'Marketplace';
    const GOOD_RATE = 'success';

    const SHIFT_EXPRESS = 'EX';
    const SHIFT_FAST_DELIVERY = 'ER';

    /**
     * @var string
     */
    const MEXICO_IDS = '17';

    const PAYMENT_METHOD_CC = 'Tarjeta de crédito';

    /**
     * @var string
     */
    protected $table = 'orders';

    protected static function boot()
    {
        parent::boot();

        static::created(function ($order) {
            // asignar letra para picking
            $order->picking_sequence_letter = get_name_from_number(substr($order->id, -4));
            $order->save();
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function store()
    {
        return $this->belongsTo('Store');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function orderGroup()
    {
        return $this->belongsTo('OrderGroup', 'group_id');
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function pickerDry()
    {
        return $this->belongsTo('Picker', 'picker_dry_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function pickerCold()
    {
        return $this->belongsTo('Picker', 'picker_cold_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('User', 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function driver()
    {
        return $this->belongsTo('Driver', 'driver_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function userCredits()
    {
        return $this->hasMany('UserCredit', 'order_id');
    }

    /**
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orderProducts()
    {
        return $this->hasMany('OrderProduct');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function storeProducts()
    {
        return $this->belongsToMany(StoreProduct::class, 'order_products', 'order_id', 'store_product_id')
                    ->withPivot([
                        'quantity',
                        'price',
                    ]);
    }

    /**
     * Relación con productos no disponibles de orden.
     */
    public function orderProductsNotAvailable()
    {
        return $this->hasMany('OrderProduct')
                    ->where('fulfilment_status', 'Not Available');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function route()
    {
        return $this->belongsTo('Routes', 'route_id');
    }

    /**
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function vehicle()
    {
        return $this->belongsTo('Vehicle', 'vehicle_id');
    }

    /**
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function alliedStore()
    {
        return $this->belongsTo('AlliedStore', 'allied_store_id');
    }

    /**
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orderProductGroup()
    {
        return $this->hasMany(OrderProductGroup::class);
    }

    /**
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function TrasnporterNew()
    {
        return $this->hasMany('TransporterNew', 'order_id');
    }

    /**
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orderPickingQuality()
    {
        return $this->hasMany(OrderPickingQuality::class);
    }

    /**
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orderDiscount()
    {
        return $this->hasMany('OrderDiscount', 'order_id');
    }

    /**
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creditCard()
    {
        return $this->belongsTo('UserCreditCard', 'credit_card_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function userCredit()
    {
        return $this->hasMany('UserCredit', 'order_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function deliveryWindow()
    {
        return $this->belongsTo('DeliveryWindow', 'delivery_window_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function userCoupon()
    {
        return $this->hasMany('UserCoupon', 'order_id');
    }

    /**
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function orderValidationReason()
    {
        return $this->belongsTo('OrderValidationReason', 'order_validation_reason_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orderPayments()
    {
        return $this->hasMany('OrderPayment', 'order_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orderPaymentsLogs()
    {
        return $this->hasMany('OrderPaymentLog');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function logs()
    {
        return $this->hasMany(OrderLog::class);
    }

    /**
     * Valida si el pedido tiene productos faltantes
     *
     * @return int Numero de productos no disponibles
     */
    public function hasMissingProducts()
    {
        return OrderProduct::where('order_id', $this->id)->where('fulfilment_status', 'Not Available')->count();
    }

    /**
     * Activa y desactiva productos dependiendo del stock actual, comprometido y solicitado al hacer un pedido
     */
    public function validateCommittedStock()
    {
        try {
            $warehouse = $this->orderGroup->warehouse;

            $storeProductWarehouses = StoreProductWarehouse::select('store_product_warehouses.*')
                ->join('store_products', 'store_product_warehouses.store_product_id', '=', 'store_products.id')
                ->join('products', 'store_products.product_id', '=', 'products.id')
                ->join('order_products', 'order_products.store_product_id', '=', 'store_products.id')
                ->where('products.type', 'Simple')
                ->where('store_product_warehouses.is_visible_stock', 0)
                ->where('store_product_warehouses.manage_stock', 1)
                ->where('store_product_warehouses.warehouse_id', $warehouse->id)
                ->where('order_products.order_id', $this->id)
                ->get();

            $storeProductWarehousesGroups = StoreProductWarehouse::select('store_product_warehouses.*')
                ->join('store_products', 'store_product_warehouses.store_product_id', '=', 'store_products.id')
                ->join('order_product_group', 'order_product_group.store_product_id', '=', 'store_products.id')
                ->join('products', 'store_products.product_id', '=', 'products.id')
                ->where('products.type', 'Simple')
                ->where('store_product_warehouses.is_visible_stock', 0)
                ->where('store_product_warehouses.manage_stock', 1)
                ->where('store_product_warehouses.warehouse_id', $warehouse->id)
                ->where('order_product_group.order_id', $this->id)
                ->get();

            $storeProductWarehouses = $storeProductWarehouses->merge($storeProductWarehousesGroups);

            $storeProductId = $storeProductWarehouses->map(function ($product) {
                return $product->store_product_id;
            })->toArray();

            \App::make(\contracts\ApiDashboardInterface::class)->currentStockRefresh($warehouse->id, $storeProductId);

        } catch (Exception $exception) {

            $request_headers = '';
            if (function_exists('getallheaders')) {
                foreach (getallheaders() as $name => $value) {
                    $request_headers .= '
  '.$name.': '.$value;
                }
            }

            $url = URL::current();
            $method = Request::method();
            $params = json_encode(Input::get());
            $ip = get_ip();
            $message = "\r\nURL: ".$url." ".$method."\r\nHEADERS: ".$request_headers."\r\nPARAMS: ".$params."\r\nMESSAGE: ".$exception->getMessage()."\r\nFILE: ".$exception->getFile()."\r\nLINE: ".$exception->getLine()."\r\nIP: ".$ip;

            //guardar error log
            $error_log = new ErrorLog();
            $error_log->url = $url;
            $error_log->response_http_code = 500;
            $error_log->message = 'Al validar productos comprometidos del pedido: '.$exception->getMessage();
            $error_log->request = $message;
            $error_log->ip = $ip;
            $error_log->save();
        }
    }

    /**
     * Actualizar totales del pedido
     */
    public function updateTotals()
    {
        $order_products = OrderProduct::select('order_products.*', 'department_id', 'shelf_id')
            ->join('store_products', 'order_products.store_product_id', '=', 'store_products.id')
            ->where('order_id', $this->id)
            ->get();
        if (count($order_products)) {
            /*//descuento en departamento especifico
            $total_on_specific_department = 0;
            $department_discount_ids = [];
            if (date('Y-m-d', strtotime($this->date)) == '2018-05-31' || date('Y-m-d', strtotime($this->date)) == '2018-06-01')
            $department_discount_ids = [20, 76];
            else if (date('Y-m-d', strtotime($this->date)) == '2018-06-02')
            $department_discount_ids = [37, 83];
            else if (date('Y-m-d', strtotime($this->date)) == '2018-06-03')
            $department_discount_ids = [15, 72];*/

            $total_products = 0;
            $total_amount = 0;
            $this->total_products = count($order_products);
            foreach ($order_products as $order_product) {
                if ($this->status == 'Validation' || $this->status == 'Initiated' || $this->status == 'Enrutado') {
                    $total_amount = $total_amount + ($order_product->quantity * $order_product->price);
                    $total_products++;
                } else {
                    if ($order_product->fulfilment_status == 'Pending' || $order_product->fulfilment_status == 'Fullfilled') {
                        $total_amount = $total_amount + ($order_product->quantity * $order_product->price);
                        $total_products++;
                    }
                }

                /*if (in_array($order_product->department_id, $department_discount_ids) && $order_product->shelf_id != 387 && $order_product->shelf_id != 413)
            $total_on_specific_department += $order_product->quantity * $order_product->price;*/
            }
            $this->total_products = $total_products;
            $this->total_amount = $total_amount;

            //recalcular descuento
            $coupon = $this->orderHasCoupons();

            if ($coupon) {

                $coupon->validateOnOrder($this);

                //actualizar movimientos de credito
                UserCredit::updateCredit($this, $this->discount_amount);
            } else {
                if ($this->discount_percentage_amount) {
                    $this->discount_amount = round($this->total_amount * ($this->discount_percentage_amount / 100), 0);
                }

                //actualizar movimientos de credito
                UserCredit::updateCredit($this, $this->discount_amount);
            }

            //validar total vs descuento
            $total = $this->total_amount + $this->delivery_amount - $this->discount_amount;
            if ($total < 0 && $this->discount_amount) {
                $new_discount_amount = $this->total_amount + $this->delivery_amount;
                $this->discount_amount = $new_discount_amount;
            }
        }
    }


    /**
     * Verifica si el usuario de la orden tiene cupones activos
     *
     * @return \App\Models\Coupon $coupon
     *
     */
    public function orderHasCoupons()
    {
        $coupon = Coupon::select('coupons.*')
            ->join('user_credits', 'coupon_id', '=', 'coupons.id')
            ->where('order_id', $this->id)
            ->where('coupons.status', 1)
            ->first();

        return $coupon;
    }


    /**
     * Actualizar stock de picking a productos alistados disponibles
     */
    public function updatePickingStock()
    {
        $warehouseId = $this->orderGroup()
                            ->pluck('warehouse_id');
        $orderProducts = OrderProduct::where('order_id', $this->id)
            ->where('fulfilment_status', 'Fullfilled')
            ->get();

        $products = [];
        foreach ($orderProducts as $orderProduct) {
            if ($orderProduct->type == 'Agrupado') {
                $orderProductGroups = OrderProductGroup::where('order_product_id', $orderProduct->id)
                    ->where('fulfilment_status', 'Fullfilled')
                    ->get();

                foreach ($orderProductGroups as $orderProductGroup) {
                    if (array_key_exists($orderProductGroup->store_product_id, $products)) {
                        $products[$orderProductGroup->store_product_id]['quantity'] += $orderProductGroup->quantity;
                    } else {
                        $products[$orderProductGroup->store_product_id]['quantity'] = $orderProductGroup->quantity;
                    }
                }
            } else {
                if (array_key_exists($orderProduct->store_product_id, $products)) {
                    $products[$orderProduct->store_product_id]['quantity'] += $orderProduct->quantity;
                } else {
                    $products[$orderProduct->store_product_id]['quantity'] = $orderProduct->quantity;
                }
            }
        }

        foreach ($products as $key => $product) {
            $storeProductWarehouse = StoreProductWarehouse::getStoreProductWarehouse($warehouseId, $key);
            $storeProductWarehouse->decrement('picking_stock', $product['quantity']);
            $storeProductWarehouse->increment('picked_stock', $product['quantity']);
            $storeProductWarehouse->queueVisibilityTask();
        }
    }

    /**
     * Actualizar stock alistado a productos despachados
     */
    public function updatePickedStock()
    {
        $warehouse_id = $this->orderGroup()->pluck('warehouse_id');
        $order_products = OrderProduct::where('order_id', $this->id)->where('fulfilment_status', 'Fullfilled')->get();
        foreach ($order_products as $order_product) {
            if ($store_product_warehouse = StoreProductWarehouse::getStoreProductWarehouseWithLocking($warehouse_id, $order_product->store_product_id)) {
                if ($order_product->type == 'Agrupado') {
                    $order_product_groups = OrderProductGroup::where('order_product_id', $order_product->id)->where('fulfilment_status', 'Fullfilled')->get();
                    foreach ($order_product_groups as $order_product_group) {
                        if ($store_product_warehouse = StoreProductWarehouse::getStoreProductWarehouseWithLocking($warehouse_id, $order_product_group->store_product_id)) {
                            $store_product_warehouse->picked_stock = $store_product_warehouse->picked_stock - $order_product_group->quantity;
                            $store_product_warehouse->save();
                        }
                    }
                } else {
                    $store_product_warehouse->picked_stock = $store_product_warehouse->picked_stock - $order_product->quantity;
                    $store_product_warehouse->save();
                }
            }
        }
    }

    /**
     * Volver a actualizar stock de picking
     *
     * @param string $order_status Estado de pedido
     */
    public function updatePickingStockBack($order_status)
    {
        //a productos despachados disponibles y despues quedaron como no disponibles
        if ($order_status == 'Dispatched' || $order_status == 'Delivered') {
            //simples
            $order_products = OrderProduct::where('order_id', $this->id)
                ->where('fulfilment_status', 'Not Available')
                ->where('type', 'Product')
                ->where('update_stock_back', 1)->get();
            if (count($order_products)) {
                foreach ($order_products as $order_product) {
                    if ($store_product_warehouse = StoreProductWarehouse::getStoreProductWarehouse($this->orderGroup->warehouse_id, $order_product->store_product_id)) {
                        if ($this->status == 'Delivered' || $this->status == 'Cancelled') {
                            $store_product_warehouse->picking_stock = $store_product_warehouse->picking_stock + $order_product->quantity;
                        } elseif ($this->status == 'Dispatched') {
                            $store_product_warehouse->picking_stock = $store_product_warehouse->picking_stock - $order_product->quantity;
                        }
                        $store_product_warehouse->save();
                    }
                }
            }
            //agrupados
            $order_product_groups = OrderProductGroup::where('order_id', $this->id)->where('fulfilment_status', 'Not Available')->where('update_stock_back', 1)->get();
            if (count($order_product_groups)) {
                foreach ($order_product_groups as $order_product_group) {
                    if ($store_product_warehouse = StoreProductWarehouse::getStoreProductWarehouse($this->orderGroup->warehouse_id, $order_product_group->store_product_id)) {
                        if ($this->status == 'Delivered' || $this->status == 'Cancelled') {
                            $store_product_warehouse->picking_stock = $store_product_warehouse->picking_stock + $order_product_group->quantity;
                        } elseif ($this->status == 'Dispatched') {
                            $store_product_warehouse->picking_stock = $store_product_warehouse->picking_stock - $order_product_group->quantity;
                        }
                        $store_product_warehouse->save();
                    }
                }
            }
        }

        //a pedido alistado y despues cancelado
        if ($order_status == 'Alistado' && $this->status == 'Cancelled') {
            //simples
            $order_products = OrderProduct::where('order_id', $this->id)
                ->where('fulfilment_status', 'Fullfilled')
                ->where('type', 'Product')
                ->get();
            if (count($order_products)) {
                foreach ($order_products as $order_product) {
                    if ($store_product_warehouse = StoreProductWarehouse::getStoreProductWarehouse($this->orderGroup->warehouse_id, $order_product->store_product_id)) {
                        $store_product_warehouse->picking_stock = $store_product_warehouse->picking_stock + $order_product->quantity;
                        $store_product_warehouse->picked_stock = $store_product_warehouse->picked_stock - $order_product->quantity;
                        $store_product_warehouse->save();
                    }
                }
            }
            //agrupados
            $order_product_groups = OrderProductGroup::where('order_id', $this->id)->where('fulfilment_status', 'Fullfilled')->get();
            if (count($order_product_groups)) {
                foreach ($order_product_groups as $order_product_group) {
                    if ($store_product_warehouse = StoreProductWarehouse::getStoreProductWarehouse($this->orderGroup->warehouse_id, $order_product_group->store_product_id)) {
                        $store_product_warehouse->picking_stock = $store_product_warehouse->picking_stock + $order_product_group->quantity;
                        $store_product_warehouse->picked_stock = $store_product_warehouse->picked_stock - $order_product_group->quantity;
                        $store_product_warehouse->save();
                    }
                }
            }
        }
    }

    /**
     * Actualizar el stock de investigación
     *
     * @param string $orderStatus Estado de pedido
     * @param string $orderStatus modulo del entity_id
     */
    public function updateInvestigationStockBack($orderStatus, $module = 'Pedido')
    {
        //a productos despachados disponibles y despues quedaron como no disponibles
        if ($orderStatus == 'Dispatched' || $orderStatus == 'Delivered') {
            //simples
            $orderProducts = OrderProduct::where('order_id', $this->id)
                ->where('fulfilment_status', 'Not Available')
                ->where('type', 'Product')
                ->where('update_stock_back', 1)
                ->get();

            if (count($orderProducts)) {
                foreach ($orderProducts as $orderProduct) {
                    if ($storeProductWarehouse = StoreProductWarehouse::getStoreProductWarehouse($this->orderGroup->warehouse_id, $orderProduct->store_product_id)) {
                        if ($this->status == 'Delivered' || $this->status == 'Cancelled') {
                            $storeProductInvestigation = new InvestigationStock();
                            $storeProductInvestigation->quantity = $orderProduct->quantity;
                            $storeProductInvestigation->warehouse_id = $this->orderGroup->warehouse_id;
                            $storeProductInvestigation->store_product_id = $orderProduct->store_product_id;
                            $storeProductInvestigation->entity_id = $this->id;
                            $storeProductInvestigation->module = $module;
                            $storeProductInvestigation->inventory_counting_position_id = 0;
                            $storeProductInvestigation->storage = 'Alistamiento';
                            $storeProductInvestigation->position = $storeProductWarehouse->storage_position;
                            $storeProductInvestigation->height_position = $storeProductWarehouse->storage_height_position;
                            $storeProductInvestigation->admin_id = Session::get('admin_id');
                            $storeProductInvestigation->product_group = 0;
                            $storeProductInvestigation->save();
                        } elseif ($this->status == 'Dispatched') {
                            InvestigationStock::where('entity_id', $this->id)
                                ->where('store_product_id', $orderProduct->store_product_id)
                                ->where('module', $module)
                                ->where('product_group', 0)
                                ->delete();
                        }
                    }
                }
            }
            //agrupados
            $orderProductGroups = OrderProductGroup::where('order_id', $this->id)->where('fulfilment_status', 'Not Available')->where('update_stock_back', 1)->get();
            if (count($orderProductGroups)) {
                foreach ($orderProductGroups as $orderProductGroup) {
                    if ($storeProductWarehouse = StoreProductWarehouse::getStoreProductWarehouse($this->orderGroup->warehouse_id, $orderProductGroup->store_product_id)) {
                        if ($this->status == 'Delivered' || $this->status == 'Cancelled') {
                            $storeProductInvestigation = new InvestigationStock();
                            $storeProductInvestigation->quantity = $orderProductGroup->quantity;
                            $storeProductInvestigation->warehouse_id = $this->orderGroup->warehouse_id;
                            $storeProductInvestigation->store_product_id = $orderProductGroup->store_product_id;
                            $storeProductInvestigation->entity_id = $this->id;
                            $storeProductInvestigation->module = $module;
                            $storeProductInvestigation->inventory_counting_position_id = 0;
                            $storeProductInvestigation->storage = 'Alistamiento';
                            $storeProductInvestigation->position = $storeProductWarehouse->storage_position;
                            $storeProductInvestigation->height_position = $storeProductWarehouse->storage_height_position;
                            $storeProductInvestigation->admin_id = Session::get('admin_id');
                            $storeProductInvestigation->product_group = 1;
                            $storeProductInvestigation->save();
                        } elseif ($this->status == 'Dispatched') {
                            InvestigationStock::where('entity_id', $this->id)
                                ->where('store_product_id', $orderProductGroup->store_product_id)
                                ->where('module', $module)
                                ->where('product_group', 1)
                                ->delete();
                        }
                    }
                }
            }
        }

        //a pedido alistado y despues cancelado
        if ($orderStatus == 'Alistado' && $this->status == 'Cancelled') {
            //simples
            $orderProducts = OrderProduct::where('order_id', $this->id)
                ->where('type', 'Product')
                ->where('fulfilment_status', 'Fullfilled')
                ->get();
            if (count($orderProducts)) {
                foreach ($orderProducts as $orderProduct) {
                    if ($storeProductWarehouse = StoreProductWarehouse::getStoreProductWarehouseWithLocking($this->orderGroup->warehouse_id, $orderProduct->store_product_id)) {
                        $storeProductWarehouse->picking_stock += $orderProduct->quantity;
                        $storeProductWarehouse->picked_stock -= $orderProduct->quantity;
                        $storeProductWarehouse->save();
                    }
                }
            }
            //agrupados
            $orderProductGroups = OrderProductGroup::where('order_id', $this->id)
                ->where('fulfilment_status', 'Fullfilled')->get();
            if (count($orderProductGroups)) {
                foreach ($orderProductGroups as $orderProductGroup) {
                    if ($storeProductWarehouse = StoreProductWarehouse::getStoreProductWarehouseWithLocking($this->orderGroup->warehouse_id, $orderProductGroup->store_product_id)) {
                        $storeProductWarehouse->picking_stock += $orderProductGroup->quantity;
                        $storeProductWarehouse->picked_stock -= $orderProductGroup->quantity;
                        $storeProductWarehouse->save();
                    }
                }
            }
        }
    }

    /**
     * Actualizar picking a picked en pedidos alistados al cambiar el estado del producto
     * @param $storeProductId
     * @param $quantity
     * @param $status
     */
    public function updateStockPickingPicked($storeProductId, $quantity, $status)
    {
        if ($storeProductWarehouse = StoreProductWarehouse::getStoreProductWarehouse(
            $this->orderGroup->warehouse_id,
            $storeProductId
        )) {
            if ($status == 'Not Available') {
                $storeProductWarehouse->picking_stock += $quantity;
                $storeProductWarehouse->picked_stock -= $quantity;
            } elseif ($status == 'Fullfilled') {
                $storeProductWarehouse->picking_stock -= $quantity;
                $storeProductWarehouse->picked_stock += $quantity;
            }

            $storeProductWarehouse->save();
        }
    }

    /**
     * Validar referido en pedido entregado
     */
    public function validateReferred()
    {
        //si es primera compra y fue referido
        $count_orders = self::where('user_id', $this->user_id)->where('status', 'Delivered')->count();
        if ($count_orders == 1) {
            $user = User::find($this->user_id);

            if ($user->referred_by && $referrer = User::find($user->referred_by)) {
                if ($referrer->total_referrals < Config::get('app.referred.limit')) {
                    /*//referido con domicilio gratis
                    $referred_by_free_delivery_days = Config::get('app.referred.referred_by_free_delivery_days');
                    $user_free_delivery = new UserFreeDelivery;
                    $user_free_delivery->user_id = $referrer->id;
                    $user_free_delivery->referrer_id = $user->id;
                    $user_free_delivery->amount = $referred_by_free_delivery_days;

                    if (!empty($referrer->free_delivery_expiration_date) && $referrer->free_delivery_expiration_date <= date('Y-m-d'))
                    $free_delivery_expiration_date = date('Y-m-d', strtotime('+'.$referred_by_free_delivery_days.' day'));
                    else $free_delivery_expiration_date = date('Y-m-d', strtotime($referrer->free_delivery_expiration_date.' +'.$referred_by_free_delivery_days.' day'));
                    $user_free_delivery->expiration_date = $free_delivery_expiration_date;
                    $description = 'Adición de '.$referred_by_free_delivery_days.' días de domicilio gratis por registro de amigo '.$user->first_name.' '.$user->last_name.' con ID usuario # '.$user->id.'
                    con sistema de referidos en pedido con ID # '.$this->id.'. Nueva fecha de vencimiento: '.format_date('normal', $user_free_delivery->expiration_date);
                    $user_free_delivery->description = $description;
                    $user_free_delivery->created_at = $user_free_delivery->updated_at = date('Y-m-d H:i:s');
                    $user_free_delivery->save();*/

                    //registrar credito a nuevo usuario
                    $referred_by_amount = Config::get('app.referred.referred_by_amount');
                    $user->order_id = $this->id;
                    $expiration_days = Config::get('app.referred.amount_expiration_days');
                    $expiration_date = date('Y-m-d', strtotime('+'.$expiration_days.' day'));
                    UserCredit::addCredit($referrer, $referred_by_amount, $expiration_date, 'referred_by', $user);

                    //aumentar cantidad de referidos y fecha de expiracion de domicilio gratis a usuario
                    //$referrer->free_delivery_expiration_date = $user_free_delivery->expiration_date;

                    $referrer->total_referrals += 1;
                    $referrer->save();

                    $user_discounts = User::getDiscounts($referrer->id);

                    //enviar mail de cargo de credito a usuario referido
                    $mail = [
                        'template_name' => 'emails.referred',
                        'subject' => 'Tienes '.currency_format($referred_by_amount).' de crédito en tu cuenta',
                        'to' => [['email' => $referrer->email, 'name' => $referrer->first_name.' '.$referrer->last_name]],
                        'vars' => [
                            'username' => $referrer->first_name,
                            'username_referrer' => $user->first_name,
                            'credit_available' => $user_discounts['amount'],
                            'referred_by_amount' => $referred_by_amount,
                            'referred' => User::validateReferred($referrer),
                            'expiration_date' => format_date('normal', $expiration_date),
                            'expiration_days' => $expiration_days,
                            'referal_code' => $referrer->referral_code,
                            'minimum_discount_amount' => $user_discounts['minimum_discount_amount'],
                        ],
                    ];
                    if (empty($this->parent_order_id)) {
                        send_mail($mail);
                    }

                }
            }
        }
    }

    /**
     * Valida datos de pedido en lista negra y tarjeta de credito
     *
     * @param  array $post_data Datos del pedido
     * @return array $response Resultado de validacion
     */
    public function validateOrderFraud($post_data)
    {
        $response = ['validation' => false, 'posible_fraud' => ''];

        //validar direccion
        if (isset($post_data['address_id']) && !empty($post_data['address_id'])) {
            $user_address = UserAddress::find($post_data['address_id']);
            $address = $user_address->address;
            $city_id = $user_address->city_id;
        } else {
            if (!isset($post_data['address'])) {
                if (!isset($post_data['is_webservice'])) {
                    $address = implode('', $post_data['dir']);
                } else {
                    $address = $post_data['dir'];
                }

            } else {
                $address = $post_data['address'];
            }

            if (isset($post_data['city'])) {
                $city = City::where('slug', $post_data['city'])->first();
            } else {
                $city = City::find($post_data['city_id']);
            }

            $city_id = $city->id;
        }
        if (!empty($address) && $result = Blacklist::where('type', 'Dirección')->where('value', 'LIKE', '%'.trim($address).'%')->where('city_id', $city_id)->first()) {
            $response['posible_fraud'] .= !empty($result->order_id) ? 'La dirección aparece reportada en la lista negra con el pedido #'.$result->order_id.'. ' : 'La dirección aparece reportada en la lista negra. ';
        }

        //validar email
        if (isset($post_data['user_id']) && !empty($post_data['user_id'])) {
            $user = User::find($post_data['user_id']);
            $email = $user->email;
        } else {
            $email = $post_data['email'];
        }

        if ($result = Blacklist::where('type', 'Email')->where('value', trim($email))->first()) {
            $response['posible_fraud'] .= !empty($result->order_id) ? 'El email aparece reportado en la lista negra con el pedido #'.$result->order_id.'. ' : 'El email aparece reportado en la lista negra. ';
        }

        //validar celular
        if (isset($post_data['user_id']) && !empty($post_data['user_id'])) {
            $user = !isset($user) ? User::find($post_data['user_id']) : $user;
            $phone = $user->phone;
        } else {
            $phone = $post_data['phone'];
        }

        if ($result = Blacklist::where('type', 'Celular')->where('value', trim($phone))->first()) {
            $response['posible_fraud'] .= !empty($result->order_id) ? 'El celular aparece reportado en la lista negra con el pedido #'.$result->order_id.'. ' : 'El celular aparece reportado en la lista negra. ';
        }

        //validar tarjeta
        if ($post_data['payment_method'] == 'Tarjeta de crédito') {
            $credit_card = false;
            if (isset($post_data['credit_card_id']) && !empty($post_data['credit_card_id'])) {
                $user_credit_card = UserCreditCard::join('users', 'users.id', '=', 'user_id')
                                    ->where('user_credit_cards.id', $post_data['credit_card_id'])
                                    ->first();

                if (strlen($user_credit_card->card_token) == Config::get('app.payu.token_length')) {
                    $credit_card = [
                        'bin' => $user_credit_card->bin,
                        'last_four' => $user_credit_card->last_four,
                        'expiration_month' => $user_credit_card->expiration_month,
                        'expiration_year' => $user_credit_card->expiration_year,
                    ];
                }
            } else {
                $credit_card = [
                    'bin' => substr($post_data['number_cc'], 0, 6),
                    'last_four' => substr($post_data['number_cc'], 11, 4),
                    'expiration_month' => $post_data['expiration_month_cc'],
                    'expiration_year' => $post_data['expiration_year_cc'],
                ];
            }
            if ($credit_card) {
                $result = Blacklist::where('type', 'Tarjeta de crédito')
                    ->where('cc_bin', trim($credit_card['bin']))
                    ->where('cc_last_four', trim($credit_card['last_four']))
                    ->where('cc_expiration_month', trim($credit_card['expiration_month']))
                    ->where('cc_expiration_year', trim($credit_card['expiration_year']))
                    ->first();
                if ($result) {
                    $response['posible_fraud'] .= !empty($result->order_id) ? 'La tarjeta de crédito aparece reportada en la lista negra con el pedido #'.$result->order_id.'. ' : 'La tarjeta de crédito aparece reportada en la lista negra. ';
                }
            }

            $response['posible_fraud'] = trim($response['posible_fraud']);

            //verificar si la tarjeta es nueva
            if (isset($post_data['credit_card_id'])) {
                $old_credit_card = self::where('credit_card_id', $post_data['credit_card_id'])->where('payment_method', 'Tarjeta de crédito')->where('status', 'Delivered')->first();
                if (!$old_credit_card) {
                    $response['posible_fraud'] .= 'Tarjeta de crédito nueva';
                    $response['validation'] = true;
                    return $response;
                }
            } else {
                if ($post_data['payment_method'] == 'Tarjeta de crédito') {
                    $credit_card_id = isset($post_data['credit_card_id']) ? $post_data['credit_card_id'] : '';
                    if (empty($credit_card_id)) {
                        $response['posible_fraud'] .= 'Tarjeta de crédito nueva';
                        $response['validation'] = true;
                        return $response;
                    }
                }else{
                    $response['validation'] = true;
                    return $response;
                }
            }

            //validar si tiene pedido en los ultimos 3 dias con tarjeta de credito
            /*$validation = Order::where('orders.user_id', $this->user_id)
        ->where('orders.payment_method', 'Tarjeta de crédito')
        ->where('orders.date', '>', date('Y-m-d H:i:s', strtotime('-72 hours')))
        ->select('orders.id')
        ->first();
        if ($validation)
        $response['validation'] = true;*/
        }

        if (!empty($response['posible_fraud'])) {
            $response['validation'] = true;
            $response['posible_fraud'] = trim($response['posible_fraud']);
        }

        return $response;
    }

    /**
     * Realiza cobro a tarjeta debito con PSE
     *
     * @param  bool|int $order_id ID del pedido
     * @param  bool|int $user_id  ID del usuario
     * @return array
     */
    public function preChargeCreditCard($user_id = false){

        Log::info('generando precobro de tarjeta');
        $preCharge = Order::chargeCreditCard($this->id, $user_id, null, true);
        $user = User::find($user_id);
        if(!$preCharge['status']){
            $this->status = 'Validation';
            $this->order_validation_reason_id = OrderValidationReason::NEW_CREDIT_CARD;
            $this->payment_date = null;
            $this->save();
            $orderGroup = OrderGroup::find($this->group_id);

            send_sms($user->phone, 'MERQUEO: Tuvimos algunos inconvenientes con el método de pago elegido para tu pedido. Por favor intenta con otro medio de pago.');
            return array('status'=>false, 'message' => 'Tuvimos algunos inconvenientes con el método de pago elegido para tu pedido. Por favor intenta con otro medio de pago.');
        }else{
            $preCharge['order_payment'] ;
            $orderGroup = OrderGroup::find($this->group_id);


            send_sms($user->phone, 'MERQUEO: Hemos realizado un cargo de $'.Config::get('app.payu.minimum_charge').' pesos a tu tarjeta para garantizar la seguridad de tu pedido. Este es un cobro temporal que será devuelto.');

            $orderPayments = $preCharge['order_payment'];
            if ($orderPayments && !empty($orderPayments->cc_charge_id)) {
                $orderPayments = "";
                $date = Carbon::now()->addSeconds(10);
                Queue::later($date, \jobs\preChargeCreditCard::class, ['orderId' => $this->id], 'quick');
            }

            $this->payment_date = null;
            $this->save();
            return array('status' => true, 'message'=> 'Hemos realizado un cargo de $'.Config::get('app.payu.minimum_charge').' pesos a tu tarjeta para garantizar la seguridad de tu pedido. Este es un cobro temporal que será devuelto.' );
        }

    }

    /**
     * Realiza cobro a tarjeta debito con PSE
     *
     * @param  bool|int $order_id ID del pedido
     * @param  bool|int $user_id  ID del usuario
     * @return array
     */
    public function chargePSE($order_id = false, $user_id = false, $pse_data)
    {
        if ($order_id && $user_id) {
            $data['total_amount'] = $this->total_amount + $this->delivery_amount - $this->discount_amount;
            $data['tax_amount'] = 0; //$total * 0.16;
            $data['order_id'] = $this->id;
            $data['user_id'] = $user_id;
            $data['description'] = 'Compra en Merqueo';
            $data['total_base'] = 0;
            $data['iva'] = 0;
            $data['device_session_id'] = md5(session_id().microtime());

            $user = User::find($this->user_id);
            $order_group = OrderGroup::find($this->group_id);
            $city = City::find($order_group->user_city_id);

            $data['buyer']['document_number'] = $user->identity_number;
            $data['buyer']['fullname'] = $order_group->user_firstname.' '.$order_group->user_lastname;
            $data['buyer']['email'] = $order_group->user_email;
            $data['buyer']['phone'] = $order_group->user_phone;
            $data['buyer']['address'] = $order_group->user_address;
            $data['buyer']['address_further'] = $order_group->user_address_further;
            $data['buyer']['city'] = $city->city;
            $data['buyer']['state'] = $city->state;

            $data['payer']['document_number'] = $pse_data['document_number_pse'];
            $data['payer']['document_type'] = $pse_data['document_number_pse'];
            $data['payer']['fullname'] = $pse_data['name_pse'];
            $data['payer']['email'] = $order_group->user_email;
            $data['payer']['phone'] = $pse_data['phone_pse'];
            $data['payer']['address'] = $order_group->user_address;
            $data['payer']['address_further'] = $order_group->user_address_further;
            $data['payer']['city'] = $city->city;

            $data['person_type'] = $pse_data['type_person_pse'];
            $data['bank_code'] = $pse_data['bank_pse'];
            $data['document_type'] = $pse_data['document_type_pse'];
            $data['document_number'] = $pse_data['document_number_pse'];

            $payu = new PayU();

            $result = $payu->pseCharge($data + ['order_reference' => $this->reference]);
            if (!$result['status']) {
                $msg = isset($result['response']->transactionResponse->responseCode) ? $payu->getErrorMessage($result['response']->transactionResponse->responseCode) : 'No especificado.';

                return ['status' => false, 'message' => 'Ocurrio un error al procesar el pago. Motivo: '.$msg];
            }
            $order_payment = new OrderPayment();
            $order_payment->cc_payment_status = 'Pendiente';
            $order_payment->order_id = $this->id;
            $order_payment->amount = $data['total_amount'];
            $order_payment->cc_payment_description = 'Solicitud pago con PSE';
            $order_payment->cc_payment_transaction_id = $result['response']->transactionResponse->transactionId;
            $order_payment->cc_charge_id = $result['response']->transactionResponse->orderId;
            $order_payment->cc_payment_date = date('Y-m-d H:i:s');
            $order_payment->save();

            $response = [
                'status' => true,
                'message' => 'El cobro fue realizado con éxito.',
                'response' => $this->getPseGetawayFromPayUResponse($result),
            ];

            return $response;
        }

        return ['status' => false, 'message' => 'El cobro no fue realizado.'];
    }

    /**
     * Obtiene la URL para llevar al cliente al portal de pago PSE a partir de
     * los datos la respuesta a PayU. El modelo de datos de la respuesta de PayU.
     *
     * @param  array  $payUResponse La respuesta a la primera llamada de PayU
     * @return string Url para del portal de pago PSE
     */
    private function getPseGetawayFromPayUResponse($payUResponse)
    {
        // aquí obtenemos un array de "entradas"
        $entries = $payUResponse['response']->transactionResponse->extraParameters->entry;

        // filtramos las entradas que tengan 'BANK_URL' en el atributo "string"
        // el cual es también un array
        $bankUrlEntry = array_first($entries, function ($key, $value) {
            return in_array('BANK_URL', $value->string);
        });

        // $bankUrlEntry es un array con dos elementos:
        // - BANK_URL -> el nombre de esta entrada
        // - https://some-url.com/ -> el valor de la entrada
        // los que nos importa es el valor de la entrada, así que lo obtenemos
        $bankUrl = array_first($bankUrlEntry->string, function ($key, $value) {
            return $value !== "BANK_URL";
        });

        return $bankUrl;
    }

    /**
     * Realiza cobro a tarjeta de credito del pedido
     *
     * @param  bool        $order_id
     * @param  bool        $user_id
     * @param  null        $picker_id
     * @throws Exception
     * @return array
     */
    public static function chargeCreditCard($order_id = false, $user_id = false, $picker_id = null, $preCharge = false)
    {
        if ($order_id && $user_id) {
            $order = self::find($order_id);
            $payments = $order->getPayments(true);

            if ($order && $order->user_id == $user_id && (!$payments || empty($payments->cc_charge_id) || !empty($payments->cc_refund_date))) {
                $store = Store::find($order->store_id);
                $order_products = OrderProduct::where('order_id', $order->id)->where('fulfilment_status', 'Fullfilled')->get();
                if (count($order_products)) {
                    $total_amount = $total_base = $iva = 0;
                    foreach ($order_products as $order_product) {
                        $total_amount = $total_amount + ($order_product->quantity * $order_product->price);
                        //calcular iva
                        if (!empty($order_product->iva)) {
                            $base_price = round($order_product->price / (($order_product->iva / 100) + 1), 2);
                            $iva += ($order_product->price - $base_price) * $order_product->quantity;
                            $total_base += $base_price;
                        }
                    }
                    $total = round($total_amount + $order->delivery_amount - $order->discount_amount);
                }elseif($preCharge === false){
                    $response = array('status' => false, 'message' => 'No se puede cobrar el pedido por que no tiene productos disponibles.');
                    return $response;
                }

                if ($preCharge === false && !$total){
                    $order->payment_date = date('Y-m-d H:i:s');
                    $order->save();
                    return ['status' => true, 'message' => 'El cobro fue realizado con éxito.'];
                }

                $data['total_amount'] = ($preCharge === true)? Config::get('app.payu.minimum_charge') : $total;
                $data['tax_amount'] = 0; //$total * 0.16;
                $data['card_token'] = $order->cc_token;
                $data['order_id'] = $order->id;
                $data['user_id'] = $user_id;
                $data['installments_cc'] = $order->cc_installments ? $order->cc_installments : 1;
                $data['description'] = 'Compra en Merqueo';

                $order_payment = new OrderPayment();
                $order_payment->is_precharge = $preCharge ? 1: 0;
                $order_payment->amount = $data['total_amount'];

                // PayU
                if (strlen($order->cc_token) == Config::get('app.payu.token_length')) {
                    $data['total_base'] = 0;
                    $data['iva'] = 0;
                    $data['card_type'] = $order->cc_type;
                    $data['security_code'] = $order->cc_type == 'AMEX' ? '0000' : '000';
                    $data['device_session_id'] = md5(session_id().microtime());
                    $data['referenceCode'] = $order->reference;

                    $user = User::find($order->user_id);
                    $order_group = OrderGroup::find($order->group_id);
                    $data['buyer']['document_number'] = $user->identity_number;
                    $data['buyer']['fullname'] = $order_group->user_firstname.' '.$order_group->user_lastname;
                    $data['buyer']['email'] = $order_group->user_email;
                    $data['buyer']['phone'] = $order_group->user_phone;
                    $data['buyer']['address'] = $order_group->user_address;
                    $data['buyer']['address_further'] = $order_group->user_address_further;
                    $city = City::find($order_group->user_city_id);
                    $data['buyer']['city'] = $city->city;
                    $data['buyer']['state'] = $city->state;

                    $user_credit_card = UserCreditCard::find($order->credit_card_id);
                    if(empty($user_credit_card)){
                        return ['status'=> false, 'message' => 'La tarjeta de credito asociada a este pedido fue eliminada. Favor contactar al usuario.'];
                    }

                    $data['payer']['document_number'] = $order->cc_holder_document_number;
                    $data['payer']['document_type'] = $order->cc_holder_document_type;
                    $data['payer']['fullname'] = $order->cc_holder_name;
                    $data['payer']['email'] = !empty($order->cc_holder_email) ? $order->cc_holder_email : $user_credit_card->holder_email;
                    $data['payer']['phone'] = $order->cc_holder_phone;
                    $data['payer']['address'] = !empty($order->cc_holder_address) ? $order->cc_holder_address : $user_credit_card->holder_address;
                    $data['payer']['address_further'] = $user_credit_card->holder_address_further;
                    $data['payer']['city'] = !empty($order->cc_holder_city) ? $order->cc_holder_city : $user_credit_card->holder_city;

                    $payu = new PayU();
                    $result = $payu->createCreditCardCharge($data, $preCharge);
                    if (!$result['status']) {
                        if ($picker_id) {
                            $order->pending_payment_response = 1;
                            $order->save();
                        }
                        $msg = isset($result['response']->transactionResponse->responseCode) ? $payu->getErrorMessage($result['response']->transactionResponse->responseCode) : 'No especificado.';

                        return ['status' => false, 'message' => 'Cobro a tarjeta rechazado. Motivo: '.$msg];
                    }

                    $order_payment->order_id = $order->id;
                    if ($result['response']->transactionResponse->state == 'APPROVED') {
                        $order_payment->cc_payment_status = 'Aprobada';
                        $order->payment_date = date('Y-m-d H:i:s');
                        if($preCharge){
                            OrderLog::create([
                                'type' => 'Precobro a tarjeta exitoso.',
                                'admin_id' => Config::get('app.dashboard_admin_id'),
                                'order_id' => $order->id,
                            ]);
                            $order->payment_date = null;
                        }
                    } else if ($result['response']->transactionResponse->state == 'DECLINED') {
                        $order_payment->cc_payment_status = 'Declinada';
                    } else if ($result['response']->transactionResponse->state == 'EXPIRED') {
                        $order_payment->cc_payment_status = 'Expirada';
                    }

                    $order_payment->cc_payment_description = $result['response']->transactionResponse->responseCode;
                    $order_payment->cc_payment_transaction_id = $result['response']->transactionResponse->transactionId;
                    $order_payment->cc_charge_id = $result['response']->transactionResponse->orderId;
                    $order_payment->cc_payment_date = date('Y-m-d H:i:s');
                    if (!$picker_id) {
                        $order_payment->admin_id = Session::get('admin_id');
                    } else {
                        $order_payment->picker_id = $picker_id;
                    }

                Log::info('payment log setted', [(array) $order_payment]);

                } else if(!$preCharge) {

                    if ($order->pending_payment_response) {
                        return ['status' => false, 'message' => 'No se puede cobrar el pedido por que tiene un cobro en proceso en TPAGA.'];
                    }

                    //bloquear pedido para no hacer mas de un cobro
                    $order->pending_payment_response = 1;
                    $order->save();
                    DB::commit();

                    DB::beginTransaction();
                    $data['nit'] = $store->nit;
                    $payment = new Payment();
                    $result = $payment->createCreditCardCharge($data);
                    //desbloquear pedido para no permitir cobro
                    $order->pending_payment_response = 0;
                    $order->save();

                    if (!$result['status']) {
                        $msg = isset($result['response']->errorMessage) ? $result['response']->errorMessage : 'No especificado.';

                        return ['status' => false, 'message' => 'Cobro a tarjeta rechazado. Motivo: '.$msg];
                    }
                    $order_payment->order_id = $order->id;
                    $order_payment->cc_charge_id = $result['response']->id;
                    $order_payment->cc_payment_status = 'Aprobada';

                    if (!$picker_id) {
                        $order_payment->admin_id = Session::get('admin_id');
                    } else {
                        $order_payment->picker_id = $picker_id;
                    }

                    $order_payment->cc_payment_transaction_id = $result['response']->paymentTransaction;
                    $order_payment->cc_payment_date = date('Y-m-d H:i:s');
                    if($preCharge === false)
                        $order->payment_date = date('Y-m-d H:i:s');
                }
                $order_payment->save();
                $order->save();

                Log::info('payment log created', [(array) $order_payment]);

                return ['status' => true, 'message' => 'El cobro fue realizado con éxito.', 'order_payment'=>$order_payment ];
            }
        }

        return ['status' => false, 'message' => 'El cobro no fue realizado.'];
    }

    /**
     * Obtener descuentos globales activos para checkout
     *
     * @param  int     $city_id
     * @return array
     */
    public static function getCheckoutDiscounts($city_id = 1)
    {
        $discounts = Discount::where('status', 1)
            ->where('city_id', '<>', self::MEXICO_IDS)
            ->get()
            ->toArray();
        $checkout = [];
        foreach ($discounts as $key => $discount) {
            if ($discount['type'] == 'free_delivery') {

                $currentDatetime = strtotime(Carbon::now()->format('Y-m-d H:i:s'));
                $discount['start_date'];
                $discount['expiration_date'];

                if ($currentDatetime >= strtotime($discount['start_date']) &&
                    $currentDatetime <= strtotime($discount['expiration_date'])
                ) {
                    $checkout['free_delivery']['status'] = 1;
                    $checkout['free_delivery']['store_ids'] = (string) $discount['store_ids'];
                    $checkout['free_delivery']['city_ids'] = (string) $discount['city_id'];
                    $checkout['free_delivery']['minimum_order_amount'] = $discount['minimum_order_amount'];
                }
            }
            if ($discount['type'] == 'discount_credit') {
                $checkout['discount_credit']['status'] = 1;
                $checkout['discount_credit']['order_for_tomorrow'] = (int) $discount['order_for_tomorrow'];
                $checkout['discount_credit']['store_id'] = (int) $discount['store_ids'];
                $checkout['discount_credit']['department_id'] = (int) $discount['department_id'];
                $checkout['discount_credit']['shelve_id'] = (int) $discount['shelf_id'];
                $checkout['discount_credit']['store_product_id'] = (int) $discount['store_product_id'];
                $checkout['discount_credit']['amount'] = (int) $discount['amount'];
                $checkout['discount_credit']['minimum_order_amount'] = (int) $discount['minimum_order_amount'];
                $checkout['discount_credit']['maximum_order_amount'] = (int) $discount['maximum_order_amount'];
            }
            if ($discount['type'] == 'discount_percentage' /*&& $discount['city_id'] == $city_id*/) {
                $checkout['discount_percentage']['status'] = 1;
                $checkout['discount_percentage']['order_for_tomorrow'] = (int) $discount['order_for_tomorrow'];
                $checkout['discount_percentage']['store_id'] = (int) $discount['store_ids'];
                $checkout['discount_percentage']['department_id'] = (int) $discount['department_id'];
                $checkout['discount_percentage']['shelve_id'] = (int) $discount['shelf_id'];
                $checkout['discount_percentage']['store_product_id'] = (int) $discount['store_product_id'];
                $checkout['discount_percentage']['amount'] = floatval($discount['amount']);
                $checkout['discount_percentage']['minimum_order_amount'] = floatval($discount['minimum_order_amount']);
                $checkout['discount_percentage']['maximum_order_amount'] = (int) $discount['maximum_order_amount'];
            }
        }

        if (empty($checkout['free_delivery'])) {
            $checkout['free_delivery'] = ['status' => 0];
        }

        if (empty($checkout['discount_credit'])) {
            $checkout['discount_credit'] = ['status' => 0];
        }

        if (empty($checkout['discount_percentage'])) {
            $checkout['discount_percentage'] = ['status' => 0];
        }

        return $checkout;
    }

    /**
     * Obtiene detalle de pedido
     *
     * @param  int    $order_id ID pedido
     * @param  int    $user_id  ID usuario
     * @return object $order Datos de pedido
     */
    public static function getDetail($user_id, $order_id)
    {
        $order = self::select(
            'orders.id',
            'orders.reference',
            'orders.status',
            'store_id',
            'date',
            'total_products',
            'orders.total_amount',
            'orders.delivery_date',
            'orders.delivery_time',
            'orders.total_amount AS subtotal',
            'orders.delivery_amount',
            'orders.discount_amount',
            'orders.payment_method',
            'orders.management_date',
            'orders.delivery_window_id',
            'source',
            DB::raw("CONCAT(order_groups.user_address, ' ', order_groups.user_address_further) AS user_address"),
            'stores.name AS store_name'
        )
            ->join('order_groups', 'group_id', '=', 'order_groups.id')
            ->join('stores', 'store_id', '=', 'stores.id')
            ->where('orders.user_id', $user_id)
            ->where('orders.id', $order_id)
            //->where('source', '<>', 'Reclamo')
            ->first();

        if (!$order) {
            return false;
        }

        $status = [
            'Validation' => 'Recibido',
            'Initiated' => 'Recibido',
            'Enrutado' => 'Recibido',
            'In Progress' => 'Recibido',
            'Alistado' => 'Alistado',
            'Dispatched' => 'Despachado',
            'Delivered' => 'Entregado',
            'Cancelled' => 'Cancelado',
        ];

        $order->status = $status[$order->status];
        $order->discount_amount = $order->discount_amount;
        $order->subtotal = $order->subtotal;
        $order->total_amount = $order->total_amount + $order->delivery_amount - $order->discount_amount;
        $order_products = OrderProduct::select(
            'store_product_id AS id',
            'price',
            'quantity_original AS quantity_cart_original',
            'quantity AS quantity_cart',
            'product_name AS name',
            'product_image_url AS image_url',
            'product_quantity AS quantity',
            'product_unit AS unit',
            'fulfilment_status AS status',
            'type'
        )
            ->where('order_id', $order->id)
            ->where('is_gift', 0)
            ->where('parent_id', 0)
            ->whereRaw("IF('Reclamo' <> '".$order->source."', type <> 'Muestra', TRUE)")
            ->orderBy(DB::raw("FIELD(fulfilment_status, 'Not Available', 'Fullfilled', 'Replacement')"))
            ->get();
        //obtener productos regalo
        $assigned_gifts = [];
        if ($order->date > '2018-01-01 00:00:00') {
            foreach ($order_products as $key => $order_product) {
                if (empty($assigned_gifts[$order_product->id])) {
                    $assigned_gifts[$order_product->id] = 1;
                    $order_product_sampling = OrderProduct::select(
                        'store_product_id AS id',
                        'price',
                        'quantity_original AS quantity_cart_original',
                        'quantity AS quantity_cart',
                        'product_name AS name',
                        'product_image_url AS image_url',
                        'product_quantity AS quantity',
                        'product_unit AS unit',
                        'fulfilment_status AS status',
                        'sampling_id',
                        'campaign_gift_id',
                        'is_gift',
                        'order_products.type'
                    )
                        ->where('parent_id', $order_product->id)
                        ->where('order_id', $order->id)
                        ->whereIn('type', ['Muestra', 'Product'])
                        ->get();
                } else {
                    $order_product_sampling = [];
                }

                $sampling = null;
                if (count($order_product_sampling) > 0) {
                    $order_product_sampling = $order_product_sampling->first();

                    if (!$order_product_sampling->is_gift && $order_product_sampling->type == 'Muestra' && $order_product_sampling->status == 'Fullfilled') {
                        $sampling = Sampling::find($order_product_sampling->sampling_id);
                    }

                    if ($order_product_sampling->is_gift) {
                        $sampling = \CampaignGift::find($order_product_sampling->campaign_gift_id);
                    }

                    if ($sampling) {
                        $order_product_sampling->message = "Merqueo y {$sampling->brand} te regalan:";
                        $order_product->gift = $order_product_sampling->toArray();
                    }
                }

                if ($order_product->status == 'Not Available') {
                    $order_product_gift = OrderProduct::select(
                        'store_product_id AS id',
                        'price',
                        'quantity_original AS quantity_cart_original',
                        'quantity AS quantity_cart',
                        'product_name AS name',
                        'product_image_url AS image_url',
                        'product_quantity AS quantity',
                        'product_unit AS unit',
                        'fulfilment_status AS status'
                    )
                        ->where('parent_id', $order_product->id)
                        ->where('order_id', $order->id)
                        ->where('is_gift', 1)
                        ->first();
                    if ($order_product_gift) {
                        $order_product_gift->message = 'Lamentamos que este producto no esté disponible, sin embargo te damos este completamente gratis.';
                        $order_product->gift = $order_product_gift->toArray();
                    }
                }
            }
        }

        $country = Country::whereHas('cities', function ($query) use ($order) {
            $query->where('cities.coverage_store_id', $order->store_id);
        })->first();

        if ($country->country_code === Country::COUNTRY_CODE_MEXICO
            && $order->payment_method === PaymentMethod::DATAFONO
        ) {
            $order->payment_method = PaymentMethod::TERMINAL;
        }

        //sort($order_products);
        //valid if the order is type express
        $order->is_express = $order->deliveryWindow->shifts === self::SHIFT_EXPRESS;
        $order->products = $order_products;

        return $order;
    }

    /**
     * Obtiene flujo de estado de pedido y actualiza Firebase
     *
     * @param  int     $order_id             ID de pedido
     * @param  int     $user_id              ID de usuario
     * @param  boolean $update_firebase_data Actualizar datos en Firebase
     * @param  boolean $return_next_orders   Determina si se retorna las siguientes ordenes.
     * @param  bool    $enable_recursion     Evita caer en bucles infinitos, FIXME: encontrar otra forma de hacer esto.
     * @return array   $response Respuesta
     */
    public static function getStatusFlow(
        $order_id,
        $user_id,
        $update_firebase_data = true,
        $return_next_orders = false,
        $enable_recursion = true
    ) {
        if (!$order_id || !$user_id) {
            return [
                'status' => false,
                'message' => 'Hay datos obligatorios vacios.',
            ];
        }

        try {
            $order = self::select(
                'orders.*',
                'order_groups.user_address',
                'order_groups.user_address_further',
                'order_groups.user_address_latitude',
                'order_groups.user_address_longitude',
                'vehicles.plate',
                DB::raw('CONCAT(first_name, " ", last_name) AS fullname_driver')
            )
                ->join('order_groups', 'group_id', '=', 'order_groups.id')
                ->leftJoin('vehicles', 'vehicle_id', '=', 'vehicles.id')
                ->leftJoin('drivers', 'orders.driver_id', '=', 'drivers.id')
                ->where('orders.user_id', $user_id)
                ->where('orders.id', $order_id)
                //->where('source', '<>', 'Reclamo')
                ->first();
            if ($order) {
                $hash = new Hashids();
                //pedido cancelado
                if ($order->status == 'Cancelled') {
                    $order_data = [
                        'reference' => $order->reference,
                        'statuses' => [
                            [
                                'is_checked' => 1,
                                'checked' => 1,
                                'status' => 'Recibido',
                                'message' => format_date('normal_long_with_time', $order->date),
                            ],
                            [
                                'is_checked' => 1,
                                'checked' => 1,
                                'status' => 'Cancelado',
                                'message' => format_date('normal_long_with_time', $order->management_date),
                            ],
                        ],
                        'hash_code' => $hash->encode($order->id),
                    ];

                    $response = [
                        'status' => true,
                        'message' => 'Flujo de pedido obtenido',
                        'result' => $order_data,
                    ];
                } else {
                    $delivery_time = explode(' - ', in_array($order->deliveryWindow->shifts, [
                        self::SHIFT_EXPRESS,
                        self::SHIFT_FAST_DELIVERY
                    ])
                        ? sprintf('%s - %s',date('g:i a'),date('g:i a'))
                        : $order->delivery_time
                    );
                    $delivery_date = sprintf('%s %s', date(
                        'Y-m-d H:i:s',
                        strtotime(substr($order->delivery_date, 0, 10))),
                        $delivery_time[0]
                    );
                    $picking_date = format_date('normal_day_with_time', $delivery_date);
                    $dispatched_date = format_date('normal_day_with_time', $delivery_date);

                    //validar si tiene productos faltantes
                    $missing_products = $order->hasMissingProducts();
                    if (in_array($order->status, ['Alistado', 'Dispatched']) && $missing_products) {
                        $enlisted_message = $missing_products == 1 ? $missing_products.' producto no estaba disponible.' : $missing_products.' productos no estaban disponibles.';
                        $enlisted_message = 'Hay novedades en tu pedido, '.$enlisted_message;
                    } else {
                        $enlisted_message = '';
                    };

                    $dispatched_message = $order->status == 'Dispatched' ? 'Tu pedido esta en ruta.' : '';

                    //fecha de entrega
                    if ($order->status == 'Dispatched' && !empty($order->ontheway_notification_date)) {
                        $dispatched_message = 'Pronto llegaremos a tu ubicación.';
                        if (!empty($order->onmyway_date) && empty($order->arrived_date)) {
                            $dispatched_message = 'Tu pedido va en camino.';
                        } elseif (!empty($order->onmyway_date) && !empty($order->arrived_date)) {
                            $dispatched_message = 'Tu pedido ha llegado, revisa que los contenedores vengan sellados.';
                        }

                        $hour1 = date('g:i a', strtotime($order->estimated_arrival_date.' -30 minutes'));
                        $hour2 = date('g:i a', strtotime($order->estimated_arrival_date.' +30 minutes'));
                        $delivery_date = [
                            'date' => $hour1.' - '.$hour2,
                            'message' => 'RANGO DE ENTREGA ESTIMADO',
                        ];

                        //calcular fecha estimada de entrega
                        if (date('Y-m-d H:i:s') > $order->delivery_date) {
                            /*$result = $order->getEstimatedArrivalTime();
                            if ($result['status']){
                            $delivery_date = [
                            'date' => $result['result'],
                            'message' => 'HORA DE ENTREGA ESTIMADA'
                            ];
                            }else{*/
                            $hour1 = date('g:i a', strtotime(' -5 minutes'));
                            $hour2 = date('g:i a', strtotime(' +55 minutes'));
                            $delivery_date = [
                                'date' => $hour1.' - '.$hour2,
                                'message' => 'RANGO DE ENTREGA ESTIMADO',
                            ];
                            //}
                        } else {
                            if (date('Y-m-d H:i:s') > $order->estimated_arrival_date) {
                                $hour1 = date('g:i a', strtotime(' -5 minutes'));
                                $hour2 = date('g:i a', strtotime(' +55 minutes'));
                                $delivery_date = [
                                    'date' => $hour1.' - '.$hour2,
                                    'message' => 'RANGO DE ENTREGA ESTIMADO',
                                ];
                            }
                        }
                    } elseif ($order->status == 'Delivered') {
                        $delivery_date = [
                            'date' => format_date('normal_long_with_time', $order->management_date),
                            'message' => 'Tu pedido ya fue entregado',
                        ];
                    } else {
                        $delivery_date = [
                            'date' => $order->delivery_time,
                            'message' => 'RANGO DE ENTREGA SELECCIONADO',
                        ];
                    }

                    //en camino
                    if ($order->status == 'Dispatched' && !empty($order->ontheway_notification_date)) {
                        //obtener pedidos a entregar antes
                        $orders = self::select('orders.status', 'order_groups.user_address_latitude', 'order_groups.user_address_longitude')
                            ->join('order_groups', 'group_id', '=', 'order_groups.id')
                            ->where('orders.route_id', $order->route_id)
                            ->where('orders.planning_sequence', '<', $order->planning_sequence)
                            ->where('orders.planning_sequence', '>=', $order->planning_sequence - 2)
                        //->where('source', '<>', 'Reclamo')
                        ->whereNotNull('ontheway_notification_date')
                        ->orderBy('planning_sequence')
                        ->get();

                        $orders = $orders ? $orders->toArray() : [];

                        $cloudfront_url = Config::get('app.aws.cloudfront_url');

                        $on_the_way_info = [
                            'delivery' => [
                                'address' => $order->user_address,
                                'location' => $order->user_address_latitude.','.$order->user_address_longitude,
                            ],
                            'driver' => [
                                'name' => $order->fullname_driver,
                                'plate' => $order->plate,
                                'image' => $cloudfront_url.'/shoppers/photos/avatar.png',
                                'vehicle_id' => $order->vehicle_id,
                            ],
                            'orders_before' => $orders,
                        ];
                    } else {
                        $on_the_way_info = [];
                    }

                    $order_data = [
                        'statuses' => [
                            [
                                'is_checked' => 1,
                                'checked' => 1,
                                'status' => 'Recibido',
                                'message' => format_date('normal_long_with_time', $order->date),
                            ],
                            [
                                'is_checked' => in_array($order->status, ['Alistado', 'Dispatched', 'Delivered']) ? 1 : 0,
                                'checked' => in_array($order->status, ['Alistado', 'Dispatched', 'Delivered']) ? 1 : 0,
                                'status' => 'Alistado',
                                'message' => !in_array($order->status, ['Alistado', 'Dispatched', 'Delivered'])
                                    ? 'Empezaremos a alistar tu pedido '.$picking_date
                                    : $enlisted_message,
                                //'message_details' => $missing_products,
                            ],
                            [
                                'is_checked' => in_array($order->status, ['Dispatched', 'Delivered']) ? 1 : 0,
                                'checked' => in_array($order->status, ['Dispatched', 'Delivered']) ? 1 : 0,
                                'status' => 'Despachado',
                                'message' => !in_array($order->status, ['Dispatched', 'Delivered'])
                                    ? 'Tu pedido saldrá de nuestra bodega '.$dispatched_date
                                    : $dispatched_message,
                                'is_on_the_way' => $on_the_way_info,
                                'on_the_way' => $on_the_way_info,
                            ],
                            [
                                'is_checked' => $order->status == 'Delivered' ? 1 : 0,
                                'checked' => $order->status == 'Delivered' ? 1 : 0,
                                'status' => 'Entregado',
                                'message' => $order->status != 'Delivered'
                                    ? 'Tu pedido será entregado en la '.trim($order->user_address.' '.$order->user_address_further)
                                    : format_date('normal_long_with_time', $order->management_date),
                            ],
                        ],
                        'reference' => $order->reference,
                        'delivery_date' => $delivery_date,
                        'pending_score' => $order->status == 'Delivered' && empty($order->user_score_date) ? 1 : 0,
                        'updated_at' => date('Y-m-d H:i:s'),
                        'driver' => $order->fullname_driver,
                        //retornar codigo de transportador para gestionar mapa con mensajeros urbanos
                        'transporter_provider_code' => is_null($order->driver) || is_null($order->driver->transporter) ? 'merqueo' : strpos($order->driver->transporter->fullname, 'MU TEAM') !== false ? 'mu' : 'merqueo',
                        'vehicle_id' => strval($order->vehicle_id),
                        'hash_code' => $hash->encode($order->id),
                        'follow_order_url' => route('user.followOrder', [
                            'user_id' => $order->user_id,
                            'order_reference' => $order->reference,
                            'wb' => 1,
                        ]),
                    ];

                    if (!in_array($order->status, ['Alistado', 'Dispatched', 'Delivered'])) {
                        $order_data['init_message'] = '¡Hemos recibido tu pedido!. No te preocupes, te estaremos notificando el estado de tu pedido.';
                    }

                    foreach ($order_data['statuses'] as $key => $status) {
                        if (isset($status['message_details']) && empty($status['message_details'])) {
                            unset($order_data['statuses'][$key]['message_details']);
                        }

                        if ((isset($status['is_on_the_way']) && empty($status['is_on_the_way']))
                            && (isset($status['on_the_way']) && empty($status['on_the_way']))
                        ) {
                            unset($order_data['statuses'][$key]['is_on_the_way']);
                            unset($order_data['statuses'][$key]['on_the_way']);
                        }

                    }

                    $response = [
                        'status' => true,
                        'message' => 'Flujo de pedido obtenido',
                        'result' => $order_data,
                    ];
                }

                //actualizar datos en firebase
                $management_date = new DateTime($order->management_date);
                $current_date = new DateTime();
                $diff = $current_date->diff($management_date);
                $hours = $diff->h;
                $hours = $hours + ($diff->days * 24);
                if (!empty($order->management_date) && $hours > 12) {

                } else {
                    if ($update_firebase_data) {
                        $firebase = new FirebaseClient('general');
                        $firebase->setData('orders/'.$order->id, $order_data);
                    }
                }

                //actualizar siguientes pedidos de ruta en firebase
                $next_orders = [];
                if (($update_firebase_data || $return_next_orders) && !empty($order->route_id) && !empty($order->management_date) && $hours < 12 && $enable_recursion) {
                    //obtener pedidos a entregar despues
                    $orders = self::select('orders.id', 'orders.user_id')
                        ->join('order_groups', 'group_id', '=', 'order_groups.id')
                        ->where('orders.route_id', $order->route_id)
                        ->where('orders.planning_sequence', '>', $order->planning_sequence)
                        ->where('orders.planning_sequence', '<=', $order->planning_sequence + 2)
                    //->where('source', '<>', 'Reclamo')
                    ->whereNotNull('ontheway_notification_date')
                    ->orderBy('planning_sequence')
                    ->get();

                    if ($orders) {
                        foreach ($orders as $order) {
                            $result = self::getStatusFlow($order->id, $order->user_id, $update_firebase_data, false, false);
                            $result['result']['id'] = $order->id;
                            $next_orders[] = $result;
                        }
                    }
                }

                $response['next_orders'] = $next_orders;

                return $response;
            } else {
                return [
                    'status' => false,
                    'message' => 'El pedido no pertenece al usuario o no existe.',
                ];
            }
        } catch (Exception $e) {
            ErrorLog::add($e);
            return [
                'status' => false,
                'message' => 'Ocurrió un error generico.',
            ];
        }
    }

    /**
     * Obtener tiempo estimado de entrega de pedido
     */
    public function getEstimatedArrivalTime()
    {
        if ($this->status != 'Dispatched') {
            return ['status' => false, 'message' => 'Estado no valido.'];
        }

        $destination = $this->orderGroup->user_address_latitude.','.$this->orderGroup->user_address_longitude;

        //obtener ultima ubicacion del vehiculo
        $seconds_to_add_per_order = 660;
        $firebase = new FirebaseClient('gps');
        $current_location = $firebase->getData('drivers/'.$this->vehicle_id);
        $current_location = json_decode($current_location);
        if (empty($current_location->lat) || empty($current_location->lng)) {
            return ['status' => false, 'message' => 'No hay datos GPS.'];
        }

        $origin = $current_location->lat.','.$current_location->lng;

        //calcular tiempo de entrega
        $orders = self::select('orders.id', 'orders.delivery_time', 'management_date', 'planning_sequence', 'planning_duration', 'user_address_latitude', 'user_address_longitude', 'cities.latitude', 'cities.longitude')
            ->join('order_groups', 'order_groups.id', '=', 'group_id')
            ->join('stores', 'stores.id', '=', 'orders.store_id')
            ->join('cities', 'cities.id', '=', 'stores.city_id')
            ->where('route_id', $this->route_id)
            ->where('planning_sequence', '<', $this->planning_sequence + 1)
            ->where('orders.status', 'Dispatched')
            ->where('orders.vehicle_id', $this->vehicle_id)
            ->orderBy('planning_sequence')
            ->get();

        if (count($orders)) {
            /*$last_order_estimated_arrival_date = $last_delivery_time = false;
            $last_delivery_time = '';
            foreach($orders as $order)
            {
            //calcular fecha de entrega estimada
            $delivery_time = explode(' - ', $order->delivery_time);
            $minutes = round($order->planning_duration / 60);
            if (!$last_order_estimated_arrival_date){
            $delivery_departure_hour = date('H:i:s', strtotime(date('Y-m-d ').$delivery_time[0].' -20 minutes'));
            $last_order_estimated_arrival_date = date('Y-m-d H:i:s', strtotime(date('Y-m-d '.$delivery_departure_hour).' +'.$minutes.' minutes'));
            }
            //si la franja cambia y la hora de entrega del ultimo pedido es mayor a la hora de inicio de la franja tomar esa
            if ($last_delivery_time && $last_delivery_time != $order->delivery_time){
            if ($last_order_estimated_arrival_date < date('Y-m-d H:i:s', strtotime(date('Y-m-d ').$delivery_time[0].' -20 minutes'))){
            $last_order_estimated_arrival_date = date('Y-m-d H:i:s', strtotime(date('Y-m-d ').$delivery_time[0].' -20 minutes'));
            $delivery_departure_hour = date('H:i:s', strtotime(date('Y-m-d ').$delivery_time[0].' -20 minutes'));
            }else $delivery_departure_hour = date('H:i:s', strtotime($last_order_estimated_arrival_date));
            }
            $order->estimated_arrival_date = $last_order_estimated_arrival_date = date('Y-m-d H:i:s', strtotime($last_order_estimated_arrival_date.' +'.$minutes.' minutes'));
            $last_delivery_time = $order->delivery_time;

            echo $order->planning_sequence.' - '.$order->id.' - '.$order->delivery_time.' - '.$delivery_departure_hour.' - '.$order->estimated_arrival_date.' - '.$order->management_date.'<br>';
            }*/
            //exit;
            $waypoints = '';
            if (count($orders) > 1) {
                foreach ($orders as $order) {
                    $waypoints .= $order->user_address_latitude.','.$order->user_address_longitude.'|';
                }
            }

            $params = [
                'origin' => $origin,
                'destination' => $destination,
                'departure_time' => 'now',
                'key' => Config::get('app.planning_google_api_key'),
                'mode' => 'driving',
                'language' => 'es-Es',
            ];

            if (!empty($waypoints)) {
                $params['waypoints'] = trim($waypoints, '|');
            }

            //calcular teniendo en cuenta todos los pedidos
            $params = http_build_query($params);

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://maps.googleapis.com/maps/api/directions/json?'.$params);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
            $json_response = curl_exec($ch);
            $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);

            $response = json_decode($json_response, true);
            //debug($response);

            //solo este pedido
            if (empty($waypoints)) {
                if (isset($response['routes'][0]['legs'])) {
                    if (isset($response['routes'][0]['legs'][0]['duration_in_traffic'])) {
                        $index = $response['routes'][0]['legs'][0]['duration_in_traffic']['value'] > 600 ? 'duration_in_traffic' : 'duration';
                        $duration = $response['routes'][0]['legs'][0][$index]['value'] + $seconds_to_add_per_order;
                        $duration = round($duration / 60);
                        $duration = date('g:i a', strtotime('+'.$duration.' minutes'));

                        return ['status' => true, 'message' => 'Tiempo de entrega estimado de pedido obtenido.', 'result' => $duration];
                    }

                    return ['status' => false, 'message' => 'Coordenada incorrecta.'];
                }
            } else {
                //varios pedidos
                if (isset($response['routes'][0]['waypoint_order'])) {
                    $duration = 0;
                    foreach ($response['routes'][0]['waypoint_order'] as $key => $value) {
                        if (isset($response['routes'][0]['legs'][0]['duration'])) {
                            $duration += $response['routes'][0]['legs'][$key]['duration']['value'] + $seconds_to_add_per_order;
                        }

                    }
                    $duration = round($duration / 60);
                    $duration = date('g:i a', strtotime('+'.$duration.' minutes'));

                    return ['status' => true, 'message' => 'Tiempo obtenido con todos los pedidos.', 'result' => $duration];
                } else {
                    return ['status' => false, 'message' => 'Ocurrió un error de Google Maps: '.$response['status']];
                }
            }
        }

        return ['status' => false, 'message' => 'No hay datos.'];
    }

    /**
     * Permite obtener todos los pagos o el ultimo pago de un pedido
     *
     * @param  boolean                         $last determina si retorna todos los pagos o solo el último
     * @return \OrderPayment[]|\OrderPayment
     */
    public function getPayments($last = false)
    {
        $payments = OrderPayment::select(
            'order_payments.cc_payment_status',
            'order_payments.cc_payment_description',
            'order_payments.cc_payment_transaction_id',
            'order_payments.cc_charge_id',
            DB::raw('order_payment_refunds.status AS cc_refund_status'),
            DB::raw("IF(order_payment_refunds.status = 'Declinada',null, order_payment_refunds.date) AS cc_refund_date"),
            'order_payments.cc_payment_date'
        )
            ->leftJoin('order_payment_refunds', 'order_payment_refunds.order_payment_id', '=', 'order_payments.id')
            ->where('order_payments.order_id', $this->id)
            ->where('order_payments.is_precharge', 0)
            ->orderBy('order_payments.cc_payment_date', 'DESC')
            ->orderBy('order_payment_refunds.date', 'ASC')
            ->get();

        if ($payments && $last) {
            $payments = $payments->first();
        }

        return $payments;
    }

    /**
     * Retorna el estado del pago de un pedido
     *
     * @return bool
     */
    public function getPaymentStatus(){
        $paidOut = false;
        $payment = $this->getPayments(true);
        switch ($this->payment_method){
            case 'Tarjeta de crédito' :
                if( (!empty($payment) && empty($payment->cc_refund_date) && !empty($this->payment_date) && $payment->cc_payment_status == 'Aprobada')
                    || (!empty($payment) && empty($payment->cc_refund_date) && empty($this->payment_date) && $payment->cc_payment_status == 'Aprobada')
                ){
                    $paidOut = true;
                }
                break;
            case 'Efectivo':
            case 'Efectivo y datáfono':
            case 'Datáfono':
                if(!empty($this->payment_date)){
                    $paidOut = true;
                }
                break;
            case 'Débito - PSE':
                if(!empty($this->payment_date) && $payment->cc_payment_status == 'Aprobada'){
                    $paidOut = true;
                }
                break;
        }
        return $paidOut;
    }

    /**
     * Obtiene el pago segun el numero de transaccion
     *
     * @param  String $cc_payment_transaction_id numero de la transacción retornado de la pasarela de pago
     * @return object \OrderPayment
     */
    public function getPaymentsByTransactionId($cc_payment_transaction_id)
    {
        $payments = OrderPayment::select('orders.*', 'order_payments.cc_payment_status', 'order_payments.cc_payment_date', 'order_payments.cc_charge_id', 'order_payments.cc_payment_transaction_id', DB::raw('order_payment_refunds.status AS cc_refund_status'), DB::raw('order_payment_refunds.date AS cc_refund_date'))
            ->leftJoin('order_payment_refunds', 'order_payment_refunds.order_payment_id', '=', 'order_payments.id')
            ->where('order_payments.cc_payment_transaction_id', $cc_payment_transaction_id)
            ->orderBy('order_payments.cc_payment_date', 'DESC')
            ->orderBy('order_payment_refunds.date', 'ASC')
            ->get();

        if ($payments) {
            return $payments->first();
        }

    }

    /**
     * Se realiza la validación de los datos ingresados para almacenar
     * la calificación de un usuario.
     *
     * @todo Agregar excepciones personalizadas
     * @param  $input
     * @throws HttpException
     * @return \Order
     */
    public static function validateScoreData($input)
    {
        if (empty($input['rating'])) {
            throw new HttpException(400, 'Se debe especificar si el comentario fue positivo.');
        }

        if (empty($input['user_id'])) {
            throw new HttpException(400, 'El ID usuario es requerido.');
        }

        $order = self::where('user_id', $input['user_id'])->find($input['order_id']);
        if (empty($order)) {
            throw new HttpException(400, 'ID pedido no existe.');
        }

        if (!empty($order->user_score_date)) {
            throw new HttpException(200, 'No se guardo tu calificación, ya habias calificado este pedido.');
        }

        if (empty($input['other']) && empty($input['order_products']) && $input['rating'] !== 'success') {
            throw new HttpException(400, 'Se debe indicar alguno de los problemas ocurridos.');
        }

        if (!empty($input['order_products'])) {
            OrderProductScore::validateOrderProductScores($input);
        }

        return $order;
    }

    /**
     * Retorna el formato usado para renderizar el contenido
     * de la calificación de los pedidos.
     *
     * @return array
     */
    public function getOrderScoreFormat()
    {
        $total_orders = count(
            $this->user->orders()
                 ->select('id')
                 ->where('id', '<=', $this->id)
                 ->where('status', \orders\OrderStatus::DELIVERED)
                 ->groupBy('group_id')
                 ->lists('id')
        );

        $available_problems = [
            'No recibí el producto y me lo cobraron.',
            'El producto estaba en mal estado.',
            'El producto estaba vencido.',
            'Recibí el producto equivocado.',
        ];
        $unavailable_problems = [
            'Me molesta no haber recibido este producto.',
        ];

        $others = [
            'Con el pago',
            'Un inconveniente diferente',
        ];

        $titles = [
            'main' => 'Cuéntanos cómo te fue con tu pedido.',
            'bad' => '¿Con qué tuviste inconvenientes?',
            'products' => 'Selecciona los productos con los que tuviste inconvenientes.',
            'available' => '¿Qué inconveniente tuviste?',
            'unavailable' => 'Este producto no pudo ser entregado porque no estaba disponible en inventario.',
            'success' => 'Recibimos tu mensaje. Pronto te daremos respuesta a tu correo electrónico.',
        ];

        $select_fields = [
            'id',
            'order_id',
            \DB::raw('CONCAT(COALESCE(product_name, ""), " ", COALESCE(product_quantity, ""), " ", COALESCE(product_unit, "")) as product_name'),
            'price',
            'product_image_url',
            'fulfilment_status',
        ];

        $order_products = $this->orderProducts()
                               ->select($select_fields)
                               ->where('is_gift', 0)
                               ->orderBy('fulfilment_status', 'DESC')
                               ->orderBy('product_name')
                               ->get();

        if (strtotime("{$this->management_date} + 7 days") > strtotime('now')) {
            $day = format_date('day_name', $this->management_date);
            $hora = date('g:i a', strtotime($this->management_date));
            $date_format = "{$day} a las {$hora}";
        } else {
            $date_format = format_date('normal_day_with_time', $this->management_date);
        }

        $json_response = [
            'details' => [
                'title' => $titles['main'],
                'order_id' => $this->id,
                'management_date' => $date_format,
            ],
            'rating' => [
                'bad' => [
                    'title' => $titles['bad'],
                    'products' => [
                        'title' => $titles['products'],
                        'order_products' => [
                            'available_title' => $titles['available'],
                            'unavailable_title' => $titles['unavailable'],
                            'products_list' => $order_products,
                            'available_problems' => $available_problems,
                            'unavailable_problems' => $unavailable_problems,
                        ],
                    ],
                    'other' => $others,
                ],
                'success' => [
                    'title' => $titles['success'],
                    'rate_modal' => [
                        'shown' => $total_orders % 5 === 0,
                        'title' => '¿Te gusta Merqueo?',
                        'message' => 'Compártenos tu experiencia',
                        'button_text' => 'Calificar',
                    ],
                ],
            ],
        ];

        return $json_response;
    }

    /**
     * Almacena la calificación del pedido realizada por el usuario.
     *
     * @param  $input
     * @return Order
     */
    public function saveOrderScore($input)
    {
        $this->user_score_date = \Carbon\Carbon::now();
        $this->user_score = $good_rate = $input['rating'] === self::GOOD_RATE;

        if (!$good_rate) {
            $other = !empty($input['other']) ? $input['other'] : null;
            $comment = !empty($input['comment']) ? $input['comment'] : null;
            $order_products = !empty($input['order_products']) ? $input['order_products'] : null;

            if (!empty($comment)) {
                $this->user_score_comments = $comment;
            }

            if (!empty($order_products)) {
                OrderProductScore::saveProductScores($order_products);
                $other[] = 'Con los productos';
            }

            if (!empty($other)) {
                $other = array_filter($other, function ($item) {
                    return !empty($item);
                });

                $this->user_score_typification = implode(';', $other);
            }
        }

        $this->save();

        return $this;
    }

    /**
     * Realiza la validación de fraude al crearse el pedido
     */
    public function fraudValidation()
    {
        $fraud_points = 0;
        $message = '';
        $is_first_order = $this->user->orders()
                               ->where('id', '<>', $this->id)
                               ->where('status', '<>', \orders\OrderStatus::CANCELED)
                               ->count() === 0;

        //si es menor al pedido minimo mas el valor definido
        $ticket_amount = $this->store->minimum_order_amount + 10000;
        if ($this->total_amount < $ticket_amount) {
            $fraud_points += 1;
            $message .= 'ticket bajo, ';
        }

        $suspectMaxOrderAmount = Config::get('app.suspect_max_order_amount');
        if ($this->total_amount >= $suspectMaxOrderAmount) {
            $fraud_points += 6;
            $message .= 'excede monto máximo, ';
        }

        //si tiene cupon redimido
        $order_id = $this->id;
        $credit = $this->userCredits->filter(function ($user_credit) use ($order_id) {
            return !empty($user_credit->coupon_id) && !empty($user_credit->order_id) && $user_credit->order_id == $order_id;
        })->count();
        if ($credit) {
            $fraud_points += 1;
            $message .= 'cupón usado, ';
        }

        //si uso creditos
        $credit = $this->userCredits->filter(function ($user_credit) use ($order_id) {
            return empty($user_credit->coupon_id) && !empty($user_credit->order_id) && $user_credit->order_id == $order_id;
        })->count();
        if ($credit) {
            $fraud_points += 1;
            $message .= 'crédito usado, ';
        }

        // Valida que algún usuario referido haya realizado un pedido en la misma dirección.
        $users_there = $is_first_order
            ? User::whereNotNull('referred_by')
            ->whereHas('orderGroups', function ($query) {
                $query->where('user_address_latitude', $this->orderGroup->user_address_latitude)
                      ->where('user_address_longitude', $this->orderGroup->user_address_longitude)
                      ->where('user_id', '<>', $this->user_id);
            })
            ->count()
            : 0;

        if ($users_there > 0) {
            $fraud_points += 3;
            $message .= "{$users_there} usuario(s) con el beneficio de referidos en esa dirección, ";
        }
        $sourceOrder = array_merge(['Web'], User::DARKSUPERMARKET_TYPE);
        if (!in_array($this->orderGroup->source, $sourceOrder) && $this->orderGroup->userDevice) {
            $ordersByDeviceId = OrderGroup::whereHas('userDevice', function($query){
                $query->where('regid', $this->orderGroup->userDevice->regid);
            })
                ->where('user_id', '!=', $this->user_id)
                ->with('userDevice')
                ->groupBy('user_id')
                ->get();
            if($ordersByDeviceId->count() > 2 && $this->discount_amount > 0){
                $fraud_points += 6;
                $message .= $ordersByDeviceId->count()." usuario(s) con pedidos desde el mismo dispositivo y con descuento., ";
            }else if($ordersByDeviceId->count() > 2){
                $fraud_points += 6;
                $message .= $ordersByDeviceId->count()." usuario(s) con pedidos desde el mismo dispositivo., ";
            }
        }

        $deliveryDate = Carbon::now()->format('Y-m-d');
        $usersFromIp = OrderGroup::where('ip', $this->orderGroup->ip)
            ->whereHas('orders', function($query)use($deliveryDate){
                $query->whereDate('delivery_date','>=', $deliveryDate );
            })
            ->groupBy('user_id')
            ->get();

        if($usersFromIp->count() > 2 && $this->discount_amount > 0){
            $fraud_points += 6;
            $message .= $usersFromIp->count()." usuario(s) con pedidos desde la misma dirección IP., ";
        }

        //validar si el pedido del referido es en la misma direccion del promotor
        if (!empty($this->user->referred_by)) {
            $total_orders = OrderGroup::where('user_id', $this->user->referred_by)
                ->where('user_address_latitude', $this->orderGroup->user_address_latitude)
                ->where('user_address_longitude', $this->orderGroup->user_address_longitude)
                ->count();

            if ($total_orders > 0) {
                $fraud_points += 3;
                $message .= 'es la misma dirección del promotor, ';
            }

            //validar si el promotor pide el mismo dia
            $promotor_has_orders_today = $this->user->referred_by && self::where('user_id', $this->user->referred_by)
                                              ->where(\DB::raw('DATE(created_at)'), Carbon::create()->format('Y-m-d'))
                                              ->count() > 0;
            if ($promotor_has_orders_today) {
                $fraud_points += 2;
                $message .= 'el promotor pide el mismo dia, ';
            }

            //validar historial de referidos
            $result = $this->validateReferredHistorical();
            if ($result) {
                $fraud_points += 2;
                $message .= 'mas de 2 referidos cerca de la misma dirección, ';
            }
        }

        //si tiene mas productos con descuento
        /*$products = OrderProduct::select(
        DB::raw('SUM(IF(original_price <> price, 1, 0)) AS discount_products'),
        DB::raw('SUM(IF(original_price = price, 1, 0)) AS normal_products')
        )
        ->where('order_id', $this->id)
        ->first();
        if ($products) {
        if ($products->discount_products > $products->normal_products) {
        $fraud_points += 2;
        $message .= 'mas productos con descuento, ';
        }
        }*/

        // validar si es primera orden y si tiene mas del valor de crédito definido par descuentos
        if ($is_first_order && $this->discount_amount > Config::get('app.suspect_max_order_discount_amount')) {
            $fraud_points += 6;
            $message .= 'El pedido es primera compra y el descuento en el pedido es superior al monto definido para descuentos ('.currency_format(Config::get('app.suspect_max_order_discount_amount')).') , ';
        }

        $message = trim($message, ', ');
        //echo $message.'<br>'.$fraud_points; exit;

        //si tiene los puntos suficientes para entrar en validación
        if ($fraud_points >= 6) {
            $this->status = 'Validation';
            $this->posible_fraud = 'Pedido con posible fraude por que tiene: '.$fraud_points.' puntos, '.$message;
            $this->order_validation_reason_id = OrderValidationReason::FRAUD;
            $this->save();
        }
    }

    /**
     * Valida historial de pedidos con base a referido del pedido actual
     */
    public function validateReferredHistorical()
    {
        $allowed_km = 0.5;
        $user = $this->user->load('referredBy');
        $user_referred_by = $user->referredBy;
        $target['lat'] = $this->orderGroup->user_address_latitude;
        $target['lng'] = $this->orderGroup->user_address_longitude;
        $orders_inside = 0;
        $count = 0;
        $i = 0;
        while (!empty($user_referred_by) && $i <= 4) {
            $first_order = $user_referred_by->orders->first();
            if (!empty($first_order)) {
                $order['lat'] = $first_order->load('orderGroup')->orderGroup->user_address_latitude;
                $order['lng'] = $first_order->load('orderGroup')->orderGroup->user_address_longitude;

                $km_distance = $this->getOrderDistanceFromPoint($order, $target);
                if ($km_distance <= $allowed_km) {
                    $orders_inside++;
                }
                $count++;
            }
            $user_referred_by = $user_referred_by->load('referredBy');
            $user_referred_by = $user_referred_by->referredBy;
            $i++;
        }
        if ($count > 0) {
            if ($orders_inside >= 3) {
                return true;
            }
        }

        return false;
    }

    /**
     * Obtiene distancia de un punto a otro en km
     *
     * @param  $order_lat_lng
     * @param  $target
     * @return float|string
     */
    private function getOrderDistanceFromPoint($order_lat_lng, $target)
    {
        $lat1 = $order_lat_lng['lat'];
        $lng1 = $order_lat_lng['lng'];
        $lat2 = $target['lat'];
        $lng2 = $target['lng'];
        $pi = 3.14159;
        $rad = floatval($pi / 180.0);

        $lon1 = floatval($lng1) * $rad;
        $lat1 = floatval($lat1) * $rad;
        $lon2 = floatval($lng2) * $rad;
        $lat2 = floatval($lat2) * $rad;
        $theta = $lng2 - $lng1;

        $dist = acos(sin($lat1) * sin($lat2) + cos($lat1) * cos($lat2) * cos($theta));

        if ($dist < 0) {
            $dist += $pi;
        }

        $miles = floatval($dist * 69.1);
        $inches = floatval($miles * 63360);
        $km = floatval($dist * 115.1666667);

        $dist = sprintf("%.2f", $dist);
        $miles = sprintf("%.2f", $miles);
        $inches = sprintf("%.2f", $inches);
        $km = sprintf("%.2f", $km);
        //Here you can return whatever you please

        return $km;
    }

    /**
     * @return bool
     */
    public function hasGoodRate()
    {
        return !empty($this->user_score);
    }

    /**
     * Filtra para obtener solo las ordenes de tipo marketplace.
     *
     * @param  $query
     * @return mixed
     */
    public function scopeMarketplace($query)
    {
        return $query->where('orders.type', self::MARKETPLACE);
    }

    /**
     * Scope para filtrar las ordenes que se encuentran en estado Entregado.
     */
    public function scopeDelivered($query)
    {
        return $query->whereStatus('Delivered');
    }

    /**
     * Scope para filtrar las ordenes que se encuentran en estado Cancelado.
     */
    public function scopeCancelled($query)
    {
        return $query->whereStatus('Cancelled');
    }

    /**
     * Scope para filtrar las ordenes cuya secuencia de planeación sea la que se envía por el parámetro
     * en caso de no envíarlo se le asigna por defecto 1.
     *
     * @param int $value Valor a comparar con el campo planningsequeb¡nce.
     */
    public function scopePlannigSequence($query, $value = 1)
    {
        return $query->wherePlanningSequence($value);
    }

    /**
     * Almacena los datos de la tarjeta de credito.
     *
     * @param  UserCreditCard $user_credit_card
     * @param  int            $installments
     * @return Order
     */
    public function assignCreditCard(UserCreditCard $user_credit_card, $installments)
    {
        $this->payment_method = 'Tarjeta de crédito';
        $this->credit_card_id = $user_credit_card->id;
        $this->cc_token = $user_credit_card->card_token;
        $this->cc_holder_name = $user_credit_card->holder_name;
        $this->cc_holder_document_type = $user_credit_card->holder_document_type;
        $this->cc_holder_document_number = $user_credit_card->holder_document_number;
        $this->cc_holder_phone = $user_credit_card->holder_phone;
        $this->cc_holder_email = $user_credit_card->holder_email;
        $this->cc_holder_address = $user_credit_card->holder_address;
        $this->cc_holder_city = $user_credit_card->holder_city;
        $this->cc_bin = $user_credit_card->bin;
        $this->cc_last_four = $user_credit_card->last_four;
        $this->cc_expiration_month = $user_credit_card->expiration_month;
        $this->cc_expiration_year = $user_credit_card->expiration_year;
        $this->cc_type = $user_credit_card->type;
        $this->cc_country = $user_credit_card->country;
        $this->cc_installments = $installments;

        return $this;
    }

    /**
     * @param  string  $company_id
     * @param  string  $company_name
     * @return $this
     */
    public function assignBillToCompany($company_id, $company_name)
    {
        $this->user_identity_type = 'NIT';
        $this->user_identity_number = $company_id;
        $this->user_business_name = $company_name;

        return $this;
    }

    /**
     * @param  $new_payment_method
     * @throws MerqueoException
     * @return $this
     */
    public function changePaymentMethod($new_payment_method)
    {
        $this->payment_method = $new_payment_method;
        if ($this->isDirty('payment_method')) {
            $this->removeVisaDiscount();
        }

        $status = new \orders\OrderValidator($this);
        $status->validatePaymentMethodChange($new_payment_method);

        return $this;
    }

    /**
     * @return bool
     */
    public function hasVisaPaymentMethod()
    {
        return $this->payment_method === 'Tarjeta de crédito' && $this->cc_type === 'VISA';
    }

    /**
     * Remueve el descuento en el domicilio por la campaña de Visa.
     */
    public function removeVisaDiscount()
    {
        $has_visa_discount = $this->getOriginal('payment_method') === 'Tarjeta de crédito' && $this->cc_type === 'VISA';

        if ($has_visa_discount) {
            $campaign = $this->orderDiscount()
                             ->where('description', 'visa_domicilio_4500')
                             ->first();

            if ($campaign) {
                $this->delivery_amount = 4500;
                $campaign->delete();
            }
        }
    }

    /**
     * @param  $month
     * @param  $ciudad
     * @return array
     */
    public static function heatMapData($month, $ciudad)
    {
        $months = ['enero' => '01', 'febrero' => '02', 'marzo' => '03', 'abril' => '04', 'mayo' => '05', 'junio' => '06', 'julio' => '07', 'agosto' => '08', 'septiembre' => '09', 'octubre' => '10', 'noviembre' => '11', 'diciembre' => '12'];
        $stores = ['bogota' => 63, 'medellin' => 64];
        $date_ini = date('Y-'.$months[$month].'-01');
        $date_end = date('Y-'.$months[$month].'-t');
        $cordinates = self::select('order_groups.user_address_latitude as lat', 'order_groups.user_address_longitude as lng')
            ->join('order_groups', 'orders.group_id', '=', 'order_groups.id')
            ->where('orders.store_id', $stores[$ciudad])
            ->whereRaw("DATE(orders.created_at) BETWEEN '".$date_ini."' AND '".$date_end."'")
            ->get();

        $line = [];
        foreach ($cordinates as $coor) {
            $line[] = "new google.maps.LatLng(".$coor->lat.",".$coor->lng.")";
        }
        $store = Store::find($stores[$ciudad]);

        return ['origin' => ['lat' => $store->latitude, 'lng' => $store->longitude], 'orders' => implode(', ', $line)];
    }

    /**
     * @param  $route_id
     * @return mixed
     */
    public static function getOrdersByRoute($route_id)
    {
        return self::select(
            'delivery_windows.delivery_window as delivery_time',
            DB::raw('COUNT(orders.id) AS orders_quantity')
        )
            ->join('delivery_windows', 'delivery_windows.id', '=', 'orders.delivery_window_id')
            ->where('orders.route_id', $route_id)
            ->where('delivery_windows.status', 1)
            ->groupBy('orders.delivery_window_id')
            ->orderBy('orders.delivery_date')
            ->get();
    }

    /**
     * Asigna los valores de estado y validación dependiendo del tipo de acción requerida.
     *
     * @todo Eliminar este método, modificar el método {@see \Order::validateOrderFraud}.
     * @param Order   $order
     * @param $data
     * @param $statusOrder
     */
    public function validateOrderBlackListed(array $data, $statusOrder = OrderStatus::INITIATED)
    {
        $result = $this->validateOrderFraud($data);
        $this->status = $statusOrder;
        if ($this->type === 'Marketplace') {
            $this->status = 'Validation';
            $this->order_validation_reason_id = OrderValidationReason::MARKETPLACE;
        }

        if ($result['validation']) {
            $this->status = 'Validation';
            $this->posible_fraud = empty($result['posible_fraud']) ? 0 : $result['posible_fraud'];
            $this->order_validation_reason_id = $this->posible_fraud
                ? OrderValidationReason::FRAUD
                : OrderValidationReason::NEW_CREDIT_CARD;
        }
    }

    /**
     * Retorna los métodos de pagos de orders.
     * @return mixed
     */
    public function getPaymentMethods()
    {
        return get_enum_column_values('orders', 'payment_method');
    }

    /**
     * Accesor suspect. Deternima si un pedido es "sospechoso".
     *
     * @return int
     */
    public function getSuspectAttribute()
    {
        $suspectCreditCardOrderAmount = \Config::get('app.suspect_credit_card_order_amount');
        $statusesToSkipe = ['Cancelled', 'Delivered'];

        return !in_array($this->status, $statusesToSkipe) &&
        $this->payment_method == 'Tarjeta de crédito' &&
        $this->total_amount_og > $suspectCreditCardOrderAmount;
    }

    /**
     * Accessor, fue el último pago mediante tarjeta (credito o débito) exitoso?
     *
     * @return bool
     */
    public function lastPaymentByCardWasSucceed()
    {
        $lastPayment = $this->getPayments($last = true);

        return $lastPayment && ($this->wasSuccessfulPayedByCreditCard($lastPayment) || $this->wasSuccessfulPayedByPse($lastPayment));
    }

    /**
     * Fue la orden pagada exitosamente mediante tarjeta de crédito?
     *
     * @return bool
     */
    private function wasSuccessfulPayedByCreditCard($lastPayment)
    {
        return $this->payment_method !== 'Tarjeta de crédito' && $lastPayment && $lastPayment->cc_payment_status == 'Aprobada' && empty($payment->cc_refund_date);
    }

    /**
     * Fue la orden pagada exitosamente mediante tarjeta débito (PSE)?
     *
     * @return bool
     */
    private function wasSuccessfulPayedByPse($lastPayment)
    {
        return $this->payment_method !== 'Débito - PSE' && $lastPayment && $lastPayment->cc_payment_status == 'Aprobada';
    }

    /**
     * El pedido tiene log de productos no disponibles reembolsados?
     *
     * @return bool
     */
    public function hasMissingProductsRefundLog()
    {
        return $this->logs()->whereType('Productos no disponibles reembolsados.')->exists();
    }

    /**
     * Obtiene los products de la orden segun el nombre y excluye los id de order_products que no se deseen buscar
     * obtiene o excluye los productos en la nota credito
     * @param  string  $search
     * @param  array   $excludeIds
     * @return mixed
     */
    public function getOrderProducts($search = '', $excludeIds = [], $excludeCreditNote = true)
    {
        $order_products = self::orderProducts()->select(
                                                   'id',
                                                   'order_id',
                                                   'product_name',
                                                   'product_image_url',
                                                   'type',
                                                   'product_quantity',
                                                   'product_unit',
                                                   'base_price',
                                                   'price',
                                                   'iva',
                                                   'quantity_original',
                                                   'quantity',
                                                   'quantity_credit_note',
                                                   'reason_credit_note'
                                               )
                                               ->where(function ($q) use ($search) {
                                                   return $q->where('product_name', 'LIKE', '%'.$search.'%')
                                                            ->orWhere('reference', 'LIKE', '%'.$search.'%');
                                               });
        if ($excludeIds) {
            $order_products = $order_products->whereNotIn('id', $excludeIds);
        }
        if (!$excludeCreditNote) {
            $order_products = $order_products->whereNotNull('reason_credit_note')->whereNotNull('quantity_credit_note');
        }
        $order_products = $order_products->where('type', 'Product');
        $order_products = $order_products->get();

        return $order_products;
    }

    /**
     * @param $search
     * @param array $excludeIds
     * @param $excludeCreditNote
     * @return mixed
     */
    public function getOrderProductGroup($search = '', $excludeIds = [], $excludeCreditNote = true)
    {
        $order_product_group = self::orderProductGroup()->select(
                                                            'id',
                                                            'order_id',
                                                            'product_name',
                                                            'product_image_url',
                                                            DB::raw("'Agrupado' AS type"),
                                                            'product_quantity',
                                                            'product_unit',
                                                            'base_price',
                                                            'price',
                                                            'iva',
                                                            'quantity_original',
                                                            'quantity',
                                                            'quantity_credit_note',
                                                            'reason_credit_note'
                                                        )
                                                        ->where(function ($q) use ($search) {
                                                            return $q->where('product_name', 'LIKE', '%'.$search.'%')
                                                                     ->orWhere('reference', 'LIKE', '%'.$search.'%');
                                                        });
        if ($excludeIds) {
            $order_product_group = $order_product_group->whereNotIn('id', $excludeIds);
        }
        if (!$excludeCreditNote) {
            $order_product_group = $order_product_group->whereNotNull('reason_credit_note')
                                                       ->whereNotNull('quantity_credit_note');
        }
        $order_product_group = $order_product_group->get();

        return $order_product_group;
    }

    public function generateConsecutive()
    {
        $consecutive = DB::table('consecutives')
            ->where('type', 'invoice')
            ->where('prefix', 'F3')
            ->lockForUpdate()
            ->select(DB::raw("CONCAT(prefix,'-',prefix_complement) AS prefix"), 'consecutive', 'id', 'resolution')
            ->first();

        $numeration = $consecutive->consecutive;
        if ($numeration < 100) {
            $numeration = str_pad($consecutive->consecutive, 3, 0, STR_PAD_LEFT);
        }

        $this->invoice_number = $consecutive->prefix.$numeration;
        $this->invoice_date = $this->management_date;
        $this->invoice_issuance_date = date('Y-m-d H:i:s');
        $this->invoice_resolution = $consecutive->resolution;
        $this->save();

        $consecutive->consecutive += 1;
        $consecutive = DB::table('consecutives')
            ->where('id', $consecutive->id)
            ->update(['consecutive' => $consecutive->consecutive]);

        return true;
    }
    /**
     *
     * Permite actualizar los pedidos creados para darksupermarket
     */
    public function validateDarkSuperMarketOrder()
    {
        try {
            $userType = Auth::user()->type;
            $deliveryWindow = DeliveryWindow::find(DeliveryWindow::DELIVERY_WINDOW_EX_ID);

            if (in_array($userType, User::DARKSUPERMARKET_TYPE)) {
                DB::beginTransaction();
                $this->delivery_amount = 0;
                $this->delivery_window_id = $deliveryWindow->id;
                $this->delivery_time = sprintf(
                    '%s - %s',
                    date('h:i a', strtotime($deliveryWindow->hour_start)),
                    date('h:i a', strtotime($deliveryWindow->hour_end))
                );

                $orderGroup = $this->orderGroup;
                $orderGroup->source = $userType;
                $orderGroup->zone_id = Zone::EXPRESS_ZONE;
                $zone = Zone::find(Zone::EXPRESS_ZONE);
                $route = Routes::getCurrentDarksupermarketRoute(
                    $orderGroup->warehouse,
                    $zone,
                    DeliveryWindow::SHIFT_EXPRESS,
                    $userType
                );
                $this->route_id = $route->id;
                $this->status = 'Enrutado';
                foreach ($this->orderProducts as $orderProduct) {
                    $orderProduct->darkSuperMarketUpdate();
                }

                $this->updateTotals();
                $orderGroup = $this->setSmallerAmountsUser($orderGroup);
                $this->save();
                $orderGroup->updateTotals();
                $orderGroup->save();
                DB::commit();
                $mail = [
                    'template_name' => 'emails.order_dark_supermarket',
                    'subject' => 'Se creo un nuevo pedido para Dark Supermarket '.$userType,
                    'to' => [
                        [
                            'email' => Config::get('app.dark_supermarket.email_accounts.warehouse.email'),
                            'name' => Config::get('app.dark_supermarket.email_accounts.warehouse.name'),
                        ],
                        [
                            'email' => Auth::user()->email,
                            'name' => Auth::user()->first_name,
                        ],
                    ],
                    'vars' => [
                        'order' => $this->id,
                    ],
                ];

                //enviar mail
                send_mail($mail);
            }
        } catch (Exception $e) {
            ErrorLog::add($e, 500);
            DB::rollBack();
        }
    }

    public function setSmallerAmountsUser($orderGroup)
    {
        $smallerAmountUser = User::find(User::SMALLER_AMOUNTS_USER);
        $this->user_id = $smallerAmountUser->id;

        $orderGroup->user_id = $smallerAmountUser->id;
        $orderGroup->user_firstname = $smallerAmountUser->first_name;
        $orderGroup->user_email = $smallerAmountUser->email;
        $orderGroup->user_lastname = $smallerAmountUser->last_name;

        return $orderGroup;
    }

    /**
     * @param  $status
     * @throws Exception
     */
    public function validatePreviousStatus($status)
    {
        $previousStatus = [
            'Initiated' => 'Validation',
            'Enrutado' => 'Initiated',
            'In Progress' => 'Enrutado',
            'Alistado' => 'In Progress',
            'Dispatched' => 'Alistado',
            'Delivered' => 'Dispatched',
        ];

        if (isset($previousStatus[$status]) && $this->status != $previousStatus[$status]) {
            throw new Exception('El pedido no se encuentra en un estado valido.');
        }
    }

    /**
     * Funcion que valida si necesita un alistador frío o seco o en su defecto ambos
     *
     * @throws Exception
     */
    public function validatePicker()
    {
        $dryProducts = OrderProduct::with(['storeProduct' => function ($query) {
            $query->where('storage', 'Seco');
        }])
            ->where('order_id', $this->id)
            ->whereHas('storeProduct', function ($query) {
                $query->where('storage', 'Seco');
            })
            ->count();
        $dryProductsGroup = OrderProductGroup::with(['storeProduct' => function ($query) {
            $query->where('storage', 'Seco');
        }])
            ->where('order_id', $this->id)
            ->whereHas('storeProduct', function ($query) {
                $query->where('storage', 'Seco');
            })
            ->count();

        $coldProducts = OrderProduct::with(['storeProduct' => function ($query) {
            $query->where('storage', '<>', 'Seco');
        }])
            ->where('order_id', $this->id)
            ->whereHas('storeProduct', function ($query) {
                $query->where('storage', '<>', 'Seco');
            })
            ->count();

        $coldProductsGroup = OrderProductGroup::with(['storeProduct' => function ($query) {
            $query->where('storage', '<>', 'Seco');
        }])
            ->where('order_id', $this->id)
            ->whereHas('storeProduct', function ($query) {
                $query->where('storage', '<>', 'Seco');
            })
            ->count();

        $dry = $dryProducts + $dryProductsGroup;
        $cold = $coldProducts + $coldProductsGroup;
        if ($dry > 0 && is_null($this->picker_dry_id)) {
            throw new Exception('Se debe primero asignar el pedido a un alistador de seco.');
        }
        if ($cold > 0 && is_null($this->picker_cold_id)) {
            throw new Exception('Se debe primero asignar el pedido a un alistador de frío.');
        }

    }

    /**
     * @param  $campaignName
     * @return bool
     */
    public function hasCampaignDiscount($campaignName)
    {
        return $this->orderDiscount()
                    ->where('description', $campaignName)
                    ->count() > 0;
    }

    /**
     * Recibe orden express para pasarla a enrutado directamente
     *
     * @param Zone $zone
     */
    public function validateOrderExpress(Zone $zone)
    {
        try {
            DB::beginTransaction();
            $orderGroup = $this->orderGroup;

            $vehicleDriver = VehicleDrivers::with('driver.transporter')
                ->whereHas('driver.transporter', function ($transporter) {
                    $transporter->where('fullname', 'MU TEAM SAS');
                })->first();

            $route = Routes::getCurrentDarksupermarketRoute($orderGroup->warehouse, $zone, DeliveryWindow::SHIFT_EXPRESS);
            $this->route_id = $route->id;
            $this->driver_id = $vehicleDriver->driver_id;
            $this->vehicle_id = $vehicleDriver->vehicle_id;
            $this->status = 'Enrutado';
            $this->planning_date = \Carbon\Carbon::now()->format('Y-m-d');
            $this->save();

            DB::commit();
        } catch (Exception $e) {
            ErrorLog::add($e, 500);
            DB::rollBack();
        }
    }

    /**
     * Realiza la petición al servicio
     * qué crea una orden en Mensajeros Urbanos
     *
     */
    public function createServiceMensajerosUrbanos()
    {
        $payment_type = [
            'Efectivo' => 1,
            'Datáfono' => 2,
            'Tarjeta de crédito' => 3,
            'Débito - PSE' => 3,
        ];

        $products = $this->orderProducts;

        $arrayProducts = [];
        foreach ($products as $product) {
            $arrayProducts[] = [
                "barcode" => $product->reference,
                "planogram" => 'Piso 1',
                "product_name" => $product->product_name,
                "quantity" => $product->quantity,
                "sku" => $product->reference,
                "store_id" => $this->orderGroup->warehouse_id,
                "url_img" => $product->product_image_url,
                "value" => round($product->price, 0),
            ];
        }

        $mapJson = [
            'order' => [
                'id' => $this->id,
                'payment_type' => $payment_type[$this->payment_method],
                'value' => round(($this->total_amount + $this->delivery_amount - $this->discount_amount), 0),
                'city_id' => $this->store_id
            ],
            'client' => [
                'phone' => $this->user->phone,
                'document' => '900871444-8',
                'email' => $this->user->email,
                'name' => $this->user->first_name." ".$this->user->last_name,
                'address' => $this->orderGroup->user_address." ".$this->orderGroup->user_address_further,
            ],
            'products' => [
                'payload' => $arrayProducts,
            ],
        ];

        $order_data = [
            'headers' => [
                'Authorization' => Config::get('app.delivery_express.key'),
                'Content-Type' => 'application/json',
            ],
            'body' => json_encode($mapJson),
        ];

        $client = new GuzzleHttp\Client([
            'base_uri' => Config::get('app.delivery_express.base_uri'),
        ]);
        $path = Config::get('app.delivery_express.path');

        $response = $client->post($path, $order_data);

        return $response->getStatusCode() == 200 ? true : false;
    }

    /**
     * Method for get difference and delivery and management date in minutes
     *
     * @return int
     */
    public function diffBetweenDeliveryAndManagementDatesInMinutes()
    {
        $origin_format = Carbon::createFromFormat('Y-m-d H:i:s', $this->management_date); //2019-05-03 07:10:46.000000
        $compare_format = Carbon::createFromFormat('Y-m-d H:i:s', $this->delivery_date); //2019-05-02 07:00:00.000000

        return $origin_format->diffInMinutes($compare_format);
    }

    public function managementDateIsGreaterThanDeliveryDate()
    {
        $origin_format = Carbon::createFromFormat('Y-m-d H:i:s', $this->management_date);
        $compare_format = Carbon::createFromFormat('Y-m-d H:i:s', $this->delivery_date);

        if($origin_format->greaterThan($compare_format)){
            return true;
        }

        return false;
    }

    /**
     * Method for verify if order is delivered
     * @return bool
     */
    public function isDelivered()
    {
        return $this->status == 'Delivered';
    }

	/**
	 * @param  $route_id
	 * @return mixed
	 */
	public static function getQuantityOrdersByRoute($route_id)
	{
		return self::select(DB::raw('COUNT(orders.id) AS orders_quantity'))
			->join('delivery_windows', 'delivery_windows.id', '=', 'orders.delivery_window_id')
			->where('orders.route_id', $route_id)
			->where('delivery_windows.status', 1)
			->groupBy('orders.route_id')
			->first();
	}
}
