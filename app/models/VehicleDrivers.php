<?php

class VehicleDrivers extends \Illuminate\Database\Eloquent\Model
{

    protected $table = 'vehicle_drivers';

    public function vehicle()
    {
        return $this->belongsTo('Vehicle', 'vehicle_id');
    }

    public function driver()
    {
        return $this->belongsTo('Driver', 'driver_id');
    }


}