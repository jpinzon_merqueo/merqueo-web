<?php

class InventoryCountingDetailCount extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'inventory_counting_details_count';

    public function warehouseStorage() {
      return $this->belongsTo('WarehouseStorage');
    }

    public function inventoryCountingDetail(){
        return $this->belongsTo('InventoryCountingDetail');
    }

}
