<?php

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * @property User $user
 * @property string $card_token
 * @property mixed last_four
 * @property mixed expiration_month
 * @property mixed expiration_year
 * @property mixed bin
 * @property string type
 */
class UserCreditCard extends Model
{
    const CARD_CODENSA = 'CODENSA';

    /**
     * @var string
     */
    protected $table = 'user_credit_cards';

    /**
     * @var array
     */
    protected $fillable = [
        'bin',
        'last_four',
        'type',
        'holder_document_type',
        'holder_document_number',
        'holder_name',
        'holder_email',
        'holder_phone',
        'holder_address',
        'holder_address_further',
        'holder_city',
    ];

    /**
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('User');
    }

    /**
     * Valida si el bin de la tarjeta existe en la tabla
     *
     * @param $reference Referencia del descuento
     * @return boolean
     */
    public function isValidBin($reference)
    {
        $bin = CreditCardBin::select('id')->where('bin', $this->bin)
            ->where('reference', $reference)
            ->first();

        if ($bin) {
            return true;
        }

        return false;
    }

    /**
     * Valida si la tarjeta del usuario tiene compras Entregadas
     */
    public function haveOrders()
    {
        return ((Order::where('credit_card_id', $this->id)
                    ->where('status', 'Delivered')
                    ->count() > 0)
            ||
            (ExternalService::where('credit_card_id', $this->id)
                    ->where('status', 'Aprobada')
                    ->count() > 0));
    }

    /**
     * Registra la tarjeta de crédito del cliente.
     * @param $post_data
     * @param $order
     * @param $config
     * @return Collection|null|UserCreditCard
     * @throws Exception
     */
    public static function registerCreditCard($post_data, $order, $config)
    {
        //validar si tarjeta es nueva
        $credit_card_id = isset($post_data['credit_card_id']) ? $post_data['credit_card_id'] : '';
        if (empty($credit_card_id)) {
            $payu = new PayU;
            $order_group = OrderGroup::find($order->group_id);
            $address = UserAddress::find($order_group->address_id);
            $user = User::find($order->user_id);
            $city = City::find($address->city_id);

            //validar pais de la tarjeta de credito
            $bin = substr($post_data['number_cc'], 0, 6);
            $credit_card = $payu->getCreditCardInfo($bin, $post_data);
            if (!$credit_card) {
                throw new Exception('Ocurrió un problema al validar el origen de tu tarjeta de crédito.');
            }
            if (!$credit_card->is_valid) {
                throw new Exception('Solo se aceptan tarjetas de crédito nacionales.');
            }

            $post_data['card_type'] = get_card_type($post_data['number_cc']);
            //PayU
            if (date('Y-m-d H:i:s') >= Config::get('app.payu.start_date')) {
                $result = $payu->associateCreditCard($post_data);
                if (!$result['status']) {
                    throw new Exception($result['message']);
                }

                if (!in_array($result['response']->creditCardToken->paymentMethod, $config['credit_cards_types'])) {
                    throw new Exception('Solo se aceptan tarjetas de crédito ' . $config['credit_cards_message']);
                }

                $post_data['card_token'] = $result['response']->creditCardToken->creditCardTokenId;
            } else {
                $payment = new Payment;

                //si el usuario no esta creado en la api se crea
                if (Auth::check()) {
                    $create_user_api = false;
                    if (empty(Auth::user()->customer_token)) {
                        $create_user_api = true;
                    } else {
                        $post_data['customer_token'] = Auth::user()->customer_token;
                    }
                } else {
                    $create_user_api = true;
                }

                if ($create_user_api) {
                    if ($credit_card) {
                        $result = $payment->createCustomer($post_data);
                    }
                    if (!$result['status']) {
                        throw new Exception('Ocurrió un error al procesar tus datos personales de la tarjeta de crédito.');
                    }
                    $user->customer_token = $post_data['customer_token'] = $result['response']->id;
                    $user->save();
                }

                $result = $payment->associateCreditCard($post_data);
                if (!$result['status']) {
                    throw new Exception('Ocurrió un error al validar los datos de tu tarjeta de crédito.');
                }

                if (!in_array($result['response']->type, $config['credit_cards_types'])) {
                    throw new Exception('Solo se aceptan tarjetas de crédito ' . $config['credit_cards_message']);
                }

                $post_data['card_token'] = $result['response']->id;
            }
            //guardar tarjeta en bd
            $user_credit_card = new self;
            $user_credit_card->user_id = $user->id;
            $user_credit_card->card_token = $post_data['card_token'];
            $user_credit_card->bin = substr($post_data['number_cc'], 0, 6);
            $user_credit_card->last_four = substr($post_data['number_cc'], 12, 4);
            $user_credit_card->type = $post_data['card_type'];
            $user_credit_card->country = $credit_card->country_name;
            $user_credit_card->holder_document_type = $post_data['document_type_cc'];
            $user_credit_card->holder_document_number = $post_data['document_number_cc'];
            $user_credit_card->holder_name = $post_data['name_cc'];
            $user_credit_card->holder_email = $user->email;
            $user_credit_card->holder_phone = $post_data['phone_cc'];
            $user_credit_card->holder_address = $post_data['address_cc'];
            $user_credit_card->holder_address_further = '';
            $user_credit_card->holder_city = $city->city;
            $user_credit_card->save();
        } else {
            $user_credit_card = self::find($credit_card_id);
            if (!$user_credit_card) {
                throw new Exception('Ocurrió un error al obtener información de la tarjeta de crédito.');
            }
            $post_data['card_token'] = $user_credit_card->card_token;
            if (!empty($post_data['add_document_number_cc_' . $user_credit_card->id])) {
                $user_credit_card->holder_document_type = $post_data['add_document_type_cc_' . $user_credit_card->id];
                $user_credit_card->holder_document_number = $post_data['add_document_number_cc_' . $user_credit_card->id];
                $user_credit_card->save();
            }
        }

        return $user_credit_card;
    }

    /**
     * @param Builder $query
     * @param Order|null $order
     */
    public function scopeWithoutCodensaCards(Builder $query, Order $order = null)
    {
        $query->where('type', '!=', static::CARD_CODENSA);

        if (!empty($order)) {
            $country = $order->store()
                    ->select(['countries.id'])
                    ->join('cities' ,'cities.id', '=', 'stores.city_id')
                    ->join('countries', 'countries.id', '=', 'cities.country_id')->first();

            if (!empty($country)) {
                $query->where('country_id', $country->id);
            }
        }
    }
}
