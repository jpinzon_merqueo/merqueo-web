<?php

namespace users;

use Carbon\Carbon;
use Discount;
use Order;
use orders\OrderStatus;
use \UpdateUserFieldsLeanplum;
use \DB;
use UserAddress;
use Zone;

/**
 * Listado de atributos calculados de la clase User
 * @package users
 */
trait UserAttributes
{
    /**
     * Obtiene la orden más costosa del usuario.
     *
     * @return mixed
     */
    public function getMaxTicketAttribute()
    {
        return $this->orders()
            ->where('status', OrderStatus::DELIVERED)
            ->max('total_amount');
    }

    /**
     * Obtiene el promedio de las ordenes calificadas.
     *
     * @return mixed
     */
    public function getAverageOrderAttribute()
    {
        $order_query = $this->orders()->where('status', OrderStatus::DELIVERED);
        $total_orders = $order_query->count();
        $rated_orders = $order_query->whereNotNull('user_score_date')->count();

        return $total_orders ? $rated_orders / $total_orders : 0;
    }

    /**
     * Determina si el usuario ha realizado compras con tarjeta de credito.
     *
     * @return bool
     */
    public function getHasCreditCardPaymentsAttribute()
    {
        return $this->orders()
                ->where('payment_method', 'Tarjeta de crédito')
                ->where('status', OrderStatus::DELIVERED)
                ->count() > 0;
    }

    /**
     * Determina si el usuario ha realizado compras en efectivo
     *
     * @return bool
     */
    public function getHasCashPaymentsAttribute()
    {
        return $this->orders()
                ->where('payment_method', 'Efectivo')
                ->where('status', OrderStatus::DELIVERED)
                ->count() > 0;
    }

    /**
     * Determina si el usuario ha realizado compras con datafono.
     *
     * @return bool
     */
    public function getHasSwipeCardTerminalPaymentsAttribute()
    {
        return $this->orders()
                ->where('payment_method', 'Datáfono')
                ->where('status', OrderStatus::DELIVERED)
                ->count() > 0;
    }

    /**
     * Determina la catuidad de usuarios referidos por el usuario que han comprado.
     *
     * @return mixed
     */
    public function getReferralsNumberAttribute()
    {
        return $this->referrals()
            ->whereHas('orders', function ($query) {
                $query->where('status', OrderStatus::DELIVERED);
            })
            ->count();
    }

    /**
     * Obtiene la fecha de vencimiento del proximo crédito del usuario.
     *
     * @return mixed
     */
    public function getNextCreditExpirationDateAttribute()
    {
        $credit_available = $this->getCreditAvailable();

        if ($credit_available <= 0) {
            return null;
        }

        return $this->userCredits()
            ->where('type', 1)
            ->where('status', 1)
            ->where('current_amount', '>', 0)
            ->where('expiration_date', '>', Carbon::create()->format('Y-m-d'))
            ->min('expiration_date');
    }

    /**
     * Obtiene el monto del próximo credito disponible para el usuario.
     *
     * @return mixed
     */
    public function getNextCreditExpirationAmountAttribute()
    {
        $credit_available = $this->getCreditAvailable();

        if ($credit_available <= 0) {
            return null;
        }

        return $this->userCredits()
            ->where('type', 1)
            ->where('status', 1)
            ->where('current_amount', '>', 0)
            ->where('expiration_date', '>', Carbon::create()->format('Y-m-d'))
            ->orderBy('expiration_date')
            ->pluck('amount');
    }

    /**
     * Devuelve el nombre completo del usuario
     *
     * @return string Nombre completo del usuario
     */
    public function getNameAttribute()
    {
        return "$this->first_name $this->last_name";
    }

    /**
     * Devuelve la ciudad del usuario
     *
     * @return string Nombre de la ciudad
     */
    public function getCityAttribute()
    {
        $city = UserAddress::select('user_address.*', 'cities.city as city')
            ->join('cities', 'cities.id', '=', 'city_id')
            ->where('user_address.user_id', $this->id)
            ->orderby('id', 'DESC')
            ->first();

        if ($city) {
            return $city->city;
        } else {
            return null;
        }
    }

    /**
     * Devuelve el número de órdenes del usuario
     *
     * @return integer Número de órdenes
     */
    public function getOrderCountAttribute()
    {
        return count(
            $this->orders()
                ->select('id')
                ->groupBy('group_id')
                ->lists('id')
        );
    }

    /**
     * Devuelve el número de órdenes entregadas del usuario
     *
     * @return integer Número de órdenes
     */
    public function getOrderDeliveredCountAttribute()
    {
        return count(
            $this->orders()
                ->select('id')
                ->groupBy('group_id')
                ->whereHas('orderGroup', function ($query) {
                    $query->where('source', '<>', 'Reclamo');
                })
                ->where('status', OrderStatus::DELIVERED)
                ->lists('id')
        );
    }

    /**
     * Devuelve el total de las órdenes del usuario
     *
     * @return double Total
     */
    public function getTotalAmountAttribute()
    {
        if (isset($this->ordersTotal)) {
            return $this->ordersTotal;
        }

        $orderTotal = Order::select(DB::raw('SUM(total_amount) as total'))
            ->where('status', 'Delivered')
            ->where('user_id', $this->id)
            ->from('orders')
            ->first();

        return $orderTotal->total;
    }

    /**
     * Devuelve el descuento total de las órdenes del usuario
     *
     * @return double Total descuentos
     */
    public function getTotalDiscountAttribute()
    {
        if (isset($this->discountTotal)) {
            return $this->discountTotal;
        }

        $discountTotal = Order::select(DB::raw('SUM(discount_amount) as total'))
            ->where('status', 'Delivered')
            ->where('user_id', $this->id)
            ->from('orders')
            ->first();

        return $discountTotal->total;
    }

    /**
     * Devuelve el total de precio especial que el usuario ha aprovechado
     *
     * @return double Total precios especiales
     */
    public function getTotalSpecialPriceAttribute()
    {
        $specialPriceTotal = Order::select(DB::raw('SUM((original_price - price) * quantity) as total'))
            ->where('status', 'Delivered')
            ->where('user_id', $this->id)
            ->from('orders')
            ->join('order_products', 'order_products.order_id', '=', 'orders.id')
            ->first();

        return $specialPriceTotal->total;
    }

    /**
     * Devuelve los atributos del usuario para enviar a analytics
     *
     * @return array Atributos del usuario
     */
    public function getTraitsAttribute()
    {
        return [
            'name' => $this->name,
            'email' => $this->email,
            'firstName' => $this->first_name,
            'lastName' => $this->last_name,
            'phone' => $this->phone,
            'city' => $this->city,
            'createdAt' => $this->created_at->timestamp,
        ];
    }

    /**
     * Obtiene el número de días restantes para que
     * el usuario cuente con la promoción del 5%.
     *
     * @return int|null
     */
    public function getLastOrderDaysAttribute()
    {
        $lastOrder = $this->orders()
            ->select('management_date')
            ->latest()
            ->where('status', OrderStatus::DELIVERED)
            ->first();

        if (empty($lastOrder)) {
            return null;
        }

        $managedDate = new Carbon($lastOrder->management_date);
        $discountBeginDate = new Carbon(Discount::PERCENT_DISCOUNT_BEGIN_DATE);
        $managedDate = $managedDate->startOfDay();
        $startOfTheDay = Carbon::now()->startOfDay();

        if ($managedDate->gte($discountBeginDate)) {
            return max(Discount::REORDER_DAYS_DISCOUNT - $startOfTheDay->diffInDays($managedDate), 0);
        }

        return max(Discount::REORDER_DAYS_DISCOUNT - $startOfTheDay->diffInDays($discountBeginDate), 0);
    }

    /**
     * Devuelve los atributos completos del usuario para enviar a analytics
     *
     * @return array Atributos completos del usuario
     */
    public function getFullTraitsAttribute()
    {
        return [
            UpdateUserFieldsLeanplum::MAX_TICKET => $this->maxTicket,
            UpdateUserFieldsLeanplum::AVG_ORDER => $this->averageOrder,
            UpdateUserFieldsLeanplum::CREDIT_CARD => $this->hasCreditCardPayments,
            UpdateUserFieldsLeanplum::CASH => $this->hasCashPayments,
            UpdateUserFieldsLeanplum::SWIPE_CARD_TERMINAL => $this->hasSwipeCardTerminalPayments,
            UpdateUserFieldsLeanplum::REFERRAL_NUMBER => $this->referralsNumber,
            UpdateUserFieldsLeanplum::REFERRAL_CODE => $this->referral_code,
            UpdateUserFieldsLeanplum::NEXT_CREDIT_EXPIRATION_DATE => $this->nextCreditExpirationDate,
            UpdateUserFieldsLeanplum::NEXT_CREDIT_EXPIRATION_AMOUNT => $this->nextCreditExpirationAmount,
            UpdateUserFieldsLeanplum::CITY => $this->city,
        ];
    }

    /**
     * @return float
     */
    public function averageDaysBetweenOrders()
    {
        $result = DB::select(
            'SELECT AVG(DATEDIFF(created_at, prev_created_at)) AS difference ' .
            'FROM (SELECT orders.*, ' .
            '(SELECT temporal_orders.created_at ' .
            'FROM orders temporal_orders ' .
            'WHERE temporal_orders.user_id = orders.user_id ' .
            'AND temporal_orders.created_at < orders.created_at ' .
            'AND orders.status <> "Cancelled" ' .
            'ORDER BY temporal_orders.created_at DESC ' .
            'LIMIT 1 ' .
            ') AS prev_created_at ' .
            'FROM orders ' .
            "WHERE user_id = '{$this->id}' " .
            'AND orders.status <> "Cancelled" ' .
            ') temporal_table'
        );

        return $result[0]->difference;
    }

    /**
     * @return Zone|null
     */
    public function lastOrderZone()
    {
        $lastOrder = $this->orderGroups()
            ->latest()
            ->with('zone');

        if (empty($lastOrder)) {
            return null;
        }

        return $lastOrder->zone;
    }
}
