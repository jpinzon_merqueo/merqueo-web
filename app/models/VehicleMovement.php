<?php

class VehicleMovement extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'vehicle_movements';

    /**
     * Guardar movimiento de un vehiculo transportador
     *
     * @param object $vehicle_movement Objeto movimiento
     * @param object $order Objeto pedido
     * @return object $movement Objeto movimiento
     */
     public static function saveMovement($movement, $order)
     {
        if ($movement->payment_method == 'Efectivo y datáfono' || $movement->payment_method == 'Efectivo'){
            $order_total_amount = $order->total_amount + $order->delivery_amount - $order->discount_amount;
            $movement->driver_balance = 0;

            //cliente efectivo
            if ($movement->payment_method == 'Efectivo')
                $movement->driver_balance = $order_total_amount * -1;

            //cliente efectivo y datafono
            if ($movement->payment_method == 'Efectivo y datáfono')
                $movement->driver_balance = ($order_total_amount - $movement->user_card_paid) * -1;

            unset($movement->payment_method);
            $movement->save();
            return $movement;

        }else{
            if($movement->payment_method == 'Datáfono' ||  $movement->payment_method == 'Tarjeta de crédito'){
                $movement->driver_balance = 0;
                unset($movement->payment_method);
                $movement->save();
                return $movement;
            }else
                return false;
        }
     }
}