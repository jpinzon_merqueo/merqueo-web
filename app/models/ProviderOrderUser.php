<?php

class ProviderOrderUser extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'provider_order_users';
    protected $hidden = array('created_at','updated_at');

}