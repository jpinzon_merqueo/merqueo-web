<?php


class PickingSupplierLog extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'picking_supplier_log';

    public static function log($picking_supplier_task, $stowage, $action, $quantity_to_suppling=null){
    	$picking_supplier_log = PickingSupplierLog::findOrNew(0);

    	$picking_supplier_log->admin_id = Session::get('admin_id');
    	$picking_supplier_log->store_product_id = $picking_supplier_task->store_product_id;
    	$picking_supplier_log->warehouse_id = $picking_supplier_task->warehouse_id;
    	$picking_supplier_log->picking_stock = $picking_supplier_task->quantity_picking;
    	$picking_supplier_log->quantity_to_suppling = $quantity_to_suppling;
    	$picking_supplier_log->quantity_supplied = $picking_supplier_task->quantity_supplied;
    	$picking_supplier_log->automatic = $picking_supplier_task->automatic;
    	$picking_supplier_log->picking_position = $picking_supplier_task->picking_position;
    	$picking_supplier_log->picking_position_height = $picking_supplier_task->picking_position_height;
    	if(!is_null($stowage)){
    		$stowage = WarehouseStorage::find($stowage);
    		
	    	$picking_supplier_log->store_position = $stowage->position;
	    	$picking_supplier_log->store_position_height = $stowage->position_height;
    	}
    	
    	$picking_supplier_log->status = $picking_supplier_task->status;
    	$picking_supplier_log->action = $action;
    	$picking_supplier_log->save();
    }

}
