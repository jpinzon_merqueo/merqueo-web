<?php

class Department extends \Illuminate\Database\Eloquent\Model
{
    /**
     * @var string
     */
    const NORMAL = 'NORMAL';

    /**
     * @var string
     */
    protected $table = 'departments';

    /**
     * @var array
     */
    protected $hidden = ['created_at','updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo}
     */
    public function store()
    {
        return $this->belongsTo('Store', 'store_id');
    }

    /**
     * Obtiene pasillos del departamento
     *
     * @param int $warehouse_id ID de la bodega
     * @return Product
     */
    public function getShelves($warehouse_id)
    {
        return Shelf::select('shelves.id', 'shelves.store_id', 'shelves.department_id', 'shelves.name', DB::raw('COUNT(store_products.id) AS products_qty'))
            ->join('store_products', 'store_products.shelf_id', '=', 'shelves.id')
            ->join('products', 'store_products.product_id', '=', 'products.id')
            ->join('store_product_warehouses', 'store_products.id', '=', 'store_product_warehouses.store_product_id')
            ->where('store_product_warehouses.warehouse_id', $warehouse_id)
            ->where('shelves.department_id', $this->id)
            ->where('store_product_warehouses.status', 1)
            ->where('shelves.status', 1)
            ->whereRaw('IF(store_product_warehouses.manage_stock = 1, IF(store_product_warehouses.is_visible_stock = 0, IF( store_product_warehouses.current_stock > 0, TRUE, FALSE), TRUE), TRUE)')
            ->where('products.type', '<>', 'Proveedor')
            ->groupBy('shelves.id')->orderBy('shelves.sort_order')
            ->orderBy('shelves.name')
            ->limit(60)
            ->get();
    }

    /**
     * @return mixed
     */
    public function shelves()
    {
        return $this->hasMany(Shelf::class);
    }

    /**
     * Modificacion al guardar el evento
     */
    public static function boot()
    {
        parent::boot();

        static::updated(function ($department) {

            if ($department->isDirty())
            {
                $dirty_data = $department->getDirty();
                if (Config::get('app.aws.elasticsearch.is_enable') && isset($dirty_data['slug']) || isset($dirty_data['status']))
                {
                    $aws_elasticsearch = new AWSElasticsearch();
                    $aws_elasticsearch->update_products(array('department_id' => $department->id));
                }
            }
        });
    }
}
