<?php

/**
 * Class Discount
 */
class Discount extends \Illuminate\Database\Eloquent\Model
{
    /**
     * @var string
     */
	protected $table = 'discounts';

    /**
     * @var int Cantidad de pedidos entregados para descuento
     */
	const REORDER_ORDERS_DISCOUNT = 2;

    /**
     * @var int Número de dias para aplicar descuento
     */
	const REORDER_DAYS_DISCOUNT = 35;

    /**
     * @var string Fecha de inicio de la promoción general de la tienda del 5%
     */
	const PERCENT_DISCOUNT_BEGIN_DATE = '2018-10-09';

    /**
     * Determina si es necesario aplicar la validación del 5% de descuento.
     *
     * @return bool
     */
	public static function shouldApplyReorder()
    {
        $limit_date = new Carbon\Carbon(self::PERCENT_DISCOUNT_BEGIN_DATE);
        $limit_date->addDay(self::REORDER_DAYS_DISCOUNT);

        return \Carbon\Carbon::create()
            ->gte($limit_date);
    }

    /**
     * Departamentos que no pueden tener el descuento del 5%
     *
     * @return array
     */
    public static function skippedShelves()
    {
        return array_merge(Shelf::MILK, Shelf::CIGARETTES);
    }

    /**
     * Determina si es necesario omitir un pasillo por su id.
     *
     * @param $shelf_id
     * @return bool
     */
    public static function shouldSkippedShelve($shelf_id)
    {
        return in_array($shelf_id, Shelf::MILK) || in_array($shelf_id, Shelf::CIGARETTES);
    }
}
