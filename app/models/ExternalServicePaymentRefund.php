<?php

use \Illuminate\Database\Eloquent\Model;

/**
 * Class ExternalServicePaymentRefund
 * @property ExternalServicePayment $externalServicePayment
 * @property string $status
 */
class ExternalServicePaymentRefund extends Model
{
    protected $table = 'external_service_payment_refunds';

    protected $hidden = ['created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function admin()
    {
        return $this->belongsTo('Admin');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function externalServicePayment()
    {
        return $this->belongsTo('ExternalServicePayment');
    }

    /**
     * @param PayU $payment_manager
     * @return ExternalServicePaymentRefund
     */
    public function reloadState(PayU $payment_manager)
    {
        $transaction_id = $this->externalServicePayment->cc_charge_id;
        $response = $payment_manager->getRefundResponse($transaction_id, new \StdClass());

        if ($response['status'] && $response['response']->code = 'SUCCESS') {
            $payload = $response['response']->result->payload;
            $transaction = $payload->transactions->transaction;
            if (is_array($transaction)) {
                $status = $payload->status;
                $type = $transaction[0]->type;
                $state = $transaction[0]->transactionResponse->state;
            } else {
                $status = $payload->status;
                $type = $transaction->type;
                $state = $transaction->transactionResponse->state;
            }

            if ($status === 'CAPTURED' && $state === 'APPROVED' && $type === 'AUTHORIZATION_AND_CAPTURE') {
                $this->status = 'Pendiente';
            }
            if ($status === 'CAPTURED' && $state === 'DECLINED' && ($type === 'REFUND' || $type === 'VOID')) {
                $this->status = 'Declinada';
            }
            if (($status == 'CANCELLED' || $status == 'REFUNDED') && $state == 'APPROVED' && ($type == 'REFUND' || $type == 'VOID')) {
                $this->status = 'Aprobada';
            }
        }

        $was_changed = $this->isDirty('status');
        if ($was_changed) {
            $this->save();
        }

        return $this;
    }
}