<?php

namespace orders;

use Carbon\Carbon;
use Illuminate\Support\Collection;

/**
 * Class OrderBuilder
 * @package orders
 */
class OrderBuilder
{
    /**
     * @var \Order
     */
    protected $order;

    /**
     * OrderBuilder constructor.
     * @param \Order|null $order
     */
    public function __construct(\Order $order = null)
    {
        $this->order = $order;
    }

    /**
     * @param \AlliedStore|null $allied_store
     * @param int $total_products
     * @return null|\Order
     */
    public function findOrCreateOrder(\AlliedStore $allied_store = null, $total_products = 0)
    {
        if ($this->isMerqueoOrder($this->order, $allied_store) ||
            $this->isMarketplaceOrder($this->order, $allied_store)) {
            return $this->order;
        }

        foreach ($this->order->orderGroup->orders as $order_from_group) {
            if (in_array($order_from_group->status, [OrderStatus::DELIVERED, OrderStatus::CANCELED])) {
                continue;
            }

            if ($this->isMerqueoOrder($order_from_group, $allied_store) ||
                $this->isMarketplaceOrder($order_from_group, $allied_store)) {
                return $order_from_group;
            }
        }

        if (!$total_products) {
            return null;
        }

        return $this->getCopyOrder($allied_store);
    }

    /**
     * @param \AlliedStore|null $allied_store
     * @return \Order
     */
    private function getCopyOrder(\AlliedStore $allied_store = null)
    {
        $new_order = $this->order->replicate();
        $new_order->id = null;
        $new_order->exists = false;
        $new_order->type = empty($allied_store) ? 'Merqueo' : 'Marketplace';
        $new_order->total_amount = 0;
        $new_order->discount_amount = 0;
        $new_order->total_products = 0;
        $new_order->delivery_amount = 0;
        $new_order->date = Carbon::create();
        $new_order->user_score_token = generate_token();
        $new_order->setRelation('orderProducts', new Collection());
        if (!empty($allied_store)) {
            $new_order->alliedStore()->associate($allied_store);
            $new_order->delivery_date = get_delivery_date_marketplace(
                new Carbon($this->order->delivery_date),
                $allied_store->delivery_time_hours
            );
        } else {
            $new_order->allied_store_id = null;
        }

        $new_order->save();

        return $new_order;
    }

    /**
     * @param \Order $order
     * @param \AlliedStore $allied_store
     * @return bool
     */
    private function isMerqueoOrder(\Order $order, \AlliedStore $allied_store = null)
    {
        return $order->type === 'Merqueo' && empty($allied_store);
    }

    /**
     * @param \Order $order
     * @param \AlliedStore $allied_store
     * @return bool
     */
    private function isMarketplaceOrder(\Order $order, \AlliedStore $allied_store = null)
    {
        return !empty($allied_store->id) && $order->allied_store_id === $allied_store->id;
    }
}
