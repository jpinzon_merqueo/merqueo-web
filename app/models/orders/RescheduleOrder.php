<?php

namespace orders;

use Carbon\Carbon;
use DeliveryWindow;
use exceptions\MerqueoException;
use Order;
use usecases\Interfaces\orders\CalculateDeliveryWindowAmountInterface;

/**
 * Class RescheduleOrder
 * @package orders
 */
class RescheduleOrder
{
    const LIMIT_RESCHEDULE = 2;

    /**
     * @var string
     */
    protected $deliveryDay;

    /**
     * @var string
     */
    protected $deliveryTime;

    /**
     * @var Order
     */
    protected $order;

    /**
     * @var string
     */
    protected $reason;

    /**
     * RescheduleOrder constructor.
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * @param $deliveryDate
     * @param $deliveryTime
     * @param $reason
     * @throws MerqueoException
     */
    public function make($deliveryDate, $deliveryTime, $reason)
    {
        $this->deliveryDay = $deliveryDate;
        $this->deliveryTime = $deliveryTime;
        $this->reason = $reason;

        $this->validateDeliveryToAddress();
        $slot = $this->validateTimeSelected();
        $this->order->delivery_amount = $slot['delivery_amount'];
        $visa_campaign = $this->order->orderDiscount()
            ->where('description', 'visa_domicilio_4500')
            ->first();

        if ($this->order->hasVisaPaymentMethod() && $visa_campaign) {
            $this->order->delivery_amount -= $visa_campaign->amount;
            if ($this->order->delivery_amount < 0) {
                $this->order->delivery_amount = 0;
            }
        } elseif (!$visa_campaign && empty($this->order->getOriginal('delivery_amount'))) {
            $this->order->delivery_amount = 0;
        }

        $this->changeDeliveryDate();
    }

    /**
     * @throws MerqueoException
     */
    private function validateTimeSelected()
    {
        $slots = $this->getDeliveryDates();
        foreach ($slots['time'] as $day => $interval) {
            if ($day === $this->deliveryDay) {
                foreach ($interval as $time => $detail) {
                    if ($time == $this->deliveryTime) {
                        return $slots['time'][$day][$time];
                    }
                }
            }
        }

        throw new MerqueoException(
            'Los horarios de entrega han cambiado, por favor vuelve a ' .
            'cargar esta pagina para refrescar la información.'
        );
    }

    /**
     * @return array
     */
    public function getDeliveryDates()
    {
        $user = $this->order->user;
        $user_discounts = \User::getDiscounts($user->id);
        $allied_store = $this->order->alliedStore;
        $slots = $this->order->store->getClearDeliverySlots(
            $this->order->orderGroup->zone,
            $user->hasFreeDelivery($this->order->store, $user_discounts, $this->order->total_amount),
            $this->order->orderGroup->user_address_latitude,
            $this->order->orderGroup->user_address_longitude
        );

        $slots = $slots[$this->order->store_id];

        if (!empty($allied_store)) {
            $days = [];
            $next_available_date = add_available_days(
                Carbon::create(),
                $allied_store->delivery_time_hours / Carbon::HOURS_PER_DAY
            )->format('Y-m-d');

            foreach ($slots['days'] as $day => $slot) {
                if ($day >= $next_available_date) {
                    $days[$day] = $slot;
                }
            }
            $slots['days'] = $days;
        }

        return $this->removeDeliveryWindowER($slots);
    }

    /**
     *
     */
    public function changeDeliveryDate()
    {
        $deliveryWindowUseCase = app()->make(CalculateDeliveryWindowAmountInterface::class);
        $deliveryWindowAmount = $deliveryWindowUseCase->handle($this->order);
        $deliveryWindow = $this->order->store->validateStoreDeliversToAddress(
            $this->order->orderGroup->user_address_latitude,
            $this->order->orderGroup->user_address_longitude,
            new Carbon($this->deliveryDay),
            $this->deliveryTime
        );

        $this->order->real_delivery_date = $this->order->delivery_date = "$this->deliveryDay $deliveryWindow->hour_end";
        $this->order->delivery_time = $deliveryWindow->delivery_window;
        $this->order->deliveryWindow()->associate($deliveryWindow);
        $this->order->scheduled_delivery++;
        $this->order->orderGroup->delivery_amount = $deliveryWindowAmount['delivery_amount'];
        $this->order->delivery_amount = $deliveryWindowAmount['delivery_amount'];
        $this->order->orderGroup->save();
        $this->log();
        $this->order->save();
    }

    /**
     * Almacena en el log cambio en la fecha y el motivo de la reprogramación.
     */
    private function log()
    {
        $oldDeliveryDate = format_date('normal_with_time', $this->order->getOriginal('delivery_date'));
        $newDeliveryDate = format_date('normal_with_time', $this->order->delivery_date);

        $log = new \OrderLog();
        $log->type = "Fecha de entrega actualizada: {$oldDeliveryDate} a {$newDeliveryDate}. " .
            "Razón: '$this->reason'";
        $log->user_id = $this->order->user_id;
        $log->order_id = $this->order->id;
        $log->save();
    }

    /**
     * @throws MerqueoException
     */
    private function validateDeliveryToAddress()
    {
        $orderGroup = $this->order->orderGroup;
        $delivery_window = $this->order->store->validateStoreDeliversToAddress(
            $orderGroup->user_address_latitude,
            $orderGroup->user_address_longitude,
            new Carbon($this->deliveryDay),
            $this->deliveryTime
        );

        $this->order->deliveryWindow()->associate($delivery_window);
    }

    /**
     * @param array $slots
     * @return array
     */
    private function removeDeliveryWindowER($slots)
    {
        $today = Carbon::now()->format('Y-m-d');

        if (isset($slots['days'][$today]) && isset($slots['time'][$today])) {

            $deliveryWindowsER = DeliveryWindow::where('shifts', DeliveryWindow::SHIFT_FAST_DELIVERY)
                ->where('city_id', $this->order->store['city_id'])
                ->get()
                ->toArray();

            foreach ($slots['time'][$today] as $deliveryWindowId => $slot) {
                $deliveryWindowER = array_filter($deliveryWindowsER, function ($deliveryWindow) use ($deliveryWindowId) {
                    return $deliveryWindowId === $deliveryWindow['id'];
                });

                // Se remueve de los slots, el slot de entrega rápida
                if (!empty($deliveryWindowER)) {
                    unset($slots['time'][$today][$deliveryWindowId]);
                }
            }

            if (empty($slots['time'][$today])) {
                unset($slots['days'][$today]);
            }
        }

        return $slots;
    }
}
