<?php

namespace orders;

/**
 * Class OrderCredit
 * @package orders
 */
class OrderCredit
{
    /**
     * @var \Order
     */
    protected $order;

    /**
     * @var \UserCredit
     */
    protected $user_credit;

    /**
     * OrderCredit constructor.
     * @param \Order $order
     * @param \UserCredit $user_credit
     */
    public function __construct(\Order $order, \UserCredit $user_credit)
    {
        $this->order = $order;
        $this->user_credit = $user_credit;
    }

    public function discount()
    {

    }
}