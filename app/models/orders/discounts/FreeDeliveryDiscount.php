<?php

namespace orders\discounts;

use Carbon\Carbon;

/**
 * @property string $description
 */
final class FreeDeliveryDiscount extends \OrderDiscount
{
    const BY_CITY = 1;

    const BY_STORE = 2;

    /**
     * @var \User
     */
    private $user;

    /**
     * @var float
     */
    private $amount;

    /**
     * @var int
     */
    private $reason;

    /**
     * FreeDeliveryDiscount constructor.
     * @param $amount
     */
    public function __construct($amount)
    {
        parent::__construct();
        $this->amount = $amount;
    }

    /**
     * @param \User $user
     * @return string
     */
    public function setUser(\User $user)
    {
        $this->user = $user;
    }

    /**
     * @param int $reason FreeDeliveryDiscount::BY_STORE, FreeDeliveryDiscount::BY_CITY
     */
    public function setReason($reason)
    {
        if (!in_array($reason, [self::BY_CITY, self::BY_STORE])) {
            throw new \InvalidArgumentException(
                'Available types: FreeDeliveryDiscount::BY_STORE, FreeDeliveryDiscount::BY_CITY'
            );
        }

        $this->reason = $reason;
    }

    /**
     * @param array $options
     * @return bool
     */
    public function save(array $options = [])
    {
        if (empty($this->user)) {
            throw new \InvalidArgumentException('Se debe asignar el usuario al cual se le dio el domicilio gratis.');
        }

        $description = '';
        $free_delivery_expiration_date = new Carbon($this->user->free_delivery_expiration_date);
        $this->attributes['type'] = 'Domicilio gratis';
        if (!empty($this->user->free_delivery_next_order)) {
            $description = 'Domicilio gratis en el próximo pedido';
        } elseif (!empty($this->user->free_delivery_expiration_date) && $free_delivery_expiration_date > Carbon::create()) {
            $description = "Domicilio gratis hasta {$this->user->free_delivery_expiration_date}";
        } elseif ($this->user->orders()->count() === 1) {
            $description = 'Domicilio gratis por primera compra.';
        } elseif (!empty($this->reason)) {
            $description = 'Domicilio gratis por ' . $this->type === self::BY_STORE ? 'Tienda' : 'Ciudad';
        }

        $this->attributes['description'] = $description;

        return parent::save($options);
    }
}