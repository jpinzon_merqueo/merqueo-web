<?php

namespace orders\discounts;

use exceptions\MerqueoException;
use Illuminate\Support\Collection;

/**
 * @property string $description
 */
final class DeliveryDiscountByProduct extends \OrderDiscount
{
    /**
     * Listado de productos que ofrecen descuento en el domicilio.
     * @var Collection|\StoreProduct[]
     */
    private $store_products;

    /**
     * @var int
     */
    private $total_amount = 0;

    /**
     * DeliveryDiscountByProduct constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->store_products = new Collection();
    }

    /**
     * Obtiene el texto del campo "description".
     *
     * @return string
     */
    private function setDiscountDetails()
    {
        $description = "Descuento en domicilio por productos:\n";
        foreach ($this->store_products as $store_product) {
            $this->amount += $store_product->delivery_discount_amount;
            $description .= $this->getProductDetails($store_product);
        }

        $this->description = $description;
    }

    /**
     * @param \StoreProduct $product
     * @throws MerqueoException
     */
    public function add(\StoreProduct $product)
    {
        if (empty($product->delivery_discount_amount)) {
            throw new MerqueoException('El producto no cuenta con descuento.');
        }

        $this->total_amount += $product->delivery_discount_amount;
        $this->store_products->push($product);
    }

    /**
     * @return int
     */
    public function count()
    {
        return $this->store_products->count();
    }

    /**
     * Obtiene el total del descuento obtenido por los productos.
     *
     * @return int
     */
    public function getTotalDiscount()
    {
        return $this->total_amount;
    }

    /**
     * TODO Almacenar el descuento por producto para evitar que se modifique el valor del objeto.
     *
     * @param $key
     */
    public function forget($key)
    {
        $store_product = $this->store_products->get($key);
        $this->total_amount -= $store_product->delivery_discount_amount;
        $this->store_products->forget($key);
    }

    /**
     * @param array $options
     * @return bool
     * @throws MerqueoException
     */
    public function save(array $options = [])
    {
        if (!$this->store_products->count()) {
            throw new MerqueoException('No se han agregado productos de descuento.');
        }

        $this->attributes['type'] = 'Descuento';
        $this->setDiscountDetails();

        return parent::save($options);
    }

    /**
     * Obtiene el texto que se va a almacenar en el campo "description".
     *
     * @param \StoreProduct $store_product
     * @return string
     */
    private function getProductDetails(\StoreProduct $store_product)
    {
        $product = $store_product->product;
        $name = empty($product->name) ? 'ELIMINADO' : $product->name;
        $amount = currency_format($store_product->delivery_discount_amount);

        return "#{$store_product->id}, Nombre: {$name}, Descuento: {$amount}\n";
    }
}
