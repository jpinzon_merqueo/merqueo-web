<?php

namespace orders;

use exceptions\MerqueoException;
use Illuminate\Support\Collection;

/**
 * Clase OrderProductUpdating que agrega producto a una orden si es neceario.
 * @package orders
 */
class OrderProductUpdating
{
    /**
     * @var \Order
     */
    protected $primary_order;

    /**
     * @var int
     */
    protected $delivery_discount_amount = 0;

    /**
     * @var bool
     */
    protected $has_new_products = false;

    /**
     * @var array
     */
    private $products_to_remove = [];

    /**
     * @var OrderProductValidator
     */
    protected $product_validation;

    /**
     * @var OrderBuilder
     */
    protected $order_builder;

    /**
     * OrderProductUpdating constructor.
     * @param \Order $primary_order
     * @param OrderProductValidator $validator
     * @param OrderBuilder $order_builder
     */
    public function __construct(\Order $primary_order, OrderProductValidator $validator, OrderBuilder $order_builder)
    {
        $this->primary_order = $primary_order;
        $this->product_validation = $validator;
        $this->order_builder = $order_builder;
    }

    /**
     * @param \Cart $cart
     * @throws MerqueoException
     */
    public function addFromCart(\Cart $cart)
    {
        $this->findValidOrders();
        $current_order = $this->order_builder->findOrCreateOrder($cart->alliedStore, count($cart->products));
        if (!$current_order) {
            return;
        }

        $this->getNewOrderStatus($current_order, $cart, $this->primary_order->id == $current_order->id);

        $this->primary_order->orderGroup->addDeliveryDiscount($this->delivery_discount_amount);
        $this->primary_order->orderGroup->updateTotals();
        $order_status = new OrderValidator($this->primary_order);
        $order_status->validateTotals();
    }

    /**
     * @param \Order $order
     * @param \Cart $cart
     * @param bool $remove_items
     * @return null|\Order
     * @throws MerqueoException
     */
    private function getNewOrderStatus(\Order $order, \Cart $cart, $remove_items = false)
    {
        $this->products_to_remove = [];
        $order->load([
            'orderProducts' => function ($query) use ($order) {
                // Es necesario realizar el ordenamiento por
                // store_product_id para realizar la busqueda binaria.
                $query->whereNull('sampling_id')
                    ->where('is_gift', 0)
                    ->with('orderProductGroup')
                    ->orderBy('store_product_id');
            }
        ]);

        $old_products = clone $order->orderProducts;
        $current_total_products = count($order->orderProducts);

        foreach ($cart->products as $cart_product) {
            $order_products = $this->get_products_by_content($old_products, $cart_product->store_product_id);

            if (empty($order_products) && $cart_product->quantity) {
                $this->addProduct($order, $cart_product);
                if (!$remove_items) {
                    $this->has_new_products = true;
                }
                continue;
            }

            if (empty($order_products)) {
                continue;
            }

            $quantity_special_price = $cart_product->quantity - $cart_product->quantity_full_price;
            $sell_off_item = $this->get_sell_off_product($order_products);
            $full_price_item = $this->get_full_price_product($order_products);

            if (!empty($sell_off_item)) {
                $this->product_validation->validateOrderProductSellOffChange($sell_off_item, $quantity_special_price);
                if ($quantity_special_price > 0) {
                    $sell_off_item->quantity = $quantity_special_price;
                    $sell_off_item->quantity_original = $quantity_special_price;
                    $cart_product->quantity -= $quantity_special_price;
                    if ($sell_off_item->isDirty('quantity')) {
                        $sell_off_item->save();

                        if ($sell_off_item->type === 'Agrupado') {
                            $this->setNewQuantity($sell_off_item, $quantity_special_price);
                        }

                        \Event::fire('order.product_log', [[$sell_off_item, $order->user_id, 'user']]);
                    }
                } else {
                    $this->products_to_remove[] = $sell_off_item;
                }
            }

            if (!empty($full_price_item)) {
                if ($cart_product->quantity_full_price > 0 && $full_price_item->quantity != $cart_product->quantity_full_price) {
                    $full_price_item->quantity = $cart_product->quantity_full_price;
                    $full_price_item->quantity_original = $cart_product->quantity_full_price;

                    if ($full_price_item->isDirty('quantity')) {
                        $full_price_item->save();

                        if ($full_price_item->type === 'Agrupado') {
                            $this->setNewQuantity($full_price_item, $cart_product->quantity_full_price);
                        }

                        \Event::fire('order.product_log', [[$full_price_item, $order->user_id, 'user']]);
                    }
                } elseif (!$cart_product->quantity_full_price) {
                    $this->products_to_remove[] = $full_price_item;
                }
            } elseif ($cart_product->quantity_full_price) {
                $this->addProduct($order, $cart_product);
                if (!$remove_items) {
                    $this->has_new_products = true;
                }
            }
        }


        if ($current_total_products === count($this->products_to_remove) && $current_total_products > 0) {
            $this->cancelOrder($order);
            return null;
        }

        if ($remove_items) {
            $this->removeItems($old_products);
        }

        $this->products_to_remove = [];
        $order->updateTotals();
        $order->save();

        return $order;
    }

    /**
     * @param \OrderProduct $orderProduct
     * @param $quantity
     * @return void
     */
    private function setNewQuantity(\OrderProduct $orderProduct, $quantity)
    {
        foreach ($orderProduct->orderProductGroup as $orderProductGroup) {
            foreach ($orderProduct->storeProduct->storeProductGroup as $item) {
                if ($item->store_product_group_id == $orderProductGroup->store_product_id) {
                    $orderProductGroup->quantity = $quantity * $item->quantity;
                    $orderProductGroup->quantity_original = $orderProductGroup->quantity;
                    $orderProductGroup->save();
                }
            }
        }
    }

    /**
     * @param $list
     * @return \OrderProduct|null
     */
    private function get_sell_off_product($list)
    {
        foreach ($list as $item) {
            if ($item->price < $item->original_price) {
                return $item;
            }
        }
    }

    /**
     * @param $list
     * @return \OrderProduct|null
     */
    private function get_full_price_product($list)
    {
        foreach ($list as $item) {
            if ((int)$item->price == (int)$item->original_price) {
                return $item;
            }
        }
    }

    /**
     * @param Collection|array $list
     * @param mixed $find
     * @return array
     */
    private function get_products_by_content($list, $find)
    {
        $order_product = binary_search($find, $list, 'store_product_id');

        if (empty($order_product)) {
            return [];
        }

        foreach ($list as $key => $item) {
            if ($item->id == $order_product->id) {
                $list->splice($key, 1);
            }
        }

        $items = $this->get_products_by_content($list, $find);

        return array_merge([$order_product], $items);
    }

    /**
     *
     */
    private function findValidOrders()
    {
        $this->primary_order->load([
            'orderGroup.orders' => function ($query) {
                $query->where('status', '<>', OrderStatus::CANCELED);
            }
        ]);
    }

    public function hasNewProducts()
    {
        return $this->has_new_products;
    }

    /**
     * @param \Order $order
     * @param \CartProduct $cart_product
     *
     * @throws MerqueoException
     */
    private function addProduct(\Order $order, \CartProduct $cart_product)
    {
        $quantity_special_price = $cart_product->quantity - $cart_product->quantity_full_price;
        $store_product = $cart_product->storeProduct;

        if (empty($store_product) || !$store_product->storeProductWarehouses->count()) {
            $name = !empty($cart_product->storeProduct->product) ? $cart_product->storeProduct->product->name : '';
            throw new MerqueoException("El producto {$name} no se encuentra disponible.");
        }

        if ($quantity_special_price > 0 && $cart_product->special_price > 0) {
            $order_product = \OrderProduct::saveProduct(
                $cart_product->special_price,
                $quantity_special_price,
                $store_product,
                $order
            );

            \Event::fire('order.product_log', [[$order_product, $order->user_id, 'user']]);
        }

        if ($cart_product->quantity_full_price > 0 && $cart_product->price > 0) {
            $order_product = \OrderProduct::saveProduct(
                $cart_product->price,
                $cart_product->quantity_full_price,
                $store_product,
                $order
            );

            \Event::fire('order.product_log', [[$order_product, $order->user_id, 'user']]);
        }

        if (!empty($order_product)) {
            $this->delivery_discount_amount += $store_product->delivery_discount_amount;
            $order->orderProducts->push($order_product);
        }
    }

    /**
     * @param \Order $order
     * @throws MerqueoException
     */
    private function cancelOrder(\Order $order)
    {
        $cancellation = new CancelOrder($order);
        $reason = \RejectReason::hidden()->first();
        $cancellation->manage($reason);
    }

    /**
     * @param \OrderProduct[] $old_products
     */
    private function removeItems($old_products)
    {
        $products_to_remove = $old_products->merge($this->products_to_remove);
        foreach ($products_to_remove as $product) {
            if (empty($product->sampling_id)) {
                $this->delivery_discount_amount -= $product->product_delivery_discount_amount;
                $product->deleteSamplingProductChild();
                $product->delete();
                $product->orderProductGroup()->delete();
                \Event::fire('order.product_remove_log', [[$product, null], true]);
            }
        }
    }
}
