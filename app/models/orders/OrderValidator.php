<?php

namespace orders;

use Carbon\Carbon;
use exceptions\MerqueoException;
use function GuzzleHttp\Psr7\try_fopen;

/**
 * Class OrderValidator
 * @package orders
 */
class OrderValidator
{
    /**
     * @var \Order
     */
    protected $order;

    /**
     * @var array
     */
    protected $payments;

    /**
     * OrderStatus constructor.
     * @param \Order $order
     */
    public function __construct(\Order $order)
    {
        $this->order = $order;
    }

    /**
     * Determina si el pedido puede ser cancelado.
     *
     * @return void
     * @throws MerqueoException
     */
    public function canBeCancelled()
    {
        $total_amount = 0;
        $this->validateDeliveryStatus();

        if ($this->order->payment_method !== 'Débito - PSE' && !in_array($this->order->status, [OrderStatus::VALIDATION, OrderStatus::INITIATED])) {
            throw new MerqueoException('No puedes reprogramar el pedido.');
        }

        $orders = $this->order->orderGroup->orders()
            ->where('status', '<>', OrderStatus::CANCELED)
            ->get();

        if ($orders->count() === 1) {
            return;
        }

        foreach ($orders as $order) {
            if ($order->id == $this->order->id) {
                continue;
            }

            $total_amount += $order->total_amount;
        }

        if ($total_amount < $this->order->store->minimum_order_amount) {
            throw new MerqueoException('Valor mínimo de pedido no alcanzado.');
        }
    }

    /**
     * @throws MerqueoException
     */
    public function canChangeTime()
    {
        $this->validateIfIsAlreadyPayedByPse();

        $limit = RescheduleOrder::LIMIT_RESCHEDULE;
        $valid_status = [OrderStatus::VALIDATION, OrderStatus::INITIATED];
        $this->validateDeliveryStatus();
        if (!in_array($this->order->status, $valid_status) || !empty($this->order->route_id)) {
            throw new MerqueoException('No puedes reprogramar el pedido.');
        }

        if ($this->order->scheduled_delivery >= $limit) {
            $suffix = $limit > 1 ? 'ces' : 'z';
            throw new MerqueoException("El pedido solo se puede reprogramar {$limit} ve{$suffix}.");
        }
    }

    /**
     * @throws MerqueoException
     */
    public function canAddReferenceCode()
    {
        $this->validateIfIsAlreadyPayedByPse();
        $this->validateOrderStatus();
        $this->validateCreditCardPayments();
        $this->canAddAnotherDiscount();
        $this->validateDeliveryExpress();
    }

    /**
     * @param int $amount
     * @throws MerqueoException
     */
    public function canAddDiscount($amount = 0)
    {
        $this->validateIfIsAlreadyPayedByPse();
        $this->validateDeliveryStatus();
        $this->validateCreditCardPayments();
        $this->canAddAnotherDiscount();
        $this->validateAmountDiscount($amount);
    }

    /**
     * @param $amount
     * @throws MerqueoException
     */
    public function canAddCoupon($amount = 0)
    {
        $this->validateIfIsAlreadyPayedByPse();
        // TODO Validar que solo pueda agregar un cupón
        $this->validateCreditCardPayments();
        $this->validateAmountDiscount($amount);
        $this->canAddAnotherDiscount();
    }

    /**
     * @param string $payment_method
     * @throws MerqueoException
     */
    public function canChangePaymentMethod($payment_method = '')
    {
        $this->validateIfIsAlreadyPayedByPse();
        $older_payment_method = $this->order->payment_method;
        $valid_status = [
            OrderStatus::VALIDATION,
            OrderStatus::INITIATED,
            OrderStatus::ROUTED,
            OrderStatus::IN_PROGRESS
        ];

        $this->validateDeliveryStatus();
        $this->validateCreditCardPayments();
        $this->validateDeliveryExpress();

        if ($this->order->payment_method === 'Tarjeta de crédito' && !in_array($this->order->status, $valid_status)) {
            throw new MerqueoException('El pedido ya fue alistado.');
        }
        if ($older_payment_method === $payment_method || empty($payment_method)) {
            return;
        }
    }

    /**
     * @throws MerqueoException
     */
    public function canShowDeliveryIsLate()
    {
        if ($this->order->status !== OrderStatus::DISPATCHED || empty($this->order->onmyway_date)) {
            throw new MerqueoException('El pedido aún no ha sido despachado.');
        }
    }

    /**
     * @throws MerqueoException
     */
    public function canAssociateCompanyOnBill()
    {
        if ($this->order->status === OrderStatus::CANCELED) {
            throw new MerqueoException('El pedido ha sido cancelado.');
        }

        if (!empty($this->order->invoice_number)) {
            throw new MerqueoException('El pedido ya se ha facturado.');
        }
    }

    /**
     * @throws MerqueoException
     */
    private function validateOrderStatus()
    {
        if ($this->order->status !==  OrderStatus::VALIDATION || $this->order->status !== OrderStatus::INITIATED) {
            throw new MerqueoException('El pedido ya esta en proceso de alistamiento.');
        }

        $this->validateDeliveryStatus();
    }

    /**
     * @throws MerqueoException
     */
    private function validateDeliveryStatus()
    {
        if ($this->order->status === OrderStatus::CANCELED) {
            throw new MerqueoException('El pedido ya se ha cancelado.');
        }

        if ($this->order->status === OrderStatus::DELIVERED) {
            throw new MerqueoException('El pedido ya se ha entregado.');
        }
    }

    /**
     * @throws MerqueoException
     */
    private function validateCreditCardPayments()
    {
        if ($this->order->lastPaymentByCardWasSucceed()) {
            throw new MerqueoException('El total del pedido ya fue cobrado a la tarjeta.');
        }
    }

    /**
     * @throws MerqueoException
     */
    private function canAddAnotherDiscount()
    {
        $order_discounts = \UserCredit::where('order_id', $this->order->id)->count();
        if ($order_discounts > 0) {
            throw new MerqueoException('Ya se ha agregado un descuento al pedido.');
        }
    }

    private function getPayments()
    {
        if ($this->payments) {
            return $this->payments;
        }

        return $this->payments = $this->order->getPayments(true);
    }

    /**
     * @param string $selected_method
     * @return null
     * @throws MerqueoException
     */
    public function validatePaymentMethodChange($selected_method)
    {
        $this->validateIfIsAlreadyPayedByPse();
        $methods = array_values(\OrderPayment::getPaymentMethods());
        $this->validateDeliveryStatus();

        if (!in_array($selected_method, $methods)) {
            throw new MerqueoException('El método de pago seleccionado no es valido.');
        }

        if (\OrderPayment::hasCreditCardPaymentMethod($this->order->payment_method) || \OrderPayment::hasCreditCardPaymentMethod($selected_method)) {
            $valid_order_status = in_array(
                $this->order->status,
                [OrderStatus::VALIDATION, OrderStatus::INITIATED, OrderStatus::ROUTED]
            );

            if ($valid_order_status) {
                return;
            }

            switch ($this->order->status) {
                case OrderStatus::DISPATCHED:
                    throw new MerqueoException('Tu pedido ha sido despachado, ya no es posible hacer un cambio en el método de pago.');
                case OrderStatus::PREPARED:
                    throw new MerqueoException('Tu pedido ha sido alistado, ya no es posible hacer un cambio en el método de pago.');
                default:
                    throw new MerqueoException('No es posible hacer un cambio en el método de pago.');
            }
        }
    }

    /**
     * @return bool
     */
    private function notDispatched()
    {
        return in_array($this->order->status, [OrderStatus::VALIDATION, OrderStatus::INITIATED, OrderStatus::ROUTED]);
    }

    /**
     * @param $discount
     * @throws MerqueoException
     */
    private function validateAmountDiscount($discount)
    {
        if ($discount > $this->order->total_amount) {
            throw new MerqueoException('El cupón supera el valor del pedido.');
        }
    }

    /**
     * @throws MerqueoException
     */
    public function canAddProducts()
    {
        $this->validateIfIsAlreadyPayedByPse();
        $this->validateDeliveryStatus();
        $this->validateDeliveryExpress();

        $created_at = new Carbon($this->order->created_at);
        if (self::getHelpCenterReleaseDate() > $created_at) {
            throw new MerqueoException(
                'Las ordenes no cuenta con los campos "quantity_special_price_stock, product_delivery_discount_amount"' .
                ' que son necesarios para poder agregar nuevo productos al pedido.'
            );
        }

        if (!$this->notDispatched()) {
            throw new MerqueoException('El pedido ya ha sido despachado.');
        }

        foreach ($this->order->orderGroup->orders as $order) {
            if ($order->status === OrderStatus::DELIVERED) {
                throw new MerqueoException('El pedido ya se ha entregado.');
            }
        }
    }

    /**
     * @return Carbon
     */
    public static function getHelpCenterReleaseDate()
    {
        return Carbon::create(2018, 5, 17);
    }

    /**
     * @throws MerqueoException
     */
    public function validateTotals()
    {
        $this->order->load('store');

        if ($this->order->orderGroup->total_amount < $this->order->store->minimum_order_amount) {
            throw new MerqueoException('Valor mínimo de pedido no alcanzado.');
        }

        if ($this->orderHasReferralCodeCredit() && $this->order->orderGroup->total_amount < $this->getMinimumOrderAmountWithReferralCode()) {
            throw new MerqueoException('Valor mínimo de pedido no alcanzado.');
        }

        // consulta si la orden tiene cupones asociados
        $coupons = $this->order->orderHasCoupons();
        // en caso de tener cupones verifica si la orden cumple con los montos maximos y minimos del cupon
        if (!empty($coupons) && $coupons->count() && $validateTotalAmounts = $coupons->validateTotalAmounts($this->order, true)) {
            throw new MerqueoException($validateTotalAmounts);
        }
    }

    /**
     * @return int
     */
    private function getMinimumOrderAmountWithReferralCode()
    {
        return \Config::get('app.minimum_discount_amount');
    }

    /**
     * @return bool
     */
    private function orderHasReferralCodeCredit()
    {
        return $this->order->userCredits()
                ->where('description', 'LIKE', 'Usaste el código de%')
                ->count() > 0;
    }

    /**
     * @throws MerqueoException
     */
    private function validateIfIsAlreadyPayedByPse()
    {
        $lastPayment = $this->order->orderPayments->sortBy('updated_at')->last();

        if ($this->order->payment_method == 'Débito - PSE' && ($lastPayment && $lastPayment->cc_payment_status == 'Aprobada')) {
            throw new MerqueoException('La orden ya ha sido pagada.');
        }
    }

    /**
     * Valida qué la orden es express
     * @throws MerqueoException
     */
    private function validateDeliveryExpress()
    {
        if ($this->order->deliveryWindow->shifts == \Order::SHIFT_EXPRESS) {
            throw new MerqueoException('Acción no permitida para horario express');
        }
    }

    /**
     * Validate the menus and created new item of menu help center
     * @param array $items_menu = Menu del help`center
     * @return array
     */

    public function validateAndCreateMenuNewVersion(array $items_menu)
    {
        $items = [];

        foreach ($items_menu as $item) {
            try {
                foreach ($item['validation_method'] as $validate) {
                    call_user_func([$this, $validate]);
                }
                $responseMenu = false;

            } catch (\Exception $exception) {
                if (stristr($exception->getMessage(), 'Deshabilitado temporalmente' ) === false){
                    continue;
                }
                $responseMenu = true;
            }

            $items[] = [
                'id' => $item['id'],
                'text' => $item['text'],
                'menu_disabled' => $responseMenu
            ];
        }
        return $items;
    }

    /**
     * validate the items of the menu Help_center and the show.
     *
     * @param array $menu_items = menu items Help Center
     * @param array $hide = text of items, what's wrong with it what hide
     * @return array = items of menu
     */
    public function validateAndCreateMenuVersionPrevious(array $menu_items, array $hide)
    {
        $items = [];

        foreach ($menu_items as $item) {
            if (in_array($item['text'], $hide)) continue;

            try {
                foreach ($item['validation_method'] as $validate) {
                    call_user_func([$this, $validate]);
                }
                $items[] = [
                    'id' => $item['id'],
                    'text' => $item['text']
                ];

            } catch (\Exception $exception) {
            }
        }

        return $items;
    }

}
