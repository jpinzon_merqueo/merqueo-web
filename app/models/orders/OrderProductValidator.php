<?php

namespace orders;

use Carbon\Carbon;
use exceptions\MerqueoException;

/**
 * Class OrderProductValidator
 * @package orders
 */
class OrderProductValidator
{
    /**
     * @var \Order
     */
    protected $order;

    /**
     * @var bool
     */
    protected $user_has_ordered;

    /**
     * OrderProductValidator constructor.
     * @param \Order $order
     */
    public function __construct(\Order $order)
    {
        $this->order = $order;
        $this->user_has_ordered = $order->user->orders()
                ->where('status', '<>', OrderStatus::CANCELED)
                ->where('id', '<>', $order->id)
                ->count() > 0;
    }

    /**
     * @param \StoreProduct $store_product
     * @param $quantity_special_price
     * @throws MerqueoException
     */
    public function validateAddProduct(\StoreProduct $store_product, $quantity_special_price)
    {
        $this->validateProductWarehouseStatus($store_product);
        if ($quantity_special_price === 0) {
            return;
        }
        $this->validateAlreadyBoughtProduct($store_product);
        $this->validatePromoProductQuantities($store_product, $quantity_special_price);
    }

    /**
     * @param \OrderProduct $order_product
     * @param $quantity_special_price
     * @throws MerqueoException
     */
    public function validateOrderProductSellOffChange(\OrderProduct $order_product, $quantity_special_price)
    {
        if ($order_product->quantity_special_price_stock > 0 && $order_product->quantity_special_price_stock < $quantity_special_price) {
            throw new MerqueoException(
                "Solo puedes agregar {$order_product->quantity_special_price_stock} " .
                "unidad(es) del producto {$order_product->product_name}"
            );
        }
    }

    /**
     * @param \StoreProduct $store_product
     * @throws MerqueoException
     */
    private function validateProductWarehouseStatus(\StoreProduct $store_product)
    {
        foreach ($store_product->storeProductWarehouses as $product_warehouse) {
            if (!$product_warehouse->status) {
                throw new MerqueoException(
                    "El producto {$store_product->product->name} ya no esta disponible, por favor " .
                    "eliminalo del carrito de compras para continuar."
                );
            }
        }
    }

    /**
     * @param \StoreProduct $store_product
     * @throws MerqueoException
     */
    private function validateAlreadyBoughtProduct(\StoreProduct $store_product)
    {
        $date = Carbon::create();
        $minimum_promo_days = Config::get('app.minimum_promo_days');
        $count_products = \Order::join('order_products', 'orders.id', '=', 'order_id')
            ->where('order_id', '<>', $this->order->id)
            ->where('store_product_id', $store_product->id)
            ->where('price', $store_product->special_price)
            ->where('user_id', $this->order->user->id)
            ->where('status', '<>', OrderStatus::CANCELED)
            ->where(\DB::raw("DATEDIFF('{$date->format('Y-m-d')}', date)"), '<=', $minimum_promo_days)
            ->groupBy('order_products.store_product_id')
            ->count();

        if ($count_products) {
            throw new MerqueoException("Ya realizaste un pedido con '{$store_product->product->name}' en promoción");
        }
    }

    /**
     * @param \StoreProduct $store_product
     * @param $quantity_special_price
     * @throws MerqueoException
     */
    private function validatePromoProductQuantities(\StoreProduct $store_product, $quantity_special_price)
    {
        if ($quantity_special_price > $store_product->quantity_special_price_stock || $store_product->first_order_special_price && $this->user_has_ordered) {
            throw new MerqueoException(
                "El producto {$store_product->product->name} por estar en promoción puedes agregar máximo " .
                "{$store_product->quantity_special_price_stockquantity_special_price_stock} unidad(es) en tu pedido, por favor disminuye la " .
                "cantidad en el carrito para continuar."
            );
        }
    }

    /**
     * @param \Order $order
     */
    public function setOrder(\Order $order)
    {
        $this->order = $order;
    }
}
