<?php

namespace orders;

/**
 * Trait OrderAttributes
 * @package orders
 */
trait OrderAttributes
{
    /**
     * Obtiene el total de pedido
     *
     * @return int
     */
    public function getFinalAmountAttribute()
    {
        return $this->total_amount + $this->delivery_amount - $this->discount_amount;
    }

    /**
     * Devuelve el descuento de precios especiales del grupo
     *
     * @return double
     */
    public function getSpecialPriceAttribute()
    {
        $specialPrice = 0;

        foreach ($this->orderProducts as $orderProduct) {
            if ($orderProduct->promo_type) {
                $specialPrice += ($orderProduct->original_price - $orderProduct->price) * $orderProduct->quantity;
            }
        }

        return $specialPrice;
    }
}
