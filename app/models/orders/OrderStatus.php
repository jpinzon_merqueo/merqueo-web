<?php

namespace orders;

/**
 * Class OrderStatus
 * @package orders
 */
class OrderStatus
{
    const VALIDATION = 'Validation';
    const INITIATED = 'Initiated';
    const ROUTED = 'Enrutado';
    const IN_PROGRESS = 'In Progress';
    const PREPARED = 'Alistado';
    const DISPATCHED = 'Dispatched';
    const DELIVERED = 'Delivered';
    const CANCELED = 'Cancelled';
}
