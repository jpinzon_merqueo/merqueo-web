<?php

namespace orders;

use Carbon\Carbon;
use RejectReason;
use Order;
use OrderProduct;
use OrderProductGroup;
use OrderReturn;
use UserBrandCampaign;
use VehicleMovement;
use OrderGroup;
use UserCredit;
use exceptions\MerqueoException;
use repositories\Eloquent\RejectReasonRepository;

/**
 * Class CancelOrder
 * @package orders
 */
class CancelOrder
{
    const NEW_STATUS = 'Cancelado';
    const NEW_ATTENDANT = 'Help Center';

    /**
     * @var Order
     */
    protected $order;
    protected $rejectReasonRepo;

    /**
     * CancelOrder constructor.
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
        $this->rejectReasonRepo = new RejectReasonRepository();
    }

    /**
     * @param RejectReason $reason
     * @throws MerqueoException
     * @return void
     */
    public function manage(RejectReason $reason)
    {
        if ($this->order->payment_method !== 'Débito - PSE' &&
            $this->order->lastPaymentByCardWasSucceed()
        ) {
            throw new MerqueoException(
                'El total del pedido ya fue cobrado a la tarjeta.'
            );
        }
        if ($this->order->payment_date) {
            $this->order->order_validation_reason_id = 7;
        }
        $this->order->status = OrderStatus::CANCELED;
        $this->order->management_date = Carbon::create();

        $rejectReason = $this->convertRejectReason(
            $reason->reason,
            $this->rejectReasonRepo
        );
        $this->order->reject_reason_id = $rejectReason->id;
        $this->order->temporarily_cancelled = 0;
        $this->order->scheduled_delivery = 0;
        $this->order->reject_comments = 'Cancelado desde el Help Center.';

        UserCredit::returnCredit($this->order);
        $oldOrderStatus = $this->order->getOriginal('status');
        if (!in_array($oldOrderStatus, [OrderStatus::DISPATCHED, OrderStatus::DELIVERED])) {
            $this->order->product_return_storage = 0;
            if ($oldOrderStatus === OrderStatus::PREPARED) {
                $this->order->updateInvestigationStockBack($oldOrderStatus);
            }
        } else {
            OrderProduct::where('order_id', $this->order->id)
                ->whereIn('fulfilment_status', ['Fullfilled', 'Returned'])
                ->update(['fulfilment_status' => 'Returned']);
            OrderProductGroup::where('order_id', $this->order->id)
                ->whereIn('fulfilment_status', ['Fullfilled', 'Returned'])
                ->update(['fulfilment_status' => 'Returned']);
            $this->order->product_return_storage = 1;
            OrderReturn::deleteOrderReturn($this->order->id, $this->order->status);
            OrderReturn::saveOrderReturn($this->order->id, $oldOrderStatus);
            if ($oldOrderStatus == OrderStatus::DISPATCHED) {
                $this->order->updateInvestigationStockBack($oldOrderStatus);
            }
        }
        UserBrandCampaign::where('order_id', $this->order->id)->delete();
        $vehicleMovement = VehicleMovement::where('order_id', $this->order->id)
            ->where('status', 1)
            ->first();
        if ($vehicleMovement) {
            $this->descriptionVehicleMovement(
                $vehicleMovement,
                $this->order->product_return_storage
            );
        }
        $this->sendPushNotification(
            $this->order->vehicle_id,
            $this->order->group_id,
            $this->order->route_id,
            $this->order->id,
            $this->order->planning_sequence,
            $this->order->picker_dry_id,
            $this->order->picker_cold_id,
            $oldOrderStatus
        );
        $this->reassignDeliveryAmountIfNeeded();
        $this->order->save();
    }

    /**
     * Reasigna el total del descuento a otra orden si es necesario
     * @return void
     */
    protected function reassignDeliveryAmountIfNeeded()
    {
        if (!$this->order->delivery_amount) {
            return;
        }
        $orderWithDeliveryAmount = $this->order->orderGroup
            ->orders()
            ->where('status', '<>', OrderStatus::CANCELED)
            ->where('delivery_amount', '>', 0)
            ->count();
        if ($orderWithDeliveryAmount > 0) {
            return;
        }
        foreach ($this->order->orderGroup->orders as $order) {
            if ($order->id == $this->order->id ||
                in_array($order->status, [OrderStatus::CANCELED])
            ) {
                continue;
            }
            $order->delivery_amount = $this->order->delivery_amount;
            $order->save();
            return;
        }
    }

    /**
     * Recibe el anterior reject reason y retorna el correspondiente nuevo
     * @param string reason
     * @return RejectReason rejectReason
     */
    public function convertRejectReason($reason)
    {
        switch ($reason) {
            case 'Elegí un horario incorrecto':
                $reason = ['Modificación de Franja', 'Elegí un horario incorrecto'];
                break;
            case 'No voy a estar en la dirección':
                $reason = ['No estaré', 'No voy a estar'];
                break;
            case 'Se me olvidaron algunos productos':
                $reason = ['Modificación de Productos', 'Se me olvidaron algunos productos'];
                break;
            default:
                return new RejectReason();
        }
        return $this->rejectReasonRepo->getRejectReasonIdUserByReasons(
            self::NEW_STATUS,
            $reason,
            self::NEW_ATTENDANT
        );
    }

    /**
     * Envia notificación tipo push a la app
     * @param int | null $vehicleId
     * @param int | null $groupId
     * @param int | null $routeId
     * @param int | null $orderId
     * @param int | null $planningSequence
     * @param int | null $pickerDryId
     * @param int | null $pickerColdId
     * @param $oldOrderStatus
     */
    private function sendPushNotification(
        $vehicleId,
        $groupId,
        $routeId,
        $orderId,
        $planningSequence,
        $pickerDryId,
        $pickerColdId,
        $oldOrderStatus
    ) {
        if ($vehicleId && $oldOrderStatus === OrderStatus::DISPATCHED) {
            $orderGroup = OrderGroup::find($groupId);
            $route = \Routes::find($routeId);
            if ($route) {
                $data = [
                    'order_id' => $orderId,
                    'user_address' => $orderGroup->user_address,
                    'route' => $route->route,
                    'planning_sequence' => $planningSequence,
                ];
                \Pusherer::send_push(
                    "private-driver-{$vehicleId}",
                    'order-cancelled',
                    $data,
                    'transporter'
                );
                \Pusherer::send_push(
                    "private-picker-{$pickerDryId}",
                    'order-cancelled',
                    $data,
                    'picker'
                );
                \Pusherer::send_push(
                    "private-picker-{$pickerColdId}",
                    'order-cancelled',
                    $data,
                    'picker'
                );
            }
        }
    }

    /**
     * Guarda en la descripción del modelo VehicleMovement el registro de la cancelación
     * @param VehicleMovement $vehicleMovement
     * @param int | null $productReturnStorage
     * @return void
     */
    private function descriptionVehicleMovement(
        $vehicleMovement,
        $productReturnStorage
    ) {
        $description = 'Pedido actualizado a Cancelado ';
        if ($productReturnStorage) {
            $description .= 'con devolución';
        } else {
            $description .= 'sin devolución';
        }
        $description .= ' desde el help center.';
        $vehicleMovement->description .= $description;
        $vehicleMovement->status = 0;
        $vehicleMovement->save();
    }
}
