<?php

class ProviderOrderReception extends \Illuminate\Database\Eloquent\Model
{
    const Date_Change_Calculate_Amount = '2019-02-20';
    protected $table = 'provider_order_receptions';
    protected $hidden = ['created_at', 'updated_at'];
    protected $products = null;
    protected $impuestos = [];

    public function providerOrderReceptionDetail()
    {
        return $this->hasMany('ProviderOrderReceptionDetail', 'reception_id');
    }

    public function providerOrder()
    {
        return $this->belongsTo('ProviderOrder', 'provider_order_id');
    }

    /**
     * Relacion con el modelo Provider.
     * Nota: Esta relación solo se utiliza cuando la factura es de productos faltantes y contabilidad desea asignar el
     * proveedor real para que este viaje a SAP.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function provider()
    {
        return $this->belongsTo('Provider','real_provider_id');
    }

    public function admin()
    {
        return $this->belongsTo('Admin', 'admin_id');
    }

    public function providerOrderReceptionValidate()
    {
        return $this->hasMany('ProviderOrderReceptionValidate', 'reception_id');
    }

    /**
     * Funcion para obtener el subtotal del recibo de bodega
     * @return int
     */
    public function getSubTotal($excludeCreditNote = true)
    {
        $totalCost = 0;
        if (is_null($this->products)) {
            $products = $this->getProducts();
        } else {
            $products = $this->products;
        }

        $products = $products->filter(function ($product) {
            if (explode(" ", $this->date)[0] > self::Date_Change_Calculate_Amount) {
                return $product->type == 'Simple';
            } else {
                return $product->status == 'Recibido'
                    || $product->status == 'Parcialmente recibido'
                    && $product->type == 'Simple';
            }
        });

        $products = $products->each(function ($product) use (&$totalCost, &$excludeCreditNote) {
            if (explode(" ", $this->date)[0] > self::Date_Change_Calculate_Amount) {
                $totalCost += ($excludeCreditNote) ?
                    ($product->base_cost * $product->quantity_expected) :
                    ($product->base_cost * $product->quantity_credit_note);
            } else {
                $totalCost += ($excludeCreditNote) ?
                    ($product->base_cost * $product->quantity_received) :
                    ($product->base_cost * $product->quantity_credit_note);
            }
            return $totalCost;
        });

        if ($groupedProducts = $this->getProductGroupDetails()) {
            $groupedProducts = $groupedProducts->filter(function ($product) {
                if (explode(" ", $this->date)[0] > self::Date_Change_Calculate_Amount) {
                  return $product;
                } else {
                    return $product->reception_detail_product_group_status == 'Recibido'
                        || $product->reception_detail_product_group_status == 'Parcialmente recibido';
                }
            });

            if (!empty($groupedProducts)) {
                $groupedProducts->each(function ($product) use (&$totalCost, &$excludeCreditNote) {
                    if (explode(" ", $this->date)[0] > self::Date_Change_Calculate_Amount) {
                        $totalCost += ($excludeCreditNote) ?
                            ($product->base_cost * $product->reception_detail_product_group_quantity_expected) :
                            ($product->base_cost * $product->quantity_credit_note);
                    }else{
                        $totalCost += ($excludeCreditNote) ?
                            ($product->base_cost * $product->reception_detail_product_group_quantity_received) :
                            ($product->base_cost * $product->quantity_credit_note);
                    }
                    return $totalCost;
                });
            }
        }

        return $totalCost;
    }

    /**
     * Obtener el total a pagar del recibo de bodega
     * @return int|mixed
     */
    public function getTotal($excludeCreditNote = true)
    {
        return $this->getIva($excludeCreditNote)
            + $this->getConsumptionTax($excludeCreditNote)
            - (($excludeCreditNote) ? $this->rete_ica_amount : 0)
            - (($excludeCreditNote) ? $this->rete_fuente_amount : 0)
            + $this->getSubTotal($excludeCreditNote);
    }

    /**
     * Función que obtiene los productos del recibo de bodega
     */
    public function getProducts($search = '', $excludeIds = [], $excludeCreditNote = true)
    {
        $reception_products = ProviderOrderReceptionDetail::select(
            'provider_order_details.*',
            'provider_order_details.id AS provider_order_details_id',
            'provider_order_reception_details.*',
            'provider_order_reception_details.iva',
            'provider_order_reception_details.id AS provider_order_reception_details_id',
            'provider_order_receptions.provider_order_id',
            'provider_order_receptions.status AS reception_status',
            'provider_order_reception_details.accounting_status',
            'store_products.handle_expiration_date',
            'store_products.product_id',
            'store_products.storage',
            DB::raw("IF(DATE(provider_order_receptions.date) > '" . self::Date_Change_Calculate_Amount . "', (provider_order_reception_details.quantity_expected * provider_order_reception_details.base_cost) * (1 * (provider_order_reception_details.iva / 100)), (provider_order_reception_details.quantity_received * provider_order_reception_details.base_cost) * (1 * (provider_order_reception_details.iva / 100)))  AS iva_amount"),
            DB::raw("IF(DATE(provider_order_receptions.date) > '" . self::Date_Change_Calculate_Amount . "', (provider_order_reception_details.base_cost * provider_order_reception_details.quantity_expected), (provider_order_reception_details.base_cost * provider_order_reception_details.quantity_received)) AS sub_total_cost"),
            DB::raw("IF(DATE(provider_order_receptions.date) > '" . self::Date_Change_Calculate_Amount . "', IF(provider_order_reception_details.cost * provider_order_reception_details.quantity_expected > 0, (provider_order_reception_details.base_cost * provider_order_reception_details.quantity_expected) + ((provider_order_reception_details.quantity_expected * provider_order_reception_details.base_cost) * (1 * (provider_order_reception_details.iva / 100))) + provider_order_reception_details.consumption_tax, 0), IF(provider_order_reception_details.cost * provider_order_reception_details.quantity_received > 0, (provider_order_reception_details.base_cost * provider_order_reception_details.quantity_received) + ((provider_order_reception_details.quantity_received * provider_order_reception_details.base_cost) * (1 * (provider_order_reception_details.iva / 100))) + provider_order_reception_details.consumption_tax, 0))  AS total_cost"),
            'products.quantity',
            'products.unit',
            'products.accounting_line',
            'products.accounting_group',
            'products.accounting_code',
            DB::raw('IF (provider_order_reception_details.type IS NULL, "Simple", provider_order_reception_details.type) AS type')
        )
            ->leftJoin('provider_order_receptions', 'provider_order_receptions.id', '=', 'provider_order_reception_details.reception_id')
            ->leftJoin('provider_order_details', function ($join) {
                $join->on('provider_order_details.provider_order_id', '=', 'provider_order_receptions.provider_order_id');
                $join->on('provider_order_details.store_product_id', '=', 'provider_order_reception_details.store_product_id');
            })
            ->leftJoin('store_products', 'store_products.id', '=', 'provider_order_details.store_product_id')
            ->join('products', 'store_products.product_id', '=', 'products.id')
            ->where('provider_order_reception_details.reception_id', $this->id);

        if (!empty($search)) {
            $reception_products = $reception_products->where(function ($query) use ($search) {
                $query->where('provider_order_details.reference', 'LIKE', '%' . $search . '%');
                $query->orWhere('provider_order_details.plu', 'LIKE', '%' . $search . '%');
                $query->orWhere('provider_order_details.product_name', 'LIKE', '%' . $search . '%');
            });
        }
        if ($excludeIds) {
            $reception_products = $reception_products->whereNotIn('provider_order_reception_details.id', $excludeIds);
        }
        if (!$excludeCreditNote) {
            $reception_products = $reception_products->where(function ($query) {
                $query->whereNotNull('provider_order_reception_details.reason_credit_note')
                    ->whereNotNull('provider_order_reception_details.quantity_credit_note')
                    ->orWhere('provider_order_reception_details.type', 'Proveedor');
            });
        }
        $reception_products = $reception_products->groupBy('provider_order_reception_details.id')
            ->get();

        $reception_products = $reception_products->each(function (&$reception_product) use (&$excludeIds, &$excludeCreditNote) {
            if ($reception_product->type == 'Proveedor') {
                $provider_order_detail_product_groups = ProviderOrderDetailProductGroup::where('provider_order_detail_id', $reception_product->provider_order_details_id)
                    ->join('store_products', 'provider_order_detail_product_group.store_product_id', '=', 'store_products.id')
                    ->join('products', 'store_products.product_id', '=', 'products.id')
                    ->select(
                        'provider_order_detail_product_group.*',
                        'store_products.storage',
                        'store_products.handle_expiration_date',
                        'products.reference',
                        'products.id AS product_id',
                        'products.quantity',
                        'products.unit',
                        'products.accounting_line',
                        'products.accounting_group',
                        'products.accounting_code'
                    )->get();

                $provider_order_reception_detail_product_groups = ProviderOrderReceptionDetailProductGroup::where('reception_detail_id', $reception_product->provider_order_reception_details_id);
                if ($excludeIds) {
                    $provider_order_reception_detail_product_groups = $provider_order_reception_detail_product_groups->whereNotIn('id', $excludeIds);
                }
                if (!$excludeCreditNote) {
                    $provider_order_reception_detail_product_groups = $provider_order_reception_detail_product_groups->whereNotNull('reason_credit_note')
                        ->whereNotNull('quantity_credit_note');
                }
                $provider_order_reception_detail_product_groups = $provider_order_reception_detail_product_groups->get();
                foreach ($provider_order_detail_product_groups as &$provider_order_detail_product_group) {
                    foreach ($provider_order_reception_detail_product_groups as $provider_order_reception_detail_product_group) {
                        if ($provider_order_detail_product_group->store_product_id == $provider_order_reception_detail_product_group->store_product_id) {
                            $provider_order_detail_product_group->reception_detail_product_group_id = $provider_order_reception_detail_product_group->id;
                            $provider_order_detail_product_group->reception_detail_product_group_status = $provider_order_reception_detail_product_group->status;
                            $provider_order_detail_product_group->reception_detail_product_group_accounting_status = $provider_order_reception_detail_product_group->accounting_status;
                            $provider_order_detail_product_group->reception_detail_product_group_quantity_received = $provider_order_reception_detail_product_group->quantity_received;
                            $provider_order_detail_product_group->reception_detail_product_group_quantity_expected = $provider_order_reception_detail_product_group->quantity_expected;
                            $provider_order_detail_product_group->reception_detail_product_group_expiration_date = $provider_order_reception_detail_product_group->expiration_date;
                            $provider_order_detail_product_group->cost = $provider_order_reception_detail_product_group->cost;
                            $provider_order_detail_product_group->consumption_tax = $provider_order_reception_detail_product_group->consumption_tax;
                            $provider_order_detail_product_group->base_cost = $provider_order_reception_detail_product_group->base_cost;
                            $provider_order_detail_product_group->quantity_credit_note = $provider_order_reception_detail_product_group->quantity_credit_note;
                            $provider_order_detail_product_group->reason_credit_note = $provider_order_reception_detail_product_group->reason_credit_note;
                            $provider_order_detail_product_group->consumption_tax_credit_note = $provider_order_reception_detail_product_group->consumption_tax_credit_note;
                            if (explode(" ", $this->date)[0] > self::Date_Change_Calculate_Amount) {
                                $provider_order_detail_product_group->iva_amount = ($provider_order_reception_detail_product_group->quantity_expected * $provider_order_reception_detail_product_group->base_cost) * (1 * ($provider_order_reception_detail_product_group->iva / 100));
                                $provider_order_detail_product_group->sub_total_cost = ($provider_order_reception_detail_product_group->quantity_expected * $provider_order_reception_detail_product_group->base_cost);
                                $provider_order_detail_product_group->total_cost = $provider_order_detail_product_group->sub_total_cost > 0 ? ($provider_order_reception_detail_product_group->quantity_expected * $provider_order_reception_detail_product_group->base_cost) + (($provider_order_reception_detail_product_group->quantity_expected * $provider_order_reception_detail_product_group->base_cost) * (1 * ($provider_order_reception_detail_product_group->iva / 100)) + $provider_order_reception_detail_product_group->consumption_tax) : 0;
                            }else{
                                $provider_order_detail_product_group->iva_amount = ($provider_order_reception_detail_product_group->quantity_received * $provider_order_reception_detail_product_group->base_cost) * (1 * ($provider_order_reception_detail_product_group->iva / 100));
                                $provider_order_detail_product_group->sub_total_cost = ($provider_order_reception_detail_product_group->quantity_received * $provider_order_reception_detail_product_group->base_cost);
                                $provider_order_detail_product_group->total_cost = $provider_order_detail_product_group->sub_total_cost > 0 ? ($provider_order_reception_detail_product_group->quantity_received * $provider_order_reception_detail_product_group->base_cost) + (($provider_order_reception_detail_product_group->quantity_received * $provider_order_reception_detail_product_group->base_cost) * (1 * ($provider_order_reception_detail_product_group->iva / 100)) + $provider_order_reception_detail_product_group->consumption_tax) : 0;
                            }
                            $provider_order_detail_product_group->iva = $provider_order_reception_detail_product_group->iva;
                        }
                    }
                }
                $reception_product->grouped_products = $provider_order_detail_product_groups;
            }
        });

        $this->products = $reception_products;
        return $this->products;
    }

    /**
     * Función que obtiene el IVA detallado del recibo de bodega
     * @return array
     */
    public function getIvaDetails()
    {
        $iva_details = [];
        if (is_null($this->products)) {
            $products = $this->getProducts();
        } else {
            $products = $this->products;
        }

        $iva_types = $products->filter(function ($product) {
            if (explode(" ", $this->date)[0] > self::Date_Change_Calculate_Amount) {
                return $product->type == 'Simple';
            } else {
                return $product->status == 'Recibido'
                    || $product->status == 'Parcialmente recibido'
                    && $product->type == 'Simple';
            }
        })->groupBy('iva');

        foreach ($iva_types as $key => $iva_type) {
            if ($iva_type[0]->iva != 0) {
                $iva_details[$key]['total'] = 0;
                $iva_details[$key]['iva'] = $iva_type[0]->iva;
                if ($iva_type[0]->iva == 19) {
                    $iva_details[$key]['iva_account'] = '2408100100';
                } elseif ($iva_type[0]->iva == 5) {
                    $iva_details[$key]['iva_account'] = '2408100200';
                }
                foreach ($iva_type as $key2 => $iva) {
                    $iva_details[$key]['total'] += $iva->iva_amount;
                }
            }
        }

        // Cálculo de iva para productos tipo proveedor
        if ($grouped_products = $this->getProductGroupDetails()) {
            $iva_types = $grouped_products->filter(function ($provider_product) {
                if (explode(" ", $this->date)[0] > self::Date_Change_Calculate_Amount) {
                    return $provider_product;
                } else {
                    return $provider_product->reception_detail_product_group_status == 'Recibido'
                        || $provider_product->reception_detail_product_group_status == 'Parcialmente recibido';
                }
            })->groupBy('iva');

            if (!empty($iva_types)) {
                foreach ($iva_types as $key => $iva_type) {
                    foreach ($iva_type as $key2 => $iva_item) {
                        if ($iva_item->iva != 0) {
                            if (!array_key_exists($key, $iva_details)) {
                                $iva_details[$key]['iva'] = $iva_item->iva;
                                $iva_details[$key]['total'] = 0;
                            }
                            if ($iva_item->iva == 19) {
                                $iva_details[$key]['iva_account'] = '2408100100';
                            } elseif ($iva_item->iva == 5) {
                                $iva_details[$key]['iva_account'] = '2408100200';
                            }
                            $iva_details[$key]['total'] += $iva_item->iva_amount;
                        }
                    }
                }
            }
        }

        return $iva_details;
    }

    /**
     * Funcion para obtener el total del iva
     * @return int
     */
    public function getIva($excludeCreditNote = true)
    {
        if (!$excludeCreditNote) {
            if (is_null($this->products)) {
                $products = $this->getProducts();
            } else {
                $products = $this->products;
            }

            $products = $products->filter(function ($product) {
                if (explode(" ", $this->date)[0] > self::Date_Change_Calculate_Amount) {
                    return  $product;
                } else {
                    return $product->status == 'Recibido'
                        || $product->status == 'Parcialmente recibido';
                }
            });
            $ivaTotal = 0;
            foreach ($products as $product) {
                if ($product->type == 'Simple') {
                    $ivaTotal += $product->base_cost * $product->quantity_credit_note * ($product->iva / 100);
                } else {
                    foreach ($product->grouped_products as $product_group) {
                        $ivaTotal += $product_group->base_cost * $product_group->quantity_credit_note * ($product_group->iva / 100);
                    }
                }
            }
            return $ivaTotal;

        } else {
            $ivaDetails = $this->getIvaDetails();
            $ivaTotal = 0;
            foreach ($ivaDetails as $ivaDetail) {
                $ivaTotal += $ivaDetail['total'];
            }
            return $ivaTotal;
        }
    }

    /**
     * Funcion para obtener el total de impuesto al consumo
     */
    public function getConsumptionTax($excludeCreditNote = true)
    {
        if (is_null($this->products)) {
            $products = $this->getProducts();
        } else {
            $products = $this->products;
        }
        $consumptionTax = 0;
        if ($excludeCreditNote) {
            $consumptionTax += $products->filter(function ($product) {
                if (explode(" ", $this->date)[0] > self::Date_Change_Calculate_Amount) {
                    return $product;
                } else {
                    return $product->status == 'Recibido' || $product->status == 'Parcialmente recibido';
                }
            })->sum('consumption_tax');
        } else {
            $consumptionTax += $products->filter(function ($product) {
                if (explode(" ", $this->date)[0] > self::Date_Change_Calculate_Amount) {
                    return $product;
                } else {
                    return $product->status == 'Recibido' || $product->status == 'Parcialmente recibido';
                }
            })->sum('consumption_tax_credit_note');
        }

        // Cálculo del impuesto al consumo de productos tipo proveedor
        if ($groupedProducts = $this->getProductGroupDetails()) {
            if ($excludeCreditNote) {
                $consumptionTax += $groupedProducts->filter(function ($product) {
                    if (explode(" ", $this->date)[0] > self::Date_Change_Calculate_Amount) {
                        return $product;
                    } else {
                        return $product->reception_detail_product_group_status == 'Recibido'
                            || $product->reception_detail_product_group_status == 'Parcialmente recibido';
                    }
                })->sum('consumption_tax');
            } else {
                $consumptionTax += $groupedProducts->filter(function ($product) {
                    if (explode(" ", $this->date)[0] > self::Date_Change_Calculate_Amount) {
                        return $product;
                    } else {
                        return $product->reception_detail_product_group_status == 'Recibido'
                            || $product->reception_detail_product_group_status == 'Parcialmente recibido';
                    }
                })->sum('consumption_tax_credit_note');
            }
        }

        return $consumptionTax;
    }

    /**
     * Función para generar consecutivo para planos E
     * @return bool
     */
    public function generateConsecutiveE()
    {
        if (empty($this->movement_consecutive_e)) {
            $providerOrder = ProviderOrder::with('warehouse')->find($this->provider_order_id);
            $consecutive = DB::table('consecutives')
                ->where('type', 'provider_order_reception_e')
                ->where('city_id', $providerOrder->warehouse->city_id)
                ->first();

            $this->movement_consecutive_e = $consecutive->consecutive;
            $this->save();
            $consecutive->consecutive++;
            $consecutive = DB::table('consecutives')
                ->where('type', 'provider_order_reception_e')
                ->where('city_id', $providerOrder->city_id)
                ->update(['consecutive' => $consecutive->consecutive]);

            return true;
        }
        return false;
    }

    /**
     * Funcion para generar consecutivo para planos P
     */
    public function generateConsecutiveP()
    {
        if (empty($this->movement_consecutive_p)) {
            $p_code = 2;
            $consecutive = DB::table('consecutives')->where('type', 'provider_order_reception_p_2')->first();
            $this->movement_consecutive_p = $consecutive->consecutive;
            $this->p_type = 'P' . $p_code;
            $this->save();
            $consecutive->consecutive++;
            $consecutive = DB::table('consecutives')->where('type', 'provider_order_reception_p_' . $p_code)->update(['consecutive' => $consecutive->consecutive]);
            return true;
        }
        return false;
    }

    /**
     * Función para obtener los productos tipo proveedor
     */
    public function getProductGroups($provider_order_reception_detail_id = false)
    {
        if ($provider_order_reception_detail_id) {
            if (is_null($this->products)) {
                $products = $this->getProducts();
            } else {
                $products = $this->products;
            }

            return $products->filter(function ($product) use ($provider_order_reception_detail_id) {
                return $product->type == 'Proveedor' && $product->id == $provider_order_reception_detail_id;
            });
        } else {
            if (is_null($this->products)) {
                $products = $this->getProducts();
            } else {
                $products = $this->products;
            }

            return $products->filter(function ($product) {
                return $product->type == 'Proveedor';
            });
        }
        return false;
    }

    /**
     * Función para obtener el detalle de todos los productos tipo proveedor
     * @return [type] [description]
     */
    public function getProductGroupDetails()
    {
        $grouped_products = $this->getProductGroups();
        if (!empty($grouped_products)) {
            $provider_products = [];
            $grouped_products->each(function ($product) use (&$provider_products) {
                $product->grouped_products->each(function ($item) use (&$provider_products) {
                    $provider_products[] = $item;
                    return;
                });
            });
            // se crea un colección del arreglo para poder usar funcionalidades del obejto collection
            $collection = Illuminate\Database\Eloquent\Collection::make($provider_products);
            return $collection;
        }
        return false;
    }

    /**
     * Función para calcular rete ica
     */
    public function updateIcaAmount()
    {
        if (!empty($this->rete_ica_account) && !empty($this->rete_ica_percentage)) {
            $this->rete_ica_amount = $this->getSubTotal() * $this->rete_ica_percentage;
            $this->save();
            return $this->rete_ica_amount;
        }
        return false;
    }

    /**
     * Función para calcular rete fuente
     */
    public function updateFuenteAmount()
    {
        if (!empty($this->rete_fuente_account) && !empty($this->rete_fuente_percentage)) {
            $this->rete_fuente_amount = ($this->getSubTotal() * $this->rete_fuente_percentage) / 100;
            $this->save();
            return $this->rete_fuente_amount;
        }
        return false;
    }

    /**
     * Funciona que actualiza los impuestos
     */
    public function updateTaxes()
    {
        $this->updateIcaAmount();
        $this->updateFuenteAmount();
    }

    /**
     * Actualiza el estado de los recibos de bodega
     * @param $status
     * @param null $parameters
     * @return mixed
     */
    public function updateStatus($status, $parameters = null)
    {
        $provider_type = true;
        if (isset($this->providerOrder->provider->provider_type)
            && $this->providerOrder->provider->provider_type == 'Marketplace') {
            $provider_type = false;
        }
        $response['status'] = false;
        $response['message'] = 'No se ha ingresado ningun estado para actualizar.';
        switch ($status) {
            case 'Revisado':
                if ($this->status == 'Almacenado' || $this->status == 'Contabilizado') {
                    $reception_products = $this->providerOrderReceptionDetail;
                    $num_products = $reception_products->count();
                    $num_products_checked = $reception_products->filter(function ($product) {
                        return $product->accounting_status == 'Revisado';
                    })->count();

                    if ($num_products != $num_products_checked) {
                        $response['status'] = false;
                        $response['message'] = 'No se puede actualizar el recibo a : ' . $this->status . '. Hay productos en el recibo que están sin revisión.';
                    } else {
                        $this->status = $status;
                        $this->save();
                        $response['status'] = true;
                        $response['message'] = 'Recibo actualizado a : ' . $this->status . '.';
                    }
                } else {
                    $response['status'] = false;
                    $response['message'] = 'No se puede actualizar el recibo a : ' . $this->status . ' porque el estado actual del recibo no es "Almacenado".';
                }
                break;

            case 'Recibido':
                if ($this->status == 'En proceso' || $this->status == 'Validación') {
                    $validate_count = $this->providerOrderReceptionValidate->count();
                    $validate_resolve = $this->providerOrderReceptionValidate->filter(function ($q) {
                        return !is_null($q->tipification);
                    })->count();

                    //Validar si tienes incidencias por resolver
                    if ($validate_count != $validate_resolve) {
                        $response['status'] = true;
                        $response['message'] = 'Hay incidencias con productos que deben ser validados primero.';
                    } else {
                        //validar estado de productos del recibo
                        $reception_products = $validate_resolve = $this->providerOrderReceptionDetail
                            ->filter(function ($q) {
                                return $q->status == 'Pendiente';
                            })
                            ->count();
                        if ($reception_products) {
                            $response['status'] = false;
                            $response['message'] = 'Hay productos con estado pendiente en el recibo de bodega.';
                        } else {
                            $this->status = $status;
                            if ($provider_type) {
                                $this->receiveProducts();
                            }
                            $this->received_date = date('Y-m-d H:i:s');
                            ProviderOrder::updateStatus($this->providerOrder->id);
                            $this->save();
                            $response['status'] = true;
                            $response['message'] = 'Se ha actualizado el recibo a : ' . $this->status . '.';
                        }
                    }
                } else {
                    $response['status'] = false;
                    $response['message'] = 'No se puede actualizar el recibo a : ' . $this->status . ' porque el estado actual del recibo no es "En proceso" o "Validación".';
                }
                break;

            case 'Recibido con factura':
                if ($this->status == 'En proceso' || $this->status == 'Iniciado') {
                    $this->status = $status;
                    $this->save();
                    $response['status'] = true;
                    $response['message'] = 'Se ha actualizado el recibo a : ' . $this->status . '.';
                } else {
                    $response['status'] = false;
                    $response['message'] = 'No se ha actualizado el recibo a : ' . $this->status . '.';
                }
                break;

            case 'En proceso':
                $this->status = $status;
                $this->save();
                $response['status'] = true;
                $response['message'] = 'Se ha actualizado el recibo a : ' . $this->status . '.';
                break;

            case 'Contabilizado':
                /*if (empty($this->invoice_url)) {
                    $response['status'] = false;
                    $response['message'] = 'No se puede actualizar el recibo a Contabilizado, no se ha cargado la factura del proveedor.';
                    break;
                }*/
                //Validar estado previo del recibo
                if ($this->status != 'Revisado') {
                    $response['status'] = false;
                    $response['message'] = 'El estado actual del recibo de bodega no es valido.';
                } else {
                    // rete ica
                    if (isset($parameters['wtcode_ica'])) {
                        $this->wtcode_ica = $parameters['wtcode_ica'];
                    }
                    if (isset($parameters['rete_ica_account'])) {
                        $this->rete_ica_account = $parameters['rete_ica_account'];
                    }
                    if (isset($parameters['rete_ica_percentage'])) {
                        $this->rete_ica_percentage = $parameters['rete_ica_percentage'];
                    }
                    if (isset($parameters['rete_ica_amount'])) {
                        $this->rete_ica_amount = $parameters['rete_ica_amount'];
                    }
                    if (isset($parameters['rete_ica_options']) && $parameters['rete_ica_options'] == 'No Aplica') {
                        $this->wtcode_ica = null;
                        $this->rete_ica_account = null;
                        $this->rete_ica_amount = null;
                        $this->rete_ica_amount = null;
                    }

                    // rete fuente
                    if (isset($parameters['wtcode_fuente'])) {
                        $this->wtcode_fuente = $parameters['wtcode_fuente'];
                    }
                    if (isset($parameters['rete_fuente_account'])) {
                        $this->rete_fuente_account = $parameters['rete_fuente_account'];
                    }
                    if (isset($parameters['rete_fuente_percentage'])) {
                        $this->rete_fuente_percentage = $parameters['rete_fuente_percentage'];
                    }
                    if (isset($parameters['rete_fuente_amount'])) {
                        $this->rete_fuente_amount = $parameters['rete_fuente_amount'];
                    }

                    if (isset($parameters['rete_fuente_options']) && $parameters['rete_fuente_options'] == 'No Aplica') {
                        $this->wtcode_fuente = null;
                        $this->rete_fuente_account = null;
                        $this->rete_fuente_percentage = null;
                        $this->rete_fuente_amount = 0;
                    }

                    $reception_products = $this->providerOrderReceptionDetail;
                    $num_products = $reception_products->count();
                    $num_products_checked = $reception_products->filter(function ($product) {
                        return $product->accounting_status == 'Contabilizado';
                    })->count();

                    if ($num_products != $num_products_checked) {
                        $response['status'] = false;
                        $response['message'] = 'Hay productos no contabilizados en el recibo de bodega.';
                    } else {
                        $response['status'] = true;
                        $response['message'] = 'El estado del recibo de bodega se actualizó a : ' . $status . '.';
                        $this->status = $status;
                        $this->save();
                        $this->generateConsecutiveP();
                    }
                }
                break;

            case 'Almacenado':
                if ($this->status == 'Recibido' && $this->providerOrder->provider->id == 17) {
                    try {
                        DB::beginTransaction();
                        $this->storeProductsFromMerqueo();
                        DB::commit();
                    } catch (\Illuminate\Database\QueryException $e) {
                        DB::rollBack();
                        if ($e->getCode() == '40001') {
                            ErrorLog::add($e, 513);
                            $response['status'] = false;
                            $response['message'] =  'Otro proceso se está ejecutando sobre estos productos, 
                            espera un momento para volver a intentarlo.';
                            return $response;
                        }
                        ErrorLog::add($e);
                        $response['status'] = false;
                        $response['message'] = $e->getMessage();
                        return $response;
                    } catch (Exception $e) {
                        DB::rollBack();
                        $response['status'] = false;
                        $response['message'] = $e->getMessage();
                        return $response;
                    }

                    $this->storage_date = date('Y-m-d H:i:s');
                    $this->status = $status;
                    $this->save();
                    $response['status'] = true;
                    $response['message'] = 'El estado del recibo de bodega se actualizó a : ' . $status . '.';
                } else {
                    $response['status'] = false;
                    $response['message'] = 'No se puede actualizar el recibo a : ' . $this->status . ' porque el estado actual del recibo no es "Recibido" o porque el proveedor no es Merqueo.';
                }
                break;

            default:
                $response['status'] = false;
                $response['message'] = 'No se realizó ninguna acción sobre el recibo.';
                break;
        }
        return $response;
    }

    /**
     * Función para recibir los productos de un recibo, se debe usar cuando
     * el recibo pase a estado recibido
     */
    private function receiveProducts()
    {
        $warehouseId = $this->providerOrder->warehouse_id;
        $this->providerOrderReceptionDetail->each(function ($detail) use ($warehouseId) {
            if ($detail->quantity_received > 0 && $detail->type == 'Simple') {
                $det = $detail->storeProduct->storeProductWarehouses()
                    ->where('warehouse_id', $warehouseId)
                    ->lockForUpdate()
                    ->first();

                if (empty($det->first_reception_date)) {
                    $det->first_reception_date = Carbon\Carbon::now();
                }
                $det->reception_stock += $detail->quantity_received;
                $det->save();

                $activable = $detail->storeProduct->getCurrentStock($warehouseId);
                if ($activable) {
                    $det->status = 1;
                    $det->save();
                }
            } elseif ($detail->type == 'Proveedor') {
                $detail->providerOrderReceptionDetailProductGroup->each(function ($groupDetail) use ($warehouseId) {
                    if ($groupDetail->quantity_received > 0) {
                        $det = $groupDetail->storeProduct
                            ->storeProductWarehouses()
                            ->where('warehouse_id', $warehouseId)
                            ->lockForUpdate()
                            ->first();

                        if (empty($det->first_reception_date)) {
                            $det->first_reception_date = Carbon\Carbon::now();
                        }
                        $det->reception_stock += $groupDetail->quantity_received;
                        $det->save();

                        $activable = $groupDetail->storeProduct->getCurrentStock($warehouseId);
                        if ($activable) {
                            $det->status = 1;
                            $det->save();
                        }
                    }
                });
            }
        });
    }

    /**
     * Función para regresar los productos recibidos.
     */
    public function rollbackReceivedProducts()
    {
        $warehouseId = $this->providerOrder->warehouse_id;
        $this->providerOrderReceptionDetail->each(function ($detail) use ($warehouseId) {
            if ($detail->quantity_received > 0 && $detail->type == 'Simple') {
                $det = $detail->storeProduct
                    ->storeProductWarehouses()
                    ->where('warehouse_id', $warehouseId)
                    ->lockForUpdate()
                    ->first();

                $det->reception_stock -= $detail->quantity_received;
                $det->save();
            } elseif ($detail->type == 'Proveedor') {
                $detail->providerOrderReceptionDetailProductGroup->each(function ($groupDetail) use ($warehouseId) {
                    if ($groupDetail->quantity_received > 0) {
                        $det = $groupDetail->storeProduct
                            ->storeProductWarehouses()
                            ->where('warehouse_id', $warehouseId)
                            ->lockForUpdate()
                            ->first();

                        $det->reception_stock -= $groupDetail->quantity_received;
                        $det->save();
                    }
                });
            }
        });
    }

    /**
     * Función para agregar cantidades al sotck de productos
     */
    public function storeProductsFromMerqueo()
    {
        $warehouseId = $this->providerOrder->warehouse_id;
        $this->providerOrderReceptionDetail->each(function ($detail) use ($warehouseId) {
            if ($detail->quantity_received > 0 && $detail->type == 'Simple') {
                $det = $detail->storeProduct
                    ->storeProductWarehouses()
                    ->where('warehouse_id', $warehouseId)
                    ->lockForUpdate()
                    ->first();

                $det->reception_stock -= $detail->quantity_received;
                $det->picking_stock += $detail->quantity_received;
                $det->save();
            } elseif ($detail->type == 'Proveedor') {
                $detail->providerOrderReceptionDetailProductGroup
                    ->each(function ($groupDetail) use ($warehouseId, $detail) {
                        if ($groupDetail->quantity_received > 0) {
                            $det = $groupDetail->storeProduct
                                ->storeProductWarehouses()
                                ->where('warehouse_id', $warehouseId)
                                ->lockForUpdate()
                                ->first();

                            $det->reception_stock -= $detail->quantity_received;
                            $det->picking_stock += $detail->quantity_received;
                            $det->save();
                        }
                    });
            }
        });
    }

    /**
     * Cambia un recibo de Contabilizado a Revisado.
     * @return mixed
     */
    public function rollbackToReviewStatus()
    {
        $response['status'] = false;
        $response['message'] = 'El Recibo no se actualizado a : ' . $this->status . '.';

        if ($this->status == 'Contabilizado') {
            $this->status = 'Revisado';
            $this->save();
            $response['status'] = true;
            $response['message'] = "Recibo actualizado a : {$this->status}.";
        }

        return $response;
    }
}
