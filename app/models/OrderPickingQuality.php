<?php
class OrderPickingQuality extends \Illuminate\Database\Eloquent\Model
{
	protected $table = 'order_picking_quality';

	/**
	 * Obtener el listado de pedidos que se encuentran en calidad de alistamiento
	 * @param int $cityId identificador de la ciudad del pedido
	 * @param int $warehouseId identificador de la bodega del pedido
	 * @param string $search id del pedido si se tiene o vacío
         * @return array pedidos
	*/
	public function getOrders($cityId, $warehouseId, $search){
		return Order::where('payment_method', '<>', 'Tarjeta de crédito')
                            ->where('orders.id', 'LIKE', '%'.$search.'%')
                            ->where('cities.id', $cityId)
                            ->where('order_groups.warehouse_id', $warehouseId)
                            ->leftJoin('pickers AS pickers_dry', 'orders.picker_dry_id', '=', 'pickers_dry.id')
                            ->leftJoin('pickers AS pickers_cold', 'orders.picker_cold_id', '=', 'pickers_cold.id')
                            ->join('order_picking_quality', 'orders.id', '=', 'order_picking_quality.order_id')
                            ->join('admin', 'order_picking_quality.admin_id', '=', 'admin.id')
                            ->join('stores', 'stores.id', '=', 'orders.store_id')
                            ->join('cities', 'cities.id', '=', 'stores.city_id')
                            ->join('order_groups', 'order_groups.id', '=', 'orders.group_id')
                            ->select('orders.id',
                                    'orders.status',
                                    'order_picking_quality.status_dry AS status_validation_dry',
                                    'order_picking_quality.status_cold AS status_validation_cold',
                                    'order_picking_quality.validation_date_start_dry',
                                    'order_picking_quality.validation_date_end_dry',
                                    'order_picking_quality.validation_date_start_cold',
                                    'order_picking_quality.validation_date_end_cold',
                                    'fullname AS admin_validator',
                                    DB::raw("CONCAT(pickers_dry.first_name, ' ', pickers_dry.last_name) AS picker_dry_name"),
                                    DB::raw("CONCAT(pickers_cold.first_name, ' ', pickers_cold.last_name) AS picker_cold_name"),
                                    DB::raw("(SELECT SUM(quantity) FROM order_products WHERE order_id = orders.id AND type = 'product') AS quantity"),
                                    DB::raw("(SELECT SUM(quantity) FROM order_product_group WHERE order_id = orders.id) AS quantity_group")
                                    )
                            ->groupBy('orders.id')
                            ->orderBy('status_validation_dry', 'DESC')
                            ->limit(80)->get();
        }
        
        /**
         * Obtener detalle de un pedido para calidad
         * @param int $id identificador del pedido
         * @return order 
         */
	public function getOrder($id){
		return Order::where('payment_method', '<>', 'Tarjeta de crédito')
                        ->where('orders.id', $id)
                        ->leftJoin('routes', 'orders.route_id', '=', 'routes.id')
                        ->leftJoin('picking_group', 'routes.picking_group_id', '=', 'picking_group.id')
                        ->leftJoin('pickers AS pickers_dry', 'orders.picker_dry_id', '=', 'pickers_dry.id')
                        ->leftJoin('pickers AS pickers_cold', 'orders.picker_cold_id', '=', 'pickers_cold.id')
                        ->leftJoin('order_picking_quality', 'order_picking_quality.order_id', '=', 'orders.id')
                        ->leftJoin('admin', 'order_picking_quality.admin_id', '=', 'admin.id')
                        ->join('order_groups', 'orders.group_id', '=', 'order_groups.id')
                        ->join('cities AS cities_user','cities_user.id','=','order_groups.user_city_id')
                        ->join('stores','stores.id','=','orders.store_id')
                        ->join('cities AS cities_store','cities_store.id','=','stores.city_id')
                        ->select('orders.id',
                                'orders.status',
                                'order_picking_quality.status_dry AS status_validation_dry',
                                'order_picking_quality.status_cold AS status_validation_cold',
                                DB::raw("CONCAT(pickers_dry.first_name, ' ', pickers_dry.last_name) AS picker_dry_name"),
                                DB::raw("CONCAT(pickers_cold.first_name, ' ', pickers_cold.last_name) AS picker_cold_name"),
                                DB::raw("(SELECT SUM(quantity) FROM order_products WHERE order_id = orders.id AND type = 'product') AS quantity"),
                                DB::raw("(SELECT SUM(quantity) FROM order_product_group WHERE order_id = orders.id) AS quantity_group"),
                                'fullname AS admin_validator',
                                'picking_group.picking_group',
                                'orders.allocated_date',
                                'orders.picking_baskets',
                                'orders.picking_bags',
                                'orders.picking_date',
                                'orders.picking_cold_start_date',
                                'orders.picking_cold_end_date',
                                'orders.picking_dry_start_date',
                                'orders.picking_dry_end_date',
                                'routes.route',
                                'orders.planning_route',
                                'orders.planning_sequence',
                                'orders.planning_date',
                                'order_groups.source_os',
                                'order_groups.source',
                                'stores.name AS store_name',
                                'cities_store.city AS store_city',
                                'orders.date',
                                'orders.delivery_date AS customer_delivery_date',
                                'orders.delivery_time',
                                'orders.vehicle_id',
                                'orders.driver_id',
                                'orders.picking_revision_date',
                                'order_picking_quality.id AS order_picking_quality_id',
                                'check_pack_dry',
                                'check_pack_cold',
                                'check_gift_dry',
                                'check_gift_cold'
                                )->first();
        }
        
        /**
         * Obtener el estado de cada producto validado de una orden
         * @param int $orderId identificador del pedido
         * @return array productos 
         */
        public function getProductsValidated($orderId){
                return OrderPickingQuality::join('order_picking_quality_details', 'order_picking_quality_id', '=', 'order_picking_quality.id')
                                        ->join('store_products', 'store_products.id', '=', 'order_picking_quality_details.store_product_id')
                                        ->join('products', 'products.id', '=', 'store_products.product_id')
                                        ->where('order_picking_quality.order_id', $orderId)
                                        ->select('products.quantity AS product_quantity',
                                                'products.unit AS product_unit',
                                                'order_picking_quality_id',
                                                'order_picking_quality_details.id AS product_validation_id',
                                                'store_products.id AS store_product_id',
                                                DB::raw('COUNT(order_picking_quality_details.store_product_id) AS quantity'),
                                                'products.reference AS reference',
                                                'products.name AS product_name',
                                                'image_medium_url AS product_image_url');

        }
}
?>