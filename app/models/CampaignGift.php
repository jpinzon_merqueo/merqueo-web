<?php

use \Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

/**
 * Class CampaignGift
 */
class CampaignGift extends Model
{
    protected $table = 'campaign_gifts';

    // protected $hidden = ['created_at', 'updated_at'];

    /**
     * Obtenemos la Campaña activa en el mes para la warehouse dada.
     */
    public static function getCurrentCampaignGift($warehouse_id, $without_accumulated = false){

        $sign_accumulated = '>';
        if($without_accumulated){
            $sign_accumulated = '>=';
        }

        $date = date('Y-m-d');
        return self::select('campaign_gifts.*', DB::raw('CONCAT("'.Config::get('app.url').'", campaign_gifts.image) AS image' ))
                ->where('start_date', '<=', $date)
                ->where('end_date', '>=', $date )
                ->where('warehouse_id', $warehouse_id)
                ->where('status', 1)
                ->where('quantity', '>', 0)
                ->where('accumulated', $sign_accumulated , 0)
                ->first();
    }

    /**
     * Parsea las reglas de la campaña
     */
    public static function getRules($campaign){
        if($campaign){
            return json_decode($campaign->rules);
        }else{
            return false;
        }
    }

    /**
     * Validacion de reglas de la campaña actual y asignacion del regalo
     */
    public static function validateCampaignGift($warehouse_id, $order){
        $campaign = self::getCurrentCampaignGift($warehouse_id);
       
        $rules = self::getRules($campaign);

        $win_gift = false;

        if($rules){
            $validatedRules = self::validateRules($rules, $order);
            if($validatedRules['validated'] ){
                $win_gift = true;
                if($campaign->type_gift=='Products'){
                    // agregamos Productos de regalo a la Orden.
                    self::addProductsToOrder($campaign, $order, $validatedRules['store_product_id']);
                }else{
                    // TODO CUPON
                }

                // decrementamos el acumulado diario y el total disponible
                $campaign->decrement('accumulated');
                $campaign->decrement('quantity');

                // Crear registro del regalo dado
                $give = new CampaignGiftGive();
                $give->campaign_gift_id = $campaign->id;
                $give->order_id = $order->id;
                $give->give_date = date("Y-m-d");
                $give->save();
                
            }
        }
        unset($campaign["rules"]);
        return [
            'win_gift' => $win_gift,
            'campaign' => $campaign ? ($campaign->toArray()) : []
        ];

    }

    /**
     * Validar las reglas de la campaña
     */
    public static function validateRules($rules, $order){
        
        $validations = [];
        $return = [];
       
        // validacion por pasillos y cantidad definida
        if(isset($rules->shelves_ids_in_cart) && isset($rules->min_products_per_shelves_in_cart)){
            
            $validations['val_shelves_in_cart'] = false;
            $validations['val_min_products_per_shelves_in_cart'] = false;

            $products = OrderProduct::join('store_products', 'order_products.store_product_id', '=', 'store_products.id')
                ->where('order_products.order_id', $order->id)
                ->get();
            
            $shelves_ids = $rules->shelves_ids_in_cart;

            $total = 0;

            $first_store_product = null;
            foreach($products as $product){
                if( in_array($product->shelf_id, $shelves_ids) ){
                    $total += $product->quantity;

                    if($first_store_product==null){
                        $first_store_product = $product->store_product_id;
                    }
                }
            }

            if($total >= $rules->min_products_per_shelves_in_cart ){
                $validations['val_shelves_in_cart'] = true;
                $validations['val_min_products_per_shelves_in_cart'] = true;
                $return['store_product_id'] = $first_store_product;
            }

        }

        // Comprobamos que todas las validaciones sean TRUE
        $return['validated'] = false;
        foreach($validations as $validation){
            if(!$validation){
                $return['validated'] = false;
                break;
            }else{
                $return['validated'] = true;
            } 
        }

        return $return; 

    }

    /**
     * Agrega Producto de regalo a la orden, cuando las validaciones de las reglas se cumplan
     */
    public static function addProductsToOrder($campaign, $order, $store_product_id_parent){
        
        $store_product_ids = explode(',', $campaign->store_product_ids);
        
        foreach($store_product_ids as $store_product_id){
            $store_product = StoreProduct::where('product_id', $store_product_id)
                ->where('store_id', $order->store_id)->first();

            $store_product->campaign_gift_id = $campaign->id;
            $store_product->is_gift = 1;
            $store_product->fulfilment_status = 'Pending';
            $store_product->parent_id = $store_product_id_parent;

            OrderProduct::saveProduct(0, 1, $store_product, $order);
        }
        
    }


    /**
     * Obtiene cantidad de regalos dados en el dia.
     */
    public function giveGiftByDay($day = null ){
        
        if($day==null)
            $day = date('Y-m-d');

        return $this->join('campaign_gift_gives', 'campaign_gifts.id', '=', 'campaign_gift_gives.campaign_gift_id')
            ->where('give_date', $day)
            ->where('campaign_gifts.id', $this->id)
            ->count();
    }

    /**
     * Calculo del Acumulado de Gift que no se repartieron el dia anterior
     */
    public function calculateAccumulated(){

        $campaign = $this;
        $rules = $campaign::getRules($campaign);

        $campaign->increment('accumulated', $rules->daily_gift);
        $campaign->save();

        return $total_accumulate;
    }





}