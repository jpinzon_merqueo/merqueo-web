<?php

class InventoryCountingPosition extends Eloquent
{

    protected $table = 'inventory_counting_position';
    const CLOSE_COUNTING_ROLE_IDS = [1, 29, 35, 30];

    /**
     * Relacion muchos a muchos con el modelo InventoryCountingPositionDetail
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function inventoryCountingPositionDetails()
    {
        return $this->hasMany(\InventoryCountingPositionDetail::class);
    }

    /**
     * Inverso de la relacion muchos a muchos con el modelo Warehouse
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function warehouse()
    {
        return $this->belongsTo(\Warehouse::class);
    }

    /**
     * Inverso de la relacion muchos a muchos con el modelo Admin
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function admin()
    {
        return $this->belongsTo(\Admin::class);
    }

    /**
     * event handler
    */
    protected static function boot()
    {
        parent::boot();

        //Delete relationship after delete InventoryCountingPosition
        static::deleting(function ($inventoricountingposition) {
            $inventoricountingposition->inventoryCountingPositionDetails()->delete();
        });
    }

    /**
     * Valida si hay diferencias entre el conteo y el sistema.
     * @param $type
     * @return bool
     */
    public function isCountingDifferent($type)
    {
        $details = $this->inventoryCountingPositionDetails;

        try {
            foreach ($details as $detail) {
                if ($type === 'Alistamiento') {
                    $storeProductWarehouse = StoreProductWarehouse::where(
                        'warehouse_id',
                        $this->warehouse_id
                    )
                        ->where('store_product_id', $detail->store_product_id)
                        ->where('storage_position', $detail->position)
                        ->where('storage_height_position', $detail->position_height)
                        ->where('picking_stock', $detail->picking_original)
                        ->firstOrFail();
                    if ($storeProductWarehouse->picking_stock != $detail->picking_counted) {
                        return true;
                    }
                } elseif ($type === 'Almacenamiento') {
                    $warehouseStorage = WarehouseStorage::where(
                        'warehouse_id',
                        $this->warehouse_id
                    )
                        ->where('store_product_id', $detail->store_product_id)
                        ->where('quantity', $detail->quantity_original)
                        ->where('expiration_date', $detail->expiration_date)
                        ->where('position', $detail->position)
                        ->where('position_height', $detail->position_height)
                        ->firstOrFail();
                    if ($warehouseStorage->quantity != $detail->quantity_counted) {
                        return true;
                    }
                }
            }
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            throw $e;
        }

        return false;
    }

    /**
     * obtiene total en cantidad de productos del conteo
     * @return int
     */
    public function getTotalQuantityAttribute()
    {
        if ($this->action === 'Almacenamiento') {
            return $this->inventoryCountingPositionDetails->sum('quantity_original');
        }
        if ($this->action === 'Alistamiento') {
            return $this->inventoryCountingPositionDetails->sum('picking_original');
        }
    }

    /**
     * obtiene total en cantidad contada de productos del conteo
     * @return int
     */
    public function getTotalCountedAttribute()
    {
        if ($this->action === 'Almacenamiento') {
            return $this->inventoryCountingPositionDetails->sum('quantity_counted');
        }
        if ($this->action === 'Alistamiento') {
            return $this->inventoryCountingPositionDetails->sum('picking_counted');
        }
    }
}