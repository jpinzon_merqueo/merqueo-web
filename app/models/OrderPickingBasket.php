<?php

class OrderPickingBasket extends \Illuminate\Database\Eloquent\Model
{

    protected $table = 'order_picking_baskets';

    /***
     * Funcion para validar disponibilidad de canastilla
     * @param $baskets
     * @return mixed
     */
    public static function validateAvailability($baskets)
    {
        $basketAvailability = false;
        $baskets = (array)$baskets;
        $basketValidate = array_unique($baskets) == $baskets;

        if (!$basketValidate) {
            $basketAvailability = "contiene valores duplicados o";
        } else {
            foreach ($baskets as $basket) {
                $orderPickingBasket = OrderPickingBasket::where('code_basket', $basket)->orderby('order_id',
                    'Desc')->first();
                if ($orderPickingBasket) {
                    $order = Order::where('id', $orderPickingBasket->order_id)->where('status', '<>',
                        'Delivered')->first();
                    if ($order) {
                        $basketAvailability .= $basket . ' - ';
                    }
                }
            }
        }
        return $basketAvailability;
    }

    /**
     * Busca la informacion de la canastillas que estan de mas al momento de
     * escanearlas en el trasnsportador
     * @param $code_basket
     * @return array
     */
    public static function getInfoBasket($code_basket)
    {
        $value = OrderPickingBasket::where('order_picking_baskets.code_basket', $code_basket)
            ->join('orders', 'orders.id', '=', 'order_picking_baskets.order_id')
            ->join('routes', 'routes.id', '=', 'orders.route_id')
            ->where(DB::raw('DATE(order_picking_baskets.created_at)'), \Carbon\Carbon::now()->toDateString())
            ->select('order_picking_baskets.code_basket as code_basket',
                'order_picking_baskets.order_id as order_id',
                'routes.route as route')->first();

        return [
            "code_basket" => $code_basket,
            "order_id" => isset($value->order_id) ? $value->order_id : 'Sin Orden',
            "route" => isset($value->route) ? $value->route : 'Sin ruta',
            "value" => false
        ];
    }

    /**
     * Buscar el codigo de la canastilla en el array
     * @param $orders
     * @param $code
     * @return int|null
     */
    public static function searchBasketCode($orders, $code)
    {
        $index_return = null;
        foreach ($orders as $index => $data) {
            if ($data["code_basket"] == $code) {
                $index_return = $index;
                break;
            }
        }
        return $index_return;
    }
}