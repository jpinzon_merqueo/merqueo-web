<?php

class ShopperZone extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'shopper_zones';
    protected $hidden = array('created_at','updated_at');

}