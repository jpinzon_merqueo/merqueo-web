<?php

class OrderLog extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'order_logs';

    public $fillable = [
        'type',
        'admin_id',
        'order_id',
        'user_id',
    ];

}