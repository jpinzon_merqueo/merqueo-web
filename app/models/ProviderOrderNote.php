<?php

class ProviderOrderNote extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'provider_order_notes';
    protected $hidden = array('created_at','updated_at');

}