<?php

/**
 * @property int quantity_counted
 */
class InventoryCountingPositionDetail extends Eloquent
{

    protected $table = 'inventory_counting_position_details';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function inventoryCountingPosition()
    {
        return $this->belongsTo(\InventoryCountingPosition::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function storeProduct()
    {
        return $this->belongsTo(\StoreProduct::class);
    }
}