<?php

/**
 * Class Slot
 */
class Slot extends \Illuminate\Database\Eloquent\Model
{
    /**
     * @const Horas limite donde se puede realizar
     * un pedido en el mismo día.
     */
    const RANGE_HOURS_SAME_DAY = [18, 22];

    /**
     * @const Hora limite en la cual se puede
     */
    const LIMIT_ON_SAME_DAY = 14;

    /**
     * @var string
     */
    protected $table = 'slots';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function store()
    {
        return $this->belongsTo('Store');
    }
}
