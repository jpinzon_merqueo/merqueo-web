<?php

use Illuminate\Database\Eloquent\Model;

/**
 *Class Country
 *@property int id
 *@property string country
 *@property string country_code
 *@property bool use_postal_code
 *@property string domain
 *@property string description
 *@property string content_title
 *@property string content_subtitle
 *@property int referred_credit
 *@property int referred_by_credit
 *@property string admin_phone
 *@property string phone_code
 *@property string currency
 *@property string admin_email
 *@property int minimum_order_promo_days
 *@property int minimum_order_total_use_credit
 *@package App\Models
 */
class Country extends Model
{
    const COLOMBIA = 1;

    const COUNTRY_CODE_MEXICO = 'mx';
    const COLOMBIA_CODE = 'co';

    protected $table = 'countries';

    protected $casts = [
        'minimum_order_total_use_credit' => 'float',
        'referred_credit' => 'float',
        'referred_by_credit' => 'float',
    ];
    
    public function cities()
    {
        return $this->hasMany(City::class);
    }
}
