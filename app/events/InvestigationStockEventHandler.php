<?php
/**
* Clase para manejar eventos de recibos de bodega
*/
class InvestigationStockEventHandler
{
    /**
     * Guarda registro de la actualización del conteo en alistamiento.
     * @param InventoryCountingPositionDetail $inventoryCountingPositionDetail
     * @param StoreProductWarehouse $storeProductWarehouse
     * @param InventoryCountingPosition $inventoryCountingPosition
     * @param string $module nombre del módulo del cual se hizo el registro
     */
    public function onUpdatePickingQuantity(
        InventoryCountingPositionDetail $inventoryCountingPositionDetail,
        StoreProductWarehouse $storeProductWarehouse,
        InventoryCountingPosition $inventoryCountingPosition,
        $module
    ) {
        $difference = $inventoryCountingPositionDetail->picking_counted - $storeProductWarehouse->picking_stock;

        $investigationStock = new InvestigationStock;
        $investigationStock->inventory_counting_position_id = $inventoryCountingPosition->id;
        $investigationStock->store_product_id = $storeProductWarehouse->store_product_id;
        $investigationStock->warehouse_id = $inventoryCountingPosition->warehouse_id;
        $investigationStock->storage = $inventoryCountingPosition->action;
        $investigationStock->position = $inventoryCountingPositionDetail->position;
        $investigationStock->height_position = $inventoryCountingPositionDetail->position_height;
        $investigationStock->quantity = $difference;
        $investigationStock->module = $module;
        $investigationStock->save();
    }

    /**
     * Guarda registro de la actualización del conteo en almacenamiento.
     * @param InventoryCountingPositionDetail $inventoryCountingPositionDetail
     * @param WarehouseStorage $warehouseStorage
     * @param InventoryCountingPosition $inventoryCountingPosition
     * @param string $module nombre del módulo del cual se hizo el registro
     */
    public function onUpdateStorageQuantity(
        InventoryCountingPositionDetail $inventoryCountingPositionDetail,
        WarehouseStorage $warehouseStorage,
        InventoryCountingPosition $inventoryCountingPosition,
        $module
    ) {
        $difference = $inventoryCountingPositionDetail->quantity_counted - $warehouseStorage->quantity;

        $investigationStock = new InvestigationStock;
        $investigationStock->inventory_counting_position_id = $inventoryCountingPosition->id;
        $investigationStock->store_product_id = $warehouseStorage->store_product_id;
        $investigationStock->warehouse_id = $inventoryCountingPosition->warehouse_id;
        $investigationStock->storage = $inventoryCountingPosition->action;
        $investigationStock->position = $inventoryCountingPositionDetail->position;
        $investigationStock->height_position = $inventoryCountingPositionDetail->position_height;
        $investigationStock->quantity = $difference;
        $investigationStock->module = $module;
        $investigationStock->save();
    }

    /**
     * Suscribe los eventos.
     * @param $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'investigationStock.onUpdatePickingQuantity',
            'InvestigationStockEventHandler@onUpdatePickingQuantity'
        );
        $events->listen(
            'investigationStock.onUpdateStorageQuantity',
            'InvestigationStockEventHandler@onUpdateStorageQuantity'
        );
    }
}