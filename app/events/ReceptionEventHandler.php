<?php
/**
* Clase para manejar eventos de recibos de bodega
*/
class ReceptionEventHandler
{
	public function onUpdatedStatus($event)
	{
		$log = new ProviderOrderReceptionLog();
		$log->type = 'Estado de recibo de bodega # '.$event['reception_id'].' actualizado: '.$event['status'];
		$log->admin_id = $event['admin_id'];
		$log->reception_id = $event['reception_id'];
		$log->save();
	}

	public function onAddedProduct($event)
	{
		$log = new ProviderOrderReceptionLog();
		$log->type = 'Producto agregado: ID: '.$event['provider_order_detail']->store_product_id.' / Nombre: '.$event['provider_order_detail']->product_name.' / Tipo: '.$event['provider_order_detail']->type.' / Cantidad: '.$event['provider_order_detail']->quantity_order.' / Costo: $'.number_format($event['provider_order_detail']->cost, 0, ',', '.');
		$log->admin_id = $event['admin_id'];
		$log->reception_id = $event['reception_id'];
		$log->save();
	}

	public function onUpdatedProductAccountingStatus($event)
	{
		$log = new ProviderOrderReceptionLog();
		$log->type = 'Estado de contabilidad de producto actualizado ID: '.$event['reception_detail']->store_product_id.' actualizado a: '.$event['reception_detail']->accounting_status;
		$log->admin_id = $event['admin_id'];
		$log->reception_id = $event['reception_id'];
		$log->save();
	}

	public function onCreated($event)
	{
		$log = new ProviderOrderReceptionLog();
		$log->type = 'Recibo de bodega creado con ID: '.$event['reception_id'].' relacionado a la orden de compra con ID: '.$event['provider_order_id'];
		$log->admin_id = $event['admin_id'];
		$log->reception_id = $event['reception_id'];
		$log->save();
	}

    public function onUpdated($event)
    {
        $log = new ProviderOrderReceptionLog();
        $log->type = 'Recibo de bodega actualizado con ID: '.$event['reception_id'].' relacionado a la orden de compra con ID: '.$event['provider_order_id'];
        $log->admin_id = $event['admin_id'];
        $log->reception_id = $event['reception_id'];
        $log->save();
    }

	public function onUpdatedProductStatus($event)
	{
		$log = new ProviderOrderReceptionLog();
		$log->type = 'Estado de producto actualizado ID: '.$event['reception_detail']->store_product_id.' actualizado a: '.$event['reception_detail']->status.' con unidades recibidas: '.$event['reception_detail']->quantity_received;
		$log->admin_id = $event['admin_id'];
		$log->reception_id = $event['reception_id'];
		$log->save();
	}

	public function onUpdatedGroupedProductStatus($event)
	{
		$log = new ProviderOrderReceptionLog();
		$log->type = 'Estado de producto agrupado actualizado ID: '.$event['reception_detail']->store_product_id.' actualizado a: '.$event['reception_detail']->status.' con unidades recibidas: '.$event['reception_detail']->quantity_received;
		$log->admin_id = $event['admin_id'];
		$log->reception_id = $event['reception_id'];
		$log->save();
	}

	public function onChangedProduct($event)
	{
		$log = new ProviderOrderReceptionLog();
		$log->type = 'Cambio de producto de ID: '.$event['provider_order_detail']->store_product_id.' cambiado por: '.$event['new_product']->id;
		$log->admin_id = $event['admin_id'];
		$log->reception_id = $event['reception_id'];
		$log->save();
	}

	public function onUpdateFullProductCost($event)
	{
		$log = new ProviderOrderReceptionLog();
		if ( is_array($event['quantity_received']) ) {
			$log->type = 'Producto ID '.$event['store_product_id'].' / Costo unitario actualizado a: $'.number_format($event['cost'], 0, ',', '.').' / Recibo de bodega #'.$event['reception_id'].' Unidades recibidas actualizada de: '.$event['quantity_received']['quantity_received_before'].' a '.$event['quantity_received']['quantity_received_after'];
		}else{
			$log->type = 'Producto ID '.$event['store_product_id'].' / Costo unitario actualizado a: $'.number_format($event['cost'], 0, ',', '.').' / Recibo de bodega #'.$event['reception_id'];
		}
		$log->admin_id = $event['admin_id'];
		$log->reception_id = $event['reception_id'];
		$log->save();
	}

	public function onUpdateFullGroupedProductCost($event)
	{
		$log = new ProviderOrderReceptionLog();
		if ( is_array($event['quantity_received']) ) {
			$log->type = 'Producto agrupado ID '.$event['store_product_id'].' / Costo unitario actualizado a: $'.number_format($event['cost'], 0, ',', '.').' / Recibo de bodega #'.$event['reception_id'].' Unidades recibidas actualizada de: '.$event['quantity_received']['quantity_received_before'].' a '.$event['quantity_received']['quantity_received_after'];
		}else{
			$log->type = 'Producto ID '.$event['store_product_id'].' / Costo unitario actualizado a: $'.number_format($event['cost'], 0, ',', '.').' / Recibo de bodega #'.$event['reception_id'];
		}
		$log->admin_id = $event['admin_id'];
		$log->reception_id = $event['reception_id'];
		$log->save();
	}

	public function onUpdateProductCost($event)
	{
		$log = new ProviderOrderReceptionLog();
		$log->type = 'Costo unitario de producto ID '.$event['store_product_id'].' actualizado a: $'.number_format($event['cost'], 0, ',', '.');
		$log->admin_id = $event['admin_id'];
		$log->reception_id = $event['reception_id'];
		$log->save();
	}

	public function onUpdateProductQuantityExpected($event)
	{
		$log = new ProviderOrderReceptionLog();
		$log->type = 'Cantidad esperada de producto ID '.$event['store_product_id'].' actualizada a: '.$event['quantity_expected'];
		$log->admin_id = $event['admin_id'];
		$log->reception_id = $event['reception_id'];
		$log->save();
	}

	public function onErrorProductQuantityExpected($event)
	{
		$log = new ProviderOrderReceptionLog();
		$log->type = 'Cantidad esperada supera la cantidad pedida del producto ID '.$event['store_product_id'].' | Cantidad esperada: '.$event['quantity_expected'].' - Cantidad pedida: '.$event['quantity_order'];
		$log->admin_id = $event['admin_id'];
		$log->reception_id = $event['reception_id'];
		$log->save();
	}

	public function onUpdateGroupedProductCost($event)
	{
		$log = new ProviderOrderReceptionLog();
		$log->type = 'Costo unitario de producto agrupado ID '.$event['store_product_id'].' actualizado a: $'.number_format($event['cost'], 0, ',', '.');
		$log->admin_id = $event['admin_id'];
		$log->reception_id = $event['reception_id'];
		$log->save();
	}

    /**
     * Log al crear nota crédito
     *
     * @param $event
     */
    public function onCreateCreditNote($event)
    {
        $log = new ProviderOrderReceptionLog();
        $log->type = 'Se agrega nota crédito #'.$event[0]->credit_note_number;
        $log->admin_id = $event[1];
        $log->reception_id = $event[0]->id;
        $log->save();

        return false;
    }

    /**
     * Log al agregar o eliminar productos en la nota crédito
     *
     * @param $event
     * @return bool
     */
    public function onUpdateCreditNote($event)
    {
        $log = new ProviderOrderReceptionLog();
        $log->type = 'Producto ' . $event[2] . ' la nota crédito: Producto ID: '.$event[0]->store_product_id.' / Cantidad: '.$event[0]->quantity_credit_note.' / Precio: $'.number_format($event[0]->cost, 0, ',', '.').' / Razón: '.$event[0]->reason_credit_note;
        $log->admin_id = $event[1];
        $log->reception_id = (isset($event[0]->type)) ? $event[0]->reception_id : $event[0]->providerOrderReceptionDetail->reception_id;
        $log->save();

        return false;
    }

    /**
     * Actualiza el log con la actualizacion del proveedor real por parte de contabilidad
     *
     * @param $event
     */
    public function onUpdateRealProvider($event)
    {
        $log = new ProviderOrderReceptionLog();
        $log->type = 'Proveedor actualizado desde contabilidad: Proveedor ID: ' . $event[0]->real_provider_id . ' / Proveedor: ' . $event[0]->provider->name;
        $log->admin_id =  $event[1];
        $log->reception_id = $event[0]->id;
        $log->save();
    }

	public function subscribe($events)
	{
		$events->listen('reception.updatedStatus', 'ReceptionEventHandler@onUpdatedStatus');
		$events->listen('reception.addedProduct', 'ReceptionEventHandler@onAddedProduct');
		$events->listen('reception.updatedProductAccountingStatus', 'ReceptionEventHandler@onUpdatedProductAccountingStatus');
		$events->listen('reception.created', 'ReceptionEventHandler@onCreated');
        $events->listen('reception.updated', 'ReceptionEventHandler@onUpdated');
		$events->listen('reception.updatedProductStatus', 'ReceptionEventHandler@onUpdatedProductStatus');
		$events->listen('reception.updatedGroupedProductStatus', 'ReceptionEventHandler@onUpdatedGroupedProductStatus');
		$events->listen('reception.changedProduct', 'ReceptionEventHandler@onChangedProduct');
		$events->listen('reception.updatedFullProductCost', 'ReceptionEventHandler@onUpdateFullProductCost');
		$events->listen('reception.updatedFullGroupedProductCost', 'ReceptionEventHandler@onUpdateFullGroupedProductCost');
		$events->listen('reception.updatedProductCost', 'ReceptionEventHandler@onUpdateProductCost');
		$events->listen('reception.updatedProductQuantityExpected', 'ReceptionEventHandler@onUpdateProductQuantityExpected');
		$events->listen('reception.errorProductQuantityExpected', 'ReceptionEventHandler@onErrorProductQuantityExpected');
		$events->listen('reception.updatedGroupedProductCost', 'ReceptionEventHandler@onUpdateGroupedProductCost');
		$events->listen('reception.create_credit_note_log', 'ReceptionEventHandler@onCreateCreditNote');
		$events->listen('reception.update_credit_note_log', 'ReceptionEventHandler@onUpdateCreditNote');
		$events->listen('reception.update_real_provider_log', 'ReceptionEventHandler@onUpdateRealProvider');
	}
}
?>