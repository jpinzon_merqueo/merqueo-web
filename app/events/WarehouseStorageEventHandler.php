<?php
/**
* Clase para manejar eventos de recibos de bodega
*/
class WarehouseStorageEventHandler
{
    /**
     * Guarda el log cuando se saca unidades de una posición en la bodega.
     * @param WarehouseStorage $warehouseStorage
     * @param $module
     */
    public function onPullQuantity(WarehouseStorage $warehouseStorage, $module)
    {
        $log = new WarehouseStorageLog;
        $log->admin_id = Session::get('admin_id');
        $log->warehouse_id = $warehouseStorage->warehouse_id;
        $log->store_product_id = $warehouseStorage->store_product_id;
        $log->module = $module;
        $log->movement = 'remove';

        $dirtyQuantity = null;
        $positionTo = null;
        $positionHeightTo = null;

        $originalQuantity = null;
        $positionFrom = null;
        $positionHeightFrom = null;

        if ($warehouseStorage->isDirty()) {
            $dirty = $warehouseStorage->getDirty();
            if (key_exists('quantity', $dirty) && $dirty['quantity'] > 0) {
                $dirtyQuantity = $dirty['quantity'];
                $positionTo = key_exists('position', $dirty) ? $dirty['position'] : null;
                $positionHeightTo = key_exists('position_height', $dirty) ? $dirty['position_height'] : null;
            }
            $original = $warehouseStorage->getOriginal();
            if (!empty($original) && $original['quantity'] > 0) {
                $originalQuantity = $original['quantity'];
                $positionFrom = $original['position'];
                $positionHeightFrom = $original['position_height'];
            }
        }

        $log->quantity_from = $originalQuantity;
        $log->position_from = $positionFrom;
        $log->position_height_from = $positionHeightFrom;

        $log->quantity_to = $dirtyQuantity;
        $log->position_to = $positionTo;
        $log->position_height_to = $positionHeightTo;

        $log->save();
    }

    /**
     * Guarda el log cuando se almacena unidades en una posición de la bodega.
     * @param WarehouseStorage $warehouseStorage
     * @param $module
     */
    public function onPushQuantity(WarehouseStorage $warehouseStorage, $module)
    {
        $log = new WarehouseStorageLog;
        $log->admin_id = Session::get('admin_id');
        $log->warehouse_id = $warehouseStorage->warehouse_id;
        $log->store_product_id = $warehouseStorage->store_product_id;
        $log->module = $module;
        $log->movement = 'storage';

        $dirtyQuantity = null;
        $originalQuantity = null;
        $positionFrom = null;
        $positionHeightFrom = null;

        if ($warehouseStorage->isDirty()) {
            $dirty = $warehouseStorage->getDirty();
            if (key_exists('quantity', $dirty)) {
                $dirtyQuantity = $dirty['quantity'];
            }
            $original = $warehouseStorage->getOriginal();
            if (!empty($original)) {
                $originalQuantity = $original['quantity'];
                $positionFrom = $original['position'];
                $positionHeightFrom = $original['position_height'];
            }
        }

        $log->quantity_from = $originalQuantity;
        $log->position_from = $positionFrom;
        $log->position_height_from = $positionHeightFrom;

        $log->quantity_to = $dirtyQuantity;
        $log->position_to = $warehouseStorage->position;
        $log->position_height_to = $warehouseStorage->position_height;

        $log->save();
    }

    public function subscribe($events)
    {
        $events->listen('warehouseStorage.onPullQuantity', 'WarehouseStorageEventHandler@onPullQuantity');
        $events->listen('warehouseStorage.onPushQuantity', 'WarehouseStorageEventHandler@onPushQuantity');
    }
}