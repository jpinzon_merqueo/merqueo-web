<?php

use Illuminate\Support\Facades\Log;

class LogRequestDevolutionRequestPreChargeEventHandler
{
    public function createLog($orderId)
    {
        $admin_id = Config::get('app.dashboard_admin_id');
        $log = new OrderLog();
        $log->type = 'Se envio la solicitud de devolución de pre cobro.';
        $log->order_id = $orderId;
        $log->admin_id = $admin_id;        
        if($data = $log->save()){            
            return true;
        }
        return false;
    }

     /**
      * @events
     */
    public function subscribe($events)
    {
        $events->listen('log.create', 'LogRequestDevolutionRequestPreChargeEventHandler@createLog');
    }
}