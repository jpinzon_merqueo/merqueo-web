<?php
/**
* Clase para manejar eventos de recibos de bodega
*/
class StoreProductWarehouseEventHandler
{
    /**
     * Guarda el log cuando se saca unidades de una posición de picking en la bodega.
     * @param StoreProductWarehouse $storeProductWarehouse
     * @param $module
     * @param $quantity
     * @return bool
     */
    public function onPullQuantity(StoreProductWarehouse $storeProductWarehouse, $module, $quantity)
    {
        $productStockUpdate = new ProductStockUpdate;
        $productStockUpdate->admin_id = Session::get('admin_id');
        $productStockUpdate->store_product_id = $storeProductWarehouse->store_product_id;
        $productStockUpdate->warehouse_id = $storeProductWarehouse->warehouse_id;
        $productStockUpdate->type = 'Disminución de unidades';

        $dirtyQuantity = 0;
        $originalQuantity = 0;
        if ($storeProductWarehouse->isDirty()) {
            $dirty = $storeProductWarehouse->getDirty();
            if (key_exists('picking_stock', $dirty)) {
                $dirtyQuantity = $dirty['picking_stock'];
            }

            $original = $storeProductWarehouse->getOriginal();
            if (!empty($original)) {
                $originalQuantity = $original['picking_stock'];
            }
        }

        $productStockUpdate->quantity = $quantity;
        $productStockUpdate->quantity_stock_before = $originalQuantity;
        $productStockUpdate->quantity_stock_after = $dirtyQuantity;
        $productStockUpdate->reason = $module;

        return $productStockUpdate->save();
    }

    /**
     * Guarda el log cuando se ingresan unidades a una posición de picking en la bodega.
     * @param StoreProductWarehouse $storeProductWarehouse
     * @param $module
     * @param $quantity
     * @return bool
     */
    public function onPushQuantity(StoreProductWarehouse $storeProductWarehouse, $module, $quantity)
    {
        $productStockUpdate = new ProductStockUpdate;
        $productStockUpdate->admin_id = Session::get('admin_id');
        $productStockUpdate->store_product_id = $storeProductWarehouse->store_product_id;
        $productStockUpdate->warehouse_id = $storeProductWarehouse->warehouse_id;
        $productStockUpdate->type = 'Aumento de unidades';

        $dirtyQuantity = 0;
        $originalQuantity = 0;
        if ($storeProductWarehouse->isDirty()) {
            $dirty = $storeProductWarehouse->getDirty();
            if (key_exists('picking_stock', $dirty)) {
                $dirtyQuantity = $dirty['picking_stock'];
            }

            $original = $storeProductWarehouse->getOriginal();
            if (!empty($original)) {
                $originalQuantity = $original['picking_stock'];
            }
        }

        $productStockUpdate->quantity = $quantity;
        $productStockUpdate->quantity_stock_before = $originalQuantity;
        $productStockUpdate->quantity_stock_after = $dirtyQuantity;
        $productStockUpdate->reason = $module;

        return $productStockUpdate->save();
    }

    public function subscribe($events)
    {
        $events->listen('storeProductWarehouse.onPullQuantity', 'StoreProductWarehouseEventHandler@onPullQuantity');
        $events->listen('storeProductWarehouse.onPushQuantity', 'StoreProductWarehouseEventHandler@onPushQuantity');
    }
}