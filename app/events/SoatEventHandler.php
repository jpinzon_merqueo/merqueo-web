<?php

/**
 * Class SoatEventHandler
 */
class SoatEventHandler
{
    const NOTIFY_USER = true;

    /**
     * @var bool
     */
    private $notify_user;

    /**
     * @param ExternalService $external_service
     * @param RejectReason $reject_reason
     * @throws Exception
     */
    public function onCancel(ExternalService $external_service, RejectReason $reject_reason)
    {
        $params = json_decode($external_service->params, true);

        send_mail([
            'template_name' => 'emails.soat_problem',
            'subject' => 'Cancelación de compra de SOAT.',
            'vars' => [],
            'to' => [
                ['email' => $params["email"], 'name' => $params["taker"]["firstName"]." ".$params["taker"]["lastName"] ],
                ['email' => $params["buyer"]["email"], 'name' => $params["buyer"]["name"]  ]
            ],
        ], false, true);

        $this->logDetails($external_service, "Cancelación del servicio, motivo: {$reject_reason->reason}");
    }

    /**
     * @param ExternalService $externalService
     */
    public function onPayment(ExternalService $externalService)
    {

    }

    /**
     * @param ExternalService $external_service
     * @param RejectReason $reject_reason
     */
    public function onRefund(ExternalService $external_service, RejectReason $reject_reason)
    {
        $this->logDetails($external_service, "Reembolso del servicio, motivo: {$reject_reason->reason}");
    }

    /**
     * @param ExternalService $external_service
     * @param $type
     */
    private function logDetails(ExternalService $external_service, $type)
    {
        $log = new ExternalServiceLog();
        $log->type = $type;
        $log->externalService()->associate($external_service);
        if (\BackendAuth::check()) {
            $log->admin()->associate(\BackendAuth::user());
        }

        if (\Auth::check() && \Auth::user()->id == $external_service->user_id) {
            $log->user()->associate(\Auth::user());
        }

        $log->save();
    }

    /**
     * @param $events
     */
    public function subscribe($events)
    {
        $events->listen('soat.cancelled', 'SoatEventHandler@onCancel');
        $events->listen('soat.payed', 'SoatEventHandler@onPayment');
        $events->listen('soat.refund', 'SoatEventHandler@onRefund');
    }
}