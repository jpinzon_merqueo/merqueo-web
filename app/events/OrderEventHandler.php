<?php

use Campaigns\Jobs\TriggerCampaignTrackEventJob;
use orders\OrderStatus;
use usecases\contracts\Countries\CountrySettingsUseCaseInterface;
use usecases\contracts\orders\OrderCreatedNotificationInterface;

class OrderEventHandler
{
    const VALID_ORDER_STATUSES = ['Validation', 'Initiated'];

    /**
     * @var CountrySettingsUseCaseInterface
     */
    private $countrySettingsUseCase;
    
     /**
     * @var OrderCreatedNotificationInterface
     */   
    private $orderCreatedNotification;

    public function __construct()
    {
        $this->countrySettingsUseCase = app(CountrySettingsUseCaseInterface::class);
        $this->orderCreatedNotification = app(OrderCreatedNotificationInterface::class);
    }

    /**
     * Pedido creado
     *
     * @param array $event => Order, 1 => Array order data
     * @return boolean|null
     */
    public function onOrderCreated($event)
    {
        try {
            $order = $event[0];
            $dataCountry = $this->countrySettingsUseCase->getByStoreId($order->store_id);
            $order_data = $event[1];
            $is_order_express = isset($order_data['is_express']) && !empty($order_data['is_express']);
            //validar estado de pedido
            if (!in_array($order->status, self::VALID_ORDER_STATUSES) && !($order->status == 'Enrutado' && $is_order_express)) {
                return false;
            }

            // Enviar eventos
            if ($order->orderGroup->source == 'Web') {
                BaseController::trackEventOnBrowser(['multi' => $order_data['product_events']]);

                BaseController::trackEventOnBrowser([
                    'order_completed' => [
                        'orderId' => $order->id,
                        'size' => $order_data['total_products'],
                        'items' => $order_data['total_items'],
                        'ticket' => $order->total_amount,
                        'paymentMethod' => $order->payment_method,
                        'freeDelivery' => $order->delivery_amount > 0 ? 0 : 1,
                    ],
                ]);
            }

            if ($order->payment_method !== 'Débito - PSE') {
                $sendOrderReceivedEmailAction = new SendOrderReceivedEmailAction(
                    $order,
                    $order_data['products_mail'],
                    $is_order_express,
                    $dataCountry
                );
                $sendOrderReceivedEmailAction->run();
            }
            
            
            // Send SNS Notification once order has been created
            $this->orderCreatedNotification->handle($order);

            // Validar fraude en pedido
            $order->fraudValidation();
            UpdateLeanplumUser::upload($order->user_id);
            Queue::push(TriggerCampaignTrackEventJob::class, ['order_id' => $order->id], 'quick');

            return false;
        } catch (Exception $exception) {
            ErrorLog::add($exception, 512);
        }
    }

    /**
     * Pedido en camino
     *
     * @param Array $event => Order
     * @return boolean
     */
    public function onOrderOnMyWay($event)
    {
        try {
            $order = $event[0];

            //validar estado de pedido
            if ($order->status != 'Dispatched') {
                return false;
            }

            $max_planning_sequence = Order::where('route_id', $order->route_id)->max('planning_sequence');
            //si es la primera secuencia enviar notificacion a los primeros 3 pedidos
            if ($order->planning_sequence == 1) {
                $orders = Order::select('orders.*', 'user_firstname', 'user_lastname', 'user_email', 'user_phone',
                    'delivery_time',
                    'user_address', 'user_address_further', 'source', 'device_player_id', 'user_device_id', 'source_os',
                    'app_version')
                    ->join('order_groups', 'order_groups.id', '=', 'group_id')
                    ->where('planning_sequence', '<', 4)
                    ->where('route_id', $order->route_id)
                    //->where('source', '<>', 'Reclamo')
                    ->where('orders.status', 'Dispatched')
                    ->whereNull('ontheway_notification_date')
                    ->orderBy('planning_sequence')
                    ->get();
            } else {
                //si es la penultima secuencia no se envia nada
                if (($max_planning_sequence - 1) == $order->planning_sequence) {
                    return false;
                }

                //enviar notificacion al pedido dos secuencias adelante
                $next_planning_sequence = $order->planning_sequence + 2;
                $orders = Order::select('orders.*', 'user_firstname', 'user_lastname', 'user_email', 'user_phone',
                    'delivery_time',
                    'user_address', 'user_address_further', 'source', 'device_player_id', 'user_device_id', 'source_os',
                    'app_version')
                    ->join('order_groups', 'order_groups.id', '=', 'group_id')
                    ->where('planning_sequence', $next_planning_sequence)
                    ->where('route_id', $order->route_id)
                    //->where('source', '<>', 'Reclamo')
                    ->where('orders.status', 'Dispatched')
                    ->whereNull('ontheway_notification_date')
                    ->orderBy('planning_sequence')
                    ->get();
                //si no se ha enviado notificacion a la secuencia actual se envia
                if (empty($order->ontheway_notification_date)) {
                    $orders[] = $order;
                }

            }

            if (count($orders)) {
                foreach ($orders as $order) {
                    $order->ontheway_notification_date = date('Y-m-d H:i:s');
                    $order->save();

                    //calcular tiempo teniendo en cuenta la franja de entrega
                    $delivery_time_text = 'en los próximos 30 a 60 mins';
                    $delivery_time = explode(' - ', $order->delivery_time);
                    $delivery_date = strtotime(date('Y-m-d') . ' ' . $delivery_time[0]);
                    $interval = $delivery_date - time();
                    if ($interval > 0) {
                        $interval = abs($interval);
                        $minutes = round($interval / 60);
                        if ($minutes > 60) {
                            $hour1 = date('g', strtotime(date('Y-m-d') . ' ' . $delivery_time[0]));
                            $hour2 = date('g a', strtotime(date('Y-m-d') . ' ' . $delivery_time[0] . ' +1 hours'));
                            $delivery_time_text = 'entre ' . $hour1 . ' y ' . $hour2 . '.';
                        }
                    }

                    $send_sms_gift = false;
                    $send_sms_sample = false;
                    $products_mail = [];
                    $products = OrderProduct::where('order_id', $order->id)
                        ->where('is_gift', 0)
                        ->where('parent_id', 0)
                        //->where('type', '<>', 'Muestra')
                        ->join('store_products', 'store_products.id', '=', 'order_products.store_product_id')
                        ->orderBy(DB::raw("FIELD(fulfilment_status, 'Not Available', 'Fullfilled', 'Replacement')"))
                        ->select('order_products.*')
                        ->get();

                    $assigned_gifts = [];
                    foreach ($products as $product) {
                        if (empty($assigned_gifts[$product->store_product_id])) {
                            $assigned_gifts[$product->store_product_id] = 1;

                            $order_product_sample = OrderProduct::select('store_product_id AS id', 'price',
                                'quantity AS qty', 'product_name AS name',
                                'product_image_url AS img', 'product_quantity AS quantity', 'product_unit AS unit',
                                'fulfilment_status AS status', 'type', 'sampling_id',
                                'campaign_gift_id', 'is_gift', 'order_products.type')
                                ->where('parent_id', $product->store_product_id)
                                ->where('order_id', $order->id)
                                ->whereIn('type', ['Muestra', 'Product'])
                                ->first();
                        } else {
                            $order_product_sample = [];
                        }

                        if ($order_product_sample) {

                            if (!$order_product_sample->is_gift && $order_product_sample->type == 'Muestra' && $order_product_sample->status == 'Fullfilled') {
                                $sampling = Sampling::find($order_product_sample->sampling_id);
                            }

                            if ($order_product_sample->is_gift) {
                                $sampling = CampaignGift::find($order_product_sample->campaign_gift_id);
                            }

                            if (isset($sampling) && $sampling) {
                                $order_product_sample->message = 'Merqueo y ' . $sampling->brand . ' te regalan:';
                            }

                            $order_product_sample->total = $order_product_sample->quantity * $order_product_sample->price;
                            $product->sample = $order_product_sample->toArray();
                            $send_sms_sample = true;
                        }
                        //obtener productos regalo
                        if ($order->date > '2018-01-01 00:00:00' && $product->fulfilment_status == 'Not Available') {
                            $order_product_gift = OrderProduct::select('store_product_id AS id', 'price',
                                'quantity AS qty', 'product_name AS name',
                                'product_image_url AS img', 'product_quantity AS quantity', 'product_unit AS unit',
                                'fulfilment_status AS status', 'type')
                                ->where('parent_id', $product->store_product_id)
                                ->where('order_id', $order->id)
                                ->where('is_gift', 1)
                                ->first();
                            if ($order_product_gift) {
                                $order_product_gift->message = 'Lamentamos que este producto no esté disponible, sin embargo te damos este completamente gratis.';
                                $order_product_gift->total = $order_product_gift->quantity * $order_product_gift->price;
                                $product->gift = $order_product_gift->toArray();
                                $send_sms_gift = true;
                            }
                        }

                        $products_mail[] = [
                            'img' => $product->product_image_url,
                            'name' => $product->product_name,
                            'qty' => $product->quantity,
                            'qty_original' => $product->quantity_original,
                            'quantity' => $product->product_quantity,
                            'unit' => $product->product_unit,
                            'price' => $product->price,
                            'total' => $product->quantity * $product->price,
                            'type' => $product->type,
                            'status' => $product->fulfilment_status != 'Not Available' && $product->fulfilment_status != 'Returned' ? 'available.png' : 'no_available.png',
                            'gift' => $product->gift,
                            'sample' => $product->sample,
                        ];
                    }

                    $products_available = ['products' => [], 'subtotal' => 0];
                    $products_not_available = ['products' => [], 'subtotal' => 0];
                    foreach ($products_mail as $product) {
                        if ($product['status'] == 'available.png') {
                            $products_available['products'][] = $product;
                            $products_available['subtotal'] += $product['total'];
                        } else {
                            $products_not_available['products'][] = $product;
                            $products_not_available['subtotal'] += $product['total'];
                        }
                    }

                    $mail = [
                        'template_name' => 'emails.order_ontheway',
                        'subject' => 'Tu pedido en Merqueo va en camino',
                        'to' => [
                            [
                                'email' => $order->user_email,
                                'name' => $order->user_firstname . ' ' . $order->user_lastname,
                            ],
                        ],
                        'vars' => [
                            'order' => $order,
                            'products' => $products_mail,
                            'products_available' => $products_available,
                            'products_not_available' => $products_not_available,
                            'delivery_time' => $delivery_time_text,
                        ],
                    ];

                    //enviar mail
                    send_mail($mail);

                    //enviar sms
                    if ($order->payment_method == 'Efectivo') {
                        $total_order = $order->total_amount + $order->delivery_amount - $order->discount_amount;
                        if ($total_order > 0) {
                            $msn = 'Tu pedido en Merqueo va en camino. Recuerda que el valor a pagar es $' . number_format($total_order,
                                    0, ',', '.') . ' y el método de pago seleccionado es efectivo. Muchas gracias!';
                            send_sms($order->user_phone, $msn);
                        }
                    }

                    //enviar notificacion
                    if ($order->source == 'Device') {
                        $msn = 'Tu pedido llegará ' . $delivery_time_text . ' ⏰.';
                        if ($order->payment_method == 'Efectivo') {
                            $msn = 'Tu pedido llegará ' . $delivery_time_text . ' ⏰. Por favor alista el efectivo para el pago 💰.';
                        }

                        if ($order->payment_method == 'Datáfono') {
                            $msn = 'Tu pedido llegará ' . $delivery_time_text . ' ⏰. Por favor prepara tu tarjeta para el pago 💳.';
                        }

                        if ($order->source == 'Android') {
                            $msn .= ' Puedes ver donde esta tu pedido haciendo click aquí 🚚.';
                        }

                        $data = [
                            'type' => $order->status,
                            'message' => $msn,
                            'order_id' => $order->id,
                        ];
                        $device_id = !empty($order->device_player_id) ? $order->device_player_id : $order->user_device_id;
                        send_notification($order->app_version, $order->source_os, $device_id, $order->device_player_id,
                            $data);
                    }

                    //enviar mensaje de texto si fue desde ios y lleva regalos
                    if ($send_sms_gift && $order->source_os == 'iOS') {
                        $msn = 'Merqueo: Algunos productos de tu pedido no estaban disponibles, sin embargo te enviamos otros completamente gratis.';
                        send_sms($order->user_phone, $msn);
                    }

                    //enviar mensaje de texto si fue desde ios y lleva muestras
                    if ($send_sms_sample && $order->source_os == 'iOS') {
                        $msn = 'Merqueo: Algunos productos de tu pedido son de muestra y son completamente gratis, esperamos los disfrutes.';
                        send_sms($order->user_phone, $msn);
                    }
                }
            }

            return false;

        } catch (Exception $e) {

        }
    }

    /**
     * He llegado
     *
     * @param Array $event => Order, 1 => OrderGroup
     * @return boolean
     */
    public function onOrderArrived($event)
    {
        try {
            $order = $event[0];
            $order_group = $event[1];

            //validar estado de pedido
            if ($order->status != 'Dispatched')
                return false;

            //enviar notificacion
            if ($order_group->source == 'Device') {
                $data = [
                    'type' => $order->status,
                    'message' => 'Tu pedido en Merqueo ha llegado.',
                    'order_id' => $order->id,
                ];
                $device_id = !empty($order_group->device_player_id) ? $order_group->device_player_id : $order_group->user_device_id;
                send_notification($order_group->app_version, $order_group->source_os, $device_id,
                    $order_group->device_player_id, $data);
            }

            return false;

        } catch (Exception $e) {

        }
    }

    /**
     * Pedido entregado o cancelado
     *
     * @param Array $event => Order, 1 => OrderGroup
     * @return boolean
     */
    public function onOrderManaged($event)
    {
        try {

            $order = $event[0];
            $order_group = $event[1];

            //validar estado de pedido
            if ($order->status != 'Delivered' && $order->status != 'Cancelled') {
                return false;
            }

            //no enviar notificacion si el pedido esta en pausa
            if ($order->status == 'Cancelled' && strstr($order->reject_reason, 'Pausar')) {
                return false;
            }

            //enviar mail de pedido
            $templates = [
                'Cancelled' => 'emails.order_rejected',
                'Delivered' => 'emails.order_delivered',
            ];

            $subjects = [
                'Cancelled' => 'ha sido cancelado',
                'Delivered' => 'ha sido entregado',
            ];

            if (isset($templates[$order->status]) /*&& $order_group->source != 'Reclamo'*/) {
                $products_mail[$order->id] = [];
                $products = OrderProduct::where('order_id', $order->id)
                    ->where('is_gift', 0)
                    ->where('parent_id', 0)
                    //->where('type', '<>', 'Muestra')
                    ->join('store_products', 'store_products.id', '=', 'order_products.store_product_id')
                    ->orderBy(DB::raw("FIELD(fulfilment_status, 'Not Available', 'Fullfilled', 'Replacement')"))
                    ->select('order_products.*')
                    ->get();

                $assigned_gifts = [];
                foreach ($products as $product) {
                    if (empty($assigned_gifts[$product->store_product_id])) {
                        $assigned_gifts[$product->store_product_id] = 1;

                        $order_product_sample = OrderProduct::select('store_product_id AS id', 'price',
                            'quantity AS qty', 'product_name AS name',
                            'product_image_url AS img', 'product_quantity AS quantity', 'product_unit AS unit',
                            'fulfilment_status AS status', 'type', 'sampling_id',
                            'campaign_gift_id', 'is_gift', 'order_products.type')
                            ->where('parent_id', $product->store_product_id)
                            ->where('order_id', $order->id)
                            ->whereIn('type', ['Muestra', 'Product'])
                            ->first();
                    } else {
                        $order_product_sample = [];
                    }

                    if ($order_product_sample) {

                        if (!$order_product_sample->is_gift && $order_product_sample->type == 'Muestra' && $order_product_sample->status == 'Fullfilled') {
                            $sampling = Sampling::find($order_product_sample->sampling_id);
                        }

                        if ($order_product_sample->is_gift) {
                            $sampling = CampaignGift::find($order_product_sample->campaign_gift_id);
                        }

                        if (isset($sampling) && $sampling) {
                            $order_product_sample->message = 'Merqueo y ' . $sampling->brand . ' te regalan:';
                        }

                        $order_product_sample->total = $order_product_sample->quantity * $order_product_sample->price;
                        $product->sample = $order_product_sample->toArray();

                    }

                    //obtener productos regalo
                    if ($order->date > '2018-01-01 00:00:00' && $product->fulfilment_status == 'Not Available') {
                        $order_product_gift = OrderProduct::select('store_product_id AS id', 'price', 'quantity AS qty',
                            'product_name AS name',
                            'product_image_url AS img', 'product_quantity AS quantity', 'product_unit AS unit',
                            'fulfilment_status AS status', 'type')
                            ->where('parent_id', $product->store_product_id)
                            ->where('order_id', $order->id)
                            ->where('is_gift', 1)
                            ->first();
                        if ($order_product_gift) {
                            $order_product_gift->message = 'Lamentamos que este producto no esté disponible, sin embargo te damos este completamente gratis.';
                            $order_product_gift->total = $order_product_gift->quantity * $order_product_gift->price;
                            $product->gift = $order_product_gift->toArray();
                        }
                    }

                    $products_mail[$order->id][] = [
                        'img' => $product->product_image_url,
                        'name' => $product->product_name,
                        'qty' => $product->quantity,
                        'qty_original' => $product->quantity_original,
                        'quantity' => $product->product_quantity,
                        'unit' => $product->product_unit,
                        'price' => $product->price,
                        'total' => $product->quantity * $product->price,
                        'type' => $product->type,
                        'status' => $product->fulfilment_status != 'Not Available' && $product->fulfilment_status != 'Returned' ? 'available.png' : 'no_available.png',
                        'gift' => $product->gift,
                        'sample' => $product->sample,
                    ];
                }

                $products_available = ['products' => null, 'subtotal' => 0];
                $products_not_available = ['products' => null, 'subtotal' => 0];
                foreach ($products_mail[$order->id] as $product) {
                    if ($product['status'] == 'available.png') {
                        $products_available['products'][] = $product;
                        $products_available['subtotal'] += $product['total'];
                    } else {
                        $products_not_available['products'][] = $product;
                        $products_not_available['subtotal'] += $product['total'];
                    }
                }

                $store = Store::find($order->store_id);

                $mail = [
                    'template_name' => $templates[$order->status],
                    'subject' => 'Tu pedido en Merqueo ' . $subjects[$order->status],
                    'to' => [
                        [
                            'email' => $order_group->user_email,
                            'name' => "{$order_group->user_firstname} {$order_group->user_lastname}",
                        ],
                    ],
                    'vars' => [
                        'order' => $order,
                        'order_group' => $order_group,
                        'store' => $store,
                        'products' => $products_mail[$order->id],
                        'products_available' => $products_available,
                        'products_not_available' => $products_not_available,
                    ],
                ];

                //enviar mail
                send_mail($mail);

                //enviar notificacion
                if ($order_group->source == 'Device') {
                    $notifications = [
                        'Delivered' => '¿Salió todo bien con tu pedido de Merqueo?',
                        'Cancelled' => 'Tu pedido en Merqueo fue cancelado.',
                    ];
                    $data = [
                        'type' => $order->status,
                        'message' => $notifications[$order->status],
                        'order_id' => $order->id,
                        // Se debe enviar el token para el uso en versiones anteriores.
                        'token' => $order->user_score_token,
                    ];

                    $device_id = !empty($order_group->device_player_id) ? $order_group->device_player_id : $order_group->user_device_id;
                    send_notification($order_group->app_version, $order_group->source_os, $device_id,
                        $order_group->device_player_id, $data);
                }

                // Los procedencia de estos datos es dudosa, no es seguro que
                // se cuente con el usuario o su identificador.
                $order = Order::with('user')
                    ->find($order->id);
                UpdateLeanplumUser::upload($order->user_id);
            }

            return false;

        } catch (Exception $e) {

        }
    }

    /**
     * Log al actualizar estado del pedido
     *
     * @param Array $event => Order, 1 => ID usuario, 2 => Tipo usuario
     * @return boolean
     */
    public function onOrderStatusUpdate($event)
    {
        $log = new OrderLog();

        if ($event[0]->status == 'Cancelled') {
            $log->type = 'Estado actualizado a: ' . $event[0]->status . ' / ' . $event[0]->reject_reason;
            Order::getStatusFlow($event[0]->id, $event[0]->user_id, true);
        } else {
            $log->type = 'Estado actualizado a: ' . $event[0]->status;
        }

        $field = $event[2] . '_id';
        $log->{$field} = $event[1];
        $log->order_id = $event[0]->id;
        $log->save();

        //activar o desactivar productos comprometidos
        //$event[0]->validateCommittedStock();

        return false;
    }

    /**
     * Log al actualizar total del pedido
     *
     * @param Array $event => Order, 1 => ID usuario, 2 => Tipo usuario
     * @return boolean
     */
    public function onOrderTotalUpdate($event)
    {
        $log = new OrderLog();
        $log->type = 'Total de pedido actualizado: $' . number_format($event[0]->total_amount, 0, ',', '.');
        $field = $event[2] . '_id';
        $log->{$field} = $event[1];
        $log->order_id = $event[0]->id;
        $log->save();

        return false;
    }

    /**
     * Log al actualizar cantidad del producto
     *
     * @param Array $event => Object, 1 => ID usuario, 2 => Tipo usuario
     * @return boolean
     */
    public function onOrderQuantityUpdate($event)
    {
        $log = new OrderLog();
        $log->type = 'Unidades modificadas: ' . $event[0]->name . ' de ' . $event[0]->old_quantity . ' a ' . $event[0]->quantity . ' unidades. Estado: ' . $event[0]->order_status;
        $field = $event[2] . '_id';
        $log->{$field} = $event[1];
        $log->order_id = $event[0]->id;
        $log->save();

        return false;
    }

    /**
     * Log al actualizar producto del pedido
     *
     * @param Array $event => Order product, 1 => ID usuario, 2 => Tipo usuario
     * @return boolean
     */
    public function onOrderProductUpdate($event)
    {
        $status = [
            'Pending' => 'Pendiente',
            'Fullfilled' => 'Disponible',
            'Not Available' => 'No disponible',
            'Missing' => 'Faltante',
            'Replacement' => 'Reemplazado',
            'Returned' => 'Devolución',
        ];

        $log = new OrderLog();
        $log->type = 'Producto actualizado: Nombre: ' . $event[0]->product_name . ' / Cantidad: ' . $event[0]->quantity . ' / Precio: $' . number_format($event[0]->price,
                0, ',', '.') . ' / Estado: ' . $status[$event[0]->fulfilment_status];
        $field = $event[2] . '_id';
        $log->{$field} = $event[1];
        $log->order_id = $event[0]->order_id;
        $log->save();

        return false;
    }

    /**
     * Log al actualizar producto del pedido
     *
     * @param Array $event => Order product, 1 => ID usuario.
     * @param bool $by_client
     * @return boolean
     */
    public function onOrderProductRemove($event, $by_client = false)
    {
        $log = new OrderLog();
        $log->type = 'Producto eliminado: Nombre: ' . $event[0]->product_name . ' / Cantidad: ' . $event[0]->quantity . ' / Precio: $' . number_format($event[0]->price,
                0, ',', '.');
        $log->admin_id = $event[1];
        if ($by_client) {
            $log->user_id = $event[0]->order->user_id;
        }
        $log->order_id = $event[0]->order_id;
        $log->save();

        return false;
    }

    /**
     * Log al asignar ruta al pedido
     *
     * @param Array $event => Order, 1 => ID usuario
     * @return boolean
     */
    public function onOrderRouteAssign($event)
    {
        $log = new OrderLog();
        $log->type = 'Pedido asignado a ruta: ' . $event[0]->route_id . ' - Secuencia: ' . $event[0]->planning_sequence;
        $log->admin_id = $event[1];
        $log->order_id = $event[0]->id;
        $log->save();

        return false;
    }

    /**
     * Log al asignar transportador al pedido
     *
     * @param Array $event => Order, 1 => ID usuario, 2 => Placa vehiculo, 3 => ID conductor
     * @return boolean
     */
    public function onOrderTransporterAssign($event)
    {
        $log = new OrderLog();
        $log->type = 'Pedido asignado a transportador: ' . $event[2] . ' - ' . $event[3];
        $log->admin_id = $event[1];
        $log->order_id = $event[0]->id;
        $log->save();

        Order::getStatusFlow($event[0]->id, $event[0]->user_id, true);

        return false;
    }

    /**
     * Log al asignar alistador al pedido
     *
     * @param Array $event => Order, 1 => ID usuario.
     * @return boolean
     */
    public function onOrderPickerAssign($event)
    {
        $log = new OrderLog();
        $log->type = 'Pedido asignado a alistadores: Seco ID: ' . $event[0]->picker_dry_id . ' / Frío ID: ' . $event[0]->picker_cold_id;
        $log->admin_id = $event[1];
        $log->order_id = $event[0]->id;
        $log->save();

        return false;
    }

    /**
     * Log al asignar permitir la entrega anticipada del pedido
     *
     * @param Array $event => Order, 1 => ID usuario.
     * @return boolean
     */
    public function onOrderAllowEarlyDelivery($event)
    {
        $log = new OrderLog();
        $log->type = 'Entrega anticipada permitida.';
        $log->admin_id = $event[1];
        $log->order_id = $event[0]->id;
        $log->save();

        return false;
    }

    /**
     * Log al actualizar el estado de pago del pedido
     *
     * @param Array $event => Order, 1 => ID usuario.
     * @return boolean
     */
    public function onOrderPaymentStatusUpdate($event)
    {
        $log = new OrderLog();
        $status = empty($event[0]->payment_date) ? 'no pagado' : 'pagado';
        $log->type = 'Estado de pago actualizado a ' . $status . '.';
        $log->admin_id = $event[1];
        $log->order_id = $event[0]->id;
        $log->save();

        return false;
    }

    /**
     * Fecha de pedido actualizada
     *
     * @param Order $order
     * @param Admin|null $admin
     * @param User|null $user
     * @return boolean
     */
    public function onDeliveryDateUpdate(Order $order, Admin $admin = null, User $user = null)
    {
        $current_time = format_date('normal_with_time', $order->getOriginal('delivery_date'));
        $new_time = format_date('normal_with_time', $order->delivery_date);
        $log_type = "Fecha de entrega actualizada: {$current_time} a {$new_time}";

        $log = new OrderLog();
        $log->type = $log_type;
        $log->admin_id = empty($admin) ? null : $admin->id;
        $log->user_id = empty($user) ? null : $user->id;
        $log->order_id = $order->id;
        $log->save();

        return false;
    }

    /**
     * Credito asignado al pedido
     *
     * @param Order $order
     * @param int $amount
     * @param string $reason
     * @param Admin|null $admin
     * @param User|null $user
     * @return boolean
     */
    public function onCreditAdded(Order $order, $amount, $reason, Admin $admin = null, User $user = null)
    {
        $currency = currency_format($amount);
        $log_type = "Credito agregado al pedido por el valor de : {$currency}. Asociado a: {$reason}";
        $log = new OrderLog();
        $log->type = $log_type;
        $log->admin_id = empty($admin) ? null : $admin->id;
        $log->user_id = empty($user) ? null : $user->id;
        $log->order_id = $order->id;
        $log->save();

        return false;
    }

    /**
     * Datos de factura actualizados
     *
     * @param Order $order
     * @param Admin|null $admin
     * @param User|null $user
     * @return boolean
     */
    public function onBusinessDataAssigned(Order $order, Admin $admin = null, User $user = null)
    {
        $log_type = "Asignación de datos de factura de empresa: Original: " .
            "{$order->getOriginal('user_identity_type')} - {$order->getOriginal('user_identity_number')} - " .
            "{$order->getOriginal('user_business_name')}
Nuevos datos: {$order->user_identity_type} - {$order->user_identity_number} - {$order->user_business_name}";
        $log = new OrderLog();
        $log->type = $log_type;
        $log->admin_id = empty($admin) ? null : $admin->id;
        $log->user_id = empty($user) ? null : $user->id;
        $log->order_id = $order->id;
        $log->save();

        return false;
    }

    /**
     * Cambio de metodo de pago
     *
     * @param Order $order
     * @param Admin|null $admin
     * @param User|null $user
     * @return boolean
     */
    public function onPaymentMethodChanged(Order $order, Admin $admin = null, User $user = null)
    {
        if (!$order->isDirty('payment_method')) {
            return false;
        }

        $original = $order->getOriginal('payment_method');
        $log = new OrderLog();
        $log->type = "Método de pago actualizado: $original a $order->payment_method";
        $log->admin_id = empty($admin) ? null : $admin->id;
        $log->user_id = empty($user) ? null : $user->id;
        $log->order_id = $order->id;
        $log->save();

        return false;
    }

    /**
     * Pedido cancelado motivo
     *
     * @param int $orderId
     * @param int $rejectReasonId
     * @param int|null $adminId
     * @return boolean
     */
    public function onRejectLog($orderId, $rejectReasonId, $adminId = null)
    {
        $log = new OrderRejectReasonLog();
        $log->reject_reason_id = $rejectReasonId;
        $log->order_id = $orderId;
        $log->admin_id = $adminId;
        $log->save();

        return false;
    }

    /**
     * Pedido cancelado
     *
     * @param Order $order
     * @param Admin|null $admin
     * @param User|null $user
     * @return boolean
     */
    public function onCancel(Order $order, Admin $admin = null, User $user = null)
    {
        if ($order->status !== OrderStatus::CANCELED) {
            return false;
        }

        $log = new OrderLog();
        $log->type = 'El pedido fue cancelado';
        $log->admin_id = empty($admin) ? null : $admin->id;
        $log->user_id = empty($user) ? null : $user->id;
        $log->order_id = $order->id;
        $log->save();

        return false;
    }

    /**
     * Pedido modificado desde faltantes
     *
     * @param int $orderId
     * @param int|null $adminId
     * @param int|null $userId
     * @return boolean
     */
    public function onOrderUpdateFromMissingProducts($orderId, $adminId = null, $userId = null)
    {
        $log = new OrderLog();
        $log->type = 'Pedido actualizado en módulo de faltantes.';
        $log->admin_id = $adminId;
        $log->user_id = $userId;
        $log->order_id = $orderId;
        $log->save();

        return false;
    }

    /**
     * Pedido gestionado y validado por Sac
     *
     * @param  $order_id
     * @param null $admin_id
     * @return boolean
     */
    public function onOrderManagedSac($orderId, $adminId = null, $reasonId)
    {
        switch ($reasonId) {
            case 3: // cambo de método de pago
                $type = 'Cambio de método de pago validado por SAC';
                break;

            case 5: // pagados con PSE faliidos
                $type = 'Método de pago PSE fallido validado por SAC';
                break;

            case 6: // pagados con PSE y con faltantes
                $type = 'Método de pago PSE y faltantes validado por SAC';
                break;

            case 7: // pagados por PSE y cancelados por cliente
                $type = 'Método de pago PSE cancelado por cliente validado por SAC';
                break;

            case null:
            case '':
                $type = 'reasonID Null';
                break;

            default:
                break;
        }

        $log = new OrderLog();
        $log->type = $type;
        $log->admin_id = $adminId;
        $log->order_id = $orderId;
        $log->save();

        return false;
    }

    /**
     * Log al crear nota crédito
     *
     * @param $event
     */
    public function onCreateCreditNote($event)
    {
        $log = new OrderLog();
        $log->type = 'Se agrega nota crédito #' . $event[0]->credit_note_number;
        $log->admin_id = $event[1];
        $log->order_id = $event[0]->id;
        $log->save();

        return false;
    }

    /**
     * Log al agregar o eliminar productos en la nota crédito
     *
     * @param $event
     * @return bool
     */
    public function onUpdateCreditNote($event)
    {
        $log = new OrderLog();
        $log->type = 'Producto ' . $event[2] . ' la nota crédito: Nombre: ' . $event[0]->product_name . ' / Cantidad: ' . $event[0]->quantity_credit_note . ' / Precio: $' . number_format($event[0]->price,
                0, ',', '.') . ' / Razón: ' . $event[0]->reason_credit_note;
        $log->admin_id = $event[1];
        $log->order_id = $event[0]->order_id;
        $log->save();

        return false;
    }

    /**
     * Registrar eventos
     */
    public function subscribe($events)
    {
        $events->listen('order.created', 'OrderEventHandler@onOrderCreated');
        $events->listen('order.onmyway', 'OrderEventHandler@onOrderOnMyWay');
        $events->listen('order.arrived', 'OrderEventHandler@onOrderArrived');
        $events->listen('order.managed', 'OrderEventHandler@onOrderManaged');
        $events->listen('order.status_log', 'OrderEventHandler@onOrderStatusUpdate');
        $events->listen('order.total_log', 'OrderEventHandler@onOrderTotalUpdate');
        $events->listen('order.product_log', 'OrderEventHandler@onOrderProductUpdate');
        $events->listen('order.product_remove_log', 'OrderEventHandler@onOrderProductRemove');
        $events->listen('order.transporter_log', 'OrderEventHandler@onOrderTransporterAssign');
        $events->listen('order.route_log', 'OrderEventHandler@onOrderRouteAssign');
        $events->listen('order.picker_log', 'OrderEventHandler@onOrderPickerAssign');
        $events->listen('order.allow_early_delivery_log', 'OrderEventHandler@onOrderAllowEarlyDelivery');
        $events->listen('order.payment_status_log', 'OrderEventHandler@onOrderPaymentStatusUpdate');
        $events->listen('order.delivery_date', 'OrderEventHandler@onDeliveryDateUpdate');
        $events->listen('order.credit_added', 'OrderEventHandler@onCreditAdded');
        $events->listen('order.business_assigned', 'OrderEventHandler@onBusinessDataAssigned');
        $events->listen('order.payment_method_changed', 'OrderEventHandler@onPaymentMethodChanged');
        $events->listen('order.canceled', 'OrderEventHandler@onCancel');
        $events->listen('order.reject_log', 'OrderEventHandler@onRejectLog');
        $events->listen('order.product_quantity_log', 'OrderEventHandler@onOrderQuantityUpdate');
        $events->listen('order.missing_products_log', 'OrderEventHandler@onOrderUpdateFromMissingProducts');
        $events->listen('order.managed_sac_log', 'OrderEventHandler@onOrderManagedSac');
        $events->listen('order.update_credit_note_log', 'OrderEventHandler@onUpdateCreditNote');
        $events->listen('order.create_credit_note_log', 'OrderEventHandler@onCreateCreditNote');
    }
}
