<?php
/**
*
*/
class ProviderOrderEventHandler
{
	public function onUpdatedDeliveryDate($event)
	{
		$log = new ProviderOrderLog();
		$log->type = 'Fecha de entrega actualizada: '.$event['delivery_date'];
		$log->admin_id = $event['admin_id'];
		$log->provider_order_id = $event['provider_order_id'] ;
		$log->save();
	}

	public function onUpdatedStatus($event)
	{
		$log = new ProviderOrderLog();
		if (empty($event['admin_id'])) {
			$log->type = 'Estado actualizado por el sistema: '.$event['status'];
		} else {
			$log->type = 'Estado actualizado: '.$event['status'];
		}
		$log->admin_id = $event['admin_id'];
		$log->provider_order_id = $event['provider_order_id'] ;
		$log->save();
	}

	public function onDeletedProduct($event)
	{
		$log = new ProviderOrderLog();
		$log->type = 'Producto eliminado: '.$event['store_product_id'];
		$log->admin_id = $event['admin_id'];
		$log->provider_order_id = $event['provider_order_id'] ;
		$log->save();
	}

	public function onUpdatedProductQuantity($event)
	{
		$log = new ProviderOrderLog();
		$log->type = 'Cantidad de unidades editada en producto: '.$event['store_product_id'].', nueva cantidad:'.$event['quantity_unit_to_request'];
		$log->admin_id = $event['admin_id'];
		$log->provider_order_id = $event['provider_order_id'] ;
		$log->save();
	}

	public function onProductAdded($event)
	{
		$log = new ProviderOrderLog();
		$log->type = 'Producto agregado: '.$event['store_product_id'];
		$log->admin_id = $event['admin_id'];
		$log->provider_order_id = $event['provider_order_id'] ;
		$log->save();
	}

	public function onUploadedRemission($event)
	{
		$log = new ProviderOrderLog();
		$log->type = 'Archivo de remisión importado';
		$log->admin_id = $event['admin_id'];
		$log->provider_order_id = $event['provider_order_id'] ;
		$log->save();
	}

	public function subscribe($events)
	{
		$events->listen('providerOrder.updatedDeliveryDate', 'ProviderOrderEventHandler@onUpdatedDeliveryDate');
		$events->listen('providerOrder.updatedStatus', 'ProviderOrderEventHandler@onUpdatedStatus');
		$events->listen('providerOrder.deletedProduct', 'ProviderOrderEventHandler@onDeletedProduct');
		$events->listen('providerOrder.updatedProductQuantity', 'ProviderOrderEventHandler@onUpdatedProductQuantity');
		$events->listen('providerOrder.addedProduct', 'ProviderOrderEventHandler@onProductAdded');
		$events->listen('providerOrder.uploadedRemission', 'ProviderOrderEventHandler@onUploadedRemission');
	}
}
?>