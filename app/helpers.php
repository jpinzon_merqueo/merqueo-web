<?php

use Hashids\Hashids;

if (!function_exists('web_url'))
{
    /**
     * Obtiene URL global
     *
     * @return string URL
     */
    function web_url()
    {
        return Config::get('app.url');
    }
}

if (!function_exists('asset_url'))
{
    /**
     * Obtiene URL a assets
     *
     * @return string URL
     */
    function asset_url()
    {
        return Config::get('app.url_web').'/assets/';
    }
}

if (!function_exists('admin_asset_url'))
{
    /**
     * Obtiene URL a admin_assets
     *
     * @return string URL
     */
    function admin_asset_url()
    {
        return web_url().'/admin_asset/';
    }
}

if (!function_exists('admin_url'))
{
    /**
     * Obtiene URL global del administrador.
     *
     * @param boolean $show_complete_url Determina si la ulr a obtener incluye el dominio.
     * @return string
     */
    function admin_url($show_complete_url = false)
    {
        $admin_prefix = getenv('URL_ADMIN') ?: 'admin';

        return $show_complete_url
            ? (web_url() . $admin_prefix)
            : "/{$admin_prefix}";
    }
}

if (!function_exists('is_admin_url'))
{
    /**
     * Determina si la url actual hace parte del administrador.
     *
     * @return bool
     */
    function is_admin_url()
    {
        $admin_segments = explode('/', getenv('URL_ADMIN') ?: 'admin');
        foreach ($admin_segments as $index => $segment) {
            $url_segment = \Request::segment($index + 1);
            if ($segment !== $url_segment) {
                return false;
            }
        }

        return true;
    }
}

if (!function_exists('uploads_path'))
{
    /**
     * Obtiene ruta a uploads
     *
     * @return string Path
     */
    function uploads_path()
    {
        return public_path().'/uploads/';
    }
}

if (!function_exists('image_url'))
{
    /**
     * Obtiene URL de imagenes de productos
     *
     * @return string URL
     */
    function image_url()
    {
        return Config::get('app.aws.cloudfront_url').'/images/';
    }
}

if (!function_exists('clean'))
{
    /**
     * Limpia string sin espacios y caracteres especiales
     *
     * @param string $string Texto
     * @return string Texto
     */
    function clean($string)
    {
       $string = str_replace(' ', '-', $string);

       return preg_replace('/[^A-Za-z0-9\-]/', '', $string); //eliminar caracteres especiales
    }
}

if (!function_exists('generate_token'))
{
    /**
     * Genera token unico
     *
     * @return string Token
     */
    function generate_token()
    {
        return clean(Hash::make( rand() . time() . rand()));
    }
}

if (!function_exists('round_price'))
{
    /**
     * Redondea precio a 50
     *
     * @param int $price Precio
     * @return string Precio redondeado
     */
    function round_price($price)
    {
        if (strlen($price) > 2){
            $last_two = substr($price, -2, 2);
            if ($last_two != '50' && $last_two != '00'){
                $difference = $last_two < 50 ? ($last_two - 50) * -1 : ($last_two - 100) * -1;
                $price += $difference;
            }
        }

        return $price;
    }
}

if (!function_exists('upload_image'))
{
    /**
     * Subir imagen o archivo al servidor
     *
     * @param object $file Archivo
     * @param int $store_id ID de la tienda del producto
     * @param int $dir Ruta a directorio especifico
     * @param string $content Contenido del archivo.
     * @return string $url URL de la imagen
     */
    function upload_image($file, $is_product = false, $dir = false, $content = null)
    {
        ini_set('memory_limit', '1024M');
        set_time_limit(0);

        $url = false;

        if (is_object($file)){
            $file_data['real_path'] = $file->getRealPath();
            $file_data['client_original_name'] = $file->getClientOriginalName();
            $file_data['client_original_extension'] = $file->getClientOriginalExtension();
        }else $file_data = $file;

        $file_name = md5(uniqid(rand(), true));

        if ($content !== null) {
            $ext = $file_data['client_original_extension'];
            return upload_file_s3("{$dir}/{$file_name}.{$ext}", null, $content);
        }

        //subir imagen de producto
        if ($is_product)
        {
           $path_tmp = $file_data['real_path'];
           $absolute_path_images = Config::get('app.absolute_path_images');
            $path_image = '/products';
           $path = $absolute_path_images.'/'.$path_image;
           $large = $medium = $app = $small = false;

           //crear directorios
           if (!is_dir($path)){
                if (!is_dir($absolute_path_images.$path_image)){
                    if (!mkdir($absolute_path_images.$path_image, 0755))
                        return false;
                    chmod($absolute_path_images.$path_image, 0755);
                }
           }
           $sizes = array('large', 'medium', 'small', 'app');
           foreach($sizes as $size){
                if (!is_dir($absolute_path_images.$path_image.'/'.$size)){
                    if (!mkdir($absolute_path_images.$path_image.'/'.$size, 0755))
                        return false;
                    chmod($absolute_path_images.$path_image.'/'.$size, 0755);
                }
           }

           $file_data['client_original_name'] = str_replace(' ', '-', $file_data['client_original_name']);

           $cloudfront_url = Config::get('app.aws.cloudfront_url');
           $bucket_path = 'images/products';
           $image_url = $cloudfront_url.'images/products';

           try {
               $image = new Imagick($path_tmp);
               $size = get_size_image($path_tmp, 1000, 700);
               $image->setImageCompressionQuality(100);
               $image->resizeImage($size['width'], $size['height'], Imagick::FILTER_LANCZOS, 1);
               if ($image->writeImage($path.'/large/'.$file_data['client_original_name']))
                   $large = upload_file_s3($bucket_path.'/large/'.$file_data['client_original_name'], $path.'/large/'.$file_data['client_original_name']);

               $image = new Imagick($path_tmp);
               $size = get_size_image($path_tmp, 280, 300);
               $image->setImageCompressionQuality(100);
               $image->resizeImage($size['width'], $size['height'], Imagick::FILTER_LANCZOS, 1);
               if ($image->writeImage($path.'/medium/'.$file_data['client_original_name']))
                    $medium = upload_file_s3($bucket_path.'/medium/'.$file_data['client_original_name'], $path.'/medium/'.$file_data['client_original_name']);

               $image = new Imagick($path_tmp);
               $size = get_size_image($path_tmp, 140, 140);
               $image->setImageCompressionQuality(100);
               $image->resizeImage($size['width'], $size['height'], Imagick::FILTER_LANCZOS, 1);
               if ($image->writeImage($path.'/app/'.$file_data['client_original_name']))
                   $app = upload_file_s3($bucket_path.'/app/'.$file_data['client_original_name'], $path.'/app/'.$file_data['client_original_name']);

               $image = new Imagick($path_tmp);
               $size = get_size_image($path_tmp, 60, 60);
               $image->setImageCompressionQuality(100);
               $image->resizeImage($size['width'], $size['height'], Imagick::FILTER_LANCZOS, 1);
               if ($image->writeImage($path.'/small/'.$file_data['client_original_name']))
                    $small = upload_file_s3($bucket_path.'/small/'.$file_data['client_original_name'], $path.'/small/'.$file_data['client_original_name']);
           } catch (Exception $e) {
                die($e->getMessage());
           }

           if ($large && $medium && $app && $small){
                $url = array(
                    'large' => $large,
                    'medium' => $medium,
                    'app' => $app,
                    'small' => $small,
                );
           }else{
                $url = array(
                    'large' => $cloudfront_url.'/images/no_disponible.jpg',
                    'medium' => $cloudfront_url.'/images/no_disponible.jpg',
                    'app' => $cloudfront_url.'/images/no_disponible.jpg',
                    'small' => $cloudfront_url.'/images/no_disponible.jpg',
                );
            }
        }else{
            $ext = $file_data['client_original_extension'];
            $url = upload_file_s3($dir.'/'.$file_name.'.'.$ext, $file_data['real_path']);
        }
        return $url;
    }
}

if (!function_exists('upload_file_s3'))
{
    /**
     * Subir archivo a S3 AWS, si el contenido es suministrado se
     * envia no es necesario almacenar archivos temporales localmente.
     *
     * @param string $path_bucket Ruta a archivo en el bucket
     * @param string $path_source_file Ruta a archivo en maquina
     * @param string|stream $body Contenido del archivo a subir.
     * @return boolean
     */
    function upload_file_s3($path_bucket, $path_source_file, $body = null)
    {
        try {
            $s3 = AWS::get('s3');
            $bucket_name = Config::get('app.aws.bucket_name');
            $cloudfront_url = Config::get('app.aws.cloudfront_url');

            $data = array(
                'Bucket' => $bucket_name,
                'Key' => $path_bucket,
            );

            if ($body !== null) {
                $data['Body'] = $body;
            } else {
                $finfo = finfo_open(FILEINFO_MIME_TYPE);
                $mime_type = finfo_file($finfo, $path_source_file);
                finfo_close($finfo);

                $data['SourceFile'] = $path_source_file;
                $data['ContentType'] = $mime_type;
            }

            $result = $s3->putObject($data);
            if (isset($result['RequestCharged'])) {
                return $cloudfront_url.'/'.$path_bucket;
            }
            return false;
        } catch (S3Exception $e) {
            die($e->getMessage());
        } catch (AwsException $e) {
            echo $e->getAwsRequestId() . "\n";
            echo $e->getAwsErrorType() . "\n";
            echo $e->getAwsErrorCode() . "\n";
            exit;
        }
    }
}

if (!function_exists('remove_file_s3'))
{
    /**
     * Eliminar archivo de S3 AWS
     *
     * @param string $path Ruta a archivo
     */
    function remove_file_s3($path)
    {
        try{
            $s3 = AWS::get('s3');
            $bucket_name = Config::get('app.aws.bucket_name');
            $cloudfront_url = Config::get('app.aws.cloudfront_url');

            $path = str_replace($cloudfront_url, '', $path);
            if ($s3->doesObjectExist($bucket_name, $path))
            {
                $result = $s3->deleteObject(array(
                    'Bucket' => $bucket_name,
                    'Key' => $path,
                ));

                if (isset($result['RequestCharged']))
                {
                    $client = AWS::get('CloudFront');
                    $result = $client->createInvalidation(array(
                        'DistributionId' => Config::get('app.aws.distribution_id'),
                        'Paths' => array(
                            'Quantity' => 1,
                            'Items' => array($path),
                        ),
                        'CallerReference' => $path,
                    ));
                }
            }
        } catch (S3Exception $e) {
            die($e->getMessage());
        } catch (AwsException $e) {
            echo $e->getAwsRequestId() . "\n";
            echo $e->getAwsErrorType() . "\n";
            echo $e->getAwsErrorCode() . "\n";
            exit;
        }
    }
}

if (!function_exists('remove_folder_s3'))
{
    /**
     * @description : Elimina una carpeta de primer nivel en la raiz del bucket
     *
     * @param $path
     */
    function remove_folder_s3($path)
    {
        $s3 = Aws\S3\S3Client::factory(array(
            'key'    => Config::get('app.aws.key'),
            'secret' => Config::get('app.aws.secret'),
        ));
        //$result = $s3->getIterator('ListObjects', array('Bucket' => 'merqueo-dev'));
        $s3->deleteMatchingObjects(Config::get('app.aws.bucket_name'), $path);
        //return $result;
    }
}

if (!function_exists('remove_file'))
{
    /**
     * Eliminar archivo
     *
     * @param string $file Ruta a archivo
     * @return boolean
     */
    function remove_file($file)
    {
        if (file_exists($file)) {
            if (@unlink($file))
                return true;
        }
        return false;
    }
}

if (!function_exists('move_file'))
{
    /**
     * Mover archivo
     *
     * @param string $file_path Ruta a archivo
     * @param string $file Nueva ruta
     * @return boolean
     */
    function move_file($file_path, $new_file_path)
    {
        if (file_exists($file_path)){
            if (@rename($file_path, $new_file_path))
                return true;
        }
        return false;
    }
}

if (!function_exists('get_size_image'))
{
    /**
     * Ajusta tamaño de imagen para mantener proporcion
     *
     * @param string $filename Nombre del archivo
     * @param int $max_width Maximo ancho
     * @param int $max_height Maximo alto
     * @return array $size Datos ddel archivo
     */
    function get_size_image($filename, $max_width, $max_height)
    {
        $file = getimagesize($filename);
        list($orig_width, $orig_height) = $file;
        $width = $orig_width;
        $height = $orig_height;
        # taller
        if ($height > $max_height) {
            $width = ($max_height / $height) * $width;
            $height = $max_height;
        }
        # wider
        if ($width > $max_width) {
            $height = ($max_width / $width) * $height;
            $width = $max_width;
        }

        return $size = array('width' => $width, 'height' => $height);
    }
}

if (!function_exists('send_mail'))
{
    /**
     * Envia mail
     *
     * @param array $mail Parametros de mail
     * @param bool|array $attachments
     * @param bool $throw_exception
     * @return boolean Resultado
     */
    function send_mail($mail, $attachments = false, $throw_exception = false, $from_email = null)
    {
        try {
            $vars = array(
                'website_name' => Config::get('app.website_name'),
                'user_url' => url('/mi-cuenta'),
                'contact_email' => Config::get('app.admin_email'),
                'contact_phone' => Config::get('app.admin_phone'),
                'android_url' => Config::get('app.android_url'),
                'ios_url' => Config::get('app.ios_url'),
                'facebook_url' => Config::get('app.facebook_url'),
                'twitter_url' => Config::get('app.twitter_url')
            );
            $vars = array_merge($vars, $mail['vars']);

            $html = View::make($mail['template_name'], $vars)->render();

            if (!Config::get('app.debug'))
            {
                //UTILIZA GUZZLE 6.0
                $client = new GuzzleHttp\Client();
                $path = Config::get('app.mandrill.url');
                $receiver = [];

                foreach($mail['to'] as $to) {
                    if (!empty($to['email']) && !stristr($to['email'], '@surtifruver.com')){
                        $receiver_temp['email'] = $to['email'];
                        $receiver_temp['name'] = $to['name'];
                        $receiver_temp['type'] = 'to';
                        array_push($receiver, $receiver_temp);
                    }
                }

                if (count($receiver))
                {
                    $from_email = !empty($from_email) ? $from_email : Config::get('app.admin_email');
                    $email_data = [
                        'json' => [
                            'key' => Config::get('app.mandrill.key'),
                            'message' => [
                                'html' => $html,
                                'subject' => $mail['subject'],
                                'from_email' => $from_email,
                                'from_name' => Config::get('app.website_name'),
                                'to' => $receiver
                            ],
                            'async' => false,
                            'ip_pool' => null,
                            'send_at' => null
                        ]
                    ];
                    if ( $attachments ) {
                        foreach ($attachments as $key => $attachment) {
                            $email_data['json']['message']['attachments'][] = $attachment;
                            /*$email_data['json']['message']['attachments'][$key]['type'] = $attachment['type'];
                            $email_data['json']['message']['attachments'][$key]['name'] = $attachment['name'];
                            $email_data['json']['message']['attachments'][$key]['content'] = $attachment['content'];*/
                        }
                    }
                    $response = $client->post($path, $email_data);

                    $body = $response->getBody()->getContents();
                    $body = json_decode($body);

                    if (isset($body[0]) && !($body[0]->status === 'rejected' || $body[0]->status === 'invalid')) {
                        return true;
                    }

                    throw new Exception("Error while sending mail. Status: {$body[0]->status}. Reason: {$body[0]->reject_reason}");
                }
            }
            return false;

        } catch (\Exception $exception) {
            if ($throw_exception) {
                throw $exception;
            }

            return false;
        }
    }
}

if (!function_exists('send_query_execution_report'))
{
    /**
     * Envía un correo notificando un log de la ejecución de la tarea $taskName
     * 
     * @param string $task_name
     * @param array $log
     * @param boolean $error
     * 
     * @return boolean
     * 
     */
    function send_query_execution_report($task_name, $log, $error = false){

        $mailing_list   = Config::get('app.bi_query_report_mailing_list');
        $emails         = array_map(
                            function($e){
                                return trim($e);
                            },
                            explode(",",$mailing_list)
                        );

        if(count($emails) == 0){
            return false;
        }

        foreach($emails as $index => $email){
            $mail['to'][$index]['email']    = $email;
            $mail['to'][$index]['name']     = "Interesado";
        }

        $now                    = date("Y-m-d H:i:s");
        $mail['template_name']  = 'emails.daily_report';
        $mail['subject']        = "Query execution report [".($error ? " FAIL " : " SUCCESS ")."][ $task_name ][ $now ]";
        $mail['vars']['title']  = "Reporte de ejecución de query '$task_name'";
        $msg                    = "LOG:<br>";
        $msg                    .= implode("<br>",$log);
        $msg                    = str_replace("\n", "<br>", $msg);

        $mail['vars']['html']   = $msg;
        return send_mail($mail);
    }
}

if (!function_exists('send_sms'))
{
    /**
     * Enviar mensaje de texto
     *
     * @param  string $to Numero celular
     * @param  string $message Mensaje a enviar
     * @param  string $schedule_date Fecha de envio de mensaje programado
     * @return boolean Resultado
     */
    function send_sms($to, $message, $schedule_date = null)
    {
        if (Config::get('app.debug')) {

            return true;
        }

        try {
            // si el número de celular no tiene el código del país, lo añado
            $to = starts_with($to, '+') ? $to : "+57{$to}";
            \App::make(\contracts\SmsService::class)->send($to, $message);
        } catch (\Exception $e) {
            \Log::error('Could not send SMS notification. Service response: ' . $e->getMessage());
        }
        return false;
    }
}

if (!function_exists('get_name_from_number'))
{
    /**
     * Retorna letras corrrespondiente a numero
     *
     * @param int $num
     * @return string
     */
    function get_name_from_number($num)
    {
        $numeric = $num % 26;
        $letter = chr(65 + $numeric);
        $num2 = intval($num / 26);
        if ($num2 > 0) {
            return get_name_from_number($num2 - 1) . $letter;
        } else {
            return $letter;
        }
    }
}

if (!function_exists('get_delivery_date_marketplace')) {
    /**
     * Suma horas a fecha y devuelve fecha sin tener en cuenta fin de semana y festivos
     *
     * @param \Carbon\Carbon $temporal_date
     * @param $hours
     * @return \Carbon\Carbon
     */
    function get_delivery_date_marketplace(\Carbon\Carbon $temporal_date, $hours)
    {
        $date = clone $temporal_date;

        $days = ceil($hours / \Carbon\Carbon::HOURS_PER_DAY);
        $next_available_date = add_available_days(
            \Carbon\Carbon::create(null, null, null, $date->hour),
            $days
        );

        if ($next_available_date <= $date) {
            return $date;
        }

        $days -= Carbon\Carbon::create()->diffInDays($date);

        for ($i = 0; $i < $days || $next_available_date > $date; $i++) {
            $date->addDay();
        }

        return $date;
    }
}

if(!function_exists('add_available_days'))
{
    /**
     * Recalcula la fecha en caso de que caiga Sábado, domingo o festivo
     *
     * @param \Carbon\Carbon $date
     * @param int $days
     * @return \Carbon\Carbon
     */
    function add_available_days(\Carbon\Carbon $date, $days = 1)
    {
        $temporal_date = clone $date;
        $festivo = new Holidays();
        // User carbon permite usar fechas de prueba con esta clase.
        $festivo->festivos(\Carbon\Carbon::create()->year);

        for ($i = 0; $i < $days; $i++) {
            $temporal_date->addDay();
            if (!$festivo->esDiaHabil($temporal_date->day, $temporal_date->month)) {
                $days++;
            }
        }

        return $temporal_date;
    }
}

if (!function_exists('format_date'))
{
    /**
     * Cambia formato a la fecha
     *
     * @param string $format Formato a retornar
     * @param string $date Fecha a cambiar
     * @return string $date Fecha con nuevo formato
     */
    function format_date($format, $date)
    {
        if (!empty($date) && $date != '0000-00-00' && $date != '0000-00-00 00:00:00')
        {
            switch($format)
            {
                //de mysql a 01/04/2008
                case 'normal':
                    $date = explode('-',$date);
                    $date = $date[2].'/'.$date[1].'/'.$date[0];
                break;

                //de mysql con hora a 01/04/2008 10:34 am
                case 'normal_with_time':
                    $date=explode(' ',$date);
                    $hora=explode(':',$date[1]);
                    if (!isset($hora[2])) $hora[2] = '00';
                    $hora=date('g:i a',mktime($hora[0],$hora[1],$hora[2],0,0,0));
                    $date=explode('-',$date[0]);
                    $date=$date[2].'/'.$date[1].'/'.$date[0].' '.$hora;
                break;

                //de mysql con hora a 01/04/2008 10:34
                case 'normal_with_time_without_seconds':
                    $date=explode(' ',$date);
                    $hora=explode(':',$date[1]);
                    $hora=date('H:i',mktime($hora[0],$hora[1],$hora[2],0,0,0));
                    $date=explode('-',$date[0]);
                    $date=$date[2].'/'.$date[1].'/'.$date[0].' '.$hora;
                break;

                //de 01/04/2008 22:00:00 a mysql con hora
                case 'mysql_with_time':
                    $date = explode(' ',$date);
                    $hora = $date[1];
                    $date = explode('/', $date[0]);
                    $date = $date[2].'-'.$date[1].'-'.$date[0].' '.$hora.':00';
                break;

                //de mysql con hora a 12/03/2008
                case 'normal_without_time':
                    $date=explode('-', $date);
                    $date=substr($date[2],0,2).'/'.$date[1].'/'.$date[0];
                break;

                //de 01/04/2008 a mysql
                case 'mysql':
                    $date = explode('/', $date);
                    $date = $date[2].'-'.$date[1].'-'.$date[0];
                break;

                //de mysql a Miercoles, 12 Marzo de 2008
                case 'normal_long';
                    $date = explode('-', $date);
                    $dias  = array('Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado');
                    $meses = array('','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
                    $date = $dias[date('w',mktime(0,0,0,$date[1],$date[2],$date[0]))].', '.intval($date[2]).' '.$meses[intval($date[1])].' de '.$date[0];
                break;

                //de mysql con hora a Miercoles, 12 de Marzo de 2008 a las 12:30 PM
                case 'normal_long_with_time';
                    $date = explode(' ',$date);
                    $hora = explode(':',$date[1]);
                    if (!isset($hora[2])) $hora[2] = '00';
                    $hora = date('g:i a',mktime($hora[0],$hora[1],$hora[2],0,0,0));
                    $date = explode('-', $date[0]);
                    $dias = array('Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado');
                    $meses = array('','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
                    $date = $dias[date('w',mktime(0,0,0,$date[1],$date[2],$date[0]))].', '.intval($date[2]).' '.$meses[intval($date[1])].' de '.$date[0].' a las '.$hora;
                break;

                //de mysql con hora a 12 Marzo
                case 'short_with_moth_name';
                    $date = explode(' ',$date);
                    $date = explode('-', $date[0]);
                    $meses = ['','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
                    $date = intval($date[2]) . ' ' . $meses[intval($date[1])];
                break;

                //de mysql con hora a el miercoles a las 12:30 PM
                case 'normal_day_with_time';
                    $date = explode(' ',$date);
                    $hora = explode(':',$date[1]);
                    if (!isset($hora[2])) $hora[2] = '00';
                    $hora = date('g:i a',mktime($hora[0],$hora[1],$hora[2],0,0,0));
                    $date = explode('-', $date[0]);
                    $dias = array('Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado');
                    if (date('w', mktime(0,0,0,$date[1],$date[2],$date[0])) == date('w', strtotime('+1 days')))
                        $day = 'mañana';
                    else if (date('w', mktime(0,0,0,$date[1],$date[2],$date[0])) == date('w'))
                        $day = 'hoy';
                    else $day = 'el '.$dias[date('w',mktime(0,0,0,$date[1],$date[2],$date[0]))];
                    //$date = $day.' a las '.$hora;
                    $date = $day;
                break;

                //de mysql a 12 Mar 2008
                case 'normal_short':
                    $meses = array('01'=>'Ene','02'=>'Feb','03'=>'Mar','04'=>'Abr','05'=>'May','06'=>'Jun','07'=>'Jul','08'=>'Ago','09'=>'Sep','10'=>'Oct','11'=>'Nov','12'=>'Dic');
                    $array = explode('-', $date);
                    if ($array[2][0] == '0') $array[2] = substr($array[2],1,1);
                    $date = $array[2].' '.$meses[$array[1]].' '.$array[0];
                break;

                //de mysql a 12 Mar 2008
                case 'normal_short_with_time':
                    $meses = array('01'=>'Ene','02'=>'Feb','03'=>'Mar','04'=>'Abr','05'=>'May','06'=>'Jun','07'=>'Jul','08'=>'Ago','09'=>'Sep','10'=>'Oct','11'=>'Nov','12'=>'Dic');
                    $date = explode(' ',$date);
                    $hora = explode(':',$date[1]);
                    if (!isset($hora[2])) $hora[2] = '00';
                    $hora = date('g:i a',mktime($hora[0],$hora[1],$hora[2],0,0,0));
                    $date = explode('-', $date[0]);
                    if ($date[2][0] == '0') $date[2] = substr($date[2], 1, 1);
                    $date = $date[2].' '.$meses[$date[1]].' '.$date[0].' '.$hora;
                break;

                //de mysql a Mañana, Hoy, Lunes o Miercoles
                case 'day_name':
                    $date = date('Y-m-d', strtotime($date));
                    if ($date == date('Y-m-d'))
                        return 'Hoy';
                    $tomorrow = date('Y-m-d', strtotime(' + 1 days'));
                    if ($date == $tomorrow)
                        return 'Mañana';
                    $date = explode('-', $date);
                    $dias  = array('Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado');
                    $date = $dias[date('w', mktime(0,0,0,$date[1],$date[2],$date[0]))];
                break;
            }
        }
        return $date;
    }
}

if (!function_exists('point_in_polygon')) {
    /**
     * @param $points_string
     * @param $pLat
     * @param $pLng
     * @return int
     */
    function point_in_polygon($points_string, $pLat, $pLng) {
        $isInZone = 0;

        $points_array = explode(',', $points_string);
        $points = array();

        for ($i=0; $i<sizeof($points_array); $i++) {
            $arrayLatLng = explode(' ', $points_array[$i]);
            if(sizeof($arrayLatLng) == 2) {
                $points[$i][0] = $arrayLatLng[0];
                $points[$i][1] = $arrayLatLng[1];
            }
        }

        $j = sizeof($points)-1;

        for($i = 0; $i<sizeof($points); $j = $i++)  {
            if (((($points[$i][1] <= $pLng) && ($pLng < $points[$j][1])) || (($points[$j][1] <= $pLng) && ($pLng < $points[$i][1]))) &&
                ($pLat < ($points[$j][0] - $points[$i][0]) * ($pLng - $points[$i][1]) / ($points[$j][1] - $points[$i][1]) + $points[$i][0]))
                $isInZone++;
        }

        return $isInZone%2;
    }
}


if (!function_exists('get_config_option'))
{
    /**
     * Obtener valor para los elementos de configuracion dependiendo el tipo
     *
     * @param string $type Tipo de variable a obtener
     * @param $city_id
     * @return string $type Valor de variable
     */
    function get_config_option($type,  $city_id)
    {
        return Configuration::where('type', $type)->where('warehouse_id', $city_id)->pluck('value');
    }
}

if (!function_exists('get_config'))
{
    /**
     * Obtener valor para los elementos de configuracion dependiendo el key
     *
     * @param string $key cariable a obtener
     * @param $city_id
     * @return string $type Valor de variable
     */
    function get_config($key, $city_id = 0)
    {
        return Configuration::where('key', $key)->where('warehouse_id', $city_id)->pluck('value');
    }
}

if (!function_exists('generate_reference'))
{
    /**
     * Genera codigo unico para pedido
     *
     * @return string $reference
     */
    function generate_reference()
    {
        $reference = '';

        srand((double)microtime()*1000000);

        $data = 'A1BC4DE123IJKL3M5N67QRST5UV5WX4YZ';
        $data .= 'A3BC3D25EF4GHI7JK0l8MN123OPQ45RS67TU8V89WX2YZ';
        $data .= '0FGH45OP89';

        for($i = 0; $i <= 5; $i++)
            $reference .= substr($data, (rand()%(strlen($data))), 1);

        //$reference .= '-'.date('YmdHis');
        $reference .= time();

        return $reference;
    }
}

if (!function_exists('generate_coupon_code'))
{
    /**
     * Genera codigo unico para cupon de descuento
     *
     * @return string $code
     */
    function generate_coupon_code()
    {
        $code = '';

        srand((double)microtime()*1000000);

        $data = 'A1BC4DE123IJKL3M5N67QRST5UV5WX4YZ';
        $data .= 'A3BC3D25EF4GHI7JK0L8MN123OPQ45RS67TU8V89WX2YZ';
        $data .= '0FGH45OP89';

        for($i = 0; $i <= 7; $i++)
            $code .= substr($data, (rand()%(strlen($data))), 1);

        return $code;
    }
}

if (!function_exists('get_current_request'))
{
    /**
     * Obtiene el nombre del action y controller actual
     *
     * @return array $request Datos de action y controller
     */
    function get_current_request()
    {
        $request = array('controller' => 'undefined', 'action' => 'undefined');
        if (Route::getCurrentRoute()){
            $data = explode('@', Route::getCurrentRoute()->getActionName());
            if (count($data) == 2){
                $request['controller'] = strtolower(str_replace('Controller', '', $data[0]));
                $request['action'] = $data[1];
            }
        }
        return $request;
    }
}

if (!function_exists('get_card_type'))
{
    /**
    * Return tipo de tarjeta de crédito si el numero es válido
    * @return string
    * @param $number string
    **/
    function get_card_type($number)
    {
        $number = preg_replace('/[^\d]/','',$number);
        if (preg_match('/^3[47][0-9]{13}$/',$number)){ return 'AMEX'; }
        elseif (preg_match('/^3(?:0[0-5]|[68][0-9])[0-9]{11}$/',$number)){ return 'DINERS'; }
        elseif (preg_match('/^5[1-5][0-9]{14}$/',$number)){ return 'MASTERCARD'; }
        elseif (preg_match('/^4[0-9]{12}(?:[0-9]{3})?$/',$number)){ return 'VISA'; }
        elseif (substr($number, 0, 6) == 590712){ return 'CODENSA'; }
        else{ return 'Unknown'; }
    }
}

if (!function_exists('get_time'))
{
    /**
     * Obtiene el timepo restante o que ha pasado en dias horas y minutos con base a fecha actual
     *
     * @param string $action Tipo de calculo
     * @param string $date Fecha
     * @param int $store_delivery_time_minutes Minutos a sumar
     * @param int $for_orders Texto para tiempo de entrega de pedidos
     * @return string Diferencia de tiempo
     */
    function get_time($action, $date, $store_delivery_time_minutes = null, $for_orders = true)
    {
        $start_date = new DateTime($date);
        $current_date = new DateTime();
        if ($store_delivery_time_minutes && $store_delivery_time_minutes < 1000){
            $store_delivery_time_minutes = round((2 * $store_delivery_time_minutes) / 3, 0);
            $start_date->modify('-'.$store_delivery_time_minutes.' minutes');
        }
        $countdown = $current_date->diff($start_date)->format('%a/%h/%i/%s');
        $countdown = explode('/', $countdown);
        $time = '';
        if ($countdown[0]) $time = $countdown[0].' días ';
        if ($countdown[1]) $time .= $countdown[1].' horas ';
        if ($countdown[2]) $time .= $countdown[2].' minutos ';
        $time = trim($time);

        if ($action == 'left'){
            if ($for_orders){
                if (date('Y-m-d H:i:s') > $start_date->format('Y-m-d H:i:s'))
                    return '<span class="font-red">Incumplido hace '.$time.'</span>';
                else return '<span class="font-green">Quedan '.$time.'</span>';
            }else{
                 $time .= ' '.$countdown[3].' segundos';
                 return $time;
            }
        }

        return $time;
    }
}

if (!function_exists('get_time_text'))
{
    /**
     * Obtener tiempo en texto
     *
     * @param int $time Minutos
     * @return string $time Tiempo en texto
     */
    function get_time_text($time)
    {
        if ($time < 1) {
            return;
        }
        $hours = floor($time / 60);
        $minutes = ($time % 60);

        if ($hours)
            $time = $hours.' horas '.$minutes.' minutos';
        else $time = $minutes.' minutos';

        return $time;
    }
}

if (!function_exists('get_ip'))
{
    /**
     * Obtiene IP del host
     *
     * @return string IP del host
     */
    function get_ip() {
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } elseif (isset($_SERVER['REMOTE_ADDR'])) {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        if (isset($ip)) {
            $data = explode(', ', $ip);

            if (count($data) > 1) {
                $ip = $data[0];
            }

            return $ip;
        }

        return '';
    }
}

if (!function_exists('private_ip')) {
    /**
     * Retorna la IP Privada del servidor
     *
     * @return string
     */
    function private_ip() {
        return gethostbyname(gethostname());
    }
}

if (!function_exists('validate_date'))
{
    /**
     * Valida formato y valor de una fecha
     *
     * @param string $date Fecha
     * @return boolean
     */
    function validate_date($date)
    {
        $date = explode('-', $date);
        if (count($date) == 3 && checkdate($date[1], $date[2], $date[0]))
            return true;
        return false;
    }
}

if (!function_exists('debug'))
{
    /**
     * Imprime variable
     */
    function debug($var, $var_dump = false)
    {
        echo '<pre>';
        if (!$var_dump)
           print_r($var);
        else var_dump($var);
        exit;
    }
}

if (!function_exists('lev'))
{
    /**
     * Algorimo levistein
     */
    function lev($s,$t) {
        $m = strlen($s);
        $n = strlen($t);

        for($i=0;$i<=$m;$i++) $d[$i][0] = $i;
        for($j=0;$j<=$n;$j++) $d[0][$j] = $j;

        for($i=1;$i<=$m;$i++) {
            for($j=1;$j<=$n;$j++) {
                $c = ($s[$i-1] == $t[$j-1])?0:1;
                $d[$i][$j] = min($d[$i-1][$j]+1,$d[$i][$j-1]+1,$d[$i-1][$j-1]+$c);
            }
        }

        return $d[$m][$n];
    }
}

if (!function_exists('get_plural_string'))
{
    /**
     * Obtiene singular de una palabra
     */
    function get_plural_string($word = '')
    {
        $word = strtolower($word);
        $vocales = ['a','e','i','o','u'];
        $str_final_letter = substr($word, -1);
        $str_final_letter_cut = substr($word, 0, -1);
        $str_final_letter_from = substr($word, -2, 1);
        $str_final_letter_2 = substr($word, -2);
        $str_final_letter_2_cut = substr($word, 0, -2);
        $str_final_letter_3 = substr($word, -3);
        $str_final_letter_3_cut = substr($word, 0, -3);
        $str_final_letter_from_3 = substr($word, -3, 1);
        $final_string = '';
        if ( $str_final_letter == 's' ) {
            if ( $str_final_letter_3 == 'ces' ) {
                $final_string = $str_final_letter_3_cut.'z';
            }elseif ( $str_final_letter_2 == 'es' ) {
                if ( $str_final_letter_from_3 == 'l' || $str_final_letter_from_3 == 'n' ) {
                    $final_string = $str_final_letter_2_cut;
                }else{
                    $final_string = $str_final_letter_cut;
                }
            }else {
                if ( in_array($str_final_letter_from , $vocales) ) {
                    $final_string = $str_final_letter_cut;
                }
            }
        }
        if ( !empty($final_string) )
            return $final_string;
        return $word;
    }
}

if (!function_exists('get_clean_string'))
{
    /**
     * Elimina caracteres especiales
     */
    function get_clean_string($q)
    {
       return trim(preg_replace('/[^A-Za-z0-9-ñ-Ñ-á-é-í-ó-ú-Á-É-Í-Ó-Ú\-]/', ' ', $q));
    }
}


if (!function_exists('getallheaders'))
{
    /**
     * Funcion para NGINX
     */
    function getallheaders()
    {
        $headers = [];
        foreach ($_SERVER as $name => $value) {
            if (substr($name, 0, 5) == 'HTTP_') {
                $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
            }
        }
        return $headers;
    }
}

if (!function_exists('convert_number_to_word'))
{
    /**
     * Funcion para convertir numero en letras
     *
     * @param int $xcifra Numero a convertir
     */
    function convert_number_to_word($xcifra)
    {
        $xarray = array(0 => "Cero",
            1 => "UN", "DOS", "TRES", "CUATRO", "CINCO", "SEIS", "SIETE", "OCHO", "NUEVE",
            "DIEZ", "ONCE", "DOCE", "TRECE", "CATORCE", "QUINCE", "DIECISEIS", "DIECISIETE", "DIECIOCHO", "DIECINUEVE",
            "VEINTI", 30 => "TREINTA", 40 => "CUARENTA", 50 => "CINCUENTA", 60 => "SESENTA", 70 => "SETENTA", 80 => "OCHENTA", 90 => "NOVENTA",
            100 => "CIENTO", 200 => "DOSCIENTOS", 300 => "TRESCIENTOS", 400 => "CUATROCIENTOS", 500 => "QUINIENTOS", 600 => "SEISCIENTOS", 700 => "SETECIENTOS", 800 => "OCHOCIENTOS", 900 => "NOVECIENTOS"
        );
    //
        $xcifra = trim($xcifra);
        $xlength = strlen($xcifra);
        $xpos_punto = strpos($xcifra, ".");
        $xaux_int = $xcifra;
        $xdecimales = "00";
        if (!($xpos_punto === false)) {
            if ($xpos_punto == 0) {
                $xcifra = "0" . $xcifra;
                $xpos_punto = strpos($xcifra, ".");
            }
            $xaux_int = substr($xcifra, 0, $xpos_punto); // obtengo el entero de la cifra a covertir
            $xdecimales = substr($xcifra . "00", $xpos_punto + 1, 2); // obtengo los valores decimales
        }

        $XAUX = str_pad($xaux_int, 18, " ", STR_PAD_LEFT); // ajusto la longitud de la cifra, para que sea divisible por centenas de miles (grupos de 6)
        $xcadena = "";
        for ($xz = 0; $xz < 3; $xz++) {
            $xaux = substr($XAUX, $xz * 6, 6);
            $xi = 0;
            $xlimite = 6; // inicializo el contador de centenas xi y establezco el límite a 6 dígitos en la parte entera
            $xexit = true; // bandera para controlar el ciclo del While
            while ($xexit) {
                if ($xi == $xlimite) { // si ya llegó al límite máximo de enteros
                    break; // termina el ciclo
                }

                $x3digitos = ($xlimite - $xi) * -1; // comienzo con los tres primeros digitos de la cifra, comenzando por la izquierda
                $xaux = substr($xaux, $x3digitos, abs($x3digitos)); // obtengo la centena (los tres dígitos)
                for ($xy = 1; $xy < 4; $xy++) { // ciclo para revisar centenas, decenas y unidades, en ese orden
                    switch ($xy) {
                        case 1: // checa las centenas
                            if (substr($xaux, 0, 3) < 100) { // si el grupo de tres dígitos es menor a una centena ( < 99) no hace nada y pasa a revisar las decenas

                            } else {
                                $key = (int) substr($xaux, 0, 3);
                                if (TRUE === array_key_exists($key, $xarray)){  // busco si la centena es número redondo (100, 200, 300, 400, etc..)
                                    $xseek = $xarray[$key];
                                    $xsub = suffix($xaux); // devuelve el subfijo correspondiente (Millón, Millones, Mil o nada)
                                    if (substr($xaux, 0, 3) == 100)
                                        $xcadena = " " . $xcadena . " CIEN " . $xsub;
                                    else
                                        $xcadena = " " . $xcadena . " " . $xseek . " " . $xsub;
                                    $xy = 3; // la centena fue redonda, entonces termino el ciclo del for y ya no reviso decenas ni unidades
                                }
                                else { // entra aquí si la centena no fue numero redondo (101, 253, 120, 980, etc.)
                                    $key = (int) substr($xaux, 0, 1) * 100;
                                    $xseek = $xarray[$key]; // toma el primer caracter de la centena y lo multiplica por cien y lo busca en el arreglo (para que busque 100,200,300, etc)
                                    $xcadena = " " . $xcadena . " " . $xseek;
                                } // ENDIF ($xseek)
                            } // ENDIF (substr($xaux, 0, 3) < 100)
                            break;
                        case 2: // checa las decenas (con la misma lógica que las centenas)
                            if (substr($xaux, 1, 2) < 10) {

                            } else {
                                $key = (int) substr($xaux, 1, 2);
                                if (TRUE === array_key_exists($key, $xarray)) {
                                    $xseek = $xarray[$key];
                                    $xsub = suffix($xaux);
                                    if (substr($xaux, 1, 2) == 20)
                                        $xcadena = " " . $xcadena . " VEINTE " . $xsub;
                                    else
                                        $xcadena = " " . $xcadena . " " . $xseek . " " . $xsub;
                                    $xy = 3;
                                }
                                else {
                                    $key = (int) substr($xaux, 1, 1) * 10;
                                    $xseek = $xarray[$key];
                                    if (20 == substr($xaux, 1, 1) * 10)
                                        $xcadena = " " . $xcadena . " " . $xseek;
                                    else
                                        $xcadena = " " . $xcadena . " " . $xseek . " Y ";
                                } // ENDIF ($xseek)
                            } // ENDIF (substr($xaux, 1, 2) < 10)
                            break;
                        case 3: // checa las unidades
                            if (substr($xaux, 2, 1) < 1) { // si la unidad es cero, ya no hace nada

                            } else {
                                $key = (int) substr($xaux, 2, 1);
                                $xseek = $xarray[$key]; // obtengo directamente el valor de la unidad (del uno al nueve)
                                $xsub = suffix($xaux);
                                $xcadena = " " . $xcadena . " " . $xseek . " " . $xsub;
                            } // ENDIF (substr($xaux, 2, 1) < 1)
                            break;
                    } // END SWITCH
                } // END FOR
                $xi = $xi + 3;
            } // ENDDO

            if (substr(trim($xcadena), -5, 5) == "ILLON") // si la cadena obtenida termina en MILLON o BILLON, entonces le agrega al final la conjuncion DE
                $xcadena.= " DE";

            if (substr(trim($xcadena), -7, 7) == "ILLONES") // si la cadena obtenida en MILLONES o BILLONES, entoncea le agrega al final la conjuncion DE
                $xcadena.= " DE";

            // ----------- esta línea la puedes cambiar de acuerdo a tus necesidades o a tu país -------
            if (trim($xaux) != "") {
                switch ($xz) {
                    case 0:
                        if (trim(substr($XAUX, $xz * 6, 6)) == "1")
                            $xcadena.= "UN BILLON ";
                        else
                            $xcadena.= " BILLONES ";
                        break;
                    case 1:
                        if (trim(substr($XAUX, $xz * 6, 6)) == "1")
                            $xcadena.= "UN MILLON ";
                        else
                            $xcadena.= " MILLONES ";
                        break;
                    case 2:
                        if ($xcifra < 1) {
                            $xcadena = "CERO PESOS";
                        }
                        if ($xcifra >= 1 && $xcifra < 2) {
                            $xcadena = "UN PESO  ";
                        }
                        if ($xcifra >= 2) {
                            $xcadena.= " PESOS  "; //
                        }
                        break;
                } // endswitch ($xz)
            } // ENDIF (trim($xaux) != "")
            // ------------------      en este caso, para México se usa esta leyenda     ----------------
            $xcadena = str_replace("VEINTI ", "VEINTI", $xcadena); // quito el espacio para el VEINTI, para que quede: VEINTICUATRO, VEINTIUN, VEINTIDOS, etc
            $xcadena = str_replace("  ", " ", $xcadena); // quito espacios dobles
            $xcadena = str_replace("UN UN", "UN", $xcadena); // quito la duplicidad
            $xcadena = str_replace("  ", " ", $xcadena); // quito espacios dobles
            $xcadena = str_replace("BILLON DE MILLONES", "BILLON DE", $xcadena); // corrigo la leyenda
            $xcadena = str_replace("BILLONES DE MILLONES", "BILLONES DE", $xcadena); // corrigo la leyenda
            $xcadena = str_replace("DE UN", "UN", $xcadena); // corrigo la leyenda
        } // ENDFOR ($xz)
        return trim($xcadena);
    }
}

if (!function_exists('suffix'))
{
    /**
     * Funcion para convert_number_to_word
     */
    function suffix($xx)
    {
        $xx = trim($xx);
        $xstrlen = strlen($xx);
        if ($xstrlen == 1 || $xstrlen == 2 || $xstrlen == 3)
            $xsub = "";
        if ($xstrlen == 4 || $xstrlen == 5 || $xstrlen == 6)
            $xsub = "MIL";
        return $xsub;
    }
}

if (!function_exists('strtoupper_utf8'))
    {
    /**
    * Convierte cadena minuscula a mayuscula
    */
    function strtoupper_utf8($string)
    {
        $string = mb_strtoupper($string);
        $string = utf8_decode($string);
        return $string;
    }
}

if (!function_exists('get_delivery_time'))
{
    /**
     * Calcula diferencia de horas entre 2 fechas
     *
     * @param string $time_begin Fecha inicial
     * @param string $time_end Fecha final
     * @return string $delivery_time
     */
    function get_delivery_time($time_begin, $time_end)
    {
         $hour_begin = new DateTime($time_begin);
         $hour_end = new DateTime($time_end);
         $minutes = date_diff( $hour_begin, $hour_end, ! false);
         $minutes = (($minutes->y * 365.25 + $minutes->m * 30 + $minutes->d) * 24 + $minutes->h) * 60 + $minutes->i + $minutes->s/60;
         $minutes = $minutes / 2;
         $delivery_time = $hour_begin->modify('+'.intval($minutes).' minutes');
         $delivery_time = $delivery_time->format('H:i:s');
         return $delivery_time;
    }
}

if (!function_exists('send_notification'))
{
    /**
     * Determina el método a usar para el envio de
     * las notificaciones push dependiendo del sistema operativo
     * y la versión de la aplicación.
     *
     * @param string $app_version
     * @param string $os
     * @param string|array $device_id
     * @param string $player_id
     * @param array $data
     * @return array
     */
    function send_notification($app_version, $os, $device_id, $player_id, $data)
    {
        if (empty($data['message']))
            return [
                'status' => false,
                'message' => 'Mensaje es requerido',
            ];

        $notifier = \Merqueo\Backend\MerqueoNotification::getSender($app_version, $os, $device_id, $player_id);
        $notification = new \Merqueo\Backend\MerqueoNotification();

        return $notification->send($notifier, $data);
    }
}

if (!function_exists('send_push_notification'))
{
    /**
     * Envia Push Notifications
     *
     * @param array $user_device_id ID del dispositivo
     * @param array $data Datos adicionales a enviar
     * @return array $result Resultado
     */
    function send_push_notification($user_device_id, $data)
    {
        ini_set('memory_limit', '3072M');
        set_time_limit(0);
        /*if (Config::get('app.debug'))
            return array(
                'status' => false,
                'message' => 'En modo debug no se envian notificaciones.'
            );*/

        $message = 'Ocurrió un error al enviar las notificaciones.';

        if (empty($data['message']))
            return array('status' => false, 'message' => 'El mensaje no puede estar vacio.');
        if (!$user_device_id)
            return array('status' => false, 'message' => 'IDs de dispositivos no especificados.');

        //enviar push normal
        if (is_numeric($user_device_id)){
            $device = UserDevice::select('id', 'regid', 'os')->where('id', $user_device_id)->orderBy('id')->first();
            if ($device){
                if ($device->os == 'Android' || strstr($device->regid, 'APA91'))
                    $registration_ids['android'][] = $device->regid;
                else{
                    if (strlen($device->regid) == 64)
                        $registration_ids['ios'][$device->id] = $device->regid;
                }
            }else return array('status' => false, 'message' => 'No se encontraron dispositivos.');
        }

        $error = $android_cont = $ios_cont = 0;

        //ANDROID
        if (isset($registration_ids['android']))
        {
            $url = Config::get('app.google_cloud_messaging.url');
            $fields = array(
                'registration_ids' => $registration_ids['android'],
                'data' => $data
            );
            $headers = array(
                'Authorization: key='.Config::get('app.google_cloud_messaging.key'),
                'Content-Type: application/json'
            );
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            $response = curl_exec($ch);
            if ($response === FALSE)
                $error = 'Failed to connect GCM.';
            curl_close($ch);
            $result = json_decode($response);
            if ($result && isset($result->success))
                $android_cont = $result->success;
            else {
                if (isset($result->results)){
                    foreach($result->results as $result){
                        $error = $result->error;
                    }
                }else{
                    if (strlen($response)){
                        $error = $response;
                    }
                }
            }

            if ($error)
                return array('status' => false, 'message' => $error);
        }

        //iOS
        if (isset($registration_ids['ios']))
        {
            //return array('status' => false, 'message' => 'No se envió ninguna notificación.');

            $gateway = Config::get('app.apple_push_notification_service.url');
            $certificate = Config::get('app.apple_push_notification_service.certificate');

            try{
                $ctx = stream_context_create();
                stream_context_set_option($ctx, 'ssl', 'local_cert', base_path().'/certificates/'.$certificate);
                stream_context_set_option($ctx, 'ssl', 'passphrase', 'vnkiyzmbnb');
                $fp = stream_socket_client(
                    $gateway, $err,
                    $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
                if (!$fp){
                    if ($android_cont){
                        if (isset($registration_ids['android']))
                            $message = 'Se enviaron '.$android_cont.' de Android y '.$ios_cont.' notificaciones de iOS. Error iOS: Failed to connect $err $errstr';
                        else $message = 'Se enviaron '.$ios_cont.' notificaciones de iOS. Error iOS: Failed to connect $err $errstr';
                        return array(
                            'status' => false,
                            'message' => $message,
                            'devices' => $android_cont
                        );
                    }else return array('status' => false, 'message' => 'Error iOS: Failed to connect APNS $err $errstr');
                }
                stream_set_blocking($fp, 0);
            }catch (Exception $e) {
                return array('status' => false, 'message' => 'No se envió ninguna notificación.');
            }

            $ios_error = $ios_cont = 0;
            $message = $data['message'];
            unset($data['message']);
            foreach($registration_ids['ios'] as $id => $token_device)
            {
                $payload = array(
                    'aps' => array(
                        'alert' => array('title' => 'Merqueo', 'body' => $message),
                        'sound' => 'default',
                        'badge' => 1
                    )
                );
                $payload = array_merge($payload, $data);
                $payload = json_encode($payload);

                try{
                   $inner = chr(1) . pack('n', 32) . pack('H*', $token_device)
                    . chr(2) . pack('n', strlen($payload)) . $payload
                    . chr(3) . pack('n', 4) . pack('N', $id)
                    . chr(4) . pack('n', 4) . pack('N', time() + 86400)
                    . chr(5) . pack('n', 1)
                    . chr(10);
                    $notification = chr(2) . pack('N', strlen($inner)) . $inner;
                }catch (Exception $e) {
                    //echo $e->getMessage(); exit;
                    continue;
                }

                try{
                    $result = fwrite($fp, $notification, strlen($notification));
                    usleep(500000);
                    if ($ios_error = check_apple_error_response($fp)){
                       fclose($fp);
                       break;
                    }
                    if ($result)
                        $ios_cont++;
                }catch (Exception $e) {
                    usleep(500000);
                    if ($ios_error = check_apple_error_response($fp))
                        $ios_error .= ' Token device: '.$token_device;
                    else $ios_error = ' Error iOS: Token device: '.$token_device;
                    fclose($fp);
                    break;
                }
            }
            if (is_resource($fp)){
                if (!$ios_error){
                    usleep(500000);
                    $ios_error = check_apple_error_response($fp);
                }
                fclose($fp);
            }

            if (isset($registration_ids['android']))
                $message = 'Se enviaron '.$android_cont.' de Android y '.$ios_cont.' notificaciones de iOS.';
            else $message = 'Se enviaron '.$ios_cont.' notificaciones de iOS.';

            if (!$ios_cont || $ios_error){
                if ($ios_error)
                    $message .= $ios_error;

                return array(
                    'status' => false,
                    'message' => $message,
                    'devices' => $ios_cont + $android_cont
                );
            }
        }

        $devices_cont = $android_cont + $ios_cont;

        if ($devices_cont)
            return array('status' => true, 'message' => 'Notificaciones enviadas con éxito.', 'devices' => $devices_cont);
        else return array('status' => false, 'message' => 'No se envió ninguna notificación.');
    }
}

if (!function_exists('check_apple_error_response'))
{
    /**
     * Verifica si hay un error en la respuesta de Apple
     *
     * @param object $fp
     * @return boolean
     */
    function check_apple_error_response($fp) {

       //byte1=always 8, byte2=StatusCode, bytes3,4,5,6=identifier(rowID). Should return nothing if OK.
       $apple_error_response = fread($fp, 6);

       if ($apple_error_response) {
            //unpack the error response (first byte 'command" should always be 8)
            $error_response = unpack('Ccommand/Cstatus_code/Nidentifier', $apple_error_response);

            if ($error_response['status_code'] == '0') {
                $error_response['status_code'] = '0-No errors encountered';
            } else if ($error_response['status_code'] == '1') {
                $error_response['status_code'] = 'Error iOS: 1-Processing error';
            } else if ($error_response['status_code'] == '2') {
                $error_response['status_code'] = '2-Missing device token';
            } else if ($error_response['status_code'] == '3') {
                $error_response['status_code'] = '3-Missing topic';
            } else if ($error_response['status_code'] == '4') {
                $error_response['status_code'] = '4-Missing payload';
            } else if ($error_response['status_code'] == '5') {
                $error_response['status_code'] = '5-Invalid token size';
            } else if ($error_response['status_code'] == '6') {
                $error_response['status_code'] = '6-Invalid topic size';
            } else if ($error_response['status_code'] == '7') {
                $error_response['status_code'] = '7-Invalid payload size';
            } else if ($error_response['status_code'] == '8') {
                $error_response['status_code'] = '8-Invalid token';
            } else if ($error_response['status_code'] == '255') {
                $error_response['status_code'] = '255-None (unknown)';
            } else {
                $error_response['status_code'] = $error_response['status_code'] . '-Not listed';
            }

            return ' Error iOS: User Device ID: ' . $error_response['identifier'] . ' Status: ' . $error_response['status_code'];
       }
       return false;
    }
}

if (!function_exists('get_user_agent'))
{
    /**
     * Funcion para obtener el user agent del actual request
     */
    function get_user_agent()
    {
        $user_agent = Request::server('HTTP_USER_AGENT');

        $control_mobile = 0;
        $tablet_browser = 0;
        $mobile_browser = 0;

        if (preg_match('/(tablet|ipad|playbook)|(android(?!.*(mobi|opera mini)))/i', strtolower($_SERVER['HTTP_USER_AGENT'])))
            $tablet_browser++;

        if (preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|android|iemobile)/i', strtolower($_SERVER['HTTP_USER_AGENT'])))
            $mobile_browser++;

        if ((isset($_SERVER['HTTP_ACCEPT']) && strpos(strtolower($_SERVER['HTTP_ACCEPT']),'application/vnd.wap.xhtml+xml') > 0) or ((isset($_SERVER['HTTP_X_WAP_PROFILE']) or isset($_SERVER['HTTP_PROFILE']))))
            $mobile_browser++;

        if (preg_match("/iPhone/i", strtolower($_SERVER['HTTP_USER_AGENT'])) || preg_match("/iPad/i", strtolower($_SERVER['HTTP_USER_AGENT'])))
            $control_mobile = 1;

        $response = [
            'control_mobile' => $control_mobile, // 1 si es iOS
            'tablet_browser' => $tablet_browser, // 1 si es tablet
            'mobile_browser' => $mobile_browser // 1 si es celular
        ];

        return $response;
    }
}

if (!function_exists('validate_latitude'))
{
    /**
     * Validates a given latitude $lat
     *
     * @param float|int|string $lat Latitude
     * @return bool `true` if $lat is valid, `false` if not
     */
    function validate_latitude($lat) {
      return preg_match('/^(\+|-)?(?:90(?:(?:\.0{1,10})?)|(?:[0-9]|[1-8][0-9])(?:(?:\.[0-9]{1,10})?))$/', $lat);
    }
}

if (!function_exists('validate_longitude'))
{
    /**
     * Validates a given longitude $long
     *
     * @param float|int|string $long Longitude
     * @return bool `true` if $long is valid, `false` if not
     */
    function validate_longitude($long) {
      return preg_match('/^(\+|-)?(?:180(?:(?:\.0{1,10})?)|(?:[0-9]|[1-9][0-9]|1[0-7][0-9])(?:(?:\.[0-9]{1,10})?))$/', $long);
    }
}

if (!function_exists('binary_search'))
{

    /**
     * Realiza una busqueda binaria dentro de un arreglo de elementos
     * ordenados ascesdentemente.
     *
     * @param int $needle
     * @param array|\Illuminate\Database\Eloquent\Collection $haystack
     * @param null|string $key
     * @param int|null $left
     * @param int|null $right
     * @return null|mixed
     */
    function binary_search($needle, $haystack, $key = null, $left = null, $right = null) {
        if (!intval($needle)) {
            return null;
        }

        if ($left === null) {
            $left = 0;
        }

        if ($right === null) {
            $right = count($haystack) - 1;
        }

        if ($left > $right) {
            return null;
        }

        $middle = floor(($left + $right) / 2);
        $element = $haystack[$middle];
        $value = $key ? $element->$key : $element;

        if ($value < $needle) {
            return binary_search($needle, $haystack, $key, $middle + 1, $right);
        } elseif ($value > $needle) {
            return binary_search($needle, $haystack, $key, $left, $middle - 1);
        }

        return $element;
    }
}

if (!function_exists('currency_format')) {

    /**
     * @param string $amount
     * @return string
     */
    function currency_format($amount)
    {
        return '$' . number_format(floor($amount), 0, ',', '.');
    }
}


if (! function_exists('retry')) {
    /**
     * Retry an operation a given number of times.
     *
     * @author Laravel
     * @param  int  $times
     * @param  callable  $callback
     * @param  int  $sleep
     * @return mixed
     *
     * @throws \Exception
     */
    function retry($times, callable $callback, $sleep = 0)
    {
        $times--;
        beginning:
        try {
            return $callback();
        } catch (Exception $e) {
            if (! $times) {
                throw $e;
            }
            $times--;
            if ($sleep) {
                usleep($sleep * 1000);
            }
            goto beginning;
        }
    }
}

if (! function_exists('safe_transaction')) {
    /**
     * Retry an operation a given number of times.
     *
     * @author Laravel
     * @param  int  $times
     * @param  callable  $callback
     * @param  int  $sleep
     * @return mixed
     *
     * @throws \Exception
     */
    function safe_transaction($times, callable $callback, $sleep = 0)
    {
        $times--;
        beginning:
        try {
            return $callback();
        } catch (Exception $e) {
            if (!$times || $e->getCode() != '40001') {
                throw $e;
            }
            $times--;
            if ($sleep) {
                usleep($sleep * 1000);
            }
            goto beginning;
        }
    }
}


if(! function_exists('get_expiration_credicard_dates')){
    /**
     *fecha vencimiento de tarjeta de credito
     *
     * @return array
     *
     */
    function get_expiration_credicard_dates(){
        $dates_cc = [];
        for ($i = 0; $i <= 9; $i++) {
            $dates_cc['years'][] = date('Y') + $i;
        }
        for ($i = 1; $i <= 12; $i++) {
            if ($i < 10) {
                $i = '0'.$i;
            }
            $dates_cc['months'][] = $i;
        }
        return $dates_cc;
    }
}

if (! function_exists('valid_version')) {
    /**
     * Retorna verdadero en caso de que la primera versión sea mayor a segunda.
     * E.j. valid_version('3.0.0', '2.11.5') -> true
     *
     * @param $first_version
     * @param $second_version
     * @return boolean
     */
    function valid_version($first_version, $second_version)
    {
        list($c_major, $c_minor, $c_patch) = array_map('intval', explode('.', $first_version));
        list($t_major, $t_minor, $t_patch) = array_map('intval', explode('.', $second_version));

        return $c_major > $t_major ||
            ($c_major >= $t_major && $c_minor > $t_minor) ||
            ($c_major >= $t_major && $c_minor >= $t_minor && $c_patch > $t_patch);
    }
}

if (! function_exists('track_leanplum_event')) {
    /**
     * @param $name
     * @param $user_id
     * @param $data
     * @param Closure|null $modifier
     * @throws \Exception
     */
    function track_leanplum_event($name, $user_id, $data, \Closure $modifier = null)
    {
        $track = new \Leanplum\Message\Request\Track();
        $track->set('event', $name);
        $track->set('userId', $user_id);
        $track->set('devMode', (bool) \Config::get('app.debug'));
        $track->set('params', (empty($data) || !is_array($data) ? new \StdClass() : $data));
        if (is_callable($modifier)) {
            $modifier($track);
        }

        Leanplum::track($track);
    }
}

if (! function_exists('track_event')) {
    /**
     * Realiza el seguimiento de un evento en
     * Analytics y Leanplum.
     *
     * @param $name
     * @param $user_id
     * @param $data
     * @param Closure|null $modifier
     * @throws Exception
     */
    function track_event($name, $user_id, $data, \Closure $modifier = null)
    {
        try {
            retry(3, function () use ($name, $user_id, $data, $modifier) {
                track_leanplum_event($name, $user_id, $data, $modifier);
            });
        } catch (\Exception $exception) {
            ErrorLog::add($exception);
        }

        $ch = curl_init();
        $content = json_encode(['event' => $name] + $data);
        curl_setopt($ch, CURLOPT_URL, \Config::get('app.events.big_query_service.url'));
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'Content-Length: ' . strlen($content)
        ]);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $content);
        $response = curl_exec($ch);
        curl_close($ch);

        if ($response === "false") {
            ErrorLog::add(new Exception("Error while trying to tack event '$name'"), 500);
        }
    }
}

if (!function_exists('get_check_digit_nit'))
{

    /**
     * Se utiliza el algoritmo módulo 11 para obtener
     * el digito verificador de un número
     * en este caso del nit o cédula.
     *
     * @param $nit
     * @return int
     */
    function get_check_digit_nit($nit)
    {
        $factors = array(1 => 3, 4 => 17, 7 => 29, 10 => 43, 13 => 59, 2 => 7, 5 => 19, 8 => 37, 11 => 47, 14 => 67, 3 => 13, 6 => 23, 9 => 41, 12 => 53, 15 => 71);
        $x = $y = 0;
        $z = strlen($nit);

        for ($i = 0; $i < $z; $i++) {
            $y = substr($nit, $i, 1);
            $x += ( $y * $factors[$z-$i] );
        }

        $y = $x % 11;

        if ( $y > 1 ) {
            $digit = 11 - $y;
            return $digit;
        } else {
            $digit = $y;
            return $digit;
        }
    }
}

if (!function_exists('get_only_nit_number'))
{

    /**
     * Expresión regular que reemplaza
     * algunos caracteres del nit incluyendo el digito verificador
     * Obtenieno así sólo el número.
     *
     * @param $nit
     * @return null|string|string[]
     */
    function get_only_nit_number($nit){
        return preg_replace(["/[-|—|_| ]\d?/", "/\./"], '', $nit);
    }
}

if(!function_exists('generate_hash_order'))
{
    function generate_hash_order($reference){
        $hash = new Hashids();

        return $hash->encode($reference);
    }
}


if(!function_exists('castValueToFloat'))
{
    function castValueToFloat($arr, $fields) {
        
        return $arr->map(function ($row) use ($fields) {

            foreach($fields as $field) {
                $row->$field = floatval($row->$field);           
            }
            
            return $row;
        });       
    }
}



if(!function_exists('setToNullIfZero'))
{
    function setToNullIfZero($arr, $fields) {
        
        return $arr->map(function ($row) use ($fields) {

            foreach($fields as $field) {
                $row->$field = empty($row->$field) ? null : $row->$field;
            }
            
            return $row;
        });       
    }
}

if (!function_exists('CountryCurrencyFormat')) {

    /**
     * @param string $formatValue
     * @param float $amount
     * @return string
     */
    function CountryCurrencyFormat($formatValue, $amount)
    {
        $currency = $formatValue['symbol'];
        $decimals = $formatValue['decimals'];
        $decPoint = $formatValue['dec_point'];
        $thousandsSep = $formatValue['thousands_sep'];
        $value = (empty($decimals)) ? intval($amount) : $amount;
        $numberFormat = sprintf('%s %s', $currency, number_format($value, $decimals, $decPoint, $thousandsSep));
        return $numberFormat;
    }
}
