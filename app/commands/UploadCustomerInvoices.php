<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class UploadCustomerInvoices extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'command:upload-customer-invoices';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Carga los clientes para facturas.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $file = $this->argument('file');
        \Excel::load($file, function ($reader) {
            $result = $reader->get();
            try {
                foreach ($result as $row) {
                    $customer_invoice = new \CustomerInvoice();
                    $customer_invoice->document_type = $row->tipodoc;
                    $customer_invoice->document_number = $row->nit;
                    $customer_invoice->fullname = $row->nombre;
                    $customer_invoice->phone = $row->telefono;
                    $customer_invoice->address = $row->direccion;
                    $customer_invoice->save();
                }
            } catch (Exception $e){
            }
        });
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            array('file', InputArgument::REQUIRED, 'Ruta del archivo.'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
        );
    }

}
