<?php

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use orders\OrderStatus;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputOption;

/**
 * Class UpdateUserFieldsLeanplum
 */
class UpdateUserFieldsLeanplum extends UpdateUserStatusLeanplum
{
    /**
     * @var string Ticket más alto del usuario.
     */
    const MAX_TICKET = 'maxTicket';

    /**
     * @var string Promedio de calificaciones del usuario.
     */
    const AVG_ORDER = 'avgOrder';

    /**
     * @var string Ha usado o no tarjeta de crédito.
     */
    const CREDIT_CARD = 'creditCard';

    /**
     * @var string Ha usado o no efectivo.
     */
    const CASH = 'cash';

    /**
     * @var string Ha usado o no datafono.
     */
    const SWIPE_CARD_TERMINAL = 'swipeCardTerminal';

    /**
     * @var string Código de referido del usuario.
     */
    const REFERRAL_CODE = 'referralCode';

    /**
     * @var string Número de usuarios que efectivamente a referido, que hayan terminado la compra.
     */
    const REFERRAL_NUMBER = 'referralsNumber';

    /**
     * @var string Null si no tiene crédito a expirer o la primera siguiente fecha de expirar.
     */
    const NEXT_CREDIT_EXPIRATION_DATE = 'nextCreditExpirationDate';

    /**
     * @var string Valor de próximo crédito a expirar o null si no tiene
     */
    const NEXT_CREDIT_EXPIRATION_AMOUNT = 'nextCreditExpirationAmount';

    /**
     * @var string Ciudad de la tienda donde el usuario ingreso
     */
    const CITY = 'city';

    /**
     * @var string Determina si el usuario ha realizado ordenes
     */
    const HAS_ORDERED = 'hasOrdered';

    /**
     * @var string Ultima fecha de creación de un pedido por el usuario
     */
    const LAST_ORDER = 'lastOrder';

    /**
     * @var string Total de ordenes del usuario
     */
    const ORDER_COUNT = 'orderCount';

    /**
     * @var string Determina si el usuario ha validado su número telefónico
     */
    const USER_VALIDATED = 'userValidated';

    /**
     * @var string Identificador de la bodega en la cual el usuario realiza sus pedidos
     */
    const WAREHOUSE_ID = 'warehouseId';

    /**
     * @var string Fecha de entrega del ultimo pedido
     */
    const LAST_ORDER_DELIVERED = 'lastOrderDelivered';

    /**
     * @var string Determina si el usuario cuenta con un pedido en progreso.
     */
    const IN_PROGRESS_ORDER = 'inProgressOrder';

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'command:update-user-fields-leanplum';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Actualiza el estado de los campos.';

    /**
     * @var ProgressBar
     */
    protected $progressBar;

    /**
     * @var string
     */
    private $dateFormatted;

    /**
     * @return array
     */
    protected function getFieldsToUpdate()
    {
        return [
            self::MAX_TICKET,
            self::AVG_ORDER,
            self::CREDIT_CARD,
            self::CASH,
            self::SWIPE_CARD_TERMINAL,
            self::REFERRAL_CODE,
            self::REFERRAL_NUMBER,
            self::NEXT_CREDIT_EXPIRATION_DATE,
            self::NEXT_CREDIT_EXPIRATION_AMOUNT,
            'city',
            'email',
            'first_name',
            'last_name',
            'phone',
            'referral_code',
            'send_advertising',
            'status',
            'orderCount',
            'orderDeliveredCount',
        ];
    }

    /**
     * @param User $user
     * @return User|void
     */
    protected function updateUser(User $user)
    {
        $onlySomeAttributes = array_filter(
            explode(',', $this->option('attr')),
            function ($item) {
                return !empty($item);
            }
        );

        if (!empty($onlySomeAttributes)) {
            $this->updateAttributesList($user, $onlySomeAttributes);
            return;
        }

        $attributes = $user->fullTraits;
        $lastOrderDelivered = $user->orders()
            ->where('status', OrderStatus::DELIVERED)
            ->whereHas('orderGroup', function ($query) {
                $query->where('source', '<>', 'Reclamo');
            })
            ->latest()
            ->first();

        $lastOrderCreated = $user->orders()
            ->latest()
            ->first();

        $totalOrders = $user->orderCount;
        $lastOrderCreatedAtDate = $lastOrderCreated ? new Carbon($lastOrderCreated->created_at) : null;
        $lastOrderDeliveredAtDate = $lastOrderDelivered ? new Carbon($lastOrderDelivered->delivery_date) : null;

        $user->lastOrderTicket = $lastOrderCreated ? $lastOrderCreated->total_amount : null;
        $user->credit = $user->getCreditAvailable() ?: null;
        $user->hasOrdered = $totalOrders > 0 ? 1 : 0;
        $user->lastOrder = $lastOrderCreatedAtDate ? $lastOrderCreatedAtDate->format('Ymd') : null;
        $user->userValidated = !empty($user->phone_validated_date) ? 1 : 0;
        $user->warehouseId = $lastOrderCreated ? $lastOrderCreated->orderGroup->warehouse_id : null;
        $user->lastOrderDelivered = $lastOrderDeliveredAtDate ? $lastOrderDeliveredAtDate->format('Ymd') : null;
        $user->inProgressOrder = $user->orders()
            ->whereNotIn('status', [OrderStatus::DELIVERED, OrderStatus::CANCELED])
            ->count() > 0 ? 1 : 0;

        foreach ($attributes as $name => $value) {
            $user->$name = is_bool($value)
                // En Leanplum se almacenan los valores booleanos con 1 o 0.
                ? ($value ? 1 : 0)
                : $value;
        }
    }

    /**
     * Asigna los valores del usuario con el fin de
     * que se actualicen en leanplum.
     *
     * @param User $user
     * @param array $attributeList
     */
    protected function updateAttributesList(User $user, array $attributeList)
    {
        foreach ($attributeList as $item) {
            // Al utilizar los métodos magicos el atributo
            // se asigna y se agrega al listado de campos
            // modificador que posteriormente van a ser
            // enviados a Leanplum.
            $user->$item = $user->$item;
        }
    }

    /**
     * @return Builder
     */
    protected function getUserQuery()
    {
        $nOrders = $this->option('num-orders');
        $hDiscount = $this->option('h-discount');
        $lessThanOrders = $this->option('num-orders-l');
        $from = intval($this->option('from'));
        $dateGreaterThan = trim($this->option('date-gt'));
        $date = Carbon::create()->subDay();
        $query = User::query();

        if ($dateGreaterThan) {
            $date = new Carbon($dateGreaterThan);
        }

        if ($this->option('only-active')) {
            $this->dateFormatted = $date->format('Y-m-d');

            $query->where(function ($query) {
                $query->where(function ($query) {
                    $query->where(DB::raw('DATE(users.created_at)'), '>=', $this->dateFormatted)
                        ->orWhere(DB::raw('DATE(users.updated_at)'), '>=', $this->dateFormatted);
                })
                    ->orWhereHas('orders', function ($query) {
                        // Debido a que el query que valida el identificador del usuario queda
                        // al mismo nivel de este query. Es necesario que la siguiente condición
                        // quede anidada para que el query no tome tanto tiempo.
                        $query->where(function ($query) {
                            $query->where(DB::raw('DATE(orders.created_at)'), '>=', $this->dateFormatted)
                                ->orWhere(DB::raw('DATE(orders.management_date)'), '>=', $this->dateFormatted);
                        });
                    })
                    ->orWhereHas('userCredits', function ($query) {
                        $query->where(function ($query) {
                            $query->where(DB::raw('DATE(user_credits.created_at)'), '>=', $this->dateFormatted)
                                ->orWhere(DB::raw('DATE(user_credits.updated_at)'), '>=', $this->dateFormatted);
                        });
                    });
            });
        }

        if ($from) {
            $query->where('users.id', '>=', $from);
        }

        $hasBottomLimit = !empty($nOrders);
        $hasUpperLimit = !empty($lessThanOrders);

        if (!empty($hDiscount)) {
            $discountDate = new Carbon(Discount::PERCENT_DISCOUNT_BEGIN_DATE);
            $discountDate->addDays(Discount::REORDER_DAYS_DISCOUNT);
            if (Carbon::now()->gte($discountDate)) {
                $query->where(function ($query) {
                    $query->has('orders', '>', Discount::REORDER_ORDERS_DISCOUNT)
                        ->whereHas('orders', function ($query) {
                            $totalDays = $this->option('h-discount-days');
                            $limitDate = Carbon::create()
                                ->subDays((int)$totalDays);

                            $query->where('management_date', '>', $limitDate->toDateTimeString());
                        });
                });
            } else {
                $query->has('orders', '>', 2);
            }
        }

        if ($hasBottomLimit && $hasUpperLimit) {
            $query->has('orders', 'between', DB::raw("{$nOrders} AND {$lessThanOrders}"));
        } else if ($hasBottomLimit) {
            $query->has('orders', '>', $nOrders);
        } else if ($hasUpperLimit) {
            $query->has('orders', '<', $lessThanOrders);
        }

        return $query;
    }

    /**
     * This hook is triggered on execution finishment.
     */
    protected function onFinished()
    {
        send_mail([
            'vars' => [
                'title' => 'Actualización',
                'html' => "Se han actualizado {$this->totalUsers} usuarios. <br><br><pre>" . `ifconfig` . "</pre>",
            ],
            'template_name' => 'emails.daily_report',
            'subject' => 'Actualización de atributos de usuarios en Leanplum',
            'to' => [
                ['email' => 'ariveray@merqueo.com', 'name' => 'Alejandro Rivera'],
                ['email' => 'jsanchez@merqueo.com', 'name' => 'Jonathan Sanchez'],
            ],
        ]);
    }

    /**
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['only-active', 'a'],
            ['date-gt', 'd', InputOption::VALUE_OPTIONAL],
            ['from', 'f', InputOption::VALUE_OPTIONAL],
            ['num-orders', 'o', InputOption::VALUE_OPTIONAL, 'Usuarios con más de "n" pedidos', 0],
            ['num-orders-l', 'l', InputOption::VALUE_OPTIONAL, 'Usuarios con menos de "n" pedidos', 0],
            ['attr', 't', InputOption::VALUE_OPTIONAL, 'Listado de atributos separados por coma', 0],
            ['h-discount', 'hd', InputOption::VALUE_OPTIONAL, 'Usuarios que tiene el descuento del 5%', 0],
            [
                'h-discount-days',
                'hdd',
                InputOption::VALUE_OPTIONAL,
                'Cantidad de días a extraer para el porcentaje de descuento',
                Discount::REORDER_DAYS_DISCOUNT + 1
            ],
        ];
    }
}
