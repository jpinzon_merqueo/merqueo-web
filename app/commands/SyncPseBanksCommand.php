<?php

use Illuminate\Console\Command;

/**
 * Class SyncPseBanksCommand.
 *
 * @author Johan Alvarez <jalvarez@merqueo.com>
 */
class SyncPseBanksCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'pse:sync-banks';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Syncs PSE banks';

    /**
     * @var PayU
     */
    private $payUService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(PayU $payUService)
    {
        $this->payUService = $payUService;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $pseBanksApiResponse = $this->payUService->getPseBanksList();

        foreach ($pseBanksApiResponse as $bankData) {
            $key = array_only($bankData, 'id');
            PseBank::updateOrCreate($key, $bankData);
        }

        $banksIds = array_pluck($pseBanksApiResponse, 'id');
        PseBank::whereNotIn('id', $banksIds)->delete();
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }

}
