<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class refactorDiscontinuedProductsField extends Command {
	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'command:refactorDiscontinued';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Ajuste de productos descontinuados.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        $store_products = StoreProduct::all()->count();
        $output = new Symfony\Component\Console\Output\ConsoleOutput();
        $progressBar = new \Symfony\Component\Console\Helper\ProgressBar($output, $store_products);
        $progressBar->start();

		StoreProduct::with('storeProductWarehouses')->chunk(200, function ($storeProducts) use ($progressBar) {
		    foreach ($storeProducts as $storeProduct) {
		        $discontinued = $storeProduct->discontinued;
		        $storeProduct->storeProductWarehouses->each(function ($storeProductWarehouse) use ($discontinued) {
                    $storeProductWarehouse->discontinued = $discontinued;
                    $storeProductWarehouse->save();
                });
                $progressBar->advance();
            }
        });

		$progressBar->finish();
        $output->writeln('Finalizado el comando');
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
	    return [];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
	    return [];
	}

}
