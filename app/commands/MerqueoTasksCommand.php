<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Carbon\Carbon;
use Illuminate\Support\Facades\Event;
use Illuminate\Database\Eloquent\Collection;

class MerqueoTasksCommand extends Command {

	use LogTrait;

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'command:merqueo-tasks';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Executes then given merqueo-task';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
		Event::subscribe(new ProviderOrderEventHandler);
		
		$this->setHelp("Available <tasks> are the listed below:\n\n".
		"daily_report\n".
		"send_mail_error_recurrence\n".
		"send_provider_order\n".
		"save_product_stock_log\n".
		"change_entities_order\n".
		"generate_invoice\n".
		"send_invoice\n".
		"make_marketplace_provider_orders\n".
		"cancel_provider_orders\n".
		"update_bough_products\n".
		"every_30_minutes\n".
		"notify_orders_next_days\n".
		"update_common_user_properties_at_leanplum\n".
		"update_accumulated_campaign_gift\n".
		"free_delivery_for_users_without_placing_an_order\n".
		"update_provider_order_status\n");
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$task			= $this->argument('task');
		$starting_time 	= time();

		$this->info("Task to be executed: $task");

		try{
			switch($task){
				case 'daily_report':
					$this->daily_report();
				break;

				case 'send_mail_error_recurrence':
					$this->send_mail_error_recurrence();
				break;

				case 'send_provider_order':
					$this->send_provider_order();
				break;

				case 'save_product_stock_log':
					$this->save_product_stock_log();
				break;

				case 'change_entities_order':
					$this->change_entities_order();
				break;

				case 'generate_invoice':
					$startDate = $this->argument('start-date');
					$endDate = $this->argument('end-date');
					$this->generate_invoice($startDate, $endDate);
				break;

				case 'send_invoice':
					$this->send_invoice();
				break;

				case 'make_marketplace_provider_orders':
					$this->make_marketplace_provider_orders();
				break;

				case 'cancel_provider_orders':
					$this->cancel_provider_orders();
				break;

				case 'update_bough_products':
					$this->update_bough_products();
				break;

				case 'every_30_minutes':
					$this->every_30_minutes();
				break;

				case 'notify_orders_next_days':
					$this->notify_orders_next_days();
				break;

				case 'update_common_user_properties_at_leanplum':
					$this->update_common_user_properties_at_leanplum();
				break;

				case 'update_accumulated_campaign_gift':
					$this->update_accumulated_campaign_gift();
				break;

				case 'free_delivery_for_users_without_placing_an_order':
					$this->free_delivery_for_users_without_placing_an_order();
				break;

				case 'update_provider_order_status':
					$this->update_provider_order_status();
				break;

				default:
					$this->error("No implementation found for task: $task");
					$this->run = false;
				break;
			}
		}catch(Exception $e){
			throw $e;
		}

		$time_spent = time() - $starting_time;
		$this->info("Finished in ".$time_spent." seconds.");
		
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('task', InputArgument::REQUIRED, 'The name of the task to be executed'),
			array('start-date', InputArgument::OPTIONAL, 'Starting date for \'generate_invoice\' YYYY-MM-DD '),
			array('end-date', InputArgument::OPTIONAL, 'Ending date for \'generate_invoice\' YYYY-MM-DD '),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			//array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}







    /**
     * Envia por mail reporte diario de pedidos con cron job
     */
    private function daily_report()
    {
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        $date = date('Y-m-d', strtotime('-1 days'));
        //$date = '2019-02-04';

        $cities = City::where('is_main', 1)->where('status', 1)->get();
        foreach ($cities as $city) {
            $cont['created'][$city->city] = ['orders_today' => 0, 'orders_week_ago' => 0, 'percentage_day' => 0, 'orders_this_month' => 0, 'orders_last_month' => 0, 'percentage_month' => 0];
            $cont['created_total_amount'][$city->city] = ['orders_today' => 0, 'orders_week_ago' => 0, 'percentage_day' => 0, 'orders_this_month' => 0, 'orders_last_month' => 0, 'percentage_month' => 0];
            $cont['created_delivery_amount'][$city->city] = ['orders_today' => 0, 'orders_week_ago' => 0, 'percentage_day' => 0, 'orders_this_month' => 0, 'orders_last_month' => 0, 'percentage_month' => 0];
            $cont['created_total'][$city->city] = ['orders_today' => 0, 'orders_week_ago' => 0, 'percentage_day' => 0, 'orders_this_month' => 0, 'orders_last_month' => 0, 'percentage_month' => 0];
            $cont['delivered'][$city->city] = ['orders_today' => 0, 'orders_week_ago' => 0, 'percentage_day' => 0, 'orders_this_month' => 0, 'orders_last_month' => 0, 'percentage_month' => 0];
            $cont['total_amount'][$city->city] = ['orders_today' => 0, 'orders_week_ago' => 0, 'percentage_day' => 0, 'orders_this_month' => 0, 'orders_last_month' => 0, 'percentage_month' => 0];
            $cont['delivery_amount'][$city->city] = ['orders_today' => 0, 'orders_week_ago' => 0, 'percentage_day' => 0, 'orders_this_month' => 0, 'orders_last_month' => 0, 'percentage_month' => 0];
            $cont['total'][$city->city] = ['orders_today' => 0, 'orders_week_ago' => 0, 'percentage_day' => 0, 'orders_this_month' => 0, 'orders_last_month' => 0, 'percentage_month' => 0];
            $cont['acquisitions'][$city->city] = ['orders_today' => 0, 'orders_week_ago' => 0, 'percentage_day' => 0, 'orders_this_month' => 0, 'orders_last_month' => 0, 'percentage_month' => 0];
            /*$cont['percentage_orders_cancelled_returned'][$city->city] = ['orders_today' => 0, 'orders_week_ago' => 0, 'percentage_day' => 0, 'orders_this_month' => 0, 'orders_last_month' => 0, 'percentage_month' => 0];
            $cont['orders_without_missing'][$city->city] = ['orders_today' => 0, 'orders_week_ago' => 0, 'percentage_day' => 0, 'orders_this_month' => 0, 'orders_last_month' => 0, 'percentage_month' => 0];
            $cont['orders_out_of_time'][$city->city] = ['orders_today' => 0, 'orders_week_ago' => 0, 'percentage_day' => 0, 'orders_this_month' => 0, 'orders_last_month' => 0, 'percentage_month' => 0];*/
        }
        $cont['created']['totals'] = ['orders_today' => 0, 'orders_week_ago' => 0, 'percentage_day' => 0, 'orders_this_month' => 0, 'orders_last_month' => 0, 'percentage_month' => 0];
        $cont['created_total_amount']['totals'] = ['orders_today' => 0, 'orders_week_ago' => 0, 'percentage_day' => 0, 'orders_this_month' => 0, 'orders_last_month' => 0, 'percentage_month' => 0];
        $cont['created_delivery_amount']['totals'] = ['orders_today' => 0, 'orders_week_ago' => 0, 'percentage_day' => 0, 'orders_this_month' => 0, 'orders_last_month' => 0, 'percentage_month' => 0];
        $cont['created_total']['totals'] = ['orders_today' => 0, 'orders_week_ago' => 0, 'percentage_day' => 0, 'orders_this_month' => 0, 'orders_last_month' => 0, 'percentage_month' => 0];
        $cont['delivered']['totals'] = ['orders_today' => 0, 'orders_week_ago' => 0, 'percentage_day' => 0, 'orders_this_month' => 0, 'orders_last_month' => 0, 'percentage_month' => 0];
        $cont['total_amount']['totals'] = ['orders_today' => 0, 'orders_week_ago' => 0, 'percentage_day' => 0, 'orders_this_month' => 0, 'orders_last_month' => 0, 'percentage_month' => 0];
        $cont['delivery_amount']['totals'] = ['orders_today' => 0, 'orders_week_ago' => 0, 'percentage_day' => 0, 'orders_this_month' => 0, 'orders_last_month' => 0, 'percentage_month' => 0];
        $cont['total']['totals'] = ['orders_today' => 0, 'orders_week_ago' => 0, 'percentage_day' => 0, 'orders_this_month' => 0, 'orders_last_month' => 0, 'percentage_month' => 0];
        $cont['acquisitions']['totals'] = ['orders_today' => 0, 'orders_week_ago' => 0, 'percentage_day' => 0, 'orders_this_month' => 0, 'orders_last_month' => 0, 'percentage_month' => 0];
        /*$cont['percentage_orders_cancelled_returned']['totals'] = ['orders_today' => 0, 'orders_week_ago' => 0, 'percentage_day' => 0, 'orders_this_month' => 0, 'orders_last_month' => 0, 'percentage_month' => 0];
        $cont['orders_without_missing']['totals'] = ['orders_today' => 0, 'orders_week_ago' => 0, 'percentage_day' => 0, 'orders_this_month' => 0, 'orders_last_month' => 0, 'percentage_month' => 0];
        $cont['orders_out_of_time']['totals'] = ['orders_today' => 0, 'orders_week_ago' => 0, 'percentage_day' => 0, 'orders_this_month' => 0, 'orders_last_month' => 0, 'percentage_month' => 0];*/
        $cont['date'] = date('d/m/Y', strtotime($date));

        //CREADOS

        //pedidos creados hoy
        $data = Order::join('order_groups', 'group_id', '=', 'order_groups.id')
            ->join('cities', 'user_city_id', '=', 'cities.id')
            ->leftJoin('cities AS c', 'cities.parent_city_id', '=', 'c.id')
            ->select('cities.city', 'c.city AS parent_city', DB::raw('COUNT(orders.id) AS orders'), DB::raw('SUM(orders.total_amount) AS total_amount'), DB::raw('SUM(orders.delivery_amount) AS delivery_amount'), DB::raw('(SUM(orders.total_amount) + SUM(orders.delivery_amount)) AS total'))
            ->where('source', '<>', 'Reclamo')
            ->whereNull('child_order_id')
            ->whereRaw("date BETWEEN '" . date('Y-m-d 00:00:00', strtotime($date)) . "' AND '" . date('Y-m-d 23:59:59', strtotime($date)) . "'")
            ->groupBy('cities.id')
            ->get();
        if (count($data)) {
            foreach ($data as $row) {
                $row->city = !empty($row['parent_city']) ? $row['parent_city'] : $row['city'];
                $cont['created'][$row->city]['orders_today'] += $row->orders;
                $cont['created_total_amount'][$row->city]['orders_today'] += $row->total_amount;
                $cont['created_delivery_amount'][$row->city]['orders_today'] += $row->delivery_amount;
                $cont['created_total'][$row->city]['orders_today'] += $row->total;
            }
        }

        //pedidos creados hace una semana
        $data = Order::join('order_groups', 'group_id', '=', 'order_groups.id')
            ->join('cities', 'user_city_id', '=', 'cities.id')
            ->leftJoin('cities AS c', 'cities.parent_city_id', '=', 'c.id')
            ->select('cities.city', 'c.city AS parent_city', DB::raw('COUNT(orders.id) AS orders'), DB::raw('SUM(orders.total_amount) AS total_amount'), DB::raw('SUM(orders.delivery_amount) AS delivery_amount'), DB::raw('(SUM(orders.total_amount) + SUM(orders.delivery_amount)) AS total'))
            ->where('source', '<>', 'Reclamo')
            ->whereNull('child_order_id')
            ->whereRaw("date BETWEEN '" . date('Y-m-d 00:00:00', strtotime('-7 days', strtotime($date))) . "' AND '" . date('Y-m-d 23:59:59', strtotime('-7 days', strtotime($date))) . "'")
            ->groupBy('cities.id')
            ->get();
        if (count($data)) {
            foreach ($data as $row) {
                $row->city = !empty($row['parent_city']) ? $row['parent_city'] : $row['city'];
                $cont['created'][$row->city]['orders_week_ago'] += $row->orders;
                $cont['created_total_amount'][$row->city]['orders_week_ago'] += $row->total_amount;
                $cont['created_delivery_amount'][$row->city]['orders_week_ago'] += $row->delivery_amount;
                $cont['created_total'][$row->city]['orders_week_ago'] += $row->total;
            }
        }
        //pedidos creados este mes
        $data = Order::join('order_groups', 'group_id', '=', 'order_groups.id')
            ->join('cities', 'user_city_id', '=', 'cities.id')
            ->leftJoin('cities AS c', 'cities.parent_city_id', '=', 'c.id')
            ->select('cities.city', 'c.city AS parent_city', DB::raw('COUNT(orders.id) AS orders'), DB::raw('SUM(orders.total_amount) AS total_amount'), DB::raw('SUM(orders.delivery_amount) AS delivery_amount'), DB::raw('(SUM(orders.total_amount) + SUM(orders.delivery_amount)) AS total'))
            ->where('source', '<>', 'Reclamo')
            ->whereNull('child_order_id')
            ->whereRaw("date BETWEEN '" . date('Y-m-01 00:00:00', strtotime($date)) . "' AND '" . date('Y-m-d 23:59:59', strtotime($date)) . "'")
            ->groupBy('cities.id')
            ->get();
        if (count($data)) {
            foreach ($data as $row) {
                $row->city = !empty($row['parent_city']) ? $row['parent_city'] : $row['city'];
                $cont['created'][$row->city]['orders_this_month'] += $row->orders;
                $cont['created_total_amount'][$row->city]['orders_this_month'] += $row->total_amount;
                $cont['created_delivery_amount'][$row->city]['orders_this_month'] += $row->delivery_amount;
                $cont['created_total'][$row->city]['orders_this_month'] += $row->total;
            }
        }

        //validar si es ultimo dia del mes
        if (date('j', strtotime($date)) > date('t', strtotime($date . ' -34 day'))) {
            $start_month_day = date('Y-m-01 00:00:00', strtotime('-34 day', strtotime($date)));
            $end_month_day = date('Y-m-t 23:59:59', strtotime('-34 day', strtotime($date)));
        } else {
            $start_month_day = date('d', strtotime($date)) == date('t', strtotime($date)) ? date('Y-m-01 00:00:00', strtotime('-34 day', strtotime($date))) : date('Y-m-01 00:00:00', strtotime('-1 month', strtotime($date)));
            $end_month_day = date('d', strtotime($date)) == date('t', strtotime($date)) ? date('Y-m-t 23:59:58', strtotime('-34 day', strtotime($date))) : date('Y-m-d 23:59:59', strtotime('-1 month', strtotime($date)));
        }

        //echo $start_month_day.'<br>'.$end_month_day; exit;

        //pedidos creados el mes pasado
        $data = Order::join('order_groups', 'group_id', '=', 'order_groups.id')
            ->join('cities', 'user_city_id', '=', 'cities.id')
            ->leftJoin('cities AS c', 'cities.parent_city_id', '=', 'c.id')
            ->select('cities.city', 'c.city AS parent_city', DB::raw('COUNT(orders.id) AS orders'), DB::raw('SUM(orders.total_amount) AS total_amount'), DB::raw('SUM(orders.delivery_amount) AS delivery_amount'), DB::raw('(SUM(orders.total_amount) + SUM(orders.delivery_amount)) AS total'))
            ->where('source', '<>', 'Reclamo')
            ->whereNull('child_order_id')
            ->whereRaw("date BETWEEN '" . $start_month_day . "' AND '" . $end_month_day . "'")
            ->groupBy('cities.id')
            ->get();
        if (count($data)) {
            foreach ($data as $row) {
                $row->city = !empty($row['parent_city']) ? $row['parent_city'] : $row['city'];
                $cont['created'][$row->city]['orders_last_month'] += $row->orders;
                $cont['created_total_amount'][$row->city]['orders_last_month'] += $row->total_amount;
                $cont['created_delivery_amount'][$row->city]['orders_last_month'] += $row->delivery_amount;
                $cont['created_total'][$row->city]['orders_last_month'] += $row->total;
            }
        }

        //ENTREGADOS
        //pedidos entregados hoy
        $data = Order::join('order_groups', 'group_id', '=', 'order_groups.id')
            ->join('cities', 'user_city_id', '=', 'cities.id')
            ->leftJoin('cities AS c', 'cities.parent_city_id', '=', 'c.id')
            ->select('cities.city', 'c.city AS parent_city', DB::raw('COUNT(orders.id) AS orders'), DB::raw('SUM(orders.total_amount) AS total_amount'), DB::raw('SUM(orders.delivery_amount) AS delivery_amount'), DB::raw('(SUM(orders.total_amount) + SUM(orders.delivery_amount)) AS total'))
            ->where('orders.status', 'Delivered')
            ->where('source', '<>', 'Reclamo')
            ->whereNull('child_order_id')
            ->whereRaw("management_date BETWEEN '" . date('Y-m-d 00:00:00', strtotime($date)) . "' AND '" . date('Y-m-d 23:59:59', strtotime($date)) . "'")
            ->groupBy('cities.id')
            ->get();
        if (count($data)) {
            foreach ($data as $row) {
                $row->city = !empty($row['parent_city']) ? $row['parent_city'] : $row['city'];
                $cont['delivered'][$row->city]['orders_today'] += $row->orders;
                $cont['total_amount'][$row->city]['orders_today'] += $row->total_amount;
                $cont['delivery_amount'][$row->city]['orders_today'] += $row->delivery_amount;
                $cont['total'][$row->city]['orders_today'] += $row->total;
            }
        }

        //pedidos entregados hace una semana
        $data = Order::join('order_groups', 'group_id', '=', 'order_groups.id')
            ->join('cities', 'user_city_id', '=', 'cities.id')
            ->leftJoin('cities AS c', 'cities.parent_city_id', '=', 'c.id')
            ->select('cities.city', 'c.city AS parent_city', DB::raw('COUNT(orders.id) AS orders'), DB::raw('SUM(orders.total_amount) AS total_amount'), DB::raw('SUM(orders.delivery_amount) AS delivery_amount'), DB::raw('(SUM(orders.total_amount) + SUM(orders.delivery_amount)) AS total'))
            ->where('orders.status', 'Delivered')
            ->where('source', '<>', 'Reclamo')
            ->whereNull('child_order_id')
            ->whereRaw("management_date BETWEEN '" . date('Y-m-d 00:00:00', strtotime('-7 days', strtotime($date))) . "' AND '" . date('Y-m-d 23:59:59', strtotime('-7 days', strtotime($date))) . "'")
            ->groupBy('cities.id')
            ->get();
        if (count($data)) {
            foreach ($data as $row) {
                $row->city = !empty($row['parent_city']) ? $row['parent_city'] : $row['city'];
                $cont['delivered'][$row->city]['orders_week_ago'] += $row->orders;
                $cont['total_amount'][$row->city]['orders_week_ago'] += $row->total_amount;
                $cont['delivery_amount'][$row->city]['orders_week_ago'] += $row->delivery_amount;
                $cont['total'][$row->city]['orders_week_ago'] += $row->total;
            }
        }

        //pedidos entregados este mes
        $data = Order::join('order_groups', 'group_id', '=', 'order_groups.id')
            ->join('cities', 'user_city_id', '=', 'cities.id')
            ->leftJoin('cities AS c', 'cities.parent_city_id', '=', 'c.id')
            ->select('cities.city', 'c.city AS parent_city', DB::raw('COUNT(orders.id) AS orders'), DB::raw('SUM(orders.total_amount) AS total_amount'), DB::raw('SUM(orders.delivery_amount) AS delivery_amount'), DB::raw('(SUM(orders.total_amount) + SUM(orders.delivery_amount)) AS total'))
            ->where('orders.status', 'Delivered')
            ->where('source', '<>', 'Reclamo')
            ->whereNull('child_order_id')
            ->whereRaw("management_date BETWEEN '" . date('Y-m-01 00:00:00', strtotime($date)) . "' AND '" . date('Y-m-d 23:59:59', strtotime($date)) . "'")
            ->groupBy('cities.id')
            ->get();
        if (count($data)) {
            foreach ($data as $row) {
                $row->city = !empty($row['parent_city']) ? $row['parent_city'] : $row['city'];
                $cont['delivered'][$row->city]['orders_this_month'] += $row->orders;
                $cont['total_amount'][$row->city]['orders_this_month'] += $row->total_amount;
                $cont['delivery_amount'][$row->city]['orders_this_month'] += $row->delivery_amount;
                $cont['total'][$row->city]['orders_this_month'] += $row->total;
            }
        }

        //pedidos entregados el mes pasado
        $data = Order::join('order_groups', 'group_id', '=', 'order_groups.id')
            ->join('cities', 'user_city_id', '=', 'cities.id')
            ->leftJoin('cities AS c', 'cities.parent_city_id', '=', 'c.id')
            ->select('cities.city', 'c.city AS parent_city', DB::raw('COUNT(orders.id) AS orders'), DB::raw('SUM(orders.total_amount) AS total_amount'), DB::raw('SUM(orders.delivery_amount) AS delivery_amount'), DB::raw('(SUM(orders.total_amount) + SUM(orders.delivery_amount)) AS total'))
            ->where('orders.status', 'Delivered')
            ->where('source', '<>', 'Reclamo')
            ->whereNull('child_order_id')
            ->whereRaw("management_date BETWEEN '" . $start_month_day . "' AND '" . $end_month_day . "'")
            ->groupBy('cities.id')
            ->get();
        if (count($data)) {
            foreach ($data as $row) {
                $row->city = !empty($row['parent_city']) ? $row['parent_city'] : $row['city'];
                $cont['delivered'][$row->city]['orders_last_month'] += $row->orders;
                $cont['total_amount'][$row->city]['orders_last_month'] += $row->total_amount;
                $cont['delivery_amount'][$row->city]['orders_last_month'] += $row->delivery_amount;
                $cont['total'][$row->city]['orders_last_month'] += $row->total;
            }
        }

        //pedidos creados por usuarios nuevos hoy
        $data = Order::join('order_groups', 'group_id', '=', 'order_groups.id')
            ->join(DB::raw("(SELECT o.id FROM orders AS o
                            INNER JOIN order_groups AS og ON o.group_id = og.id 
                            WHERE og.source NOT IN ('Callcenter', 'Web Service', 'Reclamo')
                            AND o.parent_order_id IS NULL
                            GROUP BY og.user_email
                            HAVING MIN(o.created_at) BETWEEN '" . date('Y-m-d 00:00:00', strtotime($date)) . "' AND '" . date('Y-m-d 23:59:59', strtotime($date)) . "')AS qty"), "qty.id", "=", "orders.id")
            ->join('cities', 'user_city_id', '=', 'cities.id')
            ->leftJoin('cities AS c', 'cities.parent_city_id', '=', 'c.id')
            ->select('cities.city', 'c.city AS parent_city', DB::raw('COUNT(qty.id) AS tqty'))
            ->whereNotIn('source', ['Callcenter', 'Web Service', 'Reclamo'])
            ->whereNull('parent_order_id')
            ->groupBy('cities.id')
            ->havingRaw("MIN(orders.created_at) BETWEEN '" . date('Y-m-d 00:00:00', strtotime($date)) . "' AND '" . date('Y-m-d 23:59:59', strtotime($date)) . "'")
            ->get();
        if (count($data)) {
            foreach ($data as $row) {
                $row->city = !empty($row['parent_city']) ? $row['parent_city'] : $row['city'];
                $cont['acquisitions'][$row->city]['orders_today'] += $row->tqty;
            }
        }
        //pedidos creados por usuarios nuevos hace una semana
        $data = Order::join('order_groups', 'group_id', '=', 'order_groups.id')
            ->join(DB::raw("(SELECT o.id FROM orders AS o
                            INNER JOIN order_groups AS og ON o.group_id = og.id 
                            WHERE og.source NOT IN ('Callcenter', 'Web Service', 'Reclamo')
                            AND o.parent_order_id IS NULL
                            GROUP BY og.user_email
                            HAVING MIN(o.created_at) BETWEEN '" . date('Y-m-d 00:00:00', strtotime('-7 days', strtotime($date))) . "' AND '" . date('Y-m-d 23:59:59', strtotime('-7 days', strtotime($date))) . "')AS qty"), "qty.id", "=", "orders.id")
            ->join('cities', 'user_city_id', '=', 'cities.id')
            ->leftJoin('cities AS c', 'cities.parent_city_id', '=', 'c.id')
            ->select('cities.city', 'c.city AS parent_city', DB::raw('COUNT(qty.id) AS tqty'))
            ->whereNotIn('source', ['Callcenter', 'Web Service', 'Reclamo'])
            ->whereNull('parent_order_id')
            ->groupBy('cities.id')
            ->havingRaw("MIN(orders.created_at) BETWEEN '" . date('Y-m-d 00:00:00', strtotime('-7 days', strtotime($date))) . "' AND '" . date('Y-m-d 23:59:59', strtotime('-7 days', strtotime($date))) . "'")
            ->get();
        if (count($data)) {
            foreach ($data as $row) {
                $row->city = !empty($row['parent_city']) ? $row['parent_city'] : $row['city'];
                $cont['acquisitions'][$row->city]['orders_week_ago'] += $row->tqty;
            }
        }
        //pedidos creados por usuarios nuevos este mes
        $data = Order::join('order_groups', 'group_id', '=', 'order_groups.id')
            ->join(DB::raw("(SELECT o.id FROM orders AS o
                            INNER JOIN order_groups AS og ON o.group_id = og.id 
                            WHERE og.source NOT IN ('Callcenter', 'Web Service', 'Reclamo')
                            AND o.parent_order_id IS NULL
                            GROUP BY og.user_email
                            HAVING MIN(o.created_at) BETWEEN '" . date('Y-m-01 00:00:00', strtotime($date)) . "' AND '" . date('Y-m-d 23:59:59', strtotime($date)) . "')AS qty"), "qty.id", "=", "orders.id")
            ->join('cities', 'user_city_id', '=', 'cities.id')
            ->leftJoin('cities AS c', 'cities.parent_city_id', '=', 'c.id')
            ->select('cities.city', 'c.city AS parent_city', DB::raw('COUNT(qty.id) AS tqty'))
            ->whereNotIn('source', ['Callcenter', 'Web Service', 'Reclamo'])
            ->whereNull('parent_order_id')
            ->groupBy('cities.id')
            ->havingRaw("MIN(orders.created_at) BETWEEN '" . date('Y-m-01 00:00:00', strtotime($date)) . "' AND '" . date('Y-m-d 23:59:59', strtotime($date)) . "'")
            ->get();
        if (count($data)) {
            foreach ($data as $row) {
                $row->city = !empty($row['parent_city']) ? $row['parent_city'] : $row['city'];
                $cont['acquisitions'][$row->city]['orders_this_month'] += $row->tqty;
            }
        }
        //pedidos creados por usuarios nuevos el mes pasado
        $data = Order::join('order_groups', 'group_id', '=', 'order_groups.id')
            ->join(DB::raw("(SELECT o.id FROM orders AS o
                            INNER JOIN order_groups AS og ON o.group_id = og.id 
                            WHERE og.source NOT IN ('Callcenter', 'Web Service', 'Reclamo')
                            AND o.parent_order_id IS NULL
                            GROUP BY og.user_email
                            HAVING MIN(o.created_at) BETWEEN '" . $start_month_day . "' AND '" . $end_month_day . "')AS qty"), "qty.id", "=", "orders.id")
            ->join('cities', 'user_city_id', '=', 'cities.id')
            ->leftJoin('cities AS c', 'cities.parent_city_id', '=', 'c.id')
            ->select('cities.city', 'c.city AS parent_city', DB::raw('COUNT(qty.id) AS tqty'))
            ->whereNotIn('source', ['Callcenter', 'Web Service', 'Reclamo'])
            ->whereNull('parent_order_id')
            ->groupBy('cities.id')
            ->havingRaw("MIN(orders.created_at) BETWEEN '" . $start_month_day . "' AND '" . $end_month_day . "'")
            ->get();
        if (count($data)) {
            foreach ($data as $row) {
                $row->city = !empty($row['parent_city']) ? $row['parent_city'] : $row['city'];
                $cont['acquisitions'][$row->city]['orders_last_month'] += $row->tqty;
            }
        }

        //calcular porcentajes
        //Realizados
        foreach ($cont['created'] as $key => $data) {
            if ($key == 'totals') continue;

            if ($data['orders_week_ago']) {
                $diff = $data['orders_today'] - $data['orders_week_ago'];
                $cont['created'][$key]['percentage_day'] = round(($diff / $data['orders_week_ago']) * 100, 1) . '%';
            }
            if ($data['orders_last_month']) {
                $diff = $data['orders_this_month'] - $data['orders_last_month'];
                $cont['created'][$key]['percentage_month'] = round(($diff / $data['orders_last_month']) * 100, 1) . '%';
            }

            $cont['created']['totals']['orders_today'] += $cont['created'][$key]['orders_today'];
            $cont['created']['totals']['orders_week_ago'] += $cont['created'][$key]['orders_week_ago'];
            $cont['created']['totals']['orders_this_month'] += $cont['created'][$key]['orders_this_month'];
            $cont['created']['totals']['orders_last_month'] += $cont['created'][$key]['orders_last_month'];
        }
        if ($cont['created']['totals']['orders_week_ago']) {
            $diff = $cont['created']['totals']['orders_today'] - $cont['created']['totals']['orders_week_ago'];
            $cont['created']['totals']['percentage_day'] = round(($diff / $cont['created']['totals']['orders_week_ago']) * 100, 1) . '%';
        }
        if ($cont['created']['totals']['orders_this_month']) {
            $diff = $cont['created']['totals']['orders_this_month'] - $cont['created']['totals']['orders_last_month'];
            $cont['created']['totals']['percentage_month'] = round(($diff / $cont['created']['totals']['orders_last_month']) * 100, 1) . '%';
        }

        foreach ($cont['created_total_amount'] as $key => $data) {
            if ($key == 'totals') continue;

            if ($data['orders_week_ago']) {
                $diff = $data['orders_today'] - $data['orders_week_ago'];
                $cont['created_total_amount'][$key]['percentage_day'] = round(($diff / $data['orders_week_ago']) * 100, 1) . '%';
            }
            if ($data['orders_last_month']) {
                $diff = $data['orders_this_month'] - $data['orders_last_month'];
                $cont['created_total_amount'][$key]['percentage_month'] = round(($diff / $data['orders_last_month']) * 100, 1) . '%';
            }

            $cont['created_total_amount']['totals']['orders_today'] += $cont['created_total_amount'][$key]['orders_today'];
            $cont['created_total_amount']['totals']['orders_week_ago'] += $cont['created_total_amount'][$key]['orders_week_ago'];
            $cont['created_total_amount']['totals']['orders_this_month'] += $cont['created_total_amount'][$key]['orders_this_month'];
            $cont['created_total_amount']['totals']['orders_last_month'] += $cont['created_total_amount'][$key]['orders_last_month'];
        }
        if ($cont['created_total_amount']['totals']['orders_week_ago']) {
            $diff = $cont['created_total_amount']['totals']['orders_today'] - $cont['created_total_amount']['totals']['orders_week_ago'];
            $cont['created_total_amount']['totals']['percentage_day'] = round(($diff / $cont['created_total_amount']['totals']['orders_week_ago']) * 100, 1) . '%';
        }
        if ($cont['created_total_amount']['totals']['orders_this_month']) {
            $diff = $cont['created_total_amount']['totals']['orders_this_month'] - $cont['created_total_amount']['totals']['orders_last_month'];
            $cont['created_total_amount']['totals']['percentage_month'] = round(($diff / $cont['created_total_amount']['totals']['orders_last_month']) * 100, 1) . '%';
        }


        foreach ($cont['created_delivery_amount'] as $key => $data) {
            if ($key == 'totals') continue;

            if ($data['orders_week_ago']) {
                $diff = $data['orders_today'] - $data['orders_week_ago'];
                $cont['created_delivery_amount'][$key]['percentage_day'] = round(($diff / $data['orders_week_ago']) * 100, 1) . '%';
            }
            if ($data['orders_last_month']) {
                $diff = $data['orders_this_month'] - $data['orders_last_month'];
                $cont['created_delivery_amount'][$key]['percentage_month'] = round(($diff / $data['orders_last_month']) * 100, 1) . '%';
            }

            $cont['created_delivery_amount']['totals']['orders_today'] += $cont['created_delivery_amount'][$key]['orders_today'];
            $cont['created_delivery_amount']['totals']['orders_week_ago'] += $cont['created_delivery_amount'][$key]['orders_week_ago'];
            $cont['created_delivery_amount']['totals']['orders_this_month'] += $cont['created_delivery_amount'][$key]['orders_this_month'];
            $cont['created_delivery_amount']['totals']['orders_last_month'] += $cont['created_delivery_amount'][$key]['orders_last_month'];
        }
        if ($cont['created_delivery_amount']['totals']['orders_week_ago']) {
            $diff = $cont['created_delivery_amount']['totals']['orders_today'] - $cont['created_delivery_amount']['totals']['orders_week_ago'];
            $cont['created_delivery_amount']['totals']['percentage_day'] = round(($diff / $cont['created_delivery_amount']['totals']['orders_week_ago']) * 100, 1) . '%';
        }
        if ($cont['created_delivery_amount']['totals']['orders_this_month']) {
            $diff = $cont['created_delivery_amount']['totals']['orders_this_month'] - $cont['created_delivery_amount']['totals']['orders_last_month'];
            $cont['created_delivery_amount']['totals']['percentage_month'] = round(($diff / $cont['created_delivery_amount']['totals']['orders_last_month']) * 100, 1) . '%';
        }


        foreach ($cont['created_total'] as $key => $data) {
            if ($key == 'totals') continue;

            if ($data['orders_week_ago']) {
                $diff = $data['orders_today'] - $data['orders_week_ago'];
                $cont['created_total'][$key]['percentage_day'] = round(($diff / $data['orders_week_ago']) * 100, 1) . '%';
            }
            if ($data['orders_last_month']) {
                $diff = $data['orders_this_month'] - $data['orders_last_month'];
                $cont['created_total'][$key]['percentage_month'] = round(($diff / $data['orders_last_month']) * 100, 1) . '%';
            }

            $cont['created_total']['totals']['orders_today'] += $cont['created_total'][$key]['orders_today'];
            $cont['created_total']['totals']['orders_week_ago'] += $cont['created_total'][$key]['orders_week_ago'];
            $cont['created_total']['totals']['orders_this_month'] += $cont['created_total'][$key]['orders_this_month'];
            $cont['created_total']['totals']['orders_last_month'] += $cont['created_total'][$key]['orders_last_month'];
        }
        if ($cont['created_total']['totals']['orders_week_ago']) {
            $diff = $cont['created_total']['totals']['orders_today'] - $cont['created_total']['totals']['orders_week_ago'];
            $cont['created_total']['totals']['percentage_day'] = round(($diff / $cont['created_total']['totals']['orders_week_ago']) * 100, 1) . '%';
        }
        if ($cont['created_total']['totals']['orders_this_month']) {
            $diff = $cont['created_total']['totals']['orders_this_month'] - $cont['created_total']['totals']['orders_last_month'];
            $cont['created_total']['totals']['percentage_month'] = round(($diff / $cont['created_total']['totals']['orders_last_month']) * 100, 1) . '%';
        }

        //Entregados
        foreach ($cont['delivered'] as $key => $data) {
            if ($key == 'totals') continue;

            if ($data['orders_week_ago']) {
                $diff = $data['orders_today'] - $data['orders_week_ago'];
                $cont['delivered'][$key]['percentage_day'] = round(($diff / $data['orders_week_ago']) * 100, 1) . '%';
            }
            if ($data['orders_last_month']) {
                $diff = $data['orders_this_month'] - $data['orders_last_month'];
                $cont['delivered'][$key]['percentage_month'] = round(($diff / $data['orders_last_month']) * 100, 1) . '%';
            }

            $cont['delivered']['totals']['orders_today'] += $cont['delivered'][$key]['orders_today'];
            $cont['delivered']['totals']['orders_week_ago'] += $cont['delivered'][$key]['orders_week_ago'];
            $cont['delivered']['totals']['orders_this_month'] += $cont['delivered'][$key]['orders_this_month'];
            $cont['delivered']['totals']['orders_last_month'] += $cont['delivered'][$key]['orders_last_month'];
        }
        if ($cont['delivered']['totals']['orders_week_ago']) {
            $diff = $cont['delivered']['totals']['orders_today'] - $cont['delivered']['totals']['orders_week_ago'];
            $cont['delivered']['totals']['percentage_day'] = round(($diff / $cont['delivered']['totals']['orders_week_ago']) * 100, 1) . '%';
        }
        if ($cont['delivered']['totals']['orders_this_month']) {
            $diff = $cont['delivered']['totals']['orders_this_month'] - $cont['delivered']['totals']['orders_last_month'];
            $cont['delivered']['totals']['percentage_month'] = round(($diff / $cont['delivered']['totals']['orders_last_month']) * 100, 1) . '%';
        }

        //MONTOS

        //calcular porcentajes de los Montos
        foreach ($cont['total_amount'] as $key => $data) {
            if ($key == 'totals') continue;

            if ($data['orders_week_ago']) {
                $diff = $data['orders_today'] - $data['orders_week_ago'];
                $cont['total_amount'][$key]['percentage_day'] = round(($diff / $data['orders_week_ago']) * 100, 1) . '%';
            }
            if ($data['orders_last_month']) {
                $diff = $data['orders_this_month'] - $data['orders_last_month'];
                $cont['total_amount'][$key]['percentage_month'] = round(($diff / $data['orders_last_month']) * 100, 1) . '%';
            }

            $cont['total_amount']['totals']['orders_today'] += $cont['total_amount'][$key]['orders_today'];
            $cont['total_amount']['totals']['orders_week_ago'] += $cont['total_amount'][$key]['orders_week_ago'];
            $cont['total_amount']['totals']['orders_this_month'] += $cont['total_amount'][$key]['orders_this_month'];
            $cont['total_amount']['totals']['orders_last_month'] += $cont['total_amount'][$key]['orders_last_month'];
        }
        if ($cont['total_amount']['totals']['orders_week_ago']) {
            $diff = $cont['total_amount']['totals']['orders_today'] - $cont['total_amount']['totals']['orders_week_ago'];
            $cont['total_amount']['totals']['percentage_day'] = round(($diff / $cont['total_amount']['totals']['orders_week_ago']) * 100, 1) . '%';
        }
        if ($cont['total_amount']['totals']['orders_this_month']) {
            $diff = $cont['total_amount']['totals']['orders_this_month'] - $cont['total_amount']['totals']['orders_last_month'];
            $cont['total_amount']['totals']['percentage_month'] = round(($diff / $cont['total_amount']['totals']['orders_last_month']) * 100, 1) . '%';
        }

        //Domicilios
        foreach ($cont['delivery_amount'] as $key => $data) {
            if ($key == 'totals') continue;

            if ($data['orders_week_ago']) {
                $diff = $data['orders_today'] - $data['orders_week_ago'];
                $cont['delivery_amount'][$key]['percentage_day'] = round(($diff / $data['orders_week_ago']) * 100, 1) . '%';
            }
            if ($data['orders_last_month']) {
                $diff = $data['orders_this_month'] - $data['orders_last_month'];
                $cont['delivery_amount'][$key]['percentage_month'] = round(($diff / $data['orders_last_month']) * 100, 1) . '%';
            }

            $cont['delivery_amount']['totals']['orders_today'] += $cont['delivery_amount'][$key]['orders_today'];
            $cont['delivery_amount']['totals']['orders_week_ago'] += $cont['delivery_amount'][$key]['orders_week_ago'];
            $cont['delivery_amount']['totals']['orders_this_month'] += $cont['delivery_amount'][$key]['orders_this_month'];
            $cont['delivery_amount']['totals']['orders_last_month'] += $cont['delivery_amount'][$key]['orders_last_month'];
        }
        if ($cont['delivery_amount']['totals']['orders_week_ago']) {
            $diff = $cont['delivery_amount']['totals']['orders_today'] - $cont['delivery_amount']['totals']['orders_week_ago'];
            $cont['delivery_amount']['totals']['percentage_day'] = round(($diff / $cont['delivery_amount']['totals']['orders_week_ago']) * 100, 1) . '%';
        }
        if ($cont['delivery_amount']['totals']['orders_this_month']) {
            $diff = $cont['delivery_amount']['totals']['orders_this_month'] - $cont['delivery_amount']['totals']['orders_last_month'];
            $cont['delivery_amount']['totals']['percentage_month'] = round(($diff / $cont['delivery_amount']['totals']['orders_last_month']) * 100, 1) . '%';
        }

        foreach ($cont['total'] as $key => $data) {
            if ($key == 'totals') continue;

            if ($data['orders_week_ago']) {
                $diff = $data['orders_today'] - $data['orders_week_ago'];
                $cont['total'][$key]['percentage_day'] = round(($diff / $data['orders_week_ago']) * 100, 1) . '%';
            }
            if ($data['orders_last_month']) {
                $diff = $data['orders_this_month'] - $data['orders_last_month'];
                $cont['total'][$key]['percentage_month'] = round(($diff / $data['orders_last_month']) * 100, 1) . '%';
            }

            $cont['total']['totals']['orders_today'] += $cont['total'][$key]['orders_today'];
            $cont['total']['totals']['orders_week_ago'] += $cont['total'][$key]['orders_week_ago'];
            $cont['total']['totals']['orders_this_month'] += $cont['total'][$key]['orders_this_month'];
            $cont['total']['totals']['orders_last_month'] += $cont['total'][$key]['orders_last_month'];
        }
        if ($cont['total']['totals']['orders_week_ago']) {
            $diff = $cont['total']['totals']['orders_today'] - $cont['total']['totals']['orders_week_ago'];
            $cont['total']['totals']['percentage_day'] = round(($diff / $cont['total']['totals']['orders_week_ago']) * 100, 1) . '%';
        }
        if ($cont['total']['totals']['orders_this_month']) {
            $diff = $cont['total']['totals']['orders_this_month'] - $cont['total']['totals']['orders_last_month'];
            $cont['total']['totals']['percentage_month'] = round(($diff / $cont['total']['totals']['orders_last_month']) * 100, 1) . '%';
        }

        //ADQUISICIONES
        //calcular porcentajes de las Adquisiciones
        foreach ($cont['acquisitions'] as $key => $data) {
            if ($key == 'totals') continue;

            if ($data['orders_week_ago']) {
                $diff = $data['orders_today'] - $data['orders_week_ago'];
                $cont['acquisitions'][$key]['percentage_day'] = round(($diff / $data['orders_week_ago']) * 100, 1) . '%';
            }
            if ($data['orders_last_month']) {
                $diff = $data['orders_this_month'] - $data['orders_last_month'];
                $cont['acquisitions'][$key]['percentage_month'] = round(($diff / $data['orders_last_month']) * 100, 1) . '%';
            }

            $cont['acquisitions']['totals']['orders_today'] += $cont['acquisitions'][$key]['orders_today'];
            $cont['acquisitions']['totals']['orders_week_ago'] += $cont['acquisitions'][$key]['orders_week_ago'];
            $cont['acquisitions']['totals']['orders_this_month'] += $cont['acquisitions'][$key]['orders_this_month'];
            $cont['acquisitions']['totals']['orders_last_month'] += $cont['acquisitions'][$key]['orders_last_month'];
        }
        if ($cont['acquisitions']['totals']['orders_week_ago']) {
            $diff = $cont['acquisitions']['totals']['orders_today'] - $cont['acquisitions']['totals']['orders_week_ago'];
            $cont['acquisitions']['totals']['percentage_day'] = round(($diff / $cont['acquisitions']['totals']['orders_week_ago']) * 100, 1) . '%';
        }
        if ($cont['acquisitions']['totals']['orders_this_month']) {
            $diff = $cont['acquisitions']['totals']['orders_this_month'] - $cont['acquisitions']['totals']['orders_last_month'];
            ($cont['acquisitions']['totals']['orders_last_month'] != 0) ? $cont['acquisitions']['totals']['percentage_month'] = round(($diff / $cont['acquisitions']['totals']['orders_last_month']) * 100, 1) . '%' : $cont['acquisitions']['totals']['percentage_month'] = '0%';
        }

        //debug($cont);
        //return View::make('emails.daily_report_orders')->with('cont', $cont);

        //enviar mails
        $mail = array(
            'template_name' => 'emails.daily_report_orders',
            'subject' => 'Reporte diario de pedidos',
            'to' => array(
                array('email' => 'jsanchez@merqueo.com', 'name' => 'Jonathan Sanchez'),
                array('email' => 'miguel@merqueo.com', 'name' => 'Miguel Mcallister'),
                array('email' => 'sebastian@merqueo.com', 'name' => 'Sebastian Noguera'),
                array('email' => 'jose@merqueo.com', 'name' => 'Jose Calderon'),
                array('email' => 'pablo@merqueo.com', 'name' => 'Pablo Gonzalez'),
                array('email' => 'jtrujillo@merqueo.com', 'name' => 'Juan Pablo Trujilllo'),
                array('email' => 'smontejo@merqueo.com', 'name' => 'Silvia Montejo'),
                array('email' => 'lmolina@merqueo.com', 'name' => 'Luis Molina'),
                array('email' => 'loyuela@merqueo.com', 'name' => 'Laura Oyuela'),
                array('email' => 'jesguerra@merqueo.com', 'name' => 'Juan Esteban Esguerra'),
                array('email' => 'strujillo@merqueo.com', 'name' => 'Santiago Trujillo'),
                array('email' => 'jpajoy@merqueo.com', 'name' => 'Jhon Pajoy'),
                array('email' => 'acastro@merqueo.com', 'name' => 'Andres Castro'),
                array('email' => 'jmarin@merqueo.com', 'name' => 'Juan David Marin'),
                array('email' => 'jchate@merqueo.com', 'name' => 'Juan Carlos Chate'),
                array('email' => 'dacuna@merqueo.com', 'name' => 'Diego Acuña'),
                array('email' => 'dramirez@merqueo.com', 'name' => 'Laura Daniela Ramirez'),
                array('email' => 'jcamacho@merqueo.com', 'name' => 'Juan Manuel Camacho'),
                array('email' => 'pgaona@merqueo.com', 'name' => 'Paula Gaona'),
                array('email' => 'drojas@merqueo.com', 'name' => 'Didier Rojas'),
                array('email' => 'lnunez@merqueo.com', 'name' => 'Lady Nuñez'),
                array('email' => 'dospina@merqueo.com', 'name' => 'Daniel Ospina'),
            ),
            'vars' => array(
                'cont' => $cont
            )
        );
        if (send_mail($mail))
            echo 'Reporte enviado.';
	}
	

	/**
     * Envia mail con errores en produccion
     */
    private function send_mail_error_recurrence()
    {
        ini_set('memory_limit', '-1');
        $errors = [];
        $start_date = date('Y-m-d H:i:s', strtotime('-30 minutes'));

        $sql = "SELECT NOW(), message, url, MAX(created_at) AS update_at, COUNT(id) AS cant
        FROM error_log
        WHERE created_at BETWEEN '" . $start_date . "' AND '" . date('Y-m-d H:i:s') . "'
        GROUP BY message, DATE(created_at)
        ORDER BY DATE(created_at) DESC, cant DESC;";
        $rows = DB::select(DB::raw($sql));
        foreach ($rows as &$row) {
            if ($row->cant > getenv('APP_ERROR_LOG_NUMBER')) {
                $error['url'] = $row->url;
                $error['description'] = $row->message;
                $error['cant'] = $row->cant;
                $error['updated_at'] = $row->update_at;
                $errors[] = $error;
            }

        }

        if (count($errors)) {
            $mail = array(
                'template_name' => 'emails.error_report',
                'subject' => 'Reporte ultimos errores',
                'to' => array(
                    array('email' => 'jsanchez@merqueo.com', 'name' => 'Jonathan Sanchez'),
                    array('email' => 'moviedo@merqueo.com', 'name' => 'Marlon Oviedo'),
                    array('email' => 'arielp@merqueo.com', 'name' => 'Ariel Patiño'),
                    array('email' => 'acorrea@merqueo.com', 'name' => 'Alexander Correa'),
                    array('email' => 'mmorcillo@merqueo.com', 'name' => 'Miguel Morcillo'),
                    array('email' => 'amartinezd@merqueo.com', 'name' => 'Alvaro Martinez'),
                    array('email' => 'jalvarez@merqueo.com', 'name' => 'Johan Alvarez'),
                    array('email' => 'lpaloma@merqueo.com', 'name' => 'Luis Paloma'),
                    array('email' => 'jjaimes@merqueo.com', 'name' => 'Jose Jaimes'),
                    array('email' => 'rrivera@merqueo.com', 'name' => 'Leyniker Rivera'),
                    array('email' => 'eheredia@merqueo.com', 'name' => 'Elvis Heredia'),
                    array('email' => 'ariveray@merqueo.com', 'name' => 'Alejandro Rivera'),
                    array('email' => 'serodriguez@merqueo.com', 'name' => 'Sergio Rodriguez'),
                    array('email' => 'jlara@merqueo.com', 'name' => 'Juan Camilo Lara'),
                    array('email' => 'salvarez@merqueo.com', 'name' => 'Sebastian Alvarez'),
                ),
                'vars' => array(
                    'errors' => $errors
                )
            );

            if (send_mail($mail)) {
                echo 'Reporte enviado.';
            } else {
                echo 'Reporte no enviado.';
            }
        } else {
            echo 'Reporte no enviado sin datos disponibles.';
        }
	}
	


	/**
     * Función para crear ordenes de compra y enviarlas automáticamente
     */
    public function send_provider_order()
    {
        //Ocico  S.A.S
        $provider_id = 46;
        $admin_id = 63;
        $city_id = 1;
        $admin = Admin::find($admin_id);
        $date = Carbon::now()->addDay()->format('Y-m-d');
        $start_date = Carbon::now();
        $end_date = Carbon::now();
        $start_date->hour = 22;
        $start_date->minute = 9;
        $start_date->second = 0;

        $end_date->hour = 23;
        $end_date->minute = 0;
        $end_date->second = 0;
        $time = Carbon::now()->format('H:i:s');

        $provider_verify = ProviderOrder::where('provider_id', DB::raw($provider_id))
            ->whereBetween('created_at', [$start_date->format('Y-m-d H:i:s'), $end_date->format('Y-m-d H:i:s')])
            ->get();

        $warehouses = Warehouse::where('city_id', $city_id)->get();

        foreach ($warehouses as $warehouse) {

            if (count($provider_verify) == 0) {
                // Consulta para obtener los productos y cantidades a pedir
                $products = Order::whereIn('orders.status', ['Validation', 'Initiated', 'Enrutado', 'In Progress'])
                    ->whereIn('order_products.fulfilment_status', ['Pending', 'Not Avaliable'])
                    ->where('order_products.type', 'Product')
                    ->where('store_products.store_id', 63)
                    ->where('provider_id', $provider_id)
                    ->whereRaw('DATE(orders.delivery_date) = "' . $date . '"')
                    ->where('order_groups.warehouse_id', $warehouse->id)
                    ->join('order_products', 'order_products.order_id', '=', 'orders.id')
                    ->join('store_products', 'store_products.id', '=', 'order_products.store_product_id')
                    ->join('products', 'products.id', '=', 'store_products.product_id')
                    ->join('providers', 'providers.id', '=', 'store_products.provider_id')
                    ->join('order_groups', 'orders.group_id', '=', 'order_groups.id')
                    ->join('store_product_warehouses', 'store_products.id', '=', 'store_product_warehouses.store_product_id')
                    ->groupBy('order_products.store_product_id')
                    ->select('orders.id',
                        'order_products.id AS order_product_id',
                        'order_products.store_product_id',
                        'order_products.product_name',
                        DB::raw('SUM(order_products.quantity) AS committed_stock'),
                        'store_product_warehouses.current_stock',
                        'store_products.provider_plu',
                        'store_products.iva',
                        'store_products.cost',
                        'store_products.base_cost',
                        'store_products.provider_pack_type',
                        'store_products.provider_pack_quantity',
                        'store_product_warehouses.ideal_stock',
                        'products.type',
                        'products.reference',
                        'products.quantity',
                        'products.unit',
                        'products.image_medium_url')
                    ->get();
                //->toSql();
                $product_group = Order::whereIn('orders.status', ['Validation', 'Initiated', 'Enrutado', 'In Progress'])
                    ->whereIn('order_product_group.fulfilment_status', ['Pending', 'Not Avaliable'])
                    ->where('order_products.type', 'Agrupado')
                    ->where('store_products.store_id', '=', 63)
                    ->where('provider_id', $provider_id)
                    ->where('order_groups.warehouse_id', $warehouse->id)
                    ->whereRaw('DATE(orders.delivery_date) = "' . $date . '"')
                    ->join('order_products', 'order_products.order_id', '=', 'orders.id')
                    ->join('order_product_group', 'order_product_group.order_product_id', '=', 'order_products.id')
                    ->join('store_products', 'store_products.id', '=', 'order_product_group.store_product_id')
                    ->join('products', 'products.id', '=', 'store_products.product_id')
                    ->join('providers', 'providers.id', '=', 'store_products.provider_id')
                    ->join('order_groups', 'orders.group_id', '=', 'order_groups.id')
                    ->join('store_product_warehouses', 'store_products.id', '=', 'store_product_warehouses.store_product_id')
                    ->groupBy('order_product_group.store_product_id')
                    ->select('orders.id',
                        'order_products.id AS order_product_id',
                        'order_product_group.store_product_id',
                        'order_product_group.product_name',
                        DB::raw('SUM(order_product_group.quantity) AS committed_stock'),
                        'store_product_warehouses.current_stock',
                        'store_products.provider_plu',
                        'store_products.iva',
                        'store_products.cost',
                        'store_products.base_cost',
                        'store_products.provider_pack_type',
                        'store_products.provider_pack_quantity',
                        'store_product_warehouses.ideal_stock',
                        'products.type',
                        'products.reference',
                        'products.quantity',
                        'products.unit',
                        'products.image_medium_url')
                    ->get();

                $order_products = [];
                foreach ($products as $product)
                    $order_products[$product->store_product_id] = $product->toArray();

                if ($product_group) {
                    foreach ($product_group as $product_group_item) {
                        if (!array_key_exists($product_group_item->store_product_id, $order_products))
                            $order_products[$product_group_item->store_product_id] = $product_group_item->toArray();
                        else
                            $order_products[$product_group_item->store_product_id]['committed_stock'] += $product_group_item['committed_stock'];
                    }
                }

                if (count($order_products)) {
                    try {
                        DB::beginTransaction();
                        //orden de compra a merqueo
                        if ($time > '20:00:00') {
                            $provider = Provider::find($provider_id);
                            $provider_order = new ProviderOrder;
                            $provider_order->provider_id = $provider_id;
                            $provider_order->city_id = 1;
                            $provider_order->warehouse_id = $warehouse->id;
                            $provider_order->provider_name = $provider->name;
                            $provider_order->admin_id = $admin_id;
                            $provider_order->status = 'Iniciada';
                            $provider_order->date = date('Y-m-d H:i:s');
                            $provider_order->delivery_date = date('Y-m-d H:i:s', strtotime('+' . $provider->delivery_time_hours . ' hour'));
                            $provider_order->save();

                            $status = 'Enviada';
                            $provider_order->status = $status;
                            $provider_order->save();

                            $log = new ProviderOrderLog();
                            $log->type = 'Estado actualizado: ' . $status;
                            $log->admin_id = $admin_id;
                            $log->provider_order_id = $provider_order->id;
                            $log->save();
                        }

                        //Eliminar archivo
                        $path = public_path(Config::get('app.download_directory'));
                        /*if ($gestor = opendir($path)){
                            while (false !== ($file = readdir($gestor))){
                                if (strstr($file, 'ocico -'))
                                    remove_file($path.$file);
                            }
                        }*/

                        //crear excel para adjuntar al correo
                        $objPHPExcel = new PHPExcel();

                        //propiedades
                        $objPHPExcel->getProperties()->setCreator('Merqueo');
                        $objPHPExcel->getProperties()->setTitle('Orden de Compra');
                        $objPHPExcel->getProperties()->setSubject('Orden de Compra');
                        $objPHPExcel->setActiveSheetIndex(0);

                        //estilos
                        $style_header = array(
                            'fill' => array(
                                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                                'color' => array('rgb' => '5178A5'),
                            ),
                            'font' => array(
                                'color' => array('rgb' => 'FFFFFF'),
                                'bold' => true,
                            )
                        );
                        $objPHPExcel->getActiveSheet()->getStyle('A1:G1')->applyFromArray($style_header);

                        //titulos
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(0, 1, 'ID');
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(1, 1, 'PLU');
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(2, 1, 'Nombre');
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(3, 1, 'Costo');
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(4, 1, 'Contenido');
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(5, 1, 'Unidad');
                        $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(6, 1, 'Cantidad');
                        $j = 2;

                        //crear detalle de ordenes de compra por proveedor
                        foreach ($order_products as $key => $product) {
                            $store_product_warehouse = StoreProductWarehouse::getStoreProductWarehouse($warehouse->id, $product['store_product_id']);
                            $quantity_order = 0;
                            if ($store_product_warehouse->current_stock < $product['committed_stock'] && $store_product_warehouse->current_stock == 0) {
                                $quantity_order = $product['committed_stock'];
                            } elseif ($store_product_warehouse->current_stock < $product['committed_stock'] && $store_product_warehouse->current_stock > 0) {
                                $quantity_order = $product['committed_stock'] - $store_product_warehouse->current_stock;
                            }

                            if ($quantity_order > 0) {
                                /*echo "Bodega:".$warehouse->id."<br>";
                                echo "Producto: ".$product['product_name'].' '.$product['quantity'].' '.$product['unit']."</br>";
                                echo "cantidad a solicitar: ".$quantity_order."</br>";
                                echo "Cantidad Comprometidad: ".$product['committed_stock']."</br>";
                                echo "Stock Actual: ".$store_product_warehouse->current_stock."</br>";
                                echo "-------------------------------------------------------</br></br>";*/
                                if ($time > '20:00:00') {
                                    $provider_order_detail = new ProviderOrderDetail;
                                    $provider_order_detail->provider_order_id = $provider_order->id;
                                    $provider_order_detail->store_product_id = $product['store_product_id'];
                                    $provider_order_detail->type = $product['type'];
                                    $provider_order_detail->reference = $product['reference'];
                                    $provider_order_detail->plu = $product['provider_plu'];
                                    $provider_order_detail->product_name = $product['product_name'] . ' ' . $product['quantity'] . ' ' . $product['unit'];
                                    $provider_order_detail->quantity_order = $quantity_order;
                                    $provider_order_detail->ideal_stock = $product['ideal_stock'];
                                    $provider_order_detail->pack_description = $product['provider_pack_type'] != 'Unidad' ? '1 ' . $product['provider_pack_type'] . ' x ' . $product['provider_pack_quantity'] . ' Unidades' : '1 Unidad';
                                    $provider_order_detail->quantity_pack = $quantity_order;
                                    $provider_order_detail->base_cost = $product['base_cost'];
                                    $provider_order_detail->cost = $product['cost'];
                                    $provider_order_detail->iva = $product['iva'];
                                    $provider_order_detail->image_url = $product['image_medium_url'];
                                    $provider_order_detail->save();
                                }

                                //Se agregan los datos al archivo excel
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(0, $j, $product['store_product_id']);
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(1, $j, $product['provider_plu']);
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(2, $j, $product['product_name']);
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(3, $j, $product['cost']);
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(4, $j, $product['quantity']);
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(5, $j, $product['unit']);
                                $objPHPExcel->getActiveSheet()->SetCellValueByColumnAndRow(6, $j, $quantity_order);
                                $j++;
                            }
                        }

                        //crear archivo
                        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
                        $filename = 'Bodega' . $warehouse->warehouse . ' - ocico - ' . date('Y-m-d H.i.s', time()) . '.xlsx';
                        $objWriter->save($path . $filename);
                        $file['real_path'] = $path . $filename;
                        $file['client_original_name'] = $filename;
                        $file['client_original_extension'] = 'xlsx';
                        $url = upload_image($file, false, Config::get('app.download_directory_temp'));

                        if ($time > '20:00:00') {
                            DB::commit();
                            $provider_order_detail = ProviderOrderDetail::select(DB::raw('SUM(quantity_order) AS quantity_order'))
                                ->where('provider_order_id', $provider_order->id)
                                ->groupBy('provider_order_id')->first();


                            $html = '<p><strong>Orden de compra #:</strong> ' . $provider_order->id . ' Bodega' . $warehouse->warehouse . '</p>
                                      <p><strong>Proveedor:</strong> ' . $provider_order->provider_name . '</p>
                                      <p><strong>Estado:</strong> ' . $provider_order->status . '</p>
                                      <p><strong>Fecha de creaci&oacute;n:</strong> ' . date("d M Y g:i a", strtotime($provider_order->date)) . '</p>
                                      <p><strong>Fecha de entrega:</strong> ' . date("d M Y g:i a", strtotime($provider_order->delivery_date)) . '</p>
                                      <p><strong>Unidades solicitadas:</strong> ' . $provider_order_detail->quantity_order . '</p>
                                      <p><strong>Creada por:</strong> ' . $admin->fullname . '</p>';
                            $subject = 'Nueva orden de compra # ' . $provider_order->id . ' de ' . $provider_order->provider_name;
                        } else {
                            $html = '<p><strong>Orden de compra</strong></p>
                                     <p><strong>Fecha de entrega:</strong> ' . $date . '</p>
                                     <p><strong>Proveedor: OCICO S.A.S</strong></p>';
                            $subject = 'Nueva orden de compra para OCICO S.A.S';
                        }

                        $mail = array(
                            'template_name' => 'emails.daily_report',
                            'subject' => $subject,
                            'to' => [
                                [
                                    'email' => 'jtrujillo@merqueo.com',
                                    'name' => 'Juan Pablo Trujillo'
                                ],
                                [
                                    'email' => 'rrivera@merqueo.com',
                                    'name' => 'Leyniker Rivera'
                                ],
                                [
                                    'email' => 'eheredia@merqueo.com',
                                    'name' => 'Elvis Heredia'
                                ],
                                [
                                    'email' => 'julian.sanchez@ocico.co',
                                    'name' => 'Julian Sánchez'
                                ],
                                [
                                    'email' => 'dmercado@merqueo.com',
                                    'name' => 'Diana Mercado'
                                ]
                                ,
                                [
                                    'email' => 'arodriguez@merqueo.com',
                                    'name' => 'Alejandro Rodriguez'
                                ]

                            ],
                            'vars' => array(
                                'title' => 'Se ha creado una nueva orden de compra.',
                                'html' => $html
                            )
                        );
                        send_mail($mail);


                        //enviar correo con archivo adjunto
                        if ($time > '20:00:00')
                            $delivery_date = format_date('normal_long', date("Y-m-d", strtotime($provider_order->delivery_date)));
                        else
                            $delivery_date = $date;


                        $mail['subject'] = 'Pedido para ' . $delivery_date . ' Bodega ' . $warehouse->warehouse;
						$mail['to'] = [];
                        $mail['to'][0]['email'] = 'rrivera@merqueo.com';
                        $mail['to'][0]['name'] = 'Leyniker Rivera';
                        $mail['to'][1]['email'] = 'jtrujillo@merqueo.com';
                        $mail['to'][1]['name'] = 'Juan Pablo Trujillo';
                        $mail['to'][2]['email'] = 'julian.sanchez@ocico.co';
                        $mail['to'][2]['name'] = 'Julian Sánchez';
                        $mail['to'][4]['email'] = 'dmercado@merqueo.com';
                        $mail['to'][4]['name'] = 'Diana Mercado';
                        $mail['to'][5]['email'] = 'arodriguez@merqueo.com';
						$mail['to'][5]['name'] = 'Alejandro Rodriguez';
                        $mail['vars']['title'] = 'Pedido para ' . $delivery_date;
                        $mail['vars']['html'] = '
                        <h3>Pedido para ' . $delivery_date . '</h3>
                        <p>
                            Se envía archivo de excel adjunto con el detalle del pedido.
                        </p>
                    ';
                        $type = pathinfo($path . $filename, PATHINFO_EXTENSION);
                        $data = file_get_contents($path . $filename);
                        $base64 = base64_encode($data);
                        $attachments[0]['type'] = 'application/vnd.ms-excel';
                        $attachments[0]['name'] = $filename;
                        $attachments[0]['content'] = $base64;
                        send_mail($mail, $attachments);
                        echo $url;
                    } catch (\Exception $e) {
                        DB::rollback();
                        //echo $e->getMessage();
                        throw $e;
                    }
                }

            }
        }
	}
	

	/**
     * Función para guardar los inventarios de productos
     */
    private function save_product_stock_log()
    {
        $log                = [];
        $starting_datetime  = date('Y-m-d H:i:s');
        $error              = false;
        $saved_logs         = 0;
        $log[]              = "CRON: save_product_stock_log";
        $log[]              = "Tiempo de inicio: $starting_datetime";
        ini_set('memory_limit', '1024M');
        set_time_limit(0);

        try {

            DB::beginTransaction();

            //QUITAR PROMOCIONES DE PRODUCTOS
            $store_products = StoreProduct::whereNotNull('special_price_expiration_date')
                ->whereNotNull('special_price')
                ->where('special_price_expiration_date', '<>', '0000-00-00 00:00:00')
                ->where('special_price_expiration_date', '<', date('Y-m-d H:i:s'))
                ->get();
            foreach ($store_products as $store_product) {
                $store_product->special_price = null;
                $store_product->quantity_special_price = 0;
                $store_product->first_order_special_price = 0;
                $store_product->merqueo_discount = 0;
                $store_product->provider_discount = 0;
                $store_product->seller_discount = 0;
                $store_product->save();
                $this->admin_log('store_products', $store_product->id, 'update_cron_special_price');

                //desactivar banner asociado
                $banners = Banner::select('banners.*')
                    ->where('banners.store_product_id', $store_product->id)
                    ->where('banners.status', 1)->get();
                if (count($banners)) {
                    foreach ($banners as $banner) {
                        $banner->status = 0;
                        $banner->save();
                        $this->admin_log('banners', $banner->id, 'update_cron_inactive');
                    }
                }
            }

            $warehouses = Warehouse::where('status', 1)->get();

            foreach ($warehouses as $warehouse) {
                //dejar stock alistado en cero
//                StoreProductWarehouse::where('warehouse_id', $warehouse->id)->update(['picked_stock' => 0]);

                $store_products = StoreProduct::select('store_products.id', 'store_product_warehouses.picking_stock', 'store_product_warehouses.picked_stock', 'store_product_warehouses.current_stock',
                    'store_product_warehouses.reception_stock', 'store_product_warehouses.return_stock', 'store_product_warehouses.is_visible_stock', 'store_products.cost', 'store_products.price',
                    'store_products.provider_discount', 'store_products.merqueo_discount', 'store_products.seller_discount',
                    'store_products.special_price', 'store_products.base_cost', 'store_products.cost_average', 'store_product_warehouses.discontinued', 'store_product_warehouses.status')
                    ->join('store_product_warehouses', 'store_product_warehouses.store_product_id', '=', 'store_products.id')
                    ->where('store_product_warehouses.warehouse_id', $warehouse->id)
                    ->whereNull('store_products.allied_store_id')
                    //->where('store_products.id', 882)
                    ->get();

                foreach ($store_products as $key => $store_product) {
                    $product_log = new ProductStockLog;
                    $product_log->warehouse_id = $warehouse->id;
                    $product_log->store_product_id = $store_product->id;

                    $product_log->reception_stock = $store_product->reception_stock;
                    $product_log->picking_stock = $store_product->picking_stock;
                    $product_log->picked_stock = $store_product->picked_stock;
                    $product_log->return_stock = $store_product->return_stock;
                    $product_log->current_stock = $store_product->getCurrentStock($warehouse->id);

                    /*$product_log->reception_stock = 0;
                    $product_log->picking_stock = 0;
                    $product_log->picked_stock = 0;
                    $product_log->return_stock = 0;
                    $product_log->current_stock = 0;
                    $product_log->created_at = $product_log->updated_at = '2018-06-16 00:04:50';*/

                    $product_log->is_visible_stock = $store_product->is_visible_stock;
                    $product_log->price = $store_product->price;
                    $product_log->special_price = $store_product->special_price;
                    $product_log->cost = $store_product->cost;
                    $product_log->base_cost = $store_product->base_cost;
                    $product_log->cost_average = $store_product->cost_average;
                    $product_log->provider_discount = $store_product->provider_discount;
                    $product_log->merqueo_discount = $store_product->merqueo_discount;
                    $product_log->seller_discount = $store_product->seller_discount;
                    $product_log->discontinued = $store_product->discontinued;
                    $product_log->status = $store_product->status;
                    $product_log->save();
                    $saved_logs++;
                }
            }

            DB::commit();
            $log[] = "CRON ejecutado correctamente";
            $log[] = "$saved_logs logs de producto guardados";

        } catch (\Exception $exception) {
            DB::rollback();
            $error = true;
            $log[] = "ERROR: ".$exception->getMessage();
        }

        // Envio de reporte de ejecución
        $finish_datetime    =  date('Y-m-d H:i:s');
        $log[]              = "Tiempo de finalización: $finish_datetime";
        send_query_execution_report("CRON stock_product_log", $log, $error);

        //eliminar pedidos de firebase
        $firebase = new FirebaseClient('general');
        $firebase->deleteData('orders');
        //Actualizar estado de producto sugerido
        $suggested_products = ShelvesSuggestedProducts::where('status', 1)
            ->where('end_date', '<', date('Y-m-d'))
            ->get();
        foreach ($suggested_products as $suggested_product) {
            $suggested_product->status = 0;
            $suggested_product->save();
        }
	}
	

	/**
     * Actualiza posiciones de productos y banners mas vendidos
     */
    private function change_entities_order()
    {
        /*ini_set('memory_limit', '3072M');
        set_time_limit(0);

        $cities = City::where('status', 1)->groupBy('coverage_store_id')->select('id', 'city')->get();
        $departments_positions_banned = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18];
        $products_position_banned = [0, 1, 2, 3, 4, 5];
        $date = Carbon::now()->subDays(1)->toDateString();
        foreach ($cities as $key => $city) {
            $store = Store::getActiveStores($city->id);
            $updated_departments = 0;
            $updated_shelves = 0;
            $updated_products = 0;
            $updated_banners = 0;


            //actualizar departamentos, pasillos y productos
            $departments = DB::select("
                SELECT
                    *
                FROM (
                    SELECT
                        sp.product_id,
                        op.product_name,
                        p.category_id,
                        p.subcategory_id,
                        sp.department_id,
                        sp.shelf_id,
                        SUM(op.quantity) AS 'quantity'
                    FROM order_products op
                    INNER JOIN orders o ON op.order_id = o.id
                    LEFT JOIN store_products sp ON op.store_product_id = sp.id
                    LEFT JOIN products p ON sp.product_id = p.id
                    LEFT JOIN stores s ON sp.store_id = s.id
                    WHERE
                        o.status <> 'Cancelled'
                        AND op.store_product_id IS NOT NULL
                        AND op.store_product_id <> 0
                        AND p.id IS NOT NULL
                        AND s.city_id = " . $city->id . "
                        AND DATE(o.created_at) = '" . $date . "'
                        AND sp.shelf_id <> 16
                    GROUP BY
                        op.store_product_id
                    ORDER BY quantity DESC
                ) data_products
                GROUP BY department_id
                ORDER BY quantity DESC
            ");

            foreach ($departments as $key => $department) {
                $dep = Department::find($department->department_id);
                if ($dep && !in_array($dep->sort_order, $departments_positions_banned)) {
                    $dep->sort_order = ($key + 1);
                    $dep->save();
                    $updated_departments++;
                }

                $shelves = DB::select("
                    SELECT
                        *
                    FROM (
                        SELECT
                            sp.product_id,
                            op.product_name,
                            p.category_id,
                            p.subcategory_id,
                            sp.department_id,
                            sp.shelf_id,
                            SUM(op.quantity) AS 'quantity'
                        FROM order_products op
                        INNER JOIN orders o ON op.order_id = o.id
                        LEFT JOIN store_products sp ON op.store_product_id = sp.id
                        LEFT JOIN products p ON sp.product_id = p.id
                        LEFT JOIN stores s ON sp.store_id = s.id
                        WHERE
                            o.status <> 'Cancelled'
                            AND op.store_product_id IS NOT NULL
                            AND op.store_product_id <> 0
                            AND p.id IS NOT NULL
                            AND sp.department_id = " . $department->department_id . "
                            AND DATE(o.created_at) = '" . $date . "'
                            AND s.city_id = " . $city->id . "
                            AND sp.shelf_id <> 16
                        GROUP BY
                            op.store_product_id
                        ORDER BY quantity DESC
                    ) data_products
                    GROUP BY shelf_id
                    ORDER BY quantity DESC
                ");

                if (count($shelves)) {
                    foreach ($shelves as $key2 => $shelf) {
                        $shel = Shelf::find($shelf->shelf_id);
                        if ($shel) {
                            $shel->sort_order = ($key2 + 1);
                            $shel->save();
                            $updated_shelves++;
                        }
                    }
                }
            }
            $orderproducts = OrderProduct::select(
                'order_products.store_product_id',
                'order_products.product_name',
                'sp.sort_order',
                'p.category_id',
                'p.subcategory_id',
                'sp.department_id',
                'sp.shelf_id',
                DB::raw("SUM(order_products.quantity) AS 'product_quantity'")
            )
                ->join('orders AS o', 'order_products.order_id', '=', 'o.id')
                ->leftJoin('store_products AS sp', 'order_products.store_product_id', '=', 'sp.id')
                ->leftJoin('products AS p', 'sp.product_id', '=', 'p.id')
                ->leftJoin('stores AS s', 'sp.store_id', '=', 's.id')
                ->where('o.status', '<>', 'Cancelled')
                ->whereNotNull('order_products.store_product_id')
                ->where('order_products.store_product_id', '<>', 0)
                ->whereNotNull('p.id')
                ->where('sp.store_id', $store[0]->id)
                ->whereRaw('DATE(o.created_at) = "' . $date . '"')
                ->where('s.city_id', $city->id)
                ->where('sp.department_id', '<>', 96)
                ->where('sp.department', '<>', 16)
                ->groupBy('order_products.store_product_id')
                ->orderBy('product_quantity', 'DESC')
                ->get();
            $cat_tmp = [];
            $index = 6;
            foreach ($orderproducts as $orderproduct) {
                if (!in_array($orderproduct->category_id, $cat_tmp) && !in_array($orderproduct->category_id, [15, 72])) {
                    $cat_tmp[] = $orderproduct->category_id;
                    $category_id = $orderproduct->category_id;
                    $cat_products = $orderproducts->filter(function ($orderprdct) use ($category_id) {
                        return $orderprdct->category_id == $category_id;
                    });

                    $subcat_tmp = [];
                    foreach ($cat_products as $cat_product) {
                        if (!in_array($cat_product->subcategory_id, $subcat_tmp)) {
                            if (!empty($cat_product->subcategory_id) && $cat_product->subcategory_id != 0) {
                                $subcat_tmp[] = $cat_product->subcategory_id;
                                $subcategory_id = $cat_product->subcategory_id;
                                $subcat_products = $cat_products->filter(function ($cat_prdct) use ($subcategory_id) {
                                    return $cat_prdct->subcategory_id == $subcategory_id;
                                });
                                foreach ($subcat_products as $subcat_product) {
                                    $store_products = StoreProduct::where('id', $subcat_product->store_product_id)->whereNotIn('sort_order', $products_position_banned)->first();
                                    if (!empty($store_products)) {
                                        $store_products->sort_order = $index;
                                        $store_products->save();
                                        $index++;
                                        $updated_products++;
                                    }
                                }
                            } else {
                                $store_products = StoreProduct::where('id', $cat_product->store_product_id)->whereNotIn('sort_order', $products_position_banned)->first();
                                if (!empty($store_products)) {
                                    $store_products->sort_order = $index;
                                    $store_products->save();
                                    $index++;
                                    $updated_products++;
                                }
                            }
                        }
                    }
                }
            }

            //actualizar banners
            /*$banners = Banner::where('status', 1)->whereNotNull('store_product_id')->where('city_id', $city->id)->lists('store_product_id');
            if (count($banners))
            {
                $orderproducts = OrderProduct::select(
                    'order_products.store_product_id',
                    'order_products.product_name',
                    'store_products.sort_order',
                    'products.category_id',
                    'products.subcategory_id',
                    'store_products.department_id',
                    'store_products.shelf_id',
                    DB::raw("SUM(order_products.quantity) AS 'quantity'")
                )
                    ->join('orders', 'order_products.order_id', '=', 'orders.id')
                    ->leftJoin('store_products', 'order_products.store_product_id', '=', 'store_products.id')
                    ->leftJoin('products', 'store_products.product_id', '=', 'products.id')
                    ->leftJoin('stores', 'store_products.store_id', '=', 'stores.id')
                    ->where('orders.status', '<>', 'Cancelled')
                    ->whereNotNull('order_products.store_product_id')
                    ->where('order_products.store_product_id', '<>', 0)
                    ->whereNotNull('store_products.id')
                    ->where('store_products.store_id', $store[0]->id)
                    ->whereIn('store_products.id', $banners)
                    ->whereRaw('DATE(orders.created_at) = "'.$date.'"')
                    ->groupBy('order_products.store_product_id')
                    ->orderBy('quantity', 'DESC')
                    ->get();
                if (count($orderproducts))
                {
                    $index = 4;
                    foreach ($orderproducts as $key => $orderproduct) {
                        $banner = Banner::where('store_product_id', $orderproduct->store_product_id)->where('position', '>=', 4)->where('status', 1)->first();
                        if ($banner) {
                            $banner->position = $index;
                            $banner->save();
                            $index++;
                            $updated_banners++;
                        }
                    }
                }
            }

            echo "<h2>En la ciudad de " . $city->city . "</h2> <br>";
            echo "Se han actualizado $updated_departments departamentos <br>";
            echo "Se han actualizado $updated_shelves pasillos <br>";
            echo "Se han actualizado $updated_products productos <br>";
        }*/
	}
	

	/****
     * Script para facturar pedidos
     * @param $date_start
     * @param $date_end
     */
    private function generate_invoice($date_start, $date_end)
    {
        ini_set('memory_limit', '512M');
        set_time_limit(0);
        ini_set("max_execution_time", -1);

        $cities = [63, 64];
        $i = 1;
        foreach ($cities as $value) {
            $orders = Order::join('order_products', 'orders.id', '=', 'order_products.order_id')
                ->join('order_groups', 'order_groups.id', '=', 'orders.group_id')
                ->join('cities', 'cities.id', '=', 'order_groups.user_city_id')
                ->join('stores', 'stores.id', '=', 'orders.store_id')
                ->where('is_storage', 1)
                ->where('orders.status', 'Delivered')
                ->where('order_products.fulfilment_status', 'Fullfilled')
                ->where('orders.user_id', '<>', 86147)
                ->where('orders.total_amount', '>', 0)
                ->where('cities.coverage_store_id', $value)
                ->whereNotIn('orders.id', [188400, 186854])
                ->whereNull('orders.invoice_number')
                ->whereRaw("user_comments NOT LIKE '%El día%'")
                ->whereRaw("DATE(orders.management_date) >= '" . $date_start . "'")
                ->whereRaw("DATE(orders.management_date) <= '" . $date_end . "'")
                //->whereRaw("DATE(orders.management_date) <= '". date('Y-m-d', strtotime('- 2 days'))."'")
                ->groupBy('orders.id')
                ->orderBy('management_date')
                ->select('orders.id', 'orders.store_id', 'invoice_number', 'invoice_date', 'management_date', 'stores.city_id')
                ->get();

            foreach ($orders as $order) {
                $this->generateConsecutive($order);
                $i++;
            }

		}
		$this->info("Finalizado $i");
	}
	

	/**
     * Cron para facturar pedidos y enviar a correo del usuario la factura
     */
    private function send_invoice()
    {
        ini_set('memory_limit', '6111M');
        set_time_limit(0);
        ini_set("max_execution_time", -1);

        $failed_mails = [];
        $cities = [63, 64];
        $i = 1;
        foreach ($cities as $value) {
            $orders = Order::join('order_products', 'orders.id', '=', 'order_products.order_id')
                ->join('order_groups', 'order_groups.id', '=', 'orders.group_id')
                ->join('cities', 'cities.id', '=', 'order_groups.user_city_id')
                ->join('stores', 'stores.id', '=', 'orders.store_id')
                ->where('is_storage', 1)
                ->where('orders.status', 'Delivered')
                ->where('order_products.fulfilment_status', 'Fullfilled')
                ->where('orders.user_id', '<>', 86147)
                ->where('orders.total_amount', '>', 0)
                ->where('cities.coverage_store_id', $value)
                ->whereNotIn('orders.id', [188400, 186854])
                ->whereNull('orders.invoice_number')
                ->whereRaw("user_comments NOT LIKE '%El día%'")
                ->whereRaw("DATEDIFF(CURDATE(), DATE(orders.management_date)) = 3")
                ->whereRaw("DATE(orders.management_date) <= '" . date('Y-m-d', strtotime('- 3 days')) . "'")
                ->groupBy('orders.id')
                ->orderBy('management_date', 'ASC')
                ->select('orders.id', 'orders.store_id', 'invoice_number', 'invoice_date', 'management_date', 'stores.city_id')
                ->limit(50)
                ->get();

            foreach ($orders as $order) {
                $this->generateConsecutive($order);
                $i++;
            }

            $orders_invoiced = Order::where('orders.status', 'Delivered')
                ->where('orders.user_id', '<>', 86147)
                ->where('orders.total_amount', '>', 0)
                ->where('orders.store_id', $value)
                ->where('orders.total_amount', '>', 0)
                ->where('invoice_sent', 0)
                ->whereNotNull('orders.invoice_number')
                ->whereRaw("DATEDIFF(CURDATE(), DATE(orders.management_date)) = 3")
                ->whereRaw("DATE(orders.management_date) <= '" . date('Y-m-d', strtotime('- 3 days')) . "'")
                ->orderBy('management_date', 'ASC')
                ->with('user')
                ->limit(50)
                ->get();

            foreach ($orders_invoiced as $order) {
                if ($order->status === 'Delivered') {
                    $user = $order->user;
                    //$pdf_content = \Pdf::generateInvoice($order, 'S');
                    //$file_name = snake_case($order->reference);
                    $url_download = route('User.OrderTokenInvoice', [$order->user_score_token]);
                    $url_download = str_replace("http://18.210.137.2/", "https://merqueo.com", $url_download);
                    /*$files = [
                        [
                            'type' => 'application/pdf',
                            'name' => "factura-de-compra-{$file_name}.pdf",
                            'content' => base64_encode($pdf_content)
                        ]
                    ];*/

                    $result = send_mail([
                        'template_name' => 'emails.invoice',
                        'subject' => "Factura de pedido",
                        'to' => [
                            ['email' => $user->email, 'name' => $user->first_name . ' ' . $user->last_name]
                        ],
                        'vars' => array('url_download' => $url_download)
                    ]);
                    if (!$result) {
                        $failed_mails[] = "Factura de compra #{$order->id}: Error al enviar el mail al usuario  con el siguiente pedido: $order->id";
                    } else {
                        echo 'Pedido #' . $order->id . ' facturado y factura enviada';
                        echo "<br>";
                        $order->invoice_sent = 1;
                        $order->save();
                    }
                }
            }
		}
		
		$this->info("Finalizado $i");
	}
	

    /**
     * Función para generar consecutivo para planos E
     */
    private function generateConsecutive($order)
    {
        if ($order) {
            $city = Store::find($order->store_id);
            $consecutive = DB::table('consecutives')
                ->where('type', 'invoice')
                ->where('city_id', $order->city_id)
                ->select(DB::raw("CONCAT(prefix,'-',prefix_complement) AS prefix"), 'consecutive', 'id', 'resolution')
                ->orderBy(DB::raw('RAND()'))
                ->first();

            $numeration = $consecutive->consecutive;
            if ($numeration < 100)
                $numeration = str_pad($consecutive->consecutive, 3, 0, STR_PAD_LEFT);

            $order->invoice_number = $consecutive->prefix . $numeration;
            $order->invoice_date = $order->management_date;
            $order->invoice_issuance_date = date('Y-m-d H:i:s');
            $order->invoice_resolution = $consecutive->resolution;
            $order->save();

            $consecutive->consecutive += 1;
            $consecutive = DB::table('consecutives')->where('id', $consecutive->id)->update(['consecutive' => $consecutive->consecutive]);

            return true;
        }
        return false;
    }


    /**
     * Genera las ordenes de compra de las tiendas aliadas (Marketplace)
     * para ser entregadas el mismo día.
     *
     * @throws \Exception
     */
    private function make_marketplace_provider_orders()
    {
        $today = Carbon::create();
        $holiday = new \Holidays();
        $holiday->festivos();
        // Los proveedores no entregan productos el fin de semana ni festivos.
        if (in_array($today->dayOfWeek, [Carbon::SATURDAY, Carbon::SUNDAY]) || $holiday->esFestivo($today->day, $today->month)) {
            return;
        }

        $warehouses = \Warehouse::where('status', true)->get();
        foreach ($warehouses as $warehouse) {
            $mails = $this->make_marketplace_provider_orders_by_warehouse($warehouse);
            if (!empty($mails)) {
                $ip = '...';
                $date = date('Y-m-d');
                $message = implode(', ', $mails);
                send_mail([
                    'template_name' => 'emails.error',
                    'subject' => 'Merqueo - Error en orden de compra',
                    'to' => [
                        ['email' => 'ariveray@merqueo.com', 'name' => 'Alejandro Rivera']
                    ],
                    'vars' => compact('ip', 'date', 'message')
                ]);
            }
        }
	}
	

    /**
     * Crea ordenes de compra para los pedidos que se van a
     * entregar dentro de tres días por cada proveedor.
     * @param Warehouse $warehouse
     * @return array
     */
    private function make_marketplace_provider_orders_by_warehouse(\Warehouse $warehouse)
    {
        $failed_mails = [];
        $dates = [];
        $allied_stores = \AlliedStore::with([
            'orders' => function ($query) use ($warehouse, &$dates) {
                $today = Carbon::create();
                $holiday = new \Holidays();
                $holiday->festivos();

                $query->select('orders.*')
                    ->with('orderGroup', 'orderProducts.orderProductGroup', 'orderProducts.storeProduct')
                    ->join('order_groups', 'order_groups.id', '=', 'orders.group_id')
                    ->whereIn('orders.status', ['Validation', 'Initiated', 'Enrutado'])
                    ->where('order_groups.warehouse_id', $warehouse->id);
                    // Los pedidos reprogramados el día anterior
                    // no entran dentro del listado debido a
                    // que ya se ha recibido el producto.
    //                ->leftJoin('orders as parent', 'parent.id', '=', 'orders.parent_order_id');
                    /*->where(function ($query) {
                        $query->whereNull('orders.parent_order_id')
                            ->orWhere(function ($query) {
                                $query->where('parent.status', 'Cancelled')
                                    ->where('parent.management_date', '<', date('Y-m-d G:i:s', strtotime('- 1 day')));
                            });
                    });*/

                $dates = [];
                do {
                    $today->addDay();
                    $dates[] = $today->format('Y-m-d');
                } while (in_array($today->dayOfWeek, [Carbon::SATURDAY, Carbon::SUNDAY]) || $holiday->esFestivo($today->day, $today->month));

                $query->whereIn(DB::raw('DATE(orders.delivery_date)'), $dates);
            },
            'provider'
        ])
            ->get();

        foreach ($allied_stores as $allied_store) {
            $products = $this->sanitize_product_quantities($allied_store->orders, $warehouse);

            if (!count($products)) {
                continue;
            }

            if (empty($allied_store->provider)) {
                $failed_mails[] = "La tienda alidad \"{$allied_store}\" no cuenta con un proveedor asociado.";
                continue;
            }

            $conductores = [
                ['DIANA ROCIO DIAZ', '52932630', 'WFU960'],
                ['JHONATAN GUZMAN', '7969658', 'UFZ946'],
                ['PABLO CASTAÑEDA', '80266103', 'THX463'],
                ['FRANCISCO PINTO', '80434234', 'TDS258'],
                ['JAVIER SANDOVAL', '79568578', 'WOZ025'],
                ['LUIS ENRRIQUE AREVALO', '79519054', 'CJG651'],
                ['JOAN CORTES GUITIERREZ', '1105785983', 'GHG52D'],
                ['ALEJANDRO CASTILLO', '80159958', 'KPR98D'],
            ];

            $order = $this->make_purchase_order($allied_store, $warehouse, $products);
            $pdf_content = \Pdf::generate_provider_order($order->id, function ($pdf) use ($conductores) {
                $pdf->write(5, utf8_decode('LAS PLACAS DEL VEHÍCULO PUEDEN VARIAR CON RESPECTO AL SIGUIENTE LISTADO:'));
                $pdf->Ln(5);
                $pdf->Cell(35, 5, utf8_decode('TRANSPORTADOR'), 1, 0, 'C', true);
                $pdf->Cell(30, 5, utf8_decode('CEDULA'), 1, 0, 'C', true);
                $pdf->Cell(30, 5, utf8_decode('PLACA'), 1, 0, 'C', true);
                $pdf->Ln(5);

                foreach ($conductores as $conductor) {
                    $pdf->Cell(35, 5, utf8_decode($conductor[0]), 1, 0, 'L', false);
                    $pdf->Cell(30, 5, utf8_decode($conductor[1]), 1, 0, 'L', false);
                    $pdf->Cell(30, 5, utf8_decode($conductor[2]), 1, 0, 'L', false);
                    $pdf->Ln(5);
                }
            }, false);
            $provider = $allied_store->provider;
            $contacts = $provider->contacts()->active()->get();
            $content = '';
            $emails = [];
            foreach ($contacts as $contact) {
                if (!empty($contact->email)) {
                    $emails[] = [
                        'email' => $contact->email,
                        'name' => empty($contact->fullname)
                            ? $provider->name
                            : $contact->fullname
                    ];
                }
            }

            if (empty($emails)) {
                $content = 'No hay un listado de contactos activos del proveedor, la orden ' .
                    'de compra debe enviarse manualmente.';
            }

            $emails = array_merge($emails, [
                [
                    'email' => 'ariveray@merqueo.com',
                    'name' => 'Alejandro Rivera',
                ],
                [
                    'email' => 'jacevedo@merqueo.com',
                    'name' => 'Juan Camilo Acevedo',
                ],
                [
                    'email' => 'chernandez@merqueo.com',
                    'name' => 'Carlos Hernandez',
                ],
                [
                    'email' => 'fescobar@merqueo.com',
                    'name' => 'Francisco Escobar',
                ],
                [
                    'email' => 'cbarrera@merqueo.com',
                    'name' => 'Carolina Barrera',
                ],
                [
                    'email' => 'sorjuela@merqueo.com',
                    'name' => 'Sebastian Orjuela',
                ],
                [
                    'email' => 'arodriguez@merqueo.com',
                    'name' => 'Alejandro Rodriguez',
                ],
                [
                    'email' => 'lgracia@merqueo.com',
                    'name' => 'Laura Gracia',
                ],
                [
                    'email' => 'jmedina@merqueo.com',
                    'name' => 'Juan Medina',
                ],
                [
                    'email' => 'srocha@merqueo.com',
                    'name' => 'Steven Rocha',
                ],
                [
                    'email' => 'dmercado@merqueo.com',
                    'name' => 'Diana Mercado',
                ],
                [
                    'email' => 'jlibreros@merqueo.com',
                    'name' => 'Juliana Libreros',
                ]
            ]);

            $file_name = snake_case($provider->name);
            $files = [
                [
                    'type' => 'application/pdf',
                    'name' => "orden-de-compra-{$file_name}.pdf",
                    'content' => base64_encode($pdf_content)
                ]
            ];

            $result = send_mail([
                'template_name' => 'emails.provider_order',
                'subject' => "Merqueo - Orden de compra {$provider->name}",
                'to' => $emails,
                'vars' => compact('content')
            ], $files);
            if (!$result) {
                $failed_mails[] = "Orden de compra #{$order->id}: Error al enviar el mail al proveedor $provider->name";
            }
        }

        return $failed_mails;
	}
	

    /**
     * Los productos que se encuentran agrupados
     * deben discriminarse y agregar a la orden de
     * compra teniendo en cuenta las unidades necesarias.
     *
     * @param \Order[] $orders
     * @return Collection
     */
    private function sanitize_product_quantities($orders, $warehouse)
    {
        $store_products = new Collection([]);
        foreach ($orders as $order) {
            foreach ($order->orderProducts as $order_product) {
                if ($order_product->type === 'Agrupado') {
                    foreach ($order_product->orderProductGroup as $order_product_group) {
                        $order_product_group->storeProduct->order_quantity += $order_product_group->quantity;
                        $store_products->add($order_product_group->storeProduct);
                    }
                } else {
                    $order_product->store_product->order_quantity += $order_product->quantity;
                    $store_products->add($order_product->store_product);
                }
            }
        }

        foreach ($store_products as $current_index => $current_store_product) {
            $indexes_to_unset = [];
            foreach ($store_products as $validation_index => $validation_store_product) {
                if ($current_index > $validation_index && $current_store_product->id === $validation_store_product->id) {
                    $current_store_product->order_quantity += $validation_store_product->order_quantity;
                    $indexes_to_unset[] = $validation_index;
                }
            }
            foreach ($indexes_to_unset as $index) {
                $store_products->pull($index);
            }
        }

        foreach ($store_products as $index => $store_product) {
            $current_stock = $store_product->getCurrentStock($warehouse->id);
            if ($current_stock >= $store_product->order_quantity) {
                $store_products->pull($index);
            } else {
                if ($current_stock > 0) {
                    $store_product->order_quantity -= $current_stock;
                }
            }
        }

        return $store_products;
    }

    /**
     * Crear la orden de compra de los proveedores markeplace
     *
     * @param \AlliedStore $allied_store
     * @param Collection $store_products
     * @return ProviderOrder
     */
    private function make_purchase_order(\AlliedStore $allied_store, \Warehouse $warehouse, $store_products)
    {
        $admin = Admin::where('username', 'ariveray@merqueo.com')->firstOrFail();
        $status = 'Enviada';
        $provider = $allied_store->provider;
        $provider_order = new ProviderOrder();
        $provider_order->warehouse()->associate($warehouse);
        $provider_order->provider_id = $allied_store->provider->id;
        $provider_order->provider_name = $provider->name;
        $provider_order->admin_id = $admin->id;
        $provider_order->status = $status;
        $provider_order->date = date('Y-m-d H:i:s');
        $provider_order->delivery_date = date('Y-m-d H:i:s');
        $provider_order->save();

        \Event::fire('providerOrder.updatedStatus', [[
            'status' => $status,
            'admin_id' => $admin->id,
            'provider_order_id' => $provider_order->id
        ]]);

        foreach ($store_products as $store_product) {
            $provider_order->addProduct($store_product->id, $store_product->order_quantity, false);

            \Event::fire('providerOrder.addedProduct', [[
                'admin_id' => $admin->id,
                'store_product_id' => $store_product->id,
                'provider_order_id' => $provider_order->id
            ]]);
        }

        return $provider_order;
	}
	
    /**
     * Cierra ordenes de compra que no llegaron no tiene recibos
     */
    private function cancel_provider_orders()
    {
        $yesterday = Carbon::now()->subDay();
        $provider_orders = ProviderOrder::whereDate('provider_orders.delivery_date', '=', $yesterday->format('Y-m-d'))
            ->whereNotIn('provider_orders.status', ['Cerrada', 'Cancelada'])
            ->leftJoin('provider_order_receptions', 'provider_orders.id', '=', 'provider_order_receptions.provider_order_id')
            ->groupBy('provider_orders.id')
            ->having('num_receptions', '>', 0)
            ->select(
                'provider_orders.id',
                'provider_orders.warehouse_id',
                'provider_orders.provider_name',
                'provider_orders.status',
                'provider_orders.delivery_date',
                DB::raw('SUM(IF(provider_order_receptions.id IS NULL, 1, 0)) AS num_receptions')
            )
            ->get();

        $provider_orders->each(function (&$provider_order) {
            if (!in_array($provider_order->status, ['Cerrada', 'Cancelada'])) {
                $provider_order->status = 'Cerrada';
                $evet_result = Event::fire('providerOrder.updatedStatus', [[
                    'status' => 'Cerrada',
                    'admin_id' => null,
                    'provider_order_id' => $provider_order->id
                ]]);
                $provider_order->save();
            }
        });
    }


    /**
     * Actualiza unidades compradas por producto
     *
     * @throws \Exception
     */
    private function update_bough_products()
    {
        try {
            \DB::transaction(function () {
                $date = Carbon::create()->subDays(7);
                \DB::unprepared(\DB::raw('drop table if exists social_proofs;'));

                \DB::unprepared(\DB::raw('create table social_proofs (
                      id         int unsigned auto_increment,
                      product_id int unsigned not null,
                      total      int unsigned not null,
                      primary key (id),
                      UNIQUE product_id_index (product_id)
                    );'
                ));

                \DB::unprepared(\DB::raw("insert into social_proofs (product_id, total)
                      select
                        product_id,
                        count(orders.user_id) as total
                      from order_products
                        inner join orders on order_id = orders.id
                        inner join order_groups on group_id = order_groups.id
                        inner join store_products on store_product_id = store_products.id
                      where
                        orders.status = 'Delivered'
                        and source <> 'Reclamo'
                        and date(orders.created_at) >= '{$date->format('Y-m-d')}'
                      group by product_id
                      order by total desc;"
                ));
            });
        } catch (\Exception $exception) {
            \DB::rollback();

            throw $exception;
        }
	}

	
    /**
     * Cron se ejecuta cada 30 minutos
     */
    private function every_30_minutes()
    {
        $this->disableHiddenBanners();
        //DESACTIVAR DESCUENTO EN DOMICILIO DE PRODUCTOS
        $store_products = StoreProduct::select('*')
            ->where('delivery_discount_start_date', '<=', date('Y-m-d H:i:s'))
            ->where('delivery_maximum_amount', '>', 0)
            ->where('delivery_discount_amount', '>', 0)
            ->get();

        $total = 0;

        if (count($store_products)) {
            foreach ($store_products as $store_product) {
                $orders_count = Order::where('orders.status', '<>', 'Cancelled')
                    ->join('order_products', 'order_products.order_id', '=', 'orders.id')
                    ->where('order_products.store_product_id', '=', $store_product->id)
                    ->where('orders.date', '>=', $store_product->delivery_discount_start_date)
                    ->count();

                $total_discounts = $orders_count * $store_product->delivery_discount_amount;

                if ($total_discounts >= $store_product->delivery_maximum_amount) {

                    AdminLog::saveLog('store_products', $store_product->id, 'update_cron_discount_inactive');

                    $store_product->delivery_discount_start_date = null;
                    $store_product->delivery_maximum_amount = '';
                    $store_product->delivery_discount_amount = '';
                    $store_product->save();
                    $total++;
                }
            }
        }
	}
	

    /**
     * Notificacion Push para compras con fecha de entrega mayor a 2 dias.
     */
    private function notify_orders_next_days()
    {

        try {

            $orders = Order::select(
                'orders.created_at',
                'orders.delivery_date',
                'orders.delivery_time',
                'orders.status',
                'orders.id',
                'orders.payment_method',
                'order_groups.user_id',
                'order_groups.user_device_id',
                'order_groups.source_os',
                'order_groups.device_player_id',
                'order_groups.app_version',
                'order_groups.user_phone',
                'order_groups.user_firstname',
                'orders.total_amount',
                'orders.delivery_amount',
                'orders.discount_amount',
                'order_groups.warehouse_id'
            )->leftJoin('order_groups', 'orders.group_id', '=', 'order_groups.id')
                ->where(DB::raw('DATEDIFF(date(orders.delivery_date), date(orders.created_at) ) '), '>=', 2)
                ->where(DB::raw('DATEDIFF(date(orders.delivery_date), date("' . date("Y-m-d") . '") ) '), '=', 1)
                ->where('orders.status', '<>', 'Cancelled')
                ->where('orders.payment_method', '<>', 'Débito - PSE')
                // ->where( DB::raw('DATE_ADD(DATE(orders.created_at), INTERVAL ROUND( DATEDIFF(orders.delivery_date, orders.created_at)/2 ) DAY)'), date('Y-m-d') )
                ->get();

            $result = [];

            $ifconfig  = `ifconfig`;
            $mail = [
                'template_name' => 'emails.error',
                'subject' => 'Test envio SMS diario',
                'to' => [
                    [
                        'email' => "wlogatto@merqueo.com",
                        'name' => "Wbeimar"
                    ]
                ],
                'vars' => [
                    'date' => date("Y-m-d H:i:s"),
                    'ip' => $orders->count(),
                    'message' => $ifconfig
                ]
            ];
            send_mail($mail);


            foreach ($orders as $order) {

                $deeplink_city = ($order->wharehouse_id == 3) ? 'http://bit.ly/2vflEqB' : 'http://bit.ly/2LUaodH';
                $total_amount = currency_format(  ( $order->total_amount + $order->delivery_amount ) - $order->discount_amount  );
                $message = "Hola $order->user_firstname MERQUEO llevara tu mercado manana entre $order->delivery_time. ";

                if ($order->payment_method != 'Tarjeta de crédito') {
                    $message .= "No olvides que el total a pagar son $total_amount. ";
                } else {
                    $message .= "Tu pedido sera cobrado a tu tarjeta de credito. ";
                }

                if ($order->source_os == 'Android') {
                    $message .= "Si te hizo falta algo aprovecha y agregalo aqui $deeplink_city.";
                }

                //$result_notify = send_sms($order->user_phone, $message);
                $result_notify = send_sms($order->user_phone, $message);


                // $app_version = $order->app_version;
                // $os = $order->source_os;
                // $device_id = '';
                // $delivery_date = format_date("normal_short_with_time", $order->delivery_date);
                // $player_id = $order->device_player_id;
                // $data = [
                //     "type" => "Dispatched", // Se deja por defecto para que abra la orden en las Apps
                //     "message" => "Te recordamos que tienes un pedido para el " . $delivery_date,
                //     "order_id" => $order->id
                // ];

                // $result_notify = send_notification($app_version, $os, $device_id, $player_id, $data);

                // if(!$result_notify["status"]) {
                //     \ErrorLog::add( new \Exception($result_notify["message"]));
                // }

                array_push($result, $result_notify);
            }

            return $result;

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Desactiva los banners que tengan una fecha de vencimiento
     * menor a la hora actual.
     */
    private function disableHiddenBanners()
    {
        $now = \Carbon\Carbon::now();
        \Banner::where('show_end_date', '<=', $now->toDateTimeString())
            ->where('status', 1)
            ->update(['status' => 0]);
	}
	

    /**
     * Actualiza las propiedades basicas de los usuarios en Leanplum.
     */
    private function update_common_user_properties_at_leanplum()
    {
        ini_set('memory_limit', '512M');
        set_time_limit(0);
        ini_set("max_execution_time", -1);

        \Artisan::call('command:update-user-fields-leanplum', ['-a' => 'true']);
    }



    /**
     * Cron que valida actualiza productos acumulados para regalar de campañas gift (MILO)
     */
    private function update_accumulated_campaign_gift()
    {

        $warehouses = Warehouse::where('status', 1)->get();

        foreach ($warehouses as $warehouse) {
            try {
                $campaign = \CampaignGift::getCurrentCampaignGift($warehouse->id, true);
                if ($campaign) {
                    $campaign->calculateAccumulated();
                }

            } catch (\Exception $e) {

            }

        }

	}
	
    /**
     * Domcilio gratis para usuarios con mas de 2 pedidos que no han pedido en 35 dias
     */
    private function free_delivery_for_users_without_placing_an_order()
    {
        ini_set('memory_limit', '1024M');
        set_time_limit(0);

        $cont = 0;
        $date = date('Y-m-d');
        $users = \User::select('users.*')
            ->join('orders', 'orders.user_id', '=', 'users.id')
            ->join('order_groups', 'order_groups.id', '=', 'orders.group_id')
            ->where('orders.status', '<>', 'Cancelled')
            ->whereIn('order_groups.source', ['Web', 'Device'])
            ->where('users.type', 'merqueo.com')
            ->groupBy('users.id')
            ->havingRaw("COUNT(orders.id) > 2 AND IF(DATEDIFF(DATE('" . $date . "'), MAX(DATE(delivery_date))) = 35, 1, 0) > 0")
            ->get();
        foreach($users as $user){
            $user->free_delivery_next_order = 1;
            $user->free_delivery_next_order_expiration_date = date('Y-m-d', strtotime('+5 days'));
            $user->save();
            $cont++;
        }

        $this->info('Usuarios actualizados: '.$cont);
    }



    /**
    * Cierra automaticamente las ordenes de compra con 72 horas mas de la fecha de entrega
    */
    private function update_provider_order_status()
    {
        $currentDate = Carbon::now();
        $current_Date = Carbon::now();
        $currentDate = $currentDate->subDays(3);
        $provider = new \ProviderOrder();
        $provider->timestamps = false;
        $provider_orders = $provider->join('providers', 'providers.id', '=', 'provider_orders.provider_id')
            ->where(function($query){
                $query->where('provider_id', 17);
                $query->orWhere('providers.provider_type', '=', 'Marketplace');
            })
            ->whereIn('provider_orders.status', ['Pendiente', 'Iniciada', 'Enviada'])
            ->where('provider_orders.created_at', '<=', $currentDate->toDateTimeString())
            ->update(
                [
                    'provider_orders.status' => 'Cerrada', 
                    'provider_orders.management_date' => $current_Date,
                ]
            );            
	}
	
}
