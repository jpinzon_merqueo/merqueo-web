<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class UsersNewSiteSegmentation extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'command:new-site-segmentation';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * @var int
     */
    protected $numberUsersWillChange = 0;

    /**
     * @var \Symfony\Component\Console\Helper\ProgressBar
     */
    private $bar;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $limitChunk = 1000;
        $currentAmount = $this->getTotalTestUsers();
        $totalUsers = $this->totalAvailableUsers();
        $percentage = floatval($this->argument('percentage'));
        $currentPercentage = $currentAmount / $totalUsers;
        $targetPercentage = $percentage / 100 - $currentPercentage;
        $this->numberUsersWillChange = round($totalUsers * $targetPercentage);

        if ($this->numberUsersWillChange < 1) {
            throw new Exception("La cantidad de usuarios a modificar no es corrcta {$this->numberUsersWillChange}");
        }

        $this->bar = new \Symfony\Component\Console\Helper\ProgressBar($this->output, $this->numberUsersWillChange);
        $this->bar->start();
        \DB::beginTransaction();

        while ($this->numberUsersWillChange > 1) {
            $currentChunk = $this->numberUsersWillChange > $limitChunk
                ? $limitChunk : $this->numberUsersWillChange;

            $users = $this->usersQuery()
                ->where('enable_new_site', 0)
                ->orderBy(\DB::raw('RAND()'))
                ->limit($currentChunk)
                ->get();

            foreach ($users as $user) {
                $this->bar->advance();
                $user->enable_new_site = 1;
                $user->save();
            }

            $this->numberUsersWillChange -= $currentChunk;
        }

        \DB::commit();
        $this->bar->finish();
    }

    /**
     * @return int
     */
    private function getTotalTestUsers()
    {
        return $this->usersQuery()
            ->where('enable_new_site', 1)
            ->count();
    }

    /**
     * @return int
     */
    private function totalAvailableUsers()
    {
        return $this->usersQuery()
            ->count();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder
     */
    private function usersQuery()
    {
        return User::where('status', 1);
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['percentage', InputArgument::REQUIRED, 'Users that need will have enabled the new site'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }

}
