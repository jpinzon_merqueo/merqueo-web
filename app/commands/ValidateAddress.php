<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ValidateAddress extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'command:validate_address';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Validacion de direcciones guardadas de los usuarios';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{

		$addreses = UserAddress::limit(10)->get();

		foreach($addreses as $address){

			$address = $address->address;
			$return_address = UserAddress::isCorrectAddress($address);
			
			$this->info("> Direccion : ".$address);

			if($return_address){
				// $this->info("...OK");
			}else{
				$this->error("...ERROR");
			}

		}

	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			//array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			//array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
