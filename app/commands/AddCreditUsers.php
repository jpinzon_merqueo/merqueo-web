<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use \Symfony\Component\Console\Helper\ProgressBar;

class AddCreditUsers extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'command:add-credit-users';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Asigna creditos a usuarios.';

    /**
     * @var ProgressBar
     */
    private $progressBar;

    /**
     * @var array
     */
    private $errors;

    /**
     * @var int
     */
    private $cont;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $file = $this->argument('file');
        \Excel::load($file, function ($reader) {
            $result = $reader->get();
            $this->progressBar = new ProgressBar($this->getOutput(), $result->count());
            $this->progressBar->setFormat(' %current%/%max% [%bar%] %percent:3s%% %elapsed:6s%/%estimated:-6s% %memory:6s%');
            $this->progressBar->start();
            try {
                foreach ($result as $row) {
                    $this->progressBar->advance();
                    if (count($row) >= 2) {
                        if ($user = User::find(trim($row[0]))) {
                            UserCredit::addCredit($user, trim($row[1]), $this->argument('expiration_date'), 'marketing');
                            $this->cont++;
                        }
                    }
                }
                $this->info('
Usuarios afectados: '.$this->cont);
            } catch (Exception $e){
                $this->errors[] = [
                    'ticket' => $row[0].';'.$row[1],
                    'message' => $e->getMessage(),
                    'trace' => $e->getTraceAsString(),
                ];
            }
        });
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['file', InputArgument::REQUIRED, 'Ruta del archivo.'],
            ['expiration_date', InputArgument::REQUIRED, 'Fecha de expiracion.'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }

}