<?php

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;
use Merqueo\Leanplum\LeanplumMultiClient;
use Merqueo\Leanplum\SetUserAttributes;
use Symfony\Component\Console\Helper\ProgressBar;

/**
 * Class UpdateUserStatusLeanplum
 */
class UpdateUserStatusLeanplum extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'command:update-user-status-leanplum';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Actualiza el estado de los campos.';

    /**
     * @var ProgressBar
     */
    protected $progressBar;

    /**
     * @var int
     */
    protected $totalUsers;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->client = app(LeanplumMultiClient::class);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $query = $this->getUserQuery();
        $this->totalUsers = $query->count();
        $this->progressBar = new ProgressBar($this->getOutput(), $this->totalUsers);
        $this->progressBar->start();
        $query->chunk(5000, function ($items) {
            $this->client->clear();
            foreach ($items as $item) {
                $this->updateUser($item);
                $this->client->setUserAttributes(new SetUserAttributes($item, $this->getFieldsToUpdate()));
                $this->progressBar->advance();
            }

            try {
                retry(5, function () {
                    $this->client->send();
                }, 2);
            } catch (Exception $exception) {
                $this->getOutput()->write("Error in batch #{$this->progressBar->getProgress()}");
                return;
            }

            $wasValid = $this->client->wasValid();

            if (!$wasValid) {
                $date = Carbon::create();
                $fileName = "/tmp/{$date->format('Y-m-dH:i:s')}.txt";
                $this->getOutput()->write("Found some errors, log file saved at: '$fileName''");
                file_put_contents($fileName, json_encode($this->client->getErrorMessages()));
            }
        });

        if (method_exists($this, 'onFinished')) {
            $this->onFinished();
        }

        $this->progressBar->finish();
    }

    /**
     * @return array
     */
    protected function getFieldsToUpdate()
    {
        return ['status', 'send_advertising'];
    }

    /**
     * @param User $user
     * @return User
     */
    protected function updateUser(User $user)
    {
        return $user;
    }

    /**
     * @return Builder
     */
    protected function getUserQuery()
    {
        return User::query();
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}
