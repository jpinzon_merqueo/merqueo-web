<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class SendInvoices extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'command:send-invoices';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Factura pedidos y envia email para descargar archivos';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        $output = new Symfony\Component\Console\Output\ConsoleOutput();
        $failed_mails = [];
        $cities = [63, 64];
        $i = $j = 0;

        foreach ($cities as $value) {
            /*$orders = \Order::join('order_products', 'orders.id', '=', 'order_products.order_id')
                ->join('order_groups', 'order_groups.id', '=', 'orders.group_id')
                ->join('cities', 'cities.id', '=', 'order_groups.user_city_id')
                ->join('stores', 'stores.id', '=', 'orders.store_id')
                ->where('is_storage', 1)
                ->where('orders.status', 'Delivered')
                ->where('order_products.fulfilment_status', 'Fullfilled')
                ->where('orders.user_id', '<>' ,86147)
                ->where('orders.total_amount', '>', 0)
                ->where('cities.coverage_store_id', $value)
                ->whereNotIn('orders.id', [188400,186854])
                ->whereNull('orders.invoice_number')
                ->whereRaw("user_comments NOT LIKE '%El día%'")
                ->whereRaw("DATEDIFF(CURDATE(), DATE(orders.management_date)) = 3")
                ->whereRaw("DATE(orders.management_date) <= '". date('Y-m-d', strtotime('- 3 days'))."'")
                ->groupBy('orders.id')
                ->orderBy('management_date', 'ASC')
                ->select('orders.id', 'orders.store_id', 'invoice_number', 'invoice_date', 'management_date', 'stores.city_id')
                ->get();

            foreach ($orders as $order) {
                $this->generateConsecutive($order);
                $i++;
            }*/

            $orders_invoiced = \Order::where('orders.status', 'Delivered')
                ->where('orders.user_id', '<>' ,86147)
                ->where('orders.total_amount', '>', 0)
                ->where('orders.store_id', $value)
                ->where('orders.total_amount', '>', 0)
                ->where('invoice_sent', 0)
                ->whereNotNull('orders.invoice_number')
                ->whereRaw("DATEDIFF(CURDATE(), DATE(orders.management_date)) = 3")
                ->whereRaw("DATE(orders.management_date) <= '". date('Y-m-d', strtotime('- 3 days'))."'")
                ->orderBy('management_date', 'ASC')
                ->with('user')
                ->get();

            foreach ($orders_invoiced as $order) {
                if ($order->status === 'Delivered') {
                    $user = $order->user;
                    $url_download = route('User.OrderTokenInvoice', [$order->user_score_token]);
                    $url_download = str_replace("http://18.210.137.2/", "https://merqueo.com", $url_download);
                    $result = send_mail([
                        'template_name' => 'emails.invoice',
                        'subject' => "Factura de pedido",
                        'to' => [
                            ['email' => $user->email, 'name' => $user->first_name.' '.$user->last_name]
                        ],
                        'vars' => array('url_download' => $url_download)
                    ]);
                    if (!$result) {
                        $failed_mails[] = "Factura de compra #{$order->id}: Error al enviar el mail al usuario  con el siguiente pedido: $order->id";
                    }else{
                        $j++;

                        $output->writeln('Pedido #'.$order->id.' facturado y factura enviada');
                        $order->invoice_sent = 1;
                        $order->save();
                    }
                }
            }
        }

        $output->writeln('Pedidos facturados:'. $i);
        $output->writeln('Pedidos con facturas enviadas:'. $j);
    }

    /**
     * Función para generar consecutivo para planos E
     */
    private function generateConsecutive($order)
    {
        if ($order) {
            $consecutive = DB::table('consecutives')
                ->where('type', 'invoice')
                ->where('city_id', $order->city_id)
                ->select(DB::raw("CONCAT(prefix,'-',prefix_complement) AS prefix"), 'consecutive', 'id', 'resolution')
                ->orderBy(DB::raw('RAND()'))
                ->first();

            $numeration = $consecutive->consecutive;
            if($numeration < 100)
                $numeration = str_pad($consecutive->consecutive, 3,  0, STR_PAD_LEFT);

            $order->invoice_number = $consecutive->prefix.$numeration;
            $order->invoice_date = $order->management_date;
            $order->invoice_issuance_date = date('Y-m-d H:i:s');
            $order->invoice_resolution = $consecutive->resolution;
            $order->save();

            $consecutive->consecutive += 1;
            \DB::table('consecutives')->where('id', $consecutive->id)->update(['consecutive' => $consecutive->consecutive]);

            return true;
        }
        return false;
    }
}
