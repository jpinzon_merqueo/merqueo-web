<?php

/**
 * Se encarga de actualizar los precios de los combos para que
 * concuerden con el precio del producto.
 */
class SyncPrices extends \Illuminate\Console\Command
{
    /**
     * @var string
     */
    protected $name = 'command:sync-prices';

    /**
     *
     */
    public function fire()
    {
        \DB::beginTransaction();

        $query = StoreProduct::with('storeProductGroup.storeProductGroup', 'product')
            ->whereHas('product', function ($query) {
                $query->where('type', '<>', 'Simple');
            });

        $store_products = $query->count();
        $output = new \Symfony\Component\Console\Output\ConsoleOutput();
        $progressBar = new \Symfony\Component\Console\Helper\ProgressBar($output, $store_products);
        $progressBar->start();

        $query->chunk(100, function ($items) use ($progressBar) {
            foreach ($items as $item) {
                $price = 0;
                foreach ($item->storeProductGroup as $product) {
                    $price += $product->storeProductGroup->price * $product->quantity;
                }
                if ($price != $item->price) {
                    foreach ($item->storeProductGroup as $product) {
                        try {
                            $product->storeProductGroup->updateAncestorsPrice();
                        } catch (\exceptions\MerqueoException $exception) {
                            $this->info($exception->getMessage());
                        } catch (\Elasticsearch\Common\Exceptions\Forbidden403Exception $exception) {
                        }
                    }
                }

                $progressBar->advance();
            }
        });

        $progressBar->finish();
        \DB::commit();
    }
}
