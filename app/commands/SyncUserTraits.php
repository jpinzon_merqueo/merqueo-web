<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class SyncUserTraits extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'command:sync-user-traits';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Syncs user traits to Segment.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		// Web
		$server = AnalyticsClient::webAnalytics();

		$webUsers = User::select('users.*')
											->join('order_groups', 'users.id', '=', 'order_groups.user_id')
											->where('source', '!=', 'Device')
											->where('order_groups.created_at', '>', $this->argument('date'))
											->groupBy('users.id');

		foreach ($webUsers->get() as $user) {
			print "Update Web user $user->id\n";
			$server->identify($user->id, $user->fullTraits);
		}

		// Android
		$android = AnalyticsClient::androidAnalytics();

		$androidUsers = User::select('users.*')
												->join('order_groups', 'users.id', '=', 'order_groups.user_id')
												->where('source', '=', 'Device')
												->where('source_os', '=', 'Android')
												->where('order_groups.created_at', '>', $this->argument('date'))
												->groupBy('users.id');

		foreach ($androidUsers->get() as $user) {
			print "Update Android user $user->id\n";
			$android->identify($user->id, $user->fullTraits);
		}

		// iOS
		$ios = AnalyticsClient::iosAnalytics();

		$iosUsers = User::select('users.*')
										->join('order_groups', 'users.id', '=', 'order_groups.user_id')
										->where('source', '=', 'Device')
										->where('source_os', '=', 'iOS')
										->where('order_groups.created_at', '>', $this->argument('date'))
										->groupBy('users.id');

		foreach ($iosUsers->get() as $user) {
			print "Update iOS user $user->id\n";
			$ios->identify($user->id, $user->fullTraits);
		}
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('date', InputArgument::REQUIRED, 'Start date required.')
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
