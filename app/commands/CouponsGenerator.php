<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class CouponsGenerator extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'command:generate-coupons';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Generador de cupones.';


    /**
     * Errores del compando
     *
     * @var array
     */
    protected $errors = [];

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        $main_string = $this->argument('main_string');
        $coup_cant = $this->argument('coup_cant');
        $digit_length = $this->argument('digit_length');
        $digit_type = $this->argument('digit_type');
        $campaign_validation = $this->argument('campaign_validation');
        $retry = 0;
        for ($i = 0; $i < $coup_cant; $i++) {
            if ($retry == 3){
                $retry = 0;
            }
            if ($digit_type == 'string') {
                $subfix = \Illuminate\Support\Str::quickRandom($digit_length);
            } elseif ($digit_type == 'number') {
                $subfix = rand(1000000, 9999999);
            }
            $code = strtoupper($main_string . $subfix);

            $coupon_obj = Coupon::where('code', $code)->first();

            if (empty($coupon_obj)) {
                $coupon_obj = new Coupon;
                $coupon_obj->type = 'Credit Amount';
                $coupon_obj->code = $code;
                $coupon_obj->amount = 10000;
                $coupon_obj->number_uses = 1;
                $coupon_obj->type_use = 'Normal';
                $coupon_obj->minimum_order_amount = 30000;
                $coupon_obj->maximum_order_amount = 0;
                $coupon_obj->campaign_validation = $campaign_validation;
                // $coupon_obj->payment_method = 0;
                // $coupon_obj->cc_bin = 0;
                // $coupon_obj->cc_bank = 0;
                $coupon_obj->redeem_on = 'Total Cart';
                // $coupon_obj->store_id = 64;
                // $coupon_obj->department_id = 76;
                // $coupon_obj->shelf_id = 301;
                // $coupon_obj->store_product_id = 3238;
                // $coupon_obj->shopper_id = 0;
                $coupon_obj->expiration_date = '2018-05-31';
                $coupon_obj->visible = 1;
                $coupon_obj->status = 1;
                $coupon_obj->save();
            } elseif ($retry != 3) {
                $i--;
                $retry++;
            }
        }

        $this->info("Se han creado: $i cupones y se han hecho $retry reintentos.");
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
	    return [
            ['coup_cant', InputArgument::REQUIRED, 'Cantidad de cupones'],
            ['digit_type', InputArgument::REQUIRED, 'Tipo de cupon, número con cadena'],
            ['digit_length', InputArgument::REQUIRED, 'Largo de la cadena o número'],
            ['campaign_validation', InputArgument::REQUIRED, 'Nombre de la campaña'],
            ['main_string', InputArgument::OPTIONAL, 'Prefijo del cupón'],
        ];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
