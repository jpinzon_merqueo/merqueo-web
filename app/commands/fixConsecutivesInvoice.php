<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use \Symfony\Component\Console\Helper\ProgressBar;

class fixConsecutivesInvoice extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'command:fix-consecutive-invoices';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Actualiza el consecutivo de las facturas.';

    /**
     * @var ProgressBar
     */
    private $progressBar;

    /**
     * @var array
     */
    private $errors;

    /**
     * @var int
     */
    private $currentInvoice;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $file = $this->argument('file');
        \Excel::load($file, function ($reader) {
            $result = $reader->get();
            $this->currentInvoice = null;
            $this->progressBar = new ProgressBar($this->getOutput(), $result->count());
            $this->progressBar->setFormat(' %current%/%max% [%bar%] %percent:3s%% %elapsed:6s%/%estimated:-6s% %memory:6s% %message%');
            $this->progressBar->start();
            try {
                foreach ($result as $row) {
                    $this->progressBar->advance();
                    $this->currentInvoice = $row->consecutivo;
                    $order = \Order::find($row->id);
                    $order->invoice_number = $row->consecutivo;
                    $order->save();
                }
            } catch (Exception $e){
                $this->errors[] = [
                    'ticket' => $this->currentInvoice,
                    'message' => $e->getMessage(),
                    'trace' => $e->getTraceAsString(),
                ];
            }
        });
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            array('file', InputArgument::REQUIRED, 'Ruta del archivo.'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
        );
    }

}