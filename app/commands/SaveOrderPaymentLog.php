<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class SaveOrderPaymentLog extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'command:SaveOrderPaymentLog';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Guardar los log en la tabla order_payment_log';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        return $this->saveOrderPaymentLog($this->argument('data'));
    }

    /**
     * @param $data
     * @return bool
     */
    private function saveOrderPaymentLog(array $data)
    {
        try {
            $orderPaymentLogModel = new OrderPaymentLog();

            foreach ($data as $key => $datum) {
                $orderPaymentLogModel->{$key} = $datum;
            }

            $saved = $orderPaymentLogModel->save();

            $this->info($saved);

            return intval($saved);

        } catch (\Exception $e) {
            Log::error($e->getMessage());
            $this->info(0);
            return 0;
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['data', InputArgument::REQUIRED, 'datos del log.']
        ];
    }

}
