<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class BdScripts extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'command:scripts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * The mysqli object
     * 
     * @var mysqli
     * 
     */
    protected $mysqli;

    /**
     * Boolean flag which indicates if there was an error running the script
     * 
     * @var boolean
     * 
     */
    protected $error;

    /**
     * An array which stores the msgs during the script execution
     * 
     * @var array
     * 
     */
    protected $log;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {

        $starting_datetime  =  date('Y-m-d H:i:s');
        $this->log          = [];
        $this->error        = false;
        $accion_type        = $this->argument('function');
        $this->log[]        = "Nombre del script: $accion_type";
        $this->log[]        = "Tiempo de inicio: $starting_datetime";

        $this->mysqli_connect();

        switch ($accion_type) {
            case 'bi':

                try {
                    
                    // validación store_product_id_day
                    $sql = 'DELETE FROM store_product_id_day WHERE DATE(DATE) = DATE_ADD(DATE(DATE_ADD(NOW(),INTERVAL -5 HOUR)), INTERVAL -1 DAY)';
                    $this->mysqli_exec(
                        $sql,
                        "Error al ejecutar la query 'validación store_product_id_day'\n",
                        "Ejecutada correctamente query 'validación store_product_id_day'\n"
                    );

                    // actualización store_product_id_day
                    $sql = 'INSERT INTO store_product_id_day (DATE,store_product_id,store_product_warehouse_id,warehouse_id)
                            SELECT
                            DATE(DATE)AS Fecha,
                            store_product_id AS StoreProductId,
                            store_product_warehouses.id AS StoreProductIdWarehouses,
                            warehouse_id AS Warehouse
                            FROM orders,
                            store_products
                            LEFT JOIN store_product_warehouses ON store_products.id = store_product_warehouses.store_product_id
                            WHERE
                            (DATE(DATE)>= DATE(store_products.created_at)) AND
                            (DATE(DATE) BETWEEN DATE_ADD(DATE(DATE_ADD(NOW(),INTERVAL -5 HOUR)), INTERVAL -1 DAY) AND DATE(DATE_ADD(NOW(),INTERVAL -5 HOUR)))
                            GROUP BY
                            DATE(DATE),
                            store_product_id,
                            store_product_warehouses.id,
                            warehouse_id';
                    $this->mysqli_exec(
                        $sql,
                        "Error al ejecutar la query 'actualización store_product_id_day'\n",
                        "Ejecutada correctamente query 'actualización store_product_id_day'\n"
                    );

                    // validación firstemailorder
                    $sql = 'DELETE FROM firstemailorder WHERE user_id IS NOT NULL';
                    $this->mysqli_exec(
                        $sql,
                        "Error al ejecutar la query 'validación firstemailorder'",
                        "Ejecutada correctamente query 'validación firstemailorder'\n"
                    );

                    // actualización firstemailorder
                    $sql = 'INSERT INTO firstemailorder
                    
                    SELECT *, 
                    IF((MinDate<= `management_date` AND `management_date`<= MaxDate),\'On Time\', \'Off Time\')
                    
                    FROM
                    
                    (SELECT og.user_email, 
                    base.id orderId, 
                    o.user_id UserId,
                    base.group_id, 
                    c.city `city`, 
                    s.name Store, 
                    IF(cc.`campaign_validation` IS NULL,"No Coupon", "Coupon") CouponUse,
                    cc.`campaign_validation` CouponCampaign, 
                    (o.created_at) FirstOrderDate, 
                    o.`payment_method` PaymentMethod, 
                    o.`total_amount`, 
                    o.`delivery_amount` DeliveryCost,
                    IF(o.`delivery_amount`=0, "Free Delivery", "Not Free Delivery") DeliveryCostType,
                    IFNULL(og.`source_os`, "Web") OSsystem, o.status, 
                    IFNULL(pr.IsSubsidy, "Not Subsidy") IsSubsidy,
                    IF(Referidos.DeduccionR>0 AND Promotores.DeduccionP>0, \'Referido y Promotor\',  IF(Referidos.DeduccionR>0, \'Referido\', IF(Promotores.DeduccionP>0, \'Promotor\', IF(ufd.referred IS NOT NULL,\'Domicilio Gratis\',\'Regular\')))) AS TipoBeneficiario,
                    CASE 
                    WHEN o.`total_amount`<=30000 THEN \'30k\'
                    WHEN o.`total_amount`<=40000 THEN \'40k\'
                    WHEN o.`total_amount`<=50000 THEN \'50k\'
                    WHEN o.`total_amount`<=60000 THEN \'60k\'
                    WHEN o.`total_amount`<=70000 THEN \'70k\'
                    WHEN o.`total_amount`<=80000 THEN \'80k\'
                    WHEN o.`total_amount`<=90000 THEN \'90k\'
                    WHEN o.`total_amount`<=100000 THEN \'100k\'
                    WHEN o.`total_amount`<=110000 THEN \'110k\'
                    WHEN o.`total_amount`<=120000 THEN \'120k\'
                    WHEN o.`total_amount`<=130000 THEN \'130k\'
                    WHEN o.`total_amount`<=140000 THEN \'140k\'
                    ELSE \'150K<\' END Ticket,
                        
                    CASE 
                    WHEN o.`scheduled_delivery` = 1 THEN \'1990-01-01 00:00\'
                    WHEN o.delivery_time LIKE\'Immediately\' THEN o.created_at
                    ELSE CONCAT(DATE(IFNULL(o.`first_delivery_date`, IFNULL(o.`real_delivery_date`, o.delivery_date))),\' \',STR_TO_DATE(SUBSTRING_INDEX(o.`delivery_time`,\'-\',1), \'%h:%i%p\') ) 
                        END MinDate,
                        
                    CASE 
                    WHEN o.`scheduled_delivery`=1 THEN \'2000-01-01 00:00\'
                    WHEN o.delivery_time LIKE\'Immediately\' THEN IFNULL(o.`first_delivery_date`, IFNULL(o.`real_delivery_date`, o.delivery_date)) 
                    ELSE CONCAT(DATE(IFNULL(o.`first_delivery_date`, IFNULL(o.`real_delivery_date`, o.delivery_date))),\' \',STR_TO_DATE(SUBSTRING_INDEX(o.`delivery_time`,\'-\',-1), \'%h:%i%p\') ) 
                    END MaxDate, 
                        
                    o.`management_date`
                    
                    FROM(
                    
                    SELECT MIN(og.id) group_id, og.user_email, o.user_id, MIN(og.`created_at`) FirstOrderDate, MIN(o.id) id
                    FROM order_groups AS og
                    LEFT JOIN orders AS o ON o.group_id=og.id
                    LEFT JOIN users AS u ON u.id=og.user_id
                    
                    WHERE		
                                    og.source NOT IN ("Callcenter", "Web Service", "Reclamo") AND
                                        (o.reject_reason IS NULL OR                        
                                            o.reject_reason NOT LIKE \'%Cancelado por que era una prueba%\' AND
                                            o.reject_reason NOT LIKE \'%Cancelado por que fue un pedido falso%\' AND
                                            o.reject_reason NOT LIKE \'%Fue un pedido falso%\' AND
                                            o.reject_reason NOT LIKE \'%Fue una prueba%\' AND
                                            o.reject_reason NOT LIKE \'%Pedido duplicado con devolución%\' AND
                                            o.reject_reason NOT LIKE \'%Pedido duplicado sin devolución%\' AND
                                            o.reject_reason NOT LIKE \'%Pedido falso con devolución%\' AND
                                            o.reject_reason NOT LIKE \'%Pedido falso sin devolución%\' AND
                                            o.reject_reason NOT LIKE \'%falso%\' AND
                                            o.reject_reason NOT LIKE \'%duplicado%\' AND
                                            o.reject_reason NOT LIKE \'%Prueba%\' AND
                                            o.reject_reason NOT LIKE \'%prueba%\' AND
                                            o.reject_reason NOT LIKE \'%PRUEBA%\' AND
                                            o.reject_reason NOT LIKE \'%Prueba / Broma%\' AND
                                            o.reject_reason NOT LIKE \'%Rejected because it was a test%\')       

                                          AND (u.first_name NOT LIKE \'%Prueba%\' AND u.last_name NOT LIKE \'%Prueba%\')
                    AND (og.user_comments NOT LIKE \'%PRUEBA%\' OR og.user_comments NOT LIKE \'%Prueba%\' 
                        OR og.`user_comments` NOT LIKE \'%prueba%\')  

                    
                    GROUP BY  o.user_id
                    
                    )base
                    
                    LEFT JOIN orders AS o ON o.id=base.id
                    LEFT JOIN order_groups AS og ON og.id=base.`group_id`
                    LEFT JOIN stores AS s ON s.id=o.store_id
                    LEFT JOIN cities AS c ON c.id=s.city_id

                    LEFT JOIN (
                    SELECT * FROM user_credits AS uc 
                    WHERE uc.description LIKE \'%Deducción de%\' 
                    ) AS cr ON cr.order_id=base.id

                    LEFT JOIN coupons AS cc ON cc.id = cr.`coupon_id`   

                    LEFT JOIN (
                    SELECT op.order_id, "Subsidy" IsSubsidy 
                    FROM order_products AS op 
                    WHERE op.`promo_type`="Merqueo"
                    ) AS pr ON pr.order_id = base.id
                    
                    /* Traigo ordenes y monto de Referidos */
                    LEFT JOIN 
                    (SELECT order_id, SUM(deduccion) AS DeduccionR
                    FROM
                        (SELECT order_id, user_credit_id,  amount-current_amount AS deduccion
                        FROM user_credits
                        WHERE user_credits.type = 0
                            AND DATE(created_at) >= \'2018-05-21\'
                            AND (coupon_id IS NULL)
                        ) AS Deduccion

                    LEFT JOIN
                    (SELECT id
                    FROM user_credits
                    WHERE   user_credits.type = 1
                        AND (DATE(created_at) >= \'2018-05-21\')
                        AND (description_admin LIKE \'%referido%\')
                        AND description LIKE \'%Usaste%\') AS Referidos
                    ON Deduccion.user_credit_id = Referidos.id

                    WHERE id IS NOT NULL
                    GROUP BY order_id) Referidos
                    ON Referidos.order_id=base.id
                    
                    /* Traigo ordenes y monto de Promotores */
                    LEFT JOIN 
                    (SELECT order_id, SUM(deduccion) AS DeduccionP
                    FROM
                        (SELECT order_id, user_credit_id, amount-current_amount AS deduccion 
                        FROM user_credits
                        WHERE user_credits.type = 0
                            AND DATE(created_at) >= \'2018-05-21\'
                            AND (coupon_id IS NULL)
                        ) AS Deduccion

                    LEFT JOIN
                    (SELECT id
                    FROM user_credits
                    WHERE   user_credits.type = 1
                        AND (DATE(created_at) >= \'2018-05-21\')
                        AND (description_admin LIKE \'%referido%\')
                        AND description LIKE \'%usó%\') AS Promotores
                    ON Deduccion.user_credit_id = Promotores.id

                    WHERE id IS NOT NULL
                    GROUP BY order_id) Promotores
                    ON Promotores.order_id=base.id

                    LEFT JOIN (
                    SELECT uf.order_id, "Referred"
                    FROM user_free_deliveries AS uf 
                    WHERE uf.`description` LIKE \'%Domicilio gratis en pedido%\' 
                    ) AS ufd ON ufd.order_id=base.id


                    GROUP BY base.id)t'
                    ;
                    $this->mysqli_exec(
                        $sql, 
                        "Error al ejecutar la query 'validación firstemailorder'",
                        "Ejecutada correctamente query 'validación firstemailorder'\n"
                    );

                $sql = 'INSERT INTO producto_detalle

                        SELECT
                                products.id         AS product_id,
                                store_products.id   AS store_product_id,
                                products.reference  AS products_reference,
                                products.name       AS products_name,
                                products.unit       AS products_unit,
                                products.quantity   AS quantity,
                                brands.name         AS marca,
                                makers.name         AS fabricante,
                                IF(store_products.allied_store_id IS NULL,providers.name,allied_stores.name)    AS proveedor,
                                IF(store_products.allied_store_id IS NULL, \'Merqueo\', \'Marketplace\')            AS tipo_proveedor,
                                products.type           AS \'type\',
                                store_products.storage  AS almacenamiento,
                                departments.name        AS departamento,
                                shelves.name            AS pasillo,
                                categories.name         AS categoria,
                                subcategories.name      AS subcategoria,
                                shelves.category        AS supercategoria,
                                providers.id            AS provider_id,
                                stores.name             AS tienda

                                FROM store_products
                                    LEFT JOIN products      ON store_products.product_id = products.id
                                    LEFT JOIN departments   ON store_products.department_id = departments.id
                                    LEFT JOIN shelves       ON store_products.shelf_id = shelves.id
                                    LEFT JOIN categories    ON categories.id = products.category_id
                                    LEFT JOIN subcategories ON subcategories.id = products.subcategory_id
                                    LEFT JOIN providers     ON providers.id = store_products.provider_id
                                    LEFT JOIN allied_stores ON allied_stores.id = store_products.allied_store_id
                                    LEFT JOIN brands        ON products.brand_id = brands.id
                                    LEFT JOIN makers        ON products.maker_id = makers.id
                                    LEFT JOIN stores        ON stores.id = store_products.store_id';  

                        $this->mysqli_exec(
                        $sql, 
                        "Error al ejecutar la query 'insert into producto_detalle'",
                        "Ejecutada correctamente query 'insert into producto_detalle'\n"
                    );      

                } catch (Exception $e) {
                    $err_msg        = $e->getMessage();
                    $this->error    = true;
                    $this->log[]    = $err_msg;
                    echo "$err_msg\n";
                }
                $this->mysqli->close();

                break;

            case 'bi_retention':

                $yesterdayDateString       = date("Y-m-d", time() - 43200);
                try {

                    // actualización active_users (ejecución 1)
                    $sql = 'CALL bi_active_users();';
                    $this->mysqli_exec(
                        $sql, 
                        "Error al ejecutar por primera vez la query 'actualización active_users'", 
                        "Ejecutada correctamente por primera vez query 'actualización active_users'\n"
                    );

                    // Borrado de active_users
                    $sql = 'DELETE FROM active_users_BI WHERE fechacorte = "'.$yesterdayDateString.'"';
                    $this->mysqli_exec(
                        $sql, 
                        "Error al ejecutar el borrado de active_users después de la primera ejecución de la query", 
                        "Ejecutado correctamente el borrado de active_users después de la primera ejecución de la query\n"
                    );

                    // actualización active_users (ejecución 2)
                    $sql = 'CALL bi_active_users();';
                    $this->mysqli_exec(
                        $sql, 
                        "Error al ejecutar por segunda vez la query 'actualización active_users'", 
                        "Ejecutada correctamente por segunda vez query 'actualización active_users'\n"
                    );

                    $info_msg       = 'Ejecución del script finalizada.';
                    $this->log[]    = $info_msg;
                    echo "$info_msg\n";

                } catch (Exception $e) {
                    $err_msg        = $e->getMessage();
                    $this->error    = true;
                    $this->log[]    = $err_msg;
                    echo "$err_msg\n";
                }
                $this->mysqli->close();
                break;

            default:
                $err_msg        = "No existe función.<br>";
                $this->error    = true;
                $this->log[]    = $err_msg;
                echo "$err_msg\n"; 
                break;
        }

        $finish_datetime    =  date('Y-m-d H:i:s');
        $this->log[]        = "Tiempo de finalización: $finish_datetime";
        return send_query_execution_report($accion_type, $this->log, $this->error);
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            array('function', InputArgument::REQUIRED),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }

    /**
     * Connects to datadabase with mysqli driver
     * 
     */
    private function mysqli_connect(){

        $this->mysqli = new mysqli(getenv('DB_BI.HOST'), getenv('DB_BI.USERNAME'), getenv('DB_BI.PASSWORD'), getenv('DB_BI.DATABASE'));

        if ($this->mysqli->connect_errno){
            $err_msg    = "Falló la conexión a MySQL: (" . $this->mysqli->connect_errno . ") " . $this->mysqli->connect_error;
            $this->error      = true;
            $this->log[]      = $err_msg;
            echo "$err_msg\n";
        }else{
            $info_msg   = "Conectado correctamente a ".$this->mysqli->host_info;
            $this->log[]      = $info_msg;
            echo "$info_msg\n"; 
        }

    }

    /**
     * Executes an $sql statement and stores the respective log according
     * to the statement execution result
     * 
     * @param string $sql the statement
     * @param string $err_msg the error msg in case of error
     * @param string $info_msg the information msg 
     */
    private function mysqli_exec($sql, $err_msg, $info_msg){

        if (!$this->mysqli->query($sql)){
            $this->error    = true;
            $err_msg        .= ": (".$this->mysqli->errno.", ".$this->mysqli->error.")";
            $this->log[]    = $err_msg;
            echo "$err_msg\n";
        }else{
            $info_msg       .= "Filas afectadas: ".$this->mysqli->affected_rows;
            $this->log[]    = $info_msg;
            echo "$info_msg\n";
        }

    }

}