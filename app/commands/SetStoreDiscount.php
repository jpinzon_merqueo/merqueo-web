<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;

class SetStoreDiscount extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'command:store-discounts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sets a discounts to the product stores.';

    /**
     * @var \Carbon\Carbon
     */
    private $startDate;

    /**
     * @var \Carbon\Carbon
     */
    private $limitDate;

    /**
     * @var float
     */
    private $discountPercentage;

    /**
     * @var int
     */
    private $discount;

    /**
     * @var int
     */
    private $quantitySpecialPrice;

    /**
     * @var \Symfony\Component\Console\Helper\ProgressBar
     */
    private $progressBar;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {

        $this->startDate = new \Carbon\Carbon($this->argument('start-date'));
        $this->limitDate = new \Carbon\Carbon($this->argument('limit-date'));
        $this->discountPercentage = $this->argument('discount') / 100;
        $this->discount = (int)$this->argument('discount');
        $this->quantitySpecialPrice = (int)$this->argument('q-special-price');
        $exclude = array_merge(Shelf::CIGARETTES, Shelf::MILK);

        $query = StoreProduct::with('storeProducts')
            ->with(['storeProductWarehouses'=>function($query){
                $query->where('status', '=', 1)
                    ->where('is_visible', '=', 1);
            }])
            ->whereHas('storeProductWarehouses', function($query){
                $query->where('status', '=', 1)
                    ->where('is_visible', '=', 1);
            })
            ->whereNotIn('shelf_id', $exclude);

        if ($this->argument('stores')) {
            $query->whereIn('store_id', explode(',', $this->argument('stores')));
        }

        if ($this->argument('departments')) {
            $query->whereIn('department_id', explode(',', $this->argument('departments')));
        }

        if ($this->argument('shelves')) {
            $query->whereIn('shelf_id', explode(',', $this->argument('shelves')));
        }

        \DB::beginTransaction();
        $this->progressBar = new \Symfony\Component\Console\Helper\ProgressBar($this->output, $query->count());

        $query->chunk(500, function ($products) {
            foreach ($products as $product) {
                try{
                    $this->progressBar->advance();
                    $isSimple = $product->storeProducts->count() === 0;
                    $hasSpecialPrice = !!$product->realSpecialPrice(0, null);
                    $discountPercentage = $product->getPercentageDiscount(0, null);

                    if ($hasSpecialPrice && $discountPercentage >= $this->discount) {
                        continue;
                    }

                    $initialSpecialPrice = $product->special_price;
                    $product->special_price = round($product->price - $product->price * $this->discountPercentage);
                    $product->special_price_starting_date = $this->startDate;
                    $product->special_price_expiration_date = $this->limitDate;
                    $product->quantity_special_price = $this->quantitySpecialPrice;

                    if ($isSimple) {
                        if (!$hasSpecialPrice) {
                            $product->merqueo_discount = 100;
                            $product->seller_discount = 0;
                            $product->provider_discount = 0;
                        } else {
                            $currentDiscount = $product->price - $product->special_price;
                            $originalDiscount = $product->price - $initialSpecialPrice;

                            $product->merqueo_discount = (($currentDiscount - $originalDiscount) + $product->merqueo_discount / 100 * $originalDiscount) / $currentDiscount * 100;
                            $product->provider_discount = (($product->provider_discount / 100 * $originalDiscount) / $currentDiscount) * 100;
                            $product->seller_discount = (($product->seller_discount / 100 * $originalDiscount) / $currentDiscount) * 100;
                        }
                    } else {
                        if ($product->storeProducts->count() == 0) {
                            continue;
                        }

                        if (!$hasSpecialPrice) {
                            $discount = ($product->price - $product->special_price) / $product->storeProducts->count();

                            foreach ($product->storeProducts as $storeProduct) {
                                $storeProduct->pivot->discount_amount = $discount;
                                $storeProduct->pivot->merqueo_discount = 100;
                                $storeProduct->pivot->seller_discount = 0;
                                $storeProduct->pivot->provider_discount = 0;

                                $storeProduct->pivot->save();
                            }
                        } else {
                            $difference = $initialSpecialPrice - $product->special_price;
                            $productDifference = $difference / $product->storeProducts->count();

                            foreach ($product->storeProducts as $storeProduct) {
                                $pivot = $storeProduct->pivot;
                                $originalDiscount = $pivot->discount_amount;
                                $pivot->discount_amount += $productDifference;
                                $currentDiscount = $pivot->discount_amount;

                                $pivot->merqueo_discount = (($currentDiscount - $originalDiscount) + $originalDiscount * $pivot->merqueo_discount / 100) / $currentDiscount * 100;
                                $pivot->provider_discount = (($pivot->provider_discount / 100 * $originalDiscount) / $currentDiscount) * 100;
                                $pivot->seller_discount = (($pivot->seller_discount / 100 * $originalDiscount) / $currentDiscount) * 100;

                                $pivot->save();
                            }
                        }
                    }

                    $product->save();
                }catch (Exception $e){
                    $this->error("Producto ID ".$product->id);
                }

            }
        });

        \DB::commit();
        $this->progressBar->finish();
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['discount', InputArgument::REQUIRED, 'Store identifier.'],
            ['start-date', InputArgument::REQUIRED, 'Store identifier.'],
            ['limit-date', InputArgument::REQUIRED, 'Store identifier.'],
            ['q-special-price', InputArgument::REQUIRED, 'Store identifier.'],
            ['stores', InputArgument::OPTIONAL, 'Store identifier.'],
            ['departments', InputArgument::OPTIONAL, 'Store identifier.'],
            ['shelves', InputArgument::OPTIONAL, 'Store identifier.'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }

}
