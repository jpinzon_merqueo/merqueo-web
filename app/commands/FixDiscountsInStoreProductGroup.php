<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;

class FixDiscountsInStoreProductGroup extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'command:fix-discounts-store-product-group';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fixes the discount_amount in store_product_group';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {

        // Get the current string date
        $now = date('Y-m-d H:i:s');

        // Fetch the combo store_products with an active discount
        $storeProducts = StoreProduct::join('products', 'products.id','=','store_products.product_id')
            ->where('products.type','Agrupado')
            ->where('store_products.special_price','<>','null')
            ->where('store_products.special_price_starting_date','<',$now)
            ->where('store_products.special_price_expiration_date','>',$now)
            ->select('products.*','store_products.*')
            ->get();

        
        $this->info("Found ".count($storeProducts)." combo products with active discount");

        \DB::beginTransaction();
        // Loop through the fetched records
        try{

            foreach($storeProducts as $storeProduct){


                $this->info("---------------------[REF: $storeProduct->reference]-----------------------");
                $this->info("store_product_id: $storeProduct->id");
                $this->info("product name: $storeProduct->name");
                $this->info("product reference: $storeProduct->reference");
                $this->info("product price: $storeProduct->price");
                $this->info("special price: $storeProduct->special_price");

                // Get the product special price
                $specialPrice = $storeProduct->special_price;

                // Fetch the products composing the combo
                $storeProductsGroup = StoreProductGroup::where('store_product_id',$storeProduct->id)->get();

                // These vars will store the added price of the products composing the combo both 
                // with and without discount
                $addedPrice             = 0;
                $addedPriceWithDiscount = 0;

                // This array will store the products percentage of the combo's full price (no discount)
                $distribution = [];

                // Loop through the combo's products
                foreach($storeProductsGroup as $key => $storeProductGroup){

                    // Fetch the actual StoreProduct
                    $storeProductOfTheCombo = StoreProduct::where('id',$storeProductGroup->store_product_group_id)->first();

                    // Calculate the individual price
                    $currentPrice = ($storeProductOfTheCombo->price * $storeProductGroup->quantity);

                    // If the currentPrice is 0, it means that there is a gift and in such case
                    // discount distributions won't be modified
                    if($currentPrice == 0){
                        $this->info("INFO: This combo has a gift, continuing..");
                        continue 2; 
                    }

                    $addedPrice             += $currentPrice;
                    $addedPriceWithDiscount += $currentPrice - $storeProductGroup->discount_amount;
                    $distribution[$key]     = (($currentPrice * 100) / $storeProduct->price)/100;
                }

                // Get the difference between the combo's special price and the added price of the combo's products
                $difference             = $storeProduct->special_price - $addedPriceWithDiscount;
                $differenceToDistribute = $addedPrice - $storeProduct->special_price;

                $this->info("Added price: $addedPrice");
                $this->info("Added price with Discount: $addedPriceWithDiscount");
                $this->info("Current difference: $difference");
                $this->info("Actual difference to distribute: $differenceToDistribute");

                // If the combo's price without discount, is different to the added price of its
                // products without discount, it's necessary to recalculate the combo's price
                if( abs($storeProduct->price - $addedPrice) > 1 ) {
                    $this->info("ERROR: This combo has not a valid price..");
                    $this->info("Recalculating combo price..");
                    
                    $this->info("-> Reseting discounts..");
                    foreach($storeProductsGroup as $key => $storeProductGroup){
                        $storeProductGroup->discount_amount = 0;
                        $storeProductGroup->save();
                    }
                    $this->info("-> OK");

                    $this->info("-> Asigning $addedPrice as new combo price..");
                    $storeProduct->price = $addedPrice;
                    $storeProduct->save();
                    $this->info("-> OK");
                    continue;
                }


                // If the current difference between the combo's special price and the added
                // price of its products with discounts, is lower than 1. It is okay.
                if(abs($difference) < 1){
                    $this->info("This combo is OK, continuing..");
                    continue;
                }

                // At this point, it's necessary to redistribute discounts
                $this->info("Distributing discounts..");

                // Loop again through the combo's products to set the appropiate discount
                foreach($storeProductsGroup as $key => $storeProductGroup){
                    $newDiscount                        = abs($differenceToDistribute) * $distribution[$key];
                    $this->info("-> Assigning $newDiscount as discount_amount for store_product_group with id $storeProductGroup->id");
                    $storeProductGroup->discount_amount = $newDiscount;
                    $storeProductGroup->save();
                    $this->info("-> OK");
                }

            }// End foreach

        }catch(\Exception $e){
            $this->error($e->getMessage());
            \DB::rollBack();
        }
        \DB::commit();

    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }

    /**
     * Makes a PAUSE in cli
     */
    private function makePause(){
        $this->info("press ANY KEY to continue...");
        $fp = fopen("php://stdin","r");
        fgets($fp);
    }

}
