<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class FirebaseGenerateDynamicLinksReferral extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'firebase:generate-dynamic-links-referral';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Genera dynamic links para codigo de referido de usuarios.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{

		$users = \User::get();
		// $user = \User::find(1021);

		foreach($users as $user){

			$info_user = $user->getLastOrder();

			$city_slug = isset($info_user->city_slug) ? $info_user->city_slug : 'bogota';

			$store_id = isset($info_user->store_id) ? $info_user->store_id : 63;
			$store_slug = isset($info_user->store_slug) ? $info_user->store_slug : 'super-ahorro';
				

			$url_web = route('frontStore.all_departments', [
				'city_slug'       => $city_slug,
				'store_slug'      => $store_slug
			]);

			$this->info($url_web);


			$dynamic_link = FirebaseClient::generateDynamicLink([
				'url'           => $url_web,
				'type'			=> 'referral',
				'store_id'      => $store_id,
				'referral_code' => $user->referral_code
			]);

			// dd($dynamic_link);
			$user->referral_url = $dynamic_link->shortLink;
			$user->save();

			$this->info($dynamic_link->shortLink);

		}
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [];
		// return array(
		// 	array('example', InputArgument::REQUIRED, 'An example argument.'),
		// );
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [];
		// return array(
		// 	array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		// );
	}

}
