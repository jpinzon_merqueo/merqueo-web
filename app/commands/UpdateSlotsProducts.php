<?php

use Illuminate\Console\Command;

class UpdateSlotsProducts extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'command:update-slots-products';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Actualiza la cache en Laravel de los slots por zona cada cierto tiempo.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        $zones = Zone::where('status', 1)->get();

	    $output = new \Symfony\Component\Console\Output\ConsoleOutput();
        $progressBar = new \Symfony\Component\Console\Helper\ProgressBar($output, $zones->count());
        $progressBar->start();

        $warehouses = Warehouse::where('status', 1)->get();
        foreach ($warehouses as $warehouse) {
            $store = Store::where('city_id', $warehouse->city_id)->where('status', 1)->first();
            $zones = Zone::where('status', 1)->where('warehouse_id', $warehouse->id)->get();
            foreach ($zones as $zone) {
                $store->getDeliverySlots($zone, false);
                $progressBar->advance();
            }
        }

        $progressBar->finish();

        $this->info('');
        $this->info('');
        $this->info('Cache de slots de productos por zonas actualizada.');
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			//array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			//array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
