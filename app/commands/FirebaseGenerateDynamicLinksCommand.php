<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class FirebaseGenerateDynamicLinksCommand extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'firebase:generate-dynamic-links';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Genera los vínculos dinámicos para los productos de la tienda.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        ini_set('memory_limit', '512M');
        set_time_limit(0);
        //ini_set("max_execution_time", -1);

        echo 'Obteniendo los productos activos de la tienda...'.PHP_EOL;
        $storeProducts = StoreProduct::whereIn('store_id', array(63, 64))->whereNull('deeplink')->get();
        $departments = \Department::whereIn('store_id', array(63, 64))->whereNull('deeplink')->get();
        $shelves = \Shelf::whereIn('store_id', array(63, 64))->whereNull('deeplink')->get();

        echo 'Generando dynamic links...'.PHP_EOL;
        foreach ($departments as $department) {
            $url_department = route('frontStoreDepartment.single_department', [
                'city_slug'       => $department->store->city->slug,
                'store_slug'      => $department->store->slug,
                'department_slug' => $department->slug
            ]);

            //$url_department  = str_replace("http://localhost", "https://merqueo.com", $url_department);
            $deeplink_department_url =  FirebaseClient::generateDynamicLink([
                'url'           => $url_department,
                'type'          => 'department',
                'store_id'      => $department->store_id,
                'department_id' => $department->id,
            ]);

            if($deeplink_department_url){
                $department->deeplink = $deeplink_department_url->shortLink;
                $department->save();
            }
            sleep(1);
        }

        foreach ($shelves as $shelf) {
            $url_shelf = route('frontStoreShelf.single_shelf', [
                'city_slug'       => $shelf->store->city->slug,
                'store_slug'      => $shelf->store->slug,
                'department_slug' => $shelf->department->slug,
                'shelf_slug'      => $shelf->slug,
            ]);
            //$url_shelf  = str_replace("http://localhost", "https://merqueo.com", $url_shelf);
            $deeplink_shelf_url =  FirebaseClient::generateDynamicLink([
                'url'           => $url_shelf,
                'type'          => 'shelf',
                'store_id'      => $shelf->store_id,
                'department_id' => $shelf->department_id,
                'shelf_id'      => $shelf->id,
            ]);

            if($deeplink_shelf_url){
                $shelf->deeplink = $deeplink_shelf_url->shortLink;
                $shelf->save();
            }
            sleep(1);
        }

        foreach ($storeProducts as $storeProduct) {
            if($storeProduct->product_id > 0 && $storeProduct->product){
                $url_product = route('frontStoreProducts.single_product', [
                    'city_slug'       => $storeProduct->store->city->slug,
                    'store_slug'      => $storeProduct->store->slug,
                    'department_slug' => $storeProduct->department->slug,
                    'shelf_slug'      => $storeProduct->shelf->slug,
                    'product_slug'    => $storeProduct->product->slug,
                ]);

                //$url_product  = str_replace("http://localhost", "https://merqueo.com", $url_product);

                $deeplink_product_url =  FirebaseClient::generateDynamicLink([
                    'url'           => $url_product,
                    'type'          => 'product',
                    'store_id'      => $storeProduct->store_id,
                    'department_id' => $storeProduct->department_id,
                    'shelf_id'      => $storeProduct->shelf_id,
                    'product_id'    => $storeProduct->id,
                ]);

                $storeProduct->deeplink = $deeplink_product_url->shortLink;
                $storeProduct->save();
            }
            sleep(1);
        }
        echo 'Generación de dynamic links finalizada'.PHP_EOL;
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }

}
