<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class SanitizeGroupProductSpecialPrice extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'command:sanitize-group-product-special-price';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Actualiza el precio de los combos y muestra el listado de producto con un precio erroneo.';

    /**
     * @var array
     */
    private $ids;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        \DB::beginTransaction();
        $query = OrderProduct::where('type', 'Agrupado')
            ->with(['orderProductGroup' => function ($query) {
                $query->with('storeProduct')
                    ->where('fulfilment_status', '<>', 'Not Available');
            }])
            ->whereHas('order', function ($query) {
                $query->where('status', '<>', 'Cancelled');
            })
            ->whereHas('orderProductGroup', function ($query) {
                $query->where('fulfilment_status', '<>', 'Not Available');
            })
            ->where('created_at', '>=', $this->argument('date') ?: '2018-07-01');

        $this->progressBar = new \Symfony\Component\Console\Helper\ProgressBar($this->getOutput(), $query->count());
        $this->progressBar->start();

        $query->chunk(3000, function ($items) {
            foreach ($items as $item) {
                $price = 0;
                foreach ($item->orderProductGroup as $product) {
                    $price += $product->price * $product->quantity;
                }

                $finalResult = $item->original_price * $item->quantity;
                if (intval($price - $finalResult) !== 0) {

                    foreach ($item->orderProductGroup as $product) {
                        $product->price = $product->base_price * (1 + $product->iva / 100);
                        $product->save();
                    }

                    $price = 0;
                    foreach ($item->orderProductGroup as $product) {
                        $price += $product->price * $product->quantity;
                    }

                    $difference = $price - $finalResult;

                    $this->ids[$item->store_product_id] = empty($this->ids[$item->store_product_id])
                        ? [] : $this->ids[$item->store_product_id];

                    $this->ids[$item->store_product_id][$difference] = empty($this->ids[$item->store_product_id][$difference])
                        ? 0 : $this->ids[$item->store_product_id][$difference];
                    $this->ids[$item->store_product_id][$difference]++;
                }

                $this->progressBar->advance();
            }
        });

        $this->progressBar->finish();
        $this->info('Total' . count($this->ids));
        $this->info(print_r($this->ids, true));
        \DB::commit();
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['date', InputArgument::REQUIRED, 'Fecha de inicio de limpiado.']
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }

}
