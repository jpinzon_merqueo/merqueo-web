<?php


namespace app\objects;


use app\objects\contracts\ObjectInterface;
use Credit;

class CreditDataObject extends Object implements ObjectInterface
{
    public $amount;
    public $current_amount;
    public $expiration_date;
    public $object;
    public $claim_motive;
    public $text_products_related;
    public $user_id;
    public $credit_id;

}