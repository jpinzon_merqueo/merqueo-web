<?php


namespace app\objects;


class Object
{

    public function create(array $data = [])
    {
        array_walk( $data, [$this, 'walk']);

        return $this;
    }

    private function walk($value, $key){
        if(is_array($value)){
            array_walk($value, [$this, 'walk']);
        } else {
            $this->$key = $value;
        }
    }



}