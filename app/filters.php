<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
    //Artisan::call('down');
    //date_default_timezone_set('America/Bogota');
    //Cache::remember('js_version_number', 720, function(){ return time(); });
    Cache::put('js_version_number', time(), 720);

    //forzar uso de https
    if (Config::get('app.force_schema_url')) {
        URL::forceSchema('https');
    }

    $controller = Request::segment(1);
    $controllers = array('app', 'api', 'terminos', 'feed', 'orden', 'password', 'registro', 'order');
    //validar acceso mobile
    $control_mobile = 0;

    if (!in_array($controller, $controllers) && !is_admin_url() && !$request->ajax() && isset($_SERVER['HTTP_USER_AGENT']) && !Input::get('wb')) {
        $tablet_browser = 0;
        $mobile_browser = 0;

        if (preg_match('/(tablet|ipad|playbook)|(android(?!.*(mobi|opera mini)))/i', strtolower($_SERVER['HTTP_USER_AGENT'])))
            $tablet_browser++;

        if (preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|android|iemobile)/i', strtolower($_SERVER['HTTP_USER_AGENT'])))
            $mobile_browser++;

        if ((isset($_SERVER['HTTP_ACCEPT']) && strpos(strtolower($_SERVER['HTTP_ACCEPT']),'application/vnd.wap.xhtml+xml') > 0) or ((isset($_SERVER['HTTP_X_WAP_PROFILE']) or isset($_SERVER['HTTP_PROFILE']))))
            $mobile_browser++;

        if (preg_match("/iPhone/i", strtolower($_SERVER['HTTP_USER_AGENT'])) || preg_match("/iPad/i", strtolower($_SERVER['HTTP_USER_AGENT'])))
            $control_mobile = 1;
        //dd($_SERVER['REQUEST_URI']);
        if (preg_match("/^\/l\//", strtolower($_SERVER['REQUEST_URI'])))
            $mobile_browser = 0;

        $mobile_ua = strtolower(substr($_SERVER['HTTP_USER_AGENT'], 0, 4));
        $mobile_agents = array(
            'w3c ','acs-','alav','alca','amoi','audi','avan','benq','bird','blac',
            'blaz','brew','cell','cldc','cmd-','dang','doco','eric','hipt','inno',
            'ipaq','java','jigs','kddi','keji','leno','lg-c','lg-d','lg-g','lge-',
            'maui','maxo','midp','mits','mmef','mobi','mot-','moto','mwbp','nec-',
            'newt','noki','palm','pana','pant','phil','play','port','prox',
            'qwap','sage','sams','sany','sch-','sec-','send','seri','sgh-','shar',
            'sie-','siem','smal','smar','sony','sph-','symb','t-mo','teli','tim-',
            'tosh','tsm-','upg1','upsi','vk-v','voda','wap-','wapa','wapi','wapp',
            'wapr','webc','winw','winw','xda ','xda-');

        if (in_array($mobile_ua,$mobile_agents))
            $mobile_browser++;

        if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'opera mini') > 0) {
            $mobile_browser++;
            //Check for tablets on opera mini alternative headers
            $stock_ua = strtolower(isset($_SERVER['HTTP_X_OPERAMINI_PHONE_UA'])?$_SERVER['HTTP_X_OPERAMINI_PHONE_UA']:(isset($_SERVER['HTTP_DEVICE_STOCK_UA'])?$_SERVER['HTTP_DEVICE_STOCK_UA']:''));
            if (preg_match('/(tablet|ipad|playbook)|(android(?!.*mobile))/i', $stock_ua)) {
              $tablet_browser++;
            }
        }

        if ($mobile_browser > 0) {

            $referrer = $request->getRequestUri();
            $referrer = str_replace(Config::get('app.url'), '', $referrer);
            $referrer = str_replace('/domicilios-super-ahorro', '', $referrer);
            $referrer = str_replace("/buscar?q=", "/buscar/", $referrer);

            $address = Session::get("address");

            if($address && $address->city_id){
                $cityCurrent = City::where(['status' => 1, 'id' => $address->city_id])->first();
                $myCity = City::where(['status' => 1, 'coverage_store_id' => $cityCurrent->coverage_store_id])->first();
                $referrer = str_replace('/'.$myCity->slug, '/'.$cityCurrent->slug, $referrer);
            }

            $migrateUrl = ( str_contains($referrer, '?') || str_contains($referrer, '&') ) ? '&' : '?';

            $urlFinalRedirect = Config::get('app.new_site_url') . $referrer . $migrateUrl .'migrate=true';

            # return Redirect::to($urlFinalRedirect);

        } else if ($tablet_browser > 0) {
            $device_validate = 0;
            if (Cookie::get('device_send_mob') || isset($_SESSION['mobile'])){
                $device_validate = Cookie::get('device_send_mob');
            }

            if (!$device_validate) {
                $urlval = explode('not-mobile', $_SERVER["REQUEST_URI"]);
                if (count($urlval) > 1) {

                    $url = str_replace('/not-mobile', '',$_SERVER["REQUEST_URI"]);
                    $_SESSION['mobile'] = 1;

                    Cookie::queue('device_send_mob', 1, 30);
                    if($url == ''){
                        $url = '/';
                    }
                    return Redirect::to($url);
                }else{
                    $metadata = array();

                    $links = array(
                        'contact_phone' => Config::get('app.admin_phone'),
                        'android_url' => Config::get('app.android_url'),
                        'ios_url' => Config::get('app.ios_url'),
                    );
                    $footer['links'] = $links;

                    if($_SERVER['REQUEST_URI'] == '/'){
                        $_SERVER['REQUEST_URI'] =  URL::to('/');
                    }

                   return View::make('device')
                                    ->with('footer', $footer)
                                    ->with('metadata', $metadata)
                                    ->with('url_send', $_SERVER['REQUEST_URI'])
                                    ->with('controlmobile', $control_mobile);
                }
            }
        }
    }

});

App::before(function ($request) {
    if ($request->getMethod() === 'OPTIONS') {
        $statusCode = 204;

        $headers = [
            'Access-Control-Allow-Origin' => Config::get('cors.allow_origin'),
            'Allow' => Config::get('cors.methods'),
            'Access-Control-Allow-Headers' => Config::get('cors.headers'),
            'Access-Control-Allow-Credentials' => Config::get('cors.credentials'),
        ];

        return Response::make(null, $statusCode, $headers);
    }
});

App::after(function ($request, $response) {
    $response->headers->set('Access-Control-Allow-Origin', Config::get('cors.allow_origin'));
    $response->headers->set('Allow', Config::get('cors.methods'));
    $response->headers->set('Access-Control-Allow-Headers', Config::get('cors.headers'));
    $response->headers->set('Access-Control-Allow-Credentials', Config::get('cors.credentials'));

    return $response;
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
    if (Auth::guest())
    {
        if (Request::ajax())
        {
            return Response::make('Unauthorized', 401);
        }
        return Redirect::guest('login');
    }
});


Route::filter('auth.basic', function()
{
    return Auth::basic();
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
    if (Auth::check()) return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
    if (Session::token() !== Input::get('_token'))
    {
        throw new Illuminate\Session\TokenMismatchException;
    }
});
