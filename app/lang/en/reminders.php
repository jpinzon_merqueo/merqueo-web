<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Password Reminder Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are the default lines which match reasons
	| that are given by the password broker for a password update attempt
	| has failed, such as for an invalid token or invalid new password.
	|
	*/

	"password" => "La contraseña debe ser al menos de 6 caracteres de longitud y la confirmación debe coincidir.",

	"user" => "No existe un usuario con el email ingresado.",

	"token" => "El token para restablecer la contraseña es invalido.",

	"sent" => "Hemos enviado un mail a tu correo con los pasos a seguir.",

	"reset" => "Tu contraseña ha sido restablecida.",

    "error" => "Algo salió mal al enviar el mail, por favor intenta mas tarde o comunícate con nosotros a servicioalcliente@merqueo.com.",

);
