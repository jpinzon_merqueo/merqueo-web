# Merqueo Web

## Integración con InfoBit

[InfoBip](https://www.infobip.com) es utilizado como servicio para el envío de mensajes de texto. Para que el servicio funcione, se deben definir dos variables de entorno de la siguiente forma:

```
'INFOBIP_URL' => 'https://8pvrd.api.infobip.com/sms/2/text/single',
'INFOBIP_KEY' => 'some-generated-key-by-infobip',
```

El valor `some-generated-key-by-infobip` debe ser una API key generada consumiendo los servicios REST de Infobip, asunto que se comenta a continuación.

Para realizar operaciones sobre las API keys de InfoBip como crear una nueva llave, listar las que hay actualmente o actualizar una específicamente, la única forma es consultando la API de InfoBip mediante un cliente http, pues el BackOffice de dicho servicio no tiene una interfaz gráfica para hacer estas gestiones. Para brevedad, continuación se comentan ejemplos de usos reales para el consumo de los servicios:

```php
<?php

$http = new GuzzleHttp\Client();

/**
 * Crea API key
 */
$response = $http->post('https://8pvrd.api.infobip.com/settings/1/accounts/account-key-here/api-keys', [
    'json'    => [
        "name"        => "Staging Key",
        "allowedIPs"  => [
            "127.0.0.1",
        ],
        "permissions" => ["ALL"],
        "validFrom"   => \Carbon\Carbon::now()->toIso8601String(),
        "validTo"     => \Carbon\Carbon::now()->addYears(2)->toIso8601String(),
    ],
    'headers' => [
        'Authorization' => "Basic ".base64_encode("InfoBipUserName:InfoBip.User.Password"),
        'Content-Type'  => 'application/json',
        'Accept'        => 'application/json',
    ],
]);

$responseBody = json_decode($response->getBody()->getContents());

/**
 * Lista API keys
 */
$response = $http->get('https://8pvrd.api.infobip.com/settings/1/accounts/account-key-here/api-keys', [
    'headers' => [
        'Authorization' => "Basic ".base64_encode("InfoBipUserName:InfoBip.User.Password"),
        'Content-Type'  => 'application/json',
        'Accept'        => 'application/json',
    ]
]);

$responseBody = json_decode($response->getBody()->getContents());
```

Los anteriores ejemplos pueden ser ejecutados en una sesión de Laravel Tinker para comprobar las respuestas devueltas, para la creación de llaves hay que tener en cuenta que hay que cambiar las IPs que tendrá asociada la llave, y para todos los casos, cambiar las credenciales `InfoBipUserName:InfoBip.User.Password` por las que son usadas para iniciar sesión en InfoBip y `account-key-here` para la llave de la cuenta, la cual puede ser obtenida en el BackOffice de InfoBip en el área de configuraciones u opciones. Como la documentación de InfoBip señala, usar el método de autenticación **Basic** es inseguro y no es recomendado su uso, en cambio se debe hacer uso de API keys, pero para estos endpoints de gestión de las API keys, según la documentación esta la única forma de autenticarse, pues una API key no se puede usar para consultar otras, así que se debe proceder con precaución al realizar estas operaciones.

En la actualidad hay tres API keys creadas, para conocerlas se debe ejecutar el comando anteriormente mencionado para listar las llaves creadas.

- Leer más sobre [gestión de API keys en InfoBip](https://dev.infobip.com/settings/create-and-manage-api-key).
- Leer más sobre [envío de mensajes de texto en InfoBip](https://dev.infobip.com/send-sms/single-sms-message).
- Leer más sobre [buenas practicas integrando el servicio](https://dev.infobip.com/getting-started/integration-best-practices).