function paging(page, search)
{
	if (search != '' && search.length <= 3){
		alert('Digita mas de tres letras en el buscador.');
		return false;
	}

	if ($('.orders-storage-table').length)
	{
        if( $('#delivery_date').val() == '' && $('#delivery_date_end').val() == ''){
            alert('Los campos de fechas no pueden estar vacios');
            $('.paging-loading').hide();
            return;
        }
        if( $('#delivery_date').val() != '' && $('#delivery_date_end').val() == '') {
            alert('Debe seleccionar la fecha de finalización en el rango de fechas');
            $('.paging-loading').hide();
            return;
        } else if ($('#delivery_date').val() == '' && $('#delivery_date_end').val() != '') {
            alert('Debe seleccionar la fecha de inicio en el rango de fechas');
            $('.paging-loading').hide();
            return;
        }

	    $('.paging').html('');
	    $('.paging-loading').show();
		var status_ids = '';
		$('#status :selected').each(function(i, selected){
		  status_ids += $(selected).text() + ',';
		});
		data = {
			city_id: $('#city_id').val(),
			store_id: $('#store_id').val(),
			revision: $('#revision').val(),
			show_orders: $('#show_orders').val(),
			transporter_id: $('#transporter_id').val(),
			vehicle_id: $('#vehicle_id').val(),
			driver_id: $('#driver_id').val(),
			picker_dry_id: $('#picker_dry_id').val(),
			status: status_ids,
			type: $('#type').val(),
			source: $('#source').val(),
			source_os: $('#source_os').val(),
			order_by: $('#order_by').val(),
			delivery_date: $('#delivery_date').val(),
      		delivery_date_end: $('#delivery_date_end').val(),
            delivery_window_id: $('#delivery_window_id').val(),
			payment_method: $('#payment_method').val(),
			route_id: $('#route_id').val(),
			zone_id: $('#zone_id').val(),
			reject_reason: $('#reject_reason').val(),
			allied_stores : $('#allied_stores').val(),
			warehouse_id : $('#warehouse_id').val(),
			s: search,
			p: 1
		};
	} else {
		$('.paging').html('');
		$('.paging-loading').show();
	}

	if ($('.orders-table').length)
	{
		var status_ids = '';
		$('#status :selected').each(function(i, selected){
		  status_ids += $(selected).text() + ',';
		});
		data = {
			city_id: $('#city_id').val(),
			store_id: $('#store_id').val(),
			storage_position: $('#storage_position').val(),
			revision: $('#revision').val(),
			show_orders: $('#show_orders').val(),
			shopper_id: $('#shopper_id').val(),
			zone_id: $('#zone_id').val(),
			status: status_ids,
			source: $('#source').val(),
			source_os: $('#source_os').val(),
			order_by: $('#order_by').val(),
			payment_method: $('#payment_method').val(),
			//allied_stores : $('#allied_stores').val(),
			s: search,
			p: 1
		};
	}
	if ($('.blacklist-table').length) {
		data = {
			type: $('#type').val(),
			city: $('#city').val(),
			search: $('#search').val(),
			bin: $('#bin').val(),
			four: $('#four').val(),
			year: $('#year').val(),
			month: $('#month').val(),
			p: 1
		};
	}
	if ($('.banners-table').length) {
		data = {
			status: $('#status').val(),
			store_id: $('#store_id').val(),
			city_id: $('#city_id').val(),
            s: $('#input-search').val(),
            p: 1
		};
	}
	if ($('.admin-user-table').length) {
		data = {
			s: $('.search').val(),
			p: 1,
			status: $('#status').val(),
			city_id: $('#city_id').val()
		};
	}	
		if ($('.admin-order-providers-table').length) {

		 if( $('#start_delivery_date').val() != '' && $('#end_delivery_date').val() == '') {
            alert('Debe seleccionar la fecha de finalización en el rango de fechas');
            $('.paging-loading').hide();
        } else if ($('#start_delivery_date').val() == '' && $('#end_delivery_date').val() != '') {
            alert('Debe seleccionar la fecha de inicio en el rango de fechas');
            $('.paging-loading').hide();
        }
        
        var inicio = ($('#start_delivery_date').val()).split('/');
        var final = ($('#end_delivery_date').val()).split('/');

         if( (inicio[2] > final[2]) ){
            alert('El año de inicio no puede ser mayor que el año final');
            $('.paging-loading').hide();
            
       	}if ( (inicio[2] >= final[2]) && (inicio[1] > final[1]) ){
       		alert('El mes de inicio no puede ser mayor que el mes final');
       		$('.paging-loading').hide();
            
       	} else if ( (inicio[2] >= final[2]) && (inicio[1] >= final[1]) && (inicio[0] > final[0]) ){
       		alert('El día de inicio no puede ser mayor que el día final');
       		$('.paging-loading').hide();
            
       	}

		var status = '';
		$('#status :selected').each(function(i, selected){
		   status += $(selected).val() + ',';
		});
		data = {
			s: $('#input-search').val(),
			p: 1,
			provider: $('#provider_id').val(),
			warehouse_id: $('#warehouse_id').val(),
			provider_type: $('#provider_type').val(),
			status: status,
			city_id: $('#city_id').val(),
			user_id: $('#user_id').val(),
			start_delivery_date: $('#start_delivery_date').val(),
			end_delivery_date: $('#end_delivery_date').val()
		};
	}
	if ($('.admin-stock-table').length) {
		data = {
			s: $('.search').val(),
			p: 1,
			city_id: $('#city_id').val(),
			warehouse_id : $('#warehouse_id').val(),
		};
	}
	if ($('.transporter-table').length) {
        data = {
            city_id: $('#city_id').val(),
            s: $('#input-search').val(),
            p: 1
        };
   }
    if ($('.vehicle-table').length) {
        data = {
            city_id: $('#city_id').val(),
            transporter_id: $('#transporter_id').val(),
            s: $('#input-search').val(),
            p: 1
        };
    }
    if ($('.balance-table').length) {
        data = {
            city_id: $('#city_id').val(),
            s: $('#input-search').val(),
            p: 1,
            warehouse_id: $('#warehouse_id').val(),
        };
    }
    if ($('.providers-table').length) {
        data = {
            city_id: $('#city_id').val(),
            s: $('#input-search').val(),
            p: 1
        };
    }
    if ($('.driver-table').length) {
        data = {
            city_id: $('#city_id').val(),
            transporter_id: $('#transporter_id').val(),
            s: $('#input-search').val(),
            p: 1
        };
    }
    if ($('.store-table').length) {
        data = {
            s: $('#input-search').val(),
            p: 1
        };
    }
    if ($('.pickers-table').length) {
        data = {
            city_id: $('#city_id').val(),
            s: $('#input-search').val(),
            p: 1
        };
    }
    if ($('.tracing-table').length) {
        data = {
            city_id: $('#city_id').val(),
            order_by: $('#order_by').val(),
            route_id: $('#route_id').val(),
			transporter_id: $('#transporter_id').val(),
			driver_id: $('#driver_id').val(),
            show_orders: $('#show_orders').val(),
            s: $('#input-search').val(),
            p: 1
        };
    }
    if ($('.routes-table').length) {
        data = {
            city_id: $('#city_id').val(),
            date: $('#date').val(),
            p: 1
        };
    }
    if ($('.planning-routes-table').length) {
    	data = {
            s: search,
            p: 1
        };
    }

    if ($('.warehouses-table').length) {
        data = {
            s: $('#input-search').val(),
            p: 1
        };
    }

    if ($('.qualities-table').length) {
        data = {
            city_id: $('#city_id').val(),
            warehouse_id: $('#warehouse_id').val(),
            s: $('#input-search').val(),
            p: 1
        };
    }

    if ($('.admin-shrinkages').length) {
        data = {
            city_id: $('#city_id').val(),
            warehouse_id: $('#warehouse_id').val(),
            p: 1
        };
    }

    if (typeof data === 'undefined'){
		data = { p: page, s: search };
	}
    if ($('.allied-store-table').length) {
        data = {
            s: $('#input-search').val(),
            p: 1
        };
    }

	$.ajax({
		url: web_url_ajax,
		data: data,
		type: 'GET',
		dataType: 'html',
		success: function(response) {
			$('.paging-loading').hide();
			$('.paging').html(response);
		}, error: function() {
			$('.paging-loading').hide();
			$('.paging').html('Ocurrió un error al obtener los datos.');
		}
	});
}


function numberWithCommas(x) {
    return '$' + x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}

function roundNumber(number, precision){
    precision = Math.abs(parseInt(precision)) || 0;
    var multiplier = Math.pow(10, precision);
    return (Math.round(number * multiplier) / multiplier);
}

var load_animation = {
    _init : function (element_id) {
        anim = bodymovin.loadAnimation({
            wrapper: document.getElementById(element_id),
            animType: 'svg',
            loop: true,
            prerender: true,
            autoplay: true,
            path: "/admin_asset/img/material_loader.json"
        });
    },
    _add_element : function (parent_id, loader_id) {
        $(parent_id).html('<div align="center" class="paging-loading img-loader center-block" id="'+loader_id+'" style="width: 40px; height: 40px;"></div>');
        this._init(loader_id);
    },
    _start : function () {
        var self = this;
        $.each($('.img-loader'), function(index, element){
            self._init($(element).attr('id'));
        });
    },
}

$(document).ready(function(){
	var searchButton = $('#btn-search');
	searchButton.click(search);
	searchButton.parents('form.admin-user-search-form').submit(search);

	function search(event) {
        event.preventDefault();
        paging(1, $('.search').val() || '');
	}

	$('#source').change(function(){
		if ($(this).val() == 'Device'){
			if (!$('#source_os').is(':visible'))
				$('#source_os').show();
		}else{
			$('#source_os').hide();
			$('#source_os').val('');
		}
	});

	$('.get-warehouses').change(function(){
		$('#warehouse_id').addClass('disabled');
		$.ajax({
			url: web_url_warehouses_ajax,
			data: { city_id: $('#city_id').val() },
			type: 'GET',
			dataType: 'json',
            success:
                function(response) {
                    $('#warehouse_id').empty();
                    $.each(response, function(key, value){
                        $('#warehouse_id').append('<option value="' + key + '">' + value + '</option>');
                    });
                    $('#warehouse_id').removeClass('disabled').trigger('change');
               }
        });
    });
});

