function copyToClipboard() {
    $('#deeplink_generated').select();
    document.execCommand("copy");
    alert('URL copiada al portapapeles.');
}
var Deeplink = (function() {
        'use strict';

        function Deeplink() {
            // enforces new
            if (!(this instanceof Deeplink)) {
                return new Deeplink();
            }
            // constructor body
            var form = $('#generate-deeplink');
            this.deeplink_type = $('#deeplink_type').val();
            this.deeplink_city_id = $('#deeplink_city_id').val();
            this.deeplink_store_id = $('#deeplink_store_id').val();
            this.deeplink_department_id = $('#deeplink_department_id').val();
            this.deeplink_shelf_id = $('#deeplink_shelf_id').val();
            this.get_stores_url = form.data('store-url');
            this.get_departments_url = form.data('department-url');
            this.get_shelves_url = form.data('shelves-url');
            this.get_products_url = form.data('product-url');
            this.bindActions();
        }

        Deeplink.prototype.bindActions = function() {
            let self = this;
            $('#deeplink').change(function() {
                let is_deeplink = $(this).val();
                $('#deeplink_generated').val('');
                if(is_deeplink == 1){
                    $('.deeplink_type, .deeplink_cities, .deeplink_store, .deeplink_departments, .deeplink_shelves, .deeplink_products').slideUp('fast');
                    $('.is_deeplink').slideDown('fast');
                    $('.deeplink_type').slideDown('fast');
                }else{
                    $('.deeplink_type, .deeplink_cities, .deeplink_store, .deeplink_departments, .deeplink_shelves, .deeplink_products').slideUp('fast');
                    $('.is_deeplink').slideDown('fast');
                    $('.deeplink_type').slideDown('fast');
                }
            });
            $('#deeplink_type').change(function() {
                self.deeplink_type = $(this).val();
                if($('#deeplink').val() == 1){
                    $('#deeplink_generated').val('mrq://merqueo?type=' + $(this).val());
                }
                $('.deeplink_cities, .deeplink_store, .deeplink_departments, .deeplink_shelves, .deeplink_products').slideUp('fast');
                $('#deeplink_city_id, #deeplink_store_id, #deeplink_department_id, #deeplink_shelf_id, #deeplink_product_id').prop('selectedIndex', 0);
                if ((self.deeplink_type !== '' && self.deeplink_type !== 'cart' && self.deeplink_type !== 'list_banners') || $('#deeplink').val() == 0) {
                    $('#deeplink_generated').val('');
                    $('.deeplink_cities').slideDown('fast');
                }else{
                    if($('#deeplink').val() != 1) {
                        $('#deeplink_generated').val('');
                    }
                    $('.deeplink_cities').slideUp('fast');
                }
            });
            $('#deeplink_city_id').change(function() {
                self.deeplink_city_id = $('#deeplink_city_id').val();
                $('#deeplink, #deeplink_type, #deeplink_city_id, #deeplink_store_id, #deeplink_department_id, #deeplink_shelf_id, #deeplink_product_id').prop('disabled', true);
                $('.deeplink_store, .deeplink_departments, .deeplink_shelves, .deeplink_products').slideUp('fast');
                $('#deeplink_store_id, #deeplink_department_id, #deeplink_shelf_id, #deeplink_product_id').prop('selectedIndex',0);
                self.getStores();
            });
            $('#deeplink_store_id').change(function() {
                self.deeplink_store_id = $('#deeplink_store_id').val();
                $('#deeplink, #deeplink_type, #deeplink_city_id, #deeplink_store_id, #deeplink_department_id, #deeplink_shelf_id, #deeplink_product_id').prop('disabled', true);
                if($('#deeplink').val() == 1){
                    $('#deeplink_generated').val('mrq://merqueo?type=' + $('#deeplink_type').val() + '&store_id=' + $(this).val());
                }
                $('.deeplink_departments, .deeplink_shelves, .deeplink_products').slideUp('fast');
                $('#deeplink_department_id, #deeplink_shelf_id, #deeplink_product_id').prop('selectedIndex',0);

                if (self.deeplink_type === 'department' || self.deeplink_type === 'shelf' || self.deeplink_type === 'product') {
                    self.getDepartments();
                } else if($('#deeplink').val() == 0) {
                    self.getDeeplinkStore();
                } else {
                    $('#deeplink, #deeplink_type, #deeplink_city_id, #deeplink_store_id, #deeplink_department_id, #deeplink_shelf_id, #deeplink_product_id').prop('disabled', false);
                }
            });
            $('#deeplink_department_id').change(function() {
                self.deeplink_department_id = $(this).val();
                $('#deeplink, #deeplink_type, #deeplink_city_id, #deeplink_store_id, #deeplink_department_id, #deeplink_shelf_id, #deeplink_product_id').prop('disabled', true);
                if($('#deeplink').val() == 1){
                    $('#deeplink_generated').val('mrq://merqueo?type=' + $('#deeplink_type').val() + '&store_id=' + $('#deeplink_store_id').val() + '&department_id=' + $(this).val());
                }
                $('.deeplink_shelves').slideUp('fast');
                $('#deeplink_shelf_id').prop('selectedIndex',0);

                if (self.deeplink_type === 'shelf' || self.deeplink_type === 'product') {
                    self.getShelves();
                }else if($('#deeplink').val() == 0){
                    self.getDeeplinkDepartment();
                }else{
                    $('#deeplink, #deeplink_type, #deeplink_city_id, #deeplink_store_id, #deeplink_department_id, #deeplink_shelf_id, #deeplink_product_id').prop('disabled', false);
                }
            });
            $('#deeplink_shelf_id').change(function() {
                self.deeplink_shelf_id = $(this).val();
                $('#deeplink, #deeplink_type, #deeplink_city_id, #deeplink_store_id, #deeplink_department_id, #deeplink_shelf_id, #deeplink_product_id').prop('disabled', true);
                if($('#deeplink').val() == 1){
                    $('#deeplink_generated').val('mrq://merqueo?type=' + $('#deeplink_type').val() + '&store_id=' + $('#deeplink_store_id').val() + '&department_id=' + $('#deeplink_department_id').val() + '&shelf_id=' + $(this).val());
                }
                $('.deeplink_products').slideUp('fast');
                $('#deeplink_product_id').prop('selectedIndex',0);

                if (self.deeplink_type === 'product') {
                    self.getProducts();
                }else if($('#deeplink').val() == 0){
                    self.getDeeplinkShelves();
                }else{
                    $('#deeplink, #deeplink_type, #deeplink_city_id, #deeplink_store_id, #deeplink_department_id, #deeplink_shelf_id, #deeplink_product_id').prop('disabled', false);
                }
            });
            $('#deeplink_store_product_id').change(function() {
                self.deeplink_store_product_id = $(this).val();
                $('#deeplink, #deeplink_type, #deeplink_city_id, #deeplink_store_id, #deeplink_department_id, #deeplink_shelf_id, #deeplink_product_id').prop('disabled', true);
                if($('#deeplink').val() == 1){
                    $('#deeplink_generated').val('mrq://merqueo?type=' + $('#deeplink_type').val() + '&store_id=' + $('#deeplink_store_id').val() + '&department_id=' + $('#deeplink_department_id').val() + '&shelf_id=' + $('#deeplink_shelf_id').val() + '&product_id=' + $(this).val());
                    $('#deeplink, #deeplink_type, #deeplink_city_id, #deeplink_store_id, #deeplink_department_id, #deeplink_shelf_id, #deeplink_product_id').prop('disabled', false);
                } else {
                    self.getDeeplinkStoreProduct();
                }
            });
        };

        Deeplink.prototype.getStores = function() {
            $.ajax({
                url: this.get_stores_url,
                type: 'GET',
                dataType: 'json',
                data: {city_id: this.deeplink_city_id},
                context: this
            })
            .done(function(data) {
                let deeplink_store_id = $('#deeplink_store_id');
                deeplink_store_id.empty();
                deeplink_store_id.append('<option value="">-Selecciona-</option>');
                $.each(data.result.stores, function(key, value) {
                    deeplink_store_id.append('<option value="' + value.id + '">'+ value.name + '</option>');
                });
                $('#deeplink, #deeplink_type, #deeplink_city_id, #deeplink_store_id, #deeplink_department_id, #deeplink_shelf_id, #deeplink_product_id').prop('disabled', false);
                $('.deeplink_store').slideDown('fast');
            })
            .fail(function(error) {
                alert(error.responseJSON);
                $('#deeplink, #deeplink_type, #deeplink_city_id, #deeplink_store_id, #deeplink_department_id, #deeplink_shelf_id, #deeplink_product_id').prop('disabled', false);
            });
        };

        Deeplink.prototype.getDeeplinkStore = function () {
            $.ajax({
                url: this.get_stores_url,
                type: 'GET',
                dataType: 'json',
                data: {type: this.deeplink_type, store_id: this.deeplink_store_id},
                context: this
            })
                .done(function(msg) {
                    $('#deeplink_generated').val(msg);
                    $('#deeplink, #deeplink_type, #deeplink_city_id, #deeplink_store_id, #deeplink_department_id, #deeplink_shelf_id, #deeplink_product_id').prop('disabled', false);
                })
                .fail(function(error) {
                    alert('Error al consultar deeplink.');
                });
        };

        Deeplink.prototype.getDepartments = function() {
            $.ajax({
                url: this.get_departments_url,
                type: 'GET',
                dataType: 'json',
                data: {store_id: this.deeplink_store_id},
                context: this
            })
            .done(function(msg) {
                let deeplink_department_id = $('#deeplink_department_id');
                deeplink_department_id.empty();
                deeplink_department_id.append('<option value="">-Selecciona-</option>');
                $.each(msg, function(key, value){
                    if(value.status === 1) deeplink_department_id.append('<option value="' + value.id + '">'+ value.name + '</option>');
                });
                $('#deeplink, #deeplink_type, #deeplink_city_id, #deeplink_store_id, #deeplink_department_id, #deeplink_shelf_id, #deeplink_product_id').prop('disabled', false);
                $('.deeplink_departments').slideDown('fast');
            })
            .fail(function(error) {
                alert(error.responseJSON);
                $('#deeplink, #deeplink_type, #deeplink_city_id, #deeplink_store_id, #deeplink_department_id, #deeplink_shelf_id, #deeplink_product_id').prop('disabled', false);
            });
        };

        Deeplink.prototype.getDeeplinkDepartment = function () {
            $.ajax({
                url: this.get_departments_url,
                type: 'GET',
                dataType: 'json',
                data: {department_id: this.deeplink_department_id},
                context: this
            })
            .done(function(msg) {
                $('#deeplink_generated').val(msg);
                $('#deeplink, #deeplink_type, #deeplink_city_id, #deeplink_store_id, #deeplink_department_id, #deeplink_shelf_id, #deeplink_product_id').prop('disabled', false);
            })
            .fail(function(error) {
                alert(error.responseJSON);
                $('#deeplink, #deeplink_type, #deeplink_city_id, #deeplink_store_id, #deeplink_department_id, #deeplink_shelf_id, #deeplink_product_id').prop('disabled', false);
            });
        };

        Deeplink.prototype.getShelves = function() {
            $.ajax({
                url: this.get_shelves_url,
                type: 'GET',
                dataType: 'json',
                data: {department_id: this.deeplink_department_id},
                context: this
            })
            .done(function(msg) {
                let deeplink_shelf_id = $('#deeplink_shelf_id');
                deeplink_shelf_id.empty();
                deeplink_shelf_id.append('<option value="">-Selecciona-</option>');
                $.each(msg,function(key,value){
                    if (value.status === 1) deeplink_shelf_id.append('<option value="' + value.id + '">'+ value.name + '</option>');
                });
                $('#deeplink, #deeplink_type, #deeplink_city_id, #deeplink_store_id, #deeplink_department_id, #deeplink_shelf_id, #deeplink_product_id').prop('disabled', false);
                $('.deeplink_shelves').slideDown('fast');
            })
            .fail(function(error) {
                alert(error.responseJSON);
                $('#deeplink, #deeplink_type, #deeplink_city_id, #deeplink_store_id, #deeplink_department_id, #deeplink_shelf_id, #deeplink_product_id').prop('disabled', false);
            });
        };

        Deeplink.prototype.getDeeplinkShelves = function() {
            $.ajax({
                url: this.get_shelves_url,
                type: 'GET',
                dataType: 'json',
                data: {shelf_id: this.deeplink_shelf_id},
                context: this
            })
            .done(function(msg) {
                $('#deeplink_generated').val(msg);
                $('#deeplink, #deeplink_type, #deeplink_city_id, #deeplink_store_id, #deeplink_department_id, #deeplink_shelf_id, #deeplink_product_id').prop('disabled', false);
            })
            .fail(function(error) {
                alert(error.responseJSON);
                $('#deeplink, #deeplink_type, #deeplink_city_id, #deeplink_store_id, #deeplink_department_id, #deeplink_shelf_id, #deeplink_product_id').prop('disabled', false);
            });
        };

        Deeplink.prototype.getProducts = function() {
            $.ajax({
                url: this.get_products_url,
                type: 'GET',
                dataType: 'json',
                data: {
                    shelf_id: this.deeplink_shelf_id
                }
            })
            .done(function(data) {
                let deeplink_store_product_id = $('#deeplink_store_product_id');
                deeplink_store_product_id.empty();
                deeplink_store_product_id.append('<option value="">-Selecciona-</option>');
                $.each(data,function(key,value){
                    deeplink_store_product_id.append('<option value="' + value.id + '">'+ value.name + '</option>');
                });
                $('#deeplink, #deeplink_type, #deeplink_city_id, #deeplink_store_id, #deeplink_department_id, #deeplink_shelf_id, #deeplink_product_id').prop('disabled', false);
                $('.deeplink_products').slideDown('fast');
            })
            .fail(function(error) {
                alert(error.responseJSON);
                $('#deeplink, #deeplink_type, #deeplink_city_id, #deeplink_store_id, #deeplink_department_id, #deeplink_shelf_id, #deeplink_product_id').prop('disabled', false);
            });
        };

        Deeplink.prototype.getDeeplinkStoreProduct = function(){
            $.ajax({
                url: this.get_products_url,
                type: 'GET',
                dataType: 'json',
                data: {store_product_id: this.deeplink_store_product_id},
                context: this
            })
            .done(function(msg) {
                $('#deeplink_generated').val(msg);
                $('#deeplink, #deeplink_type, #deeplink_city_id, #deeplink_store_id, #deeplink_department_id, #deeplink_shelf_id, #deeplink_product_id').prop('disabled', false);
            })
            .fail(function(error) {
                alert(error.responseJSON);
                $('#deeplink, #deeplink_type, #deeplink_city_id, #deeplink_store_id, #deeplink_department_id, #deeplink_shelf_id, #deeplink_product_id').prop('disabled', false);
            });
        };

        return Deeplink;
    }());
$(document).ready(function(){
    let deeplink =new Deeplink;
});