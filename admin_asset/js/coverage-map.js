function MapPosition(lat, lng, element, validator) {
    this.poly = null;
    this.canvasMap = null;
    this.element = element || document.getElementById("map_canvas");
    this.lat = lat;
    this.lng = lng;
    this.validator = validator;
    this.direccionLatLong = lat + ',' + lng;
    this.polyOptions = {
        strokeColor: "#f33f00",
        strokeOpacity: 0.8,
        strokeWeight: 4,
        fillColor: "#FF0000",
        fillOpacity: 0.2,
        editable: true
    };
}

MapPosition.prototype.init = function () {
    let self = this;
    this.myOptions = {center: new google.maps.LatLng(this.lat, this.lng), zoom: 10, mapTypeId: google.maps.MapTypeId.ROADMAP};
    this.canvasMap = new google.maps.Map(this.element, this.myOptions);
    this.poly = new google.maps.Polygon(this.polyOptions);
    this.poly.setMap(this.canvasMap);
    this._mapInit();

    google.maps.event.addListener(this.canvasMap, 'click', function (event) {
        if (!self.validator || self.validator(event)) {
            self.addLatLng(event.latLng);
        }
    });
};

MapPosition.prototype.center = function () {
    this.canvasMap.setCenter(new google.maps.LatLng(this.lat, this.lng));
};

MapPosition.prototype.undo = function () {
    let path = this.poly.getPath();
    if (path.length > 0) {
        path.removeAt(path.length - 1);
    }
};

MapPosition.prototype.hasPoint = function () {
    let path = this.poly.getPath();
    return path.length > 0;
};

MapPosition.prototype.addLatLng = function(pos) {
    let path = this.poly.getPath();
    path.push(pos);
};

MapPosition.prototype.clear = function() {
    let path = this.poly.getPath();
    while(path.length > 0) {
        path.removeAt(0);
    }
};

MapPosition.prototype.getCoordinates = function() {
    if (this.poly) {
        let coverage = this.poly.getPath().getArray();
        return coverage.join(';').replace(/ /g, '').replace(/,/g, ' ').replace(/;/g, ',').replace(/\(|\)/g, '');
    }

    return '';
};

MapPosition.prototype.centerByPolygon = function (coverage) {
    let bounds = new google.maps.LatLngBounds();

    for (let i = 0; i < coverage.length; i++) {
        let coodinates = coverage[i].split(' ');
        bounds.extend( new google.maps.LatLng( coodinates[0], coodinates[1]) );
    }

    let center = bounds.getCenter();
    this.canvasMap.setCenter(new google.maps.LatLng(center.lat(), center.lng()));
};

MapPosition.pointInPolygon = function (polygon, latitude, longitude) {
    let inside = false;
    for (let i = 0, j = polygon.length - 1; i < polygon.length; j = i++) {
        let xi = polygon[i][0], yi = polygon[i][1];
        let xj = polygon[j][0], yj = polygon[j][1];

        let intersect = ((yi > longitude) != (yj > longitude))
            && (latitude < (xj - xi) * (longitude - yi) / (yj - yi) + xi);
        if (intersect) inside = !inside;
    }

    return inside;
};

MapPosition.prototype.addPolygon = function (polygon) {
    polygon.setMap(this.canvasMap);
};

MapPosition.prototype._mapInit = function() {
    // this.canvasMap.setUIToDefault();
    //
    // this.canvasMap.disableContinuousZoom();
    // this.canvasMap.disableDoubleClickZoom();
    // this.canvasMap.disableScrollWheelZoom();
    //
    // this.canvasMap.addControl(new GLargeMapControl());
    // this.canvasMap.addControl(new GMapTypeControl());
    // this.canvasMap.addControl(new GScaleControl());

    this.color = this._makeColor();
    let zonesDraw = new google.maps.Polygon({
        path: [],
        strokeColor: this.color,
        strokeOpacity: 1.0,
        strokeWeight: 3,
        fillColor: this.color,
        fillOpacity: 0.35
    });

    zonesDraw.setMap(this.canvasMap);
};

MapPosition.prototype._makeColor = function() {
    let hexVal = "0123456789ABCDEF".split("");

    return '#' + hexVal.sort(function () {
        return (Math.round(Math.random()) - 0.5);
    }).slice(0, 6).join('');
};
