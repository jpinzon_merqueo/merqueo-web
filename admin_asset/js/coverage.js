var poly, map,
	polyOptions = {
		strokeColor: "#f33f00",
		strokeOpacity: 0.8,
		strokeWeight: 4,
		fillColor: "#FF0000",
		fillOpacity: 0.2, 
		editable: true
	};

function mapPosition(lat, lng, element) {
	element = element || document.getElementById("map_canvas");
	if(typeof lat === 'undefined' || typeof lng === 'undefined') return false;
	direccionLatLong = lat + ',' + lng;
	if(map == null) {
		var myOptions = { center: new google.maps.LatLng(lat, lng),zoom: 10, mapTypeId: google.maps.MapTypeId.ROADMAP };
		map = new google.maps.Map(element, myOptions);
		
		google.maps.event.addListener(map, 'click', function(event) {
			var location = event.latLng; 
			addLatLng(location);
		});
				
		poly = new google.maps.Polygon(polyOptions);
		poly.setMap(map);
		
		$('.undo-poly').click(function() {
			var path = poly.getPath();
			if(path.length > 0) path.removeAt(path.length - 1);
			if(path.length === 0) $('.undo-poly').addClass('disabled');
		});//.tooltip('show');
	} else {
		map.setCenter(new google.maps.LatLng(lat, lng));
	}
}

function addLatLng(pos) {
	var path = poly.getPath();
		path.push(pos);
	if($('.undo-poly').hasClass('disabled')) $('.undo-poly').removeClass('disabled');
	$('input[name="poly"]').val(getCoverage()); // Actualiza la variable con la coverage
}

function getCoverage() {
	if(typeof poly !== 'undefined') {
		var coverage = poly.getPath().getArray();
		if(!coverage || coverage.length <= 0) {
			//alert('Debes definir una coverage');
			//return false;
		}
	} else {
		return "";		
	}

	return coverage.join(';').replace(/ /g, '').replace(/,/g, ' ').replace(/;/g, ',').replace(/\(|\)/g, '');
}

function mapInit() {
	map.setUIToDefault();
	 
	map.disableContinuousZoom();
	map.disableDoubleClickZoom(); 
	map.disableScrollWheelZoom();
	 
	map.addControl(new GLargeMapControl());
	map.addControl(new GMapTypeControl());
	map.addControl(new GScaleControl()) ;
}

function trim(s) {
	if(typeof s == 'undefined' || s == null ) return '';
	return s.replace(/^\s+/g,'').replace(/\s+$/g,'');
}